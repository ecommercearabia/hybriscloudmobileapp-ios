//
//  ProductDetailsUseCase.swift
//  NetworkPlatform
//
//  Created by khalil on 8/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class ProductDetailsUseCase<Cache> : Domain.ProductDetailsUseCase where Cache : AbstractCache , Cache.T == ProductDetails {
    
    private let network : ProductDetailsNetwork
    private let cache : Cache
    init(network : ProductDetailsNetwork , cache : Cache) {
        self.network = network
        self.cache = cache
    }
    
    func productDetails(productCode: String) -> Observable<ProductDetails> {
        let fetchComponent = cache.fetch(withID: productCode).asObservable()
        let stored = network.fetchProductDetails(productCode: productCode).flatMap{
            return self.cache.save(object: $0)
            .asObservable()
                .map(to: ProductDetails.self)
                .concat(Observable.just($0))
        }
        return fetchComponent.concat(stored)
    }
}

final class ProductReviewsUseCase<Cache>: Domain.ProductReviewsUseCase where Cache: AbstractCache, Cache.T == Reviews {
   
    
    private let network: ProductDetailsNetwork
    private let cache: Cache
    
    init(network: ProductDetailsNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }

    func productReviews(productCode: String) -> Observable<Reviews> {
        return network.productReviews(productCode: productCode)
       }
   

}

final class ProductReviewUseCase<Cache>: Domain.ProductReviewUseCase where Cache: AbstractCache, Cache.T == Review {
   
    
    private let network: ProductDetailsNetwork
    private let cache: Cache
    
    init(network: ProductDetailsNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }


     func addReviews(productCode : String ,review : Review) -> Observable<Review> {
            return network.addReviews(productCode: productCode,review : review)
        }

}

