//
//  SavedCardsUseCase.swift
//  NetworkPlatform
//
//  Created by walaa omar on 26/12/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class SavedCardsUseCase<Cache>: Domain.SavedCardsUseCase where Cache: AbstractCache, Cache.T == SavedCardsModel {
    
  
    private let network: SavedCardsNetwork
    private let cache: Cache
    
    init(network: SavedCardsNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    func fetchSavedCards(userId: String) -> Observable<SavedCardsModel> {
        
        let fetchComponent = cache.fetch(withID: userId).asObservable()
        
                        let stored = network.fetchSavedCards(userId: userId)
                            .flatMap {
                                return self.cache.save(object: $0)
                                    .asObservable()
                                    .map(to: SavedCardsModel.self)
                                    .concat(Observable.just($0))
                        }
                        
                        return fetchComponent.concat(stored)
    }
    

    func deleteSavedCards(userId: String, savedCardId: String) -> Observable<Void> {
        return network.deleteSavedCards(userId: userId, savedCardId: savedCardId).map({_ in})
    }

}
