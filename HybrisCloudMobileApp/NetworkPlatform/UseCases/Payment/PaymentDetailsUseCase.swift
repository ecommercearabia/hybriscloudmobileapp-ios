//
//  PaymentDetailsUseCase.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 8/18/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class PaymentDetailsUseCase<Cache>: Domain.PaymentDetailsUseCase where Cache: AbstractCache, Cache.T == PaymentDetailsResponse {
 
 
    private let network: PaymentDetailsNetwork
    private let cache: Cache
    
    init(network: PaymentDetailsNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }

  func cartPaymentDetials(paymentDetails: PaymentDetailsRequest) -> Observable<PaymentDetailsResponse> {
    return network.setCartPaymentDetails(paymentDetials: paymentDetails)
     }
    func cartPaymentDetials(trackingId: String) -> Observable<PaymentDetailsResponse> {
        return network.setCartPaymentDetails(trackingId: trackingId)
    }
}

final class PaymentInfoUseCase<Cache>: Domain.PaymentInfoUseCase where Cache: AbstractCache, Cache.T == PaymentInfo {
   
 
 
    private let network: PaymnetInfoNetwork
    private let cache: Cache
    
    init(network: PaymnetInfoNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }


    func cartPaymentInfo() -> Observable<PaymentInfo> {
        return network.getCartPaymentInfo()
       }
       
     
}
