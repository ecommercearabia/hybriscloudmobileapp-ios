//
//  hipmentTypes.swift
//  NetworkPlatform
//
//  Created by khalil on 12/2/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class ShipmentTypesUseCase<Cache>: Domain.ShipmentTypesUseCase where Cache: AbstractCache, Cache.T == ShipmentTypesModel {
    
    private let network: ShipmentTypesNetwork
    private let cache: Cache
    init(network: ShipmentTypesNetwork , cache: Cache) {
        self.network = network
     
        self.cache = cache
    }
    
    func getShipmentTypes() -> Observable<ShipmentTypesModel> {
        return network.getShipmentTypes()
    }
    
    func selectedShipmentType(code: String) -> Observable<ShipmentSelctedShipmentTypesModel> {
        return network.selectedShipmentType(code: code)
    }
    
    func getType() -> Observable<CurrentShipmentTypeModel> {
        return network.getShipmentTypes()
    }
    

}
