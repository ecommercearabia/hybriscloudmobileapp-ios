//
//  HomeUseCase.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 7/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class PagesUseCase<Cache>: Domain.PagesUseCase where Cache: AbstractCache, Cache.T == Page {
    private let network: PagesNetwork
    private let cache: Cache
    
    init(network: PagesNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
   /* func pages() -> Observable<[Page]> {
        let fetchPages = cache.fetchObjects().asObservable()
        let stored = network.fetchPages()
            .flatMap {
                return self.cache.save(objects: $0)
                    .asObservable()
                    .map(to: [Page].self)
                    .concat(Observable.just($0))
        }
        
        return fetchPages.concat(stored)
    }
    */
    func page(pageId: String) -> Observable<Page> {
        let fetchPage = cache.fetch(withID: pageId).asObservable()
        let stored = network.fetchPage(pageId: pageId)
            .flatMap {
                return self.cache.save(object: $0)
                    .asObservable()
                    .map(to: Page.self)
                    .concat(Observable.just($0))
        }
        
        return fetchPage.concat(stored)
    }
    
    func save(page: Page) -> Observable<Void> {
        return network.createPage(page: page)
            .map { _ in }
    }
    
    func delete(page: Page) -> Observable<Void> {
        return network.deletePage(pageId: page.identity).map({_ in})
    }
    
}

struct MapFromNever: Error {}
extension ObservableType where Element == Never {
    func map<T>(to: T.Type) -> Observable<T> {
        return self.flatMap { _ in
            return Observable<T>.error(MapFromNever())
        }
    }
}
