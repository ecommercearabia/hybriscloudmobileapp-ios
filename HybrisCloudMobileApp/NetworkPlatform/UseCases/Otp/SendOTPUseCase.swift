//
//  SendOTPUseCase.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import Domain
import RxSwift

final class SendOTPUseCase<Cache>: Domain.SendOTPUseCase where Cache: AbstractCache, Cache.T == SendOTP {
 
    
    private let network: SendOTPNetwork
    private let cache: Cache
    
    init(network: SendOTPNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    

    func sendOtp(sendOtpParam: SendOtpModel) -> Observable<SendOTP> {
          return network.sendOtp(sendOtpParam: sendOtpParam)
     }
     
}

