//
//  VerifyOtpUseCase.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class VerifyOtpUseCase<Cache>: Domain.VerifyOTPUseCase where Cache: AbstractCache, Cache.T == VerifyOTP {
  
    
    private let network: VerifyOTPNetwork
    private let cache: Cache
    
    init(network: VerifyOTPNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
   
      func vrifyOTP(sendOtpParam: SendOtpModel) -> Observable<VerifyOTP> {
        return network.verifyOtp(sendOtpParam:sendOtpParam )
        }
        
     
}

