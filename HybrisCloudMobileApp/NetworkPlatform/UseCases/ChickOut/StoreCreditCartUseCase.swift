//
//  StoreCreditCartUseCase.swift
//  NetworkPlatform
//
//  Created by ahmadSaleh on 9/7/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift


final class StoreCreditCartUseCase<Cache>: Domain.StoreCreditCartUseCase where Cache: AbstractCache {
   
   private let network: StoreCreditCartNetwork
   private let cache: Cache

   init(network: StoreCreditCartNetwork, cache: Cache) {
    self.network = network
       self.cache = cache
   }
 
 
 func getStoreCreditModes(cartId : String , userId : String) -> Observable<StoreCreditModes> {
    return network.getStoreCreditModes(cartId: cartId, userId: userId)
  }
    
    
 func setStoreCreditMode(amount : Double ,cartId : String , storeCreditModeTypeCode : String , userId : String) -> Observable<Void> {
     return network.setStoreCreditMode(amount: amount, cartId: cartId, storeCreditModeTypeCode: storeCreditModeTypeCode, userId: userId)
  }

    func getStoreCreditAvailableAmount(userId: String) -> Observable<StoreCreditAvailableAmountModel> {
       return network.getStoreCreditAvailableAmount(userId: userId)
    }


}

