//
//  ChickOutUseCase.swift
//  NetworkPlatform
//
//  Created by ahmadSaleh on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift


final class ChickOutUseCase<Cache>: Domain.chickOutUseCase where Cache: AbstractCache, Cache.T == TimeSlotsModel  {
 

   private let network: ChickOutNetwork
   private let cache: Cache

   init(network: ChickOutNetwork, cache: Cache) {
       self.network = network
       self.cache = cache
   }

    func SetDeliveryAddress(addressId : String) -> Observable<Void> {
        return network.setDeliveryAddress(addressId: addressId)
    }
    func getTimeSlots() -> Observable<TimeSlotsModel> {
        let fetchComponent = cache.fetch(withID: "cashId").asObservable()
        let stored = network.getTimeSlots().flatMap {
            return self.cache.save(object: $0)
                .asObservable()
                .map(to: TimeSlotsModel.self)
                .concat(Observable.just($0))
        }
        
        return fetchComponent.concat(stored)

    }
    func setTimeSlots(cartId: String, timeSlotInfos: SetTimeSlotModel) -> Observable<Void> {
        return network.setTimeSlots(cartId: cartId, timeSlotInfos: timeSlotInfos)

    }
    
    


}


 
