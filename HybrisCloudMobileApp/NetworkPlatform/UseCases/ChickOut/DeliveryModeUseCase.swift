//
//  DeliveryModeUseCase.swift
//  NetworkPlatform
//
//  Created by ahmadSaleh on 8/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

 
import Foundation
import Domain
import RxSwift


final class DeliveryModeUseCase<Cache>: Domain.setDeliveryModeUseCase where Cache: AbstractCache, Cache.T == DeliveryModeModel  {


   private let network: DeliveryModeNetwork
   private let cache: Cache

   init(network: DeliveryModeNetwork, cache: Cache) {
       self.network = network
       self.cache = cache
   }

    func getDeliveryModes(cartId: String) -> Observable<DeliveryModeModel> {
        return network.getDeliveryModes(cartId: cartId)
    }

    func setDeliveryMode(deliveryModeId: String, cartId: String) -> Observable<Void> {
        return network.setDeliveryMode(deliveryModeId: deliveryModeId, cartId: cartId)
    }



}

