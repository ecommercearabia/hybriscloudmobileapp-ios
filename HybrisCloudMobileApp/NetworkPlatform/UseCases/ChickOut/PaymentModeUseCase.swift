//
//  PaymentUseCase.swift
//  NetworkPlatform
//
//  Created by ahmadSaleh on 8/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift


final class PaymentModeUseCase<Cache>: Domain.paymentModeUseCase where Cache: AbstractCache, Cache.T == PaymentModesModel  {
    


   private let network: PaymentNetwork
   private let cache: Cache

   init(network: PaymentNetwork, cache: Cache) {
       self.network = network
       self.cache = cache
   }
 
        func getPaymentModes() -> Observable<PaymentModesModel> {
            return network.getPaymentModes()
    }
    
    func setPaymentMode(paymentModeCode: String) -> Observable<SetPaymentModeResponse> {
        return network.setPaymentModes(paymentModeCode: paymentModeCode)
    }


}

