//
//  ApplePayUseCase.swift
//  NetworkPlatform
//
//  Created by khalil on 5/8/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//


import Foundation
import Domain
import RxSwift

final class ApplePayUseCase<Cache> : Domain.applePayUseCase where
Cache : AbstractCache , Cache.T ==

ApplePayModel {
    
    private let network: ApplePayNetwork
    private let cache: Cache

    init(network: ApplePayNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    func setApplePay(paymentOption: String, cardType: String, cardName: String, paymentData: String, appleCardType: String, amount: String, currency: String, orderId: String, userId: String, cartId: String) -> Observable<ApplePayModel> {
        return network.setApplePay(paymentOption: paymentOption, cardType: cardType, cardName: cardName, paymentData: paymentData, appleCardType: appleCardType, amount: amount, currency: currency, orderId: orderId, userId: userId, cartId: cartId)
    }
}
