//
//  CountriesUseCase.swift
//  NetworkPlatform
//
//  Created by Admin on 2020-08-03.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class CountriesUseCase<Cache>: Domain.CountryUseCase where Cache: AbstractCache, Cache.T == CountriesModel {
    
    private let countriesNetwork: CountriesNetwork
    private let cache: Cache
    
    init(network: CountriesNetwork, cache: Cache) {
        self.countriesNetwork = network
        self.cache = cache
    }
    
    func fetchCountries() -> Observable<CountriesModel> {
        let fetchCountries = cache.fetch().asObservable()

        let stored = countriesNetwork.fetchCountries()
            .flatMap {
                return self.cache.save(object: $0)
                                .asObservable()
                                .map(to: CountriesModel.self)
                                .concat(Observable.just($0))
        }
        
        return fetchCountries.concat(stored)
    }
    
    func fetchCountriesMobile() ->  Observable<CountriesModel> {
        let fetchCountries = cache.fetch().asObservable()

               let stored = countriesNetwork.fetchCountriesMobile()
                   .flatMap {
                       return self.cache.save(object: $0)
                                       .asObservable()
                                       .map(to: CountriesModel.self)
                                       .concat(Observable.just($0))
               }
               
               return fetchCountries.concat(stored)
        
    }
    
    func fetchCountriesShipping() ->  Observable<CountriesModel> {
        let fetchCountries = cache.fetch().asObservable()

               let stored = countriesNetwork.fetchCountriesShipping()
                   .flatMap {
                       return self.cache.save(object: $0)
                                       .asObservable()
                                       .map(to: CountriesModel.self)
                                       .concat(Observable.just($0))
               }
               
               return fetchCountries.concat(stored)
        
    }
}

final class RegionsUseCase<Cache>: Domain.RegionsUseCase where Cache: AbstractCache, Cache.T == RegionsModel {
    
    private let regionsNetwork: RegionsNetwork
    private let cache: Cache
    
    
    init(network: RegionsNetwork, cache: Cache) {
        self.regionsNetwork = network
        self.cache = cache
    }
    
    func fetchRegions(countryIsoCode: String) -> Observable<RegionsModel> {
        let stored = regionsNetwork.fetchRegions(countyIsoCode: countryIsoCode)
            .flatMap {
                return self.cache.save(object: $0)
                    .asObservable()
                    .map(to: RegionsModel.self)
        }
        
        return stored
    }
}


final class CitiesUseCase<Cache>: Domain.CitiesUseCase where Cache: AbstractCache, Cache.T == CitiesModel {
    
    private let citiesNetwork: CitiesNetwork
    private let cache: Cache
    
    
    init(network: CitiesNetwork, cache: Cache) {
        self.citiesNetwork = network
        self.cache = cache
    }
    
    func fetchCities(countryIsoCode: String) -> Observable<CitiesModel> {
    
        let fetchCities = cache.fetch().asObservable()

        let stored = citiesNetwork.fetchCities(countryIsoCode: countryIsoCode)
            .flatMap {
                return self.cache.save(object: $0)
                                .asObservable()
                                .map(to: CitiesModel.self)
                                .concat(Observable.just($0))
        }
        
        return fetchCities.concat(stored)
    }
}
