//
//  TitlesUseCase.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 7/21/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import Domain
import RxSwift

final class MiscsUseCase<Cache>: Domain.MiscsUseCase where Cache: AbstractCache, Cache.T == Titles {

    private let network: MiscsNetwork
    private let cache: Cache

    init(network: MiscsNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }

    func titles() -> Observable<Titles> {
        let fetchTitles = cache.fetch().asObservable()
        let stored = network.fetchTitles()
            .flatMap {
                return self.cache.save(object: $0)
                    .asObservable()
                    .map(to: Titles.self)
                    .concat(Observable.just($0))
            }
        
        return fetchTitles.concat(stored)
    }
    

}
