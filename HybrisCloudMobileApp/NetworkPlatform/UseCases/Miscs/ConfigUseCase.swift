//
//  ConfigUseCase.swift
//  NetworkPlatform
//
//  Created by khalil on 4/2/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class ConfigUseCase<Cache>: Domain.ConfigUseCase where Cache: AbstractCache, Cache.T == ConfigModel {

    private let network: ConfigNetwork
    private let cache: Cache

    init(network: ConfigNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }

    func config() -> Observable<ConfigModel> {
        let fetchConfig = cache.fetch().asObservable()
        let stored = network.fetchConfig()
            .flatMap {
                return self.cache.save(object: $0)
                    .asObservable()
                    .map(to: ConfigModel.self)
                    .concat(Observable.just($0))
            }
        
        return fetchConfig.concat(stored)
    }
    

}
