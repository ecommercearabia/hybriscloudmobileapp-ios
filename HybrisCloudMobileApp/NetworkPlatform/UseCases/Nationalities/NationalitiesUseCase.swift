//
//  NationalitiesUseCase.swift
//  NetworkPlatform
//
//  Created by khalil on 4/2/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class NationalitiesUseCase<Cache>: Domain.NationalitiesUseCase where Cache: AbstractCache, Cache.T == NationalitiesModel {
    
  
    private let network: NationalitiesNetwork
    private let cache: Cache
    
    init(network: NationalitiesNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    func fetchNationalities() -> Observable<NationalitiesModel> {
        
        let fetchComponent = cache.fetch().asObservable()
        
                        let stored = network.fetchNationalities()
                            .flatMap {
                                return self.cache.save(object: $0)
                                    .asObservable()
                                    .map(to: NationalitiesModel.self)
                                    .concat(Observable.just($0))
                        }
                        return fetchComponent.concat(stored)
    }
    
}
