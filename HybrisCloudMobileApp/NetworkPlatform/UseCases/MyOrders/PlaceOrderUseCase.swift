//
//  PlaceOrderUseCase.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 8/18/20.
//  Copyright © 2020 Erabia. All rights reserved.
//
import Foundation
import Domain
import RxSwift
final class PlaceOrderUseCase<Cache>: Domain.PlaceOrderUserCase where Cache: AbstractCache, Cache.T == PalceOrderResponse {
    
    private let network: PlaceOrderNetwork
    private let cache: Cache
    
    init(network: PlaceOrderNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    
    func setPlaceOrderUserCase(cartId : String) -> Observable<PalceOrderResponse> {
        return network.setPlaceOrder(cartId : cartId)
    }
    
    
    
}
