//
//  OrdersDetailsUseCase.swift
//  NetworkPlatform
//
//  Created by khalil on 8/13/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift
final class OrdersDetailsUseCase<Cache> : Domain.OrdersDetailsUseCase where Cache : AbstractCache, Cache.T == ordersDetailsMyOrdersDetails{
    private let network : OrdersDetailsNetwork
    private let cache : Cache
       init(network : OrdersDetailsNetwork , cache : Cache) {
           self.network = network
           self.cache = cache
       }
    
    func getOrdersDetails(userId: String, orderCode: String) -> Observable<ordersDetailsMyOrdersDetails> {
        let fetchOrdersDetails = cache.fetch(withID: userId).asObservable()
        
        let stored = network.getOrdersDetails(userId: userId, code: orderCode).flatMap{
            return self.cache.save(object : $0)
                .asObservable()
                .map(to : ordersDetailsMyOrdersDetails.self)
                .concat( Observable.just($0))
        
    }
    return fetchOrdersDetails.concat(stored)
        
        
    }
}
