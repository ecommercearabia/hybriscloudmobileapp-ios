//
//  MyOrdersUseCase.swift
//  NetworkPlatform
//
//  Created by khalil on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class MyOrdersUseCase<Cache> : Domain.MyOrdersUseCase where Cache : AbstractCache , Cache.T == MyOrders {
    private let network : MyOrdersNetwork
    private let cache : Cache
    init(network : MyOrdersNetwork , cache : Cache) {
        self.network = network
        self.cache = cache
    }
    
    func getMyOrders(currentPage: Int, pageSize: Int, statuses: String, sort: String, userId: String) -> Observable<MyOrders> {
        let fetchOrders = cache.fetch(withID: userId).asObservable()
        let stored = network.getMyOrders(currentPage: currentPage, pageSize: pageSize, statuses: statuses, sort: sort, userId: userId).flatMap{
            return self.cache.save(object: $0).asObservable()
                .map(to: MyOrders.self).concat(Observable.just($0))
        }
        return fetchOrders.concat(stored)
    }
    
    
    
}
