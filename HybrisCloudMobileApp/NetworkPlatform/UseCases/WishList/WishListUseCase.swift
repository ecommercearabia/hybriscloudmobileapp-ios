//
//  WishListUseCase.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 8/26/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import Domain
import RxSwift

final class WishListUseCase<Cache>: Domain.WishListUseCase where Cache: AbstractCache, Cache.T == WishList {
  
   
    private let network: WishListNetwork
    private let cache: Cache
    
    init(network: WishListNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }

    
    func getWishList() -> Observable<WishList> {
        let fetchComponent = cache.fetch().asObservable()
        let stored = network.fetchWishList().flatMap {
                    return self.cache.save(object: $0)
                        .asObservable()
                        .map(to: WishList.self)
                        .concat(Observable.just($0))
                }
                
                return fetchComponent.concat(stored)
      }
    
    func deleteWishListIteam(deleteIteamData: AddDeleteWishListItemRequest) -> Observable<Void> {
        return network.deleteWishListIteam(deleteIteamData: deleteIteamData).map({_ in})
            
        }
    func addWishListIteam(iteamData: AddDeleteWishListItemRequest) -> Observable<Void> {
        return network.addWishListIteam(iteamData: iteamData).map({_ in})
            
        }
    
    }
    

      

