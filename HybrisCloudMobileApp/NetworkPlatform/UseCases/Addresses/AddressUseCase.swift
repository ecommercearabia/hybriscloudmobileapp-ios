//
//  AddressUseCase.swift
//  NetworkPlatform
//
//  Created by Admin on 2020-08-03.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class AddressUseCase<Cache>: Domain.AddressesUseCase where Cache: AbstractCache, Cache.T == AddressesModel {
    
  
    private let network: AddressesNetwork
    private let cache: Cache
    
    init(network: AddressesNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    func fetchAddresses(userId: String) -> Observable<AddressesModel> {
        
        let fetchComponent = cache.fetch(withID: userId).asObservable()
        
                        let stored = network.fetchAddresses(userId: userId)
                            .flatMap {
                                return self.cache.save(object: $0)
                                    .asObservable()
                                    .map(to: AddressesModel.self)
                                    .concat(Observable.just($0))
                        }
                        
                        return fetchComponent.concat(stored)
    }
    
//    func getAddress(userId: String, addressId: String) -> Observable<Address> {
//        let fetchAddresses = cache.fetch(withID: addressId).asObservable()
//        let stored = network.getAddress(userId: userId, addressId: addressId)
//            .flatMap {
//                return self.cache.save(object: $0)
//                    .asObservable()
//                    .map(to: Address.self)
//                    .concat(Observable.just($0))
//        }
//
//        return fetchAddresses.concat(stored)
//    }
    
//    func addAddress(userId: String, address: Address) -> Observable<Address> {
//        let fetchComponent = cache.fetch(withID: userId).asObservable()
//
//        let stored = network.addAddress(userId: userId, address: address)
//                                 .flatMap {
//                                     return self.cache.save(object: $0)
//                                         .asObservable()
//                                         .map(to: Address.self)
//                                         .concat(Observable.just($0))
//                             }
//
//                             return fetchComponent.concat(stored)
//
//    }
//
    func deleteAddress(userId: String, addressId: String) -> Observable<Void> {
        return network.deleteAddress(userId: userId, addressId: addressId).map({_ in})
    }

//    func updateAddress(userId: String, address: Address) -> Observable<Void> {
//        return network.updateAddress(userId: userId, addressId: address.identity, address: address)
//    }
    
    func setDefaultAddress(userId: String, address: Address) -> Observable<Void> {
        return network.updateAddress(userId: userId, addressId: address.identity, address: address)
    }

//    func verifiesAddress(userId: String, address: Address) -> Observable<Address> {
//        return network.verifyAddress(userId: userId, addressId: address.identity, address: address)
//    }
}
