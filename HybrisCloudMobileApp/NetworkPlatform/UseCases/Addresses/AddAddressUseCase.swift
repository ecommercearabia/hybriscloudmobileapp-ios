//
//  AddAddressUseCase.swift
//  NetworkPlatform
//
//  Created by walaa omar on 8/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class AddAddressUseCase<Cache>: Domain.AddAddressesUseCase where Cache: AbstractCache, Cache.T == Address {
  
    private let network: AddressesNetwork
    private let cache: Cache
    
    init(network: AddressesNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    

    
    func addAddress(userId: String, address: Address) -> Observable<Address> {
        let fetchComponent = cache.fetch(withID: userId).asObservable()

        let stored = network.addAddress(userId: userId, address: address)
                                 .flatMap {
                                
                                     return self.cache.save(object: $0)
                                         .asObservable()
                                         .map(to: Address.self)
                                         .concat(Observable.just($0))
                             }
                             
                             return fetchComponent.concat(stored)
        
    }
    
    func updateAddress(userId: String, address: Address) -> Observable<Void> {
        return network.updateAddress(userId: userId, addressId: address.identity, address: address)

    }

}
