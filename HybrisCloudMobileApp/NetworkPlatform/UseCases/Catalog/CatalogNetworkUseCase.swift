//
//  CatalogNetworkUseCase.swift
//  NetworkPlatform
//
//  Created by Ahmad Bader on 8/1/20.
//  Copyright © 2020 Erabia. All rights reserved.
//
 import Foundation
import Domain
import RxSwift

final class CatalogNetworkUseCase<Cache>: Domain.CatalogUseCase where Cache: AbstractCache, Cache.T == Catalog {

    private let network: CatalogNetwork
    private let cache: Cache
    
    init(network: CatalogNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
   func categories(id: String, version: String) -> Observable<Catalog> {
          let fetchComponent = cache.fetch(withID: version).asObservable()
                 let stored = network.fetchCatalog(id: id, version: version)
                     .flatMap {
                         return self.cache.save(object: $0)
                             .asObservable()
                             .map(to: Catalog.self)
                             .concat(Observable.just($0))
                 }
                 
                 return fetchComponent.concat(stored)
      }
    
    
}

