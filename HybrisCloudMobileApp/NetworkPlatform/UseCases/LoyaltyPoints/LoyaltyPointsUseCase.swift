//
//  LoyaltyPointsUseCase.swift
//  NetworkPlatform
//
//  Created by khalil anqawi on 26/05/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class LoyaltyPointsUseCase<Cache>: Domain.LoyaltyPointsUseCase where Cache: AbstractCache, Cache.T == LoyaltyPointsModel {
   
    
    private let network: LoyaltyPointsNetwork
    private let cache: Cache
    init(network: LoyaltyPointsNetwork , cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    func getLoyaltyPoints() -> Observable<LoyaltyPointsModel> {
        return network.fetchLoyaltyPoints()
    }
    func getAvailableLoyaltyPoints() -> Observable<Double> {
        return network.fetchAvailableLoyaltyPointsModel()
    }
    

}
