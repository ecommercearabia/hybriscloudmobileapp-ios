//
//  DeliveryTypesUseCase.swift
//  NetworkPlatform
//
//  Created by khalil anqawi on 12/11/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class DeliveryTypesUseCase<Cache>: Domain.DeliveryTypesUseCase where Cache: AbstractCache, Cache.T == DeliveryTypesModel {

   
    
    private let network: DeliveryTypesNetwork
    private let cache: Cache
    init(network: DeliveryTypesNetwork , cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    func getDeliveryTypes() -> RxSwift.Observable<Domain.DeliveryTypesModel> {
        return network.fetchDeliveryTypes()
    }
    
    func getDeliveryType() -> RxSwift.Observable<String> {
        return network.fetchDeliveryType()
    }
    
    func setDeliveryType(deliveryType: String) -> RxSwift.Observable<Domain.CartModel> {
        return network.setDeliveryType(deliveryType: deliveryType)
    }

}
