//
//  ComponentUseCase.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 7/27/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class ComponentUseCase<Cache>: Domain.ComponentUseCase where Cache: AbstractCache, Cache.T == Component {
    
    private let network: ComponentNetwork
    private let cache: Cache
    
    init(network: ComponentNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    func component(componentId: String) -> Observable<Component> {
        let fetchComponent = cache.fetch(withID: componentId).asObservable()
        let stored = network.fetchComponent(componentId: componentId)
            .flatMap {
                return self.cache.save(object: $0)
                    .asObservable()
                    .map(to: Component.self)
                    .concat(Observable.just($0))
        }
        
        return fetchComponent.concat(stored)
    }
    
    
}

