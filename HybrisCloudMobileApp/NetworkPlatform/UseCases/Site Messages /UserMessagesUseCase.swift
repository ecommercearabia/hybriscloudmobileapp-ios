//
//  UserMessagesUseCase.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 9/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import Domain
import RxSwift

final class UserMessagesUseCase<Cache>: Domain.UserMessagesUseCase where Cache: AbstractCache, Cache.T == UserMessages {

  
   
    private let network: UserMessagesNetwork
    private let cache: Cache
    
    init(network: UserMessagesNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }

    
    func getUserMessages() -> Observable<UserMessages> {
        let fetchComponent = cache.fetch().asObservable()
        let stored = network.fetchUserMessages().flatMap {
                    return self.cache.save(object: $0)
                        .asObservable()
                        .map(to: UserMessages.self)
                        .concat(Observable.just($0))
                }
                
                return fetchComponent.concat(stored)
      }
    

    
    
    }
    

      

