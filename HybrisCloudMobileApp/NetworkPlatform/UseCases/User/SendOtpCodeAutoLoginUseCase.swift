//
//  SendOtpCodeAutoLoginUseCase.swift
//  NetworkPlatform
//
//  Created by ahmadSaleh on 12/3/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class SendOtpCodeAutoLoginUseCase<Cache>: Domain.SendOtpCodeAutoLoginUseCase where Cache: AbstractCache, Cache.T == SendOtpCode {
    
    

    private let network: UsersNetwork
    private let cache: Cache
    
    init(network: UsersNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
     
    func getHistory(userId: String) -> Observable<StoreCreditHistroy> {
               return network.getHistoryStoreCredit(userId: userId)

    }
  
}


final class StoreCreditAmountUseCase<Cache>: Domain.StoreCreditAmountUseCase where Cache: AbstractCache, Cache.T == StoreCreditAmount {
  


    private let network: UsersNetwork
    private let cache: Cache
    
    init(network: UsersNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
     
    func getAmount(userId: String) -> Observable<StoreCreditAmount> {
        return network.getAmountStoreCredit(userId: userId)

    }
  
  
}
