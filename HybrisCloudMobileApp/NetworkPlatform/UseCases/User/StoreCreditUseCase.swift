//
//  StoreCreditUseCase.swift
//  NetworkPlatform
//
//  Created by walaa omar on 8/19/20.
//  Copyright © 2020 Erabia. All rights reserved.
//userId: String

import Foundation
import Domain
import RxSwift

final class StoreCreditHistroyUseCase<Cache>: Domain.StoreCreditHistroyUseCase where Cache: AbstractCache, Cache.T == StoreCreditHistroy {
    
    

    private let network: UsersNetwork
    private let cache: Cache
    
    init(network: UsersNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
     
    func getHistory(userId: String) -> Observable<StoreCreditHistroy> {
               return network.getHistoryStoreCredit(userId: userId)

    }
  
}


final class StoreCreditAmountUseCase<Cache>: Domain.StoreCreditAmountUseCase where Cache: AbstractCache, Cache.T == StoreCreditAmount {
  


    private let network: UsersNetwork
    private let cache: Cache
    
    init(network: UsersNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
     
    func getAmount(userId: String) -> Observable<StoreCreditAmount> {
        return network.getAmountStoreCredit(userId: userId)

    }
  
  
}

final class SendOtpCodeAutoLoginUseCase<Cache>: Domain.SendOtpCodeAutoLoginUseCase where Cache: AbstractCache, Cache.T == SendOtpCode {
    
    

    private let network: UsersNetwork
    private let cache: Cache
    
    init(network: UsersNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
     
    func sendOtpCode(userId: String) -> Observable<SendOtpCode> {
        return network.sendOtpCode(email: userId)
    }
    

 
}

 
