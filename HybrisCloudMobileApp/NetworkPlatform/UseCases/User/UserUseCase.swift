//
//  RegistersUseCase.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 7/21/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import Domain
import RxSwift

final class UserUseCase<Cache>: Domain.UserUseCase where Cache: AbstractCache, Cache.T == UserData {
    
    
    func setOrUpdateFaceId(signatureId: String, userId: String) -> Observable<Void> {
        return network.setOrUpdateFaceId(signatureId: signatureId, userId: userId)
    }
    
    func loginWithFaceId(signatureId: String, email: String) -> Observable<LoginFaceID> {
        return network.loginWithFaceId(signatureId: signatureId, email: email)
    }
    
    
    func setFirebaseToken( mobileToken : String ) -> Observable<Void>{
        return network.setFirebaseToken( mobileToken: mobileToken)
    }
   
    
   

    private let network: UsersNetwork
    private let cache: Cache
    
    init(network: UsersNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
   func register(user: UserSignup) -> Observable<UserData> {
    return network.register(user: user)
    }

    func login(user: UserLogin) -> Observable<Void> {
          return network.login(user: user)
     }
    
    func logOut() -> Observable<Void> {
          return network.logout()
     }

    
    func changePassword(user: ChangePasswordRequest) -> Observable<Void> {
        return network.changePassword(user: user)
    }
    
    func getCustomerProfile(userId : String) -> Observable<UserData>
    {
        return network.getCustomerProfile(userId: userId)
    }
    
    func updateEmailAddress(newLogin : String, password : String , userId : String) -> Observable<Void>{
          return network.updateEmailAddress(newLogin: newLogin, password: password, userId: userId)
      }
      

    public func updateCustomerProfile(userId : String,user:UserData) -> Observable<Void> {
        
        return network.updateCustomerProfile(userId: userId, user: user)
        
       }
  func forgetPassword(forgetPasswordParam: String) -> Observable<Void> {
    return network.forgetPassword(forgetPasswordParam: forgetPasswordParam)
     }
    
    func verifyOtpCode(code: String, email: String, name: String) -> Observable<Void> {
        return network.verifyOtpCode(code: code, email: email, name: name)
    }
    
     
}
