//
//  UseCaseProvider.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 7/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain

public final class UseCaseProvider: Domain.UseCaseProvider {
 
    
    
    private let networkProvider: NetworkProvider
    
    public init() {
        networkProvider = NetworkProvider()
    }
    
    
    public func makePagesUseCase() -> Domain.PagesUseCase {
        return  PagesUseCase(network: networkProvider.makePagesNetwork(),
                             cache: Cache<Domain.Page>(path: "allPages"))
    }
    
    
    public func makeUserUseCase() -> Domain.UserUseCase {
        return UserUseCase(network: networkProvider.makeUsersNetwork(),
                           cache: Cache<Domain.UserData>(path: "user"))
    }
    
    public func makeStoreCreditHistroyUseCase() -> Domain.StoreCreditHistroyUseCase {
        return StoreCreditHistroyUseCase(network: networkProvider.makeUsersNetwork(),
                                         cache: Cache<Domain.StoreCreditHistroy>(path: "storeCreditHistroy"))
    }
    
    public func makeStoreCreditAmountUseCase() -> Domain.StoreCreditAmountUseCase {
        return StoreCreditAmountUseCase(network: networkProvider.makeUsersNetwork(),
                                        cache: Cache<Domain.StoreCreditAmount>(path: "storeCreditAmount"))
    }
    
    public func makeTitleUseCase() -> Domain.MiscsUseCase {
        return MiscsUseCase(network: networkProvider.makeTitleNetwork(),cache: Cache<Domain.Titles>(path: "miscs"))
        
    }
    
    public func makeComponentUseCase() -> Domain.ComponentUseCase {
        return ComponentUseCase(network: networkProvider.makeCompnentNetwork(),cache: Cache<Domain.Component>(path: "miscs"))
        
    }
    
    
    public func makeCatalogUseCase() -> Domain.CatalogUseCase {
        return CatalogNetworkUseCase(network: networkProvider.makecatalogNetwork(),cache: Cache<Domain.Catalog>(path: "catalog"))
        
    }
    
    public func makeAddressesUseCase() -> Domain.AddressesUseCase {
        return  AddressUseCase(network: networkProvider.makeAddressesNetwork(),
                               cache: Cache<Domain.AddressesModel>(path: "addresses"))
    }
    
    public func makeAddAddressesUseCase() -> Domain.AddAddressesUseCase {
        return  AddAddressUseCase(network: networkProvider.makeAddressesNetwork(),
                                  cache: Cache<Domain.Address>(path: "address"))
    }
    
    
    
    public func makeCartUseCase() -> Domain.CartUseCase {
        return CartUseCase(network: networkProvider.makeCartNetwork(), cache: Cache<Domain.CartModel>(path: "cart"))
        
    }
    
    public func makeCartCreatUseCase() -> Domain.CartUseCase {
        return CartUseCase(network: networkProvider.makeCreatCartNetwork(), cache: Cache<Domain.CartModel>(path: "creatCart"))
        
    }
    
    
    public func makeBestDealProductUseCase() -> Domain.BestDealProductUseCase {
        return BestDealProductUseCase(network: networkProvider.makeBestDealProductNetwork(), cache: Cache<Domain.ProductDetails>(path: "bestDealProduct"))
    }
    
    
    public func makeCountryUseCase() -> Domain.CountryUseCase {
        
        return CountriesUseCase(network: networkProvider.makeCountryNetwork(), cache: Cache<Domain.CountriesModel>(path: "country"))
    }
    
    public func makeProductListingUseCase() -> Domain.ProductUseCase {
        
        return ProductUseCase(network: networkProvider.makeProductNetwork(), cache: Cache<Domain.ProductSearch>(path: "ProductSearch"))
    }
    
    
    public func makebestSellerUserCase() -> Domain.BestSellerProductsUseCase {
        
        return BestSellerProductsUseCase(network: networkProvider.makeBestSellerProductNetwork(), cache: Cache<Domain.BestSellerProducts>(path: "bestSellerProduct"))
    }
    
    public func makeAddToCartUseCase() -> Domain.AddtoCartUseCase {
        
        return AddtoCartUseCase(network: networkProvider.makeAddToCartNetwork(), cache: Cache<Domain.AddedToCartModel>(path: "addToCart"))
    }
    
    public func makeProductDetailsUseCase() -> Domain.ProductDetailsUseCase {
        return ProductDetailsUseCase(network: networkProvider.makeProductDetailsNetwork(), cache: Cache<Domain.ProductDetails>(path: "ProductDetails"))
    }
    
    public func makeProductReviewsUseCase() -> Domain.ProductReviewsUseCase {
        return ProductReviewsUseCase(network: networkProvider.makeProductDetailsNetwork(), cache: Cache<Domain.Reviews>(path: "ProductReviews"))
    }
    public func makeProductReviewUseCase() -> Domain.ProductReviewUseCase {
        return ProductReviewUseCase(network: networkProvider.makeProductDetailsNetwork(), cache: Cache<Domain.Review>(path: "ProductReview"))
    }
    public func makeChickOutUseCase() -> Domain.chickOutUseCase {
        return ChickOutUseCase(network: networkProvider.makeChickOutNetwork(), cache: Cache<Domain.TimeSlotsModel>(path: "chickOut"))
    }
    
    public func makeSendOtpUseCase() -> Domain.SendOTPUseCase {
        return SendOTPUseCase(network: networkProvider.makeSendOtpNetwork(), cache: Cache<Domain.SendOTP>(path: "sendOtp"))
    }
    public func makeVerifyOtpUseCase() -> Domain.VerifyOTPUseCase {
        return VerifyOtpUseCase(network: networkProvider.makeVerifyOtpNetwork(), cache: Cache<Domain.VerifyOTP>(path: "VerifyOtp"))
    }
    
    public func makeDeliveryUseCase() -> Domain.setDeliveryModeUseCase {
        return DeliveryModeUseCase(network: networkProvider.makeDeliveryNetwork(), cache: Cache<Domain.DeliveryModeModel>(path: "DeliveryMode"))
    }
    
    public func makeMyOrdersUseCase() -> Domain.MyOrdersUseCase{
        return MyOrdersUseCase(network: networkProvider.makeMyOrdersNetwork(), cache: Cache<Domain.MyOrders>(path:"MyOrders"))
    }
    
    public func makeOrdersDetailsUseCase() -> Domain.OrdersDetailsUseCase{
        return OrdersDetailsUseCase(network: networkProvider.makeOrdersDetailsNetwork(),cache:Cache<Domain.ordersDetailsMyOrdersDetails>(path: "OrdersDetails"))
    }
    
    public func makePaymentUseCase() -> Domain.paymentModeUseCase{
        return PaymentModeUseCase(network: networkProvider.makePaymentNetwork(),cache:Cache<Domain.PaymentModesModel>(path: "PaymentModes"))
    }
    
    public func makeCitiesUseCase() -> Domain.CitiesUseCase{
        return CitiesUseCase(network: networkProvider.makeCitiesNetwork(),cache:Cache<Domain.CitiesModel>(path: "Cities"))
    }
    
    public func makePaymentDetailsUseCase() -> Domain.PaymentDetailsUseCase{
        return PaymentDetailsUseCase(network: networkProvider.makePaymentDetailsNetwork(), cache: Cache<Domain.PaymentDetailsResponse>(path: "PaymentDetails"))
    }
    
    public func makePlaceOrderUseCase() -> Domain.PlaceOrderUserCase{
        return PlaceOrderUseCase(network: networkProvider.makePlaceOrderNetwork(), cache: Cache<Domain.PalceOrderResponse>(path: "PlaceOrder"))
    }
    
    public func makePaymentInfoUseCase() -> Domain.PaymentInfoUseCase{
        return PaymentInfoUseCase(network: networkProvider.makePaymentInfoNetwork(), cache: Cache<Domain.PaymentInfo>(path: "PaymentInfo"))
    }
    
    public func makeCartQuantityUserCase() -> Domain.CartQuantityUserCase{
        return CartQuantityUserCase(network: networkProvider.makeCartQuantityNetwork(),cache:  Cache<Domain.CartQuantityUpdate>(path: "quantity"))
    }
    
    public func makeCartsUseCase() -> Domain.CartsUseCase{
        
        return CartsUseCase.init(network: networkProvider.makeCartsNetwork(), cache: Cache<Domain.CartsModel>(path : "Carts"))
    }
    
    public func makeValidationCartUseCase() -> Domain.ValidationCartUseCase{
        
        return ValidationCartUseCase.init(network: networkProvider.makeValidationCartUseCase(), cache: Cache<Domain.ValidationCartErrors>(path : "ValidationCartErrors"))
    }
    
    public func makeWishListUseCase() -> Domain.WishListUseCase{
        
        return WishListUseCase.init(network: networkProvider.makeWishListUseCase(), cache: Cache<Domain.WishList>(path: "WishList"))
    }
    
    public func makeForgetPassword()  -> Domain.UserUseCase {
        return UserUseCase(network: networkProvider.makeUsersNetwork(),
                           cache: Cache<Domain.UserData>(path: "forgetPassword"))
        
    }
    
    public func makeStroyCreditCartUseCase() -> Domain.StoreCreditCartUseCase{
        return StoreCreditCartUseCase(network: networkProvider.makeStroyCreditCartUseCase(), cache: Cache<Domain.StoreCreditModes>(path: "StroyCredit"))
    }
    
    public func makeUserMesseges() -> Domain.UserMessagesUseCase {
        
        return UserMessagesUseCase.init(network: networkProvider.makeUserMessegesUseCase(), cache: Cache<Domain.UserMessages>(path: "UserMesseges"))
        
        
    }
    
    public func makeBankOffersCartUseCase() -> Domain.BankOffersCartUseCase{
        
        return BankOffersCartUseCase.init(network: networkProvider.makeBankOffersUseCase(), cache: Cache<Domain.BankOffers>(path : "BankOffers"))
    }
    
    public func makeSendOtpCodeAutoLoginUseCase() -> Domain.SendOtpCodeAutoLoginUseCase{
        
        return SendOtpCodeAutoLoginUseCase.init(network: networkProvider.makeUsersNetwork(), cache: Cache<Domain.SendOtpCode>(path : "SendOtpCode"))
    }
    
    public func makeSuggestionUseCase() -> Domain.SuggestionUseCase{
        
        return SuggestionUseCase.init(network: networkProvider.makeSuggestionUseCase(), cache: Cache<Domain.SuggestionModel>(path : "Suggestion"))
    }
    
    public func makeReferralCodeUseCase() -> Domain.ReferralCodeUseCase{
        return ReferralCodeUseCase.init(network: networkProvider.makeReferralCodeUseCase(), cache: Cache<Domain.ReferralCode>(path : "referralCode"))
    }

    public func makeApplePayUseCase() -> Domain.applePayUseCase{
        return ApplePayUseCase.init(network: networkProvider.makeApplePayUseCase(), cache: Cache<Domain.ApplePayModel>(path : "applePay"))
    }
    
    
    public func makeConsentsList() -> Domain.ConsentsListUseCase {
        return ConsentsListUseCase.init(network: networkProvider.makeConsentsListUseCase(), cache: Cache<Domain.Consents>(path: "Consents"))
    }
    
    public func makeSavedCards() -> Domain.SavedCardsUseCase {
        return SavedCardsUseCase.init(network: networkProvider.makeSavedCardsUseCase(), cache: Cache<Domain.SavedCardsModel>(path: "SavedCardsModel"))
    }
    
    public func makeNationalities() -> Domain.NationalitiesUseCase {
        return NationalitiesUseCase.init(network: networkProvider.makeNationalitiesUseCase(), cache: Cache<Domain.NationalitiesModel>(path: "NationalitiesModel"))
    }
    
    public func makeConfig() -> Domain.ConfigUseCase {
        return ConfigUseCase.init(network: networkProvider.makeConfigUseCase(), cache: Cache<Domain.ConfigModel>(path: "ConfigModel"))
    }
    
    public func makeShipmentTypes() -> Domain.ShipmentTypesUseCase {
        return ShipmentTypesUseCase.init(network: networkProvider.makeShipmentTypesUseCase(), cache: Cache<Domain.ShipmentTypesModel>(path: "ShipmentTypesModel"))
    }
    
    public func makeshipmentTypes() -> Domain.ShipmentTypesUseCase {
        return ShipmentTypesUseCase(network: networkProvider.makeShipmentTypesUseCase(),
                           cache: Cache<Domain.ShipmentTypesModel>(path: "ShipmentTypesModel"))
    }
    
    public func makeLoyaltyPoints() -> Domain.LoyaltyPointsUseCase {
        return LoyaltyPointsUseCase(network: networkProvider.makeLoyaltyPointsUseCase(),
                           cache: Cache<Domain.LoyaltyPointsModel>(path: "LoyaltyPointsModel"))
    }
    
    public func makeDeliveryTypes() -> Domain.DeliveryTypesUseCase {
        return DeliveryTypesUseCase(network: networkProvider.makeDeliveryTypesUseCase(),
                           cache: Cache<Domain.DeliveryTypesModel>(path: "DeliveryTypesModel"))
    }
}
