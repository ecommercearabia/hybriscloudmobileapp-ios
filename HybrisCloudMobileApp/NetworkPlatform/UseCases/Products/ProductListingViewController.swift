//
//  ProductListingViewController.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/3/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import Domain
import Kingfisher
import FSPagerView
import Firebase
class ProductListingViewController: UIViewController, GetRelevanceValueDelegate {
    func getRelevanceValue(relevance: String) {
        self.relevancesSubject.onNext(relevance)
    }

    @IBOutlet weak var emptyView: UIView!
    
    @IBOutlet weak var sortButton: UIBarButtonItem!
    @IBOutlet weak var sortPickerView: UIPickerView!
    @IBOutlet weak var selectSortTypeButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var productListingCollectionView: UICollectionView!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var selectView: UIView!
    @IBOutlet weak var organicSwitch: UISwitch!
    @IBOutlet weak var veganSwitch: UISwitch!
    @IBOutlet weak var glutenSwitch: UISwitch!
    @IBOutlet weak var selectedMainCatLbl: UILabel!
    @IBOutlet weak var cancelFilter: UIBarButtonItem!
    
    @IBOutlet weak var switchsHight: NSLayoutConstraint!
    @IBOutlet weak var switchsView: UIView!
    
    
    @IBOutlet weak var veganStack: UIStackView!
    @IBOutlet weak var glutenFreeStack: UIStackView!
    @IBOutlet weak var organic: UIStackView!
    
    @IBOutlet weak var topViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var topVIew: UIView!
    
    @IBOutlet weak var veganLbl: UILabel!
    @IBOutlet weak var glutenLbl: UILabel!
    @IBOutlet weak var organicLbl: UILabel!
    @IBOutlet weak var searchBarButton: UIBarButtonItem!
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            
            self.pagerView.register(UINib(nibName: "CategoriesTypeCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesTypeCell")
            
            self.pagerView.itemSize = FSPagerView.automaticSize
            self.pagerView.delegate = self
            self.pagerView.dataSource = self
            self.pagerView.transformer = FSPagerViewTransformer(type: .linear)
            self.pagerView.interitemSpacing = 40
            
            pagerView.itemSize = CGSize(width: 65 , height: 65)
            pagerView.isInfinite = true
            pagerView.transformer = NoVerticalScalePagerViewTransformer(type: .linear)
        }
    }
    
    var fromBunners = false
    var fromPustNotification = false
    var numberItemsInRow : CGFloat = 1.6
    let sectionInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    let disposeBag = DisposeBag()
    var productViewModel : ProductListingViewModel!
    var categoryviewModel : CategoryViewModel!
    var categorys : [Subcategory]?
    var categoryId : String? = ""
    let manuleGetCategoryTrigger = BehaviorSubject<Void>(value: ())

    var selectedCategorySubjec = BehaviorSubject<String>(value: "")
    var selectedSortTypeSubjec = BehaviorSubject<searchSort>(value: searchSort.init(code: nil, name: nil, selected: nil))
    var selectedSortType : searchSort? = nil
    var sortType = [searchSort]()
    var relevancesSubject = BehaviorSubject<String>(value: "")
    
    var paginationPageNumberBehaviorSubject = BehaviorSubject<Int>(value: 0)
    var bestDealProductSectionBehaviorSubject = BehaviorSubject<[BestDealProductSection]>(value: [])
    
    var paginationTotalResult : Int = 0
    var productListingCount : Int = 0
    
    var manualGetProduct = BehaviorSubject<Void>(value: ())
    var pinchGesture = UIPinchGestureRecognizer()
    var categoryIdBehaviorSubject = BehaviorSubject<String>(value: "")

    var catId : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        organicSwitch.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
        veganSwitch.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
        glutenSwitch.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
        
        self.veganStack.isHidden = true
        self.glutenFreeStack.isHidden = true
        self.organic.isHidden = true
        
        if self.fromBunners {
            self.topViewConstraint.constant = 0
            self.topVIew.isHidden = true
            self.view.layoutIfNeeded()

 
        }
        
        addNavBarImage()
        sortView.isHidden = true
        sortView.layer.cornerRadius = 25
        sortView.layer.maskedCorners = [.layerMinXMinYCorner , .layerMaxXMinYCorner]
        let backBarButtonItem = UIBarButtonItem(title: "Close".localized(), style: .plain, target: nil, action: nil)
        backBarButtonItem.tintColor = #colorLiteral(red: 0.3843137255, green: 0.431372549, blue: 0.4156862745, alpha: 1)
        
        
        navigationController?.navigationBar.backIndicatorImage = UIImage()
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage()
        navigationItem.backBarButtonItem = backBarButtonItem
        
        productListingCollectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionViewCell")
        
        
        self.productListingCollectionView.allowsSelection = true
        preparation()
        
        self.handelPinchGesture()
        
        if self.fromPustNotification {
            let backButton = UIBarButtonItem(title: "Back".localized(), style: .plain, target: self, action: #selector(self.backAction))
            backButton.setTitleTextAttributes([.foregroundColor: UIColor.black], for: .normal)

                self.navigationItem.leftBarButtonItem = backButton
        }
    }
    
    @objc func backAction() {

       
        self.goToHome()
    }
    
    func handelPinchGesture () {
        self.productListingCollectionView.isUserInteractionEnabled = true
        self.productListingCollectionView.isMultipleTouchEnabled = true
        
        self.pinchGesture = UIPinchGestureRecognizer(target: self, action:#selector(pinchRecognized(_:)))
        self.productListingCollectionView.addGestureRecognizer(self.pinchGesture)
        
    }
    
    var itemNeedShow = 2
    var cellSizeType = CellSizeType.larg
    
    @objc func pinchRecognized(_ pinch: UIPinchGestureRecognizer) {
        
        switch pinch.state {
        case .possible:
            break
            
        case .began:
            
            break;
            
        case .changed:
            print(pinch.scale)
            //
            if pinch.scale >=  1.0 && itemNeedShow != 1{
                itemNeedShow =  1
                changeCollectionViewItemLayout(newItemSize: itemNeedShow)
            }
            else if oldItemIsShow >= 4 {
                break
            }
            else if pinch.scale < 1.0 && pinch.scale >= 0.75 &&  itemNeedShow != oldItemIsShow + 1 {
                
                itemNeedShow = oldItemIsShow + 1
                changeCollectionViewItemLayout(newItemSize: itemNeedShow)
            }
            else if pinch.scale < 0.75 && pinch.scale >= 0.50  && itemNeedShow != oldItemIsShow + 2 {
                itemNeedShow = oldItemIsShow + 2
                changeCollectionViewItemLayout(newItemSize: itemNeedShow)
            }
            else if pinch.scale < 0.50 &&  itemNeedShow != oldItemIsShow + 3 {
                itemNeedShow = oldItemIsShow + 3
                changeCollectionViewItemLayout(newItemSize: itemNeedShow)
            }
            else {
                break
            }
            
            
            
            
        case .ended:
            oldItemIsShow = itemNeedShow
            
            break
            
        case .cancelled:
            break
        case .failed:
            break
        @unknown default:
            break
        }
        
    }
    
    var oldItemIsShow = 1
    func changeCollectionViewItemLayout(newItemSize : Int){
        
        
        UIView.animate(withDuration: 0, delay: 0, options: .transitionCrossDissolve, animations: {
            self.productListingCollectionView.alpha = 0
            self.productListingCollectionView.collectionViewLayout.invalidateLayout()
            
        }) { _ in
            self.productListingCollectionView.alpha = 1
            self.productListingCollectionView.collectionViewLayout.invalidateLayout()
        }
        
        
    }
    
    func preparation() {
        
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        productViewModel = ProductListingViewModel.init(useCase: networkUseCaseProvider.makeProductListingUseCase(),getCategoryUseCase: networkUseCaseProvider.makeCatalogUseCase())
        
        //        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
        //            .mapToVoid()
        //            .asDriverOnErrorJustComplete()
        
        
        let switchTrigger = self.organicSwitch
            .rx.value.asDriver()
        
        let veganTrigger = self.veganSwitch
            .rx.value.asDriver()
        
        
        let glutenTrigger = self.glutenSwitch
            .rx.value.asDriver()
        
        let input = ProductListingViewModel.Input(
            selectCategoryTrigger: self.selectedCategorySubjec.asDriverOnErrorJustComplete(),
            trigger: manualGetProduct.asDriverOnErrorJustComplete(),
            gerCatigoryTrigger: manuleGetCategoryTrigger.asDriverOnErrorJustComplete(),
            switchTrigger: switchTrigger, veganTrigger: veganTrigger, glutenTrigger: glutenTrigger,
            filterTrigger: self.relevancesSubject.asDriverOnErrorJustComplete(),
            pageNumber: paginationPageNumberBehaviorSubject.asDriverOnErrorJustComplete(),
            sort: selectedSortTypeSubjec.asDriverOnErrorJustComplete())
        let output = productViewModel.transform(input : input)
        
        
        output.category.asObservable().do(afterNext: { Catalog in
            self.categorys = Catalog.categories?.filter({ (Category) -> Bool in
                return Category.id == "1"
            }).first?.subcategories
                       
          //  self.selectedMainCatLbl.text = self.categorys?.first?.name ?? ""
            if self.catId == nil || self.catId == "" {
                self.catId = try? self.categoryIdBehaviorSubject.value()
            }
           
            let indexSelected :Int? = nil
//            if(self.catId != "" && self.catId != nil) {
//
//                self.categorys?.enumerated().forEach( { (index,subcategory) in
//                    if(subcategory.id == self.catId){
//                        self.productViewModel.productSubject.onNext([CategorySection(header: "", items: subcategory.subcategories ?? [])])
//                        self.selectedCategorySubjec.onNext(subcategory.id ?? "")
////                        indexSelected = index
//
//                }
//                })
//
//                if(indexSelected == nil ){
//
//                    self.selectedCategorySubjec.onNext(self.catId ?? "")
//                }
//            }else {
//                self.selectedMainCatLbl.text = self.categorys?.first?.name ?? ""
//                self.productViewModel.productSubject.onNext([CategorySection(header: "", items: self.categorys?.first?.subcategories ?? [])])
//                guard let defaultSelectedCategory = self.categorys?.first else {return}
//                self.selectedCategorySubjec.onNext(defaultSelectedCategory.id ?? "")
//            }
            self.manualGetProduct.onNext(())
//            self.pagerView.selectItem(at: indexSelected ?? 0, animated: true)
          
            self.pagerView.reloadData()
          
        }).subscribe(onNext: { (Catalog) in
            

            if self.categorys?.contains(where: { sub in
                sub.category?.code == "NEW"
            }) ?? false  {
                let index =  self.categorys?.firstIndex(where: { cat in
                    cat.category?.code == "NEW"
                })
                self.productViewModel.productSubject.onNext([CategorySection(header: "", items: self.categorys?[index ?? 0].subcategories ?? [])])
                guard let defaultSelectedCategory = self.categorys?[index ?? 0] else {return}
                self.selectedCategorySubjec.onNext(defaultSelectedCategory.id ?? "")
                self.pagerView.scrollToItem(at: index ?? 0, animated: true)
                UIView.transition(with: self.selectedMainCatLbl,
                                  duration: 0.25,
                                  options: .transitionCrossDissolve,
                                  animations: { [weak self] in
                    self?.selectedMainCatLbl.text = self?.categorys?[index ?? 0].name ?? ""
                }, completion: nil)
            }
      
          
        }).disposed(by: disposeBag)
        self.relevancesSubject.asObserver().map { (text) -> Bool in
            self.paginationTotalResult  = 0
            self.productListingCount  = 0
            self.paginationPageNumberBehaviorSubject.onNext(0)
            self.bestDealProductSectionBehaviorSubject.onNext([BestDealProductSection(header: "", items: [])])
            return !text.isEmpty
        }.bind(to: self.cancelFilter.rx.isEnabled).disposed(by: disposeBag)
        
 
 
        output.error.asObservable().subscribe (onNext: { (Error) in
            let error = Error as? ErrorResponse
            self.errorLoaf(message: error?.errors?.first?.message ?? "")
  
        }).disposed(by: disposeBag)


        
        
        self.cancelFilter.rx.tap.subscribe(onNext: { (void) in
            self.relevancesSubject.onNext("")
        }).disposed(by: disposeBag)
        
        
        //MARK: Product Output
        self.productListingCollectionView.rx.setDelegate(self).disposed(by: disposeBag)
        
        
        output.product.asObservable().subscribe(onNext: { (product) in
            
            guard let listCol = try? self.bestDealProductSectionBehaviorSubject.value() else {return}
            var items = listCol.first?.items
//            items?.removeAll(where: { (productDetails) -> Bool in
//                return product.products?.contains(productDetails) ?? false
//            })
            if items != product.products ?? [] {
                items?.append(contentsOf: product.products ?? [])
           }
           
            self.productListingCount = items?.count ?? product.products?.count ?? 0
            
            self.bestDealProductSectionBehaviorSubject.onNext([BestDealProductSection(header: "", items: items ?? product.products ?? [])])
            
          
            Analytics.logEvent(AnalyticsEventViewItemList, parameters: [
                AnalyticsParameterItemListID: product.categoryCode ?? "",
                AnalyticsParameterItemListName: self.selectedMainCatLbl.text ?? ""
              ])
            
            if UserDefaults.standard.bool(forKey: "isRunCategory") != true {
                UserDefaults.standard.set(true, forKey: "isRunCategory")
                let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
                    self.changeCollectionViewItemLayout(newItemSize: self.itemNeedShow)
                    if self.itemNeedShow == 4 {
                        self.itemNeedShow = 0
                    }
                    self.itemNeedShow = self.itemNeedShow + 1
                }
                
                let popOverVC = self.storyboard?.instantiateViewController(withIdentifier: "VCpopUp") as! VCpopUp
                popOverVC.modalPresentationStyle = .overFullScreen
                popOverVC.closePopUp.subscribe(onNext: { (void) in
                    timer.invalidate()
                    self.itemNeedShow = 1
                    self.changeCollectionViewItemLayout(newItemSize: self.itemNeedShow)
                }).disposed(by: self.disposeBag)

                self.present(popOverVC,animated: true)
                
                
                
            }
            
            
           
            
        }).disposed(by: disposeBag)
        
        self.bestDealProductSectionBehaviorSubject.bind(to: productListingCollectionView.rx.items(dataSource: self.productViewModel.dataSource())).disposed(by: disposeBag)
        self.bestDealProductSectionBehaviorSubject.subscribe {[unowned self] sections in
            if let index = try? self.paginationPageNumberBehaviorSubject.value() {
                if index == 0{
            self.productListingCollectionView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
            }
            }
        } onError: { error in
            
        } onCompleted: {
            
        } onDisposed: {
            
        }.disposed(by: disposeBag)

        self.productListingCollectionView.rx.modelSelected(BestDealProductSection.Item.self).subscribe(onNext: {(item) in print("model Selected")
            
            Analytics.logEvent(AnalyticsEventSelectItem, parameters: [
                AnalyticsParameterItemListID: item.code ?? "" ,
                AnalyticsParameterItemListName: item.name ?? ""
              ])
            self.performSegue(withIdentifier: "toProductDetails", sender: item)
        }).disposed(by: disposeBag)
        
        output.product.map { (product) -> Bool in
            
            self.paginationTotalResult = product.pagination?.totalResults ?? 0
            
            return  self.paginationTotalResult != 0
        }.asObservable().bind(to:  self.emptyView.rx.isHidden).disposed(by: disposeBag)
        
        
        output.product.map { (product) -> Bool in
            product.pagination?.totalResults ?? 0 == 0
        }.asObservable().bind(to:  self.productListingCollectionView.rx.isHidden).disposed(by: disposeBag)
        
        
        //MARK: Product sort
        
        output.product.asObservable().subscribe(onNext: { (ProductSearch) in
            self.productViewModel.relevancesFacets.onNext(ProductSearch.facets ?? [])
            self.sortType = []
            
            
            self.veganStack.isHidden = true
            self.glutenFreeStack.isHidden = true
            self.organic.isHidden = true
            
            ProductSearch.facets?.forEach({ (searchFacet) in
                
                let array = searchFacet.values?.filter({ (searchValue) -> Bool in
                    return searchValue.name == "true"
                })
                
                if (searchFacet.name == "Vegan"  || searchFacet.name == "نباتي" ) && (array?.count ?? 0) > 0 {
                    self.veganStack.isHidden = false
                }
                if (searchFacet.name == "Organic" || searchFacet.name == "عضوي") && (array?.count ?? 0) > 0{
                    self.organic.isHidden = false
                }
                if (searchFacet.name == "Gluten Free" || searchFacet.name == "خالي من الغلوتين")  && (array?.count ?? 0) > 0 {
                    self.glutenFreeStack.isHidden = false
                }
                
                
            })
            
            if self.veganStack.isHidden && self.glutenFreeStack.isHidden && self.organic.isHidden {
               // self.switchsHight.constant = 0.0
                
                self.veganStack.isHidden = false
                self.glutenFreeStack.isHidden = false
                self.organic.isHidden = false
                
                self.organicSwitch.isUserInteractionEnabled = false
                self.veganSwitch.isUserInteractionEnabled = false
                self.glutenSwitch.isUserInteractionEnabled = false
                self.veganLbl.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                self.glutenLbl.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                self.organicLbl.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                
            }
            else {
                self.organicSwitch.isUserInteractionEnabled = true
                self.veganSwitch.isUserInteractionEnabled = true
                self.glutenSwitch.isUserInteractionEnabled = true
                self.veganLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                self.glutenLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                self.organicLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                
               // self.switchsHight.constant = 35.0
            }
            self.switchsView.layoutIfNeeded()
            
            
            ProductSearch.sorts.forEach({ (searchSort) in
                self.sortType.append(searchSort)
            })
            
      
            
        }).disposed(by: disposeBag)
        self.productViewModel.toLoginSubject.subscribe(onNext: { (bool) in
            self.performSegue(withIdentifier: "toLogin", sender: nil)
            
        }).disposed(by: disposeBag)
        //MARK: Sort Button
        sortButton.rx.tap.subscribe(onNext: { (Void) in
            self.sortView.alpha = 0
            self.sortView.isHidden = false
            self.selectedSortType = searchSort.init(code: self.sortType.first?.code, name: self.sortType.first?.name, selected: self.sortType.first?.selected)
            self.sortPickerView.delegate = nil
            self.sortPickerView.dataSource = nil
            
            UIView.transition(with: self.sortView
                , duration:0.5, options: .transitionCrossDissolve, animations: {
                    self.sortView.alpha = 1
                    self.sortType.forEach { (typeName) in
                        
                    }
                    Observable.just(self.sortType)
                        .bind(to: self.sortPickerView.rx.itemTitles) { _, item in
                            return item.name
                    }
                    .disposed(by: self.disposeBag)
            })
            
            
        }).disposed(by: disposeBag)
        
        self.sortPickerView.rx.modelSelected(searchSort.self)
            .subscribe(onNext: { models in
                self.selectedSortType = models.first ?? searchSort.init(code: self.sortType.first?.code, name: self.sortType.first?.name, selected: self.sortType.first?.selected)
                
            }).disposed(by: self.disposeBag)
        
        //MARK: Selected sort Type Button
        
        Observable.merge( organicSwitch.rx.isOn.mapToVoid().skip(1),veganSwitch.rx.isOn.mapToVoid().skip(1) ,glutenSwitch.rx.isOn.mapToVoid().skip(1), selectSortTypeButton.rx.tap.mapToVoid()).subscribe(onNext: { (Void) in
            
            UIView.transition(with: self.sortView,
                              duration: 0.5,
                              options: .transitionCurlDown,
                              animations: { [weak self] in
                                self?.sortView.isHidden = true
                                self?.sortPickerView.delegate = nil
                                self?.sortPickerView.dataSource = nil
                }, completion: nil)
            
            self.paginationTotalResult  = 0
            self.productListingCount  = 0
            self.paginationPageNumberBehaviorSubject.onNext(0)
            self.bestDealProductSectionBehaviorSubject.onNext([BestDealProductSection(header: "", items: [])])
            self.selectedSortTypeSubjec.onNext((self.selectedSortType) ?? searchSort.init(code: nil, name: nil, selected: nil))
            
            
            
        }).disposed(by: disposeBag)
        
        //MARK: cancel Button
        cancelButton.rx.tap.subscribe(onNext: { (Void) in
            UIView.transition(with: self.sortView,
                              duration: 0.5,
                              options: .transitionCurlDown,
                              animations: { [weak self] in
                                self?.sortView.isHidden = true
                                self?.sortPickerView.delegate = nil
                                self?.sortPickerView.dataSource = nil
                }, completion: nil)
        }).disposed(by: disposeBag)
        
        //MARK: Search Button
        self.searchBarButton.rx.tap.subscribe(onNext: {
            self.performSegue(withIdentifier: "toSearch", sender: nil)
        }).disposed(by: disposeBag)
        
    }
    
    func addNavBarImage () {
        let navController = navigationController!
        let logoImage = #imageLiteral(resourceName: "logo")
        let logoImageView = UIImageView()
        
        let bannerWidth = navController.navigationBar.frame.size.width / 2
        let bannerHeight = navController.navigationBar.frame.size.height / 2
        let bannerX = bannerWidth / 2 - logoImage.size.width / 2
        let bannerY = bannerHeight / 2 - logoImage.size.height / 2
        
        logoImageView.frame = CGRect(x:bannerX , y: bannerY, width: bannerWidth, height: bannerHeight)
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.image = logoImage
        navigationItem.titleView = logoImageView
        
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toProductDetails"{
            if let vc = segue.destination as? ProductDetailsViewController{
                guard let pd = sender as? ProductDetails else {
                    return
                }
                vc.productDetailsBehaviorSubject = BehaviorSubject<String>(value: pd.code ?? "")
            }
        } else if segue.identifier == "toSearch" {
            if let vc = segue.destination as? SearchViewController {
//                let categoryId = try? self.selectedCategorySubjec.value()
//                vc.categoryIdSubject.onNext(categoryId ?? "")
            }
        } else { //toFilter
            if let vc = segue.destination as? ListingFitersViewController{
                vc.categoryIdSubject = self.selectedCategorySubjec
                vc.delegate = self
                self.paginationTotalResult  = 0
                self.productListingCount  = 0
                self.paginationPageNumberBehaviorSubject.onNext(0)
//                self.bestDealProductSectionBehaviorSubject.onNext([BestDealProductSection(header: "", items: [])])
            }
            
        }
    }
    
    
}


extension ProductListingViewController : FSPagerViewDelegate , FSPagerViewDataSource , UICollectionViewDelegateFlowLayout{
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.categorys?.count ?? 0
    }
    
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        guard let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "CategoriesTypeCell", at: index) as? CategoriesTypeCell else{ return FSPagerViewCell()}
        cell.contentView.layer.shadowColor = UIColor.clear.cgColor
        cell.img?.kf.setImage(with: URL(string: "\(Environment.shared.apiEndpoint?.rawValue ?? "")\(self.categorys?[index].category?.image?.url ?? "")"))

        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        self.pagerView.scrollToItem(at: index, animated: true)
        UIView.transition(with: self.selectedMainCatLbl,
                          duration: 0.25,
                          options: .transitionCrossDissolve,
                          animations: { [weak self] in
                            self?.selectedMainCatLbl.text = self?.categorys?[index].name ?? ""
            }, completion: nil)
        
        
        guard let defaultSelectedCategory = self.categorys?[index] else  {return}
        
        self.organicSwitch.setOn(false, animated: true)
        self.veganSwitch.setOn(false, animated: true)
        self.glutenSwitch.setOn(false, animated: true)
        
        self.organicSwitch.sendActions(for: .valueChanged)
        self.veganSwitch.sendActions(for: .valueChanged)
        self.glutenSwitch.sendActions(for: .valueChanged)
        
        
        self.paginationTotalResult  = 0
        self.productListingCount  = 0
        self.paginationPageNumberBehaviorSubject.onNext(0)
        self.bestDealProductSectionBehaviorSubject.onNext([BestDealProductSection(header: "", items: [])])
        
        self.relevancesSubject.onNext("")
        self.selectedCategorySubjec.onNext(defaultSelectedCategory.id ?? "")
        
//        self.productListingCollectionView?.scrollToItem(at: IndexPath(row: 0, section: 0),
//                                          at: .top,
//                                    animated: true)
        
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        
        UIView.transition(with: self.selectedMainCatLbl,
                          duration: 0.25,
                          options: .transitionCrossDissolve,
                          animations: { [weak self] in
                            self?.selectedMainCatLbl.text = self?.categorys?[targetIndex].name ?? ""
            }, completion: nil)
        
        guard let defaultSelectedCategory = self.categorys?[targetIndex] else  {return}
        
        self.organicSwitch.setOn(false, animated: true)
        self.veganSwitch.setOn(false, animated: true)
        self.glutenSwitch.setOn(false, animated: true)
        
        self.organicSwitch.sendActions(for: .valueChanged)
        self.veganSwitch.sendActions(for: .valueChanged)
        self.glutenSwitch.sendActions(for: .valueChanged)
        
        self.paginationTotalResult  = 0
        self.productListingCount  = 0
        self.paginationPageNumberBehaviorSubject.onNext(0)
        
        self.bestDealProductSectionBehaviorSubject.onNext([BestDealProductSection(header: "", items:  [])])
      //  self.productListingCollectionView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: false)
        self.relevancesSubject.onNext("")
        
        self.selectedCategorySubjec.onNext(defaultSelectedCategory.id ?? "")
        
//        self.productListingCollectionView?.scrollToItem(at: IndexPath(row: 0, section: 0),
//                                          at: .top,
//                                    animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = Double(UIScreen.main.bounds.width) - Double((self.itemNeedShow) * 20)
        let height = 400.0
        
        switch self.itemNeedShow {
        case 1:
            return CGSize(width: Double(width - 100), height: height)
        case 2:
            return CGSize(width: Double(width / 2), height: Double(height / 1.2))
        case 3:
            return CGSize(width: Double(width / 3), height: Double(width / 3))
        case 4:
            return CGSize(width: Double(width / 4), height: Double(width / 4))
        default:
            return CGSize(width: Double(width - 100), height: height)
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row > productListingCount / 2{
            if paginationTotalResult > productListingCount{
                guard let pageNum = try? paginationPageNumberBehaviorSubject.value() + 1 else {return}
                print( "productListingCount\(productListingCount)")
                print( "paginationTotalResult\(paginationTotalResult)")
               
                if paginationTotalResult / 20 >= pageNum && pageNum == productListingCount / 20 {
                    print( "pageNum\(pageNum)")
                paginationPageNumberBehaviorSubject.onNext(pageNum)
                }
            }
        }
        
    }
    
    
}

public enum CellSizeType {
    case larg
    case medium
    case small
}



