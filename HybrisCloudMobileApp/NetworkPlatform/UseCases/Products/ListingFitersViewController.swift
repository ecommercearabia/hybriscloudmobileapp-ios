//
//  ListingFitersViewController.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/4/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import UIKit

protocol GetRelevanceValueDelegate {
    func getRelevanceValue(relevance:String)
}

class ListingFitersViewController: UIViewController {
    
    var categoryIdSubject = BehaviorSubject<String>(value: "")
    var relevancesSubject = BehaviorSubject<String>(value: "")
    var filterDataSubject = BehaviorSubject<[searchFacet]>(value: [])
    var facetsSectionsSubject = BehaviorSubject<[FilterSection]>(value: [])
    var relevanceValue = ""
    var delegate:GetRelevanceValueDelegate?
    
    var filterViewModel = ListingFiltersViewModel(filterUseCase: NetworkPlatform.UseCaseProvider().makeProductListingUseCase())
    
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.preparation()
    }
    
    //MARK: Preparation Function
    private func preparation(){
        
        let input = ListingFiltersViewModel.Input(
            categoryIdTrigger: self.categoryIdSubject.asDriverOnErrorJustComplete(),
            relevanceTrigger: self.relevancesSubject.asDriverOnErrorJustComplete()
        )
        
        let output = self.filterViewModel.transform(input: input)
        
        self.getFilterData(output: output)
        self.bindDataToTable()
        self.handleTableViewCellSelected()
        self.handleDoneButton()
        self.handleResetButton()
    }
    
    //MARK: Get Filter Section And Data
    private func getFilterData(output:ListingFiltersViewModel.Output) {
        output.filterResult.asObservable().subscribe(onNext: {
            self.filterDataSubject.onNext($0.facets ?? [])
        }).disposed(by: disposeBag)
        
        // Get Sections
        self.filterDataSubject.subscribe(onNext: {[unowned self] (facets) in
            guard var subject = try? self.facetsSectionsSubject.value() else {return}
             subject.removeAll()
             facets.forEach { (searchFacet) in
              
                 subject.append(FilterSection(header: searchFacet.name ?? "", items: searchFacet.values ?? []))
             }
              self.facetsSectionsSubject.onNext(subject)
        }).disposed(by: disposeBag)
    }
    
    //MARK: Bind Filter Data To TableView Data Source Function
    private func bindDataToTable() {
        self.facetsSectionsSubject.bind(to: tableView.rx.items(dataSource: self.filterViewModel.filterdataSource())
        ).disposed(by: disposeBag)
    }
        
    //MARK: When TableView Cell Selelcted
    private func handleTableViewCellSelected() {
        
        //Select
        self.tableView.rx.itemSelected.subscribe(onNext:{ _ in
            self.reCallFilterApi()
        }).disposed(by: disposeBag)
        
        //Deselect
        self.tableView.rx.itemDeselected.subscribe(onNext: { _ in
            self.reCallFilterApi()
        }).disposed(by: disposeBag)
    }
    
    
    //MARK: Recall Api When Cell Selected
    private func reCallFilterApi() {
        var selected = [String]()
        let selectedIndexs = self.tableView.indexPathsForSelectedRows
        guard let allfacetsSections = try? self.facetsSectionsSubject.value() else {return}
        for (indexSection, section) in allfacetsSections.enumerated() {
            selectedIndexs?.forEach({ (indexPath) in
                if indexPath.section == indexSection {
                    for (indexRow, item) in section.items.enumerated() {
                        if indexRow == indexPath.row{
                            let value = item.query?.query?.value?.replacingOccurrences(of: ":relevance", with: "")
                            selected.append(value ?? "")
                        }
                    }
                }
            })
        }

        let relevance = selected.joined()
        self.relevanceValue = relevance
        self.relevancesSubject.onNext(relevance)
    }
    
    //MARK: Done Button Action
    private func handleDoneButton() {
        self.doneButton.rx.tap.subscribe(onNext: { _ in
            self.delegate?.getRelevanceValue(relevance: self.relevanceValue)
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
    }
    
    //MARK: Reset Button Action
    private func handleResetButton() {
        self.resetButton.rx.tap.subscribe(onNext: { (void) in
            guard let subject = try? self.facetsSectionsSubject.value() else {return}
            self.facetsSectionsSubject.onNext(subject)
        }).disposed(by: disposeBag)
    }
}
