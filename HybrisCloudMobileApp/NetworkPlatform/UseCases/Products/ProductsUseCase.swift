//
//  ProductsUseCase.swift
//  NetworkPlatform
//
//  Created by khalil on 8/2/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class ProductUseCase<Cache> : Domain.ProductUseCase where Cache : AbstractCache, Cache.T == ProductSearch {
 
 
    
    
    private let network : ProductsNetwork
    private let cache : Cache
    
    init(network : ProductsNetwork , cache : Cache){
        
        self.network = network
        self.cache = cache
    }
    
    func search() -> Observable<ProductSearch>{
        return network.searchProducts(currentPage: 0, pageSize: 20, sort: "")
    }
}
