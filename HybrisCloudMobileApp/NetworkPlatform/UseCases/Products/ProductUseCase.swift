//
//  ProductUseCase.swift
//  NetworkPlatform
//
//  Created by khalil on 8/3/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class ProductUseCase<Cache> : Domain.ProductUseCase where Cache : AbstractCache, Cache.T == ProductSearch {
   
 
    private let network : ProductsNetwork
    private let cache : Cache
    
    init(network : ProductsNetwork , cache : Cache){
        
        self.network = network
        self.cache = cache
    }
    
    func search(categoryID:String,currentPage: Int, pageSize: Int, query: String, sort: String) -> Observable<ProductSearch> {
        return network.searchProducts(categoryID: categoryID,currentPage: currentPage, pageSize: pageSize, query: query, sort: sort)
    }
    
}
