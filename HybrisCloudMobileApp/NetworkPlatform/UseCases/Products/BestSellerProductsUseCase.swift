//
//  BestSellerProductsUseCase.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 8/6/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import Domain
import RxSwift

final class BestSellerProductsUseCase<Cache>: Domain.BestSellerProductsUseCase where Cache: AbstractCache, Cache.T == BestSellerProducts {
    
    private let network: BestSellerProductsNetwork
    private let cache: Cache
    
    init(network: BestSellerProductsNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    
    func bestSellerProduct(productCode: String) -> Observable<BestSellerProducts> {
        let fetchComponent = cache.fetch(withID: productCode).asObservable()
        let stored = network.fetchBestSellerProduct(productCode: productCode)
            .flatMap {
                return self.cache.save(object: $0)
                    .asObservable()
                    .map(to: BestSellerProducts.self)
                    .concat(Observable.just($0))
        }
        
        return fetchComponent.concat(stored)
    }
    

    
}

