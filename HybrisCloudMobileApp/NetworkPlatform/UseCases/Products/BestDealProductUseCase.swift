//
//  BestDealProductUseCase.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import Domain
import RxSwift

final class BestDealProductUseCase<Cache>: Domain.BestDealProductUseCase where Cache: AbstractCache, Cache.T == ProductDetails {
    
    private let network: BestDealProductNetwork
    private let cache: Cache
    
    init(network: BestDealProductNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    
    func bestDealProduct(productCode: String) -> Observable<ProductDetails> {
        let fetchComponent = cache.fetch(withID: productCode).asObservable()
        let stored = network.fetchBestDealProduct(productCode: productCode)
            .flatMap {
                return self.cache.save(object: $0)
                    .asObservable()
                    .map(to: ProductDetails.self)
                    .concat(Observable.just($0))
        }
        
        return fetchComponent.concat(stored)
    }
    

    
}

