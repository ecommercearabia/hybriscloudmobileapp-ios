//
//  SuggestionUseCase.swift
//  NetworkPlatform
//
//  Created by ahmadSaleh on 12/31/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

 import Foundation
import Domain
import RxSwift

final class SuggestionUseCase<Cache> : Domain.SuggestionUseCase where Cache : AbstractCache, Cache.T == SuggestionModel {
    
   
 
    private let network : SuggestionNetwork
    private let cache : Cache
    
    init(network : SuggestionNetwork , cache : Cache){
        
        self.network = network
        self.cache = cache
    }
    
    func Suggestion(max: String, term: String) -> Observable<SuggestionModel> {
        network.suggestion(max: max, term: term)
    }

 
}
