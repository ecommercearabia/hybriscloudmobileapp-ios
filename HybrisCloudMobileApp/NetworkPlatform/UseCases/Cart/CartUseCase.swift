//
//  CartUseCase.swift
//  NetworkPlatform
//
//  Created by ahmadSaleh on 8/4/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class CartUseCase<Cache>: Domain.CartUseCase where Cache: AbstractCache, Cache.T == CartModel  {
    

    
    
    private let network: CartNetwork
    private let cache: Cache
    
    init(network: CartNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    func getCart(userId: String, cartId: String) -> Observable<CartModel> {
        
//        let fetchComponent = cache.fetch(withID: cartId).asObservable()
        let stored = network.fetchCart( cartId: cartId,userId : userId)/*.flatMap {
            return self.cache.save(object: $0)
                .asObservable()
                .map(to: CartModel.self)
                .concat(Observable.just($0))
        }*/
        
        return stored
    }
    
    func creatCart(userId: String) -> Observable<CartModel> {
        let fetchComponent = cache.fetch().asObservable()
        let stored = network.createCart(userId : userId).flatMap {
            return self.cache.save(object: $0)
                .asObservable()
                .map(to: CartModel.self)
                .concat(Observable.just($0))
        }
        
        return fetchComponent.concat(stored)
        
    }
    
    func mergeCart(oldCartGUID: String, currentCartGUID: String) -> Observable<CartModel> {
        return  network.mergeCart(oldCartGUID: oldCartGUID, currentCartGUID: currentCartGUID)
    }
    func deleteCartEntry(entryNumber: String , userId : String,cartId : String) -> Observable<Void> {
        
        return network.daleteIntry(itemId: entryNumber,cartId: cartId,userId: userId).map({_ in})
        
    }
    
    func appliesVoucher(cartId: String, userId: String, voucherId: String) -> Observable<Void> {
        return network.appliesVoucher(cartId: cartId, userId: userId, voucherId: voucherId)
    }
    
    func deletesVoucher(cartId: String, userId: String, voucherId: String) -> Observable<Void> {
        return network.deletesVoucher(cartId: cartId, userId: userId, voucherId: voucherId)

    }

    
    
}

final class CartMeargUseCase<Cache>: Domain.CartMeargUseCase where Cache: AbstractCache, Cache.T == CartMargeModel {
    
    
    private let network: CartMeargeNetwork
    private let cache: Cache
    
    init(network: CartMeargeNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    
    func meargeCart(userId: String, oldCartGuid: String, toMergeCartGuid: String) -> Observable<CartMargeModel> {
        let fetchComponent = cache.fetch().asObservable()
        let stored = network.meargeCart(userId : userId , oldCartGuid : oldCartGuid , toMergeCartGuid : toMergeCartGuid).flatMap {
            return self.cache.save(object: $0)
                .asObservable()
                .map(to: CartMargeModel.self)
                .concat(Observable.just($0))
        }
        
        return fetchComponent.concat(stored)
    }
    
}


final class AddtoCartUseCase<Cache>: Domain.AddtoCartUseCase where Cache: AbstractCache, Cache.T == AddedToCartModel {
 
    
    
    private let network: CartAddProductNetwork
    private let cache: Cache
    
    init(network: CartAddProductNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    func addAllToCart(userId: String, cartId: String, wishlistPK: String) -> Observable<AddedToCartModel> {
        return network.addAllToCart(userId: userId, cartId: cartId, wishlistPK: wishlistPK)
    }
    func addToCart(userId: String, cartId: String, entry: AddToCartProductModel) -> Observable<AddedToCartModel> {
        let fetchComponent = cache.fetch(withID: cartId).asObservable()
        let stored = network.addToCart( userId : userId, cartId: cartId, entry: entry).flatMap {
            return self.cache.save(object: $0)
                .asObservable()
                .map(to: AddedToCartModel.self)
                .concat(Observable.just($0))
        }
        
        return fetchComponent.concat(stored)
        
    }    
}

final class CartQuantityUserCase<Cache>: Domain.CartQuantityUserCase where Cache: AbstractCache, Cache.T == CartQuantityUpdate {
 
    private let network: CartQuantityNetwork
    private let cache: Cache
    
    init(network: CartQuantityNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    
    func updateCartQuantity(entryNumber: String, entry: CartEntry , userId : String , cartId : String) -> Observable<CartQuantityUpdate> {
        return network.updateCartQuantity(entryNumber: entryNumber, entry: entry, userId: userId, cartId : cartId )
    }
    
}


final class CartsUseCase<Cache>: Domain.CartsUseCase where Cache: AbstractCache, Cache.T == CartsModel {
    
    private let network: CartsNetwork
    private let cache: Cache
    
    init(network: CartsNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    func getCarts(userId: String) -> Observable<CartsModel> {
        network.getCarts(userId: userId)
    }

    
 
}


final class ValidationCartUseCase<Cache>: Domain.ValidationCartUseCase where Cache: AbstractCache, Cache.T == ValidationCartErrors {
    
    
    private let network: ValidationCartNetwork
    private let cache: Cache
    
    init(network: ValidationCartNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }

    func validationCart(userId: String, cartId: String) -> Observable<ValidationCartErrors> {
                network.validationCart(userId: userId, cartId: cartId)
    }

 
}


final class BankOffersCartUseCase<Cache>: Domain.BankOffersCartUseCase where Cache: AbstractCache, Cache.T == BankOffers {
  
 
    private let network: BankOffersCartNetwork
    private let cache: Cache
    
    init(network: BankOffersCartNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    func bankOffersCart() -> Observable<BankOffers> {
        network.bankOffersCart()
    }

 
 
}
