//
//  ReferralCodeUseCase.swift
//  NetworkPlatform
//
//  Created by admin on 20/06/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class ReferralCodeUseCase<Cache>: Domain.ReferralCodeUseCase where Cache: AbstractCache, Cache.T == ReferralCode {
   
    private let network: ReferralCodeNetwork
    private let cache: Cache
    
    init(network: ReferralCodeNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    //MARK: Get Referral Code
    func getReferralCode() -> Observable<ReferralCode> {
        return network.fetchReferralCode()
    }
    
    //MARK: Generate Referral Code
    func generateReferralCode() -> Observable<ReferralCode>{
        return network.generateReferralCode()
    }
    
    //MARK: Send Subscribe Email
    func sendSubscribeEmail(email: String, inSeperateEmails: Bool) -> Observable<Void> {
        return network.sendSubscribeEmail(email: email, inSeperateEmails: inSeperateEmails)
    }
}
