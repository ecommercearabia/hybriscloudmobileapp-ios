//
//  ConsentsListUseCase.swift
//  NetworkPlatform
//
//  Created by khalil on 9/9/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift


final class ConsentsListUseCase<Cache>: Domain.ConsentsListUseCase where Cache : AbstractCache ,Cache.T == Consents {
    private let network: ConsentsNetwork
    private let cache: Cache
    
    init(network: ConsentsNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    func getConsentsList() -> Observable<Consents> {
        return network.getConsentsList()
    }
    
    func addConsent(consentTemplateId: String, Version: Int) -> Observable<AddConsent> {
        return network.addConsent(consentTemplateId: consentTemplateId, version: Version)
    }
    
    func deleteConsent(code: String) -> Observable<Void> {
        return network.deleteConsent(code: code)
    }
    
    
    
    
}

