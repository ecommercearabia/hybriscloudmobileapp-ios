//
//  Data.swift
//  NetworkPlatform
//
//  Created by ahmadSaleh on 8/7/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
extension Data{
    func printData<T>(_ type: T.Type){
    print("========== \(type.self) ============")
    print("========== Success ============")
    print(String(decoding: self, as: UTF8.self))
    print("========== End ============")

    }
    
    func printData(){
    print("========== Success ============")
    print(String(decoding: self, as: UTF8.self))
    print("========== End ============")

    }
    
    func printData(url : String){
    print("========== Success ============")
    print("========== URL ============")
    print(url)
    print("========== Response ============")
    print(String(decoding: self, as: UTF8.self))
    print("========== End ============")

    }


}
