//
//  Network.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 7/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import Alamofire
import Domain
import RxAlamofire
import RxSwift
import OAuthSwift
import KeychainSwift

let interceptor : AuthenticationInterceptor<OAuthAuthenticator> = AuthenticationInterceptor(
    authenticator: OAuthAuthenticator(),
    credential: (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false))



final class Network<T: Decodable> {
    
    private let endPoint: String
    private let scheduler = MainScheduler.instance
    
    
    
    init(_ endPoint: String) {
        
        self.endPoint = endPoint
        
    }
    
    /*  func getItems(_ path: String) -> Observable<[T]> {
     let absolutePath = "\(endPoint)/\(path)"
     return Observable.create { observer -> Disposable in
     
     RxAlamofire
     .request(.get, absolutePath,interceptor: interceptor)
     .observeOn(self.scheduler)
     .responseData()
     .expectingObject(ofType:T.self)
     .subscribe(onNext: { apiResult in
     switch apiResult{
     case let .success(result):
     observer.onNext(result as! [T])
     observer.onCompleted()
     break
     
     case let .failure(apiErrorMessage) :
     print(apiErrorMessage)
     observer.onError(apiErrorMessage)
     break
     }
     },onError:{ err in
     observer.onError(err)
     
     })
     }
     }*/
    
    func getItems(_ path: String) -> Observable<T> {
        // interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)".setLangToApi()
        print("======= Model Type : \(T.self) =======")
        print("======= URL =======")
        print(absolutePath)
        print("======= Start request =======")
        
        
        return Observable.create { observer -> Disposable in
            
            RxAlamofire
                .request(.get, absolutePath,interceptor:  interceptor)
                .observe(on: self.scheduler)
                .validate(statusCode: 200 ..< 401)
                .responseData()
                .expectingObject(ofType:T.self)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case let .failure(apiErrorMessage) :
                        print("======= Error Model Type .get : \(T.self) ==== \(absolutePath) ===")
                        
                        print(apiErrorMessage)
                        observer.onError(apiErrorMessage)
                        break
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
    }
    
    
    func getItem(_ path: String, itemId: String) -> Observable<T> {
        //  interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)/\(itemId)".setLangToApi()
        return Observable.create { observer -> Disposable in
            RxAlamofire
                .request(.get, absolutePath,interceptor: interceptor)
                .observe(on: self.scheduler)
                .validate()
                .responseData()
                .expectingObject(ofType:T.self)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case let .failure(apiErrorMessage) :
                        print("======= Error Model Type .get : \(T.self) ==== \(absolutePath) ===")
                        
                        print(apiErrorMessage)
                        observer.onError(apiErrorMessage)
                        break
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
            
        }
        
    }
    //    func urlPostItemWithoutResponse(_ path: String, parameters: [String: Any]) -> Observable<Void> {
    //        let absolutePath = "\(endPoint)/\(path)"
    //        print("======= Model Type : \(T.self) =======")
    //        print(parameters)
    //        print("======= URL =======")
    //        print(absolutePath)
    //        print("======= Start request =======")
    //
    //        return Observable.create { observer -> Disposable in
    //
    //                  RxAlamofire
    //                  .request(.post, absolutePath, parameters: parameters , encoding: JSONEncoding.default,interceptor: interceptor)
    //                      .observeOn(self.scheduler)
    //                      .responseData()
    //                      .expectingObject(ofType:T.self)
    //                      .subscribe(onNext: { apiResult in
    //                          switch apiResult{
    //                          case let .success(result):
    //                            observer.onNext(result as! Void)
    //                              observer.onCompleted()
    //                              break
    //
    //                          case let .failure(apiErrorMessage) :
    //                              print(apiErrorMessage)
    //                              observer.onError(apiErrorMessage)
    //                              break
    //                          }
    //                      },onError:{ err in
    //                          observer.onError(err)
    //
    //                      })
    //              }
    //
    ////            .request(.post, absolutePath, parameters: parameters , encoding: URLEncoding.default ,interceptor: interceptor)
    ////            .observeOn(self.scheduler)
    ////            .response()
    ////            .map { (response) -> Void in
    ////
    ////
    ////        }
    //    }
    func urlPostItemWithoutResponse(_ path: String, parameters: [String: Any]) -> Observable<Void> {
        //interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)".setLangToApi()
        print("======= Model Type : \(T.self) =======")
        print(parameters)
        print("======= URL =======")
        print(absolutePath)
        print("======= Start request =======")
        
        //           return RxAlamofire
        //               .request(.post, absolutePath, parameters: parameters , encoding: URLEncoding.default ,interceptor: interceptor)
        //            .observe(on: self.scheduler)
        //               .response()
        //               .map { (response) -> Void in
        //
        //
        //           }
        
        return Observable.create { observer -> Disposable in
            RxAlamofire
                .request(.post, absolutePath, parameters: parameters , encoding: URLEncoding.default ,interceptor: interceptor)
                .debug()
                .response()
                .expectingObject()
                .observe(on: self.scheduler)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case .failure(_) :
                        
                        RxAlamofire
                            .request(.post, absolutePath, parameters: parameters , encoding: URLEncoding.default ,interceptor: interceptor)
                            .debug()
                            .responseData()
                            .expectingObject(ofType:T.self)
                            .observe(on: self.scheduler)
                            .subscribe(onNext: { apiResult in
                                switch apiResult{
                                case  .success(_):
                                    
                                    break
                                    
                                case let .failure(apiErrorMessage) :
                                    print("======= Error Model Type .put : \(T.self) ==== \(absolutePath) ===")
                                    
                                    print(apiErrorMessage)
                                    observer.onError(apiErrorMessage)
                                    break
                                }
                            },onError:{ err in
                                observer.onError(err)
                                
                            })
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
    }
    func urlPostItem(_ path: String, parameters: [String: Any]) -> Observable<T> {
        // interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)".setLangToApi()
        print("======= Model Type : \(T.self) =======")
        print(parameters)
        print("======= URL =======")
        print(absolutePath)
        print("======= Start request =======")
        
        return Observable.create { observer -> Disposable in
            RxAlamofire
                .request(.post, absolutePath, parameters: parameters , encoding: URLEncoding.default ,interceptor: interceptor)
                .observe(on: self.scheduler)
                .responseData()
                .expectingObject(ofType:T.self)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case let .failure(apiErrorMessage) :
                        print("======= Error Model Type  .post : \(T.self) ==== \(absolutePath) ===")
                        
                        print(apiErrorMessage)
                        observer.onError(apiErrorMessage)
                        break
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
    }
    
    func urlPostItemForLogin(_ path: String, parameters: [String: Any]) -> Observable<T> {
        // interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)".setLangToApi()
        print("======= Model Type : \(T.self) =======")
        print(parameters)
        print("======= URL =======")
        print(absolutePath)
        print("======= Start request =======")
        
        return Observable.create { observer -> Disposable in
            RxAlamofire
                .request(.post, absolutePath, parameters: parameters , encoding: URLEncoding.default,headers: HTTPHeaders(["clientId":"occ_mobile","clientSecret":"Erabia@123"]))
                .observe(on: self.scheduler)
                .responseData()
                .expectingObject(ofType:T.self)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case let .failure(apiErrorMessage) :
                        print("======= Error Model Type  .post : \(T.self) ==== \(absolutePath) ===")
                        
                        print(apiErrorMessage)
                        observer.onError(apiErrorMessage)
                        break
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
    }
    
    
    func urlPostItemForFaceId(_ path: String, parameters: [String: Any]) -> Observable<T> {
        // interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)".setLangToApi()
        print("======= Model Type : \(T.self) =======")
        print(parameters)
        print("======= URL =======")
        print(absolutePath)
        print("======= Start request =======")
        
        //        let interceptorForFaceId : AuthenticationInterceptor<OAuthAuthenticator> = AuthenticationInterceptor(
        //            authenticator: OAuthAuthenticator(),
        //            credential: OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false))
        
        return Observable.create { observer -> Disposable in
            RxAlamofire
                .request(.post, absolutePath, parameters: parameters ,interceptor: interceptor)
                .observe(on: self.scheduler)
                .responseData()
                .expectingObject(ofType:T.self)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        observer.onNext(result)
                        //                           observer.onCompleted()
                        break
                        
                    case let .failure(apiErrorMessage) :
                        print("======= Error Model Type  .post : \(T.self) ==== \(absolutePath) ===")
                        
                        print(apiErrorMessage)
                        observer.onError(apiErrorMessage)
                        break
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
    }
    
    func jsonPostItem(_ path: String, parameters: [String: Any]) -> Observable<T> {
        // interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)".setLangToApi()
        print("======= Model Type : \(T.self) =======")
        
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            jsonData.printData() }
        catch {print("error")}
        
        print("======= URL =======")
        print(absolutePath)
        print("======= Start request =======")
        
        return Observable.create { observer -> Disposable in
            
            RxAlamofire
                .request(.post, absolutePath, parameters: parameters , encoding: JSONEncoding.default ,interceptor: interceptor)
                .observe(on: self.scheduler)
                .responseData()
                .expectingObject(ofType:T.self)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case let .failure(apiErrorMessage) :

                        print("======= Error Model Type .post : \(T.self) ==== \(absolutePath) ===")
                        
                        print(apiErrorMessage)
                        observer.onError(apiErrorMessage)
                        break
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
    }
    
    
    func josnPostItem(_ path: String, parameters: [String: Any]) -> Observable<Void> {
        //  interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)".setLangToApi()
        print("======= parameters =======")
        print(parameters)
        print("======= URL =======")
        print(absolutePath)
        print("======= Start request =======")
        
        //
        //        return RxAlamofire
        //            .request(.post, absolutePath, parameters: parameters , encoding: JSONEncoding.default ,interceptor: interceptor)
        //            .debug()
        //            .response()
        //            .observe(on: scheduler)
        //            .map({ data -> Void in
        //
        //            })
        
        
        return Observable.create { observer -> Disposable in
            RxAlamofire
                .request(.post, absolutePath, parameters: parameters , encoding: JSONEncoding.default ,interceptor: interceptor)
                .debug()
                .response()
                .expectingObject()
                .observeOn(self.scheduler)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case .failure(_) :
                        
                        RxAlamofire
                            .request(.post, absolutePath, parameters: parameters , encoding: JSONEncoding.default ,interceptor: interceptor)
                            .debug()
                            .responseData()
                            .expectingObject(ofType:T.self)
                            .observeOn(self.scheduler)
                            .subscribe(onNext: { apiResult in
                                switch apiResult{
                                case  .success(_):
                                    
                                    break
                                    
                                case let .failure(apiErrorMessage) :
                                    print("======= Error Model Type .put : \(T.self) ==== \(absolutePath) ===")
                                    
                                    print(apiErrorMessage)
                                    observer.onError(apiErrorMessage)
                                    break
                                }
                            },onError:{ err in
                                observer.onError(err)
                                
                            })
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
    }
    
    func josnPostItem2(_ path: String, parameters: [String: Any]) -> Observable<Void> {
        //  interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)".setLangToApi()
        print("======= parameters =======")
        print(parameters)
        print("======= URL =======")
        print(absolutePath)
        print("======= Start request =======")
        
        
        //        return RxAlamofire
        //            .request(.post, absolutePath, parameters: parameters , encoding: JSONEncoding.default ,interceptor: interceptor)
        //            .debug()
        //        .validate()
        //            .response()
        //            .observeOn(scheduler)
        //            .map({ data -> Void in
        //
        //            })
        
        
        return Observable.create { observer -> Disposable in
            
            RxAlamofire.request(.post, absolutePath, parameters: parameters , encoding: JSONEncoding.default ,interceptor: interceptor)
                .debug()
                .response()
                .expectingObject()
                .observeOn(self.scheduler)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case .failure(_) :
                        
                        RxAlamofire.request(.post, absolutePath, parameters: parameters , encoding: JSONEncoding.default ,interceptor: interceptor)
                            .debug()
                            .responseData()
                            .expectingObject(ofType:T.self)
                            .observeOn(self.scheduler)
                            .subscribe(onNext: { apiResult in
                                switch apiResult{
                                case  .success(_):
                                    
                                    break
                                    
                                case let .failure(apiErrorMessage) :
                                    print("======= Error Model Type .put : \(T.self) ==== \(absolutePath) ===")
                                    
                                    print(apiErrorMessage)
                                    observer.onError(apiErrorMessage)
                                    break
                                }
                            },onError:{ err in
                                observer.onError(err)
                                
                            })
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
    }
    
    
    
    func setItemWithResponse(_ path: String) -> Observable<T> {
        // interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)".setLangToApi()
        print("======= Model Type : \(T.self) =======")
        print("======= URL =======")
        print(absolutePath)
        print("======= Start request =======")
        return RxAlamofire
            .request(.put, absolutePath,interceptor: interceptor)
            .debug()
            .data()
            .observeOn(scheduler)
            .map({ data -> T in
                data.printData(T.self)
                let model = try JSONDecoder().decode(T.self, from: data)
                return  model
                
            })
    }
    
    func setItemWithoutResponse(_ path: String) -> Observable<Void> {
        //  interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)".setLangToApi()
        print("======= Model Type : \(T.self) =======")
        print("======= URL =======")
        print(absolutePath)
        print("======= Start request =======")
        //          return RxAlamofire
        //              .request(.put, absolutePath,interceptor: interceptor)
        //              .debug()
        //              .response()
        //              .observeOn(scheduler)
        //              .map({ response -> Void in
        //
        //             })
        
        
        return Observable.create { observer -> Disposable in
            
            RxAlamofire.request(.put, absolutePath,interceptor: interceptor)
                .debug()
                .response()
                .expectingObject()
                .observeOn(self.scheduler)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case .failure(_) :
                        
                        RxAlamofire.request(.put, absolutePath,interceptor: interceptor)
                            .debug()
                            .responseData()
                            .expectingObject(ofType:T.self)
                            .observeOn(self.scheduler)
                            .subscribe(onNext: { apiResult in
                                switch apiResult{
                                case  .success(_):
                                    
                                    break
                                    
                                case let .failure(apiErrorMessage) :
                                    print("======= Error Model Type .put : \(T.self) ==== \(absolutePath) ===")
                                    
                                    print(apiErrorMessage)
                                    observer.onError(apiErrorMessage)
                                    break
                                }
                            },onError:{ err in
                                observer.onError(err)
                                
                            })
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
    }
    
    
    func setItem(_ path: String, parameters: [String: Any]) -> Observable<T> {
        // interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)".setLangToApi()
        print(parameters)
        return Observable.create { observer -> Disposable in
            
            RxAlamofire
                .request(.put, absolutePath, parameters: parameters , encoding: JSONEncoding.default,interceptor: interceptor)
                .observeOn(self.scheduler)
                .responseData()
                .expectingObject(ofType:T.self)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case let .failure(apiErrorMessage) :
                        print("======= Error Model Type  .put : \(T.self) ==== \(absolutePath) ===")
                        
                        print(apiErrorMessage)
                        observer.onError(apiErrorMessage)
                        break
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
    }
    
    func setItem(_ path: String, parameters: [String: Any]) -> Observable<Void> {
        //  interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)".setLangToApi()
        
        //        return RxAlamofire
        //            .request(.put, absolutePath, parameters: parameters ,interceptor: interceptor )
        //            .debug()
        //            .responseData()
        //            .observeOn(scheduler)
        //
        //            .map({ response -> Void in
        //
        //            })
        print(parameters)
        
        return Observable.create { observer -> Disposable in
            
            RxAlamofire.request(.put, absolutePath, parameters: parameters ,interceptor: interceptor )
                .debug()
                .response()
                .expectingObject()
                .observeOn(self.scheduler)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case .failure(_) :
                        
                        RxAlamofire.request(.put, absolutePath, parameters: parameters ,interceptor: interceptor )
                            .debug()
                            .responseData()
                            .expectingObject(ofType:T.self)
                            .observeOn(self.scheduler)
                            .subscribe(onNext: { apiResult in
                                switch apiResult{
                                case  .success(_):
                                    
                                    break
                                    
                                case let .failure(apiErrorMessage) :
                                    print("======= Error Model Type .put : \(T.self) ==== \(absolutePath) ===")
                                    
                                    print(apiErrorMessage)
                                    observer.onError(apiErrorMessage)
                                    break
                                }
                            },onError:{ err in
                                observer.onError(err)
                                
                            })
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
        
    }
    
    func setItemRowJson(_ path: String, parameters: [String: Any]) -> Observable<Void> {
        //  interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)".setLangToApi()
        
        //        return RxAlamofire
        //            .request(.put, absolutePath, parameters: parameters ,interceptor: interceptor )
        //            .debug()
        //            .responseData()
        //            .observeOn(scheduler)
        //
        //            .map({ response -> Void in
        //
        //            })
        print(parameters)
        
        return Observable.create { observer -> Disposable in
            
            RxAlamofire.request(.put, absolutePath, parameters: parameters , encoding: JSONEncoding.default,interceptor: interceptor )
                .debug()
                .response()
                .expectingObject()
                .observeOn(self.scheduler)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case .failure(_) :
                        
                        RxAlamofire.request(.put, absolutePath, parameters: parameters, encoding: JSONEncoding.default,interceptor: interceptor)
                            .debug()
                            .responseData()
                            .expectingObject(ofType:ErrorResponse.self)
                            .observeOn(self.scheduler)
                            .subscribe(onNext: { apiResult in
                                switch apiResult{
                                case  .success(_):
                                    
                                    break
                                    
                                case let .failure(apiErrorMessage) :
                                    print("======= Error Model Type .put : \(T.self) ==== \(absolutePath) ===")
                                    
                                    print(apiErrorMessage)
                                    observer.onError(apiErrorMessage)
                                    break
                                }
                            },onError:{ err in
                                observer.onError(err)
                                
                            })
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
        
    }
    
    func setItemWithItemId(_ path: String, itemId: String, parameters: [String: Any]) -> Observable<Void> {
        //  interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)/\(itemId)".setLangToApi()
        print("======= parameters =======")
        print(parameters)
        print("======= URL =======")
        print(absolutePath)
        print("======= Start request =======")
        
        
        
        return Observable.create { observer -> Disposable in
            
            RxAlamofire
                .request(.put, absolutePath, parameters: parameters , encoding: JSONEncoding.default , interceptor: interceptor )
                .debug()
                .response()
                .expectingObject()
                .observeOn(self.scheduler)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case .failure(_) :
                        
                        RxAlamofire
                            .request(.put, absolutePath, parameters: parameters , encoding: JSONEncoding.default , interceptor: interceptor )
                            .debug()
                            .responseData()
                            .expectingObject(ofType:T.self)
                            .observeOn(self.scheduler)
                            .subscribe(onNext: { apiResult in
                                switch apiResult{
                                case  .success(_):
                                    
                                    break
                                    
                                case let .failure(apiErrorMessage) :
                                    print("======= Error Model Type .put : \(T.self) ==== \(absolutePath) ===")
                                    
                                    print(apiErrorMessage)
                                    observer.onError(apiErrorMessage)
                                    break
                                }
                            },onError:{ err in
                                observer.onError(err)
                                
                            })
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
    }
    
    
    func deleteItem(_ path: String, itemId: String) -> Observable<Void> {
        // interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)/\(itemId)".setLangToApi()
        
        //        return RxAlamofire
        //            .request(.delete, absolutePath,interceptor: interceptor)
        //            .debug()
        //            .observeOn(scheduler)
        //            .data()
        //            .map({ data -> Void in
        //            })
        
        
        return Observable.create { observer -> Disposable in
            
            RxAlamofire
                .request(.delete, absolutePath,interceptor: interceptor)
                .debug()
                .response()
                .expectingObject()
                .observeOn(self.scheduler)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case .failure(_) :
                        
                        RxAlamofire
                            .request(.delete, absolutePath,interceptor: interceptor)
                            .debug()
                            .responseData()
                            .expectingObject(ofType:T.self)
                            .observeOn(self.scheduler)
                            .subscribe(onNext: { apiResult in
                                switch apiResult{
                                case  .success(_):
                                    
                                    break
                                    
                                case let .failure(apiErrorMessage) :
                                    print("======= Error Model Type .put : \(T.self) ==== \(absolutePath) ===")
                                    
                                    print(apiErrorMessage)
                                    observer.onError(apiErrorMessage)
                                    break
                                }
                            },onError:{ err in
                                observer.onError(err)
                                
                            })
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
    }
    
    
    
    func deleteItem(_ path: String) -> Observable<Void> {
        //   interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)/".setLangToApi()
        
        //        return RxAlamofire
        //                   .request(.delete, absolutePath,interceptor: interceptor)
        //                   .debug()
        //                   .response()
        //                   .observeOn(scheduler)
        //                   .map({ data -> Void in
        //
        //                   })
        return Observable.create { observer -> Disposable in
            
            RxAlamofire
                .request(.delete, absolutePath,interceptor: interceptor)
                .debug()
                .response()
                .expectingObject()
                .observeOn(self.scheduler)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case .failure(_) :
                        
                        RxAlamofire
                            .request(.delete, absolutePath,interceptor: interceptor)
                            .debug()
                            .responseData()
                            .expectingObject(ofType:T.self)
                            .observeOn(self.scheduler)
                            .subscribe(onNext: { apiResult in
                                switch apiResult{
                                case  .success(_):
                                    
                                    break
                                    
                                case let .failure(apiErrorMessage) :
                                    print("======= Error Model Type .put : \(T.self) ==== \(absolutePath) ===")
                                    
                                    print(apiErrorMessage)
                                    observer.onError(apiErrorMessage)
                                    break
                                }
                            },onError:{ err in
                                observer.onError(err)
                                
                            })
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
    }
    
    //
    
    func patchItemWithoutResponse(_ path: String) -> Observable<Void> {
        //  interceptor.credential =  (try? JSONDecoder().decode(OAuthCredential.self, from: KeychainSwift().getData("usercredentials") ?? Data())) ?? OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
        let absolutePath = "\(endPoint)/\(path)".setLangToApi()
        print("======= Model Type : \(T.self) =======")
        print("======= URL =======")
        print(absolutePath)
        print("======= Start request =======")
        //          return RxAlamofire
        //              .request(.put, absolutePath,interceptor: interceptor)
        //              .debug()
        //              .response()
        //              .observeOn(scheduler)
        //              .map({ response -> Void in
        //
        //             })
        
        
        return Observable.create { observer -> Disposable in
            
            RxAlamofire.request(.patch, absolutePath,interceptor: interceptor)
                .debug()
                .response()
                .expectingObject()
                .observeOn(self.scheduler)
                .subscribe(onNext: { apiResult in
                    switch apiResult{
                    case let .success(result):
                        
                        observer.onNext(result)
                        observer.onCompleted()
                        break
                        
                    case .failure(_) :
                        
                        RxAlamofire.request(.patch, absolutePath,interceptor: interceptor)
                            .debug()
                            .responseData()
                            .expectingObject(ofType:T.self)
                            .observeOn(self.scheduler)
                            .subscribe(onNext: { apiResult in
                                switch apiResult{
                                case  .success(_):
                                    
                                    break
                                    
                                case let .failure(apiErrorMessage) :
                                    print("======= Error Model Type .put : \(T.self) ==== \(absolutePath) ===")
                                    
                                    print(apiErrorMessage)
                                    observer.onError(apiErrorMessage)
                                    break
                                }
                            },onError:{ err in
                                observer.onError(err)
                                
                            })
                    }
                },onError:{ err in
                    observer.onError(err)
                    
                })
        }
    }
    
    //
    
}


public struct ErrorResponse: Codable , Error {
    public  let  errors: [ErrorResponseElement]?
}

public struct ErrorResponseElement: Codable {
    public let message, type: String?
    public let reason :String?
    public let subject :String?
    public let subjectType :String?
    
    
}


enum ApiResult<Value, Error>{
    case success(Value)
    case failure(Error)
    
    init(value: Value){
        self = .success(value)
    }
    
    init(error: Error){
        self = .failure(error)
    }
}




extension Observable where Element == (HTTPURLResponse, Data){
    fileprivate func expectingObject<T : Decodable>(ofType type: T.Type) -> Observable<ApiResult<T, Error>>{
        return self.map{ (httpURLResponse, data) -> ApiResult<T, Error> in
            
            if(httpURLResponse.statusCode >= 200 && httpURLResponse.statusCode < 300)
            {
                do {
                    data.printData(url: httpURLResponse.url?.string ?? "")
                    let object = try JSONDecoder().decode(type, from: data)
                    return .success(object)
                    
                    
                } catch {
                    data.printData()
                    print(error)
                    #if RELEASE || RELEASEPROD
                    return .failure(ErrorResponse.init(errors: [ErrorResponseElement(message: "There was an error. Please try again.", type: nil, reason: nil,
                                                                                     subject: nil, subjectType: nil)]))
                    #else
                    return .failure(ErrorResponse.init(errors: [ErrorResponseElement(message: "can not convert to model \(T.self) \(httpURLResponse.statusCode)", type: nil, reason: nil,
                                                                                     subject: nil, subjectType: nil)]))
                    #endif
                    
                }
                
            }
            else
            {
                do {
                    data.printData()
                    let object = try JSONDecoder().decode(ErrorResponse.self, from: data)
                    return .failure(object)
                    
                } catch {
                    data.printData()
                    
                    #if RELEASE || RELEASEPROD
                    return .failure(ErrorResponse.init(errors: [ErrorResponseElement(message: "There was an error. Please try again", type: nil, reason: nil,
                                                                                     subject: nil, subjectType: nil)]))
                    
                    #else
                    return .failure(ErrorResponse.init(errors: [ErrorResponseElement(message: "can not convert to model \(ErrorResponse.self)\(httpURLResponse.statusCode)", type: nil, reason: nil,
                                                                                     subject: nil, subjectType: nil)]))
                    #endif
                    
                }
                
            }
        }
    }
}


extension Observable where Element == (HTTPURLResponse){
    fileprivate func expectingObject() -> Observable<ApiResult<Void, Error>>{
        return self.map{ (httpURLResponse) -> ApiResult<Void, Error> in
            
            if(httpURLResponse.statusCode >= 200 && httpURLResponse.statusCode < 300)
            {
                return .success(())
                
            }
            else
            {
                #if RELEASE || RELEASEPROD
                return .failure(ErrorResponse.init(errors: [ErrorResponseElement(message: "", type: "There was an error. Please try again", reason: nil,
                                                                                 subject: nil, subjectType: nil )]))
                #else
                return .failure(ErrorResponse.init(errors: [ErrorResponseElement(message: "", type: "Should Read Error From Another Alamofire", reason: nil,
                                                                                 subject: nil, subjectType: nil )]))
                
                #endif
                
                
            }
        }
    }
}

extension String {
    
    public func setLangToApi()->String{
        
        
        let queryItems = [URLQueryItem(name: "lang", value: currentAppleLanguage())]
        var urlComps = URLComponents(string: self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "") ?? URLComponents(string: "")
        urlComps?.queryItems?.append(contentsOf: queryItems)
        let result = urlComps?.string ?? ""
        return result
    }
    
    
    public func currentAppleLanguage() -> String{
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: "AppleLanguages") as! NSArray
        let current = langArray.firstObject as! String
        
        let index = current.index(current.startIndex, offsetBy: 2)
        let currentWithoutLocale = current.prefix(upTo: index) // Hello
        if String(currentWithoutLocale) == "ar" || String(currentWithoutLocale) == "en" {
            return String(currentWithoutLocale)
        }
        else{
            return "en"
        }
    }
}


