//
//  NetworkProvider.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 7/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Domain

final class NetworkProvider {
    private let apiEndpoint: String
    
    public init() {
        apiEndpoint = Environment.shared.apiEndpoint?.rawValue ?? ""
    }
    
    public func makePagesNetwork() -> PagesNetwork {
        let network = Network<Page>(apiEndpoint)
        return PagesNetwork(network: network)
    }
    
    public func makeUsersNetwork() -> UsersNetwork {
        let network = Network<UserData>(apiEndpoint)
        let networkStoreCreditAmount = Network<StoreCreditAmount>(apiEndpoint)
        let networkStoreCreditHistroy = Network<StoreCreditHistroy>(apiEndpoint)
        let networkSendOtp = Network<SendOtpCode>(apiEndpoint)
        let networkVerifyOtp = Network<VerifyOtpCode>(apiEndpoint)
        let networkNewLogin = Network<NewLoginModel>(apiEndpoint)
        let networkLoginWithFaceId = Network<LoginFaceID>(apiEndpoint)
        return UsersNetwork(network: network, networkStoreCreditAmount: networkStoreCreditAmount, networkStoreCreditHistroy: networkStoreCreditHistroy, networkSendOtp: networkSendOtp, networkVerifyOtp: networkVerifyOtp, networkLoginWithFaceId: networkLoginWithFaceId, networkNewLogin: networkNewLogin)
        
    }
    
    
    public func makeTitleNetwork() -> MiscsNetwork {
        let network = Network<Titles>(apiEndpoint)
        return MiscsNetwork(network: network)
    }
    
    public func makeCompnentNetwork() -> ComponentNetwork {
        let network = Network<Component>(apiEndpoint)
        return ComponentNetwork(network: network)
    }
    
    public func makecatalogNetwork() -> CatalogNetwork {
        let network = Network<Catalog>(apiEndpoint)
        return CatalogNetwork(network: network)
    }
    
    public func makeAddressesNetwork() -> AddressesNetwork {
        let network = Network<AddressesModel>(apiEndpoint)
        let networkAddAddress = Network<Address>(apiEndpoint)
        return AddressesNetwork(network: network,networkAddAddress: networkAddAddress)
    }
    public func makeCartNetwork() -> CartNetwork {
        let network = Network<CartModel>(apiEndpoint)
        return CartNetwork(network: network)
    }
    
    public func makeCreatCartNetwork() -> CartNetwork {
        let network = Network<CartModel>(apiEndpoint)
        return CartNetwork(network: network)
    }
    
    
    public func makeBestDealProductNetwork() -> BestDealProductNetwork {
        let network = Network<ProductDetails>(apiEndpoint)
        return BestDealProductNetwork(network: network)
    }
    public func makeBestSellerProductNetwork() -> BestSellerProductsNetwork {
        let network = Network<BestSellerProducts >(apiEndpoint)
        return BestSellerProductsNetwork(network: network)
    }
    
    public func makeCountryNetwork() -> CountriesNetwork {
        let network = Network<CountriesModel>(apiEndpoint)
        return CountriesNetwork(network: network)
    }
    
    public func makeAddToCartNetwork() -> CartAddProductNetwork {
        let network = Network<AddedToCartModel>(apiEndpoint)
        return CartAddProductNetwork(network: network)
    }
    public func makeProductNetwork() -> ProductsNetwork {
        let network = Network<ProductSearch>(apiEndpoint)
        return ProductsNetwork(network: network)
    }
    
    
    public func makeProductDetailsNetwork() -> ProductDetailsNetwork{
        let network = Network<ProductDetails>(apiEndpoint)
        let networkReviews = Network<Reviews>(apiEndpoint)
        let networkReview = Network<Review>(apiEndpoint)
        
        return ProductDetailsNetwork(network: network, networkReviews: networkReviews, networkReview: networkReview)
    }
    public func makeSendOtpNetwork() -> SendOTPNetwork{
        let network = Network<SendOTP>(apiEndpoint)
        return SendOTPNetwork(network: network)
    }
    public func makeVerifyOtpNetwork() -> VerifyOTPNetwork{
        let network = Network<VerifyOTP>(apiEndpoint)
        return VerifyOTPNetwork(network: network)
    }
    public func makeChickOutNetwork() -> ChickOutNetwork {
        let network = Network<TimeSlotsModel>(apiEndpoint)
        return ChickOutNetwork(network: network)
    }
    
    public func makeDeliveryNetwork() -> DeliveryModeNetwork {
        let network = Network<DeliveryModeModel>(apiEndpoint)
        return DeliveryModeNetwork(network: network)
    }
    
    public func makeMyOrdersNetwork() -> MyOrdersNetwork{
        let network = Network<MyOrders>(apiEndpoint)
        return MyOrdersNetwork(network: network)
    }
    
    public func makeOrdersDetailsNetwork() -> OrdersDetailsNetwork{
        let network = Network<ordersDetailsMyOrdersDetails>(apiEndpoint)
        return OrdersDetailsNetwork(network: network)
    }
    
    public func makePaymentNetwork() -> PaymentNetwork{
        let network = Network<PaymentModesModel>(apiEndpoint)
        let networkTow = Network<SetPaymentModeResponse>(apiEndpoint)
        return PaymentNetwork(network: network, networkSet: networkTow)
    }
    
    
    public func makeCitiesNetwork() -> CitiesNetwork{
        let network = Network<CitiesModel>(apiEndpoint)
        return CitiesNetwork(network: network)
    }
    
    public func makePaymentDetailsNetwork() -> PaymentDetailsNetwork{
        let network = Network<PaymentDetailsResponse>(apiEndpoint)
        return PaymentDetailsNetwork(network: network)
    }
    public func makePlaceOrderNetwork() -> PlaceOrderNetwork{
        let network = Network<PalceOrderResponse>(apiEndpoint)
        return PlaceOrderNetwork(network: network)
    }
    
    public func makePaymentInfoNetwork() -> PaymnetInfoNetwork{
        let network = Network<PaymentInfo>(apiEndpoint)
        return PaymnetInfoNetwork(network: network)
    }
    
    public func makeCartQuantityNetwork() -> CartQuantityNetwork{
        let network = Network<CartQuantityUpdate>(apiEndpoint)
        return CartQuantityNetwork(network: network)
    }
    
    public func makeCartsNetwork() -> CartsNetwork{
        let network = Network<CartsModel>(apiEndpoint)
        return CartsNetwork(network: network)
    }
    
    public func makeValidationCartUseCase() -> ValidationCartNetwork{
        let network = Network<ValidationCartErrors>(apiEndpoint)
        return ValidationCartNetwork(network: network)
    }
    
    public func makeWishListUseCase() -> WishListNetwork{
        let network = Network<WishList>(apiEndpoint)
        return WishListNetwork(network: network)
    }
    
    public func makeStroyCreditCartUseCase() -> StoreCreditCartNetwork{
        let network = Network<StoreCreditModes>(apiEndpoint)
        let networkAvilableAmountInStoreCredit = Network<StoreCreditAvailableAmountModel>(apiEndpoint)
        return StoreCreditCartNetwork(network: network, networkStoreCredit: networkAvilableAmountInStoreCredit)
    }
    
    public func makeUserMessegesUseCase() -> UserMessagesNetwork{
        let network = Network<UserMessages>(apiEndpoint)
        return UserMessagesNetwork(network: network)
    }
    
    public func makeBankOffersUseCase() -> BankOffersCartNetwork{
        let network = Network<BankOffers>(apiEndpoint)
        return BankOffersCartNetwork(network: network)
    }
    
    public func makeSuggestionUseCase() -> SuggestionNetwork{
        let network = Network<SuggestionModel>(apiEndpoint)
        return SuggestionNetwork(network: network)
    }
    
    public func makeReferralCodeUseCase() -> ReferralCodeNetwork {
        let network = Network<ReferralCode>(apiEndpoint)
        return ReferralCodeNetwork(network: network)
    }
    
    public func makeApplePayUseCase() -> ApplePayNetwork {
        let network = Network<ApplePayModel>(apiEndpoint)
        return ApplePayNetwork(network: network)
    }
    
    public func makeConsentsListUseCase() -> ConsentsNetwork {
        let network = Network<Consents>(apiEndpoint)
        let networkAdd = Network<AddConsent>(apiEndpoint)
        return ConsentsNetwork(network: network, networkAdd: networkAdd)
    }
    
    public func makeSavedCardsUseCase() -> SavedCardsNetwork {
        let network = Network<SavedCardsModel>(apiEndpoint)
        return SavedCardsNetwork(network: network)
    }
    
    public func makeNationalitiesUseCase() -> NationalitiesNetwork {
        let network = Network<NationalitiesModel>(apiEndpoint)
        return NationalitiesNetwork(network: network)
    }
    
    public func makeConfigUseCase() -> ConfigNetwork {
        let network = Network<ConfigModel>(apiEndpoint)
        return ConfigNetwork(network: network)
    }
    
    public func makeShipmentTypesUseCase() -> ShipmentTypesNetwork {
        let network = Network<ShipmentTypesModel>(apiEndpoint)
        let networkSelectedShipment = Network<ShipmentSelctedShipmentTypesModel>(apiEndpoint)
        let networkGetType = Network<CurrentShipmentTypeModel>(apiEndpoint)
        return ShipmentTypesNetwork(network: network ,networkSelectedShipment: networkSelectedShipment , networkGetType: networkGetType )
    }
    
    public func makeLoyaltyPointsUseCase() -> LoyaltyPointsNetwork {
        let network = Network<LoyaltyPointsModel>(apiEndpoint)
        let networkAvailableLoyaltyPoints = Network<Double>(apiEndpoint)
        return LoyaltyPointsNetwork(network: network, networkAvailableLoyaltyPoints: networkAvailableLoyaltyPoints)
    }
    
    public func makeDeliveryTypesUseCase() -> DeliveryTypesNetwork {
        let network = Network<DeliveryTypesModel>(apiEndpoint)
        let networkDeliveryType = Network<String>(apiEndpoint)
        let networkSetDeliveryType = Network<CartModel>(apiEndpoint)
        
        return DeliveryTypesNetwork(network: network, networkDeliveryType: networkDeliveryType, networkSetDeliveryType: networkSetDeliveryType)
    }

}


