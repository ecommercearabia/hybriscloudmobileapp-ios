//
//  OAuth.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 7/27/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxAlamofire
import RxSwift
import Alamofire
import OAuthSwift
import KeychainSwift
import Domain

struct OAuthCredential: AuthenticationCredential {
    let accessToken: String
    let refreshToken: String
    let expiration: Date
    let isUserCreintials: Bool
    // Require refresh if within 5 minutes of expiration
    var requiresRefresh: Bool { isUserCreintials && Date(timeIntervalSinceNow:  5 * 60) > expiration }
}


protocol Serializable:Codable {
    
    func serialize() -> Data
   
}
extension OAuthCredential:Serializable{
    
    func serialize() -> Data {
        let encoder = JSONEncoder()
        guard let data = try? encoder.encode(self) else{
            return Data()
        }
        return data
    }
    
}

class OAuthAuthenticator: Authenticator {
    
    
    func apply(_ credential: OAuthCredential, to urlRequest: inout URLRequest) {
        print("bearer token :\(credential.accessToken)")
        
        urlRequest.cachePolicy = .reloadIgnoringCacheData
        urlRequest.headers.add(.authorization(bearerToken: credential.accessToken))
    }
    
    func didRequest(_ urlRequest: URLRequest,
                    with response: HTTPURLResponse,
                    failDueToAuthenticationError error: Error) -> Bool {
        return response.statusCode == 401
    }
    
    func isRequest(_ urlRequest: URLRequest, authenticatedWith credential: OAuthCredential) -> Bool {
        let bearerToken = HTTPHeader.authorization(bearerToken: credential.accessToken).value
        print("bearer token :\(bearerToken)")
        return urlRequest.headers["Authorization"] == bearerToken
        
    }
    
    let oauth2 = OAuth2Swift(consumerKey: "occ_mobile", consumerSecret: "Erabia@123",
                             authorizeUrl: "\(Environment.shared.apiEndpoint?.rawValue ?? "")/authorizationserver/oauth/token",
                             accessTokenUrl: "\(Environment.shared.apiEndpoint?.rawValue ?? "")/authorizationserver/oauth/token", responseType: "")
    
    func refresh(_ credential: OAuthCredential,
                 for session: Session,
                 completion: @escaping (Result<OAuthCredential, Error>) -> Void) {
        if credential.isUserCreintials{
            oauth2.renewAccessToken(withRefreshToken: credential.refreshToken) {[unowned self] (result) in
                switch result {
                case .success(let (credential, response, parameters)):
                    print("Authorized! Access token is in `oauth2.accessToken`")
                    print("Authorized! Additional parameters: \(credential)")
                    let credential = OAuthCredential(accessToken: credential.oauthToken,
                                                     refreshToken: credential.oauthRefreshToken,
                                                     expiration: credential.oauthTokenExpiresAt ?? Date(), isUserCreintials: true)
                    KeychainSwift().set(credential.serialize(), forKey: "usercredentials")
                    KeychainSwift().set("current", forKey: "user")

                    completion(.success(credential))
                    
                // Do your request
                case .failure(let error):
                    self.oauth2.authorize(deviceToken: "123", grantType: "client_credentials") { (result) in
                        switch result {
                        case .success(let (credential, response, parameters)):
                            print("Authorized! Access token is in `oauth2.accessToken`")
                            print("Authorized! Additional parameters: \(credential)")
                            let credential = OAuthCredential(accessToken: credential.oauthToken,
                                                             refreshToken: credential.oauthRefreshToken,
                                                             expiration: credential.oauthTokenExpiresAt ?? Date(), isUserCreintials: false)
                           
                            KeychainSwift().set(credential.serialize(), forKey: "usercredentials")
                            KeychainSwift().set("anonymous", forKey: "user")
                            completion(.success( credential))
                            
                        // Do your request
                        case .failure(let error):
                             KeychainSwift().delete("usercredentials")
                            print("Authorization was canceled or went wrong: \(error)")
                             KeychainSwift().delete("user")// error will not be nil
                            completion(.failure(error))
                        }
                        
                    }
                }
            }

        }else{
            self.oauth2.authorize(deviceToken: "123", grantType: "client_credentials") { (result) in
                switch result {
                case .success(let (credential, response, parameters)):
                    print("Authorized! Access token is in `oauth2.accessToken`")
                    print("Authorized! Additional parameters: \(credential)")
                    let credential = OAuthCredential(accessToken: credential.oauthToken,
                                                     refreshToken: credential.oauthRefreshToken,
                                                     expiration: credential.oauthTokenExpiresAt ?? Date(), isUserCreintials: false)
                   
                    KeychainSwift().set(credential.serialize(), forKey: "usercredentials")
                    KeychainSwift().set("anonymous", forKey: "user")
                    completion(.success( credential))
                    
                // Do your request
                case .failure(let error):
                     KeychainSwift().delete("usercredentials")
                    print("Authorization was canceled or went wrong: \(error)")
                     KeychainSwift().delete("user")// error will not be nil
                    completion(.failure(error))
                }
                
            }
        }
       
    }
    
}


