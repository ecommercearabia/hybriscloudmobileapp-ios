
import Foundation
import RxSwift
import Domain

protocol AbstractCache {
    associatedtype T
    func save(object: T) -> Completable
    func save(objects: [T]) -> Completable
    func fetch(withID id: String) -> Maybe<T>
    func fetch() -> Maybe<T>

    func fetchObjects() -> Maybe<[T]>
}

final class Cache<T: Codable&Identifiable>: AbstractCache  {
    enum Error: Swift.Error {
        case saveObject(T)
        case saveObjects([T])
        case fetchObject(T.Type)
        case fetchObjects(T.Type)
    }
    enum FileNames {
        static var objectFileName: String {
            return "\(T.self)\("".currentAppleLanguage()).dat"
        }
        static var objectsFileName: String {
            return "\(T.self)s\("".currentAppleLanguage()).dat"
        }
    }

    private let path: String
    private let cacheScheduler = SerialDispatchQueueScheduler(internalSerialQueueName: "com.CleanAchitecture.Network.Cache.queue")

    init(path: String) {
        self.path = path
    }

    func save(object: T) -> Completable {
        return Completable.create { (observer) -> Disposable in
            
            guard let directoryURL = self.directoryURL() else {
                           observer(.completed)
                           return Disposables.create()
                       }
                       let path = directoryURL
                           .appendingPathComponent("\(object.identity)\(FileNames.objectFileName)")
                       self.createDirectoryIfNeeded(at: directoryURL)
            
           
            
            do {
              let data = try PropertyListEncoder().encode(object)
               try data.write(to: path)
              observer(.completed)
            } catch {
              observer(.error(error))
            }
            
           
            
            return Disposables.create()
        }.subscribeOn(cacheScheduler)
    }

    func save(objects: [T]) -> Completable {
        return Completable.create { (observer) -> Disposable in
            guard let directoryURL = self.directoryURL() else {
                observer(.completed)
                return Disposables.create()
            }
            let path = directoryURL
                .appendingPathComponent(FileNames.objectsFileName)
            self.createDirectoryIfNeeded(at: directoryURL)
            do {
                let data = try PropertyListEncoder().encode(objects)
                let success = NSKeyedArchiver.archiveRootObject(data, toFile: path.path)
                success ? observer(.completed) : observer(.error(Error.saveObjects(objects)))
            } catch {
                observer(.error(error))
            }
            
            return Disposables.create()
        }.subscribeOn(cacheScheduler)
    }

    func fetch(withID id: String) -> Maybe<T> {
        return Maybe<T>.create { (observer) -> Disposable in
            guard let directoryURL = self.directoryURL() else {
                                     observer(.completed)
                                     return Disposables.create()
                                 }
                                 let path = directoryURL
                                     .appendingPathComponent("\(id)\(FileNames.objectFileName)")
                
                
            
            guard let data = try? Data(contentsOf: path) else {
                observer(.completed)
            return Disposables.create()
            }
            do {
              let object = try PropertyListDecoder().decode(T.self, from: data)
              observer(MaybeEvent<T>.success(object))
              return Disposables.create()
            } catch {
              observer(.completed)
            return Disposables.create()
            }
           
            
        }.subscribeOn(cacheScheduler)
    }
    func fetch() -> Maybe<T> {
           return Maybe<T>.create { (observer) -> Disposable in
               guard let directoryURL = self.directoryURL() else {
                                        observer(.completed)
                                        return Disposables.create()
                                    }
                                    let path = directoryURL
                                        .appendingPathComponent((FileNames.objectFileName))
                   
                   
               
               guard let data = try? Data(contentsOf: path) else {
                   observer(.completed)
               return Disposables.create()
               }
               do {
                 let object = try PropertyListDecoder().decode(T.self, from: data)
                 observer(MaybeEvent<T>.success(object))
                 return Disposables.create()
               } catch {
                 observer(.completed)
               return Disposables.create()
               }
              
               
           }.subscribeOn(cacheScheduler)
       }
    func fetchObjects() -> Maybe<[T]> {
        return Maybe<[T]>.create { (observer) -> Disposable in
            guard let directoryURL = self.directoryURL() else {
                observer(.completed)
                return Disposables.create()
            }
            let fileURL = directoryURL
                .appendingPathComponent(FileNames.objectsFileName)
            
            guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: fileURL.path) as? Data else {
                observer(.completed)
                return Disposables.create()
             }
            do {
              let objects = try PropertyListDecoder().decode([T].self, from: data)
                observer(MaybeEvent.success(objects))
                return Disposables.create()
            } catch {
               observer(.completed)
            return Disposables.create()
            }
            
            
          
        }.subscribeOn(cacheScheduler)
    }
    
    private func directoryURL() -> URL? {
        return FileManager.default
            .urls(for: .documentDirectory,
                  in: .userDomainMask)
            .first?
            .appendingPathComponent(path)
    }
    
    private func createDirectoryIfNeeded(at url: URL) {
        do {
            try FileManager.default.createDirectory(at: url,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
        } catch {
            print("Cache Error createDirectoryIfNeeded \(error)")
        }
    }
}
