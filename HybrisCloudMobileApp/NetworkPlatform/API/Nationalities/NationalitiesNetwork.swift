//
//  NationalitiesNetwork.swift
//  NetworkPlatform
//
//  Created by khalil on 4/2/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//


import Domain
import RxSwift

public final class NationalitiesNetwork {
    private let network: Network<NationalitiesModel>

    init(network: Network<NationalitiesModel>) {
        self.network = network
    }
    
    
    public func fetchNationalities() -> Observable<NationalitiesModel> {
        return network.getItems("rest/v2/foodcrowd-ae/nationalities?fields=FULL")
        
    }
}



