//
//  PaymentDetailsNetwork.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 8/18/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Domain
import RxSwift

public final class PaymentDetailsNetwork {
    private let network: Network<PaymentDetailsResponse>
    
    init(network: Network<PaymentDetailsResponse>){
         self.network = network
     }

//setCartPaymentDetails(trackingId: trackingId)
     public func setCartPaymentDetails(paymentDetials: PaymentDetailsRequest) -> Observable<PaymentDetailsResponse> {
        return network.jsonPostItem("rest/v2/foodcrowd-ae/users/current/carts/current/paymentdetails?fields=FULL", parameters: paymentDetials.dictionary)
     }
    public func setCartPaymentDetails(trackingId: String) -> Observable<PaymentDetailsResponse> {
        return network.urlPostItem("rest/v2/foodcrowd-ae/users/current/carts/current/payment/transaction?fields=FULL", parameters: ["data":trackingId])
    }
 }

public final class PaymnetInfoNetwork {
    private let network: Network<PaymentInfo>
    
    init(network: Network<PaymentInfo>){
        self.network = network
    }
    
    
    public func getCartPaymentInfo() -> Observable<PaymentInfo> {
        return network.getItems("rest/v2/foodcrowd-ae/users/current/carts/current/payment/info?fields=FULL")
    }
}

