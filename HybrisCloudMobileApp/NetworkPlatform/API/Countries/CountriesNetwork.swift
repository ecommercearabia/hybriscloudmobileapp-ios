//
//  CountriesNetwork.swift
//  NetworkPlatform
//
//  Created by Admin on 2020-08-03.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Domain
import RxSwift

public final class CountriesNetwork {
    private let network: Network<CountriesModel>
    
    init(network: Network<CountriesModel>) {
        self.network = network
    }
    
    public func fetchCountries() -> Observable<CountriesModel> {
        return network.getItems("rest/v2/foodcrowd-ae/countries?fields=FULL")

    }
    
    public func fetchCountriesShipping() -> Observable<CountriesModel> {
        return network.getItems("rest/v2/foodcrowd-ae/countries?fields=FULL&type=SHIPPING")

    }
    
    public func fetchCountriesMobile() -> Observable<CountriesModel>{
        return network.getItems("rest/v2/foodcrowd-ae/countries?fields=FULL&type=MOBILE")
    }
}

public final class RegionsNetwork {
    private let network: Network<RegionsModel>
    
    init(network: Network<RegionsModel>) {
        self.network = network
    }
    
    public func fetchRegions(countyIsoCode: String) -> Observable<RegionsModel> {
        return network.getItems("foodcrowd-ae/countries/\(countyIsoCode)/regions")
    }
}

public final class CitiesNetwork  {
    private let network: Network<CitiesModel>
     
     init(network: Network<CitiesModel>) {
         self.network = network
     }
     
     public func fetchCities(countryIsoCode: String) -> Observable<CitiesModel> {
         return network.getItems("rest/v2/foodcrowd-ae/countries/\(countryIsoCode)/cities?fields=FULL")
     }
    
}
