//
//  AddressesNetwork.swift
//  NetworkPlatform
//
//  Created by Admin on 2020-08-03.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Domain
import RxSwift

public final class AddressesNetwork {
    private let network: Network<AddressesModel>
    private let networkAddAddress: Network<Address>

    init(network: Network<AddressesModel>,networkAddAddress: Network<Address>) {
        self.network = network
        self.networkAddAddress = networkAddAddress
    }
    
    
    public func fetchAddresses(userId: String) -> Observable<AddressesModel> {
        
        return network.getItems("rest/v2/foodcrowd-ae/users/\(userId)/addresses?fields=FULL")
  }
    
    

//
//    public func getAddress(userId: String, addressId: String) -> Observable<Address> {
//        return network.getItem("foodcrowd-ae/users/\(userId)/addresses", itemId: addressId)
//    }
//
    public func addAddress(userId: String, address: Address) -> Observable<Address> {
        return networkAddAddress.jsonPostItem("rest/v2/foodcrowd-ae/users/\(userId)/addresses?fields=FULL", parameters: address.dictionary)
    }

    public func deleteAddress(userId: String, addressId: String) -> Observable<Void> {
        return network.deleteItem("rest/v2/foodcrowd-ae/users/\(userId)/addresses", itemId: addressId)
    }

    public func updateAddress(userId: String, addressId: String, address: Address) -> Observable<Void> {
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: address.dictionary,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                       encoding: .ascii)
                    print(theJSONText!)
        }
        
        return network.setItemWithItemId("rest/v2/foodcrowd-ae/users/\(userId)/addresses", itemId: addressId, parameters: address.dictionary)
    }
//
//    public func verifyAddress(userId: String, addressId: String, address: Address) -> Observable<Address> {
//        return network.postItem("foodcrowd-ae/users/\(userId)/addresses/verification", parameters: address.dictionary)
  // }
}
