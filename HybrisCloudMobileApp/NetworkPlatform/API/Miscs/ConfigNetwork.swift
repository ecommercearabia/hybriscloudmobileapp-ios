//
//  ConfigNewwork.swift
//  NetworkPlatform
//
//  Created by khalil on 4/2/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Domain
import RxSwift

public final class ConfigNetwork {
    private let network: Network<ConfigModel>

    init(network: Network<ConfigModel>) {
        self.network = network
    }
    

    public func fetchConfig() -> Observable<ConfigModel> {
   return  network.getItems("rest/v2/foodcrowd-ae/config?fields=FULL")
        
    }

}
