//
//  TitlesNetwork.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 7/21/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Domain
import RxSwift

public final class MiscsNetwork {
    private let network: Network<Titles>

    init(network: Network<Titles>) {
        self.network = network
    }
    

    public func fetchTitles() -> Observable<Titles> {
   return  network.getItems("rest/v2/foodcrowd-ae/titles?fields=FULL")
        
    }


}
