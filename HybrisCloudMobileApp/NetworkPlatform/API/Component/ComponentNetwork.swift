//
//  ComponentNetwork.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 7/27/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Domain
import RxSwift

public final class ComponentNetwork {
    private let network: Network<Component>
    
    init(network: Network<Component>) {
        self.network = network
    }
    public func fetchComponent(componentId: String) -> Observable<Component> {
        return network.getItems("rest/v2/foodcrowd-ae/cms/components/\(componentId)?fields=FULL")
      }


}
