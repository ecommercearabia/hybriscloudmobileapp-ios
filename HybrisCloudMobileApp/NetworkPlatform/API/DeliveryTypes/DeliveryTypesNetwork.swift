//
//  DeliveryTypesNetwork.swift
//  NetworkPlatform
//
//  Created by khalil anqawi on 12/11/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Domain
import RxSwift
import KeychainSwift

public final class DeliveryTypesNetwork {
    private let network: Network<DeliveryTypesModel>
    private let networkDeliveryType: Network<String>
    private let networkSetDeliveryType : Network<CartModel>
    init(network: Network<DeliveryTypesModel> , networkDeliveryType: Network<String> , networkSetDeliveryType : Network<CartModel>) {
        self.network = network
        self.networkDeliveryType = networkDeliveryType
        self.networkSetDeliveryType = networkSetDeliveryType
        
    }
    

    public func fetchDeliveryTypes() -> Observable<DeliveryTypesModel> {
        return network.getItems("rest/v2/foodcrowd-ae/users/current/carts/current/deliverytypes?fields=FULL")
        
    }


    public func fetchDeliveryType() -> Observable<String> {
        return networkDeliveryType.getItems("rest/v2/foodcrowd-ae/users/current/carts/current/deliverytype?fields=FULL")
        
    }
    


    
    public func setDeliveryType(deliveryType: String) -> Observable<CartModel> {
        return networkSetDeliveryType.setItem("rest/v2/foodcrowd-ae/users/current/carts/current/deliverytype?deliveryTypeCode=\(deliveryType)&fields=FULL", parameters: [ : ])
        
    }
    
    
}
