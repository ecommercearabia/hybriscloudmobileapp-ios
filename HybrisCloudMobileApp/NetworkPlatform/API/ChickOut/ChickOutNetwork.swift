//
//  ChickOutNetwork.swift
//  NetworkPlatform
//
//  Created by ahmadSaleh on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

public final class ChickOutNetwork {
private let network: Network<TimeSlotsModel>

init(network: Network<TimeSlotsModel>) {
    self.network = network
}
    
    public func setDeliveryAddress(addressId : String) -> Observable<Void> {
        return network.setItem("rest/v2/foodcrowd-ae/users/current/carts/current/addresses/delivery", parameters: ["addressId": addressId])
 
      }
 
    public func getTimeSlots() -> Observable<TimeSlotsModel> {
         return network.getItems("rest/v2/foodcrowd-ae/users/current/carts/current/timeSlot?fields=FULL")
      }
 

    public func setTimeSlots(cartId : String , timeSlotInfos : SetTimeSlotModel) -> Observable<Void> {
        return network.josnPostItem("rest/v2/foodcrowd-ae/users/current/carts/\(cartId)/timeSlot?fields=FULL", parameters: timeSlotInfos.dictionary)
        
    }
 
    
    
}


public final class DeliveryModeNetwork {
private let network: Network<DeliveryModeModel>

init(network: Network<DeliveryModeModel>) {
    self.network = network
}
    
    func getDeliveryModes(cartId : String) -> Observable<DeliveryModeModel> {
        return network.getItems("rest/v2/foodcrowd-ae/users/\(cartId)/carts/\(cartId)/deliverymode?fields=FULL")
    }
    
    func setDeliveryMode(deliveryModeId : String , cartId : String) -> Observable<Void> {
        return network.setItem("rest/v2/foodcrowd-ae/users/current/carts/current/deliverymode", parameters: ["deliveryModeId":deliveryModeId])
    }
 
}
 

public final class StoreCreditCartNetwork {
private let network: Network<StoreCreditModes>
private let networkStoreCredit: Network<StoreCreditAvailableAmountModel>

init(network: Network<StoreCreditModes> , networkStoreCredit: Network<StoreCreditAvailableAmountModel>) {
    self.network = network
    self.networkStoreCredit = networkStoreCredit
}
    
    func getStoreCreditModes(cartId : String , userId : String) -> Observable<StoreCreditModes> {
        return network.getItems("rest/v2/foodcrowd-ae/users/\(userId)/carts/\(cartId)/storecreditmodes?fields=FULL")
     }
    func setStoreCreditMode(amount : Double ,cartId : String , storeCreditModeTypeCode : String , userId : String) -> Observable<Void> {
        var url = "rest/v2/foodcrowd-ae/users/\(userId)/carts/\(cartId)/storecreditmodes?fields=FULL&storeCreditModeTypeCode=\(storeCreditModeTypeCode)"
        if amount > 0.0 {
            url = "\(url)&amount=\(amount)"
        }
        return network.setItemWithoutResponse(url)
     }
    
    func getStoreCreditAvailableAmount(userId : String) -> Observable<StoreCreditAvailableAmountModel> {
        networkStoreCredit.getItems("rest/v2/foodcrowd-ae/users/\(userId)/storecredit/amount?fields=FULL")
    }

}


 
     
public final class ApplePayNetwork {
    private let network: Network<ApplePayModel>
    init(network : Network<ApplePayModel> ) {
        self.network = network
    }
    func setApplePay( paymentOption : String , cardType : String ,cardName : String ,  paymentData : String , appleCardType : String ,  amount : String , currency : String  , orderId : String  , userId : String  , cartId : String) -> Observable<ApplePayModel> {
        
       return network.jsonPostItem("rest/v2/foodcrowd-ae/users/\(userId)/carts/\(cartId)/payment/transaction/applepay?fields=FULL", parameters: ["amount":amount,"appleCardType":appleCardType,"cardName":cardName,"cardType":cardType,"currency":currency,"orderId":orderId,"paymentData":paymentData,"paymentOption":paymentOption])
    }
}

