//
//  PaymentNetwork.swift
//  NetworkPlatform
//
//  Created by ahmadSaleh on 8/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

 
import Foundation
import Domain
import RxSwift

public final class PaymentNetwork {
    
private let network: Network<PaymentModesModel>
private let networkSet: Network<SetPaymentModeResponse>

init(network: Network<PaymentModesModel>,networkSet: Network<SetPaymentModeResponse>) {
    self.network = network
    self.networkSet = networkSet
}
    
    public func getPaymentModes() -> Observable<PaymentModesModel> {
        return network.getItems("rest/v2/foodcrowd-ae/users/current/carts/current/paymentmodes?fields=FULL&includeApplePay=true")
    }
 
    
    
    public func setPaymentModes(paymentModeCode : String) -> Observable<SetPaymentModeResponse> {
        return networkSet.setItemWithResponse("rest/v2/foodcrowd-ae/users/current/carts/current/paymentmodes?fields=FULL&paymentModeCode=\(paymentModeCode)")
    }

    
    
}

 
