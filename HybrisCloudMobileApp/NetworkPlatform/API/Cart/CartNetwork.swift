//
//  CartNetwork.swift
//  NetworkPlatform
//
//  Created by ahmadSaleh on 8/4/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

public final class CartNetwork {
    private let network: Network<CartModel>

    init(network: Network<CartModel>) {
        self.network = network
    }
    
    public func fetchCart( cartId : String  , userId : String) -> Observable<CartModel> {
       
        print("======= fetchCart =======")
        return network.getItems("rest/v2/foodcrowd-ae/users/\(userId)/carts/\(cartId)?fields=FULL")
      }
    
    public func daleteIntry(itemId : String,cartId : String  , userId : String) -> Observable<Void> {
        return network.deleteItem("rest/v2/foodcrowd-ae/users/\(userId)/carts/\(cartId)/entries", itemId: itemId)
      }


    public func createCart(userId : String) -> Observable<CartModel> {
        print("======= createCart =======")
        return network.urlPostItem("rest/v2/foodcrowd-ae/users/\(userId)/carts?fields=FULL", parameters: [:])
         }

    public func mergeCart(oldCartGUID:String, currentCartGUID:String) -> Observable<CartModel> {
           print("======= merge =======")
        return network.urlPostItem("rest/v2/foodcrowd-ae/users/current/carts?fields=FULL", parameters: ["oldCartId":oldCartGUID  , "toMergeCartGuid" : currentCartGUID])
            }
    
    public func appliesVoucher(cartId: String, userId: String, voucherId: String) -> Observable<Void> {
           print("======= appliesVoucher =======")
        return network.josnPostItem2("rest/v2/foodcrowd-ae/users/\(userId)/carts/\(cartId)/vouchers/?voucherId=\(voucherId)", parameters: [:])
    }
    
    public func deletesVoucher(cartId: String, userId: String, voucherId: String) -> Observable<Void> {
           print("======= deletesVoucher =======")
        return network.deleteItem("rest/v2/foodcrowd-ae/users/\(userId)/carts/\(cartId)/vouchers/\(voucherId)")
    }

    

}

public final class CartAddProductNetwork {
    private let network: Network<AddedToCartModel>

    init(network: Network<AddedToCartModel>) {
        self.network = network
    }
    
    public  func addAllToCart(userId: String, cartId: String, wishlistPK: String) -> Observable<AddedToCartModel> {
        return network.setItemWithResponse("rest/v2/foodcrowd-ae/users/\(userId)/carts/\(cartId)/addwishlistproducttocart/\(wishlistPK)?fields=FULL")

    }

    public func addToCart( userId : String , cartId : String , entry:AddToCartProductModel) -> Observable<AddedToCartModel> {
       
        
        return network.jsonPostItem("rest/v2/foodcrowd-ae/users/\(userId)/carts/\(cartId)/entries?fields=FULL&salesApp=IOS", parameters: entry.dictionary)
      }
    
}

public final class CartMeargeNetwork {
    private let networkMeargeCart: Network<CartMargeModel>

    init(networkMeargeCart : Network<CartMargeModel>) {
         self.networkMeargeCart = networkMeargeCart
    }
    
    
 
    public func meargeCart(userId : String , oldCartGuid: String, toMergeCartGuid: String) -> Observable<CartMargeModel> {
        return networkMeargeCart.getItems("rest/v2/foodcrowd-ae/users/\(userId)/carts?fields=FULL&oldCartId=\(oldCartGuid)&toMergeCartGuid=\(toMergeCartGuid)")
      }

}


public final class CartQuantityNetwork {
    private let network: Network<CartQuantityUpdate>

    init(network : Network<CartQuantityUpdate>) {
         self.network = network
    }
 
    public func updateCartQuantity(entryNumber : String , entry : CartEntry , userId : String , cartId : String) -> Observable<CartQuantityUpdate>{
         return network.setItem("/rest/v2/foodcrowd-ae/users/\(userId)/carts/\(cartId)/entries/\(entryNumber)", parameters: entry.dictionary)
      }

}


public final class CartsNetwork {
    private let network: Network<CartsModel>

    init(network : Network<CartsModel>) {
         self.network = network
    }
 
    public func getCarts(userId : String) -> Observable<CartsModel>{
        return network.getItems("rest/v2/foodcrowd-ae/users/\(userId)/carts?fields=FULL")
      }

}

public final class ValidationCartNetwork {
    private let network: Network<ValidationCartErrors>

    init(network : Network<ValidationCartErrors>) {
         self.network = network
    }
 
    public func validationCart(userId : String , cartId : String) -> Observable<ValidationCartErrors>{
        return network.urlPostItem("rest/v2/foodcrowd-ae/users/\(userId)/carts/\(cartId)/verification?fields=FULL", parameters: [:])
        
    }
 }

public final class BankOffersCartNetwork {
   private let network: Network<BankOffers>

   init(network : Network<BankOffers>) {
        self.network = network
   }

   public func bankOffersCart() -> Observable<BankOffers>{
    return network.getItems("rest/v2/foodcrowd-ae/bankoffers?fields=FULL")
       
   }
}



