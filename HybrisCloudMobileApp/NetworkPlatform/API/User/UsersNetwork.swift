//
//  RegistersNetwork.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 7/21/20.
//  Copyright © 2020 Erabia. All rights reserved.
//



import Domain
import RxSwift
import RxCocoa
import OAuthSwift
import KeychainSwift

public final class UsersNetwork {
    private let network: Network<UserData>
    private let networkStoreCreditAmount: Network<StoreCreditAmount>
    private let networkStoreCreditHistroy: Network<StoreCreditHistroy>
    private let networkSendOtp: Network<SendOtpCode>
    private let networkVerifyOtp: Network<VerifyOtpCode>
    private let networkNewLogin: Network<NewLoginModel>
    
    private let networkLoginWithFaceID : Network<LoginFaceID>
    init(network: Network<UserData> ,
         networkStoreCreditAmount: Network<StoreCreditAmount> ,
         networkStoreCreditHistroy: Network<StoreCreditHistroy>,
         networkSendOtp: Network<SendOtpCode>,
         networkVerifyOtp: Network<VerifyOtpCode>,
         networkLoginWithFaceId :  Network<LoginFaceID> ,
         networkNewLogin: Network<NewLoginModel>
    ) {
        self.network = network
        self.networkStoreCreditAmount = networkStoreCreditAmount
        self.networkStoreCreditHistroy = networkStoreCreditHistroy
        self.networkSendOtp = networkSendOtp
        self.networkVerifyOtp = networkVerifyOtp
        self.networkNewLogin = networkNewLogin
        networkLoginWithFaceID = networkLoginWithFaceId
    }
    
    public func register(user: UserSignup) -> Observable<UserData> {
        return network.jsonPostItem("rest/v2/foodcrowd-ae/users?fields=FULL", parameters: user.dictionary)
    }
    
    func setFirebaseToken(  mobileToken : String) -> Observable<Void>{
        return network.patchItemWithoutResponse("rest/v2/foodcrowd-ae/users/current/mobile-token?mobileToken=\(mobileToken)" )
    }
    
       
    func forgetPassword(forgetPasswordParam: String) -> Observable<Void>{
        
        
        return network.urlPostItemWithoutResponse("rest/v2/foodcrowd-ae/forgottenpasswordtokens?userId=\(forgetPasswordParam)", parameters: [:])
        
        
    }
    
    
    let oauth2 = OAuth2Swift(consumerKey: "occ_mobile", consumerSecret: "Erabia@123", authorizeUrl: "\(Environment.shared.apiEndpoint?.rawValue ?? "")/authorizationserver/oauth/token",accessTokenUrl: "\(Environment.shared.apiEndpoint?.rawValue ?? "")/authorizationserver/oauth/token", responseType: "")
    
    public func setOrUpdateFaceId(signatureId : String , userId : String) -> Observable<Void> {
        
        return network.urlPostItemWithoutResponse("rest/v2/foodcrowd-ae/users/\(userId)/signature?signatureId=\(signatureId)", parameters: [:])
    }
    
    public func loginWithFaceId(signatureId: String , email : String)-> Observable<LoginFaceID>{
        
        
        
        return Observable.create { observer -> Disposable in
            self.networkLoginWithFaceID.urlPostItemForFaceId("rest/v2/foodcrowd-ae/signature/auth?email=\(email)&signatureId=\(signatureId)", parameters: [:]).subscribe(onNext: { (credentialResponse) in
                let credential = OAuthCredential(accessToken: credentialResponse.accessToken ?? "",
                                                 refreshToken: credentialResponse.refreshToken ?? "",
                                                 expiration: Date(timeIntervalSince1970: TimeInterval(credentialResponse.expiresIn ?? 0)), isUserCreintials: true)
                KeychainSwift().set(credential.serialize(), forKey: "usercredentials")
                KeychainSwift().set("current", forKey: "user")
                interceptor.credential = credential
                observer.onNext(credentialResponse)

            }, onError: { (error) in
//                KeychainSwift().delete("usercredentials")
                KeychainSwift().set("anonymous", forKey: "user")
                
                observer.onError(error)
                
            })
        }
        
    }
    
    
    
    public func login(user: UserLogin) -> Observable<Void> {
       // let loginSubject = PublishSubject<Void>()
       
//        curl -X POST "https://api-stg.foodcrowd.com/rest/v2/foodcrowd-ae/login/token?identifier=0776627658&password=000000" -H "accept: application/json" -H "clientId: mobile_android" -H "clientSecret: secret"
//
      
       
        return Observable.create { observer -> Disposable in
            self.networkNewLogin.urlPostItemForLogin("rest/v2/foodcrowd-ae/login/token?identifier=\(user.uid ?? "")&password=\(user.password ?? "")", parameters: [:] ).subscribe(onNext: { (VerifyOtpCode) in
                let credential = OAuthCredential(accessToken: VerifyOtpCode.accessToken ?? "",
                                                 refreshToken: VerifyOtpCode.refreshToken ?? "",
                                                 expiration: Date(timeIntervalSince1970: TimeInterval(VerifyOtpCode.expiresIn ?? 0)), isUserCreintials: true)
                KeychainSwift().set(credential.serialize(), forKey: "usercredentials")
                KeychainSwift().set("current", forKey: "user")
               
                interceptor.credential = credential
                observer.onNext(())
            }, onError: { (error) in
                KeychainSwift().delete("usercredentials")
                KeychainSwift().set("anonymous", forKey: "user")
                
                observer.onError(error)
                
            })
        }
        
     //  oauth2.authorize(username: user.uid ?? "", password: user.password ?? "", scope: "") { (result) in
//            switch(result){
//
//            case .success((let credential, _, _)):
//
//                let credential = OAuthCredential(accessToken: credential.oauthToken,refreshToken: credential.oauthRefreshToken,expiration: credential.oauthTokenExpiresAt ?? Date(), isUserCreintials: true)
//                KeychainSwift().set(credential.serialize(), forKey: "usercredentials")
//                KeychainSwift().set("current", forKey: "user")
//                interceptor.credential = credential
//                loginSubject.onNext(())
//            case .failure(let error):
//                KeychainSwift().delete("usercredentials")
//                KeychainSwift().set("anonymous", forKey: "user")
//
//                loginSubject.onError(error)
//
//            }
//        }
       // return loginSubject.asObservable()
    }
    
    
    
    func logout() -> Observable<Void> {
        
        
        let logOutSubject = PublishSubject<Void>()
        
        
        oauth2.authorize(deviceToken: "123", grantType: "client_credentials") { (result) in
            switch result {
            
            case .success((let credential, let response, let parameters)):
                let credential = OAuthCredential(accessToken: credential.oauthToken,
                                                 refreshToken: credential.oauthRefreshToken,
                                                 expiration: credential.oauthTokenExpiresAt ?? Date(), isUserCreintials: false)
                
                KeychainSwift().set(credential.serialize(), forKey: "usercredentials")
                KeychainSwift().set("anonymous", forKey: "user")
                interceptor.credential = credential
                logOutSubject.onNext(())
            case .failure(let error):
                KeychainSwift().delete("usercredentials")
                print("Authorization was canceled or went wrong: \(error)")
                KeychainSwift().set("anonymous", forKey: "user")// error will not be nil
                logOutSubject.onCompleted()
                interceptor.credential = OAuthCredential(accessToken: "", refreshToken: "", expiration: Date(), isUserCreintials: false)
                
            }
        }
        
        
        return logOutSubject
    }
    
    
    public func changePassword(user: ChangePasswordRequest)-> Observable<Void> {
        return network.setItem("rest/v2/foodcrowd-ae/users/current/password", parameters: user.dictionary)
    }
    
    public func getCustomerProfile(userId: String)-> Observable<UserData> {
        return network.getItems("rest/v2/foodcrowd-ae/users/\(userId)?fields=FULL")
    }
    
    public func updateCustomerProfile(userId : String,user:UserData) -> Observable<Void> {
        let mobileCountry : [String : Any] = [
            "isdcode": user.mobileCountry?.isdcode ?? "",
            "isocode": user.mobileCountry?.isocode ?? "",
            "name": user.mobileCountry?.name ?? ""
        ]
 
        
        
        let dic : [String:Any] = [
            
            "customerId": user.customerID ?? "",
            "displayUid": user.displayUid ?? "",
            "firstName": user.firstName ?? "",
            "lastName": user.lastName ?? "",
            "mobileCountry": mobileCountry,
            "mobileNumber": user.mobileNumber ?? "",
            "name": user.name ?? "",
            "title": user.title ?? "" ,
            "titleCode": user.titleCode ?? "",
            "type": user.type ?? "",
            "uid": user.uid ?? ""
        ]
        
        let jsonData = try! JSONEncoder().encode(user)
        let jsonString = String(data: jsonData, encoding: .utf8)!
print(jsonString)
        
        return network.setItemRowJson("rest/v2/foodcrowd-ae/users/\(userId)?",parameters: user.dictionary)
    }
    
    
    public func getAmountStoreCredit(userId: String)-> Observable<StoreCreditAmount> {
        return networkStoreCreditAmount.getItems("rest/v2/foodcrowd-ae/users/\(userId)/storecredit/amount?fields=FULL")
    }
    
    
    public func getHistoryStoreCredit(userId: String)-> Observable<StoreCreditHistroy> {
        return networkStoreCreditHistroy.getItems("rest/v2/foodcrowd-ae/users/\(userId)/storecredit/history?fields=FULL")
    }
    
    
    func updateEmailAddress(newLogin : String, password : String , userId : String) -> Observable<Void>{
        return network.setItemWithoutResponse("/rest/v2/foodcrowd-ae/users/\(userId)/login?newLogin=\(newLogin)&password=\(password)")
    }
    
    func sendOtpCode(email: String) -> Observable<SendOtpCode> {
        networkSendOtp.urlPostItem("rest/v2/foodcrowd-ae/otp/send/login?email=\(email)", parameters: [:])
    }
    
    func verifyOtpCode(code : String , email: String , name : String) -> Observable<Void> {
        
        return Observable.create { observer -> Disposable in
            self.networkVerifyOtp.urlPostItem("rest/v2/foodcrowd-ae/otp/verify/login?code=\(code)&email=\(email)", parameters: [:]).subscribe(onNext: { (VerifyOtpCode) in
                let credential = OAuthCredential(accessToken: VerifyOtpCode.accessToken ?? "",
                                                 refreshToken: VerifyOtpCode.refreshToken ?? "",
                                                 expiration: Date(timeIntervalSince1970: TimeInterval(VerifyOtpCode.expiresIn ?? 0)), isUserCreintials: true)
                KeychainSwift().set(credential.serialize(), forKey: "usercredentials")
                KeychainSwift().set("current", forKey: "user")
                interceptor.credential = credential
                observer.onNext(())
            }, onError: { (error) in
                KeychainSwift().delete("usercredentials")
                KeychainSwift().set("anonymous", forKey: "user")
                
                observer.onError(error)
                
            })
        }
        
    }
    
    
    
}


