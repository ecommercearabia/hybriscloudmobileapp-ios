//
//  ConsentsNetwork.swift
//  NetworkPlatform
//
//  Created by khalil on 9/9/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Domain
import RxSwift


public final class ConsentsNetwork {
    
    private let network : Network<Consents>
    private let networkAdd : Network<AddConsent>
    

    init(network : Network<Consents>,networkAdd : Network<AddConsent>) {
        self.network = network
        self.networkAdd = networkAdd
      
    }
    
    public func getConsentsList ()-> Observable<Consents>{
        return network.getItems("rest/v2/foodcrowd-ae/users/current/consenttemplates?fields=FULL")
    }
    

    
    func addConsent(consentTemplateId : String , version : Int) -> Observable<AddConsent>{
        return networkAdd.urlPostItem("rest/v2/foodcrowd-ae/users/current/consents?consentTemplateId=\(consentTemplateId)&consentTemplateVersion=\(version)", parameters: [:])
    }
    
    
   
    func deleteConsent(code: String) ->Observable<Void>{
        return network.deleteItem("rest/v2/foodcrowd-ae/users/current/consents/\(code)")
    }
    

   
}
