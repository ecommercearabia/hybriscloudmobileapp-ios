//
//  UsersNetwork.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 7/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Domain
import RxSwift

public final class PagesNetwork {
    private let network: Network<Page>

    init(network: Network<Page>) {
        self.network = network
    }
    
 /*  public func fetchPages() -> Observable<[Page]> {
        return network.getItems("rest/v2/foodcrowd-ae/cms/pages")
        
    }*/
    
     public func fetchPage(pageId: String) -> Observable<Page> {
        return network.getItem("rest/v2/foodcrowd-ae/cms/pages?", itemId: pageId)
    }

    public func createPage(page: Page) -> Observable<Page> {
        return network.jsonPostItem("rest/v2/foodcrowd-ae/cms/pages", parameters: page.dictionary)
    }

    public func deletePage(pageId: String) -> Observable<Void> {
        return network.deleteItem("rest/v2/foodcrowd-ae/cms/pages", itemId: pageId)
    }
}
