//
//  UserMessagesNetwork.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 9/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

public final class UserMessagesNetwork {
    private let network: Network<UserMessages>

    init(network: Network<UserMessages>) {
        self.network = network
    }
    
    public func fetchUserMessages() -> Observable<UserMessages> {
       
        return network.getItems("rest/v2/foodcrowd-ae/users/current/notifications/sitemessages")
   
      }
    
}
