//
//  LoyaltyPointsNetwork.swift
//  NetworkPlatform
//
//  Created by khalil anqawi on 26/05/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Domain
import RxSwift
import KeychainSwift

public final class LoyaltyPointsNetwork {
    private let network: Network<LoyaltyPointsModel>
    private let networkAvailableLoyaltyPoints: Network<Double>
    
    init(network: Network<LoyaltyPointsModel> , networkAvailableLoyaltyPoints: Network<Double>) {
        self.network = network
        self.networkAvailableLoyaltyPoints = networkAvailableLoyaltyPoints
        
    }
    public func fetchLoyaltyPoints() -> Observable<LoyaltyPointsModel> {
        return network.getItems("rest/v2/foodcrowd-ae/users/current/loyaltypoint/history?fields=FULL")
        
    }
    public func fetchAvailableLoyaltyPointsModel() -> Observable<Double> {
        return networkAvailableLoyaltyPoints.getItems("rest/v2/foodcrowd-ae/users/current/loyaltypoint?fields=FULL")
        
    }
}
