//
//  SavedCardsNetwork.swift
//  NetworkPlatform
//
//  Created by walaa omar on 26/12/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Domain
import RxSwift

public final class SavedCardsNetwork {
    private let network: Network<SavedCardsModel>

    init(network: Network<SavedCardsModel>) {
        self.network = network
    }
    
    
    public func fetchSavedCards(userId: String) -> Observable<SavedCardsModel> {
        return network.getItems("rest/v2/foodcrowd-ae/users/\(userId)/customer-payment-options?fields=FULL")
        
    }

    public func deleteSavedCards(userId: String, savedCardId: String) -> Observable<Void> {
        return network.deleteItem("rest/v2/foodcrowd-ae/users/\(userId)/customer-payment-options", itemId: savedCardId)
        
    }
    

}
