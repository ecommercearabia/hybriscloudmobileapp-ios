//
//  ShipmentTypesNetwork.swift
//  NetworkPlatform
//
//  Created by khalil on 12/2/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Domain
import RxSwift
import KeychainSwift
public final class ShipmentTypesNetwork {
    private let network: Network<ShipmentTypesModel>
    private let networkSelectedShipment: Network<ShipmentSelctedShipmentTypesModel>
    private let networkGetType: Network<CurrentShipmentTypeModel>
    var userId =  KeychainSwift().get("user") == "current" ? "current" : "anonymous"
    
    
    var cartId = ""
    init(network: Network<ShipmentTypesModel> ,  networkSelectedShipment: Network<ShipmentSelctedShipmentTypesModel> ,  networkGetType: Network<CurrentShipmentTypeModel>) {
        self.network = network
        self.networkSelectedShipment = networkSelectedShipment
        self.networkGetType = networkGetType
    }
   
    
    public func getShipmentTypes() -> Observable<ShipmentTypesModel> {
        if KeychainSwift().get("user") != "current" {
            cartId =  KeychainSwift().get("guid") ?? ""
        } else {
        cartId = "current"
        }
        return network.getItems("rest/v2/foodcrowd-ae/users/\( KeychainSwift().get("user") ?? " ")/carts/\(cartId)/shipment-types")
        
    }
    
  
    public func selectedShipmentType(code: String) -> Observable<ShipmentSelctedShipmentTypesModel> {
        if KeychainSwift().get("user") != "current" {
            cartId =  KeychainSwift().get("guid") ?? ""
        } else {
        cartId = "current"
        }
        
        return networkSelectedShipment.setItem("rest/v2/foodcrowd-ae/users/\( KeychainSwift().get("user") ?? " ")/carts/\(cartId)/shipment-types?code=\(code)&fields=FULL", parameters: [:])
        
    }
    
    public func getShipmentTypes() -> Observable<CurrentShipmentTypeModel> {
        if KeychainSwift().get("user") != "current" {
            cartId =  KeychainSwift().get("guid") ?? ""
        } else {
        cartId = "current"
        }
        return networkGetType.getItems("rest/v2/foodcrowd-ae/users/\( KeychainSwift().get("user") ?? " ")/carts/\(cartId)/shipment-types/current")
        
    }

}
