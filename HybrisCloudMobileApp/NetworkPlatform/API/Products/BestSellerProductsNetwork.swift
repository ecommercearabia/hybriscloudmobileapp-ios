//
//  BestSellerProductsNetwork.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 8/6/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Domain
import RxSwift

public final class BestSellerProductsNetwork {
    private let network: Network<BestSellerProducts>
    
    init(network: Network<BestSellerProducts>) {
        self.network = network
    }

    public func fetchBestSellerProduct(productCode: String) -> Observable<BestSellerProducts> {
      return network.getItems("rest/v2/foodcrowd-ae/cms/components/\(productCode)?fields=FULL")
    }
}

