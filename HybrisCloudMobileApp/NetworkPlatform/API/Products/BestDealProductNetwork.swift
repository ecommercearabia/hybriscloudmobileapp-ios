//
//  BestDealProductNetwork.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Domain
import RxSwift

public final class BestDealProductNetwork {
    private let network: Network<ProductDetails>
    
    init(network: Network<ProductDetails>) {
        self.network = network
    }

    public func fetchBestDealProduct(productCode: String) -> Observable<ProductDetails> {
      return network.getItems("rest/v2/foodcrowd-ae/products/\(productCode)?fields=FULL")
    }
}

