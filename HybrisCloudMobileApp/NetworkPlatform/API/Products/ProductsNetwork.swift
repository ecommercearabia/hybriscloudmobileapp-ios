//
//  ProductsNetwork.swift
//  NetworkPlatform
//
//  Created by khalil on 8/2/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Domain
import RxSwift


public final class ProductsNetwork {
    
    
    private let network : Network<ProductSearch>
    
    init(network : Network<ProductSearch>) {
        self.network = network
    }
    
    public func searchProducts(categoryID:String,currentPage : Int , pageSize : Int ,query: String, sort : String) -> Observable<ProductSearch>{
        return network.getItems("rest/v2/foodcrowd-ae/products/search/\(categoryID)?currentPage=\(currentPage)&fields=FULL&pageSize=\(pageSize)&query=\(query)&sort=\(sort)")
    }
}




