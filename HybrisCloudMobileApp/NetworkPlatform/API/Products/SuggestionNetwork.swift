//
//  SuggestionNetwork.swift
//  NetworkPlatform
//
//  Created by ahmadSaleh on 12/31/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Domain
import RxSwift

public final class SuggestionNetwork {
    private let network: Network<SuggestionModel>
    
    init(network: Network<SuggestionModel>) {
        self.network = network
    }

    public func suggestion(max : String, term: String) -> Observable<SuggestionModel> {
      return network.getItems("rest/v2/foodcrowd-ae/products/suggestions?fields=FULL&max=\(max)&term=\(term)")
    }
}

