//
//  WishListNetwork.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 8/26/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

public final class WishListNetwork {
    private let network: Network<WishList>

    init(network: Network<WishList>) {
        self.network = network
    }
    
    public func fetchWishList() -> Observable<WishList> {
       
        return network.getItems("rest/v2/foodcrowd-ae/users/current/wishlists/default?fields=FULL")
   
      }
    
    public func deleteWishListIteam(deleteIteamData: AddDeleteWishListItemRequest) -> Observable<Void> {
        
        return network.deleteItem("rest/v2/foodcrowd-ae/users/current/wishlists/\(deleteIteamData.wishlistPK ?? "")/\(deleteIteamData.productCode ?? "")")
        
       }
    
    public func addWishListIteam(iteamData: AddDeleteWishListItemRequest) -> Observable<Void> {
        
        return network.setItemWithoutResponse("/rest/v2/foodcrowd-ae/users/current/wishlists/\(iteamData.wishlistPK ?? "")/\(iteamData.productCode ?? "")")
        
       }
}

