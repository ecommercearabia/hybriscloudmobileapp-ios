//
//  ReferralCodeNetwork.swift
//  NetworkPlatform
//
//  Created by admin on 20/06/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift

public final class ReferralCodeNetwork {
    private let network: Network<ReferralCode>
    
    init(network: Network<ReferralCode>) {
        self.network = network
    }
    
    //MARK: Fetch Referral Code
    public func fetchReferralCode() -> Observable<ReferralCode> {
        return network.getItems("rest/v2/foodcrowd-ae/users/current/referral-code?fields=FULL")
    }
    
    // MARK: Generate Referral Code
    public func generateReferralCode() -> Observable<ReferralCode> {
        return network.jsonPostItem("rest/v2/foodcrowd-ae/users/current/referral-code/generate?fields=FULL", parameters: [:])
    }
  
    //MARK: Send Subscription Email
    public func sendSubscribeEmail(email:String, inSeperateEmails:Bool) -> Observable<Void> {
        return network.josnPostItem("rest/v2/foodcrowd-ae/users/current/subscribe?emails=\(email)&inSeperateEmails=\(inSeperateEmails)", parameters: [:])
    }
}
