//
//  CatalogNetwork.swift
//  NetworkPlatform
//
//  Created by Ahmad Bader on 8/1/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Domain
import RxSwift

public final class CatalogNetwork {
    private let network: Network<Catalog>
    
    init(network: Network<Catalog>) {
        self.network = network
    }
    public func fetchCatalog(id: String,version: String) -> Observable<Catalog> {
        return network.getItems("rest/v2/foodcrowd-ae/catalogs/\(id)/\(version)?fields=FULL")
      }


}

