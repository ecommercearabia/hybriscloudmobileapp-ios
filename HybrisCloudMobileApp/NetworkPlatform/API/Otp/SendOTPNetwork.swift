//
//  SendOTPNetwork.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Domain
import RxSwift

public final class SendOTPNetwork {
    private let network: Network<SendOTP>
    init(network: Network<SendOTP>) {
        self.network = network
    }
    public func sendOtp(sendOtpParam: SendOtpModel) -> Observable<SendOTP> {
        
        return network.urlPostItem("rest/v2/foodcrowd-ae/otp/send", parameters: sendOtpParam.dictionary)
        
    }
    
}


