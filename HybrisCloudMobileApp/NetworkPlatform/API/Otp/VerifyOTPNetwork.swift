//
//  VerifyOTPNetwork.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Domain
import RxSwift

public final class VerifyOTPNetwork {
    private let network: Network<VerifyOTP>
    init(network: Network<VerifyOTP>) {
        self.network = network
    }
    public func verifyOtp(sendOtpParam: SendOtpModel) -> Observable<VerifyOTP> {
        
        return network.urlPostItem("rest/v2/foodcrowd-ae/otp/verify", parameters: sendOtpParam.dictionary)
        
    }
    
}
