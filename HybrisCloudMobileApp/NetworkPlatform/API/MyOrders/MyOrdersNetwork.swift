//
//  MyOrdersNetwork.swift
//  NetworkPlatform
//
//  Created by khalil on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Domain
import RxSwift

public final class MyOrdersNetwork {
    
    private let network : Network<MyOrders>
    init(network : Network<MyOrders>) {
        
        self.network = network
    }
    
    public func getMyOrders(currentPage : Int , pageSize: Int , statuses: String , sort:String , userId : String)->Observable<MyOrders>{
        return network.getItems("rest/v2/foodcrowd-ae/users/\(userId)/orders?currentPage\(currentPage)&fieldsFULL&pageSize\(pageSize)")
      
 
     }

}
