//
//  OrdersDetailsNetwork.swift
//  NetworkPlatform
//
//  Created by khalil on 8/13/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Domain
import RxSwift

public final class OrdersDetailsNetwork {
    private let network : Network<ordersDetailsMyOrdersDetails>
    init(network : Network<ordersDetailsMyOrdersDetails>) {
        self.network = network
    }
    
    public func getOrdersDetails(userId :String , code : String)-> Observable<ordersDetailsMyOrdersDetails>{
        return network.getItems("rest/v2/foodcrowd-ae/users/\(userId)/orders/\(code)?fields=FULL")
        
        
    }
}
