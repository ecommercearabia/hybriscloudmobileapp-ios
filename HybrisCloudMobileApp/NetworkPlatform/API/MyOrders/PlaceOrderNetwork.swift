//
//  PlaceOrderNetwork.swift
//  NetworkPlatform
//
//  Created by Saja Hammad on 8/18/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Domain
import RxSwift

public final class PlaceOrderNetwork {
    private let network : Network<PalceOrderResponse>
    init(network : Network<PalceOrderResponse>) {
        self.network = network
    }
    
    public func setPlaceOrder(cartId : String)-> Observable<PalceOrderResponse>{
        return
            network.urlPostItem("rest/v2/foodcrowd-ae/users/current/orders?cartId=\(cartId)&fields=FULL", parameters: [:])

        
    }
}
