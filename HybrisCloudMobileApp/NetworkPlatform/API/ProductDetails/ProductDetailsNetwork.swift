//
//  ProductDetailsNetwork.swift
//  NetworkPlatform
//
//  Created by khalil on 8/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Domain
import RxSwift


public final class ProductDetailsNetwork {
    
    private let network : Network<ProductDetails>
    private let networkReviews : Network<Reviews>
    private let networkReview : Network<Review>

    init(network : Network<ProductDetails>,networkReviews : Network<Reviews>,networkReview : Network<Review>) {
        self.network = network
        self.networkReviews = networkReviews
        self.networkReview = networkReview
    }
    
    public func fetchProductDetails (productCode : String)-> Observable<ProductDetails>{
        return network.getItems("rest/v2/foodcrowd-ae/products/\(productCode)?fields=FULL")
    }
    
    
    func productReviews(productCode : String) -> Observable<Reviews>{
        return networkReviews.getItems("rest/v2/foodcrowd-ae/products/\(productCode)/reviews?fields=FULL&maxCount=100")
    }
    
    func addReviews(productCode: String,review : Review) ->Observable<Review>{
        return networkReview.jsonPostItem("rest/v2/foodcrowd-ae/products/\(productCode)/reviews?fields=FULL", parameters: review.dictionary)
    }
    

   
}
