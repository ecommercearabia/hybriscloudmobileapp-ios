//
//  DateFormateType.swift
//  HybrisCloudMobileApp
//
//  Created by khalil anqawi on 28/05/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation
import UIKit

enum DateFormateType: CaseIterable {
    /// Formatted date "yyyy"
    case yyyy
    /// Formatted date "yyyy-MM-dd"
    case yyyyMMdd
    /// Formatted date and time "MM/dd/yyyy hh:mm:ss"
    case mmddyyyyhhmmss
    /// Formatted date and time "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    case yyyyMMddTHHmmssSSSZ
    /// Formatted date and time "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
    case yyyyMMddTHHmmssSSSXXX
    /// Formatted date and time "yyyy-MM-dd'T'HH:mm:ss.SSS"
    case yyyyMMddTHHmmssSSS
    /// Formatted date and time "MM/dd/yyyy"
    case mmddyyyy
    /// Formatted date and time "dd MMMM yyyy"
    case ddMMMMyyyy
    // Formatted date and time "yyyyMMddHHmmssfff"
    case yyyyMMddHHmmssfff
    //yyyy-MM-dd'T'HH:mm:ss'Z'
    case yyyyMMddTHHmmssZ
    //yyyy-MM-dd'T'HH:mm:ssZZZZ
    case yyyyMMddTHHmmssZZZZ
    //HH:mm
    case HHmm
    // "yyyyMMddHHmmss"
    case yyyyMMddHHmmss
    // "yyyymmdd"
    case yyyymmdd
    // "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//    case yyyyMMddTHHmmssSSSZ
    // "yyyy-MM-dd'T'HH:mm:ssZZZ"
    case yyyyMMddTHHmmssZZZ
    // "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    case yyyyMMddTHHmmssZZZZZ
    // "yyyy/MM/dd"
    case yyyyMMddSlashed
    // "MM/dd/yyyy hh:mm:ss a"
    case MMddyyyySlashedhhmmssa
    // "yyyy-MM-dd'T'HH:mm"
    case yyyyMMddTHHmm
    // "yyyy-MM-dd hh:mm:ss"
    case yyyyMMddhhmmss
    // "yyyy-MM-dd'T'HH:mm:ss"
    case yyyyMMddTHHmmss
    
    var stringFormat:String {
        switch self {
        case .yyyy: return "yyyy"
        case .yyyyMMdd: return "yyyy-MM-dd"
        case .mmddyyyyhhmmss: return "MM/dd/yyyy hh:mm:ss"
        case .yyyyMMddTHHmmssSSSZ: return "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        case .yyyyMMddTHHmmssSSSXXX: return "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
        case .yyyyMMddTHHmmssSSS: return "yyyy-MM-dd'T'HH:mm:ss.SSS"
        case .mmddyyyy: return "MM/dd/yyyy"
        case .ddMMMMyyyy: return "dd MMMM yyyy"
        case .yyyyMMddHHmmssfff: return "yyyyMMddHHmmssfff"
        case .yyyyMMddTHHmmssZ: return "yyyy-MM-dd'T'HH:mm:ss'Z'"
        case .yyyyMMddTHHmmssZZZZ: return "yyyy-MM-dd'T'HH:mm:ssZZZZ"
        case .HHmm: return "HH:mm"
        case .yyyyMMddHHmmss: return "yyyyMMddHHmmss"
        case .yyyymmdd: return "yyyymmdd"
        case .yyyyMMddTHHmmssZZZ: return "yyyy-MM-dd'T'HH:mm:ssZZZ"
        case .yyyyMMddTHHmmssZZZZZ: return "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        case .yyyyMMddSlashed: return "yyyy/MM/dd"
        case .MMddyyyySlashedhhmmssa: return "MM/dd/yyyy hh:mm:ss a"
        case .yyyyMMddTHHmm: return "yyyy-MM-dd'T'HH:mm"
        case .yyyyMMddhhmmss: return "yyyy-MM-dd hh:mm:ss"
        case .yyyyMMddTHHmmss: return "yyyy-MM-dd'T'HH:mm:ss"
        }
    }
}
