//
//  String.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/23/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import UIKit
extension String {

    func localized(withComment comment: String? = nil) -> String {
        return NSLocalizedString(self, comment: comment ?? "")
    }
    
    func toDateString(format:String) -> String? {
        let dateFormatter = DateFormatter()
        let requiredFormat = format
        var returnedDate = ""
        DateFormateType.allCases.forEach { (format) in
            dateFormatter.dateFormat = format.stringFormat
            if let date = dateFormatter.date(from: self) {
                dateFormatter.dateFormat = requiredFormat
                let dateString = dateFormatter.string(from: date)
                returnedDate = dateString
            }
        }
        return returnedDate
    }

}


extension StringProtocol {
    func index<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
        range(of: string, options: options)?.lowerBound
    }
    func endIndex<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
        range(of: string, options: options)?.upperBound
    }
    func indices<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Index] {
        var indices: [Index] = []
        var startIndex = self.startIndex
        while startIndex < endIndex,
            let range = self[startIndex...]
                .range(of: string, options: options) {
                indices.append(range.lowerBound)
                startIndex = range.lowerBound < range.upperBound ? range.upperBound :
                    index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return indices
    }
    func ranges<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var startIndex = self.startIndex
        while startIndex < endIndex,
            let range = self[startIndex...]
                .range(of: string, options: options) {
                result.append(range)
                startIndex = range.lowerBound < range.upperBound ? range.upperBound :
                    index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
}
