//
//  ImageView.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/3/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Kingfisher

extension UIImageView {
    
    func downloadImageInCellWithoutCorner(urlString : String){
        let url = URL(string: "\(Environment.shared.apiEndpoint?.rawValue ?? "")\(urlString )")

        let size = CGSize(width: self.bounds.size.width , height: self.bounds.size.height)

        let processor = DownsamplingImageProcessor(size: size)
           self.kf.indicatorType = .activity
           
            self.kf.setImage(
               with: url,
               placeholder: UIImage(named: "placeholder"),
               options: [
                   .processor(processor),
                   .scaleFactor(UIScreen.main.scale),
                   .transition(.fade(1)),
                   .cacheOriginalImage
               ])
           {
               result in
               switch result {
               case .success(let value):
                    break
               case .failure(let error):
                   break
               }
           }

           
       }

    func downloadImageInCellWithoutCornerFullResulution(urlString : String){
        let url = URL(string: "\(Environment.shared.apiEndpoint?.rawValue ?? "")\(urlString )")

       // let size = CGSize(width: self.bounds.size.width , height: self.bounds.size.height)

       // let processor = DownsamplingImageProcessor(size: size)
           self.kf.indicatorType = .activity
           
            self.kf.setImage(
               with: url,
               placeholder: UIImage(named: "placeholder"),
               options: [
//                   .processor(processor),
                   .scaleFactor(UIScreen.main.scale),
                   .transition(.fade(1)),
                   .cacheOriginalImage
                
               ])
           {
               result in
               switch result {
               case .success(let value):
break
               case .failure(let error):
                   break
               }
           }

           
       }

    
}
