//
//  UIViewController.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/23/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Loaf
import UIKit

extension UIViewController {
    
    func isArabic () -> Bool {
        return  L102Language.isRTL
    }
    

    func errorLoaf (message : String ,  location : Loaf.Location = .top )  {
        
        Loaf(message , state: .error(L102Language.isRTL ? .right : .left) ,location: location , sender: self ).show()
    }
    
    func warningLoaf (message : String ,  location : Loaf.Location = .top )  {
        
        Loaf(message , state: .warning ,location: location , sender: self).show()
    }
    
    func successLoaf (message : String ,  location : Loaf.Location = .top )  {
           
           Loaf(message , state: .success(L102Language.isRTL ? .right : .left) ,location: location , sender: self).show()
       }
    
    
         func goToHome(){
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let navController = mainStoryBoard.instantiateInitialViewController()
             let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = navController

        }
    
}

extension UIViewController {
   
    func changeRoot(storyBoardName : String){
        let storyboard = UIStoryboard(name: storyBoardName, bundle: nil)
        let controller = storyboard.instantiateInitialViewController()
        guard let window = UIApplication.shared.windows.first else {
            return
        }
        window.rootViewController = controller
        let options: UIView.AnimationOptions = .transitionCrossDissolve
        let duration: TimeInterval = 0.3
        UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
        { completed in

        })

    }
}
