//
//  UICollectionView.swift
//  HybrisCloudMobileApp
//
//  Created by khalil anqawi on 19/06/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//


import Foundation

extension UICollectionViewFlowLayout {

    open override var flipsHorizontallyInOppositeLayoutDirection: Bool {
        return L102Language.isRTL
    }
}

