//
//  UIApplication+Rx.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import UIKit
import RxSwift

extension Reactive where Base: UIApplication {
    
    /// Bindable sink for `networkActivityIndicatorVisible`.
    public var isNetworkActivityIndicatorVisible: Binder<Bool> {
        return Binder(self.base) { application, active in
            application.isNetworkActivityIndicatorVisible = active
        }
    }
}
