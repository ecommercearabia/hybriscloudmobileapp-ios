//
//  ErrorTracker.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import RxSwift
import RxCocoa
import NetworkPlatform
//import ProgressHUD

final class ErrorTracker: SharedSequenceConvertibleType {
    typealias SharingStrategy = DriverSharingStrategy
    private let _subject = PublishSubject<Error>()

    func trackError<O: ObservableConvertibleType>(from source: O) -> Observable<O.Element> {
        
        return source.asObservable().do(onError: onError)
    }

    func asSharedSequence() -> SharedSequence<SharingStrategy, Error> {
        return _subject.asObservable().asDriverOnErrorJustComplete()
    }

    func asObservable() -> Observable<Error> {
        return _subject.asObservable()
    }

    private func onError(_ error: Error) {
//        let errorInModel = error as? ErrorResponse
//        let  msgError =  errorInModel?.errors?.map({ (errorResponseElement) -> String in
//            return errorResponseElement.message ?? ""
//            }).joined(separator: " , ")
       
//        UIApplication.topViewController()?.errorLoaf(message: msgError ?? "")
        _subject.onNext(error)

    }
    
    deinit {
        _subject.onCompleted()
    }
}

extension ObservableConvertibleType {
    func trackError(_ errorTracker: ErrorTracker) -> Observable<Element> {
        
        return errorTracker.trackError(from: self)
    }
}
