//
//  L102Language.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/23/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import UIKit
import KeychainSwift
// constants
// constants
let APPLE_LANGUAGE_KEY = "AppleLanguages"
let LOCAL_LANGUAGE_KEY = "LocalLanguages"
/// L102Language
class L102Language {
   /// get current Apple language
   class func currentAppleLanguage() -> String{
       let userdef = UserDefaults.standard
       let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
       let current = langArray.firstObject as! String

                let index = current.index(current.startIndex, offsetBy: 2)
               let currentWithoutLocale = current.prefix(upTo: index) // Hello
               return String(currentWithoutLocale)

   }
   
   class func currentAppleLanguageFull() -> String{
       let userdef = UserDefaults.standard
       let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
       let current = langArray.firstObject as! String
       return current
   }
   
   /// set @lang to be the first in Applelanguages list
   class func setAppleLAnguageTo(lang: String) {
       let userdef = UserDefaults.standard
       KeychainSwift().set(true, forKey: "isLangChanged")
       userdef.set([lang,currentAppleLanguage()], forKey: APPLE_LANGUAGE_KEY)
       userdef.synchronize()
   }
   
   class func setLocalLAnguageTo(lang: String) {
       let userdef = UserDefaults.standard
       userdef.set(lang , forKey: LOCAL_LANGUAGE_KEY)
       userdef.synchronize()
   }
   class func getLocalLAnguageTo()-> String {
       let userdef = UserDefaults.standard
      let lang =  userdef.object(forKey: LOCAL_LANGUAGE_KEY) as? String
       userdef.synchronize()
       return lang ?? currentAppleLanguage()
   }


   
   
   
   class var isRTL: Bool {
       return L102Language.currentAppleLanguage() == "ar"
   }
}

