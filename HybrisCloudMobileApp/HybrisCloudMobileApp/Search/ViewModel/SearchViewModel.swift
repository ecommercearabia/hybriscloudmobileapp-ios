//
//  SearchViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/25/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import KeychainSwift

class SearchViewModel {
    
    var relevancesFacets = BehaviorSubject<[searchFacet]>(value: [])
    var organic = BehaviorSubject<String>(value: "")
    let toLoginSubject = PublishSubject<Void>()
    
    //Organic:true
    
    struct Input {
        
        let trigger : Driver<Void>
        let searchTrigger : Driver<String>
        let switchTrigger : Driver<Bool>
        let switchVegen : Driver<Bool>
        let switchGluten : Driver<Bool>
        let filterTrigger : Driver<String>
        let pageNumber : Driver<Int>
        let sort : Driver<searchSort>
        let categoryId: Driver<String>
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let product: Driver<ProductSearch>
        let error: Driver<Error>
    }
    
    
    private let useCase: ProductUseCase
    
    init(useCase : ProductUseCase) {
        self.useCase = useCase
    }
    
    
    func transform(input: Input) -> Output {
        
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let product = Driver.combineLatest(input.trigger,input.searchTrigger,input.switchTrigger ,input.filterTrigger,input.sort, input.pageNumber , input.switchGluten, input.switchVegen).flatMapLatest {(_,searchText ,isOrganic,relevance, sort, pageNumber , isGluten , isVegan ) -> Driver<ProductSearch> in
            
        
            //            var organic = isOrganic ? "organic:true" : ""
            
            var organic = isOrganic ? ":Organic:true" : ""
            var vegan = isVegan ? ":Vegan:true" : ""
            var gluten = isGluten ? ":GlutenFree:true" : ""

            var relevanceAfterUpdate = String()
            if relevance != "" {
                if organic != "" {
                    organic = ":" + organic
                }
                if gluten != "" {
                    gluten = ":" + gluten
                }

                if vegan != "" {
                    vegan = ":" + vegan
                }

                  relevanceAfterUpdate = ":relevance" + relevance
            }
            else {
                relevanceAfterUpdate = ":relevance"
            }
            
            
            return self.useCase.search(categoryID: "", currentPage: pageNumber, pageSize: 5, query: "\(searchText)\(relevanceAfterUpdate)\(organic)\(vegan)\(gluten)" , sort: sort.code ?? "" )
//                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching,
                      product: product,
                      error: errors)
    }
    
    
    
    
    func dataSource() -> RxCollectionViewSectionedAnimatedDataSource<BestDealProductSection> {
        return
            RxCollectionViewSectionedAnimatedDataSource<BestDealProductSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .bottom),configureCell: { ds, cv, indexPath, item in
                
                
                
                guard let cell = cv.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as? ProductCollectionViewCell else {
                    return UICollectionViewCell()
                }
                cell.setUp(prodcutDeatis: item)
                cell.wishListButton.rx.tap.subscribe(onNext: { [unowned self](void) in
                    if KeychainSwift().get("user") == "anonymous" {
                        
                        self.toLoginSubject.onNext(())
                    }
                }).disposed(by: cell.disposeBag)
                
                
                return cell
                
                
                
                
            })
    }
    
    
}
