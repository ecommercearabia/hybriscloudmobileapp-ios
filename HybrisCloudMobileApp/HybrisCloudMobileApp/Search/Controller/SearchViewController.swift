//
//  SearchViewController.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/25/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
 import Firebase

class SearchViewController: UIViewController, GetRelevanceValueDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchCollectionView: UICollectionView!
    
    var searchPublishSubject = PublishSubject<Void>()
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var sortButton: UIBarButtonItem!
    @IBOutlet weak var sortPickerView: UIPickerView!
    @IBOutlet weak var selectSortTypeButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var switchOrganic: UISwitch!
    @IBOutlet weak var switchVegen: UISwitch!
    @IBOutlet weak var switchGluten: UISwitch!
    @IBOutlet weak var cancelFilter: UIBarButtonItem!
    
    

    @IBOutlet weak var switchsHight: NSLayoutConstraint!
    @IBOutlet weak var switchsView: UIView!
    
    
    @IBOutlet weak var veganStack: UIStackView!
    @IBOutlet weak var glutenFreeStack: UIStackView!
    @IBOutlet weak var organic: UIStackView!
    

    
    
    @IBOutlet weak var back: UIBarButtonItem!
    var relevancesSubject = BehaviorSubject<String>(value: "")
    
    let disposeBag = DisposeBag()
    var viewModel:SearchViewModel!
    var selectedSortTypeSubjec = BehaviorSubject<searchSort>(value: searchSort.init(code: "", name: "", selected: false))
    var selectedSortType : searchSort? = nil
    var sortType = [searchSort]()
    let toLoginSubject = PublishSubject<Void>()
    var paginationPageNumberBehaviorSubject = BehaviorSubject<Int>(value: 0)
    var bestDealProductSectionBehaviorSubject = BehaviorSubject<[BestDealProductSection]>(value: [])
    var categoryIdSubject = BehaviorSubject<String>(value: "")
    var paginationTotalResult : Int = 0
    var productListingCount : Int = 0
    var searchSubject = PublishSubject<String>()
    
    func getRelevanceValue(relevance: String) {
        self.relevancesSubject.onNext(relevance)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpSortView()
        
        self.veganStack.isHidden = true
        self.glutenFreeStack.isHidden = true
        self.organic.isHidden = true

        searchCollectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionViewCell")
        
        self.back.rx.tap.subscribe(onNext: { (void) in
            self.navigationController?.popViewController(animated: true)
            }).disposed(by: disposeBag)
        
        
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        
        viewModel = SearchViewModel(useCase: networkUseCaseProvider.makeProductListingUseCase())
        
        searchBar
            .rx.text.orEmpty.subscribe(onNext: { (text) in
                if text.count > 2 {
                    self.searchSubject.onNext(text)
                    
                    Analytics.logEvent(AnalyticsEventSearch, parameters: [
                      AnalyticsParameterSearchTerm: text ?? ""
                      ])
                }
            }).disposed(by: disposeBag)

 
        searchBar.rx.searchButtonClicked.subscribe(onNext: {(void) in
            
            self.view.endEditing(true)
        }).disposed(by: disposeBag)


        self.bestDealProductSectionBehaviorSubject.subscribe(onNext: { (array) in
            if array.count > 0 {
                if (array.first?.items.count ?? 0) > 0 {
                    self.searchCollectionView.isHidden = false
                    self.emptyView.isHidden = true
                }
                else {
                    self.searchCollectionView.isHidden = true
                    self.emptyView.isHidden = false

                }
            }
            else {
                self.searchCollectionView.isHidden = true
                self.emptyView.isHidden = false

            }
        }).disposed(by: disposeBag)
        
        
        
        let switchTrigger = self.switchOrganic
            .rx.value.asDriver()

        let switchVegenTrigger = self.switchVegen
            .rx.value.asDriver()

        let switchGlutenTrigger = self.switchGluten
            .rx.value.asDriver()

        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:))).mapToVoid().asDriverOnErrorJustComplete()
        
        
        let input = SearchViewModel.Input(
            trigger : viewWillAppear,
            searchTrigger: searchSubject.asDriverOnErrorJustComplete(),
            switchTrigger: switchTrigger,
            switchVegen : switchVegenTrigger,
            switchGluten : switchGlutenTrigger,
            filterTrigger: self.relevancesSubject.asDriverOnErrorJustComplete(),
            pageNumber:  paginationPageNumberBehaviorSubject.asDriverOnErrorJustComplete(),
            sort: selectedSortTypeSubjec.asDriverOnErrorJustComplete(),
            categoryId: self.categoryIdSubject.asDriverOnErrorJustComplete())
        
        let output = viewModel.transform(input: input)
        
 
        output.error.asObservable().subscribe(onNext: { (error) in
            print(error)
        }).disposed(by: disposeBag)
        
        self.relevancesSubject.asObserver().map { (text) -> Bool in
            self.paginationTotalResult  = 0
            self.productListingCount  = 0
            self.paginationPageNumberBehaviorSubject.onNext(0)
            self.bestDealProductSectionBehaviorSubject.onNext([BestDealProductSection(header: "", items: [])])
            return !(text.isEmpty)
        }.bind(to: self.cancelFilter.rx.isEnabled).disposed(by: disposeBag)
        
        self.cancelFilter.rx.tap.subscribe(onNext: { (void) in
            self.relevancesSubject.onNext("")
        }).disposed(by: disposeBag)
        
        //        output.product.asObservable().map { (searchResult) -> [BestDealProductSection] in
        //            return [.init(header: "", items: searchResult.products ?? [])]
        //        }.asObservable().bind(to: searchCollectionView.rx.items(dataSource: self.viewModel.dataSource())).disposed(by: disposeBag)
        //
        output.product.asObservable().subscribe(onNext: { (productSearch) in
            self.viewModel.relevancesFacets.onNext(productSearch.facets ?? [])
//            self.searchBar.becomeFirstResponder()
        }).disposed(by: disposeBag)
        
        
        output.product.asObservable().subscribe(onNext: { (product) in
            guard let listCol = try? self.bestDealProductSectionBehaviorSubject.value() else {return}
            var items = listCol.first?.items
            
            items?.removeAll(where: { (productDetails) -> Bool in
                return product.products?.contains(productDetails) ?? false
            })

            
            
            items?.append(contentsOf: product.products ?? [])
            self.productListingCount = (items?.count ?? product.products?.count) ?? 0
            
            self.bestDealProductSectionBehaviorSubject.onNext([BestDealProductSection(header: "", items: (items ?? product.products) ?? [])])
        }).disposed(by: disposeBag)
        
        self.bestDealProductSectionBehaviorSubject.bind(to: searchCollectionView.rx.items(dataSource: self.viewModel.dataSource())).disposed(by: disposeBag)
        self.searchCollectionView.rx.setDelegate(self).disposed(by: disposeBag)
        
        self.searchCollectionView.rx.modelSelected(BestDealProductSection.Item.self).subscribe(onNext: {(item) in print("model Selected")
            
            Analytics.logEvent(AnalyticsEventSelectItem, parameters: [
                AnalyticsParameterItemListID: item.code ?? "" ,
                AnalyticsParameterItemListName: item.name ?? ""
              ])
            self.performSegue(withIdentifier: "toProductDetails", sender: item)
        }).disposed(by: disposeBag)
        
        output.product.map { (product) -> Bool in
            self.paginationTotalResult = product.pagination?.totalResults ?? 0
            
            return product.products?.count ?? 0 != 0
        }.asObservable().bind(to:  self.emptyView.rx.isHidden).disposed(by: disposeBag)
        
        output.product.map { (product) -> Bool in
            product.products?.count ?? 0 < 0
        }.asObservable().bind(to:  self.searchCollectionView.rx.isHidden).disposed(by: disposeBag)
        
        
        output.product.asObservable().subscribe(onNext: { (ProductSearch) in
            self.sortType = []
            ProductSearch.sorts.forEach({ (searchSort) in
                self.sortType.append(searchSort)
            })
            
            self.veganStack.isHidden = true
            self.glutenFreeStack.isHidden = true
            self.organic.isHidden = true
            
            ProductSearch.facets?.forEach({ (searchFacet) in
                if searchFacet.name == "Vegan" || searchFacet.name == "نباتي" {
                    self.veganStack.isHidden = false
                }
                if searchFacet.name == "Organic" || searchFacet.name == "عضوي" {
                    self.organic.isHidden = false
                }
                if searchFacet.name == "Gluten Free" || searchFacet.name == "خالي من الغلوتين" {
                    self.glutenFreeStack.isHidden = false
                }
                
             })

            
        }).disposed(by: disposeBag)
        self.viewModel.toLoginSubject.subscribe(onNext: { [unowned self](bool) in
            
            self.performSegue(withIdentifier: "toLogin", sender: nil)
            
            
        }).disposed(by: disposeBag)
        
        //MARK: Sort Button
        sortButton.rx.tap.subscribe(onNext: { (Void) in
            self.view.endEditing(true)
            
            self.sortView.alpha = 0
            self.sortView.isHidden = false
            self.selectedSortType = searchSort.init(code: self.sortType.first?.code, name: self.sortType.first?.name, selected: self.sortType.first?.selected)
            self.sortPickerView.delegate = nil
            self.sortPickerView.dataSource = nil
            UIView.transition(with: self.sortView
                , duration:0.5, options: .transitionCrossDissolve, animations: {
                    self.sortView.alpha = 1
                    self.sortType.forEach { (typeName) in
                        
                    }
                    Observable.just(self.sortType)
                        .bind(to: self.sortPickerView.rx.itemTitles) { _, item in
                            return item.name
                    }
                    .disposed(by: self.disposeBag)
            })
            
            
        }).disposed(by: disposeBag)
        
        self.sortPickerView.rx.modelSelected(searchSort.self)
            .subscribe(onNext: { models in
                
                self.selectedSortType = models.first ?? searchSort.init(code: self.sortType.first?.code, name: self.sortType.first?.name, selected: self.sortType.first?.selected)
                
            }).disposed(by: self.disposeBag)
        
        //MARK: Selected sort Type Button
        selectSortTypeButton.rx.tap.subscribe(onNext: { (Void) in
            
            UIView.transition(with: self.sortView,
                              duration: 0.5,
                              options: .transitionCurlDown,
                              animations: { [weak self] in
                                self?.sortView.isHidden = true
                                self?.sortPickerView.delegate = nil
                                self?.sortPickerView.dataSource = nil
                }, completion: nil)
            
            self.paginationTotalResult  = 0
            self.productListingCount  = 0
            self.paginationPageNumberBehaviorSubject.onNext(0)
            self.bestDealProductSectionBehaviorSubject.onNext([BestDealProductSection(header: "", items: [])])
            self.selectedSortTypeSubjec.onNext((self.selectedSortType) ?? searchSort.init(code: self.sortType.first?.code, name: self.sortType.first?.name, selected: self.sortType.first?.selected))
            
            
            
        }).disposed(by: disposeBag)
        
        //MARK: cancel Button
        cancelButton.rx.tap.subscribe(onNext: { (Void) in
            UIView.transition(with: self.sortView,
                              duration: 0.5,
                              options: .transitionCurlDown,
                              animations: { [weak self] in
                                self?.sortView.isHidden = true
                                self?.sortPickerView.delegate = nil
                                self?.sortPickerView.dataSource = nil
                }, completion: nil)
        }).disposed(by: disposeBag)
        
        
       let serachTrigger =  searchBar
        .rx.text.orEmpty
        
        Observable.merge( switchOrganic.rx.isOn.mapToVoid(),self.switchVegen.rx.isOn.mapToVoid() ,switchGluten.rx.isOn.mapToVoid(), selectSortTypeButton.rx.tap.mapToVoid() , serachTrigger.mapToVoid()).subscribe(onNext: { (Void) in
            
            UIView.transition(with: self.sortView,
                              duration: 0.5,
                              options: .transitionCurlDown,
                              animations: { [weak self] in
                                self?.sortView.isHidden = true
                                self?.sortPickerView.delegate = nil
                                self?.sortPickerView.dataSource = nil
                }, completion: nil)
            
            self.paginationTotalResult  = 0
            self.productListingCount  = 0
            self.paginationPageNumberBehaviorSubject.onNext(0)
            self.relevancesSubject.onNext("")
            self.bestDealProductSectionBehaviorSubject.onNext([BestDealProductSection(header: "", items: [])])
            self.selectedSortTypeSubjec.onNext((self.selectedSortType) ?? searchSort.init(code: self.sortType.first?.code, name: self.sortType.first?.name, selected: self.sortType.first?.selected))
            
            

            
        }).disposed(by: disposeBag)

        
    }
    
    fileprivate func setUpSortView() {
        sortView.isHidden = true
        sortView.layer.cornerRadius = 25
        sortView.layer.maskedCorners = [.layerMinXMinYCorner , .layerMaxXMinYCorner]
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toProductDetails"{
            if let vc = segue.destination as? ProductDetailsViewController{
                guard let pd = sender as? ProductDetails else {
                    return
                }
                
                vc.productDetailsBehaviorSubject = BehaviorSubject<String>(value: pd.code ?? "")
            }
        }
        else { // to filter
            if let vc = segue.destination as? ListingFitersViewController {
                vc.delegate = self

            }
        }
    }
}
extension SearchViewController :  UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 300, height: 440)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row ==  productListingCount - 1 {
            if paginationTotalResult > productListingCount{
                guard let pageNum = try? paginationPageNumberBehaviorSubject.value() + 1 else {return}
                print( "productListingCount\(productListingCount)")
                print( "paginationTotalResult\(paginationTotalResult)")
                print( "pageNum\(pageNum)")
                
                
                paginationPageNumberBehaviorSubject.onNext(pageNum)
            }
        }
        
    }
    
    
}






