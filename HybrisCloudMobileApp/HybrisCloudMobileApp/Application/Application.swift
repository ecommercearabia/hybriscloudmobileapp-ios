//
//  Application.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/15/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import NetworkPlatform


final class Application {
    static let shared = Application()

     let networkUseCaseProvider: Domain.UseCaseProvider

     init() {

        self.networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
    }

    func configureMainInterface(in window: UIWindow) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let cdNavigationController = UINavigationController()
        cdNavigationController.tabBarItem = UITabBarItem(title: "CoreData",
                image: UIImage(named: "Box"),
                selectedImage: nil)
//        let cdNavigator = DefaultHomeNavigator(services: coreDataUseCaseProvider,
//                navigationController: cdNavigationController,
//                storyBoard: storyboard)

        let rmNavigationController = UINavigationController()
        rmNavigationController.tabBarItem = UITabBarItem(title: "Realm",
                image: UIImage(named: "Toolbox"),
                selectedImage: nil)
//        let rmNavigator = DefaultHomeNavigator(services: realmUseCaseProvider,
//                navigationController: rmNavigationController,
//                storyBoard: storyboard)

        let networkNavigationController = UINavigationController()
        networkNavigationController.tabBarItem = UITabBarItem(title: "Network",
                image: UIImage(named: "Toolbox"),
                selectedImage: nil)
//        let networkNavigator = DefaultHomeNavigator(services: networkUseCaseProvider,
//                navigationController: networkNavigationController,
//                storyBoard: storyboard)

        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [
//                cdNavigationController,
//                rmNavigationController,
                networkNavigationController
        ]
        window.rootViewController = tabBarController

//        cdNavigator.toPosts()
//        rmNavigator.toPosts()
//        networkNavigator.toPosts()
    }
}
