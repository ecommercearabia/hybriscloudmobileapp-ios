//
//  OldPriceHandler.swift
//  HybrisCloudMobileApp
//
//  Created by Admin on 2020-08-14.
//  Copyright © 2020 Erabia. All rights reserved.
//

/*
 TO BE HANDELED AS THE LOGIC BELOW.
 
 if productDetails.discountPrice != nil {
     let oldprice = "\(String(format: "%.2f", productDetails.discountPrice?.price?.value ?? 0)) \(productDetails.discountPrice?.price?.currencyIso ?? "")"
     let price = "\(String(format: "%.2f", productDetails.discountPrice?.discountPrice?.value ?? 0)) \(productDetails.discountPrice?.discountPrice?.currencyIso ?? "")"
     let savePrice = "\(productDetails.discountPrice?.productDiscountMassage ?? "") (\(Int(productDetails.discountPrice?.percentage ?? 0.0))%)"
     
     let priceFullText = "\(oldprice) \(price) \(savePrice)"
     let attributedText = NSMutableAttributedString.getAttributedString(fromString: priceFullText)
     attributedText.apply(font: UIFont.systemFont(ofSize: 12.0), subString: oldprice)
     attributedText.apply(color: AppColor.oldPriceGrayColor.value, subString: oldprice)
     attributedText.strikeThrough(thickness: 1, subString: oldprice)
     
     attributedText.apply(font: UIFont.boldSystemFont(ofSize: 17.0), subString: price)
     attributedText.apply(color: AppColor.appMainColor.value, subString: price)
     
     attributedText.apply(font: UIFont.systemFont(ofSize: 12.0), subString: savePrice)
     attributedText.apply(color: AppColor.oldPriceGrayColor.value, subString: savePrice)
     productPrice.attributedText = attributedText
 } else {
     let priceFullText = "\(String(format: "%.2f", productDetails.price?.value ?? 0)) \(productDetails.price?.currencyIso ?? "")"
     let attributedText = NSMutableAttributedString.getAttributedString(fromString: priceFullText)
     
     attributedText.apply(font: UIFont.boldSystemFont(ofSize: 17.0), subString: priceFullText)
     attributedText.apply(color: AppColor.appMainColor.value, subString: priceFullText)
     productPrice.attributedText = attributedText
 }
 
 **/
