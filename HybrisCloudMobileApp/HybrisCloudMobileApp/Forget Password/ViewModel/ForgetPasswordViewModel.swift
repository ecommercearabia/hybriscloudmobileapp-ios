//
//  ForgetPasswordViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/30/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation

class ForgetPasswordViewModel: ViewModelType{
    struct Input {
        let trigger: Driver<Void>
        let savetrigger: Driver<Void>
        let email: Driver<String>
        

    }
    struct Output {
        let fetching: Driver<Bool>
        let succefullyForgetPassword: Driver<Void>
        let error: Driver<Error>
    }
    
    
    let bag = DisposeBag()
    
    private let useCase: UserUseCase
   
    init(useCase: UserUseCase) {
        self.useCase = useCase
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
             let errorTracker = ErrorTracker()
        

        let forgetPassword = input.savetrigger.withLatestFrom(input.email).map { (email) in
            return email
        }.flatMapLatest {  [ self] in
            return self.useCase.forgetPassword(forgetPasswordParam: $0)
                .trackError(errorTracker)
            .trackActivity(activityIndicator)
            .asDriverOnErrorJustComplete()
        }
  
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching,
                      succefullyForgetPassword: forgetPassword,
                      error: errors)
        
    }
}
