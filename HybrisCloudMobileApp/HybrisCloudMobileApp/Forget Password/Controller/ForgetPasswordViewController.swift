//
//  ForgetPasswordViewController.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/30/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import KeychainSwift
class ForgetPasswordViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
  
    var viewModel : ForgetPasswordViewModel!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        viewModel = ForgetPasswordViewModel(useCase: networkUseCaseProvider.makeForgetPassword())
        
        let input = ForgetPasswordViewModel.Input(trigger: Driver.merge(viewWillAppear), savetrigger: saveButton.rx.tap.asDriver(), email: emailTextField.rx.text.orEmpty.asDriver())
        
        let output = viewModel.transform(input: input)
        
        output.succefullyForgetPassword.asObservable().subscribe(onNext: { (data) in
            KeychainSwift().set(false, forKey: "SetFaceID")
            self.navigationController?.popViewController(animated: true)
            
        }).disposed(by: disposeBag)
        
        output.error.asObservable().subscribe(onNext: { (Error) in
            let errorInModel = Error as? ErrorResponse
            let  msgError =  errorInModel?.errors?.map({ (errorResponseElement) -> String in
                          return errorResponseElement.message ?? ""
                      }).joined(separator: " , ")
            self.errorLoaf(message: msgError ?? "" )
            
        }).disposed(by: disposeBag)
        
    }
    
    
}
