//
//  AnimationLaunchScreenVC.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 4/21/21.
//  Copyright © 2021 Erabia. All rights reserved.
//

import UIKit
import SwiftyGif

class 	AnimationLaunchScreenVC: UIViewController {

    @IBOutlet weak var itemsGif: UIImageView!
    @IBOutlet weak var bunnerGif: UIImageView!
    @IBOutlet weak var logoGif: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Timer.scheduledTimer(withTimeInterval: 1.8, repeats: false) { timer in
            self.changeRoot(storyBoardName: "tabBar")
        }

        
        do{
            let itemsGif = try UIImage(gifName: "items.gif")
            let bunnerGif = try UIImage(gifName: "bunnerCroped.gif")
            let logoGif = try UIImage(gifName: "logoCroped.gif")
            self.itemsGif.setGifImage(itemsGif, loopCount: -1)
            self.bunnerGif.setGifImage(bunnerGif, loopCount: -1)
            self.logoGif.setGifImage(logoGif, loopCount: -1)
        }
        catch {
            
        }
        
     }
    

 
}
