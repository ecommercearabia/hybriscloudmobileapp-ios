//
//  VMVerifyOtp.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 12/4/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import KeychainSwift


 class VMVerifyOtp: ViewModelType{
    
    private let userUseCase: UserUseCase
    private let sendOTPUseCase: SendOtpCodeAutoLoginUseCase
    private let cartUseCase: CartUseCase
    private let wishListuseCase: WishListUseCase

        init(userUseCase: UserUseCase , sendOTPUseCase: SendOtpCodeAutoLoginUseCase, cartUseCase: CartUseCase,wishListuseCase: WishListUseCase) {
           self.userUseCase = userUseCase
            self.sendOTPUseCase = sendOTPUseCase
            self.cartUseCase = cartUseCase
            self.wishListuseCase = wishListuseCase


       }
    
    struct Input {
        let verifyOtpTrigger: Driver<Void>
        let verifyOtpCode: Driver<String>
        let resendTrigger : Driver<Void>
        let email : Driver<String>
        let mergCart:Driver<MergeCart>
        let wishListTrigger: Driver<Void>
        let creatCart : Driver<Void>
        let getCart : Driver<Void>

     }
    
    struct Output {
        let fetching: Driver<Bool>
        let error: Driver<Error>
        let verifyOtp: Driver<Void>
        let resendOtp: Driver<SendOtpCode>
        let mergCartsSuccessfully:Driver<CartModel>
        let errorMargeCart: Driver<Error>
        let wishList: Driver<WishList>
        let cart: Driver<CartModel>

    }
       
    func transform(input: Input) -> Output {
        
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        let errorMargeCartTracker = ErrorTracker()

        
        let verifyOtp = input.verifyOtpTrigger.withLatestFrom(Driver.combineLatest(input.email,input.verifyOtpCode)).flatMapLatest { (arg0) -> Driver<Void> in
            
            let (email, otpCode) = arg0
            return self.userUseCase.verifyOtpCode(code: otpCode, email: email, name: "")
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
            }

        
        
        let resendOtp = input.resendTrigger.withLatestFrom(input.email).flatMapLatest { (email) -> Driver<SendOtpCode> in
            return self.sendOTPUseCase.sendOtpCode(userId: email)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let getCart = input.getCart.flatMapLatest {[unowned self] (code)  in
            return self.cartUseCase.getCart(userId: KeychainSwift().get("user") == "current" ? "current" : "anonymous", cartId: KeychainSwift().get("user") ?? "anonymous" == "current" ? "current" : KeychainSwift().get("guid") ?? "0000"  )
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        
        let createCart = input.creatCart.flatMapLatest { [unowned self] in
            return self.cartUseCase.creatCart(userId: KeychainSwift().get("user") ?? "anonymous")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        
        
        let mergCartResult = input.mergCart.flatMap { (mergeCart )  in
            
            return self.cartUseCase.mergeCart(oldCartGUID: mergeCart.oldCartGUID ?? "", currentCartGUID: mergeCart.currentCartGUID ?? "")
                .trackError(errorMargeCartTracker)
                .trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
        }
        
        let wishList = input.wishListTrigger.flatMapLatest { [unowned self] in
            return self.wishListuseCase.getWishList()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }

        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        let errorMargeCart = errorMargeCartTracker.asDriver()

        return Output(fetching: fetching,
                      error: errors,
                      verifyOtp: verifyOtp ,
                      resendOtp: resendOtp ,
                      mergCartsSuccessfully: mergCartResult,
                      errorMargeCart: errorMargeCart,
                      wishList: wishList ,
                      cart : Driver.merge(createCart , getCart))
    }
}

 
