//
//  LoginWithOtpViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 12/4/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

struct Input {
    let sendOtpTrigger: Driver<Void>
    let email : Driver<String>
}

struct Output {
    let fetching: Driver<Bool>
    let error: Driver<Error>
    let sendOtp: Driver<SendOtpCode>
    
}

class LoginWithOtpViewModel: ViewModelType{
    
    private let sendOTPUseCase: SendOtpCodeAutoLoginUseCase
    
    init(sendOTPUseCase: SendOtpCodeAutoLoginUseCase) {
        self.sendOTPUseCase = sendOTPUseCase
        
    }
    
    
    
    func transform(input: Input) -> Output {
        
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let sendOtp = input.sendOtpTrigger.withLatestFrom(input.email).flatMapLatest { (email) -> Driver<SendOtpCode> in
            return self.sendOTPUseCase.sendOtpCode(userId: email)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching, error: errors, sendOtp: sendOtp)
    }
}


