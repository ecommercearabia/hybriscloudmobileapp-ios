//
//  VCVerifyOtp.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 12/4/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import SVPinView
import KeychainSwift
import Firebase
class VCVerifyOtp: UIViewController {
    
    
    @IBOutlet weak var codeView: SVPinView!
    @IBOutlet weak var verifyBT: UIButton!
    @IBOutlet weak var resendyBT: UIButton!
    
    let bag = DisposeBag()
    
    var viewModel : VMVerifyOtp!
    let verifyOtpCode = PublishSubject<String>()
    let email = BehaviorSubject<String>(value: "")
    let wishListSubject = PublishSubject<Void>()
    let createCartSubject = PublishSubject<Void>()
    let getCartSubject = PublishSubject<Void>()
    let mergCartSubject = PublishSubject<MergeCart>()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configurecodeView()
        self.preparation()
     }
    
    
    func preparation(){
        
        
        viewModel = VMVerifyOtp(userUseCase: NetworkPlatform.UseCaseProvider().makeUserUseCase(), sendOTPUseCase: NetworkPlatform.UseCaseProvider().makeSendOtpCodeAutoLoginUseCase(), cartUseCase: NetworkPlatform.UseCaseProvider().makeCartUseCase(), wishListuseCase: NetworkPlatform.UseCaseProvider().makeWishListUseCase())
        
        let input = VMVerifyOtp.Input(
            verifyOtpTrigger: verifyBT.rx.tap.asDriver(),
            verifyOtpCode: verifyOtpCode.asDriverOnErrorJustComplete(),
            resendTrigger: resendyBT.rx.tap.asDriver(),
            email: email.asDriverOnErrorJustComplete(),
            mergCart: mergCartSubject.asDriverOnErrorJustComplete(),
            wishListTrigger: wishListSubject.asDriverOnErrorJustComplete() ,
            creatCart: createCartSubject.asDriverOnErrorJustComplete() ,
            getCart : getCartSubject.asDriverOnErrorJustComplete())
        
        let output = viewModel.transform(input: input)
        
        output.resendOtp.asObservable().subscribe(onNext: { (sendOtpCode) in
            if sendOtpCode.status ?? false  {
                self.successLoaf(message: "Otp sent again successfully".localized())
            }
            else {
                self.errorLoaf(message: "There was an error in sending the OTP to the . Please Try Again".localized())
            }
            
            }).disposed(by: bag)

        output.verifyOtp.asObservable().subscribe(onNext: { (void) in
            if let guid = KeychainSwift().get("guid") {
                self.getCartSubject.onNext(())
                
//                self.mergCartSubject.onNext(guid)
//                KeychainSwift().delete("guid")
            }
            
                self.wishListSubject.onNext(())
           
            Analytics.logEvent(AnalyticsEventLogin, parameters: [
              AnalyticsParameterMethod: "Login with Otp"
              ])
            
            }).disposed(by: bag)
        
        output.wishList.asObservable().subscribe(onNext: { (WishList) in
            KeychainSwift().set(WishList.pk ?? "", forKey: "WishListPK")
            self.dismiss(animated: true, completion: nil)
            
        }).disposed(by: bag)

        
        output.cart.asObservable().subscribe(onNext: { (cart) in
            if let guid = KeychainSwift().get("guid") {
                self.mergCartSubject.onNext(MergeCart(oldCartGUID : guid ,currentCartGUID:cart.guid))
                KeychainSwift().delete("guid")
                
            }
                    
        }).disposed(by: bag)
        
        
        output.error.asObservable().subscribe(onNext: { (error) in
            let error = error as? ErrorResponse
            if error?.errors?.first?.type == "AccessDeniedError" {
                if KeychainSwift().get("guid") == nil
                    || KeychainSwift().get("guid") == "current"{
                    self.createCartSubject.onNext(())
                }
                else {
                    return
                }
            }
           else if error?.errors?.first?.message == "Cart not found." {
                self.createCartSubject.onNext(())
            }
           else if error?.errors?.first?.message == "No cart created yet." {
            self.createCartSubject.onNext(())
           }
           else {
            self.errorLoaf(message: error?.errors?.first?.message ?? "Otp code is correct please try again")
            self.codeView.clearPin()
           }
        }).disposed(by: bag)
        
        
        output.mergCartsSuccessfully.asObservable().subscribe(onNext: { (cart) in
            self.dismiss(animated: true, completion: nil)
            }).disposed(by: bag)

        
        output.errorMargeCart.asObservable().subscribe(onNext: { (error) in
            self.dismiss(animated: true, completion: nil)
            }).disposed(by: bag)

        
    }
    
        func configurecodeView() {

            self.verifyBT.isEnabled = false

                codeView.pinLength = 4
                codeView.interSpace = 19 //maximam , if increace Bo0o0oM :P
            
                       codeView.allowsWhitespaces = false

            codeView.style = .box

     
               codeView.font = UIFont.systemFont(ofSize: 18)
               codeView.keyboardType = .asciiCapableNumberPad
 

            codeView.didChangeCallback = { pin in
                   print("The entered pin is \(pin)")
                 if pin.count == 4 {
                    self.verifyBT.isEnabled = true
                    self.verifyOtpCode.onNext(pin)

                }
                else {
                    self.verifyBT.isEnabled = false

                }
               }
           }

    
}
