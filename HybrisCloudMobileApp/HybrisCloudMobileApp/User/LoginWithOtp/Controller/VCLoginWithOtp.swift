//
//  VCLoginWithOtp.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 12/2/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class VCLoginWithOtp: UIViewController {

    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var sendBT: UIButton!
 
    let bag = DisposeBag()
    
    var viewModel : LoginWithOtpViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.validationEmail()
        self.preparation()
     }
    
    func validationEmail() {
//        emailTF.rx.text
//        .orEmpty
//        .map(validate)
//            .bind(to: self.sendBT.rx.isEnabled).disposed(by: bag)

    }

    func preparation(){
    
        
        viewModel = LoginWithOtpViewModel(sendOTPUseCase: NetworkPlatform.UseCaseProvider().makeSendOtpCodeAutoLoginUseCase())

        let input = LoginWithOtpViewModel.Input(sendOtpTrigger: sendBT.rx.tap.asDriver(), email: emailTF.rx.text.orEmpty.asDriver())
        
        let output = viewModel.transform(input: input)

        output.sendOtp.asObservable().subscribe(onNext: { (sendOtpCode) in
            if sendOtpCode.status ?? false  {
                self.performSegue(withIdentifier: "toOtpVerify", sender: nil)
            }
            else {
                self.errorLoaf(message: "There was an error in sending the OTP to the . Please Try Again".localized())
            }
            
            }).disposed(by: bag)
        
          output.error.asObservable().subscribe(onNext: { (Error) in
            self.errorLoaf(message: "something wrong please enter valid email address".localized())
        }).disposed(by: bag)

        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toOtpVerify" {
            if let vc = segue.destination as? VCVerifyOtp {
                vc.email.onNext(self.emailTF.text ?? "")
            }
        }
    }
 

    func validate(_ input: String) -> Bool {
        guard
            let regex = try? NSRegularExpression(
                pattern: "[0-9A-Za-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}",
                options: [.caseInsensitive]
            )
        else {
            assertionFailure("Regex not valid")
            return false
        }

        let regexFirstMatch = regex
            .firstMatch(
                in: input,
                options: [],
                range: NSRange(location: 0, length: input.count)
            )

        return regexFirstMatch != nil
    }


}
