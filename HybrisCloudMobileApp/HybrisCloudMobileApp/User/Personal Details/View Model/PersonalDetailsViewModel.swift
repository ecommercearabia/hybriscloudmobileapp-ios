//
//  PersonalDetailsViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/23/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation


class PersonalDetailsViewModel: ViewModelType{
    
    
    struct Input {
        let trigger: Driver<Void>
        let triggerForTitleAndCode: Driver<Void>
        let updateTrigger: Driver<Void>
        let title: Driver<Title>
        let firstName: Driver<String>
        let lastName: Driver<String>
        let birthday: Driver<String>
        let gender: Driver<String>
        let mobileNumberCode: Driver<Country>
        let mobileNumberNumber: Driver<String>
        let phoneNumber: Driver<String>
        let nationality: Driver<Nationality>
        let subscribeToSMS: Driver<Bool>
        let userFromApi: Driver<UserData>

    }
    
    struct Output {
        let fetching: Driver<Bool>
        let config : Driver<ConfigModel>
        let nationality: Driver<[Nationality]>
        let getUserInformation: Driver<UserData>
        let title: Driver<([Title],Title)>
        let codePhone: Driver<([Country],Country)>
        let error: Driver<Error>
        let updatePersonalDetails: Driver<Void>

    }
    
    

    private let countryUseCase: CountryUseCase
    private let userUseCase: UserUseCase
    private let titleCase: MiscsUseCase
    private let configUseCase: ConfigUseCase
    private let nationalitiesUseCase: NationalitiesUseCase
    init(countryUseCase: CountryUseCase, userUseCase: UserUseCase, createTitleUseCase: MiscsUseCase , nationalitiesUseCase: NationalitiesUseCase  , configUseCase: ConfigUseCase) {
        self.countryUseCase = countryUseCase
        self.userUseCase = userUseCase
        self.titleCase = createTitleUseCase
        self.nationalitiesUseCase = nationalitiesUseCase
        self.configUseCase = configUseCase
    }
    
    
    
    
    func transform(input: Input) -> Output {
        
        let errorTracker = ErrorTracker()
        let activityIndicator = ActivityIndicator()
        
        let config = input.trigger.flatMapLatest{
            return self.configUseCase.config()
                .trackError(errorTracker)
                    .trackActivity(activityIndicator)
                    .asDriverOnErrorJustComplete()

        }
        
        let nationality = input.trigger.flatMapLatest{
            return self.nationalitiesUseCase.fetchNationalities()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()

        }

        let nationalityResult = nationality.map({ (nationality) -> [Nationality] in
                                                    return (nationality.nationalities ?? [])})
        
        let userDetails1 = Driver.combineLatest(
            input.firstName,
            input.lastName,
            input.mobileNumberNumber,
            input.birthday,
            input.gender,
            input.phoneNumber ,
            input.nationality,
            input.subscribeToSMS )
        
        let userDetails2 = Driver.combineLatest(
                   input.title,
                   input.mobileNumberCode
               )
                 
        let userDetails = Driver.combineLatest(userDetails1,userDetails2)
               
        let updateUserDetails = Driver.combineLatest(userDetails, input.userFromApi )
            
        let updateUser = input.updateTrigger.withLatestFrom(updateUserDetails).map { (arg0) -> UserData in
            
            let (((firstName, lastName, mobileNumberNumber, birthday, gender, phoneNumber, nationality, subscribeToSMS), (title, mobileCountry)), User) = arg0
            
            var updateUser : UserData = User
            
            updateUser.mobileCountry = mobileCountry
            updateUser.titleCode = title.code
            updateUser.title = title.name
            updateUser.nationality?.name = nationality.name
            updateUser.nationality?.code = nationality.code
            updateUser.nationalityID = nationality.code
            updateUser.nationality = nationality
         
            if (!(firstName.isEmpty )) { updateUser.firstName =  firstName }
            if (!(lastName.isEmpty )) {  updateUser.lastName =  lastName }
            if (!(mobileNumberNumber.isEmpty )) {
                updateUser.mobileNumber =  mobileNumberNumber
                updateUser.defaultAddress?.mobileNumber =  mobileNumberNumber
            }


            return updateUser
       }.flatMapLatest {
                  self.userUseCase.updateCustomerProfile(userId: "current", user: $0)
                         .trackActivity(activityIndicator)
                    .trackError(errorTracker).asDriverOnErrorJustComplete()
              }
        
        let getUserInformation = input.trigger.flatMapLatest{
            
            return self.userUseCase.getCustomerProfile(userId: "current")
            .trackActivity(activityIndicator)
            .trackError(errorTracker)
            .asDriverOnErrorJustComplete()
        }
        
        let title = input.triggerForTitleAndCode.flatMapLatest{
            
            return self.titleCase.titles()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        
        
        let titleResult = title.map({ (title) -> [Title] in
            return (title.titles ?? [])
        })
        
        let titleSelected = getUserInformation.map({ (user) -> Title in
            let title = Title(code: user.titleCode, name: user.title)
                   return title  })
           

        let codePhone = input.triggerForTitleAndCode.flatMapLatest{
            return self.countryUseCase.fetchCountriesMobile()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
    
        let codePhoneResult = codePhone.map({ (country) -> [Country] in
            return (country.countries ?? [])})
        

        let codePhoneSelected = getUserInformation.map({ (user) -> Country in
            return (user.mobileCountry ?? Country(isdcode: nil, isocode: nil, name: nil)) })
    
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        
        return Output(fetching: fetching,
                      config: config ,
                      nationality: nationalityResult,
                      getUserInformation: getUserInformation,
                      title: Driver.combineLatest(titleResult,titleSelected),
                      codePhone: Driver.combineLatest(codePhoneResult,codePhoneSelected),
                      error: errors ,
                      updatePersonalDetails : updateUser )
        
    }
    
    
}


