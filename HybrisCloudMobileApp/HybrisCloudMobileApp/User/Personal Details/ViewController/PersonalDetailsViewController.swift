//
//  PersonalDetailsViewController.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/23/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

class PersonalDetailsViewController: UIViewController , UITextFieldDelegate {
    
    let codePickerView = UIPickerView()
    let titlePickerView = UIPickerView()
    let nationalityPickerView = UIPickerView()
    let disposeBag = DisposeBag()
    var viewModel : PersonalDetailsViewModel!
    var output :PersonalDetailsViewModel.Output!
    var isNationalityRequired =   false
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var mobileNumberNumberTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var nationalatyStackView: UIStackView!
    
    @IBOutlet weak var updateBarButton: UIButton!
    @IBOutlet weak var subscribeToSMSSwitch: UISwitch!

    var mobileNumberCodeSelectedPublishSubject = PublishSubject<Country>()
    var titleSelectedPublishSubject = PublishSubject<Title>()
    var getManualTitleAndMobileNumberCodePublishSubject = PublishSubject<Void>()
    var userPublishSubject = PublishSubject<UserData>()
    var nationalitySubject = PublishSubject<Nationality>()
    var updatePublishSubject = PublishSubject<Void>()
    var isFromCart  = Bool()
    var otpStatusSubject = PublishSubject<VerifyOTP>()
    var code = ""
    @IBOutlet weak var nationalityTextField: UITextField!{
    didSet{
        nationalityTextField.delegate = self
        }
        
    }
       @IBOutlet weak var titleTextField: UITextField!{
           didSet{
               titleTextField.delegate = self
           }
       }
       
       @IBOutlet weak var mobileNumberCodeTextField: UITextField!{
           didSet{
               mobileNumberCodeTextField.delegate = self
           }
       }
       
    override func viewDidLoad() {
        
        super.viewDidLoad()

        mobileNumberCodeTextField.inputAccessoryView = self.countryInitToolBar()
           
        titleTextField.inputAccessoryView = self.countryInitToolBar()
        
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        
        viewModel = PersonalDetailsViewModel(
            countryUseCase: networkUseCaseProvider.makeCountryUseCase(),
            userUseCase: networkUseCaseProvider.makeUserUseCase(),
            createTitleUseCase: networkUseCaseProvider.makeTitleUseCase() , nationalitiesUseCase: networkUseCaseProvider.makeNationalities(), configUseCase: networkUseCaseProvider.makeConfig())
           
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:))).mapToVoid().asDriverOnErrorJustComplete()
        
        let input = PersonalDetailsViewModel.Input(
            trigger: viewWillAppear,
            triggerForTitleAndCode : getManualTitleAndMobileNumberCodePublishSubject.asDriverOnErrorJustComplete(),
            updateTrigger: updatePublishSubject.asDriverOnErrorJustComplete(),
            title: titleSelectedPublishSubject.asDriverOnErrorJustComplete(),
            firstName: firstNameTextField.rx.text.orEmpty.asDriver(),
            lastName: lastNameTextField.rx.text.orEmpty.asDriver(),
            birthday: birthdayTextField.rx.text.orEmpty.asDriver(),
            gender: genderTextField.rx.text.orEmpty.asDriver(),
            mobileNumberCode: mobileNumberCodeSelectedPublishSubject.asDriverOnErrorJustComplete(),
            mobileNumberNumber: mobileNumberNumberTextField.rx.text.orEmpty.asDriver(),
            phoneNumber: mobileNumberNumberTextField.rx.text.orEmpty.asDriver(),
            nationality: nationalitySubject.asDriverOnErrorJustComplete(),
            subscribeToSMS: subscribeToSMSSwitch.rx.value.asDriverOnErrorJustComplete(),
            userFromApi: userPublishSubject.asDriverOnErrorJustComplete())
        
        output = viewModel.transform(input: input)
        
        
        output.getUserInformation.asObservable().subscribe(onNext: { (User) in

            self.userPublishSubject.onNext(User)

            self.getManualTitleAndMobileNumberCodePublishSubject.onNext(())

            }).disposed(by: disposeBag)
        
        updateBarButton.rx.tap.subscribe(onNext: { (void) in
            if self.nationalityTextField.text == "" && self.isNationalityRequired == true {
                self.errorLoaf(message: "Please select Your Nationality".localized())
            } else if self.titleTextField.text == "" {
                self.errorLoaf(message: "Please select Your Title".localized())
            }
                
            else {
               
                let otpModel =   SendOtpModel.init(isoCode: self.code , mobileNumber: self.mobileNumberNumberTextField.text ?? "", otpCode: nil)
                self.performSegue(withIdentifier: "toOtp", sender: otpModel)
            }
        }).disposed(by: disposeBag)
        
        self.otpStatusSubject.asObserver().subscribe(onNext: {(otpstatus) in
            if otpstatus.status == true {
                self.updatePublishSubject.onNext(())
            }
        }).disposed(by: disposeBag)
        
        
        output.getUserInformation.asObservable().map { (User) -> String in
            var mobileNumber : String =  ""
            let number = User.mobileNumber ?? ""
            let code = User.mobileCountry?.isdcode ?? " "

            if let space = number.firstIndex(of: (code.last ?? " ")) {
                mobileNumber =  String(number[number.index(after: space)..<number.endIndex])
            }else{
                mobileNumber =  number
            }

            return mobileNumber


              }.bind(to: mobileNumberNumberTextField.rx.text).disposed(by: disposeBag)
        
        output.getUserInformation.asObservable().map { (User) -> String in
            return User.firstName ?? ""
            
        }.bind(to: firstNameTextField.rx.text).disposed(by: disposeBag)
        
        
        output.getUserInformation.asObservable().map { (User) -> String in
            if User.nationality == nil {
                self.nationalityTextField.borderStyle = .roundedRect
                self.nationalityTextField.borderColor = .red
                self.nationalityTextField.borderWidth = 1
            }
            return User.nationality?.name ?? ""

        }.bind(to: nationalityTextField.rx.text).disposed(by: disposeBag)
        
        output.getUserInformation.asObservable().withLatestFrom( Driver.combineLatest( output.getUserInformation, output.nationality ) ) .subscribe(onNext: { (user , nationalitys) in
            self.nationalitySubject.onNext(user.nationality ?? Nationality(code: "AE", name: "Emarati"))
            self.nationalityPickerView.selectRow(nationalitys.firstIndex(where: {$0.code == user.nationality?.code ?? ""}) ?? 0 , inComponent: 0, animated: true)
           
        }).disposed(by: disposeBag)
       
        output.getUserInformation.asObservable().map { (User) -> String in
            return User.lastName ?? ""
    
        }.bind(to: lastNameTextField.rx.text).disposed(by: disposeBag)
             
  
        
        output.codePhone.asObservable().map({ (arg0) -> [Country] in
            
            let (list, _) = arg0
            return list
        }).bind(to: codePickerView.rx.itemTitles){ _, item in
          
                  return item.isdcode
              } .disposed(by: disposeBag)
    
        output.codePhone.asObservable().subscribe(onNext: {[unowned self] (countries , countrySelected) in
            if(countries.count != 0)
            { self.codePickerView.selectRow(self.getSelectedCodePhone(countries: countries,countrySelected:countrySelected), inComponent: 0 , animated: true)
                  
            self.codePickerView.delegate?.pickerView?(self.codePickerView, didSelectRow: self.getSelectedCodePhone(countries: countries,countrySelected:countrySelected), inComponent: 0)
                
                if(countries.count != 1 ){
                    self.mobileNumberCodeTextField.inputView = self.codePickerView
                    self.mobileNumberCodeTextField.isUserInteractionEnabled = true
                }
            }
              }).disposed(by: disposeBag)
        
        output.nationality.asObservable().map({ list -> [Nationality] in
            return list
        }).bind(to: nationalityPickerView.rx.itemTitles){ _, item in

            return item.name
        } .disposed(by: disposeBag)
        
        output.nationality.asObservable().subscribe(onNext: {[unowned self] (nationalities) in
            if(nationalities.count != 0 )
            {

                if(nationalities.count != 1 )
                {
                    self.nationalityTextField.inputView = self.nationalityPickerView
                    self.nationalityTextField.isUserInteractionEnabled = true
                }
            }
        }).disposed(by: disposeBag)
             
        codePickerView.rx.modelSelected(Country.self).map { (countries) -> Country in
            self.code = countries.first?.isocode ?? ""
                         return countries.first ?? Country(isdcode: nil, isocode: nil, name: nil)
                         }.bind(to: mobileNumberCodeSelectedPublishSubject).disposed(by: disposeBag)
              
        codePickerView.rx.modelSelected(Country.self).map { (countries) -> String in
                         return countries.first?.isdcode ?? ""
                     }.bind(to: mobileNumberCodeTextField.rx.text).disposed(by: disposeBag)
        
        
        
        
        output.title.asObservable().map({ (arg0) -> [Title] in
                
                let (list, _) = arg0
                return list
            }).bind(to: titlePickerView.rx.itemTitles){ _, item in
              
                return item.name
                  } .disposed(by: disposeBag)
        
            output.title.asObservable().subscribe(onNext: {[unowned self] (titles , titleSelected) in
                if(titles.count != 0 )
                {
                    if titleSelected.code != "" && titleSelected.code != nil  {
                        self.titlePickerView.selectRow(self.getSelectedTitle(titles: titles, selectedTitle: titleSelected), inComponent: 0 , animated: true)
                                
                        self.titlePickerView.delegate?.pickerView?(self.codePickerView, didSelectRow: self.getSelectedTitle(titles: titles, selectedTitle: titleSelected), inComponent: 0)
                        self.titleTextField.borderStyle = .none
                        self.titleTextField.borderColor = .clear
                        self.titleTextField.borderWidth = 0
                    } else {
                        self.titleTextField.placeholder = "Title".localized()
                        self.titleTextField.borderStyle = .roundedRect
                        self.titleTextField.borderColor = .red
                        self.titleTextField.borderWidth = 1
                    }
                self.titleTextField.inputView = self.titlePickerView
                        
                self.titleTextField.isUserInteractionEnabled = true
                    
                }
                  }).disposed(by: disposeBag)
                 

       
        
        titlePickerView.rx.modelSelected(Title.self).map { (titles) -> Title in
            if titles.first?.name != nil  {
                self.titleTextField.borderStyle = .none
                self.titleTextField.borderColor = .clear
                self.titleTextField.borderWidth = 0
            } else {
                self.titleTextField.borderStyle = .roundedRect
                self.titleTextField.borderColor = .red
                self.titleTextField.borderWidth = 1

            }
                                return titles.first ?? Title(code: nil, name: nil)
                                }.bind(to: titleSelectedPublishSubject).disposed(by: disposeBag)
                     
        titlePickerView.rx.modelSelected(Title.self).map { (titles) -> String in
            return titles.first?.name ?? ""
                            }.bind(to: titleTextField.rx.text).disposed(by: disposeBag)
        
        nationalityPickerView.rx.modelSelected(Nationality.self).map { (nationalities) -> Nationality in
            return nationalities.first ?? Nationality(code: nil, name: nil)
        }.bind(to: nationalitySubject).disposed(by: disposeBag)

        nationalityPickerView.rx.modelSelected(Nationality.self).map { (nationalities) -> String in
            self.nationalityTextField.borderStyle = .none
            self.nationalityTextField.borderColor = .clear
            self.nationalityTextField.borderWidth = 0
            return nationalities.first?.name ?? ""
        }.bind(to: nationalityTextField.rx.text).disposed(by: disposeBag)

        nationalityPickerView.rx.modelSelected(Nationality.self)
            .subscribe(onNext: { models in
                self.nationalityTextField.text =  models.first?.name
            }).disposed(by: disposeBag)

        
        output.updatePersonalDetails.asObservable().subscribe(onNext: { (arg0) in
            
            let () = arg0
            
            if self.isFromCart {
                self.navigationController?.popViewController(animated: false)
                self.successLoaf(message: "personal details updated successfully".localized())
            } else {
                self.successLoaf(message: "personal details updated successfully".localized())
            }
            
            }).disposed(by: disposeBag)
        
     
        
        output.error.asObservable().subscribe(onNext: { (error) in
       
            let errorInModel = error as? ErrorResponse
            
            let  msgError =  errorInModel?.errors?.map({ (errorResponseElement) -> String in
                          return errorResponseElement.message ?? ""
                      }).joined(separator: " , ")
                      
            self.errorLoaf(message: msgError ?? "" )
            
            }).disposed(by: disposeBag)

        output.config.asObservable().subscribe(onNext: { (configration) in

            if configration.registrationConfiguration?.nationalityConfigurations?.enabled  == true {
                self.nationalatyStackView.isHidden = false
                if configration.registrationConfiguration?.nationalityConfigurations?.configurationsRequired == true {
                    self.isNationalityRequired = true
                } else {
                    self.isNationalityRequired = false

                }
            } else {
                self.nationalatyStackView.isHidden = true
            }
          
        
        }).disposed(by: disposeBag)
    }
    
    
    func getSelectedCodePhone(countries : [Country],countrySelected:Country?) -> Int{
             
     var indexSelected = 0

        if ((countrySelected) != nil)
        {
            for (index, element) in countries.enumerated() {
                if (element ==  countrySelected)
                {
                    indexSelected = index
                    break

                }
            }
               
           

        }else {
            indexSelected = 0
        }
 
        mobileNumberCodeSelectedPublishSubject.onNext(countries[indexSelected])
        
    return indexSelected
    }
    
    
    func getSelectedTitle(titles : [Title], selectedTitle : Title?) -> Int{
         
           var indexSelected = 0

              if ((selectedTitle) != nil)
              {
                  for (index, element) in titles.enumerated() {
                      if (element ==  selectedTitle)
                      {
                          indexSelected = index

                      }
                  }
              }else {
                  indexSelected = 0
              }
          titleSelectedPublishSubject.onNext(titles[indexSelected])

          return indexSelected
          }
    //MARK: Tab Bar
        func countryInitToolBar() -> UIToolbar {
            let toolBar = UIToolbar()
            toolBar.barStyle = UIBarStyle.default
            toolBar.isTranslucent = true
            toolBar.tintColor = UIColor(red:14.0/255, green:122.0/255, blue:254.0/255, alpha: 1)
            toolBar.sizeToFit()
            let cancelButton = UIBarButtonItem(title: (NSLocalizedString("Done".localized(), comment: "")), style: .plain, target: self, action: #selector(canceldoneDateFromPicker));
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            toolBar.setItems([spaceButton, cancelButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            return toolBar
        }
        @objc func canceldoneDateFromPicker(){
            self.view.endEditing(true)
        }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toOtp"{
            guard let model = sender as? SendOtpModel else {
                return
            }
            if let vc = segue.destination as? OtpViewController{
                vc.sendOtpBehaviorSubject = BehaviorSubject(value: model)
                vc.otpStatusSubject.bind(to: self.otpStatusSubject).disposed(by: disposeBag)
                vc.isFromUpDateProvie = true
            }
        }
    }
    
    
    }
