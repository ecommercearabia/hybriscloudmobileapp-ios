//
//  WelcomeViewController.swift
//  HybrisCloudMobileApp
//
//  Created by khalil anqawi on 18/09/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import UIKit
import Foundation
import RxSwift
import KeychainSwift
class WelcomeViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var shoppingButton: UIButton!
    @IBOutlet weak var msgLabel: UILabel!
    let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true)
        titleLabel.text = "Your registration is complete".localized()
        msgLabel.text = "Remember to use the".localized()  + " " + (KeychainSwift().get("receiverRewardAmount") ?? " 30 ") + " AED".localized() + " " + "reward added in your \"store credit\"".localized()
        shoppingButton.setTitle("START SHOPPING".localized(), for: .normal)
        shoppingButton.rx.tap.subscribe(onNext: {(void) in
           /* let appdel: AppDelegate = UIApplication.shared.delegate as! AppDelegate
            let mainwindow : UIWindow = appdel.window!
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "tabBar", bundle: nil)
            let tabBarVC: MainTabBar = mainStoryboard.instantiateViewController(withIdentifier: "tabBar") as! MainTabBar
            tabBarVC.selectedIndex = 0
            mainwindow.rootViewController = tabBarVC*/
            if let nvc = self.presentingViewController as? UINavigationController {
            self.presentingViewController?.dismiss(animated: true) {
                
                    nvc.viewControllers.first?.dismiss(animated: true)
                }
            }
            
        }).disposed(by: disposeBag)
    }
}
