//
//  FaceIdCell.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 25/3/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import UIKit
import RxSwift
import KeychainSwift
class FaceIdCell: UITableViewCell {

    @IBOutlet weak var faceIdLbl: UILabel!
    @IBOutlet weak var faceSwitch: UISwitch!
    var bag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
      
        if KeychainSwift().getBool("SetFaceID") == true {
            faceSwitch.isOn = true
        } else
        {
            faceSwitch.isOn = false
        }
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.bag = DisposeBag()
    }
    
}
