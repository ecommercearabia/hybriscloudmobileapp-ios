//
//  SettingsCell.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
