//
//  SettingsViewController.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import KeychainSwift
class SettingsViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    let bag = DisposeBag()
    var viewModel = SettingsViewModel(userUseCase: NetworkPlatform.UseCaseProvider().makeUserUseCase())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "App Setting".localized()

        let input = SettingsViewModel.Input( setFaceIDTrigger : viewModel.setFaceIdSubject.asDriverOnErrorJustComplete())
        let output = viewModel.transform(input: input)
        output.confarmFaceId.asObservable().subscribe(onNext: { (void) in
            KeychainSwift().set(true, forKey: "SetFaceID")
            
        }).disposed(by: bag)
        
        viewModel.menuDataList.onNext(viewModel.sectionMenu())
        
        Observable.zip(self.tableView.rx.itemSelected , self.tableView.rx.modelSelected(SettingsMenuItem.self)).subscribe(onNext: { (index , item) in
            
            if index.section == 0 && index.row == 0 {
                self.changeLanguage()
            }
        }).disposed(by: bag)
        
        
        
        self.viewModel.menuDataList.bind(to: tableView.rx.items(dataSource: self.viewModel.dataSource())).disposed(by: bag)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
}


extension SettingsViewController {
    
    
    func changeLanguage() {
        
        let actions: [UIAlertController.AlertAction] = [
            .action(title: "العربية", style: .default),
            .action(title: "English", style: .default),
            .action(title: "Cancel".localized(), style: .cancel)
            
        ]
        
        UIAlertController
            .present(in: self, title: "Change Language".localized(), message: "Select the language".localized(), style: .actionSheet, actions: actions)
            .subscribe(onNext: { buttonIndex in
                switch buttonIndex {
                case 0: // to arabic
                    self.validationChangeLanguageTo(lang: "ar")
                case 1: //to English
                    self.validationChangeLanguageTo(lang: "en")
                case 2: //cancel
                    break
                default:
                    break
                }
            })
            .disposed(by: self.bag)
    }
    
    
    func changeLanguageTo(lang: String){
        
        if lang == "ar" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UITextView.appearance().semanticContentAttribute = .forceRightToLeft
            UILabel.appearance().semanticContentAttribute = .forceRightToLeft
            UICollectionView.appearance().semanticContentAttribute = .forceRightToLeft
            UICollectionViewCell.appearance().semanticContentAttribute = .forceRightToLeft
            UITableView.appearance().semanticContentAttribute = .forceRightToLeft
            UITableViewCell.appearance().semanticContentAttribute = .forceRightToLeft
            UIButton.appearance().semanticContentAttribute = .forceRightToLeft
            UITextField.appearance().semanticContentAttribute = .forceRightToLeft
            UITableViewHeaderFooterView.appearance().semanticContentAttribute = .forceRightToLeft
            UIImageView.appearance().semanticContentAttribute = .forceRightToLeft
            
            UISearchBar.appearance().semanticContentAttribute = .forceRightToLeft
            UIStackView.appearance().semanticContentAttribute = .forceRightToLeft
            UISlider.appearance().semanticContentAttribute = .forceRightToLeft
            UITabBar.appearance().semanticContentAttribute = .forceRightToLeft
           UIInputView.appearance().semanticContentAttribute = .forceRightToLeft
            UISwitch.appearance().semanticContentAttribute = .forceRightToLeft
            UIToolbar.appearance().semanticContentAttribute = .forceRightToLeft
            UIScrollView.appearance().semanticContentAttribute = .forceRightToLeft
           UIWindow.appearance().semanticContentAttribute = .forceRightToLeft
          
            
        } else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight

            UITextView.appearance().semanticContentAttribute = .forceLeftToRight
            UILabel.appearance().semanticContentAttribute = .forceLeftToRight
            UICollectionView.appearance().semanticContentAttribute = .forceLeftToRight
            UICollectionViewCell.appearance().semanticContentAttribute = .forceLeftToRight
            UITableView.appearance().semanticContentAttribute = .forceLeftToRight
            UITableViewCell.appearance().semanticContentAttribute = .forceLeftToRight
            UIButton.appearance().semanticContentAttribute = .forceLeftToRight
            UITextField.appearance().semanticContentAttribute = .forceLeftToRight
            UITableViewHeaderFooterView.appearance().semanticContentAttribute = .forceLeftToRight
            UIImageView.appearance().semanticContentAttribute = .forceLeftToRight
            UISearchBar.appearance().semanticContentAttribute = .forceLeftToRight
            UIStackView.appearance().semanticContentAttribute = .forceLeftToRight
            UISlider.appearance().semanticContentAttribute = .forceLeftToRight
            UITabBar.appearance().semanticContentAttribute = .forceLeftToRight
           UIInputView.appearance().semanticContentAttribute = .forceLeftToRight
            UISwitch.appearance().semanticContentAttribute = .forceLeftToRight
            UIToolbar.appearance().semanticContentAttribute = .forceLeftToRight
            UIScrollView.appearance().semanticContentAttribute = .forceLeftToRight
           UIWindow.appearance().semanticContentAttribute = .forceLeftToRight
        }
        
        L102Language.setAppleLAnguageTo(lang: lang)
        L102Language.setLocalLAnguageTo(lang: lang)
        
        guard let window = UIApplication.shared.windows.first else {
            return
        }
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        window.rootViewController = storyBoard.instantiateInitialViewController()
       
        let options: UIView.AnimationOptions = .transitionFlipFromRight
        let duration: TimeInterval = 0.5
        UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
        { completed in

        })
       
        
        
    }
    
    
    func validationChangeLanguageTo(lang: String){
        
        if lang == L102Language.currentAppleLanguage() {
            let actions: [UIAlertController.AlertAction] = [
                .action(title: "OK".localized(), style: .destructive)
            ]
            //
            UIAlertController
                .present(in: self, title: "Warning".localized(), message: "This language is already shown".localized() , style: .alert, actions: actions)
                .subscribe(onNext: { buttonIndex in
                    
                })
                .disposed(by: self.bag)
            
        }
        else {
            changeLanguageTo(lang: lang)
        }
        
    }
 
}
