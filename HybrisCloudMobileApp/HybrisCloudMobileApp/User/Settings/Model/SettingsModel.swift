//
//  SettingsModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/17/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import UIKit
import RxDataSources

struct SettingsModel {
    var section: SettingsGroupSection
    var items: [Item]
    
}

extension SettingsModel: AnimatableSectionModelType {
    
    typealias Item = SettingsMenuItem
    
    var identity: String { return self.section.name }
    
    init(original: SettingsModel, items: [Item]) {
        self = original
        self.items = items
    }
}
extension SettingsMenuItem: IdentifiableType, Equatable {
    var identity: String {
        return titleText ?? ""
    }
    
    static func == (lhs: SettingsMenuItem, rhs: SettingsMenuItem) -> Bool { return lhs.titleText == rhs.titleText }
}

struct SettingsMenuItem {
    var titleText: String?
    var valueText: String?

    init(fromTitleText titleText: String,fromValueText valueText: String) {
        
        self.titleText = titleText
        self.valueText = valueText
    }
}

struct SettingsGroupSection {
    var name:String
    
    init(fromName sectionName:String) {
        name = sectionName
    }
}
