//
//  SettingsViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import RxSwift
import RxDataSources
import KeychainSwift
import FCUUID

class SettingsViewModel : ViewModelType {
    let setFaceIdSubject = PublishSubject<Void>()
    struct Input {
        let  setFaceIDTrigger : Driver<Void>
    }
    
    struct Output {
     
        let confarmFaceId : Driver<Void>
        let error: Driver<Error>
        let fetching: Driver<Bool>
    }
    let bag = DisposeBag()
    private let userUseCase: UserUseCase
    init(userUseCase: UserUseCase) {
        self.userUseCase = userUseCase

    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
       
        let setFaceID = input.setFaceIDTrigger.flatMapLatest{
            return self.userUseCase.setOrUpdateFaceId(signatureId: FCUUID.uuidForDevice() ?? "", userId: "current")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriver(onErrorDriveWith: Driver<()>.just(()))
        }
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(confarmFaceId: setFaceID , error: errors , fetching : fetching   )
        
    }
    
    var menuDataList = BehaviorSubject<[SettingsModel]>(value: [SettingsModel(section: SettingsGroupSection.init(fromName: ""), items: [])])
    
    func dataSource() ->         RxTableViewSectionedAnimatedDataSource<SettingsModel> {
        return RxTableViewSectionedAnimatedDataSource<SettingsModel>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .right),configureCell: { ds, tv, indexPath, item in
            
            if item.valueText == "touchId" {
                guard let cell = tv.dequeueReusableCell(withIdentifier: "FaceIdCell", for: indexPath) as? FaceIdCell else { return UITableViewCell()}
                
                cell.faceIdLbl.text =  KeychainSwift().get("IdType")
                
               
                cell.faceSwitch.rx.value.subscribe(onNext: { (value) in
                    if value == true {
                        self.setFaceIdSubject.onNext(())
                        
                    } else {
                        KeychainSwift().set(false, forKey: "SetFaceID")
                    }
                }).disposed(by: cell.bag)
                
                return cell
            }
            else {
                guard let cell = tv.dequeueReusableCell(withIdentifier: "SettingsCell", for: indexPath) as? SettingsCell else { return UITableViewCell()}
                cell.valueLabel.text = item.valueText
                cell.titleLabel.text = item.titleText
                
                
                
                return cell
            }
            
            
        } , titleForHeaderInSection: { (ds, sectionIndex) -> String? in
            return ds.sectionModels[sectionIndex].section.name
        }
        
        
        ) { (ds, title, sectionIndex) -> Int in
            return 0
        }
    }
    func sectionMenu() -> [SettingsModel] {
        if  (KeychainSwift().get("user") != "current") {
            languageMenu.remove(at: 1)
        }
        let settingstMenu: [SettingsModel] =
            
            [
                SettingsModel(section: SettingsGroupSection(fromName: ""), items: languageMenu),
                SettingsModel(section: SettingsGroupSection(fromName: "About Application".localized()), items: versionMenu)
                
            ]
        return settingstMenu
    }
   
    var languageMenu: [SettingsMenuItem] = [
        
        
        SettingsMenuItem(fromTitleText: "Language".localized(), fromValueText: "English".localized() ),
        SettingsMenuItem(fromTitleText: "touch id".localized(), fromValueText: "touchId" )
        
        
        
    ]
    
    var versionMenu: [SettingsMenuItem] = [
        
        SettingsMenuItem(fromTitleText: "Version".localized(), fromValueText: Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "")
        
    ]
    
}
