//
//  ChnagePasswordViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/18/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

class ChnagePasswordViewModel: ViewModelType{
    struct Input {
        let trigger: Driver<Void>
        let newPassword: Driver<String>
        let oldPassword: Driver<String>
        let confirmPassword: Driver<String>
        let changePasswordTrigger: Driver<Void>
        let changePasswordManualTrigger : Driver<Void>


    }
    struct Output {
        let fetching: Driver<Bool>
        let canChangePassword: Driver<Bool>
        let succefullyChangePassword: Driver<Void>
        let error: Driver<Error>
    }
    
    
    let bag = DisposeBag()
    
    private let userUseCase: UserUseCase
   
    init(userUseCase: UserUseCase) {
        self.userUseCase = userUseCase
    }
    
    func transform(input: Input) -> Output {
        
        
        let userCredential = Driver.combineLatest(input.newPassword, input.oldPassword)
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let checkDataInput = Driver.combineLatest(
                input.newPassword,
                input.confirmPassword ,
                input.oldPassword)
        
        let canChangePassword = input.changePasswordTrigger.withLatestFrom(checkDataInput).map { (arg0) -> Bool in

            let (newPassword, confirmPassword,_) = arg0
                if(newPassword ==  confirmPassword)
                {
                    return true
                } else {
                    // handle error
                    return false
                }
            }
        
        
        let changePassword = input.changePasswordManualTrigger.withLatestFrom(userCredential)
            .map { (newPassword, oldPassword ) in
                return Domain.ChangePasswordRequest(newPassword: newPassword, oldPassword: oldPassword)
        }
        .flatMapLatest { [unowned self] in
            return self.userUseCase.changePassword(user:$0)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
       
        
     
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching,
                      canChangePassword : canChangePassword,
                      succefullyChangePassword: changePassword,
                      error: errors)
        
    }
}
