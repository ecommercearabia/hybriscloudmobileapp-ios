//
//  ChangePasswordViewController.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import KeychainSwift

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var newPasswordTextField: UITextField!{
        didSet{
            newPasswordTextField.isSecureTextEntry = true
        }
    }
    
    @IBOutlet weak var confirmPasswordTextField: UITextField!{
           didSet{
              confirmPasswordTextField.isSecureTextEntry = true
           }
       }
    
    @IBOutlet weak var oldPasswordTextField: UITextField!{
        didSet{
            oldPasswordTextField.isSecureTextEntry = true
        }
    }
    @IBOutlet weak var ChangePasswordButton: UIButton!
    
    let viewModel = ChnagePasswordViewModel(userUseCase: NetworkPlatform.UseCaseProvider().makeUserUseCase())
    
    let disposeBag = DisposeBag()
    var changePasswordPublishSubject = PublishSubject<Void>()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = ChnagePasswordViewModel.Input(trigger: viewWillAppear,
                                                  newPassword: newPasswordTextField.rx.text.orEmpty.asDriver(),
                                                  oldPassword: oldPasswordTextField.rx.text.orEmpty.asDriver(),
                                                  confirmPassword: confirmPasswordTextField.rx.text.orEmpty.asDriver(),
                                                  changePasswordTrigger: ChangePasswordButton.rx.tap.asDriver(), changePasswordManualTrigger: changePasswordPublishSubject.asDriverOnErrorJustComplete())
        
        let output = viewModel.transform(input: input)
        
        output.succefullyChangePassword.asObservable().subscribe(onNext: { (user) in
          self.successLoaf(message: "Password has been updated successfully".localized())
          KeychainSwift().set(false, forKey: "SetFaceID")


                   self.dismiss(animated: true) {
                        self.navigationController?.backToViewController(vc: MenuViewController.self)

                   }
               }) {
                   
               }.disposed(by: disposeBag)
        output.error.asObservable().subscribe(onNext: { (error) in
            
            let errorInModel = error as? ErrorResponse
            
            let  msgError =  errorInModel?.errors?.map({ (errorResponseElement) -> String in
                return errorResponseElement.message ?? ""
                      }).joined(separator: " , ")
            self.errorLoaf(message: msgError ?? "" )
        }).disposed(by: disposeBag)
        
        output.canChangePassword.asObservable().subscribe(onNext: { (Bool) in
               if(Bool)
               {
                   self.changePasswordPublishSubject.onNext(())
               }else {
                self.errorLoaf(message: "Please ensure that the new password matches the confirm password")
            }
               
               }).disposed(by: disposeBag)
        
    }
    
}
