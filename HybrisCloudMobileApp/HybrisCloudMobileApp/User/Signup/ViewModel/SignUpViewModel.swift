//
//  RegisterViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/21/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import NetworkPlatform
import KeychainSwift

class SignUpViewModel: ViewModelType{
    struct Input {
        let trigger: Driver<Void>
        let title: Driver<Title>
        let nationality: Driver<Nationality>
        
        let firstName: Driver<String>
        let lastName: Driver<String>
        let email: Driver<String>
        let referralCode: Driver<String>
        let password: Driver<String>
        let confirmPassword: Driver<String>
        let mobileNum: Driver<String>
        let registerTrigger: Driver<Void>
        let segueTrigger: Driver<Void>
        let segueManualTrigger: Driver<Void>
        let selectedCountry: Driver<Country>
        let otpStatus: Driver<VerifyOTP>
        let registrationStatus : Driver<UserData>
        let mergCart:Driver<MergeCart>
        let creatCart : Driver<Void>
        let getCart : Driver<Void>
        let wishListTrigger: Driver<Void>
        let setFirebaseToken : Driver<String>
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let config : Driver<ConfigModel>
        let succefullyRegister: Driver<UserData>
        let title: Driver<[Title]>
        let country: Driver<[Country]>
        let nationality: Driver<[Nationality]>
        let codePhone: Driver<[Country]>
        let otpOutput: Driver<SendOtpModel>
        let Selectedcountry: Driver<Country>
        let succefullyLogin: Driver<Void>
        let error: Driver<Error>
        let errorMargeCart: Driver<Error>
        let canSegue: Driver<Bool>
        let mergCartsSuccessfully:Driver<CartModel>
        let cart: Driver<CartModel>
        let wishList: Driver<WishList>
        let succefullySetFirebaseToken : Driver<Void>
      
        
    }
    
    

    private let countryUseCase: CountryUseCase
    private let nationalitiesUseCase: NationalitiesUseCase
    private let createUserUseCase: UserUseCase
    private let cartUseCase: CartUseCase
    private let titleCase: MiscsUseCase
    private let wishListuseCase: WishListUseCase
    private let configUseCase: ConfigUseCase
    init(countryUseCase: CountryUseCase, createUserUseCase: UserUseCase, createTitleUseCase: MiscsUseCase, cartUseCase:CartUseCase, wishListuseCase: WishListUseCase , nationalitiesUseCase: NationalitiesUseCase  , configUseCase: ConfigUseCase) {
        self.countryUseCase = countryUseCase
        self.createUserUseCase = createUserUseCase
        self.titleCase = createTitleUseCase
        self.cartUseCase = cartUseCase
        self.wishListuseCase = wishListuseCase
        self.nationalitiesUseCase = nationalitiesUseCase
        self.configUseCase = configUseCase
    }
    
    func transform(input: Input) -> Output {
        
        let errorTracker = ErrorTracker()
        let errorMargeCartTracker = ErrorTracker()
        let activityIndicator = ActivityIndicator()
        
        let config = input.trigger.flatMapLatest{
            return self.configUseCase.config()
                .trackError(errorTracker)
                    .trackActivity(activityIndicator)
                    .asDriverOnErrorJustComplete()

        }
        
        let userDetails = Driver.combineLatest(input.firstName,input.lastName,input.selectedCountry, input.mobileNum , input.password , input.title, input.email, input.referralCode )
        
      
        let fullUserDetails = Driver.combineLatest(userDetails,input.nationality)
        let register = input.otpStatus.withLatestFrom(fullUserDetails)
            .map { (arg0) in
               let  ((firstName, lastName, mobileCountryCode , mobile , password , title,email, referralCode) , (nationality)) = arg0
                return Domain.UserSignup(firstName: firstName, lastName: lastName, mobileCountryCode: mobileCountryCode.isocode, mobileNumber: mobile, password: password, referralCode: referralCode, titleCode: title.code, uid: email , nationality : nationality.code  , nationalityId : nationality.code)
                
        }
        .flatMapLatest { [unowned self] in
            return self.createUserUseCase.register(user: $0)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        
        
        
        let title = input.trigger.flatMapLatest{
            
            return self.titleCase.titles()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        
        
        let titleResult = title.map({ (title) -> [Title] in
            return (title.titles ?? [])
        })
        
        
        
        let country = input.trigger.flatMapLatest{
            return self.countryUseCase.fetchCountriesShipping()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        
        let nationality = input.trigger.flatMapLatest{
            return self.nationalitiesUseCase.fetchNationalities()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()

        }

        let nationalityResult = nationality.map({ (nationality) -> [Nationality] in
                                                    return (nationality.nationalities ?? [])})
        
        let codePhone = input.trigger.flatMapLatest{
            return self.countryUseCase.fetchCountriesMobile()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        
        let countryResult = country.map({ (country) -> [Country] in
            return (country.countries ?? [])})
        
        let codePhoneResult = codePhone.map({ (country) -> [Country] in
            return (country.countries ?? [])})
        
        let inputForSegue = Driver.combineLatest(input.password,input.confirmPassword)
        
        let canSegue = input.segueTrigger.withLatestFrom(inputForSegue).map({ (arg0) -> Bool  in
            
            let (password, confirmPassword) = arg0
            return password == confirmPassword
        })
        
        let otp = input.segueManualTrigger.withLatestFrom(Driver.combineLatest(input.mobileNum , input.selectedCountry)).map { (mobile, country) -> SendOtpModel in
            return SendOtpModel.init(isoCode: country.isocode, mobileNumber: mobile, otpCode: nil)
        }
        
        let userCredential = Driver.combineLatest( input.email,input.password)
        
        let login = input.registrationStatus.withLatestFrom(userCredential)
                     .map { (email, password ) in
                   return Domain.UserLogin(password: password, uid: email)
                         
                 }
            .flatMapLatest { [unowned self] in
            return self.createUserUseCase.login(user:$0)
                .trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
        }
        
        let wishList = input.wishListTrigger.flatMapLatest { [unowned self] in
            return self.wishListuseCase.getWishList()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let getCart = input.getCart.flatMapLatest {[unowned self] (code)  in
            return self.cartUseCase.getCart(userId: KeychainSwift().get("user") == "current" ? "current" : "anonymous", cartId: KeychainSwift().get("user") ?? "anonymous" == "current" ? "current" : KeychainSwift().get("guid") ?? "0000"  )
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        
        let createCart = input.creatCart.flatMapLatest { [unowned self] in
            return self.cartUseCase.creatCart(userId: KeychainSwift().get("user") ?? "anonymous")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        
        
        let mergCartResult = input.mergCart.flatMap { (mergeCart )  in
            return self.cartUseCase.mergeCart(oldCartGUID: mergeCart.oldCartGUID ?? "", currentCartGUID: mergeCart.currentCartGUID ?? "")
                .trackError(errorTracker)
                .trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
        }
        
        let setFirebaseToken = input.setFirebaseToken.flatMapLatest{ [unowned self] in
            return self.createUserUseCase.setFirebaseToken(mobileToken: $0) .trackError(errorTracker)
                .trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
        }
        
    
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        let errorMargeCart = errorMargeCartTracker.asDriver()
        
        let selectedCountry = input.selectedCountry.asDriver()
        
        return Output(fetching: fetching,
                      config: config ,
                      succefullyRegister: register,
                      title: titleResult,
                      country:countryResult,
                      nationality: nationalityResult,
                      codePhone: codePhoneResult,
                      otpOutput: otp,
                      Selectedcountry: selectedCountry,
                      succefullyLogin: login ,
                      error: errors ,
                      errorMargeCart: errorMargeCart,
                      canSegue: canSegue,
                      mergCartsSuccessfully: mergCartResult,
                      cart: Driver.merge(createCart , getCart), wishList: wishList, succefullySetFirebaseToken: setFirebaseToken )
        
    }
}
