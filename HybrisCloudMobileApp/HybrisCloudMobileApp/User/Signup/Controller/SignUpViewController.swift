//
//  RegisterViewController.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/21/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import UIKit
import Domain
import RxSwift
import RxCocoa
import NetworkPlatform
import KeychainSwift
import Firebase
class SignUpViewController: UIViewController , UITextFieldDelegate, UINavigationControllerDelegate  {
    
    @IBOutlet weak var registerButton: UIButton!{
        didSet{
            registerButton.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            
        }
    }
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var referralCodeTextField: UITextField!
    @IBOutlet weak var nationalityTextField: UITextField!
    @IBOutlet weak var nationalatyStackView: UIStackView!
    @IBOutlet weak var passwordTextField: UITextField!{
        didSet{
            passwordTextField.isSecureTextEntry = true
        }
    }
    @IBOutlet weak var confirmPasswordTextField: UITextField!{
        didSet{
            confirmPasswordTextField.isSecureTextEntry = true
        }
    }
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var numberTextField: UITextField!
    
    @IBOutlet weak var termsAndConditionsButton: UIButton!
    
    @IBOutlet weak var byClickingLabel: UILabel!
    
    @IBOutlet weak var termsAndConditionsSwitch: UISwitch!
    
    @IBAction func backBttnAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    let titlePickerView = UIPickerView()
    let codePickerView = UIPickerView()
    let nationalityPickerView = UIPickerView()
    
    let disposeBag = DisposeBag()
    var viewModel : SignUpViewModel!
    var isoCodeSubject = PublishSubject<Country>()
    var nationalitySubject = PublishSubject<Nationality>()
    var titleSubject = PublishSubject<Title>()
    var otpStatusSubject = PublishSubject<VerifyOTP>()
    var registrationStatusSubject = PublishSubject<UserData>()
    var canSegueSubject = PublishSubject<Void>()
    let createCartSubject = PublishSubject<Void>()
    let getCartSubject = PublishSubject<Void>()
    let mergCartSubject = PublishSubject<MergeCart>()
    let wishListSubject = PublishSubject<Void>()
    var userInformation:UserData?
    var otpStatus : Bool  = false
    
    var loginVC : LoginViewController?
    var range1 = NSRange()
    var range2 = NSRange()
    let firebaseTokenSubject = PublishSubject<String>()
    var isNationalityRequired =   false
    var isTitleRequired =   false
    override func viewDidLoad() {
        super.viewDidLoad()
        styleForByClicking()
        registerButton.layer.cornerRadius = 12
        addNavBarImage()
      //  self.nationalatyStackView.isHidden = true
        preparation()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        chackValidation()
    }
    
    func preparation(){
        
        
        titleTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        nationalityTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        firstNameTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        lastNameTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        emailTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        passwordTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
      //  confirmPasswordTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        codeTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        numberTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        
        self.codeTextField.isUserInteractionEnabled = false
        self.titleTextField.isUserInteractionEnabled = false
        self.nationalityTextField.isUserInteractionEnabled = false
        
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        
        
        viewModel = SignUpViewModel(countryUseCase: networkUseCaseProvider.makeCountryUseCase(), createUserUseCase: networkUseCaseProvider.makeUserUseCase(),createTitleUseCase: networkUseCaseProvider.makeTitleUseCase(), cartUseCase: networkUseCaseProvider.makeCartUseCase(), wishListuseCase: networkUseCaseProvider.makeWishListUseCase(), nationalitiesUseCase: networkUseCaseProvider.makeNationalities(), configUseCase: networkUseCaseProvider.makeConfig())
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = SignUpViewModel.Input(trigger: Driver.merge(Driver.just(())),
                                          title: titleSubject.asDriverOnErrorJustComplete(), nationality: nationalitySubject.asDriverOnErrorJustComplete(),
                                          firstName: firstNameTextField.rx.text.orEmpty.asDriver(),
                                          lastName: lastNameTextField.rx.text.orEmpty.asDriver(),
                                          email: emailTextField.rx.text.orEmpty.asDriver(),
                                          referralCode: referralCodeTextField.rx.text.orEmpty.asDriver(),
                                          password: passwordTextField.rx.text.orEmpty.asDriver(),
                                          confirmPassword: passwordTextField.rx.text.orEmpty.asDriver(),
                                          mobileNum: numberTextField.rx.text.orEmpty.asDriver(),
                                          registerTrigger: registerButton.rx.tap.asDriver(),
                                          segueTrigger: registerButton.rx.tap.asDriver(),
                                          segueManualTrigger: canSegueSubject.asDriverOnErrorJustComplete() ,
                                          selectedCountry: isoCodeSubject.asDriverOnErrorJustComplete(),
                                          otpStatus: otpStatusSubject.asDriverOnErrorJustComplete(),
                                          registrationStatus: registrationStatusSubject.asDriverOnErrorJustComplete(),
                                          mergCart: mergCartSubject.asDriverOnErrorJustComplete(),
                                          creatCart: createCartSubject.asDriverOnErrorJustComplete(),
                                          getCart: getCartSubject.asDriverOnErrorJustComplete(), wishListTrigger: wishListSubject.asDriverOnErrorJustComplete(), setFirebaseToken: firebaseTokenSubject.asDriverOnErrorJustComplete())
        
        let output = viewModel.transform(input: input)
        
        
        output.succefullySetFirebaseToken.asObservable().subscribe(onNext: {
            (void) in
            print("")
        }).disposed(by: disposeBag)
        
        output.succefullyRegister.asObservable().subscribe(onNext: { (user) in
            self.registrationStatusSubject.onNext(user)
            
            Analytics.logEvent(AnalyticsEventSignUp, parameters: [
              AnalyticsParameterMethod: "SignUp"
              ])

            
         

        }) .disposed(by: disposeBag)
        
        output.succefullyLogin.asObservable().subscribe(onNext: { (user) in
            print("Login login")
            
//            if let guid = KeychainSwift().get("guid") {
//                self.getCartSubject.onNext(())
////                self.mergCartSubject.onNext(guid)
////                KeychainSwift().delete("guid")
//
//
//            }
           
           
            self.loginVC?.successSignUp = true
            if self.referralCodeTextField.text != "" {
                self.loginVC?.hasReferralCode = true
                self.wishListSubject.onNext(())
                self.performSegue(withIdentifier: "goWelcomeViewController", sender: nil)
               
            } else {
                self.dismiss(animated: true){
                   
                    self.wishListSubject.onNext(())
                }
            }
            
          
        }).disposed(by: self.disposeBag)
        
        output.wishList.asObservable().subscribe(onNext: { (WishList) in
            
                KeychainSwift().set(WishList.pk ?? "", forKey: "WishListPK")
            
            
            
        }).disposed(by: disposeBag)
        
        output.canSegue.asObservable().subscribe(onNext: { (can) in
            if (can){
                self.canSegueSubject.onNext(())
            }else {
                self.errorLoaf(message: "Please ensure that the new password matches the confirm password".localized())
            }
        }).disposed(by: disposeBag)
        
        // Cart
        output.cart.asObservable().subscribe(onNext: { (cart) in
            if let guid = KeychainSwift().get("guid") {
                self.mergCartSubject.onNext(MergeCart(oldCartGUID : guid ,currentCartGUID:cart.guid))
                KeychainSwift().delete("guid")
            }
        }).disposed(by: disposeBag)
        
        output.mergCartsSuccessfully.asObservable().subscribe(onNext: { (cart) in
            self.loginVC?.successSignUp = true
            if self.referralCodeTextField.text != "" {
                self.loginVC?.hasReferralCode = true
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.dismiss(animated: true, completion: nil)
            }
        }).disposed(by: disposeBag)
        
        output.errorMargeCart.asObservable().subscribe(onNext: { (error) in
            self.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
        
        output.error.asObservable().subscribe(onNext: { (error) in
            
            let errorInModel = error as? ErrorResponse
            
            if errorInModel?.errors?.first?.type == "AccessDeniedError" {
                if KeychainSwift().get("guid") == nil
                    || KeychainSwift().get("guid") == "current"{
                    self.createCartSubject.onNext(())
                }
                else {
                    return
                }
            }
            else if errorInModel?.errors?.first?.message == "Cart not found." {
                self.createCartSubject.onNext(())
            }
            else if errorInModel?.errors?.first?.message == "No cart created yet." {
                self.createCartSubject.onNext(())
            } else {
                let  msgError =  errorInModel?.errors?.map({ (errorResponseElement) -> String in
                    return errorResponseElement.message ?? ""
                }).joined(separator: " , ")
                
                self.errorLoaf(message: msgError ?? "" )
            }
        }).disposed(by: disposeBag)
        
        output.otpOutput.asObservable().subscribe(onNext: { (sendOtp) in
            
            self.performSegue(withIdentifier: "goToOtp", sender: sendOtp)
            
            
        }).disposed(by: disposeBag)
        
        output.title.asObservable().map({ list -> [Title] in
            return list
        }).bind(to: titlePickerView.rx.itemTitles){ _, item in
            
            return item.name
        } .disposed(by: disposeBag)
        
        
        
        output.nationality.asObservable().map({ list -> [Nationality] in
            return list
        }).bind(to: nationalityPickerView.rx.itemTitles){ _, item in

            return item.name
        } .disposed(by: disposeBag)
        
        output.title.asObservable().subscribe(onNext: {[unowned self] (titles) in
            if(titles.count != 0 )
            {
                self.titlePickerView.selectRow(0, inComponent: 0 , animated: true)
                
                self.titlePickerView.delegate?.pickerView?(self.codePickerView, didSelectRow: 0, inComponent: 0)
                if(titles.count != 1 )
                {
                    self.titleTextField.inputView = self.titlePickerView
                    self.titleTextField.isUserInteractionEnabled = true
                }
            }
        }).disposed(by: disposeBag)
        
        
        output.nationality.asObservable().subscribe(onNext: {[unowned self] (nationalities) in
            if(nationalities.count != 0 )
            {
                self.nationalityPickerView.selectRow(0, inComponent: 0 , animated: true)
                self.nationalityPickerView.delegate?.pickerView?(self.nationalityPickerView, didSelectRow: 0, inComponent: 0)
                if(nationalities.count != 1 )
                {
                    self.nationalityTextField.inputView = self.nationalityPickerView
                    self.nationalityTextField.isUserInteractionEnabled = true
                }
            }
        }).disposed(by: disposeBag)
        
        titlePickerView.rx.modelSelected(Title.self).map { (titles) -> Title in
            return titles.first ?? Title(code: nil, name: nil)
        }.bind(to: titleSubject).disposed(by: disposeBag)
        
        titlePickerView.rx.modelSelected(Title.self).map { (titles) -> String in
            return titles.first?.name ?? ""
        }.bind(to: titleTextField.rx.text).disposed(by: disposeBag)
        
        titlePickerView.rx.modelSelected(Title.self)
            .subscribe(onNext: { models in
                self.titleTextField.text =  models.first?.name
            }).disposed(by: disposeBag)
        
        
        
        
        nationalityPickerView.rx.modelSelected(Nationality.self).map { (nationalities) -> Nationality in
            
            return nationalities.first ?? Nationality(code: nil, name: nil)
        }.bind(to: nationalitySubject).disposed(by: disposeBag)

        nationalityPickerView.rx.modelSelected(Nationality.self).map { (nationalities) -> String in
        
            return nationalities.first?.name ?? ""
        }.bind(to: nationalityTextField.rx.text).disposed(by: disposeBag)

        nationalityPickerView.rx.modelSelected(Nationality.self)
            .subscribe(onNext: { models in
                self.nationalityTextField.text =  models.first?.name
            }).disposed(by: disposeBag)

        
        output.config.asObservable().subscribe(onNext: { (configration) in
            
            KeychainSwift().set("\(configration.registrationConfiguration?.referralCodeConfigurations?.receiverRewardAmount ?? 0)", forKey: "receiverRewardAmount")
            if configration.registrationConfiguration?.nationalityConfigurations?.enabled  == true {
                self.nationalatyStackView.isHidden = false
                if configration.registrationConfiguration?.nationalityConfigurations?.configurationsRequired == true {
                    self.isNationalityRequired = true
                } else {
                    self.isNationalityRequired = false

                }
            } else {
                self.nationalatyStackView.isHidden = true
                self.nationalitySubject.onNext(Nationality(code: "AE", name: "United Arab Emirates"))
            }
            self.chackValidation()
        
        }).disposed(by: disposeBag)
        
        output.codePhone.asObservable().bind(to: codePickerView.rx.itemTitles){ _, item in
            return item.isdcode
        } .disposed(by: disposeBag)
        
        output.codePhone.asObservable().subscribe(onNext: {[unowned self] (countries) in
            self.codePickerView.selectRow(0, inComponent: 0, animated: true)
            if(countries.count != 0){
                if let country = countries.first{
                    
                    self.isoCodeSubject.onNext(country)
                    self.codeTextField.text = country.isdcode
                    if(countries.count != 1){
                        self.codeTextField.inputView = self.codePickerView
                        self.codeTextField.isUserInteractionEnabled = true
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        codePickerView.rx.modelSelected(Country.self).map { (countries) -> Country in
            return countries.first ?? Country(isdcode: nil, isocode: nil, name: nil)
        }.bind(to: isoCodeSubject).disposed(by: disposeBag)
        
        codePickerView.rx.modelSelected(Country.self).map { (countries) -> String in
            return countries.first?.isdcode ?? ""
        }.bind(to: codeTextField.rx.text).disposed(by: disposeBag)
        
        codePickerView.rx.modelSelected(Country.self)
            .subscribe(onNext: { models in
                self.codeTextField.text =  models.first?.isdcode
            }).disposed(by: disposeBag)
        
       
        
    }
    
    func chackValidation(){
        let titleValidation = titleTextField
            .rx.text
            .map({!($0?.isEmpty ?? true)})
            .share(replay: 1)
        
        let firstNameValidation = firstNameTextField
            .rx.text
            .map({!($0?.isEmpty ?? true)})
            .share(replay: 1)
        
        let lastNameValidation = lastNameTextField
            .rx.text
            .map({!($0?.isEmpty ?? true)})
            .share(replay: 1)
        
        let emailValidation = emailTextField
            .rx.text
            .map({!($0?.isEmpty ?? true)})
            .share(replay: 1)
        
        let passwordValidation = passwordTextField
            .rx.text
            .map({!($0?.isEmpty ?? true)})
            .share(replay: 1)
        
//        let confirmPasswordValidation = confirmPasswordTextField
//            .rx.text
//            .map({!($0?.isEmpty ?? true)})
//            .share(replay: 1)
        
        let mobileNumberValidation = numberTextField
            .rx.text
            .map({!($0?.isEmpty ?? true)})
            .share(replay: 1)
        
        let  nationalityValidation = self.nationalityTextField
            .rx.text
            .map({!($0?.isEmpty ?? true) || (!self.isNationalityRequired)})
            .share(replay: 1)

//, confirmPasswordValidation
        //, confirmPassword
   //&& confirmPassword
    
        
        let enableButton = Observable.combineLatest(titleValidation,firstNameValidation, lastNameValidation, emailValidation,passwordValidation,mobileNumberValidation,  nationalityValidation ){ (titleValidation,firstName,lastName,email,password, mobileNumber , nationality ) in
            return (titleValidation && firstName && lastName && email && password  && mobileNumber  && nationality )
        }
        
        enableButton.asObservable().map{ (isEnable) in
            if isEnable {
                self.registerButton.backgroundColor = #colorLiteral(red: 0.4329329729, green: 0.734644115, blue: 0.1981055439, alpha: 1)
            } else {
                self.registerButton.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            }
            return isEnable
        }.bind(to: registerButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        
//        enableButton.asObservable().subscribe { (buttonFlag) in
//            if buttonFlag.element ?? true { if buttonFlag.element ?? true {
//                self.registerButton.backgroundColor = #colorLiteral(red: 0.4329329729, green: 0.734644115, blue: 0.1981055439, alpha: 1)
//            }else{
//                self.registerButton.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
//            }
//        }
//        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToOtp"{
            guard let model = sender as? SendOtpModel else {
                return
            }
            if let vc = segue.destination as? OtpViewController{
                vc.sendOtpBehaviorSubject = BehaviorSubject(value: model)
                vc.otpStatusSubject.bind(to: self.otpStatusSubject).disposed(by: disposeBag)
            }
        } else if segue.identifier == "goToTermsAndConditions" {
            if let vc = segue.destination as? TearmsAndConditionsViewController{
                vc.type = sender as! String
            }
        }
             
    }
    
    //MARK: Design
    
    func styleForByClicking(){
        let textbyClicking = "I am confirming that I have read and agreed with the Terms of Use and Privacy Policy.".localized()
        byClickingLabel.text = textbyClicking
        self.byClickingLabel.textColor =  UIColor.black
        let orangeAttriString = NSMutableAttributedString(string: textbyClicking)
        range1 = (textbyClicking as NSString).range(of: "Terms of Use".localized())
        range2 = (textbyClicking as NSString).range(of: "Privacy Policy".localized())
        
        let orange = UIColor(red: 240/255.0, green: 141/255.0, blue: 57/255.0, alpha: 1.00 )
        
        orangeAttriString.addAttribute(NSAttributedString.Key.foregroundColor , value: orange, range: range1 )
        orangeAttriString.underLine(onRange: range1)
        orangeAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: orange, range: range2)
        orangeAttriString.underLine(onRange: range2)
        orangeAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: byClickingLabel.font.pointSize), range: range1)
        orangeAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: byClickingLabel.font.pointSize), range: range2)
        byClickingLabel.attributedText = orangeAttriString
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel))
        byClickingLabel.isUserInteractionEnabled = true
        byClickingLabel.addGestureRecognizer(tap)
    }
    
    
    @IBAction  func tapLabel(sender: UITapGestureRecognizer) {
        if sender.didTapAttributedTextInLabel(label: byClickingLabel, inRange: range1) {
            self.performSegue(withIdentifier: "goToTermsAndConditions", sender: "https://www.foodcrowd.com/en/termsAndConditions")
        } else {
            self.performSegue(withIdentifier: "goToTermsAndConditions", sender: "https://www.foodcrowd.com/en/privacy-policy")
        }
        
        
    }
    func addNavBarImage () {
        let navController = navigationController!
        let logoImage =  #imageLiteral(resourceName: "logo")
        let logoImageView = UIImageView()
        
        let bannerWidth = navController.navigationBar.frame.size.width / 2
        let bannerHeight = navController.navigationBar.frame.size.height / 2
        let bannerX = bannerWidth / 2 - logoImage.size.width / 2
        let bannerY = bannerHeight / 2 - logoImage.size.height / 2
        
        logoImageView.frame = CGRect(x:bannerX , y: bannerY, width: bannerWidth, height: bannerHeight)
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.image = logoImage
        navigationItem.titleView = logoImageView
        
    }
    
    
    
    @objc func doneButtonClicked(_ textField: UITextField) {
        
        switch textField{
        
        case firstNameTextField : lastNameTextField.becomeFirstResponder()
            
        case lastNameTextField : titleTextField.becomeFirstResponder()
            
        case titleTextField : nationalityTextField.becomeFirstResponder()
            
        case nationalityTextField : numberTextField.becomeFirstResponder()
            
        case codeTextField : numberTextField.becomeFirstResponder()
            
        case numberTextField : referralCodeTextField.becomeFirstResponder()

            
        case referralCodeTextField :
            if(codeTextField.isUserInteractionEnabled)
            {codeTextField.becomeFirstResponder()}
            else {numberTextField.becomeFirstResponder()}
            
        case emailTextField : passwordTextField.becomeFirstResponder()
            
        default:
            break
        }}
}



extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(
            x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
            y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y
        )
        let locationOfTouchInTextContainer = CGPoint(
            x: locationOfTouchInLabel.x - textContainerOffset.x,
            y: locationOfTouchInLabel.y - textContainerOffset.y
        )
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

