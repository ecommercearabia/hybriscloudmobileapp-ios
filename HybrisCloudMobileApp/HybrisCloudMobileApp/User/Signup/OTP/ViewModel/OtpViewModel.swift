//
//  OtpViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import Domain
import NetworkPlatform


class OtpViewModel: ViewModelType{
    
    struct Input {
        let trigger: Driver<Void>
        let resendOtpButtonTrigger: Driver<Void>
        let otpBehaviorDriver : Driver<SendOtpModel>
        let verifyButtonTrigger: Driver<Void>
        let otpCode : Driver<String>
        
    }
    struct Output {
        let fetching: Driver<Bool>
        let succefullySendOTP: Driver<SendOTP>
        let succefullyResendOTP: Driver<SendOTP>
        let succefullyVerifyOTP: Driver<VerifyOTP>
        let error: Driver<Error>
    }
    
    
    let bag = DisposeBag()
    
    
    let networkUseCaseProvider : Domain.UseCaseProvider = NetworkPlatform.UseCaseProvider()
    private let sendOtpUseCase: SendOTPUseCase
    private let verifyOtpUseCase: VerifyOTPUseCase
    
    init(sendOTPUseCase: SendOTPUseCase , verifyOtpUseCase : VerifyOTPUseCase) {
        self.sendOtpUseCase = sendOTPUseCase
        self.verifyOtpUseCase = verifyOtpUseCase
        
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        
        let sendOtpResult = input.otpBehaviorDriver.flatMap { (sendOtpResult) -> Driver<SendOTP> in
            
            return  self.sendOtpUseCase.sendOtp(sendOtpParam: sendOtpResult)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let reSendOtpResult = Driver.combineLatest(input.otpBehaviorDriver, input.resendOtpButtonTrigger).flatMap { (otpData, _) -> Driver<SendOTP> in
            
            return  self.sendOtpUseCase.sendOtp(sendOtpParam: otpData)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let verifyOTP = Driver.combineLatest(input.otpBehaviorDriver , input.otpCode , input.verifyButtonTrigger).flatMap { (otpData, otpCode , _) -> Driver<VerifyOTP> in
            
            return self.verifyOtpUseCase.vrifyOTP(sendOtpParam: SendOtpModel.init(isoCode: otpData.isoCode, mobileNumber: otpData.mobileNumber, otpCode: otpCode)).trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        
        return Output(fetching: fetching,
                      succefullySendOTP: sendOtpResult,
                      succefullyResendOTP: reSendOtpResult,
                      succefullyVerifyOTP: verifyOTP,
                      error: errors)
        
        
    }
}

