//
//  OtpViewController.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import UIKit
import SVPinView

class OtpViewController: UIViewController {
    
    @IBOutlet weak var pinView: SVPinView!
    @IBOutlet weak var errorMeseegeLabel: UILabel!{
        didSet{
            errorMeseegeLabel.isHidden = true
        }
    }
    @IBOutlet weak var verifyBut: UIButton!{
        didSet{
            self.verifyBut.isEnabled = false
            self.verifyBut.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            verifyBut.layer.cornerRadius = 12
            
        }
    }
    @IBOutlet weak var resendOtpButton: UIButton!
    
    var otpPinSubject  = PublishSubject<String>()
    var backVc : SignUpViewController?
    var countryCode = String()
    var mobileNumber = String()
    var sendOtpBehaviorSubject :BehaviorSubject<SendOtpModel>?
    var otpStatusSubject = PublishSubject<VerifyOTP>()
    var isFromUpDateProvie = false
    let viewModel = OtpViewModel(sendOTPUseCase: NetworkPlatform.UseCaseProvider().makeSendOtpUseCase(), verifyOtpUseCase: NetworkPlatform.UseCaseProvider().makeVerifyOtpUseCase())
    
    let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavBarImage()
        pinView.becomeFirstResponderAtIndex = 0
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        guard let subject = sendOtpBehaviorSubject else {
                   return
               }
        if isFromUpDateProvie{
            self.verifyBut.setTitle("Update Profile".localized(), for: .normal)
            self.verifyBut.setTitle("Update Profile".localized(), for: .disabled)
        }
        
        
        let input = OtpViewModel.Input(trigger: Driver.merge(viewWillAppear), resendOtpButtonTrigger: resendOtpButton.rx.tap.asDriver(), otpBehaviorDriver: subject.asDriverOnErrorJustComplete(), verifyButtonTrigger: verifyBut.rx.tap.asDriver(), otpCode: otpPinSubject.asDriverOnErrorJustComplete())
        
        let output = viewModel.transform(input: input)
        
        output.succefullySendOTP.asObservable().subscribe(onNext: { (data) in
            
            self.configurePinView()
            
        }).disposed(by: disposeBag)
        
        output.succefullyResendOTP.asObservable().subscribe(onNext: { (data) in
                   
                   self.configurePinView()
                   
               }).disposed(by: disposeBag)
        
        output.succefullyVerifyOTP.asObservable().subscribe(onNext: { (verifyOTP) in
            if verifyOTP.status ?? false{
                output.succefullyVerifyOTP.asObservable().bind(to: self.otpStatusSubject).disposed(by: self.disposeBag)
                output.succefullyVerifyOTP.asObservable().subscribe(onNext: { (_) in
                    self.navigationController?.popViewController(animated: true)
                    
                }).disposed(by: self.disposeBag)
            }else{
                self.errorMeseegeLabel.isHidden = false
                self.errorMeseegeLabel.text = verifyOTP.verifyOTPDescription ?? ""
            }
        }).disposed(by: disposeBag)
        
        
    }
    
    func configurePinView() {
        
        
        pinView.pinLength = 4
        pinView.interSpace = 19
        
        pinView.allowsWhitespaces = false
        
        pinView.style = .underline
        
        
        pinView.font = UIFont.systemFont(ofSize: 18)
        pinView.keyboardType = .asciiCapableNumberPad
        pinView.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        
        
        pinView.didChangeCallback = { pin in
            print("The entered pin is \(pin)")
            if pin.count == 4 {
                self.verifyBut.isEnabled = true
                self.verifyBut.backgroundColor = #colorLiteral(red: 0.4329329729, green: 0.734644115, blue: 0.1981055439, alpha: 1)
                self.otpPinSubject.onNext(pin)
                
            }
            else {
                self.verifyBut.isEnabled = false
                self.verifyBut.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                
                
            }
        }
    }
    
    
    
    @IBAction func backBttnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        //        dismiss(animated: true, completion: nil)

    }
    
    func addNavBarImage () {
        let navController = navigationController!
        let logoImage = #imageLiteral(resourceName: "logo")
        let logoImageView = UIImageView()
        
        let bannerWidth = navController.navigationBar.frame.size.width / 2
        let bannerHeight = navController.navigationBar.frame.size.height / 2
        let bannerX = bannerWidth / 2 - logoImage.size.width / 2
        let bannerY = bannerHeight / 2 - logoImage.size.height / 2
        
        logoImageView.frame = CGRect(x:bannerX , y: bannerY, width: bannerWidth, height: bannerHeight)
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.image = logoImage
        navigationItem.titleView = logoImageView
        
    }
    
}

