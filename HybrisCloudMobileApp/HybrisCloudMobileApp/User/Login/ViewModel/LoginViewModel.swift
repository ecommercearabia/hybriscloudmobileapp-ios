//
//  LoginViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Ahmad Bader on 8/2/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import KeychainSwift
import FCUUID
class LoginViewModel: ViewModelType{
    struct Input {
        let trigger: Driver<Void>
        let email: Driver<String>
        let password: Driver<String>
        let loginTrigger: Driver<Void>
        let mergCart:Driver<MergeCart>
        let wishListTrigger: Driver<Void>
        let rememberMe: Driver<Bool>
        let loginWithFaceIdTrigger : Driver<String>
        let creatCart : Driver<Void>
        let getCart : Driver<Void>
        let setFirebaseToken : Driver<String>
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let succefullyLogin: Driver<Void>
        let mergCartsSuccessfully:Driver<CartModel>
        let wishList: Driver<WishList>
        let error: Driver<Error>
        let errorMargeCart: Driver<Error>
        let succefullyLoginWithFaceId : Driver<LoginFaceID>
        let cart: Driver<CartModel>
        let errorFaceId: Driver<Error>
        let succefullySetFirebaseToken : Driver<Void>
        let config : Driver<ConfigModel>
        
    }
    
    
    let bag = DisposeBag()
    
    private let userUseCase: UserUseCase
    private let cartUseCase: CartUseCase
    private let wishListuseCase: WishListUseCase
    private let configUseCase: ConfigUseCase
    init(userUseCase: UserUseCase,cartUseCase: CartUseCase,wishListuseCase: WishListUseCase , configUseCase: ConfigUseCase) {
        self.userUseCase = userUseCase
        self.cartUseCase = cartUseCase
        self.wishListuseCase = wishListuseCase
        self.configUseCase = configUseCase
    }
    
    func transform(input: Input) -> Output {
        
        
        let userCredential = Driver.combineLatest(input.email, input.password)
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        let errorMargeCartTracker = ErrorTracker()
        let errorFaceId = ErrorTracker()
        
        let config = input.trigger.flatMapLatest{
            return self.configUseCase.config()
                .trackError(errorTracker)
                    .trackActivity(activityIndicator)
                    .asDriverOnErrorJustComplete()

        }
        let setFirebaseToken = input.setFirebaseToken.flatMapLatest{ [unowned self] in
            return self.userUseCase.setFirebaseToken(mobileToken: $0) .trackError(errorTracker)
                .trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
        }
        
        let login = input.loginTrigger.withLatestFrom(userCredential)
            .map { (email, password ) in
                return Domain.UserLogin(password: password, uid: email)
            }
            .flatMapLatest { [unowned self] in
                return self.userUseCase.login(user:$0)
                    .trackError(errorTracker)
                    .trackActivity(activityIndicator)
                    .asDriverOnErrorJustComplete()
            }
        
        let wishList = input.wishListTrigger.flatMapLatest { [unowned self] in
            return self.wishListuseCase.getWishList()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        let getCart = input.getCart.flatMapLatest {[unowned self] (code)  in
            return self.cartUseCase.getCart(userId: KeychainSwift().get("user") == "current" ? "current" : "anonymous", cartId: KeychainSwift().get("user") ?? "anonymous" == "current" ? "current" : KeychainSwift().get("guid") ?? "0000"  )
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        
        let createCart = input.creatCart.flatMapLatest { [unowned self] in
            return self.cartUseCase.creatCart(userId: KeychainSwift().get("user") ?? "anonymous")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        
        
        let mergCartResult = input.mergCart.flatMap { (mergeCart )  in
            
            return self.cartUseCase.mergeCart(oldCartGUID: mergeCart.oldCartGUID ?? "", currentCartGUID: mergeCart.currentCartGUID ?? "")
                .trackError(errorMargeCartTracker)
                .trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
        }
        
        
        let loginWithFaceId = input.loginWithFaceIdTrigger.flatMapLatest{ (email) -> Driver<LoginFaceID> in
            
            
            return self.userUseCase.loginWithFaceId(signatureId:FCUUID.uuidForDevice() ?? "", email: email)
                .trackActivity(activityIndicator)
                .trackError(errorFaceId)
                .asDriverOnErrorJustComplete()
        }
        
        
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        let errorMargeCart = errorMargeCartTracker.asDriver()
        let errorsFaceId = errorFaceId.asDriver()
        
        return Output(fetching: fetching,
                      succefullyLogin: login,
                      mergCartsSuccessfully: mergCartResult,
                      wishList: wishList,
                      error: errors,
                      errorMargeCart: errorMargeCart ,
                      succefullyLoginWithFaceId : loginWithFaceId ,
                      cart : Driver.merge(createCart , getCart), errorFaceId: errorsFaceId, succefullySetFirebaseToken: setFirebaseToken, config: config)
        
        
    }
}

class MergeCart {
    
    public var oldCartGUID : String? = ""
    public var currentCartGUID : String? = ""
    
    init(oldCartGUID : String?  , currentCartGUID : String?  ) {
        self.oldCartGUID = oldCartGUID
        self.currentCartGUID = currentCartGUID
    }
    
}
