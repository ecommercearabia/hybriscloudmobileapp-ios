//
//  LoginViewController.swift
//  FoodCrowdApp
//
//  Created by khalil on 7/20/20.
//  Copyright © 2020 khalil anqawi. All rights reserved.
//

import UIKit
import Network
import RxSwift
import RxCocoa
import KeychainSwift
import LocalAuthentication
import Firebase

class LoginViewController: UIViewController {
    
    @IBOutlet weak var uidTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!{
        didSet{
            passwordTextField.isSecureTextEntry = true
        }
    }
    @IBOutlet weak var rememberMeButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var faceIDBttn: UIButton!
    @IBOutlet weak var faceIdLbl: UILabel!
 
    
    @IBAction func makeAsSelected(_ sender: Any) {
        let button = sender as? UIButton
        button?.isSelected.toggle()
        rememberMePublishSubject.onNext(button?.isSelected ?? false)
        
    }
    
    
    let viewModel = LoginViewModel(userUseCase: NetworkPlatform.UseCaseProvider().makeUserUseCase(),cartUseCase: NetworkPlatform.UseCaseProvider().makeCartUseCase(), wishListuseCase: NetworkPlatform.UseCaseProvider().makeWishListUseCase(), configUseCase: NetworkPlatform.UseCaseProvider().makeConfig() )
    
    let disposeBag = DisposeBag()
    let mergCartSubject = PublishSubject<MergeCart>()
    let wishListSubject = PublishSubject<Void>()
    var rememberMePublishSubject = PublishSubject<Bool>()
    let createCartSubject = PublishSubject<Void>()
    let getCartSubject = PublishSubject<Void>()
    let firebaseTokenSubject = PublishSubject<String>()
    var successSignUp = false
    var isNationalityRequired =   false
    let loginWithFaceIdSubject = PublishSubject<String>()
    var menuVC : MenuViewController?
    var hasReferralCode   = false
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if successSignUp {
            if self.hasReferralCode {
                self.performSegue(withIdentifier: "goWelcomeViewController", sender: nil)
            self.wishListSubject.onNext(())
            } else {
                self.dismiss(animated: true){
                    self.wishListSubject.onNext(())
                    }
            }
        
            //self.loginButton.sendActions(for: .touchUpInside)
      
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        addNavBarImage ()
        showTouchAsId()
        self.uidTextField.text = KeychainSwift().get("userName") ?? ""
        self.passwordTextField.text = KeychainSwift().get("password") ?? ""
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        
        
         let input = LoginViewModel.Input(
            trigger: viewWillAppear,
            email: uidTextField.rx.text.orEmpty.asDriver(),
            password: passwordTextField.rx.text.orEmpty.asDriver(),
            loginTrigger: loginButton.rx.tap.asDriver(),
            mergCart: mergCartSubject.asDriverOnErrorJustComplete(),
            wishListTrigger: wishListSubject.asDriverOnErrorJustComplete(),
            rememberMe: rememberMePublishSubject.asDriverOnErrorJustComplete() ,
            loginWithFaceIdTrigger: loginWithFaceIdSubject.asDriverOnErrorJustComplete(),
            creatCart: createCartSubject.asDriverOnErrorJustComplete() ,
            getCart : getCartSubject.asDriverOnErrorJustComplete(), setFirebaseToken: firebaseTokenSubject.asDriverOnErrorJustComplete())
 
        let output = viewModel.transform(input: input)
        
    
        output.succefullySetFirebaseToken.asObservable().subscribe(onNext: {
            (void) in
            print("")
        }).disposed(by: disposeBag)
        
        output.succefullyLogin.asObservable().subscribe(onNext: { (void) in
            self.menuVC?.needToShowFaceId = true
            UserDefaults.standard.set(self.uidTextField.text, forKey: "emailForFaceId")
            KeychainSwift().set("current", forKey: "user")
            Analytics.logEvent(AnalyticsEventLogin, parameters: [
              AnalyticsParameterMethod: "Login"
              ])
            
            if let guid = KeychainSwift().get("guid") {
                self.getCartSubject.onNext(())
//                self.mergCartSubject.onNext(guid)
//                KeychainSwift().delete("guid")

                
            }
            self.wishListSubject.onNext(())
            if self.rememberMeButton.isSelected {
                KeychainSwift().set(self.uidTextField.text ?? "", forKey: "userName")
                KeychainSwift().set(self.passwordTextField.text ?? "", forKey: "password")
                
            }
            else {
                KeychainSwift().delete("userName")
                KeychainSwift().delete("password")
            }
            
          
            Messaging.messaging().token { token, error in
              if let error = error {
                print("Error fetching FCM registration token: \(error)")
              } else if let token = token {
                print("FCM registration token: \(token)")

                self.firebaseTokenSubject.onNext( token)
              }
            }

           
            
        }).disposed(by: disposeBag)
        
        
        output.cart.asObservable().subscribe(onNext: { (cart) in
            if let guid = KeychainSwift().get("guid") {
                self.mergCartSubject.onNext(MergeCart(oldCartGUID : guid ,currentCartGUID:cart.guid))
                KeychainSwift().delete("guid")
                
            }
                    
        }).disposed(by: disposeBag)
        
        
        output.error.asObservable().subscribe(onNext: { (error) in
            let error = error as? ErrorResponse
            if error?.errors?.first?.type == "AccessDeniedError" {
                if KeychainSwift().get("guid") == nil
                    || KeychainSwift().get("guid") == "current"{
                    self.createCartSubject.onNext(())
                }
                else {
                    return
                }
            }
           else if error?.errors?.first?.message == "Cart not found." {
                self.createCartSubject.onNext(())
            }
           else if error?.errors?.first?.message == "No cart created yet." {
            self.createCartSubject.onNext(())
           }
           else {
            self.errorLoaf(message: "Invalid username or password, Please try again".localized())
           }
        }).disposed(by: disposeBag)
        
        output.wishList.asObservable().subscribe(onNext: { (WishList) in
            self.dismiss(animated: true) {
                KeychainSwift().set(WishList.pk ?? "", forKey: "WishListPK")
            }
            
            
        }).disposed(by: disposeBag)
        
        output.mergCartsSuccessfully.asObservable().subscribe(onNext: { (cart) in
            self.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
        
        output.errorMargeCart.asObservable().subscribe(onNext: { (error) in
            self.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
        // Do any additional setup after loading the view.
        
        output.succefullyLoginWithFaceId.asObservable().subscribe(onNext: { ( data) in
            self.menuVC?.needToShowFaceId = true
           
            Analytics.logEvent(AnalyticsEventLogin, parameters: [
              AnalyticsParameterMethod: "Login With Face Id"
              ])
            if let guid = KeychainSwift().get("guid") {
                self.getCartSubject.onNext(())
//                self.mergCartSubject.onNext(guid)
//                KeychainSwift().delete("guid")

                
            }
            
         

                self.wishListSubject.onNext(())
            
            Messaging.messaging().token { token, error in
              if let error = error {
                print("Error fetching FCM registration token: \(error)")
              } else if let token = token {
                print("FCM registration token: \(token)")

                self.firebaseTokenSubject.onNext( token)
              }
            }
           
            
        }).disposed(by: disposeBag)
        
        output.config.asObservable().subscribe(onNext: { (configration) in

            if configration.registrationConfiguration?.nationalityConfigurations?.enabled  == true {
              
                if configration.registrationConfiguration?.nationalityConfigurations?.configurationsRequired == true {
                    self.isNationalityRequired = true
                } else {
                    self.isNationalityRequired = false
                }
            }
        
        }).disposed(by: disposeBag)
        
        output.errorFaceId.asObservable().subscribe(onNext: { (error) in
            let errors = error as? ErrorResponse
            self.errorLoaf(message: errors?.errors?.first?.message ?? "")
        }).disposed(by: disposeBag)
        
        self.faceIDBttn.rx.tap.subscribe(onNext: { ( void) in
            
            let context = LAContext()
            var error : NSError?
            if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error){
                let reason = "Identify yourself!"
                context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason){
                    [weak self ] success , authenticationError in
                    DispatchQueue.main.async {
                        if success {
                            
                            if let emailForFaceId = UserDefaults.standard.string(forKey: "emailForFaceId") {
                                self?.loginWithFaceIdSubject.onNext(emailForFaceId)
                            }
                            
//                            let alert = self!.alertForFaceID(){
//                               (string) -> Void in
//
//                           }
//
//                            alert.buttonActionCallBack = { email in
//                                self?.loginWithFaceIdSubject.onNext(email)
//                            }
//                            self?.present(alert, animated: true, completion: nil)
//
                        } else {
                            let ac = UIAlertController(title: "Authentication failed".localized(), message: "You could not be verified; please try again.".localized(), preferredStyle: .alert)
                            ac.addAction(UIAlertAction(title: "Ok".localized(), style: .default))
                            self?.present(ac, animated: true)
                        }
                    }
                }
            } else {
                let ac = UIAlertController(title: "Biometry unavailable", message: "Your device is  not configured for biometic authenticaion.".localized(), preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "Ok".localized(), style: .default))
                self.present(ac, animated: true)
            }
        }).disposed(by: disposeBag)
        
    }
    
    
    func showTouchAsId () {
        if KeychainSwift().getBool("SetFaceID") ?? false {
            faceIdLbl.isHidden = false
            faceIDBttn.isHidden = false

        }
        else {
            faceIdLbl.isHidden = true
            faceIDBttn.isHidden = true
         
        }
        
     
        
        let authenticationContext = LAContext()
        let auth = authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        
        switch (authenticationContext.biometryType){
        case .faceID:
            self.faceIDBttn.setImage(UIImage(named: "face id Icon"), for: .normal)
            self.faceIdLbl.text = "Face ID".localized()
            KeychainSwift().set("Face ID", forKey: "IdType")
        case .touchID:
             self.faceIDBttn.setImage(UIImage(named: "TouchID"), for: .normal)
            self.faceIdLbl.text = "Touch ID".localized()
            KeychainSwift().set("Touch ID", forKey: "IdType")
        default:
            break
          
           
        }
        

 
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSignUp" {
            if let nvc = segue.destination as? UINavigationController {
                if let vc = nvc.viewControllers.first as? SignUpViewController {
                    vc.loginVC = self
                    vc.isNationalityRequired = self.isNationalityRequired
                }
            }
        }
    }
    
    @IBAction func backBttnAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    func addNavBarImage () {
        let navController = navigationController!
        let logoImage = #imageLiteral(resourceName: "logo")
        let logoImageView = UIImageView()
        
        let bannerWidth = navController.navigationBar.frame.size.width / 2
        let bannerHeight = navController.navigationBar.frame.size.height / 2
        let bannerX = bannerWidth / 2 - logoImage.size.width / 2
        let bannerY = bannerHeight / 2 - logoImage.size.height / 2
        
        logoImageView.frame = CGRect(x:bannerX , y: bannerY, width: bannerWidth, height: bannerHeight)
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.image = logoImage
        navigationItem.titleView = logoImageView
        
    }
    
    func alertForFaceID( completion: @escaping (String) -> Void) -> popUpAlertViewControllerForFaceID {
            
             
             let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
             let alertVC = storyBoard.instantiateViewController(withIdentifier: "popUpAlertViewControllerForFaceID") as! popUpAlertViewControllerForFaceID
      
         
             alertVC.buttonActionCallBack = completion
              return alertVC
        }
    
}
