//
//  popUpAlertViewControllerForFaceID.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 14/3/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import UIKit
import KeychainSwift
class popUpAlertViewControllerForFaceID: UIViewController {

    @IBOutlet weak var alertTitleLbl: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var BackGroundView: UIView!
    var buttonActionCallBack: ((String) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        BackGroundView.cornerRadius = 20
        BackGroundView.layer.masksToBounds = true
        alertTitleLbl.text = " Login with".localized() + " \( KeychainSwift().get("IdType") ?? "")"
    }
    

    @IBAction func actionButton(_ sender: Any) {
        if emailTextField.text != "" {
            self.dismiss(animated: true) {
                self.buttonActionCallBack?(self.emailTextField.text ?? "")
           }
            
        }
        
        else {
             
            let ac = UIAlertController(title: "failed".localized(), message: "enter your email.".localized(), preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "Ok".localized(), style: .default))
            self.present(ac, animated: true)
        }
        
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
}
