//
//  EmailAddressViewController.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class EmailAddressViewController: UIViewController {

    @IBOutlet weak var newEmailAddressTextField: UITextField!
    @IBOutlet weak var confirmNewEmailAddressTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!{
        didSet{
            passwordTextField.isSecureTextEntry = true
        }
    }
    @IBOutlet weak var saveButton: UIButton!
    
    let disposeBag = DisposeBag()

    var changeEmailPublishSubject = PublishSubject<Void>()
    let loginSubject = PublishSubject<Void>()
    var viewModel : EmailAddressViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
               
        viewModel = EmailAddressViewModel(userUseCase: networkUseCaseProvider.makeUserUseCase())
               
               
        let input = EmailAddressViewModel.Input(
            saveTrigger: saveButton!.rx.tap.asDriver(),
            changeEmailManualTrigger: changeEmailPublishSubject.asObserver().asDriverOnErrorJustComplete(),
            newEmailAddress: newEmailAddressTextField.rx.text.orEmpty.asDriver(),
            confirmNewEmailAddress: confirmNewEmailAddressTextField.rx.text.orEmpty.asDriver(),
            password: passwordTextField.rx.text.orEmpty.asDriver(), loginTrigger: loginSubject.asDriverOnErrorJustComplete())
               
            let output = viewModel.transform(input: input)
        
        output.canChangeEmail.asObservable().subscribe(onNext: { (Bool) in
            if(Bool)
            {
                self.changeEmailPublishSubject.onNext(())
            }
            
            }).disposed(by: disposeBag)
        output.changeEmail.asObservable().subscribe(onNext: { () in
            
            self.successLoaf(message: "Email address has been updated successfully".localized())
            self.loginSubject.onNext(())
            
        }).disposed(by: disposeBag)
        
        output.succefullyLogin.asObservable().subscribe(onNext: {(void) in
                        self.dismiss(animated: true) {
                             self.navigationController?.backToViewController(vc: MenuViewController.self)
                        }
        }).disposed(by: disposeBag)
        
        output.error.asObservable().subscribe(onNext: { (Error) in
            self.errorLoaf(message: "Error occurred")

      }).disposed(by: disposeBag)
    }
    

}
