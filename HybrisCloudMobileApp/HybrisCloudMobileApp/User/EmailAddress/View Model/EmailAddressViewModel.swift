//
//  EmailAddressViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/24/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
class EmailAddressViewModel: ViewModelType{
    
    private let userUseCase: UserUseCase
    private let canChangeEmail = PublishSubject<Void>()

       init(userUseCase: UserUseCase) {
           self.userUseCase = userUseCase
        
       }
    
    struct Input {
        let saveTrigger: Driver<Void>
        let changeEmailManualTrigger : Driver<Void>
        let newEmailAddress: Driver<String>
        let confirmNewEmailAddress: Driver<String>
        let password: Driver<String>
        let loginTrigger: Driver<Void>
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let error: Driver<Error>
        let changeEmail: Driver<Void>
        let canChangeEmail: Driver<Bool>
        let succefullyLogin: Driver<Void>
        
    }
       
    func transform(input: Input) -> Output {
        
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let userCredential = Driver.combineLatest(input.newEmailAddress, input.password)
        let login = input.loginTrigger.withLatestFrom(userCredential)
            .map { (email, password ) in
                return Domain.UserLogin(password: password, uid: email)
            }
            .flatMapLatest { [unowned self] in
                return self.userUseCase.login(user:$0)
                    .trackError(errorTracker)
                    .trackActivity(activityIndicator)
                    .asDriverOnErrorJustComplete()
            }
        
        let checkDataInput = Driver.combineLatest(
            input.newEmailAddress,
            input.confirmNewEmailAddress ,
            input.password)
        
        let canChangeEmail = input.saveTrigger.withLatestFrom(checkDataInput).map { (arg0) -> Bool in

            let (newEmail, confirmNewEmail,password) = arg0
            if((newEmail ==  confirmNewEmail) && (isValidEmail(testStr: newEmail)) && (password.count > 5))
            {
                return true
            } else {
                // handle error
                return false
            }
        }
    
        let changeEmailInput = Driver.combineLatest(
        input.newEmailAddress,
        input.password)
        
        
        
        let changeEmail = input.changeEmailManualTrigger.withLatestFrom(changeEmailInput).map({ (arg0) -> ChangeEmailRequest in
            let (email, password) = arg0
           
                 return ChangeEmailRequest(newLogin: email, password: password)

        }).flatMapLatest { [unowned self] in
            return self.userUseCase.updateEmailAddress(newLogin: $0.newLogin ?? "", password: $0.password ?? "", userId: "current")
                .trackError(errorTracker)
                .trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
        }
       
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching, error: errors, changeEmail: changeEmail, canChangeEmail: canChangeEmail, succefullyLogin: login)
    }
}
func isValidEmail(testStr:String) -> Bool {
    let emailRegEx = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{1,4}$"
    let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}
