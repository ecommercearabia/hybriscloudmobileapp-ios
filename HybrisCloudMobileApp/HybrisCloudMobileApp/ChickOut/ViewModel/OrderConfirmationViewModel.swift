//
//  OrderConfirmationViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 9/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

class OrderConfirmationViewModel: ViewModelType{
    
    var orderResponse : PalceOrderResponse!
   
    struct Input {
        let orderResponse: Driver<PalceOrderResponse>
    }

    struct Output {
        let palceOrder: Driver<PalceOrderResponse>
    }
    
    func transform(input: Input) -> Output {
        
        let outputPalceOrder = input.orderResponse
    
        return Output(palceOrder: outputPalceOrder)
    }
    
    func dataSourceOrderConfirmation() -> RxTableViewSectionedAnimatedDataSource<OrderConfirmationSectionModel>
        {
            return RxTableViewSectionedAnimatedDataSource<OrderConfirmationSectionModel>(animationConfiguration: AnimationConfiguration(insertAnimation: .fade, reloadAnimation: .fade, deleteAnimation: .fade),configureCell: { ds, cv, indexPath, item in
             
               switch item{
                              case .orderDescription(orderResponse: let orderResponse):
                                   guard let cell = cv.dequeueReusableCell(withIdentifier: "orderDescription", for: indexPath) as? ConfirmationDescriptionCell else { return UITableViewCell()}
                                   cell.setUp(placeOrder : orderResponse)
                                   return cell
                                  
                              case .orderSummary(orderResponse: let orderResponse):
                                  guard let cell = cv.dequeueReusableCell(withIdentifier: "orderSummary", for: indexPath) as? ConfirmationSummeryCell else { return UITableViewCell()}
                                      cell.setUp(placeOrder : orderResponse)
                                  return cell
                                  
                              case .shippingAddress(orderResponse: let orderResponse):
                                  guard let cell = cv.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as? AddressCell else { return
                                      UITableViewCell()}
                   let cartModel = CartModel(appliedOrderPromotions: [], appliedProductPromotions: [], appliedVouchers: [], calculated: false, code: orderResponse.code, deliveryAddress: orderResponse.deliveryAddress, deliveryCost: nil, deliveryItemsQuantity: 0, deliveryMode: nil, deliveryOrderGroups: [], cartModelDescription: "", entries: orderResponse.entries, expirationTime: "", guid: orderResponse.guid, name: "", net: false, orderDiscounts: nil, paymentInfo: nil, paymentMode: nil, pickupItemsQuantity: 0, pickupOrderGroups: [], potentialOrderPromotions: [], potentialProductPromotions: [], productDiscounts: nil, saveTime: "", savedBy: nil, site: "", store: "", storeCreditAmount: nil, storeCreditAmountSelected: 0, storeCreditMode: nil, subTotal: nil, subTotalBeforeSavingPrice: nil, timeSlotInfoData: nil, totalDiscounts: nil, totalItems: 0, totalPrice: nil, totalPriceWithTax: nil, totalTax: nil, totalUnitCount: 0, user: nil, shipmentType: orderResponse.shipmentType, cartTimeSlot: nil, express: nil)
                                if let address = orderResponse.deliveryAddress {
                                    cell.setUp(address: address , cart : cartModel)
                                }
                                else if let addressPikup =  orderResponse.entries?.first?.deliveryPointOfService?.address {
                                    cell.setUp(address:  addressPikup , cart : cartModel)
                                }
                                
                                
                                  return cell
                              case .myItems(orderResponse: let orderResponse):
                                  guard let cell = cv.dequeueReusableCell(withIdentifier: "myItems", for: indexPath) as? ConfirmationCartItemsCell else { return
                                      UITableViewCell()}
                                  cell.setUp(placeOrder: orderResponse)
                                  var entryCount = orderResponse.entries?.count
                                  if entryCount ?? 0 >= 2 {entryCount = 2}
                                  cell.heightConstraint.constant = 137 * CGFloat(entryCount ?? Int(0.0))
                                  cell.layoutIfNeeded()

                                  return cell
                            
                              
                          }
        
            })}
}

