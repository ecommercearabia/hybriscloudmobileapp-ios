//
//  BankOfferCellViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by admin on 07/04/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import KeychainSwift
import RxSwift

class OfferCellViewModel: ViewModelType {
    
    var refreshCart  = PublishSubject<Void>()
    let getCartUseCase: CartUseCase
    let disposeBag = DisposeBag()
    var activityIndicator : ActivityIndicator?

    //Init
    init(getCartUseCase: CartUseCase) {
        self.getCartUseCase = getCartUseCase
    }
 
    //Input
    struct Input {
        let voucherCode : Driver<ControlProperty<String?>.Element>
        let applyVoucher : Driver<Void>
        let voucherCodeNeedDelete : Driver<Void>
        let deleteVoucher : Driver<Void>
    }
    
    //Output
    struct Output {
        let fetching: Driver<Bool>
        let error: Driver<Error>
        let applyVoucher: Driver<Void>
        let deletedVoucher: Driver<Void>
        let applyVoucherError: Driver<Error>
    }
    
    //Transform Func
    func transform(input: Input) -> Output {
         let errorTracker = ErrorTracker()
        let applyVoucherErrorTracker = ErrorTracker()
        
        let applyVoucher =  input.applyVoucher.withLatestFrom(input.voucherCode).flatMapLatest { (voucherId) -> Driver<Void> in
            return self.getCartUseCase.appliesVoucher(cartId: KeychainSwift().get("user") ?? "anonymous" == "current" ? "current" : KeychainSwift().get("guid") ?? "", userId: KeychainSwift().get("user") ?? "anonymous" == "current" ? "current" : "anonymous", voucherId: "\((voucherId ?? "").uppercased())")
                
                .trackActivity(self.activityIndicator!)
                .trackError(applyVoucherErrorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let fetching = activityIndicator!.asDriver()
        let errors = errorTracker.asDriver()
        let applyVoucherError = applyVoucherErrorTracker.asDriver()
        
        return Output(fetching: fetching,
                      error: errors,
                      applyVoucher: applyVoucher,
                      deletedVoucher: Driver<Void>.just(()),
                      applyVoucherError: applyVoucherError)
    }
    
}

//MARK:- Bank Offer DataSource Extension
extension OfferCellViewModel {
    func bankOfferDataSource() ->
    RxTableViewSectionedAnimatedDataSource<PromoCodeMsgsSection> {
        return RxTableViewSectionedAnimatedDataSource<PromoCodeMsgsSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .none, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, cv, indexPath, item in
            
            switch item {
            
            case .bankOffer(bankOffer: let bankOffer):
                guard let cell = cv.dequeueReusableCell(withIdentifier: "BankOfferCell", for: indexPath) as? BankOfferCell else {
                    return UITableViewCell()
                }
                cell.setUp(msg: bankOffer.message ?? "", imgUrl: bankOffer.icon?.url ?? "")
                cell.msg.textColor = .black
                cell.msg.font = cell.msg.font.withSize(12.0)
                cell.layoutIfNeeded()
                return cell
                
            case .potentialOrderPromotion(potentialOrderPromotion: let potentialOrderPromotion):
                guard let cell = cv.dequeueReusableCell(withIdentifier: "BankOfferCell", for: indexPath) as? BankOfferCell else {
                    return UITableViewCell()
                }
                cell.setUp(msg: potentialOrderPromotion.potentialOrderPromotionDescription ?? "", imgUrl: nil)
                cell.msg.textColor = UIColor(named: "AppMainColor")
                cell.msg.font = cell.msg.font.withSize(17.0)
                cell.layoutIfNeeded()
                return cell
                
            case .appliedOrderPromotion(appliedOrderPromotion: let appliedOrderPromotion):
                guard let cell = cv.dequeueReusableCell(withIdentifier: "BankOfferCell", for: indexPath) as? BankOfferCell else {
                    return UITableViewCell()
                }
                cell.setUp(msg: appliedOrderPromotion.appliedOrderPromotionDescription ?? "", imgUrl: nil)
                cell.msg.textColor = UIColor(named: "AppMainColor")
                cell.msg.font = cell.msg.font.withSize(17.0)
                
                cell.layoutIfNeeded()
                return cell
                
            }
        },titleForHeaderInSection: { dataSource, index in
            let sectionModel = dataSource.sectionModels[index]
            return "\(sectionModel.model)".localized()
        })
    }
    
}

//MARK:- Voucher Collection View DataSource
extension OfferCellViewModel {
    func dataSourceVoucher() ->
        RxCollectionViewSectionedAnimatedDataSource<VoucherSection> {
            return RxCollectionViewSectionedAnimatedDataSource<VoucherSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .none, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, cv, indexPath, item in
                
                guard let cell = cv.dequeueReusableCell(withReuseIdentifier: "VoucherCell", for: indexPath) as? VoucherCell else {
                    return UICollectionViewCell()
                }
                cell.viewModel.activityIndicator = self.activityIndicator
                cell.setUp(voucher : item)
                cell.refreshCartSubject.bind(to: self.refreshCart).disposed(by: cell.bag)
                
                return cell
                
            })
    }
}
