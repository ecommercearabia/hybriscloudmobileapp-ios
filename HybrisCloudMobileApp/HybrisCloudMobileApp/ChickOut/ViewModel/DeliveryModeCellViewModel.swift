//
//  DeliveryModeCellViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/17/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import NetworkPlatform
import Domain
import RxSwift
import RxDataSources
import SwipeCellKit
import KeychainSwift


class PaymentModesCellViewModel: ViewModelType{

    var activityIndicator : ActivityIndicator?

    struct Input {
         let trigger: Driver<Void>
         let paymentModesModesModel: Driver<PaymentModesModel>
         let cart: Driver<CartModel>
         let selectPaymentMode:Driver<PaymentMode>
     }
     
     struct Output {
         let fetching: Driver<Bool>
         var paymentModes: Driver<[PaymentMode]>
         let selectpaymentMode: Driver<String>
         let refreshCart: Driver<Void>
         let error: Driver<Error>
     }

    func transform(input: Input) -> Output {
        
    let errorTracker = ErrorTracker()

        let paymentModes = input.paymentModesModesModel.flatMap { (model) in
            return Driver.just(model.paymentModes ?? [])
        }
        
        let selectedPaymentMode = input.cart.flatMapLatest { (cart) in

            return Driver.just(cart.paymentMode?.code ?? "").trackActivity(self.activityIndicator!)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
             }

        
        let selectPaymentMode = input.selectPaymentMode.flatMapLatest { (mode) in
            return NetworkPlatform.UseCaseProvider().makePaymentUseCase().setPaymentMode(paymentModeCode: mode.code ?? "")
                .trackActivity(self.activityIndicator!)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }


        let fetching = activityIndicator!.asDriver()
        let errors = errorTracker.asDriver()

        return Output.init(fetching: fetching, paymentModes: paymentModes, selectpaymentMode: selectedPaymentMode, refreshCart: selectPaymentMode.mapToVoid(), error: errors)
    }
    
}
