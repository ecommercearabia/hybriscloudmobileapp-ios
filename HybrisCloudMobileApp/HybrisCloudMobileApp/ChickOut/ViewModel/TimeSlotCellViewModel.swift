//
//  TimeSlotCellViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Ahmad Bader on 8/17/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public class TimeSlotCellViewModel: ViewModelType{
    var activityIndicator : ActivityIndicator?
    let getDeliveryTypesUseCase: DeliveryTypesUseCase
    
    init(getDeliveryTypesUseCase: DeliveryTypesUseCase ) {
        self.getDeliveryTypesUseCase = getDeliveryTypesUseCase
        
    }
    struct Input {
         let trigger: Driver<Void>
         let timeSlotModel: Driver<TimeSlotsModel>
         let cart: Driver<CartModel>
         let selectTimeSlotDay:Driver<TimeSlotDay>
         let selectTimeSlotPeriod:Driver<Period>
         let expressTrigger : Driver<Void>
         let setExpress : Driver<String>
     }
     
     struct Output {
         let fetching: Driver<Bool>
         let days: Driver<[TimeSlotDay]>
         let periods: Driver<[Period]>
         let selectedDay: Driver<String>
         let selectedPeriod: Driver<String>
         let refreshCart: Driver<Void>
         let deliveryTypes : Driver<DeliveryTypesModel>
         let deliveryType : Driver<String>
         let setdeliveryType : Driver<CartModel>
         let error: Driver<Error>
       
     }

    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()

        
        let getDeliveryTypes = input.trigger.flatMapLatest {
            return self.getDeliveryTypesUseCase.getDeliveryTypes()
            .trackActivity(self.activityIndicator!)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
             }
        
        let getDeliveryType = input.expressTrigger.flatMapLatest {
            return self.getDeliveryTypesUseCase.getDeliveryType()
            .trackActivity(self.activityIndicator!)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
             }
        
        let setDeliveryType = input.setExpress.flatMapLatest { (deliveryType) in

            return self.getDeliveryTypesUseCase.setDeliveryType(deliveryType: deliveryType)
            .trackActivity(self.activityIndicator!)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
             }
        
        let setTimeSlot = Driver.combineLatest(input.selectTimeSlotDay,input.selectTimeSlotPeriod).flatMapLatest { (day,period) in
            return  NetworkPlatform.UseCaseProvider().makeChickOutUseCase().setTimeSlots(
                cartId: "current",
                timeSlotInfos: SetTimeSlotModel.init(date: day.date, day: day.day, end: period.end,
                periodCode: period.code, start: period.start))
                .trackActivity(self.activityIndicator!)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
            
            
        }
        
        let selectedDay = input.cart.flatMapLatest { (cart)  in

            return Driver.just(cart.timeSlotInfoData?.day ?? "").trackActivity(activityIndicator)
                           .trackError(errorTracker)
                           .asDriverOnErrorJustComplete()
        }
        
        let selectedPeriod = input.cart.flatMapLatest { (cart) in

            return Driver.just(cart.timeSlotInfoData?.periodCode ?? "").trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
             }
        
        let days = input.timeSlotModel.flatMap { (model) in
            return Driver.just(model.timeSlotDays ?? [])
        }
        let periods = input.selectTimeSlotDay.flatMap { (model)  in
            return Driver.just(model.periods ?? [])
        }
 
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        
        return Output.init(fetching: fetching, days: days,periods: periods,selectedDay: selectedDay,selectedPeriod: selectedPeriod,refreshCart: setTimeSlot , deliveryTypes: getDeliveryTypes , deliveryType: getDeliveryType  , setdeliveryType: setDeliveryType, error: errors)
    }
    
   
    
    
  
}
