//
//  StoreCreditModesCellVM.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 9/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import NetworkPlatform
import Domain
import RxSwift
import RxDataSources
import Foundation
import UIKit
class StoreCreditModesCellVM: ViewModelType {

    var activityIndicator : ActivityIndicator? = nil

    
    struct Input {
         let trigger: Driver<Void>
         let modesSubject: Driver<StoreCreditModes>
         let cart: Driver<CartModel>
         let selectStoreCreditMode:Driver<StoreCreditMode>
         let selectStoreCreditModeManual:Driver<StoreCreditMode>
         let AvailableAmountModel: Driver<StoreCreditAvailableAmountModel>
         let spacificAmount: Driver<String>
         let doneTrigger: Driver<Void>

         
     }
     
     struct Output {
         let fetching: Driver<Bool>
        let storeCreditModes: Driver<[StoreCreditMode]>
        let selectStoreCreditMode: Driver<String>
         let refreshCart: Driver<Void>
        let AvailableAmountModel: Driver<StoreCreditAvailableAmountModel>
        let selectStoreCreditModeManual:Driver<String>
        let spacificAmount:Driver<String>
        let cart:Driver<CartModel>
         let error: Driver<Error>
     }

    func transform(input: Input) -> Output {
    let errorTracker = ErrorTracker()

        let modesSubject = input.modesSubject.flatMap { (model) in
            return Driver.just(model.storeCreditModes ?? [])
        }
 
        let selectStoreCreditMode = input.cart.flatMapLatest { (cart) in
            return Driver.just(cart.storeCreditMode?.storeCreditModeType?.code ?? "").trackActivity(self.activityIndicator!)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
             }
        
        let selectStoreCreditModeManual = input.selectStoreCreditModeManual.flatMapLatest { (mode) in
            return Driver.just(mode.storeCreditModeType?.code ?? "").trackActivity(self.activityIndicator!)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
             }

 
        let setStoreCredit = input.doneTrigger.withLatestFrom(Driver.combineLatest(input.selectStoreCreditMode , input.spacificAmount , input.doneTrigger)) .flatMapLatest { (arg0) ->  Driver<Void> in
            let (mode, amount, _) = arg0
            return NetworkPlatform.UseCaseProvider().makeStroyCreditCartUseCase().setStoreCreditMode(amount: (amount as NSString).doubleValue , cartId: "current", storeCreditModeTypeCode: mode.storeCreditModeType?.code ?? "", userId: "current")
                            .trackActivity(self.activityIndicator!)
                            .trackError(errorTracker)
                            .asDriverOnErrorJustComplete()
        }
 
        let spacificAmountSelected = input.cart.flatMapLatest { (cart) in
            return Driver.just(String(cart.storeCreditAmountSelected ?? 0.0)).trackActivity(self.activityIndicator!)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()

        }
        
        let cart = input.cart.flatMapLatest { (cart) in
            return Driver.just(cart).trackActivity(self.activityIndicator!)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()

        }


        let fetching = self.activityIndicator!.asDriver()
        let errors = errorTracker.asDriver()

        
        return Output.init(fetching: fetching, storeCreditModes: modesSubject, selectStoreCreditMode: selectStoreCreditMode, refreshCart: setStoreCredit.mapToVoid(), AvailableAmountModel: input.AvailableAmountModel, selectStoreCreditModeManual: selectStoreCreditModeManual, spacificAmount: spacificAmountSelected, cart: cart, error: errors)
    }
 
}
