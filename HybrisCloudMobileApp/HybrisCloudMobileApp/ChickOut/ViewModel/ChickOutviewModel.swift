//
//  chickOutviewModel.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import NetworkPlatform
import Domain
import RxSwift
import RxDataSources
import SwipeCellKit
import KeychainSwift
class ChickOutviewModel: ViewModelType{
    
    let deliveryAddressSubject = BehaviorSubject<Address?>(value: nil)
    let billingAddressSubject = BehaviorSubject<Address?>(value: nil)
    let paymentModeSubject = BehaviorSubject<PaymentMode?>(value: nil)
    let timeSlotSubject = BehaviorSubject<TimeSlotDay?>(value: nil)
    let cartModelSubject = BehaviorSubject<CartModel?>(value: nil)
    var chickOutSubject = BehaviorSubject<[ChcikOutSectionModel]>(value: [])
    var chickOutSubjectFull = BehaviorSubject<[ChcikOutSectionModel]>(value: [])
    var refreshCart  = PublishSubject<Void>()
    var cartModel : CartModel!
    var bankOffersModel: BankOffers!
    var createShipmentType  = ""
    var refreshCellUiSizeManual = PublishSubject<Void>()
    let activityIndicator = ActivityIndicator()
    var selctedDeliveryType = BehaviorSubject(value: "")
    struct Input {
        let trigger: Driver<Void>
        let setDeliveryAddress:Driver<Address?>
        let setBillingAddress:Driver<Address?>
        let setDeliveryMode: Driver<Void>
        let refreshCart:Driver<Void>
        let setPaymentMode:Driver<PaymentMode?>
        let setPaymentDetils:Driver<(PaymentGatewayResponse,Bool)>
        let placeOrder:Driver<CartModel>
        let cartId : Driver<String?>
        let getPaymentInfo : Driver<Void>
        let validateCart : Driver<Void>
        let getApplePayInfo : Driver<Void>
        
        let setApplePay : Driver<Void>
        
        let paymentOption :  Driver<String?>
        let cardType :  Driver<String?>
        let cardName :  Driver<String?>
        let paymentData :  Driver<String?>
        let appleCardType :  Driver<String?>
        let amount :  Driver<String?>
        let currency :  Driver<String?>
        let orderId :  Driver<String?>
        let createShipmentType : Driver<String>
        let selectPaymentMode:Driver<PaymentMode>
    }
    struct Output {
        let fetching: Driver<Bool>
        let timeSlots: Driver<TimeSlotsModel>
        var cart: Driver<CartModel>
        let error: Driver<Error>
        let setDeliveryMode: Driver<Void>
        let paymentModes : Driver<PaymentModesModel>
        let paymentInfo : Driver<PaymentInfo>
        let placeOrder:Driver<PalceOrderResponse>
        let paymentTransactionResponse:Driver<PaymentDetailsResponse>
        let storeCreditModes:Driver<StoreCreditModes>
        let availableStoreCredit:Driver<StoreCreditAvailableAmountModel>
        let validationCartErrors:Driver<ValidationCartErrors>
        let errorPlaceOrder: Driver<Error>
        let errorSetPayment: Driver<Error>
        let bankOffers: Driver<BankOffers>
        let applePayInfo : Driver<PaymentInfo>
        let applePayResponse : Driver<ApplePayModel>
        let selectpaymentMode: Driver<SetPaymentModeResponse>
        let config : Driver<ConfigModel>
    }
    
    private let bankOffersUseCase: BankOffersCartUseCase
    private let useCase: chickOutUseCase
    private let deliveryModeUseCase: setDeliveryModeUseCase
    private let getCartUseCase: CartUseCase
    private let setDeliveryMode: setDeliveryModeUseCase
    private let getPaymentModes: paymentModeUseCase
    private let paymentInfoUseCase: PaymentInfoUseCase
    private let paymentdetilsUseCase: PaymentDetailsUseCase
    private let placeOrderUseCase: PlaceOrderUserCase
    private let storeCreditCartUseCase: StoreCreditCartUseCase
    let validationsCartErrors: ValidationCartUseCase
    let applePayUseCase: applePayUseCase
    private let configUseCase: ConfigUseCase
    var cartCode = String()
    init(useCase: chickOutUseCase , deliveryModeUseCase : setDeliveryModeUseCase , getCartUseCase: CartUseCase ,setDeliveryMode : setDeliveryModeUseCase ,getPaymentModes : paymentModeUseCase, paymentInfoUseCase: PaymentInfoUseCase, paymentdetilsUseCase: PaymentDetailsUseCase,placeOrderUseCase: PlaceOrderUserCase , storeCreditCartUseCase: StoreCreditCartUseCase, validationsCartErrors: ValidationCartUseCase,bankOffersUseCase : BankOffersCartUseCase , applePayUseCase : applePayUseCase , configUseCase: ConfigUseCase ) {
        
        self.useCase = useCase
        self.deliveryModeUseCase = deliveryModeUseCase
        self.getCartUseCase = getCartUseCase
        self.setDeliveryMode = setDeliveryMode
        self.getPaymentModes = getPaymentModes
        self.paymentInfoUseCase = paymentInfoUseCase
        self.paymentdetilsUseCase = paymentdetilsUseCase
        self.placeOrderUseCase = placeOrderUseCase
        self.storeCreditCartUseCase = storeCreditCartUseCase
        self.validationsCartErrors = validationsCartErrors
        self.bankOffersUseCase = bankOffersUseCase
        self.applePayUseCase = applePayUseCase
        self.configUseCase = configUseCase
    }
    
    
    
    func transform(input: Input) -> Output {
        let errorTracker = ErrorTracker()
        let errorPlaceOrderTracker = ErrorTracker()
        let errorSetPaymentDetils = ErrorTracker()
        
        let config = input.trigger.flatMapLatest{
            return self.configUseCase.config()
                .trackError(errorTracker)
                .trackActivity(self.activityIndicator)
                    .asDriverOnErrorJustComplete()

        }
        
        let selectPaymentMode = input.selectPaymentMode.flatMapLatest { (mode) in
            return  self.getPaymentModes.setPaymentMode(paymentModeCode: mode.code ?? "")
                .trackActivity(self.activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }

        let setDeliveryMode = input.trigger.withLatestFrom(input.createShipmentType).flatMapLatest{ [ self] in
            return self.setDeliveryMode.setDeliveryMode(deliveryModeId: $0, cartId: "current")
                .trackActivity(self.activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let bankOffers =  input.trigger.flatMapLatest { [self] (void) -> Driver<BankOffers> in
            return self.bankOffersUseCase.bankOffersCart()
                .trackActivity(self.activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let timeSlots = input.setDeliveryMode.flatMapLatest { [unowned self] in
            return self.useCase.getTimeSlots()
                .trackActivity(self.activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
 
 
        let cart = Driver.merge(input.setDeliveryMode,input.refreshCart).flatMapLatest { [unowned self] in

            return self.getCartUseCase.getCart(userId: "current", cartId:  "current").delaySubscription(RxTimeInterval.milliseconds(2000), scheduler: MainScheduler.instance)
                .trackActivity(self.activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }

        
        let getPaymentModes = Driver.merge(input.setDeliveryMode , input.refreshCart).flatMapLatest { [unowned self] in

            return self.getPaymentModes.getPaymentModes().delaySubscription(RxTimeInterval.milliseconds(1000), scheduler: MainScheduler.instance)
                .trackActivity(self.activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let paymentInfo = input.getPaymentInfo.flatMapLatest { (mode) in
            return self.paymentInfoUseCase.cartPaymentInfo()
                .trackActivity(self.activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        let applePayInfo = input.getApplePayInfo.flatMapLatest { (mode) in
            return self.paymentInfoUseCase.cartPaymentInfo()
                .trackActivity(self.activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
       
        let applePayData = Driver.combineLatest(input.amount, input.appleCardType , input.cardName , input.cardType , input.cartId , input.currency , input.orderId , input.paymentData   )

        let setApplePay = input.setApplePay.withLatestFrom(Driver.combineLatest(applePayData,input.paymentOption)).flatMapLatest { (arg0) -> Driver<ApplePayModel> in
            
            var ((amount ,appleCardType ,cardName ,cardType,cartId,currency,orderId,paymentData), paymentOption) = arg0
            
            return self.applePayUseCase.setApplePay(paymentOption: paymentOption ?? "", cardType: cardType ?? "", cardName: cardName ?? "", paymentData: paymentData ?? "", appleCardType: appleCardType ?? "", amount: amount ?? "", currency: currency ?? "", orderId: orderId ?? "", userId: "current", cartId: "current")
                            .trackActivity(self.activityIndicator)
                            .trackError(errorTracker)
                            .asDriverOnErrorJustComplete()
            
       
        }
        
        let transaction = input.setPaymentDetils.flatMapLatest { (pr,success)  in
            return self.paymentdetilsUseCase.cartPaymentDetials(trackingId: pr.trackingID ?? "")
                .trackActivity(self.activityIndicator)
                .trackError(errorSetPaymentDetils)
                .asDriverOnErrorJustComplete()
        }
        
        
        let placeorder =  input.placeOrder.flatMapLatest { (cart) in
            return self.placeOrderUseCase.setPlaceOrderUserCase(cartId: cart.code ?? "")
                .trackActivity(self.activityIndicator)
                .trackError(errorPlaceOrderTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let storeCreditModes = input.setDeliveryMode.flatMapLatest { (cart) in
            return self.storeCreditCartUseCase.getStoreCreditModes(cartId: "current", userId: "current")
                .trackActivity(self.activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let availableStoreCredit =  input.setDeliveryMode.flatMapLatest { (cart) in
            return self.storeCreditCartUseCase.getStoreCreditAvailableAmount(userId: "current")
                .trackActivity(self.activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let validationCartErrors = input.validateCart.flatMapLatest { [unowned self] in
            return self.validationsCartErrors.validationCart(userId: "current" , cartId: "current")
                .delaySubscription(RxTimeInterval.milliseconds(2500), scheduler: MainScheduler.instance)
                .trackActivity(self.activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        
        let fetching = self.activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        let errorsPlaceOrder = errorPlaceOrderTracker.asDriver()
        let errorSetPayment = errorSetPaymentDetils.asDriver()
        
        return Output(fetching: fetching,
                      timeSlots: timeSlots,
                      cart: cart,
                      error: errors,
                      setDeliveryMode : setDeliveryMode ,
                      paymentModes : getPaymentModes,
                      paymentInfo: paymentInfo,
                      placeOrder: placeorder,
                      paymentTransactionResponse: transaction,
                      storeCreditModes: storeCreditModes,
                      availableStoreCredit:availableStoreCredit,
                      validationCartErrors: validationCartErrors,
                      errorPlaceOrder: errorsPlaceOrder,
                      errorSetPayment: errorSetPayment,
                      bankOffers: bankOffers , applePayInfo: applePayInfo, applePayResponse: setApplePay, selectpaymentMode: selectPaymentMode, config: config
        )
    }
    
    //    func getPatmentDetailsResponse ( pr : PaymentGatewayResponse?)-> Observable<PaymentDetailsResponse>?{
    //        let self.activityIndicator = self.activityIndicator()
    //        let errorTracker = ErrorTracker()
    //        guard pr != nil else {return nil}
    //        return self.paymentdetilsUseCase.cartPaymentDetials(paymentDetails: PaymentDetailsRequest.init(accountHolderName: cartModel.user?.name ?? "",
    //                billingAddress: Address.init(addressName: cartModel.deliveryAddress?.addressName ?? "",
    //                                             apartmentNumber: cartModel.deliveryAddress?.apartmentNumber ?? "",
    //                    area: Area.init(code: cartModel.deliveryAddress?.area?.code ?? "", name: cartModel.deliveryAddress?.area?.name ?? ""),
    //    cellphone: cartModel.deliveryAddress?.cellphone ?? "",
    //    buildingName: cartModel.deliveryAddress?.buildingName ?? "",
    //    city: City.init(areas: [Area.init(code: cartModel.deliveryAddress?.city?.code ?? "", name: cartModel.deliveryAddress?.city?.name ?? "")],
    //    code: cartModel.deliveryAddress?.city?.code ?? "", name: cartModel.deliveryAddress?.city?.name ?? ""),
    //    companyName: cartModel.deliveryAddress?.city?.name ?? "",
    //    country: Country.init(isdcode: "971" , isocode: "AE", name: "United Arab Emirates"),
    //    defaultAddress: true,
    //    district: cartModel.deliveryAddress?.district ?? "",
    //    email: cartModel.user?.uid ?? "",
    //    firstName: cartModel.deliveryAddress?.firstName ?? "",
    //    formattedAddress: cartModel.deliveryAddress?.formattedAddress ?? "",
    //    id: cartModel.code ?? "",
    //    lastName: cartModel.deliveryAddress?.lastName ?? "",
    //    line1: cartModel.deliveryAddress?.line1 ?? "",
    //    line2: cartModel.deliveryAddress?.line2 ?? "",
    //    mobileCountry: Country.init(isdcode: "971",
    //    isocode: "AED", name: cartModel.deliveryAddress?.mobileCountry?.name ?? ""),
    //    mobileNumber: cartModel.deliveryAddress?.mobileNumber ?? "" , nearestLandmark: cartModel.deliveryAddress?.nearestLandmark ?? "", phone: cartModel.deliveryAddress?.phone ?? "", postalCode: "00000",
    //    region: nil,
    //    shippingAddress: cartModel.deliveryAddress?.shippingAddress ?? true,
    //    title: cartModel.deliveryAddress?.title ?? "" ,
    //    titleCode: cartModel.deliveryAddress?.titleCode ?? "",
    //    town: cartModel.deliveryAddress?.town ?? "town",
    //    visibleInAddressBook: cartModel.deliveryAddress?.visibleInAddressBook ?? true,
    //    streetName: cartModel.deliveryAddress?.streetName ?? ""),
    //                cardNumber: "5123456789012346",
    //                cardType: CardType.init(code: "visa", name: nil),
    //    defaultPayment: true,
    //    expiryMonth: "05",
    //    expiryYear: "2021",
    //    id: "5123456789012346",
    //    issueNumber: "",
    //    saved: true,
    //    startMonth: "",
    //    startYear: "",
    //    subscriptionID: "")).trackActivity(self.activityIndicator).trackError(errorTracker)
    //
    //    }
    
    
    func dataSourceChickOut() -> RxTableViewSectionedAnimatedDataSource<ChcikOutSectionModel>
    {
        return RxTableViewSectionedAnimatedDataSource<ChcikOutSectionModel>(animationConfiguration: AnimationConfiguration(insertAnimation: .none, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, cv, indexPath, item in
               
            switch item{
            
            case .timeSlots(let cart,TimeSlots: let timeSlots):
                guard let cell = cv.dequeueReusableCell(withIdentifier: "DeliverySlotCell", for: indexPath) as? DeliverySlotCell else { return UITableViewCell()}
               cell.viewModel.activityIndicator = self.activityIndicator
                
                cell.setUp(selctedDeliveryType: try! self.selctedDeliveryType.value())
                cell.timeSlotModelSubject.onNext(timeSlots)
                cell.cartModelSubject.onNext(cart)
                cell.refreshCart.bind(to: self.refreshCart).disposed(by: cell.bag)
                cell.selctedDeliveryType.bind(to: self.selctedDeliveryType).disposed(by: cell.bag)
                return cell
                
            case .appliedOrderPromotion(appliedOrderPromotion: let appliedOrderPromotionModel):
                print(appliedOrderPromotionModel)
                let cell = UITableViewCell()
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.font = UIFont.systemFont(ofSize: 13)
                cell.textLabel?.textColor = UIColor(named: "AppMainColor")
                cell.textLabel?.text = appliedOrderPromotionModel.appliedOrderPromotionDescription ?? ""
                return cell
            case .potentialOrderPromotion(potentialOrderPromotion: let potentialOrderPromotionModel):
                let cell = UITableViewCell()
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.font = UIFont.systemFont(ofSize: 13)
                cell.textLabel?.textColor = UIColor(named: "AppMainColor")
                cell.textLabel?.text = potentialOrderPromotionModel.potentialOrderPromotionDescription ?? ""
                return cell
            case .bankOffer(let cart,bankOffer: let bankOffer):
                guard let cell = cv.dequeueReusableCell(withIdentifier: "OffersTableViewCell", for: indexPath) as? OffersTableViewCell else {
                    return UITableViewCell()
                }
                cell.offerCellViewModel.activityIndicator = self.activityIndicator
                cell.setupCell()
                cell.bankOffersModelSubject.onNext(bankOffer)
                cell.cartModelSubject.onNext(cart)
                cell.offerCellViewModel.refreshCart.bind(to: self.refreshCart).disposed(by: cell.disposeBag)
                return cell
                
            case .paymentMode(let cart,PaymentModes: let PaymentModes):
                guard let cell = cv.dequeueReusableCell(withIdentifier: "PaymentModesCell", for: indexPath) as? PaymentModesCell else { return UITableViewCell()}
                cell.activityIndicator = self.activityIndicator
                cell.setUp()
                cell.modesSubject.onNext(PaymentModes)
                cell.cartModelSubject.onNext(cart)
                cell.refreshCart.bind(to: self.refreshCart).disposed(by: cell.bag)
                
                let entryCount = PaymentModes.paymentModes?.count
                cell.hightConstraint.constant = (44 * CGFloat(entryCount ?? Int(0.0))) + 36.5
                cell.layoutIfNeeded()
                
                return cell
                
            case .storeCreditModes(cartModel: let cartModel, CreditModes: let CreditModes ,availableAmount: let availableAmount ,  confg :let config):
                guard let cell = cv.dequeueReusableCell(withIdentifier: "storeCreditModesCell", for: indexPath) as? storeCreditModesCell else { return UITableViewCell()}
                let entryCount = CreditModes.storeCreditModes?.count
                cell.viewModel.activityIndicator = self.activityIndicator
                cell.setUp(count : entryCount ?? 0)
                cell.modesSubject.onNext(CreditModes)
                cell.cartModelSubject.onNext(cartModel)
                cell.availableAmountModel.onNext(availableAmount)
                cell.config.onNext(config)
                if let mode = cartModel.storeCreditMode.map({ (mode) -> StoreCreditMode in
                    return StoreCreditMode(name: mode.name ?? "", storeCreditModeType: StoreCreditModeType(code: mode.storeCreditModeType?.code ?? ""))
                }) {
                    cell.selectModeManual.onNext(mode)
                }
                cell.refreshCart.bind(to: self.refreshCart).disposed(by: cell.bag)
                
                cell.tableView.rx.modelSelected(StoreCreditMode.self).subscribe(onNext: { (storeCreditMode) in
                    self.refreshCellUiSizeManual.onNext(())
                }).disposed(by: cell.bag)
                
                if (config.storeCreditConfiguration?.enableCheckTotalPriceWithStoreCreditLimit ?? false ){
                    if cartModel.storeCreditMode?.storeCreditModeType?.code == "REDEEM_SPECIFIC_AMOUNT" {
                        cell.hightConstraint.constant = (44 * CGFloat(entryCount ?? Int(0.0))) + 185
                    }
                    else {
                        cell.hightConstraint.constant = (44 * CGFloat(entryCount ?? Int(0.0))) + 135.0
                    }
                } else {
                    if cartModel.storeCreditMode?.storeCreditModeType?.code == "REDEEM_SPECIFIC_AMOUNT" {
                        cell.hightConstraint.constant = (44 * CGFloat(entryCount ?? Int(0.0))) + 135
                    }
                    else {
                        cell.hightConstraint.constant = (44 * CGFloat(entryCount ?? Int(0.0))) + 90.0
                    }
                }
              
                
                
                
                cell.layoutIfNeeded()
                
                
                return cell
                
            case .orderSummary(cartModel: let cartModel):
                guard let cell = cv.dequeueReusableCell(withIdentifier: "SummeryCell", for: indexPath) as? SummeryCell else { return UITableViewCell()}
                cell.setUp(cart: cartModel)
                return cell
                
            case .shippingAddress(cartModel: let cartModel):
                guard let cell = cv.dequeueReusableCell(withIdentifier: "AddressViewCell", for: indexPath) as? AddressViewCell else { return
                    UITableViewCell()}
                if let address = cartModel.deliveryAddress {
                    
                    cell.setUp(address: address , cart : cartModel)
                }
                
                
                else if let addressPikup =  cartModel.entries?.first?.deliveryPointOfService?.address {
                    cell.setUp(address:  addressPikup , cart : cartModel)
                }
                return cell
            case .myItems(cartModel: let cartModel):
                guard let cell = cv.dequeueReusableCell(withIdentifier: "CartItemsCell", for: indexPath) as? CartItemsCell else { return
                    UITableViewCell()}
                cell.setUp(cart: cartModel)
                var entryCount = cartModel.entries?.count
                if entryCount ?? 0 >= 2 {entryCount = 2}
                cell.heightConstraint.constant = 137 * CGFloat(entryCount ?? Int(0.0))
                cell.layoutIfNeeded()
                
                return cell
            }
            
        })
    }
}

//MARK: Bank Offer 
//extension ChickOutviewModel {
//    func dataSourceBankOffers() ->
//        RxTableViewSectionedAnimatedDataSource<PromoCodeMsgsSection> {
//            return RxTableViewSectionedAnimatedDataSource<PromoCodeMsgsSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .none, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, cv, indexPath, item in
//
//                switch item {
//
//                case .bankOffer(bankOffer: let bankOffer):
//                    guard let cell = cv.dequeueReusableCell(withIdentifier: "BankOfferCell", for: indexPath) as? BankOfferCell else {
//                        return UITableViewCell()
//                    }
//                    cell.setUp(msg: bankOffer.message ?? "", imgUrl: bankOffer.icon?.url ?? "")
//                    cell.msg.textColor = .black
//                    cell.msg.font = cell.msg.font.withSize(12.0)
//                    cell.layoutIfNeeded()
//                    return cell
//
//                case .potentialOrderPromotion(potentialOrderPromotion: let potentialOrderPromotion):
//                    guard let cell = cv.dequeueReusableCell(withIdentifier: "BankOfferCell", for: indexPath) as? BankOfferCell else {
//                        return UITableViewCell()
//                    }
//                    cell.setUp(msg: potentialOrderPromotion.potentialOrderPromotionDescription ?? "", imgUrl: nil)
//                    cell.msg.textColor = UIColor(named: "AppMainColor")
//                    cell.msg.font = cell.msg.font.withSize(17.0)
//                    cell.layoutIfNeeded()
//                    return cell
//
//                case .appliedOrderPromotion(appliedOrderPromotion: let appliedOrderPromotion):
//                    guard let cell = cv.dequeueReusableCell(withIdentifier: "BankOfferCell", for: indexPath) as? BankOfferCell else {
//                        return UITableViewCell()
//                    }
//                    cell.setUp(msg: appliedOrderPromotion.appliedOrderPromotionDescription ?? "", imgUrl: nil)
//                    cell.msg.textColor = UIColor(named: "AppMainColor")
//                    cell.msg.font = cell.msg.font.withSize(17.0)
//
//                    cell.layoutIfNeeded()
//                    return cell
//
//                }
//             },titleForHeaderInSection: { dataSource, index in
//                let sectionModel = dataSource.sectionModels[index]
//                return "\(sectionModel.model)".localized()
//                 })
//    }
//}
