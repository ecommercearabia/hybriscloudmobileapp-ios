//
//  ChickOutStep2ViewController.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources
import Network
import CCAvenueSDK
import MBProgressHUD
import RappleProgressHUD
import PassKit
import FirebaseAnalytics
import WebKit

struct SectionHeader {
    var lbl : String?
    var img : String?
}

class ChickOutStep2ViewController: UIViewController ,CCAvenuePaymentControllerDelegate,WKNavigationDelegate{
    
    fileprivate var gatewayScheme: String = "gatewaysdk"
       fileprivate var gatewayHost: String = "applepay"
       fileprivate var gatewayResultParam: String = "response"
   // var paymentControllercc:CCMainViewController?
    var sectionHeader = [SectionHeader]()
    //
    @IBOutlet weak var tableView: UITableView!
    
    let bag = DisposeBag()
    var expandData = [NSMutableDictionary]()
    var cartId = BehaviorSubject<String?>(value: nil)
    var Address : Address?
    var viewModel:ChickOutviewModel!
    let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
    let setPaymentDetilsSubject = PublishSubject<(PaymentGatewayResponse,Bool)>()
    let placeOrderSubject = PublishSubject<CartModel>()
    var carts : CartsModel?
    var createShipmentType = BehaviorSubject<String>(value: "fc-ae")
    var selectPaymentModeSubject = PublishSubject<PaymentMode>()
    var paymentModeApplePay : PaymentMode?
    @IBOutlet weak var applePayWebView: WKWebView!
    
    //   let placeOrderAfterPaymentSuccessSubject = BehaviorSubject<Void>(value: ())
    let setPaymentDetailsSubject = BehaviorSubject<CartModel?>(value: nil)
    let getPaymentInfoSubject = PublishSubject<Void>()
    let validationCart = PublishSubject<Void>()
    let deliveryModeSubject = PublishSubject<Void>()
    @IBOutlet weak var switchBut: UISwitch!
    @IBOutlet weak var placeOrderBut: DesignableButton!
    @IBOutlet weak var ApplePayView: UIView!
    @IBOutlet weak var constraintApplePayView: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraintApplePayView: NSLayoutConstraint!
    let getApplePayInfoSubject = PublishSubject<Void>()
    var cartData =  [CartEntry]()
    var paymentSummaryItems = [PKPaymentSummaryItem]()
    var paymentStatus = PKPaymentAuthorizationStatus.failure
    let SupportedPaymentNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard]
    // Add in any extra support payments.
    let ApplePayMerchantID = "merchant.FoodCrowd.com.erabia.mobile.ios"
    // Fill in your merchant ID here!
    var paymentController: PKPaymentAuthorizationController?
    
    var paymentOption = BehaviorSubject<String?>(value: "OPTAPLPY")
    var cardType = BehaviorSubject<String?>(value: "")
    var cardName = BehaviorSubject<String?>(value: "")
    var paymentData = BehaviorSubject<String?>(value: "")
    var appleCardType = BehaviorSubject<String?>(value: "")
    var amount = BehaviorSubject<String?>(value: "")
    var currency = BehaviorSubject<String?>(value: "")
    var orderId = BehaviorSubject<String?>(value: "")
    let applePaySubject = PublishSubject<Void>()
    
    var applePayBttn = PKPaymentButton(paymentButtonType: .plain, paymentButtonStyle: .black)
    var showTimeSlot = false
    
    // Our app will support all available networks in Apple Pay.
    static let supportedNetworks = [
        PKPaymentNetwork.amex,
        PKPaymentNetwork.discover,
        PKPaymentNetwork.masterCard,
        PKPaymentNetwork.visa
    ]
    

    override func viewDidLoad() {
      
        tableView.rx.setDelegate(self).disposed(by: bag)
        
        registerNibs()
        
        self.constraintApplePayView.constant = 0
        self.leadingConstraintApplePayView.constant = 0
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid().take(1)
            .asDriverOnErrorJustComplete()
        
        
        
        self.viewModel = ChickOutviewModel.init(useCase: NetworkPlatform.UseCaseProvider().makeChickOutUseCase(),
                                                deliveryModeUseCase: UseCaseProvider().makeDeliveryUseCase(),
                                                getCartUseCase: UseCaseProvider().makeCartUseCase()  ,
                                                setDeliveryMode : NetworkPlatform.UseCaseProvider().makeDeliveryUseCase(),
                                                getPaymentModes: NetworkPlatform.UseCaseProvider().makePaymentUseCase(),
                                                paymentInfoUseCase: NetworkPlatform.UseCaseProvider().makePaymentInfoUseCase(),
                                                paymentdetilsUseCase: NetworkPlatform.UseCaseProvider().makePaymentDetailsUseCase(),
                                                placeOrderUseCase: NetworkPlatform.UseCaseProvider().makePlaceOrderUseCase(),
                                                storeCreditCartUseCase: NetworkPlatform.UseCaseProvider().makeStroyCreditCartUseCase(),
                                                validationsCartErrors: networkUseCaseProvider.makeValidationCartUseCase(),
                                                bankOffersUseCase: networkUseCaseProvider.makeBankOffersCartUseCase(), applePayUseCase: networkUseCaseProvider.makeApplePayUseCase(), configUseCase: networkUseCaseProvider.makeConfig())
        
        
        let input = ChickOutviewModel.Input(
            trigger: Driver.merge(viewWillAppear),
            setDeliveryAddress:self.viewModel.deliveryAddressSubject.asDriverOnErrorJustComplete(),
            setBillingAddress:self.viewModel.billingAddressSubject.asDriverOnErrorJustComplete(),
            setDeliveryMode:self.deliveryModeSubject.asDriverOnErrorJustComplete(),
            refreshCart:self.viewModel.refreshCart.asDriverOnErrorJustComplete(),
            setPaymentMode: self.viewModel.paymentModeSubject.asDriverOnErrorJustComplete(),
            setPaymentDetils: setPaymentDetilsSubject.asDriverOnErrorJustComplete(),
            placeOrder: placeOrderSubject.asDriverOnErrorJustComplete(),
            cartId: self.cartId.asDriverOnErrorJustComplete(),
            getPaymentInfo: self.getPaymentInfoSubject.asDriverOnErrorJustComplete(),
            validateCart: validationCart.asDriverOnErrorJustComplete(),
            getApplePayInfo: getApplePayInfoSubject.asDriverOnErrorJustComplete() ,
            setApplePay: applePaySubject.asDriverOnErrorJustComplete() ,
            paymentOption: paymentOption.asDriverOnErrorJustComplete() ,
            cardType: cardType.asDriverOnErrorJustComplete() ,
            cardName: cardName.asDriverOnErrorJustComplete() ,
            paymentData: paymentData.asDriverOnErrorJustComplete() ,
            appleCardType: appleCardType.asDriverOnErrorJustComplete() ,
            amount: amount.asDriverOnErrorJustComplete() ,
            currency: currency.asDriverOnErrorJustComplete() ,
            orderId: orderId.asDriverOnErrorJustComplete(), createShipmentType: createShipmentType.asDriverOnErrorJustComplete()
            
            , selectPaymentMode: self.selectPaymentModeSubject.asDriverOnErrorJustComplete())
        
        
        
        let output = viewModel.transform(input: input)
        
        
        output.setDeliveryMode.asObservable().bind(to: self.deliveryModeSubject).disposed(by: bag)
        

        
        
        self.viewModel.refreshCellUiSizeManual.subscribe(onNext: { (void) in
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
            
        }).disposed(by: bag)
        
        
 
        self.viewModel.chickOutSubjectFull.bind(to: self.tableView.rx.items(dataSource: self.viewModel.dataSourceChickOut())).disposed(by: bag)
        
        
//        Driver.combineLatest(output.timeSlots,
//                             output.bankOffers,
//                             output.cart,
//                             output.paymentModes,
//                             output.storeCreditModes,
//                             output.availableStoreCredit)
        
        
        output.cart.withLatestFrom(Driver.combineLatest(output.timeSlots,
                                                        output.bankOffers,
                                                        output.cart,
                                                        output.paymentModes,
                                                        output.storeCreditModes,
                                                        output.availableStoreCredit ,
                                                        output.config)).asObservable().subscribe(onNext: { (arg0) in
                                
            var (timeSlots, bankOffers, cart, paymentModes , storeCreditModes , availableStoreCredit ,config) = arg0
                                self.viewModel.cartModelSubject.onNext(cart)
                                var sections:[ChcikOutSectionModel] = []
                                //                 if cart.deliveryMode != nil{
                                self.sectionHeader = []
                                
                                
                                UIView.performWithoutAnimation {
                                 self.tableView.beginUpdates()
                                 self.tableView.endUpdates()
                                }
                                
                                sections.append(ChcikOutSectionModel(model: "timeSlots", items: [ChickOutCellIndexes.timeSlots(cartModel: cart, TimeSlots: timeSlots)]))
                                
                                self.sectionHeader.append(SectionHeader(lbl: "Choose Delivery Slot", img: "delivery_time"))
                                
                                
                                if cart.appliedOrderPromotions?.count ?? 0 > 0 {
                                    let items = cart.appliedOrderPromotions?.map({ (appliedPromotions) -> ChickOutCellIndexes in
                                        return .appliedOrderPromotion(appliedOrderPromotion: appliedPromotions)
                                    })
                                    sections.append(ChcikOutSectionModel(model: "appliedOrderPromotions", items: items ?? []))
                                    self.sectionHeader.append(SectionHeader(lbl: "Received Promotions", img: "bank_promotion_icon"))
                                    
                                }
                                
                                
                                if cart.potentialOrderPromotions?.count ?? 0 > 0 {
                                    var items = [ChickOutCellIndexes]()
                                    cart.potentialOrderPromotions?.forEach({ potentialOrderPromotion in
                                        items.append(.potentialOrderPromotion(potentialOrderPromotion: potentialOrderPromotion))
                                    })
                                    sections.append(ChcikOutSectionModel(model: "potentialOrderPromotions", items: items))
                                    self.sectionHeader.append(SectionHeader(lbl: "Potential Promotions", img: "bank_promotion_icon"))
                                    
                                }
                                
                                
                                if !(bankOffers.bankOffers?.isEmpty ?? true) {
                                    sections.append(ChcikOutSectionModel(model: "bankOffer", items: [ChickOutCellIndexes.bankOffer(cartModel: cart, bankOffer: bankOffers)]))
                                    self.sectionHeader.append(SectionHeader(lbl: "Bank Offers", img: "bank-offers-icon"))
                                    
                                }
                                
                                
                                
                                if (availableStoreCredit.value ?? 0.0) > 0.0 {
                                    if( paymentModes.paymentModes != nil ) {
                                        var indexForRemove : Int? = nil
                                        
                                        for (index, element) in paymentModes.paymentModes!.enumerated() {
                                            if(element.code == "apple") {
                                               indexForRemove = index
                                                
                                            }
                                            
                                        }
                                        
                                        if(indexForRemove != nil){
                                            paymentModes.paymentModes!.remove(at: indexForRemove!)
                                            self.constraintApplePayView.constant =  (UIScreen.main.bounds.width * 0.46)
                                            self.leadingConstraintApplePayView.constant = 16
                                            self.ApplePayView.cornerRadius = 10
                                            self.addButtonApplePay ()
                                            
                                        } else {
                                            self.constraintApplePayView.constant = 0
                                            self.leadingConstraintApplePayView.constant = 0
                                        }
                                        
                                    }
                                    sections.append(ChcikOutSectionModel(model: "paymentMode", items: [ChickOutCellIndexes.storeCreditModes(cartModel: cart, CreditModes: storeCreditModes, availableAmount: availableStoreCredit, confg: config),ChickOutCellIndexes.paymentMode(cartModel: cart, PaymentModes: paymentModes)]))
                                    self.sectionHeader.append(SectionHeader(lbl: "paymentMode", img: "payment"))
                                    
                                }
                                else {
                                    
                                  //  if (availableStoreCredit.value ?? 0.0) > 0.0 {
                                        if( paymentModes.paymentModes != nil ) {
                                            var indexForRemove : Int? = nil
                                            
                                            for (index, element) in paymentModes.paymentModes!.enumerated() {
                                                if(element.code == "apple") {
                                                   indexForRemove = index
                                                    self.paymentModeApplePay = element
                                                }
                                                
                                            }
                                            
                                            if(indexForRemove != nil){
                                                paymentModes.paymentModes!.remove(at: indexForRemove!)
                                                self.constraintApplePayView.constant =  (UIScreen.main.bounds.width * 0.45)
                                                self.leadingConstraintApplePayView.constant = 16
                                                self.addButtonApplePay ()
                                                
                                            } else {
                                                self.constraintApplePayView.constant = 0
                                                self.leadingConstraintApplePayView.constant = 0
                                            }
                                            
                                        }
                                    //}
                                        
                                    sections.append(ChcikOutSectionModel(model: "paymentMode", items: [ChickOutCellIndexes.paymentMode(cartModel: cart, PaymentModes: paymentModes)]))
                                    
                                    self.sectionHeader.append(SectionHeader(lbl: "Payment", img: "payment"))
                                    
                                }
                                
                                
                                sections.append(ChcikOutSectionModel(model: "orderSummary", items: [ChickOutCellIndexes.orderSummary(cartModel: cart)]))
                                self.sectionHeader.append(SectionHeader(lbl: "Order Summary", img: "payment"))
                                
                                
                                sections.append(ChcikOutSectionModel(model: "shippingAddress", items: [ChickOutCellIndexes.shippingAddress(cartModel: cart)]))
                                                            if cart.shipmentType?.code == "PICKUP_IN_STORE"
                                                                {
                                                                self.sectionHeader.append(SectionHeader(lbl: "Pick Up", img: "payment"))
                                                            } else {
                                                                self.sectionHeader.append(SectionHeader(lbl: "Shipping Address", img: "payment"))
                                                            }
                                
                                
                                sections.append(ChcikOutSectionModel(model: "myItems", items: [ChickOutCellIndexes.myItems(cartModel: cart)]))
                                self.sectionHeader.append(SectionHeader(lbl: "My Items", img: "payment"))
                                
                                
                                
                                self.viewModel.chickOutSubjectFull.onNext(sections)
                                
                                                            
                            
                                 
                             }).disposed(by: bag)
        
        
        
        Driver.combineLatest(output.cart.flatMapLatest { (cart) in
            return Driver<Bool>.just(cart.timeSlotInfoData != nil && cart.paymentMode != nil).asDriver()
        },self.switchBut.rx.value.asDriver()).map { (boolOne, boolTow) -> Bool in
            var timeSlotCond =  boolOne
            if !self.showTimeSlot {
                timeSlotCond = true
            }
            if  timeSlotCond && boolTow   == false {
                self.ApplePayView.alpha = 0.7
            } else {
                self.ApplePayView.alpha = 1
            }
            return timeSlotCond && boolTow && true
        }.asObservable().bind(to: self.placeOrderBut.rx.isEnabled).disposed(by: self.bag)
        
        Driver.combineLatest(output.cart.flatMapLatest { (cart) in
            return Driver<Bool>.just(cart.timeSlotInfoData != nil).asDriver()
        },self.switchBut.rx.value.asDriver()).map { (boolOne, boolTow) -> Bool in
            var timeSlotCond =  boolOne
            if !self.showTimeSlot {
                timeSlotCond = true
            }
            if  timeSlotCond && boolTow   == false {
                self.ApplePayView.alpha = 0.7
            } else {
                self.ApplePayView.alpha = 1
            }
            return timeSlotCond && boolTow && true
        }.asObservable().bind(to: self.ApplePayView.rx.isUserInteractionEnabled).disposed(by: self.bag)
        
//        Driver.combineLatest(output.cart.flatMapLatest { (cart) in
//            return Driver<Bool>.just(cart.timeSlotInfoData != nil && cart.paymentMode != nil).asDriver()
//        },self.switchBut.rx.value.asDriver()).map { (boolOne, boolTow) -> Bool in
//
//            if  boolOne && boolTow && true  == false {
//                self.ApplePayView.alpha = 0.7
//            } else {
//                self.ApplePayView.alpha = 1
//            }
//            return boolOne && boolTow && true
//        }.asObservable().bind(to: self.ApplePayView.rx.isUserInteractionEnabled).disposed(by: self.bag)
        
        
        //MARK:rBank Offer
        //        self.viewModel.promoCodeMsgsSubject.bind(to: self.self.bankOffersTableView.rx.items(dataSource: self.viewModel.dataSourceBankOffers())).disposed(by: bag)
        
        output.bankOffers.asObservable().subscribe(onNext: { (bankOffers) in
            self.viewModel.bankOffersModel = bankOffers
            
        }).disposed(by: bag)
        
        
        output.cart.asObservable().subscribe(onNext: { (cartModel) in
            self.viewModel.cartModel = cartModel
            self.showTimeSlot =   cartModel.cartTimeSlot?.showTimeSlot ?? false
            let coponslist =  cartModel.appliedProductPromotions?.map({ (copon) in
               return     "\(copon.promotion?.code ?? "")"
           })
           let copons = coponslist?.joined(separator: ", ")
            
            Analytics.logEvent(AnalyticsEventBeginCheckout, parameters: [
                AnalyticsParameterCoupon: copons ?? "",
              AnalyticsParameterCurrency:  cartModel.totalPrice?.currencyISO ?? "AED",
              AnalyticsParameterValue: cartModel.totalPriceWithTax ?? ""
              ])
            
            Analytics.logEvent(AnalyticsEventAddShippingInfo, parameters: [
              AnalyticsParameterCoupon: copons ?? "",
                AnalyticsParameterCurrency: cartModel.totalPrice?.currencyISO ?? "AED",
              AnalyticsParameterShippingTier: "Ground",
              AnalyticsParameterValue: cartModel.totalPriceWithTax ?? ""
              ])
                         
        }).disposed(by: bag)

       
        placeOrderBut.rx.tap.subscribe(onNext: { (void) in
            self.validationCart.onNext(())
            //  self.viewModel.refreshCart.onNext(())
        }).disposed(by: bag)
        
        
        //        placeOrderBut.rx.tap.withLatestFrom(output.cart).subscribe(onNext: { (arg0) in
        //            let (model) = arg0
        //
        //                 if model.paymentMode?.code == "cod" || model.paymentMode?.code == "continue"{
        //                    self.placeOrderSubject.onNext(model)
        //
        //                 }else if model.paymentMode?.code == "card"{
        //                    self.getPaymentInfoSubject.onNext(())
        //                }
        //
        //            }).disposed(by: bag)
        
        output.validationCartErrors.withLatestFrom(Driver.combineLatest(output.validationCartErrors , output.cart)).asObservable().subscribe(onNext: { (arg0) in
            let (errors, model) = arg0
            
            if errors.valid == true {
                if model.paymentMode?.code == "cod" || model.paymentMode?.code == "continue" || model.paymentMode?.code == "pis" || model.paymentMode?.code == "ccod"  {
//                    self.viewModel.refreshCart.onNext(())
                    self.placeOrderSubject.onNext(model)
                    
                    
                }else if model.paymentMode?.code == "card"{
//                    self.viewModel.refreshCart.onNext(())
                    self.getPaymentInfoSubject.onNext(())
                }
                else if model.paymentMode?.code == "apple" {
                 //   self.viewModel.refreshCart.onNext(())
                    self.getApplePayInfoSubject.onNext(())
                  //  self.cartData = model.entries ?? []
                }
            }
            else {
                Loaf(errors.errors?.joined(separator: ", ") ?? "", state: .error(L102Language.isRTL ? .right : .left), sender: self).show()
                self.viewModel.refreshCart.onNext(())
            }
            
            
        }).disposed(by: bag)
        
        
        
        
//        Driver.zip(output.validationCartErrors , output.cart).asObservable().subscribe(onNext: { (arg0) in
//            let (errors, model) = arg0
//            if errors.valid == true {
//                if model.paymentMode?.code == "cod" || model.paymentMode?.code == "continue"{
//                    self.placeOrderSubject.onNext(model)
//
//
//                }else if model.paymentMode?.code == "card"{
//                    self.getPaymentInfoSubject.onNext(())
//                }
//
//            }
//            else {
//                Loaf(errors.errors?.joined(separator: ", ") ?? "", state: .error, sender: self).show()
//                self.viewModel.refreshCart.onNext(())
//            }
//
//        }).disposed(by: bag)
        
        output.applePayResponse.withLatestFrom(Driver.combineLatest(output.applePayResponse,output.cart)).asObservable().subscribe(onNext: {[unowned self] (response,cart) in
            
            
            self.applePayWebView.navigationDelegate = self;
            self.applePayWebView.isHidden = false
            self.applePayWebView.loadHTMLString(response.webSource ?? "", baseURL: nil)
        
            
        }).disposed(by: bag)
        output.paymentTransactionResponse.withLatestFrom(Driver.combineLatest(output.paymentTransactionResponse,output.cart)).asObservable().subscribe(onNext: { (response,cart) in
            self.placeOrderSubject.onNext(cart)
        }).disposed(by: bag)

            
//
//        Driver.combineLatest(output.paymentTransactionResponse,output.cart).asObservable().subscribe(onNext: { (response,cart) in
//            self.placeOrderSubject.onNext(cart)
//        }).disposed(by: bag)
        
        output.placeOrder.asObservable().subscribe(onNext: { (placedOrder) in
            self.performSegue(withIdentifier: "orderConfirmation", sender: placedOrder)
        }, onError: { (Error) in
            print("error")
        }).disposed(by: bag)
        
        output.error.asObservable().subscribe(onNext: { (Error) in
            
            let errors = Error as? ErrorResponse
            Loaf(errors?.errors?.first?.message ?? "", state: .error(L102Language.isRTL ? .right : .left), sender: self).show()
            self.viewModel.refreshCart.onNext(())
            
        }).disposed(by: bag)
        
        output.errorPlaceOrder.asObservable().subscribe(onNext: { (Error) in
            
            let errors = Error as? ErrorResponse
            Loaf(errors?.errors?.first?.message ?? "", state: .error(L102Language.isRTL ? .right : .left), sender: self).show()
            
        }).disposed(by: bag)
        
        output.errorSetPayment.asObservable().subscribe(onNext: { (Error) in
            
            let errors = Error as? ErrorResponse
            Loaf(errors?.errors?.first?.message ?? "", state: .error(L102Language.isRTL ? .right : .left), sender: self).show()
            
        }).disposed(by: bag)

        output.paymentInfo.asObservable().subscribe(onNext: { [unowned self](paymentInfo) in
            
            #if PROD
            let vc =  CCAvenuePaymentController(orderId:  paymentInfo.orderId ?? "", merchantId: paymentInfo.merchantId ?? "", accessCode: paymentInfo.accessCode ?? "", custId: paymentInfo.customerIdentifier ?? "", amount: paymentInfo.amount ?? "", currency: paymentInfo.currency ?? "", rsaKeyUrl: "https://ccavenue.erabia.io/GetRSAProd.php", redirectUrl: "https://ccavenue.erabia.io/ccavResponseHandlerProd.php", cancelUrl: "https://ccavenue.erabia.io/ccavResponseHandlerProd.php", showAddress: "Y", billingName: paymentInfo.deliveryName ?? "", billingAddress:  paymentInfo.deliveryAddress ?? "", billingCity: paymentInfo.deliveryCity ?? "", billingState: paymentInfo.deliveryState ?? "", billingCountry: paymentInfo.deliveryCountry ?? "", billingTel:  paymentInfo.deliveryTel ?? "", billingEmail: paymentInfo.customerIdentifier ?? "", deliveryName:  paymentInfo.deliveryName ?? "", deliveryAddress: paymentInfo.deliveryAddress ?? "", deliveryCity:  paymentInfo.deliveryCity ?? "", deliveryState: paymentInfo.deliveryState ?? "", deliveryCountry: paymentInfo.deliveryCountry ?? "", deliveryTel: paymentInfo.deliveryTel ?? "", promoCode: paymentInfo.promoCode ?? "", merchant_param1: paymentInfo.merchantParam1 ?? "", merchant_param2: paymentInfo.merchantParam2 ?? "", merchant_param3: paymentInfo.merchantParam3 ?? "", merchant_param4: paymentInfo.merchantParam4 ?? "", merchant_param5: paymentInfo.merchantParam4 ?? "", useCCPromo: ((paymentInfo.promoCode ?? "").isEmpty) ? "N" : "Y", siType: "None", siMerchantRefNo: "", siSetupAmount: "", siAmount: "", siStartDate: "", siFrequencyType: "", siFrequency: "", siBillCycle: "")
           /* let vc = CCAvenuePaymentController.init(orderId: paymentInfo.orderId ?? "",
                                                    merchantId:  paymentInfo.merchantId ?? "", //"43560" paymentInfo.merchantId ?? ""
                                                    accessCode: paymentInfo.accessCode ?? "", // paymentInfo.accessCode ?? ""
                                                    custId: paymentInfo.customerIdentifier,
                                                    amount: paymentInfo.amount ?? "",
                                                    currency: paymentInfo.currency ?? "",
                                                    rsaKeyUrl: "https://ccavenue.erabia.io/GetRSAProd.php",
                                                    redirectUrl: "https://ccavenue.erabia.io/ccavResponseHandlerProd.php",
                                                    cancelUrl: "https://ccavenue.erabia.io/ccavResponseHandlerProd.php",
                                                    showAddress: "Y",
                                                    billingName: paymentInfo.deliveryName ?? "",
                                                    billingAddress: paymentInfo.deliveryAddress ?? "",
                                                    billingCity: paymentInfo.deliveryCity ?? "",
                                                    billingState: paymentInfo.deliveryState ?? "",
                                                    billingCountry: paymentInfo.deliveryCountry ?? "",
                                                    billingTel: paymentInfo.deliveryTel ?? "",
                                                    billingEmail: paymentInfo.customerIdentifier ?? "",
                                                    deliveryName: paymentInfo.deliveryName ?? "",
                                                    deliveryAddress: paymentInfo.deliveryAddress ?? "",
                                                    deliveryCity: paymentInfo.deliveryCity ?? "",
                                                    deliveryState: paymentInfo.deliveryState ?? "",
                                                    deliveryCountry: paymentInfo.deliveryCountry ?? "",
                                                    deliveryTel: paymentInfo.deliveryTel ?? "",
                                                    promoCode: paymentInfo.promoCode ?? "",
                                                    merchant_param1: paymentInfo.merchantParam1 ?? "",
                                                    merchant_param2: paymentInfo.merchantParam2 ?? "",
                                                    merchant_param3: paymentInfo.merchantParam3 ?? "",
                                                    merchant_param4: paymentInfo.merchantParam4 ?? "",
                                                    merchant_param5: paymentInfo.merchantParam4 ?? "",
                                                    useCCPromo: ((paymentInfo.promoCode ?? "").isEmpty) ? "N" : "Y")*/
            
            #else
            let vc =  CCAvenuePaymentController.init(orderId:  paymentInfo.orderId ?? "", merchantId: paymentInfo.merchantId ?? "", accessCode: paymentInfo.accessCode ?? "", custId: paymentInfo.customerIdentifier ?? "", amount: paymentInfo.amount ?? "", currency: paymentInfo.currency ?? "", rsaKeyUrl: "https://ccavenue.erabia.io/GetRSA.php", redirectUrl: "https://ccavenue.erabia.io/ccavResponseHandler.php", cancelUrl: "https://ccavenue.erabia.io/ccavResponseHandler.php", showAddress: "Y", billingName: paymentInfo.deliveryName ?? "", billingAddress:  paymentInfo.deliveryAddress ?? "", billingCity: paymentInfo.deliveryCity ?? "", billingState: paymentInfo.deliveryState ?? "", billingCountry: paymentInfo.deliveryCountry ?? "", billingTel:  paymentInfo.deliveryTel ?? "", billingEmail: paymentInfo.customerIdentifier ?? "", deliveryName:  paymentInfo.deliveryName ?? "", deliveryAddress: paymentInfo.deliveryAddress ?? "", deliveryCity:  paymentInfo.deliveryCity ?? "", deliveryState: paymentInfo.deliveryState ?? "", deliveryCountry: paymentInfo.deliveryCountry ?? "", deliveryTel: paymentInfo.deliveryTel ?? "", promoCode: paymentInfo.promoCode ?? "", merchant_param1: paymentInfo.merchantParam1 ?? "", merchant_param2: paymentInfo.merchantParam2 ?? "", merchant_param3: paymentInfo.merchantParam3 ?? "", merchant_param4: paymentInfo.merchantParam4 ?? "", merchant_param5: paymentInfo.merchantParam4 ?? "", useCCPromo: ((paymentInfo.promoCode ?? "").isEmpty) ? "N" : "Y", siType: "None", siMerchantRefNo: "", siSetupAmount: "", siAmount: "", siStartDate: "", siFrequencyType: "", siFrequency: "", siBillCycle: "")

            #endif
            vc?.modalPresentationStyle = .fullScreen
            vc?.delegate = self
            guard let vcPayment = vc else {return}
            
            let child = SpinnerViewController()
            
            // add the spinner view controller
            addChild(child)
            child.view.frame = view.frame
            view.addSubview(child.view)
            child.didMove(toParent: self)
            
            // wait two seconds to simulate some work happening
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                // then remove the spinner view controller
                child.willMove(toParent: nil)
                child.view.removeFromSuperview()
                child.removeFromParent()
            }
            
            self.present(vcPayment, animated: true, completion: nil)
            
            
        }, onError: { (Error) in
            print("cant get payment info")
        }).disposed(by: self.bag)
        
        output.selectpaymentMode.asObservable().subscribe(onNext: {(mode) in
            print(mode)
            self.viewModel.refreshCart.onNext(())
            self.validationCart.onNext(())
        }).disposed(by: bag)
        
        
        output.applePayInfo.asObservable().subscribe(onNext: { [unowned self](paymentInfo) in
            if PKPaymentAuthorizationController.canMakePayments(){
            let request = PKPaymentRequest()
            request.merchantIdentifier = ApplePayMerchantID
            request.supportedNetworks = SupportedPaymentNetworks
            request.requiredShippingContactFields = [.phoneNumber]
            request.merchantCapabilities = [.capabilityDebit, .capabilityCredit, .capability3DS]
            request.countryCode =  "AE"
            request.currencyCode = "AED"
            request.requiredBillingContactFields = [.name,.postalAddress]
              
            request.paymentSummaryItems = cartData.map{ (product) in PKPaymentSummaryItem(label: product.product?.name ?? "", amount: NSDecimalNumber(value: product.totalPrice?.value ?? 0.0))}
            
            request.paymentSummaryItems.append(PKPaymentSummaryItem(label: "AL DAHRA FOOD DUBAI AE" , amount: NSDecimalNumber(value: Double(paymentInfo.amount ?? "") ?? 0.0)))
                self.amount.onNext(paymentInfo.amount ?? "")
                self.currency.onNext( paymentInfo.currency ?? "")
                self.orderId.onNext(paymentInfo.orderId ?? "")
            let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
                applePayController?.delegate = self
            self.present(applePayController!, animated: true, completion: nil)
                
          
            }
        }).disposed(by: self.bag)
        
        

    }
    
    
    
    fileprivate func registerNibs() {
        tableView.register(UINib(nibName: "ChickOutSectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "ChickOutSectionHeader")
        tableView.register(UINib(nibName: "ChickOutSameNativeHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "ChickOutSameNativeHeader")
        tableView.register(UINib(nibName: "ChickOutWithSeeMore", bundle: nil), forHeaderFooterViewReuseIdentifier: "ChickOutWithSeeMore")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toCartEntrys" {
            guard let vc = segue.destination as? CartEntrysViewController else {return}
            //            vc.cartModel = self.viewModel.cartModel
            vc.cartItemsSubject.onNext([CartSection(header: "", items:  self.viewModel.cartModel?.entries ?? [] )])
        }
        //PalceOrderResponse //OrderConfirmationViewController
        else{
            guard let vc = segue.destination as? OrderConfirmationViewController else {return}
            guard let orderResponse = sender as? PalceOrderResponse else {return}
            vc.orderResponse = BehaviorSubject<PalceOrderResponse>(value: orderResponse)
        }
    }
    
   
    func addButtonApplePay () {
      //  if PKPaymentAuthorizationController.canMakePayments() {
        
        if #available(iOS 14.0, *) {
             applePayBttn = PKPaymentButton(paymentButtonType: .plain, paymentButtonStyle: .automatic)
        } else {
            // Fallback on earlier versions
        }
        
            applePayBttn.translatesAutoresizingMaskIntoConstraints = false
            
            applePayBttn.addTarget(self, action: #selector(tapOnApplePay), for: UIControl.Event.touchUpInside)
                  applePayBttn.center = ApplePayView.center
            ApplePayView.addSubview(applePayBttn)
            applePayBttn.trailingAnchor.constraint(equalTo: ApplePayView.trailingAnchor, constant: 0).isActive = true
            applePayBttn.leadingAnchor.constraint(equalTo: ApplePayView.leadingAnchor, constant: 0).isActive = true
            applePayBttn.topAnchor.constraint(equalTo: ApplePayView.topAnchor, constant: +1).isActive = true
            applePayBttn.bottomAnchor.constraint(equalTo: ApplePayView.bottomAnchor, constant: -1).isActive = true
       
            
      // }
        
    }
    
    @objc func tapOnApplePay() {
        selectPaymentModeSubject.onNext(paymentModeApplePay ?? PaymentMode(code: "apple", paymentModeDescription: "Pay with Apple", name: "Apple Pay"))
       
    }
    
    
    
}



extension ChickOutStep2ViewController: UITableViewDelegate {
    
  
 
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let sections = try? self.viewModel.chickOutSubjectFull.value() else {return UIView()}
        
        let sectionType = sections[section].model
        
        switch sectionType {
        case "timeSlots" , "appliedOrderPromotions" , "potentialOrderPromotions" , "bankOffer" , "paymentMode"  :
            let ChickOutSectionHeader = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "ChickOutSectionHeader") as! ChickOutSectionHeader
            if self.sectionHeader.count > 0 {
                ChickOutSectionHeader.name.text = self.sectionHeader[section].lbl?.localized()
                let tapgesture = UITapGestureRecognizer(target: self , action: #selector(self.sectionTapped(_:)))
                ChickOutSectionHeader.tag = section
                ChickOutSectionHeader.addGestureRecognizer(tapgesture)
                ChickOutSectionHeader.img.image = UIImage(named: self.sectionHeader[section].img ?? "")
            }
           
            
            return ChickOutSectionHeader
            
        case "orderSummary" , "shippingAddress" :
            
            let ChickOutSectionHeader = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "ChickOutSameNativeHeader") as! ChickOutSameNativeHeader
            
            if self.sectionHeader.indices.contains(section) {
                ChickOutSectionHeader.name.text = self.sectionHeader[section].lbl?.localized() ?? ""

            } else {
                ChickOutSectionHeader.name.text = ""
            }
            let tapgesture = UITapGestureRecognizer(target: self , action: #selector(self.sectionTapped(_:)))
            ChickOutSectionHeader.tag = section
            ChickOutSectionHeader.addGestureRecognizer(tapgesture)
            
            return ChickOutSectionHeader
            
        case "myItems" :
            
            let ChickOutSectionHeader = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "ChickOutWithSeeMore") as! ChickOutWithSeeMore
            
            ChickOutSectionHeader.name.text = self.sectionHeader[section].lbl?.localized() ?? ""
            let tapgesture = UITapGestureRecognizer(target: self , action: #selector(self.sectionTapped(_:)))
            ChickOutSectionHeader.tag = section
            ChickOutSectionHeader.addGestureRecognizer(tapgesture)
            ChickOutSectionHeader.seeMoreActionCallBack = {
                self.performSegue(withIdentifier: "toCartEntrys", sender: nil)
            }
            return ChickOutSectionHeader
        default:
            return UIView()
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //        cell.alpha = 0
        //        cell.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        //        UIView.animate(
        //            withDuration: 0.1,
        //            delay: 0.01 * Double(indexPath.row),
        //            animations: {
        //                cell.alpha = 1
        //                cell.transform = CGAffineTransform.identity
        //
        //        })
       
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section <= 1 ? 40 : 45
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(15)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    @objc func sectionTapped(_ sender: UITapGestureRecognizer ){
        //        let section = sender.view?.tag ?? 0
        //        let header = sender.view as? ChickOutSectionHeader
        //        guard section < 2 else {return}
        //        guard var subject = try? self.viewModel.chickOutSubject.value() else {return}
        //        guard var subjectFull = try? self.viewModel.chickOutSubjectFull.value() else {return}
        //
        //        if self.expandData[section].value(forKey: "isOpen") as! String == "1"{
        //            self.expandData[section].setValue("0", forKey: "isOpen")
        //            header?.setUp(isSelected: false)
        //            subjectFull[section].items = subject[section].items
        //            subject[section].items = []
        //            self.viewModel.chickOutSubject.onNext(subject)
        //
        //        }
        //        else  {
        //            self.expandData[section].setValue("1", forKey: "isOpen")
        //            header?.setUp(isSelected: true)
        //            subject[section].items = subjectFull[section].items
        //            self.viewModel.chickOutSubject.onNext(subject)
        //
        //        }
        
    }
    func getResponse(_ responseDict_: NSMutableDictionary!) {
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: responseDict_ ?? [:], options: .prettyPrinted)
            let object = try JSONDecoder().decode(PaymentGatewayResponse.self, from: jsonData)
            
            if object.orderStatus != "Transaction Closed" {
                self.setPaymentDetilsSubject.onNext((object,object.orderStatus == "Success"))
            }
            
            printData(data: jsonData)
        }
        catch {
            print("Count convert to Data")
        }
        
        
        
        
    }
    
    func printData(data : Data){
        print("========== Success ============")
        print(String(decoding: data, as: UTF8.self))
        print("========== End ============")
    }
    
    
    
    
}



private extension IndexPath {
    var cacheKey: String {
        return String(describing: self)
    }
}



class SpinnerViewController: UIViewController {
    var spinner = UIActivityIndicatorView(style: .whiteLarge)
    
    override func loadView() {
        view = UIView()
        //        view.backgroundColor = UIColor(white: 0, alpha: 1)
        spinner.color = .gray
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        view.addSubview(spinner)
        
        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}


extension ChickOutStep2ViewController:
PKPaymentAuthorizationViewControllerDelegate{
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: nil)
        
        
    }
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        
        // Perform some very basic validation on the provided contact information
        var errors = [Error]()
        var status = payment.token.paymentMethod.paymentPass
        //        payment.token.transactionIdentifier

            // Here you would send the payment token to your server or payment provider to process
            // Once processed, return an appropriate status in the completion handler (success, failure, etc)
            let paymentDataString = String(decoding: payment.token.paymentData, as: UTF8.self)
        self.cardName.onNext( payment.token.paymentMethod.network?.rawValue)
            


            switch payment.token.paymentMethod.type {

                case   .unknown :
                    self.appleCardType.onNext("unknown")
                    
                case .debit :
                    self.appleCardType.onNext("debit")
                self.cardType.onNext("DBCRD")
                case .credit :
                    self.appleCardType.onNext("credit")
                self.cardType.onNext("CRDC")
                case .prepaid :
                    self.appleCardType.onNext("prepaid")

                case .store :
                    self.appleCardType.onNext("store")

                default:

                    self.cardType.onNext("")
                }
        self.paymentData.onNext( paymentDataString)
            
            applePaySubject.onNext(())
       
    
        
       // self.paymentStatus = status
        completion(PKPaymentAuthorizationResult(status: .success, errors: errors))
        
    }
 
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
    }
    
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didSelect paymentMethod: PKPaymentMethod, handler completion: @escaping (PKPaymentRequestPaymentMethodUpdate) -> Void) {

     
           
        
        let discountedSummaryItems = paymentSummaryItems

            completion(PKPaymentRequestPaymentMethodUpdate(paymentSummaryItems: discountedSummaryItems))

        }
//     func paymentAuthorizationController(_ controller: PKPaymentAuthorizationController, didSelectPaymentMethod paymentMethod: PKPaymentMethod, handler completion: @escaping (PKPaymentRequestPaymentMethodUpdate) -> Void) {
//        // The didSelectPaymentMethod delegate method allows you to make changes when the user updates their payment card
//        // Here we're applying a $2 discount when a debit card is selected
//        guard paymentMethod.type == .debit else {
//            completion(PKPaymentRequestPaymentMethodUpdate(paymentSummaryItems: paymentSummaryItems))
//            return
//        }
//        switch paymentMethod.type {
//        case   .unknown :
//            self.cardType.onNext("unknown")
//
//        case .debit :
//            self.cardType.onNext("debit")
//
//        case .credit :
//            self.cardType.onNext("credit")
//
//
//        case .prepaid :
//            self.cardType.onNext("prepaid")
//
//        case .store :
//            self.cardType.onNext("store")
//
//        default:
//            self.cardType.onNext("")
//
//        }
//
//        self.cardName.onNext(paymentMethod.displayName)
//        self.appleCardType.onNext(paymentMethod.network.map { $0.rawValue })
//
//
//        var discountedSummaryItems = paymentSummaryItems
//
//        completion(PKPaymentRequestPaymentMethodUpdate(paymentSummaryItems: discountedSummaryItems))
//    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
           if let url = navigationAction.request.url, let comp = URLComponents(url: url, resolvingAgainstBaseURL: false), comp.scheme == gatewayScheme, comp.host == gatewayHost {
               decisionHandler(.cancel)
               webView.isHidden = true
               let gatewayResultItem = comp.queryItems?.first { (item) -> Bool in
                   return item.name == gatewayResultParam
               }
               
               guard let gatewayString = gatewayResultItem?.value, let gatewayData = gatewayString.data(using: .utf8) else {
                 //  completion?(self, .error("Gateway3DSecureError.missingGatewayResponse"))
                   return
               }
               
               do {
                   let gatewayResult = try JSONDecoder().decode(PaymentGatewayResponse.self, from: gatewayData)
                   if gatewayResult.orderStatus != "Transaction Closed" {
                       self.setPaymentDetilsSubject.onNext((gatewayResult,gatewayResult.orderStatus == "Success"))
                   }
                 //  completion?(self, .completed(gatewayResult: gatewayResult))
               } catch {
                 //  completion?(self, .error(Gateway3DSecureError.mappingError))
               }
           } else {
               decisionHandler(.allow)
           }
       }
}
