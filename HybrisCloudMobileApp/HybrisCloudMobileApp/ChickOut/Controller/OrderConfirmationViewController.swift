//
//  OrderConfirmationViewController.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import Foundation
import RxSwift
import RxDataSources
import FirebaseAnalytics

class OrderConfirmationViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var viewModel : OrderConfirmationViewModel!
    var orderResponse : BehaviorSubject<PalceOrderResponse>?

    let bag = DisposeBag()
    let dataLayer : TAGDataLayer? = TAGManager.instance()?.dataLayer
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
                navigationController?.setNavigationBarHidden(false, animated: false)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       navigationController?.setNavigationBarHidden(true, animated: false)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
        
        tableView.rx.setDelegate(self).disposed(by: bag)
               
        registerNibs()

        guard let orderResponseBehaviorSubject = orderResponse else {return }
    
        let orderResponse = try? orderResponseBehaviorSubject.value()

        var products : [[String : Any]] = []
        
        orderResponse?.entries?.forEach { entry in
            
            let productDic : [String : Any] = [
                "id" : entry.product?.code ?? "" ,
                "name" : entry.product?.name ?? "" ,
                "category" : entry.product?.categories?.first?.name ?? "" ,
                "price" : "\(entry.basePrice?.value ?? 0.0)" ,
                "brand" : "" ,
                "variant" : "" ,
                "quantity" : "\(entry.quantity ?? 0)"
                ]
            
            products.append(productDic)
        }
           
        let actionDic : [String : Any] = [
            "id" : orderResponse?.code ?? "" ,
            "affiliation" : "Food Crowd" ,
            "revenue" : "\(orderResponse?.totalPriceWithTax?.value ?? 0.0)" ,
            "tax" : "\(orderResponse?.totalTax?.value ?? 0.0)",
            "shipping" : "\(orderResponse?.deliveryCost?.value ?? 0.0)"
            ]
        
        let purchaseDic : [String : Any] = [
            "actionField" : actionDic ,
            "products" : products
        ]

        let ecommerceDic : [String : Any] = [
            "purchase" : purchaseDic
        ]
        
        let dic : [String : Any] = [
            "event":"order" ,
            "transactionPostalCode":"" ,
            "transactionCountryName":"United Arab Emirates" ,
            "ecommerce":ecommerceDic
        ]
        
        dataLayer?.push(dic)
        
        
        let purchaseParams: [String: Any] = [
            AnalyticsParameterTransactionID: orderResponse?.code ?? "",
            AnalyticsParameterAffiliation: "Apple Store",
            AnalyticsParameterCurrency: orderResponse?.subTotal?.currencyISO ?? "",
            AnalyticsParameterValue: Int(orderResponse?.subTotal?.formattedValue ?? "") ?? 0,
            AnalyticsParameterTax: orderResponse?.totalTax?.formattedValue ?? "" ,
            AnalyticsParameterShipping: orderResponse?.deliveryCost?.value == 0.0 ? "Free" : orderResponse?.deliveryCost?.formattedValue ?? "Free"
        ]
      
        Analytics.logEvent(AnalyticsEventPurchase, parameters: purchaseParams)

        let paymentParams: [String: Any] = [
            AnalyticsParameterCurrency: orderResponse?.subTotal?.currencyISO ?? "",
            AnalyticsParameterValue: orderResponse?.totalPriceWithTax?.value ?? 0.0 ,
            AnalyticsParameterPaymentType: orderResponse?.paymentMode?.name ?? ""
        ]

        Analytics.logEvent(AnalyticsEventAddPaymentInfo, parameters: paymentParams)
        
     
        
        viewModel = OrderConfirmationViewModel()
        
        orderResponseBehaviorSubject.subscribe(onNext: { (palceOrderResponse) in
            self.viewModel.orderResponse = palceOrderResponse
            }).disposed(by: bag)
        
        let input = OrderConfirmationViewModel.Input(orderResponse : orderResponseBehaviorSubject.asDriverOnErrorJustComplete())
        
        
        let output = viewModel.transform(input: input)

        
        output.palceOrder.asObservable().map { (palceOrder) -> [OrderConfirmationSectionModel] in
            
            self.viewModel.orderResponse = palceOrder
            let sections:[OrderConfirmationSectionModel] = [
                            OrderConfirmationSectionModel(model: "orderDescription", items: [OrderConfirmationCellIndexes.orderDescription(orderResponse: palceOrder)]
                            )
                            
                            ,
                            
                            OrderConfirmationSectionModel(model: "orderSummary", items: [OrderConfirmationCellIndexes.orderSummary(orderResponse: palceOrder)])
                            
                            ,
                            
                            OrderConfirmationSectionModel(model: "shippingAddress", items: [OrderConfirmationCellIndexes.shippingAddress(orderResponse: palceOrder)])
                            
                            ,
                            
                            OrderConfirmationSectionModel(model: "myItems", items: [OrderConfirmationCellIndexes.myItems(orderResponse: palceOrder)])

                ]

                        
                        return sections

               }.bind(to: self.tableView.rx.items(dataSource: self.viewModel.dataSourceOrderConfirmation())).disposed(by: bag)
               
               
        
        navigationController?.setNavigationBarHidden(true, animated: false)
     }
    
    @IBAction func pop(_ sender: Any) {
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.popToRootViewController(animated: true)
      }
    
    fileprivate func registerNibs() {
        tableView.register(UINib(nibName: "ChickOutSectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "ChickOutSectionHeader")
        tableView.register(UINib(nibName: "ChickOutSameNativeHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "ChickOutSameNativeHeader")
        tableView.register(UINib(nibName: "ChickOutWithSeeMore", bundle: nil), forHeaderFooterViewReuseIdentifier: "ChickOutWithSeeMore")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toCartEntrys" {
            guard let vc = segue.destination as? CartEntrysViewController else {return}
             vc.cartItemsSubject.onNext([CartSection(header: "", items:  self.viewModel.orderResponse?.entries ?? [] )])
            
        }
    }
 
}
extension OrderConfirmationViewController: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nameArray = [nil,"Order Summary", "Shipping Address" , "My Items"]
        
      
         if section == 3{
            let OrderConfirmationSectionHeader = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "ChickOutWithSeeMore") as! ChickOutWithSeeMore
            
            OrderConfirmationSectionHeader.name.text = nameArray[section]?.localized()
            let tapgesture = UITapGestureRecognizer(target: self , action: #selector(self.sectionTapped(_:)))
            OrderConfirmationSectionHeader.tag = section
            OrderConfirmationSectionHeader.addGestureRecognizer(tapgesture)
            OrderConfirmationSectionHeader.seeMoreActionCallBack = {
                self.performSegue(withIdentifier: "toCartEntrys", sender: nil)
            }
            return OrderConfirmationSectionHeader

            
        }
        else{
            if(nameArray[section] != nil) {
            let OrderConfirmationSectionHeader = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "ChickOutSameNativeHeader") as! ChickOutSameNativeHeader
        
            OrderConfirmationSectionHeader.name.text = nameArray[section]?.localized()
            let tapgesture = UITapGestureRecognizer(target: self , action: #selector(self.sectionTapped(_:)))
            OrderConfirmationSectionHeader.tag = section
            OrderConfirmationSectionHeader.addGestureRecognizer(tapgesture)
            
            return OrderConfirmationSectionHeader
            } else {
                return nil
            }
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        cell.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        UIView.animate(
            withDuration: 0.1,
            delay: 0.01 * Double(indexPath.row),
            animations: {
                cell.alpha = 1
                cell.transform = CGAffineTransform.identity
                
        })
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section <= 1 ? 40 : 45
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(15)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
 
  @objc func sectionTapped(_ sender: UITapGestureRecognizer ){}

}
