//
//  CartEntrysViewController.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/21/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class CartEntrysViewController: UIViewController {
    
    let disposeBag = DisposeBag()

//    var cartModel : CartModel?
    var cartItemsSubject = BehaviorSubject<[CartSection]>(value: [CartSection(header: "", items: [])])

    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView.init(frame: CGRect.zero)
            tableView.rx.setDelegate(self).disposed(by: self.disposeBag)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.cartItemsSubject.onNext([CartSection(header: "", items: cartModel?.entries ?? [] )])
 
        self.cartItemsSubject.bind(to: tableView.rx.items(dataSource: self.dataSourceCart())).disposed(by: disposeBag)
        

     }
    
 
    func dataSourceCart() ->
     RxTableViewSectionedAnimatedDataSource<CartSection> {
         return RxTableViewSectionedAnimatedDataSource<CartSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .none, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, cv, indexPath, item in

          guard let cell = cv.dequeueReusableCell(withIdentifier: "CartItemCell", for: indexPath) as? CartItemCell else {
                    return UITableViewCell()
                }
          cell.setUp(item: item)
         
                 return cell

        })
    }

}



extension CartEntrysViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
           cell.alpha = 0
           cell.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
           UIView.animate(
               withDuration: 0.5,
               delay: 0.01 * Double(indexPath.row),
               animations: {
                   cell.alpha = 1
                   cell.transform = CGAffineTransform.identity

           })

    }
 
}

