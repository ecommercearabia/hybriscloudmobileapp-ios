//
//  DeliverySlotCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import Foundation

class DeliverySlotCell: UITableViewCell {

    @IBOutlet weak var dateCollectionView: UICollectionView!
    @IBOutlet weak var timeCollectionView: UICollectionView!
    @IBOutlet weak var pleaseSelectDateLbl: UILabel!
    @IBOutlet weak var titleTimeSlotLbl: UILabel!
    
    @IBOutlet weak var dateSlotConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var timeSlotConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLableTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLableBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var expressStackView: UIStackView!
    @IBOutlet weak var expressView: UIView!
    @IBOutlet weak var expressLabel: UILabel!
    @IBOutlet weak var expressButton: UIButton!
    @IBOutlet weak var expressTitleLabel: UILabel!
    @IBOutlet weak var expressImage: UIImageView!
    @IBOutlet weak var normalLabel: UILabel!
    @IBOutlet weak var normalButton: UIButton!
    @IBOutlet weak var normalImage: UIImageView!
    @IBOutlet weak var normalView: UIView!
    @IBOutlet weak var confirmButton: DesignableButton!
    @IBOutlet weak var expressViewHeight: NSLayoutConstraint!
    
    let timeSlotModelSubject = PublishSubject<TimeSlotsModel>()
    let cartModelSubject = PublishSubject<CartModel>()
    let refreshCart = PublishSubject<Void>()
    let selectDateManual = PublishSubject<TimeSlotDay>()
    var bag = DisposeBag()
    var viewModel = TimeSlotCellViewModel(getDeliveryTypesUseCase: NetworkPlatform.UseCaseProvider().makeDeliveryTypes())
    var showTimeSlot = true
    let setDeliveryType = PublishSubject<String>()
    let getDeliveryType = PublishSubject<Void>()
    var deliveryTypesModel : DeliveryTypesModel!
    var deliveryType = ""
    var selctedDeliveryType = BehaviorSubject(value: "")
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    func setUp(selctedDeliveryType : String){
        self.selctedDeliveryType.onNext(selctedDeliveryType)
        self.dateCollectionView.allowsMultipleSelection = false
        self.timeCollectionView.allowsMultipleSelection = false
        
        self.dateCollectionView.rx.setDelegate(self).disposed(by: bag)
        self.timeCollectionView.rx.setDelegate(self).disposed(by: bag)

        self.pleaseSelectDateLbl.isHidden = false
        
        let prepareForReuse = rx.sentMessage(#selector(UITableViewCell.prepareForReuse))
                .mapToVoid()
                .asDriverOnErrorJustComplete()
            
            let input = TimeSlotCellViewModel.Input(trigger: Driver.merge(Driver<Void>.just(()),prepareForReuse)  ,
            timeSlotModel: timeSlotModelSubject.asDriverOnErrorJustComplete(),
            cart: cartModelSubject.asDriverOnErrorJustComplete(),
            selectTimeSlotDay:
                Driver.merge(
                    dateCollectionView.rx.modelSelected(DaysSlotsSection.Item.self).asDriverOnErrorJustComplete(),selectDateManual.asDriverOnErrorJustComplete()),
            selectTimeSlotPeriod: timeCollectionView.rx.modelSelected(TimeSlotsSection.Item.self).asDriverOnErrorJustComplete() ,
                                                    expressTrigger: getDeliveryType.asDriverOnErrorJustComplete(), setExpress: setDeliveryType.asDriverOnErrorJustComplete())
            
            let output = self.viewModel.transform(input: input)
        
        
        
        output.deliveryTypes.asObservable().subscribe(onNext: {(deliveryTypes) in
            self.deliveryTypesModel = deliveryTypes
            self.expressTitleLabel.text = deliveryTypes.deliveryModeTypes?.first?.name ?? ""
            if (deliveryTypes.deliveryModeTypes?.count ?? 0) > 1 {
                self.normalLabel.text = deliveryTypes.deliveryModeTypes?[1].name ?? ""
            } else {
                self.normalView.isHidden = true
            }
            
        }).disposed(by: bag)

        
        expressButton.rx.tap.subscribe(onNext: {(void) in
            self.expressImage.image = UIImage(named: "radioButtonSelected")
            self.normalImage.image = UIImage(named: "radioButtonUnSelected")
            self.deliveryType = self.deliveryTypesModel.deliveryModeTypes?.first?.code ?? ""
        }).disposed(by: bag)
        
        normalButton.rx.tap.subscribe(onNext: {(void) in
            self.normalImage.image = UIImage(named: "radioButtonSelected")
            self.expressImage.image = UIImage(named: "radioButtonUnSelected")
            self.deliveryType = self.deliveryTypesModel.deliveryModeTypes?[1].code ?? ""
        }).disposed(by: bag)
        
        confirmButton.rx.tap.subscribe(onNext: {(void) in
            if self.deliveryType != "" {
                self.selctedDeliveryType.onNext(self.deliveryType)
                self.setDeliveryType.onNext(self.deliveryType)
            }else {
                UIApplication.topViewController()?.errorLoaf(message: "Please pick the delivery type.".localized())
            }
          
        }).disposed(by: bag)
        
        output.setdeliveryType.asObservable().subscribe(onNext: {(deliveryType) in
            if deliveryType.code != nil {
                self.getDeliveryType.onNext(())
            }
        }).disposed(by: bag)
        
        output.deliveryType.asObservable().subscribe(onNext: {(deliveryType) in
            self.selctedDeliveryType.onNext(deliveryType)
            
            self.refreshCart.onNext(())
        }).disposed(by: bag)
        
        
        Observable.combineLatest(self.cartModelSubject , self.selctedDeliveryType ).subscribe(onNext: {(arg) in
            let (cart , selctedDeliveryType) = arg
            if cart.express == true && selctedDeliveryType == "" {
                self.expressView.isHidden = false
                self.expressViewHeight.constant = 150
                self.dateSlotConstraintHeight.constant = 0
                self.timeSlotConstraintHeight.constant = 150
                self.dateCollectionView.isHidden = true
                self.timeCollectionView.isHidden = true
                self.pleaseSelectDateLbl.isHidden = true
                self.titleView.isHidden = true
                self.titleViewTopConstraint.constant  = 0
                self.titleViewBottomConstraint.constant  = 0
                self.titleLableTopConstraint.constant  = 0
                self.titleLableBottomConstraint.constant  = 0
                self.titleViewHeightConstraint.constant  = 0
                self.showTimeSlot = false
                self.layoutIfNeeded()
            } else {
                self.expressView.isHidden = true
                self.expressViewHeight.constant =  0
                self.titleTimeSlotLbl.text = cart.cartTimeSlot?.messages?.joined(separator: " , ")
                
                if selctedDeliveryType == "" && cart.express == false && cart.cartTimeSlot?.showTimeSlot == false {
                        self.showTimeSlot = false
                        self.dateSlotConstraintHeight.constant = 0
                        self.timeSlotConstraintHeight.constant = 0
                        self.dateCollectionView.isHidden = true
                        self.timeCollectionView.isHidden = true
                        self.pleaseSelectDateLbl.isHidden = true
                        self.titleView.isHidden = true
                    
                }
                
              else   if selctedDeliveryType != "EXPRESS" {
                  
                    self.showTimeSlot = true
                    self.dateSlotConstraintHeight.constant = 63
                    self.timeSlotConstraintHeight.constant = 63
                    self.dateCollectionView.isHidden = false
                    self.timeCollectionView.isHidden = false
                    self.pleaseSelectDateLbl.isHidden = false
                    self.titleView.isHidden = false
                    
                    
                } else {
                    self.showTimeSlot = false
                    self.dateSlotConstraintHeight.constant = 0
                    self.timeSlotConstraintHeight.constant = 0
                    self.dateCollectionView.isHidden = true
                    self.timeCollectionView.isHidden = true
                    self.pleaseSelectDateLbl.isHidden = true
                    self.titleView.isHidden = true

                }
                
                if self.titleTimeSlotLbl.text != "" &&   self.titleTimeSlotLbl.text != nil &&  selctedDeliveryType != "NORMAL" {
                    self.titleView.isHidden = false
                    self.titleViewTopConstraint.constant  = 16
                    self.titleViewBottomConstraint.constant  = 8
                    self.titleLableTopConstraint.constant  = 8
                    self.titleLableBottomConstraint.constant  = 8
                    self.titleViewHeightConstraint.constant  =
                    self.titleView.heightForView(text: cart.cartTimeSlot?.messages?.joined(separator: " , ") ?? "", font: .boldSystemFont(ofSize: 15), width: self.titleView.frame.width - 64) + 16
                } else  {
                    self.titleView.isHidden = true
                    self.titleViewTopConstraint.constant  = 0
                    self.titleViewBottomConstraint.constant  = 0
                    self.titleLableTopConstraint.constant  = 0
                    self.titleLableBottomConstraint.constant  = 0
                    self.titleViewHeightConstraint.constant  = 0
                 
                }
                self.layoutIfNeeded()
            }
            
        }).disposed(by: bag)

       
        
            output.days.map({ (days) -> [DaysSlotsSection] in
                return [.init(header: "", items: days)]
                }).asObservable().bind(to: dateCollectionView.rx.items(dataSource: self.daysSlotsDataSouece())).disposed(by: bag)
            
            output.periods.map { (periods) -> [TimeSlotsSection] in
                return [.init(header: "", items: periods)]
                }.asObservable().bind(to: timeCollectionView.rx.items(dataSource: self.timesSlotsDataSouece())).disposed(by: bag)
        
        output.periods.map { (array) -> Bool in
            return array.count == 0
        }.asObservable().bind(to: timeCollectionView.rx.isHidden).disposed(by: bag)
        
        output.periods.map { (array) -> Bool in
            return array.count != 0
        }.asObservable().bind(to: self.pleaseSelectDateLbl.rx.isHidden).disposed(by: bag)

        output.periods.asObservable().subscribe(onNext: { (periods)   in
            if self.showTimeSlot {
                self.timeSlotConstraintHeight.constant = CGFloat(periods.count  * 63)
                self.layoutIfNeeded()
            }
        }).disposed(by: bag)
        
            output.refreshCart.asObservable().bind(to: refreshCart).disposed(by: bag)
          
        Driver.combineLatest(output.days , output.selectedDay).map { (days, day) -> (IndexPath?,TimeSlotDay?) in
            guard let row = days.firstIndex(where: { (slot) -> Bool in
                return slot.day == day
            }) ,let day = days.first(where: { (slot) -> Bool in
                return slot.day == day
            }) else{
                return (nil,nil)
            }
            return (IndexPath(item: row, section: 0) ,day)
        }.asObservable().subscribe(onNext: { (ip,day) in
            if let day = day{
                
                DispatchQueue.main.async {
                    self.dateCollectionView.selectItem(at: ip, animated: false, scrollPosition: .right)
                }

           
            self.selectDateManual.onNext(day)
            }
            }).disposed(by: bag)
        
       
        
        Driver.combineLatest(output.periods , output.selectedPeriod).map { (arg0) -> IndexPath? in
            let (periods, selectedPeriod) = arg0
                
            guard let row = periods.firstIndex(where: { (period) -> Bool in
                return period.code == selectedPeriod
            })else{
                return nil
            }
            return IndexPath(item: row, section: 0)
            }.asObservable().subscribe(onNext: { (ip) in
                if let ip = ip {
                self.timeCollectionView.selectItem(at: ip, animated: false, scrollPosition: .right)

                    
                }
                }).disposed(by: bag)

    }
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.bag = DisposeBag()
    }

    func daysSlotsDataSouece() -> RxCollectionViewSectionedAnimatedDataSource<DaysSlotsSection>
    {
        return RxCollectionViewSectionedAnimatedDataSource<DaysSlotsSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .none, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, cv, indexPath, item in
            guard let cell = cv.dequeueReusableCell(withReuseIdentifier:  "DateSlotCell", for: indexPath) as? DateSlotCell else { return UICollectionViewCell()}
            cell.setUp(date: item)
            return cell
                
            })
    }
    
       func timesSlotsDataSouece() -> RxCollectionViewSectionedAnimatedDataSource<TimeSlotsSection>
       {
           return RxCollectionViewSectionedAnimatedDataSource<TimeSlotsSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .none, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, cv, indexPath, item in
               
    
                    guard let cell = cv.dequeueReusableCell(withReuseIdentifier: "TimeSlotsCell", for: indexPath) as? TimeSlotsCell else { return UICollectionViewCell()}
            
            cell.setUp(time: item)
                return cell
                   
            })
       }

    
    
}
extension DeliverySlotCell : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.dateCollectionView {
            return CGSize(width: 120, height: 57)
        }
        else {
            return CGSize(width: UIScreen.main.bounds.width - 20 , height: 57)
 
        }
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if collectionView == self.dateCollectionView  {
//            cell.alpha = 0
//                cell.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
//                UIView.animate(
//                    withDuration: 0.3,
//                    delay: 0.03 * Double(indexPath.row),
//                    animations: {
//                        cell.alpha = 1
//                        cell.transform = CGAffineTransform.identity
//
//                })
//            }
//        }
    }
