//
//  DateSlotCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

 
import Foundation

class DateSlotCell : UICollectionViewCell {
    
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var borderView: UIView!
    var bag = DisposeBag()
 
    override func awakeFromNib() {
        super.awakeFromNib()
        makeUnselected()
    }
    
    func setUp(date : TimeSlotDay) {

        self.dateLbl.text = date.date ?? ""
        self.dayLbl.text = date.day ?? ""

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.bag = DisposeBag()
    }

    override var isSelected: Bool{
        didSet{
            isSelected ? makeSelected() : makeUnselected()
        }
    }

    
    
    func makeSelected() {
        borderView.layer.cornerRadius = 20
        borderView.layer.masksToBounds = true
        borderView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.5529411765, blue: 0.2235294118, alpha: 1)
        borderView.layer.borderWidth = 1
    }
    
    func makeUnselected() {
        borderView.layer.cornerRadius = 20
        borderView.layer.masksToBounds = true
        borderView.layer.borderColor = #colorLiteral(red: 0.9137254902, green: 0.9137254902, blue: 0.9137254902, alpha: 1)
        borderView.layer.borderWidth = 1
    }
 
}
