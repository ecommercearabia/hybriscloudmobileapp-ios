//
//  TimeSlotsCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

class TimeSlotsCell : UICollectionViewCell {
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var unEnableView: UIView!
    var bag = DisposeBag()
 
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUp(time : Period) {
        self.timeLbl.text = time.intervalFormattedValue ?? ""
        self.unEnableView.isHidden = true
        if !(time.enabled ?? false) {self.makeUnEnable()} else {self.makeEnable()}
        isSelected ? makeSelected() : makeUnselected()

    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.bag = DisposeBag()
    }

    override var isSelected: Bool{
        didSet{
            isSelected ? makeSelected() : makeUnselected()
        }
    }

    func makeSelected() {
        borderView.layer.cornerRadius = 20
        borderView.layer.masksToBounds = true
        borderView.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.5529411765, blue: 0.2235294118, alpha: 1)
        borderView.layer.borderWidth = 1
    }
    
    func makeUnselected() {
        borderView.layer.cornerRadius = 20
        borderView.layer.masksToBounds = true
        borderView.layer.borderColor = #colorLiteral(red: 0.9137254902, green: 0.9137254902, blue: 0.9137254902, alpha: 1)
        borderView.layer.borderWidth = 1
    }

    func makeUnEnable(){
        self.unEnableView.isHidden = false
        borderView.layer.cornerRadius = 20
        borderView.layer.masksToBounds = true
        borderView.layer.borderColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
        borderView.layer.borderWidth = 1
        self.timeLbl.text = "\(self.timeLbl.text ?? "") " + "Unavailable".localized()
        self.timeLbl.textColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
        unEnableView.transform = CGAffineTransform(rotationAngle: 3)
        self.isUserInteractionEnabled = false
    }
    
    func makeEnable(){
        self.unEnableView.isHidden = true
        makeUnselected()
        self.timeLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.isUserInteractionEnabled = true
    }

    
 
}


 
