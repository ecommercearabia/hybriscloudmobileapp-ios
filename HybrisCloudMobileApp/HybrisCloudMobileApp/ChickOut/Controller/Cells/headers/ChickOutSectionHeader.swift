//
//  ChickOutSectionHeader.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/12/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class ChickOutSectionHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var arrow: UIImageView!
    
    func setUp(isSelected : Bool) {
        var toImage = UIImage()
        if isSelected {
            toImage = UIImage(named: "downArrow") ?? UIImage()
        }
        else {
             toImage = UIImage(named: "rightArrowNew") ?? UIImage()
        }
        self.arrow.image = toImage

    }
 
    
 }
