//
//  ChickOutWithSeeMore.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation


class ChickOutWithSeeMore : UITableViewHeaderFooterView {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var seeMoreBut: UIButton!
    var seeMoreActionCallBack: (() -> Void)?
    let disposeBag = DisposeBag()

    
    override func awakeFromNib() {
        self.seeMoreBut.rx.tap.subscribe(onNext: { (void) in
            self.seeMoreActionCallBack?()
        }).disposed(by: disposeBag)
        
    }
 }
