//
//  storeCreditModesCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 9/7/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class storeCreditModesCell: UITableViewCell {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneBut: DesignableButton!
    
    @IBOutlet weak var amount: UITextField!
    @IBOutlet weak var availableAmount: UILabel!
    @IBOutlet weak var hightConstraint: NSLayoutConstraint!
 
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLableTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLableBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    
    let modesSubject = PublishSubject<StoreCreditModes>()
    let cartModelSubject = PublishSubject<CartModel>()
    let selectModeManual = PublishSubject<StoreCreditMode>()
    let availableAmountModel = PublishSubject<StoreCreditAvailableAmountModel>()
    let config = PublishSubject<ConfigModel>()
    let refreshCart = PublishSubject<Void>()
    var bag = DisposeBag()
    var enableConfig = false
    var viewModel = StoreCreditModesCellVM()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setUp(count : Int){
        self.tableView.allowsMultipleSelection = false
                self.tableView.dataSource = nil
    let prepareForReuse = rx.sentMessage(#selector(UITableViewCell.prepareForReuse))
            .mapToVoid()
            .asDriverOnErrorJustComplete()

        let input = StoreCreditModesCellVM.Input(trigger: prepareForReuse,
                modesSubject: modesSubject.asDriverOnErrorJustComplete(),
                cart: cartModelSubject.asDriverOnErrorJustComplete(),
                selectStoreCreditMode: Driver.merge(tableView.rx.modelSelected(StoreCreditMode.self).asDriverOnErrorJustComplete(), selectModeManual.asDriverOnErrorJustComplete()), selectStoreCreditModeManual: tableView.rx.modelSelected(StoreCreditMode.self).asDriverOnErrorJustComplete(), AvailableAmountModel: availableAmountModel.asDriverOnErrorJustComplete(),
                spacificAmount: self.amount.rx.text.orEmpty.asDriver(),
                doneTrigger:self.doneBut.rx.tap.asDriver()

        )
        
       
        
        self.config.asObserver().subscribe(onNext: { (data) in
            self.enableConfig = data.storeCreditConfiguration?.enableCheckTotalPriceWithStoreCreditLimit ?? false
            if  (data.storeCreditConfiguration?.enableCheckTotalPriceWithStoreCreditLimit ?? false ){
                self.hightConstraint.constant = 135
                self.titleLabel.text = "You need to have a minimum payment of".localized() +  " \(data.storeCreditConfiguration?.totalPriceWithStoreCreditLimit ?? 0)" + " AED".localized()  + " in Cash or Credit Card".localized()
                self.titleView.isHidden = false
                self.titleViewBottomConstraint.constant  = 6
                self.titleLableTopConstraint.constant  = 8
                self.titleLableBottomConstraint.constant  = 8
                self.titleViewHeightConstraint.constant  =
                self.titleView.heightForView(text: self.titleLabel.text ?? "", font: .boldSystemFont(ofSize: 15), width: self.titleView.frame.width - 64) + 16
            } else {
                self.titleView.isHidden = true
                self.titleViewBottomConstraint.constant  = 0
                self.titleLableTopConstraint.constant  = 0
                self.titleLableBottomConstraint.constant  = 0
                self.titleViewHeightConstraint.constant  = 0
            }
        }).disposed(by: bag)

        let output = self.viewModel.transform(input: input)
        output.error.asObservable().subscribe(onNext: { (erorr) in
            
                 let errorInModel = erorr as? ErrorResponse
                 
                 let  msgError =  errorInModel?.errors?.map({ (errorResponseElement) -> String in
                               return errorResponseElement.message ?? ""
                           }).joined(separator: " , ")
                           
            UIApplication.topViewController()?.errorLoaf(message: msgError ?? "" )
        }).disposed(by: bag)
        output.storeCreditModes.map({ (modes) -> [StoreCreditModesSectionModel] in
            return [.init(header: "", items: modes )]
            }).asObservable().bind(to: tableView.rx.items(dataSource: self.dataSourcePaymentModes())).disposed(by: bag)

        Driver.combineLatest(output.storeCreditModes , output.selectStoreCreditMode).map { (Modes, selectedMode) -> (IndexPath?,StoreCreditMode?) in
            guard let row = Modes.firstIndex(where: { (Mode) -> Bool in
                return Mode.storeCreditModeType?.code == selectedMode
            }) ,let storeCreditMode = Modes.first(where: { (Mode) -> Bool in
                    return Mode.storeCreditModeType?.code == selectedMode
                   }) else{
                       return (nil,nil)
                   }
                        return (IndexPath(row: row, section: 0) ,storeCreditMode)
                    }.asObservable().subscribe(onNext: { (ip,mode) in
                       
                            self.tableView.selectRow(at: ip, animated: true, scrollPosition: .none)
                        self.refreshCart.onNext(())
                        
                        }).disposed(by: bag)

        output.AvailableAmountModel.asObservable().map { (storeCreditAvailableAmountModel) in
           return storeCreditAvailableAmountModel.formattedValue ?? ""
            }.bind(to: self.availableAmount.rx.text).disposed(by: bag)
        
       output.selectStoreCreditMode.asObservable().map { (modeCode) -> Bool in
        return modeCode != "REDEEM_SPECIFIC_AMOUNT"
        }.bind(to: self.amount.rx.isHidden).disposed(by: bag)
        
        output.selectStoreCreditModeManual.asObservable().map { (modeCode) -> Bool in
         return modeCode != "REDEEM_SPECIFIC_AMOUNT"
         }.bind(to: self.amount.rx.isHidden).disposed(by: bag)

        output.selectStoreCreditModeManual.asObservable().subscribe(onNext: { (code) in
 
            if self.enableConfig {
                if code == "REDEEM_SPECIFIC_AMOUNT" {
                    self.hightConstraint.constant = (44 * CGFloat(count)) + 185
                }
                else {
                    self.hightConstraint.constant = (44 * CGFloat(count )) + 135
                }
            } else {
                if code == "REDEEM_SPECIFIC_AMOUNT" {
                    self.hightConstraint.constant = (44 * CGFloat(count)) + 135
                }
                else {
                    self.hightConstraint.constant = (44 * CGFloat(count )) + 90.0
                }
            }
         
        
            }).disposed(by: bag)
        
        Driver.combineLatest(output.selectStoreCreditModeManual , output.cart , self.amount.rx.text.orEmpty.asDriver()).flatMapLatest { (arg0) -> Driver<Bool> in
            let (modeCode, cart, amount) = arg0
            self.amount.placeholder = "Specific Amount : Maximum \(cart.subTotal?.formattedValue ?? "")"
            if modeCode == "REDEEM_SPECIFIC_AMOUNT"{
                return Driver.just(((cart.subTotal?.value ?? 0.0) >= ((amount) as NSString).doubleValue) )
            }
            else {
                self.amount.text = ""
                return Driver.just(true)
            }
                }.asObservable().bind(to: self.doneBut.rx.isEnabled).disposed(by: bag)

         
        
        output.selectStoreCreditModeManual.asObservable().map { (modeCode) -> Bool in
         return modeCode != "REDEEM_SPECIFIC_AMOUNT"
         }.bind(to: self.doneBut.rx.isEnabled).disposed(by: bag)

 
        output.spacificAmount.asObservable().bind(to: self.amount.rx.text).disposed(by: bag)
        
 
        output.refreshCart.asObservable().bind(to: refreshCart).disposed(by: bag)
        
       
       
        
    }
    
        func dataSourcePaymentModes() -> RxTableViewSectionedAnimatedDataSource<StoreCreditModesSectionModel>
        {
            return RxTableViewSectionedAnimatedDataSource<StoreCreditModesSectionModel>(animationConfiguration: AnimationConfiguration(insertAnimation: .none, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, cv, indexPath, item in
                     guard let cell = cv.dequeueReusableCell(withIdentifier: "storeCreditModeCell", for: indexPath) as? storeCreditModeCell else { return UITableViewCell()}
                cell.setUp(StoreCreditMode : item)
                
                return cell
     
                })
        }

    

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.bag = DisposeBag()
    }
    
    
    

}
