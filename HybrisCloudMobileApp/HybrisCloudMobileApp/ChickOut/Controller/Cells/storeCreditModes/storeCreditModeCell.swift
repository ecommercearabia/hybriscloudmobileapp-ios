//
//  storeCreditModeCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 9/7/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class storeCreditModeCell: UITableViewCell {

    @IBOutlet weak var radioBut: UIImageView!
    @IBOutlet weak var name: UILabel!
    var bag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUp(StoreCreditMode : StoreCreditMode){
        self.name.text = StoreCreditMode.name ?? ""
     }

 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
     selected ? self.makeSelected() : makeUnselected()

    }

       override func prepareForReuse() {
           super.prepareForReuse()
           self.bag = DisposeBag()
       }

    
       func makeSelected () {
           radioBut.image = UIImage(named: "radioButtonSelected")
       }
       
       func makeUnselected() {
           radioBut.image = UIImage(named: "radioButtonUnSelected")
       }

    
}
