//
//  CartItemsCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/15/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class CartItemsCell: UITableViewCell {
    @IBOutlet weak var tableView: UITableView!
    var disposeBag = DisposeBag()

    var cartItemsSubject = BehaviorSubject<[CartSection]>(value: [CartSection(header: "", items: [])])

    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    func setUp (cart : CartModel) {
        self.tableView.dataSource = nil

        self.cartItemsSubject.bind(to: tableView.rx.items(dataSource: self.dataSourceCart())).disposed(by: disposeBag)
        self.cartItemsSubject.onNext([CartSection(header: "", items: cart.entries ?? [])])

    }
    
       func dataSourceCart() ->
        RxTableViewSectionedAnimatedDataSource<CartSection> {
            return RxTableViewSectionedAnimatedDataSource<CartSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .none, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, cv, indexPath, item in

             guard let cell = cv.dequeueReusableCell(withIdentifier: "CartItemCell", for: indexPath) as? CartItemCell else {
                       return UITableViewCell()
                   }
             cell.setUp(item: item)
            
                    return cell

           })
       }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }

}

