//
//  CartItemCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/15/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

class CartItemCell: UITableViewCell {
    @IBOutlet weak var img: DesignableImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var qtyLbl: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
  
    let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setUp(item : CartEntry){
        
        self.name.text = item.product?.name ?? ""
        self.code.text = "SKU: \(item.product?.code ?? "")"
        self.price.text = item.basePrice?.formattedValue ?? ""
        self.qtyLbl.text = String(item.quantity ?? 0)
        self.totalPrice.text = item.totalPrice?.formattedValue ?? ""
        
        let productImage =  item.product?.images.map({ (productImage) -> [CartProductBanner] in
            return productImage.filter ({ (image) -> Bool in
                image.format == "zoom"
                
            })
        })

        self.img?.downloadImageInCellWithoutCorner(urlString: productImage?.first?.url ?? "" )
        
        
 
    }

}
