//
//  ConfirmationCartItemsCell.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 9/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
class ConfirmationCartItemsCell: UITableViewCell {
    @IBOutlet weak var tableView: UITableView!
    let disposeBag = DisposeBag()

    var cartItemsSubject = BehaviorSubject<[CartSection]>(value: [CartSection(header: "", items: [])])

    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    func setUp (placeOrder : PalceOrderResponse) {
        self.tableView.dataSource = nil

        self.cartItemsSubject.bind(to: tableView.rx.items(dataSource: self.dataSourceCart())).disposed(by: disposeBag)
        self.cartItemsSubject.onNext([CartSection(header: "", items: placeOrder.entries ?? [])])

    }
    
       func dataSourceCart() ->
        RxTableViewSectionedAnimatedDataSource<CartSection> {
            return RxTableViewSectionedAnimatedDataSource<CartSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .none, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, cv, indexPath, item in

             guard let cell = cv.dequeueReusableCell(withIdentifier: "CartItemCell", for: indexPath) as? CartItemCell else {
                       return UITableViewCell()
                   }
             cell.setUp(item: item)
            
                    return cell

           })
       }

}

