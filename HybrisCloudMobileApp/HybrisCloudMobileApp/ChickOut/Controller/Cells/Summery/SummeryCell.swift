//
//  SummeryCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/13/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class SummeryCell: UITableViewCell {
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var netAmount: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var couponUsed: UILabel!
    @IBOutlet weak var couponStack: UIStackView!
    @IBOutlet weak var storeCreditLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }
    
    func setUp(cart : CartModel) {
        self.subTotal.text = cart.subTotalBeforeSavingPrice?.formattedValue ?? ""
        self.netAmount.text = cart.totalTax?.formattedValue ?? ""
        self.total.text = cart.totalPriceWithTax?.formattedValue ?? ""
        self.discount.text = cart.totalDiscounts?.formattedValue ?? ""
        self.storeCreditLabel.text = cart.storeCreditAmount?.formattedValue ?? ""
        
        let appliedVouchers =  cart.appliedVouchers?.map({ (appliedVouchers) -> String in
            return appliedVouchers.code ?? ""
        })
        
        self.couponUsed.text = appliedVouchers?.joined(separator: ", ")

        if (cart.appliedVouchers?.count ?? 0) > 0 {
            self.couponStack.isHidden = false
        }
        else {
            self.couponStack.isHidden = true
        }
        
    }

}
