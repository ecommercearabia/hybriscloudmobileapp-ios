//
//  ConfirmationSummeryCell.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 9/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class ConfirmationSummeryCell: UITableViewCell {
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var netAmount: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var discount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }
    
    func setUp(placeOrder : PalceOrderResponse) {
        self.subTotal.text = placeOrder.subTotal?.formattedValue ?? ""
        self.netAmount.text = placeOrder.totalTax?.formattedValue ?? ""
        self.total.text = placeOrder.totalPrice?.formattedValue ?? ""
        self.discount.text = placeOrder.totalDiscounts?.formattedValue ?? ""
    }

}
