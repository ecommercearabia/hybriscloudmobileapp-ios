//
//  PaymentmodeCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/13/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

class PaymentModeCell: UITableViewCell {
    
    @IBOutlet weak var radioImage: UIImageView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    var bag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     }
    
    func setUp(paymentMode : PaymentMode){
        self.name.text = paymentMode.name ?? ""
        switch paymentMode.code {
        
        case "card":
            self.img.image = UIImage(named: "payment")
            
        case "ccod":
            self.img.image = UIImage(named: "cardOnDelivery")
        case "cod":
            self.img.image = UIImage(named: "cashOnDelivery")

        case "apple" :
            self.img.image = UIImage(named: "payment")
        default:
            print("default")
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    selected ? self.makeSelected() : makeUnselected()

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.bag = DisposeBag()
    }

 
    func makeSelected () {
        radioImage.image = UIImage(named: "radioButtonSelected")
    }
    
    func makeUnselected() {
        radioImage.image = UIImage(named: "radioButtonUnSelected")
    }
    
}
