//
//  DeliveryModeCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/12/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class PaymentModesCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var hightConstraint: NSLayoutConstraint!
    
    let modesSubject = PublishSubject<PaymentModesModel>()
    let cartModelSubject = PublishSubject<CartModel>()
    let selectModeManual = PublishSubject<PaymentMode>()
    let refreshCart = PublishSubject<Void>()
    var activityIndicator : ActivityIndicator!
    var bag = DisposeBag()
    var viewModel = PaymentModesCellViewModel()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.bag = DisposeBag()
    }

    
    func setUp(){
        self.tableView.allowsMultipleSelection = false
        self.tableView.dataSource = nil

        self.viewModel.activityIndicator = self.activityIndicator
        
        let input = PaymentModesCellViewModel.Input(trigger: Driver<Void>.just(()),
                                                    paymentModesModesModel: modesSubject.asDriverOnErrorJustComplete(),
                                                    cart: cartModelSubject.asDriverOnErrorJustComplete(),
                                                    selectPaymentMode: Driver.merge(tableView.rx.modelSelected(PaymentModesSection.Item.self).asDriverOnErrorJustComplete(),selectModeManual.asDriverOnErrorJustComplete()))
        
        let output = self.viewModel.transform(input: input)

        output.paymentModes.map({ (modes) -> [PaymentModesSection] in

            return [.init(header: "", items: modes)]
            
            }).asObservable().bind(to: tableView.rx.items(dataSource: self.dataSourcePaymentModes())).disposed(by: bag)

        Driver.combineLatest(output.paymentModes , output.selectpaymentMode).map { (Modes, selectedMode) -> (IndexPath?,PaymentMode?) in
                   guard let row = Modes.firstIndex(where: { (Mode) -> Bool in
                    return Mode.code == selectedMode
                   }) ,let day = Modes.first(where: { (Mode) -> Bool in
                    return Mode.code == selectedMode
                   }) else{
                       return (nil,nil)
                   }
                        return (IndexPath(row: row, section: 0) ,day)
                    }.asObservable().subscribe(onNext: { (ip,mode) in
                       
                            self.tableView.selectRow(at: ip, animated: true, scrollPosition: .none)
                       self.refreshCart.onNext(())
                        
                        }).disposed(by: bag)

        output.refreshCart.asObservable().bind(to: refreshCart).disposed(by: bag)

        

        
    }
    
    
    
    func dataSourcePaymentModes() -> RxTableViewSectionedAnimatedDataSource<PaymentModesSection>
    {
        return RxTableViewSectionedAnimatedDataSource<PaymentModesSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .none, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, cv, indexPath, item in
                 guard let cell = cv.dequeueReusableCell(withIdentifier: "PaymentModeCell", for: indexPath) as? PaymentModeCell else { return UITableViewCell()}
            
            cell.setUp(paymentMode : item)
            
            return cell
 
            })
    }

}
