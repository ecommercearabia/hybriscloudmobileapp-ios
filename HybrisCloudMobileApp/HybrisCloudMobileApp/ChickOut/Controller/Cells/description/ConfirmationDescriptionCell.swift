//
//  ConfirmationDescriptionCell.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 9/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

class ConfirmationDescriptionCell: UITableViewCell {
    
  @IBOutlet weak var descriptionConfirmation: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }
    
    func setUp(placeOrder : PalceOrderResponse) {
        
        let msg = "\("Your Order Number is".localized()) \(placeOrder.code ?? "") \("A copy of your order details has been sent to".localized()) \(placeOrder.user?.uid ?? "")"
        
        self.descriptionConfirmation.text = msg
       
    }

}
