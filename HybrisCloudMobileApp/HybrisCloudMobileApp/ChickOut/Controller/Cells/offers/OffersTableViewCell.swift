//
//  OffersTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by admin on 07/04/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import UIKit


class OffersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bankOfferTableView: UITableView!
    @IBOutlet weak var bankOfferTableViewHieght: NSLayoutConstraint!
    @IBOutlet weak var voucherTextField: UITextField!
    @IBOutlet weak var applyButton: DesignableButton!
    @IBOutlet weak var vouchersCollectionView: UICollectionView!
    @IBOutlet weak var vouchersUIVIew: UIView!
    @IBOutlet weak var haveCodeLabel: UILabel!
    
    var offerCellViewModel = OfferCellViewModel(getCartUseCase: NetworkPlatform.UseCaseProvider().makeCartUseCase())
    let bankOffersModelSubject = PublishSubject<BankOffers>()
    let promoCodeMsgsSubject = PublishSubject<[PromoCodeMsgsSection]>()
    let cartModelSubject = PublishSubject<CartModel>()
    var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.haveCodeLabel.text = "Do you have a code?".localized()
        self.voucherTextField.setLeftPaddingPoints(10)
    }
    
    //MARK: Setup Cell
    func setupCell() {
        self.initViewModel()
        self.bindToBankTableView()
        self.bindtoVoucherCollectionView()
        
       
    }
    
    //MARK: Prepare For Reuse
    override func prepareForReuse() {
        super.prepareForReuse()
        self.disposeBag = DisposeBag()
    }
    
    private func initViewModel(){
 
        let voucherCode = voucherTextField
            .rx.text
            .observe(on: MainScheduler.instance)
            .asDriverOnErrorJustComplete()
        
        
        let input = OfferCellViewModel.Input(voucherCode: voucherCode,
                                             applyVoucher: applyButton.rx.tap.asDriverOnErrorJustComplete(),
                                             voucherCodeNeedDelete: Observable<Void>.just(()).asDriverOnErrorJustComplete(),
                                             deleteVoucher: Observable<Void>.just(()).asDriverOnErrorJustComplete())
        
        let output = self.offerCellViewModel.transform(input: input)
        
        output.applyVoucher.asObservable().subscribe(onNext: {
            self.offerCellViewModel.refreshCart.onNext(())
        }).disposed(by: disposeBag)
        
        output.applyVoucherError.asObservable().subscribe(onNext: { (error) in
        let errors = error as? ErrorResponse
            UIApplication.topViewController()?.errorLoaf(message: errors?.errors?.first?.message ?? "")
        }).disposed(by: disposeBag)
    }
    
    //MARK: Bind Offers To Bank Offer Table
    private func bindToBankTableView() {
        self.bankOffersModelSubject.subscribe(onNext: { (bankOffers) in
            
            let bankOfferCells = bankOffers.bankOffers?.map({ (BankOffer) -> PromoCodeMsgsCellIndexes in
                return PromoCodeMsgsCellIndexes.bankOffer(bankOffer: BankOffer)
            })
            
            var bankOffersTableHight = 0
            if (bankOffers.bankOffers?.count ?? 0) > 0  {
                bankOffersTableHight = bankOffersTableHight + (50 * (bankOffers.bankOffers?.count ?? 0) )
            }
            
            self.bankOfferTableViewHieght.constant = CGFloat(bankOffersTableHight)
            
            self.promoCodeMsgsSubject.bind(to: self.bankOfferTableView.rx.items(dataSource: (self.offerCellViewModel.bankOfferDataSource()))).disposed(by: self.disposeBag)
            self.promoCodeMsgsSubject.onNext([PromoCodeMsgsSection(model: "", items: bankOfferCells ?? [])])
        }).disposed(by: disposeBag)
    }
    
    private func bindtoVoucherCollectionView() {
        
        self.cartModelSubject.map { (CartModel) -> Bool in
            return !((CartModel.appliedVouchers?.count ?? 0) > 0)
        }.bind(to: self.vouchersUIVIew.rx.isHidden).disposed(by: disposeBag)
        
        self.cartModelSubject.map { (CartModel) -> [VoucherSection] in
            return [VoucherSection(header: "", items: CartModel.appliedVouchers ?? [])]
        }.asObservable().bind(to: self.vouchersCollectionView.rx.items(dataSource: self.offerCellViewModel.dataSourceVoucher())).disposed(by: disposeBag)
        
        self.cartModelSubject.subscribe(onNext: { (CartModel) in
            self.voucherTextField.text = ""
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.vouchersCollectionView.collectionViewLayout.invalidateLayout()
            }

        }).disposed(by: disposeBag)
    }
    
}
