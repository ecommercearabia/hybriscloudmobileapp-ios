//
//  ChickOutStep2SectionModel.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//
import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxDataSources


typealias ChcikOutSectionModel = AnimatableSectionModel<String,ChickOutCellIndexes>

enum ChickOutCellIndexes :IdentifiableType, Equatable{

    typealias Identity = String

    public var identity: Identity {

        switch self {

        case .timeSlots:
            return "timeSlots"
        case .bankOffer:
            return "bankOffer"
        case .paymentMode:
            return "paymentMode"
        case .storeCreditModes:
            return "storeCreditModes"
        case .orderSummary:
            return "orderSummary"
        case .shippingAddress:
            return "shippingAddress"
        case .myItems:
            return "myItems"
        case .potentialOrderPromotion(potentialOrderPromotion: let potentialOrderPromotion):
            return potentialOrderPromotion.promotion?.code ?? ""
        case .appliedOrderPromotion(appliedOrderPromotion: let appliedOrderPromotion):
            return appliedOrderPromotion.promotion?.code ?? ""
        }


    }
 
    static func == (lhs: ChickOutCellIndexes, rhs: ChickOutCellIndexes) -> Bool {
        return false// lhs.identity == rhs.identity
        
        
        
        //false//lhs.identity == rhs.identity
    }

    case timeSlots (cartModel : CartModel,TimeSlots : TimeSlotsModel)
    case bankOffer (cartModel: CartModel, bankOffer: BankOffers)
    case potentialOrderPromotion (potentialOrderPromotion : PotentialOrderPromotion)
    case appliedOrderPromotion (appliedOrderPromotion : AppliedOrderPromotion)
    case paymentMode (cartModel : CartModel,PaymentModes : PaymentModesModel)
    case storeCreditModes (cartModel : CartModel,CreditModes : StoreCreditModes , availableAmount : StoreCreditAvailableAmountModel , confg : ConfigModel)
    case orderSummary (cartModel : CartModel)
    case shippingAddress (cartModel : CartModel)
    case myItems (cartModel : CartModel)
}


 
  
