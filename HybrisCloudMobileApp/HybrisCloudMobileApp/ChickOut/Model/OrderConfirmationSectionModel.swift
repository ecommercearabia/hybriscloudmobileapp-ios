//
//  OrderConfirmationSectionModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 9/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

typealias OrderConfirmationSectionModel = AnimatableSectionModel<String,OrderConfirmationCellIndexes>

enum OrderConfirmationCellIndexes :IdentifiableType, Equatable{

    typealias Identity = String

    public var identity: Identity {

        switch self {
        case .orderDescription:
            return "orderDescription"
        case .orderSummary:
            return "orderSummary"
        case .shippingAddress:
            return "AddressCell"
        case .myItems:
            return "myItems"
        }


    }
 
    static func == (lhs: OrderConfirmationCellIndexes, rhs: OrderConfirmationCellIndexes) -> Bool {
        return false//lhs.identity == rhs.identity
    }


    case orderDescription (orderResponse : PalceOrderResponse)
    case orderSummary (orderResponse : PalceOrderResponse)
    case shippingAddress (orderResponse : PalceOrderResponse)
    case myItems (orderResponse : PalceOrderResponse)
}
