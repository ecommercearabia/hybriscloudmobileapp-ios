//
//  PaymentGatewayResponse.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/19/20.
//  Copyright © 2020 Erabia. All rights reserved.
//
import Foundation

// MARK: - PaymentGatewayResponse
public struct PaymentGatewayResponse: Codable {
    public let merchantParam3, deliveryTel, merchantParam5, paymentMode: String?
    public let trackingID, bankRefNo, deliveryZip, billingCountry: String?
    public let billingZip, billingState, deliveryName, currency: String?
    public let deliveryCountry, bankQsiNo, statusCode, failureMessage: String?
    public let bankReceiptNo, billingEmail, orderStatus, statusMessage: String?
    public let merchantParam2, deliveryState, orderID, billingCity: String?
    public let merchantParam4, billingName, billingTel, offerType: String?
    public let merchantParam6, responseType, eciValue, merAmount: String?
    public let cardHolderName, cardName, vault, deliveryAddress: String?
    public let discountValue, offerCode, deliveryCity, merchantParam1: String?
    public let billingAddress, amount: String?

    enum CodingKeys: String, CodingKey {
        case merchantParam3 = "merchant_param3"
        case deliveryTel = "delivery_tel"
        case merchantParam5 = "merchant_param5"
        case paymentMode = "payment_mode"
        case trackingID = "tracking_id"
        case bankRefNo = "bank_ref_no"
        case deliveryZip = "delivery_zip"
        case billingCountry = "billing_country"
        case billingZip = "billing_zip"
        case billingState = "billing_state"
        case deliveryName = "delivery_name"
        case currency
        case deliveryCountry = "delivery_country"
        case bankQsiNo = "bank_qsi_no"
        case statusCode = "status_code"
        case failureMessage = "failure_message"
        case bankReceiptNo = "bank_receipt_no"
        case billingEmail = "billing_email"
        case orderStatus = "order_status"
        case statusMessage = "status_message"
        case merchantParam2 = "merchant_param2"
        case deliveryState = "delivery_state"
        case orderID = "order_id"
        case billingCity = "billing_city"
        case merchantParam4 = "merchant_param4"
        case billingName = "billing_name"
        case billingTel = "billing_tel"
        case offerType = "offer_type"
        case merchantParam6 = "merchant_param6"
        case responseType = "response_type"
        case eciValue = "eci_value"
        case merAmount = "mer_amount"
        case cardHolderName = "card_holder_name"
        case cardName = "card_name"
        case vault
        case deliveryAddress = "delivery_address"
        case discountValue = "discount_value"
        case offerCode = "offer_code"
        case deliveryCity = "delivery_city"
        case merchantParam1 = "merchant_param1"
        case billingAddress = "billing_address"
        case amount
    }

    public init(merchantParam3: String?, deliveryTel: String?, merchantParam5: String?, paymentMode: String?, trackingID: String?, bankRefNo: String?, deliveryZip: String?, billingCountry: String?, billingZip: String?, billingState: String?, deliveryName: String?, currency: String?, deliveryCountry: String?, bankQsiNo: String?, statusCode: String?, failureMessage: String?, bankReceiptNo: String?, billingEmail: String?, orderStatus: String?, statusMessage: String?, merchantParam2: String?, deliveryState: String?, orderID: String?, billingCity: String?, merchantParam4: String?, billingName: String?, billingTel: String?, offerType: String?, merchantParam6: String?, responseType: String?, eciValue: String?, merAmount: String?, cardHolderName: String?, cardName: String?, vault: String?, deliveryAddress: String?, discountValue: String?, offerCode: String?, deliveryCity: String?, merchantParam1: String?, billingAddress: String?, amount: String?) {
        self.merchantParam3 = merchantParam3
        self.deliveryTel = deliveryTel
        self.merchantParam5 = merchantParam5
        self.paymentMode = paymentMode
        self.trackingID = trackingID
        self.bankRefNo = bankRefNo
        self.deliveryZip = deliveryZip
        self.billingCountry = billingCountry
        self.billingZip = billingZip
        self.billingState = billingState
        self.deliveryName = deliveryName
        self.currency = currency
        self.deliveryCountry = deliveryCountry
        self.bankQsiNo = bankQsiNo
        self.statusCode = statusCode
        self.failureMessage = failureMessage
        self.bankReceiptNo = bankReceiptNo
        self.billingEmail = billingEmail
        self.orderStatus = orderStatus
        self.statusMessage = statusMessage
        self.merchantParam2 = merchantParam2
        self.deliveryState = deliveryState
        self.orderID = orderID
        self.billingCity = billingCity
        self.merchantParam4 = merchantParam4
        self.billingName = billingName
        self.billingTel = billingTel
        self.offerType = offerType
        self.merchantParam6 = merchantParam6
        self.responseType = responseType
        self.eciValue = eciValue
        self.merAmount = merAmount
        self.cardHolderName = cardHolderName
        self.cardName = cardName
        self.vault = vault
        self.deliveryAddress = deliveryAddress
        self.discountValue = discountValue
        self.offerCode = offerCode
        self.deliveryCity = deliveryCity
        self.merchantParam1 = merchantParam1
        self.billingAddress = billingAddress
        self.amount = amount
    }
}
