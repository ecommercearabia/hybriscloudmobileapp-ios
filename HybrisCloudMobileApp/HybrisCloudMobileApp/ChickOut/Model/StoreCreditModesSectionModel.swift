//
//  StoreCreditModesSectionModel.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 9/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

struct StoreCreditModesSectionModel {
    var header: String
    var items: [Item]
    
}

extension StoreCreditModesSectionModel : AnimatableSectionModelType, Equatable {
    typealias Item = StoreCreditMode

    var identity: String {
        return header
    }

    init(original: StoreCreditModesSectionModel, items: [StoreCreditMode]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: StoreCreditModesSectionModel, rhs: StoreCreditModesSectionModel) -> Bool {
        return lhs.header == rhs.header
    }
}


extension StoreCreditMode: IdentifiableType, Equatable {
    public var identity: String {
        return storeCreditModeType?.code ?? ""
    }
    
    public static func == (lhs: StoreCreditMode, rhs: StoreCreditMode) -> Bool { return lhs.storeCreditModeType?.code == rhs.storeCreditModeType?.code }
}

