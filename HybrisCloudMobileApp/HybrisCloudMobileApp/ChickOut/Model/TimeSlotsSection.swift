//
//  TimeSlotsSection.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//
//
import Foundation
import RxSwift
import RxCocoa
import RxDataSources

struct TimeSlotsSection {
    var header: String
    var items: [Item]
    
}

extension TimeSlotsSection : AnimatableSectionModelType, Equatable {
    typealias Item = Period

    var identity: String {
        return header
    }

    init(original: TimeSlotsSection, items: [Period]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: TimeSlotsSection, rhs: TimeSlotsSection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension Period: IdentifiableType, Equatable {
    public var identity: String {
        return code ?? ""
    }
    
    public static func == (lhs: Period, rhs: Period) -> Bool { return lhs.code == rhs.code }
}

