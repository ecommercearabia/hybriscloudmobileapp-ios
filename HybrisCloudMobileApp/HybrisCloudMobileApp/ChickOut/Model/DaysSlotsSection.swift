//
//  DaysSlotsSectionModel.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//
import Foundation
import RxSwift
import RxCocoa
import RxDataSources

struct DaysSlotsSection {
    var header: String
    var items: [Item]
    
}

extension DaysSlotsSection : AnimatableSectionModelType, Equatable {
    typealias Item = TimeSlotDay

    var identity: String {
        return header
    }

    init(original: DaysSlotsSection, items: [TimeSlotDay]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: DaysSlotsSection, rhs: DaysSlotsSection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension TimeSlotDay: IdentifiableType, Equatable {
    public var identity: String {
        return date ?? ""
    }
    
    public static func == (lhs: TimeSlotDay, rhs: TimeSlotDay) -> Bool { return lhs.date == rhs.date }
}

