//
//  PaymentModesSection.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/13/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

 import Foundation
import RxSwift
import RxCocoa
import RxDataSources

struct PaymentModesSection {
    var header: String
    var items: [Item]
    
}

extension PaymentModesSection : AnimatableSectionModelType, Equatable {
    typealias Item = PaymentMode

    var identity: String {
        return header
    }

    init(original: PaymentModesSection, items: [PaymentMode]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: PaymentModesSection, rhs: PaymentModesSection) -> Bool {
        return lhs.identity == rhs.identity
    }
}


extension PaymentMode: IdentifiableType, Equatable {
    public var identity: String {
        return code ?? ""
    }
    
    public static func == (lhs: PaymentMode, rhs: PaymentMode) -> Bool { return lhs.code == rhs.code }
}

