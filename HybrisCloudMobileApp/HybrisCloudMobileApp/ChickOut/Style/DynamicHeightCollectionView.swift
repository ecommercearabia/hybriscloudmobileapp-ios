//
//  DynamicHeightCollectionView.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

import UIKit

class DynamicHeightCollectionView: UICollectionView {
    override func layoutSubviews() {
        super.layoutSubviews()
        if !__CGSizeEqualToSize(bounds.size, self.intrinsicContentSize) {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return contentSize
    }
}
