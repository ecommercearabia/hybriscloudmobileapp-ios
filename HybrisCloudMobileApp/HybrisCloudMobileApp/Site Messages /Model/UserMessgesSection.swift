//
//  UserMessgesSection.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 9/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//



import Foundation
import RxSwift
import RxCocoa
import RxDataSources

struct UserMessgesSection {
    var header: String
    var items: [Item]
    
}

extension UserMessgesSection : AnimatableSectionModelType, Equatable {

    
    typealias Item = Message

    var identity: String {
        return header
    }

    init(original: UserMessgesSection, items: [Message]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: UserMessgesSection, rhs: UserMessgesSection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension Message: IdentifiableType, Equatable {
    public var identity: String {
        return  uid ?? ""
    }
    
    public static func == (lhs: Message, rhs: Message) -> Bool {
        return (lhs.uid  == rhs.uid)
    }
}

