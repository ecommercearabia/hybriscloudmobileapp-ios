//
//  UserMessagesViewController.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 9/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class UserMessagesViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!{didSet{tableView.tableFooterView = UIView.init(frame: CGRect.zero)}}
    @IBOutlet weak var emptyView: UIView!

    let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
    let disposeBag = DisposeBag()
    var viewModel : UserMessagesViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = .init(useCase: networkUseCaseProvider.makeUserMesseges())
            
          
          let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
                    .mapToVoid()
                    .asDriverOnErrorJustComplete()
        
        
        let input = UserMessagesViewModel.Input(trigger: viewWillAppear)
         let output = viewModel.transform(input: input)

        
        output.messegesList.map { (messegesList) -> [UserMessgesSection] in
            return [UserMessgesSection.init(header: "", items: messegesList.messages ?? [])]
               }.asObservable().bind(to: tableView.rx.items(dataSource: self.viewModel.dataSource())).disposed(by: disposeBag)

                   
    
    output.messegesList.map { (messegesList) -> Bool in
              messegesList.messages?.count ?? 0 != 0
          }.asObservable().bind(to:self.emptyView.rx.isHidden).disposed(by: disposeBag)

          output.messegesList.map { (messegesList) -> Bool in
                          messegesList.messages?.count ?? 0 == 0
               }.asObservable().bind(to:self.tableView.rx.isHidden).disposed(by: disposeBag)
          
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
