//
//  UserMessagesViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 9/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import SwipeCellKit
import KeychainSwift
import RxSwift


class UserMessagesViewModel : ViewModelType{
    let useCase: UserMessagesUseCase
    
    init(useCase: UserMessagesUseCase ) {
        self.useCase = useCase
  
        
    }
  
    struct Input {
        let trigger: Driver<Void>
     
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let messegesList: Driver<UserMessages>
       let error: Driver<Error>

    }
    func transform(input: Input) -> Output {
         let activityIndicator = ActivityIndicator()
         let errorTracker = ErrorTracker()
        
        let messegesList = input.trigger.flatMapLatest { [unowned self] in
                   return self.useCase.getUserMessages()
                       .trackActivity(activityIndicator)
                       .trackError(errorTracker)
                       .asDriverOnErrorJustComplete()
               }

      let fetching = activityIndicator.asDriver()
      let errors = errorTracker.asDriver()
        return Output(fetching: fetching, messegesList: messegesList,error: errors)
    }
    func dataSource() ->
            RxTableViewSectionedAnimatedDataSource<UserMessgesSection> {
            
                    return RxTableViewSectionedAnimatedDataSource<UserMessgesSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .bottom),configureCell: { ds, cv, indexPath, item in
                   
               
                         guard let cell = cv.dequeueReusableCell(withIdentifier: "UserMessegesTableViewCell", for: indexPath) as? UserMessegesTableViewCell else { return UITableViewCell()}
                  
                            cell.setup(messegeData: item)

                       
                    
                    return cell
                    
                })
        }

}
