//
//  BestDealProductSection.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxDataSources

struct BestDealProductSection {
    var header: String
    var items: [Item]
    
}

extension BestDealProductSection : AnimatableSectionModelType, Equatable {
    typealias Item = ProductDetails
    
    var identity: String {
        return header
    }
    
    init(original: BestDealProductSection, items: [ProductDetails]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: BestDealProductSection, rhs: BestDealProductSection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension ProductDetails: IdentifiableType, Equatable {
    
    public static func == (lhs: ProductDetails, rhs: ProductDetails) -> Bool {
        return  lhs.identity  == rhs.identity
    }
}



