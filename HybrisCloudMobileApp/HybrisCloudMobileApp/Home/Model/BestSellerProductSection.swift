//
//  BestSellerProductSection.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/6/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import RxSwift
import RxCocoa
import RxDataSources

struct BestSellerProductSection {
    var header: String
    var items: [Item]
    
}

extension BestSellerProductSection : AnimatableSectionModelType, Equatable {
 
    typealias Item = BestSellerProducts

    var identity: String {
        return header
    }

    init(original: BestSellerProductSection, items: [BestSellerProducts]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: BestSellerProductSection, rhs: BestSellerProductSection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension BestSellerProducts: IdentifiableType, Equatable {
    public var identity: String {
        return  uid ?? ""
    }
    
    public static func == (lhs: BestSellerProducts, rhs: BestSellerProducts) -> Bool { return lhs.uid  == rhs.uid }
}

