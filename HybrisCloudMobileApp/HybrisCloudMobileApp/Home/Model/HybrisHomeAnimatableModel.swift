//
//  HybrisHomeAnimatableModel.swift
//  HybrisCloudMobileApp
//
//  Created by Ahmad Bader on 7/14/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxDataSources



extension HybrisContentSlot : IdentifiableType,Equatable{
    public typealias Identity = String
    
    public var identity : Identity {
        get {
            return self.slotID ?? ""
        }
        
    }
   
    
    public static func == (lhs: HybrisContentSlot, rhs: HybrisContentSlot) -> Bool {
        return lhs.slotID == rhs.slotID
    }
    
   
}


extension HybrisComponent : IdentifiableType,Equatable{
    public typealias Identity = String
    
    public var identity : Identity {
        get {
            return self.uid ?? ""
        }
        
    }
   
    
    public static func == (lhs: HybrisComponent, rhs: HybrisComponent) -> Bool {
        return lhs.uid == rhs.uid && lhs.modifiedTime == rhs.modifiedTime
    }
    
   
}
