//
//  FeaturedProductsTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/27/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import UIKit

class FeaturedProductsTableViewCell: UITableViewCell {
    
    var numberItemsInRow : CGFloat = 1.6
     let sectionInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
     
     

    @IBOutlet weak var FeaturedProductscollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        FeaturedProductscollectionView.dataSource = self
        FeaturedProductscollectionView.delegate = self
        
        FeaturedProductscollectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension FeaturedProductsTableViewCell : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath)as! ProductCollectionViewCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        // 2
        case UICollectionView.elementKindSectionHeader:
          // 3
          guard
            let headerView = collectionView.dequeueReusableSupplementaryView(
              ofKind: kind,
              withReuseIdentifier: "FeaturedProductsCell",
              for: indexPath) as? HeaderCollectionReusableView
            else {
              fatalError("Invalid view type")
          }

         
          headerView.featuredHederLbl.text = "FEATURED PRODUCTS"
          return headerView
        default:
          // 4
          fatalError("Invalid view type")
            
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
      let padingSpace = (numberItemsInRow + 1) * sectionInsets.left

          
        let availableSpace = self.contentView.frame.width - padingSpace

          let width = availableSpace / numberItemsInRow

          return CGSize(width: width, height: width + width * 0.8)
        // return CGSize(width: 200, height: 350)
    }
    
}
