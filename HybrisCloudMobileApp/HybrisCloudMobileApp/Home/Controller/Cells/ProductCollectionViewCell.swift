//
//  ProductCollectionViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/27/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import UIKit
import SKCountryPicker
import KeychainSwift
import FirebaseAnalytics


class ProductCollectionViewCell: UICollectionViewCell {
    
    var productQuantity : Int = 1
    var maxProductQuantity : Int?
    
    @IBOutlet weak var frozenImage: UIImageView!
    @IBOutlet weak var dryImage: UIImageView!
    @IBOutlet weak var chilledImage: UIImageView!
    @IBOutlet weak var expressStack: UIStackView!
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var itemDetailsLbl: UILabel!
    @IBOutlet weak var itemCurrencyLbl: UILabel!
    @IBOutlet weak var itemPriceLbl: UILabel!
    @IBOutlet weak var itemOldPriceLbl: UILabel!
    @IBOutlet weak var itemQuantityUITextField: UITextField!
    @IBOutlet weak var itemQuantityView: UIView!
    @IBOutlet weak var flagImage: UIImageView!
    @IBOutlet weak var cartBttn: UIButton!
    @IBOutlet weak var increaseQuantityBttn: UIButton!
    @IBOutlet weak var decreaseQuantityBttn: UIButton!
    @IBOutlet weak var outOfStockLbl: UILabel!
    @IBOutlet weak var wishListButton: UIButton!
    
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var discountLbl: UILabel!
    
    @IBOutlet weak var saveingLbl: UILabel!
    
    var quantitySubject = PublishSubject<String>()
    let product = PublishSubject<ProductDetails>()
    let createCartSubject = PublishSubject<Void>()
    let manualAddTopPoductSubject = PublishSubject<Void>()
    var disposeBag = DisposeBag()
    var wishlistViewModel : WishlistViewModel!
    let toLoginSubject = PublishSubject<Void>()
    var productForLog : ProductDetails!
    let addToWishListSubject = PublishSubject<Void>()
    var viewModel : ProductCollectionViewModel = ProductCollectionViewModel(useCase: NetworkPlatform.UseCaseProvider().makeWishListUseCase(), addtoCartUseCase: NetworkPlatform.UseCaseProvider().makeAddToCartUseCase() , cartUseCase : NetworkPlatform.UseCaseProvider().makeCartUseCase())
    
    
    @IBOutlet weak var borderView: UIView!
    
    
    @IBAction func favoriteBttn(_ sender: Any) {
    }
    
    @IBOutlet weak var stepperView: UIView!
    
    
    
    static let identifier = "ProductCollectionViewCell"
    static func nib()-> UINib {
        return UINib(nibName: "ProductCollectionViewCell", bundle: nil)
    }
    
    func removeBorder(){
        borderView.layer.shadowColor = UIColor.clear.cgColor
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //        self.itemImage.cornerRadius = self.itemImage.frame.height / 2
        self.itemQuantityView.cornerRadius = self.itemQuantityView.frame.height / 2
    }
    
    
    func setUp(prodcutDeatis : ProductDetails) {
        
        stepperView.layer.borderWidth = 1
        stepperView.layer.borderColor = #colorLiteral(red: 0.8588235294, green: 0.8588235294, blue: 0.8588235294, alpha: 1)
        
        itemOldPriceLbl.attributedText = ("PreviousPrice".localized()).strikeThrough()
        
        if prodcutDeatis.discount != nil {
            self.discountView.isHidden = false
            self.saveingLbl.isHidden = false
            self.discountLbl.text = "-\(prodcutDeatis.discount?.percentage ?? 0)%"
            self.saveingLbl.text = "Save".localized() + " \(prodcutDeatis.discount?.percentage ?? 0)% " + "Discount".localized()
            self.itemOldPriceLbl.isHidden = false
            
            self.itemPriceLbl.text = prodcutDeatis.discount?.discountPrice?.formattedValue ?? ""
            self.itemOldPriceLbl.attributedText = prodcutDeatis.price?.formattedValue?.strikeThrough()
        }
        else {
            self.discountView.isHidden = true
            self.itemOldPriceLbl.isHidden = true
            self.itemPriceLbl.text = prodcutDeatis.price?.formattedValue ?? ""
            self.saveingLbl.isHidden = true
        }
        
        
        !(prodcutDeatis.frozen ?? false) ? (self.frozenImage.isHidden = true) : (self.frozenImage.isHidden = false)
        !(prodcutDeatis.dry ?? false) ? (self.dryImage.isHidden = true) : (self.dryImage.isHidden = false)
        !(prodcutDeatis.chilled ?? false) ? (self.chilledImage.isHidden = true) : (self.chilledImage.isHidden = false)
        !(prodcutDeatis.express ?? false) ? (self.expressStack.isHidden = true) : (self.expressStack.isHidden = false)
        
        
        
        
        let input = ProductCollectionViewModel.Input(addtoCartTrigger: Driver.merge(cartBttn.rx.tap.asDriverOnErrorJustComplete() , self.manualAddTopPoductSubject.asDriverOnErrorJustComplete())
                                                     , products: product.asObserver().asDriverOnErrorJustComplete(),
                                                     quantity: quantitySubject.asObserver().asDriverOnErrorJustComplete(),
                                                     increaseQuantityTrigger: increaseQuantityBttn.rx.tap.asDriver(),
                                                     decreaseQuantityTrigger: decreaseQuantityBttn.rx.tap.asDriver(),
                                                     createCartTrigger: createCartSubject.asDriverOnErrorJustComplete(),
                                                     addToWishListTrigger: addToWishListSubject.asDriverOnErrorJustComplete())
        
        
        
        let output = viewModel.transform(input: input)
        wishListButton.rx.tap.subscribe(onNext: { (void) in
            if KeychainSwift().get("user") == "anonymous" {
                
                self.toLoginSubject.onNext(())
            } else {
                self.addToWishListSubject.onNext(())
            }
        }).disposed(by: self.disposeBag)
        
        output.addToWishListResult.asObservable().subscribe(onNext: { (AddedToCartModel) in
            
            if KeychainSwift().get("user") == "current" {
            
                Loaf("product added to Wish List successfully".localized(), state: .success(L102Language.isRTL ? .right : .left),location: .top, sender: UIApplication.topViewController() ?? UIViewController()).show()
            }
            
            let price = self.productForLog.price?.value ?? 0.0
            
            let itemDetails: [String: Any] = [
                AnalyticsParameterCurrency: self.productForLog.price?.currencyISO ?? "AED".localized(),
                AnalyticsParameterValue:  price,
                AnalyticsParameterItemID: self.productForLog.code ?? "" ,
                AnalyticsParameterItemName: self.productForLog.name ?? "",
                
            ]
            Analytics.logEvent(AnalyticsEventAddToWishlist, parameters: itemDetails)
            self.wishListButton.tintColor = UIColor(named: "AppMainColor")
            
            
        }).disposed(by: self.disposeBag)
        
        output.addToCartResult.asObservable().subscribe(onNext: { (AddedToCartModel) in
            print("add To Cart Result")
            
            let quantity = (Double(AddedToCartModel.quantity ?? 0) )
            let price = AddedToCartModel.entry?.basePrice?.value ?? 0.0
            let total = quantity * price
            let itemDetails: [String: Any] = [
                AnalyticsParameterCurrency: AddedToCartModel.entry?.basePrice?.currencyISO ?? "",
                AnalyticsParameterValue:  total,
                AnalyticsParameterItemID: AddedToCartModel.entry?.product?.code ?? "" ,
                AnalyticsParameterItemName: AddedToCartModel.entry?.product?.name ?? "",
                AnalyticsParameterPrice: price
            ]
            
            Analytics.logEvent(AnalyticsEventAddToCart, parameters: itemDetails)
            
            
            let tabBarController = UIApplication.topViewController()?.tabBarController
            let cartNav = tabBarController?.viewControllers?[2] as? UINavigationController
            let cartVC =  cartNav?.topViewController as? CartViewController
            cartVC?.cartViewModel.refreshCartSubject.onNext(())
            
            Loaf("product added to cart successfully".localized(), state: .success(L102Language.isRTL ? .right : .left),location: .top, sender: UIApplication.topViewController() ?? UIViewController()).show()
            
        }).disposed(by: self.disposeBag)
        
        
        output.createdCart.asObservable().subscribe(onNext: { (CartModel) in
            print("created Cart ")
            KeychainSwift().set(CartModel.guid ?? "", forKey: "guid")
            self.manualAddTopPoductSubject.onNext(())
        }).disposed(by: self.disposeBag)
        
        
        output.error.asObservable().subscribe(onNext: { (Error) in
            let error = Error as? ErrorResponse
            if error?.errors?.first?.type == "CartError" {
                self.createCartSubject.onNext(())
            }
        }).disposed(by: self.disposeBag)
        
        output.errorAddToWishList.asObservable().subscribe(onNext: { (Error) in
            let error = Error as? ErrorResponse
            UIApplication.topViewController()?.errorLoaf(message: error?.errors?.first?.message ?? "")
        }).disposed(by: self.disposeBag)
        
        
        
        output.increaseQuantityOutput.asObservable().subscribe( onNext:
                                                                    {
                                                                        (quantity) in
                                                                        self.quantitySubject.onNext(quantity)
                                                                        
                                                                    }, onError:
                                                                        {
                                                                            (error) in
                                                                            print(error)
                                                                            
                                                                        }, onCompleted:
                                                                            {}) {}.disposed(by: disposeBag)
        
        output.decreseQuantityOutput.asObservable().subscribe( onNext:
                                                                {
                                                                    (quantity) in
                                                                    
                                                                    self.quantitySubject.onNext(quantity)
                                                                    
                                                                }, onError:
                                                                    {
                                                                        (error) in
                                                                        print(error)
                                                                        
                                                                    }, onCompleted:
                                                                        {}) {}.disposed(by: disposeBag)
        
        
        quantitySubject.bind(to: itemQuantityUITextField.rx.text).disposed(by: disposeBag)
        
        product.onNext(prodcutDeatis)
        productForLog = prodcutDeatis
        quantitySubject.onNext("1")
        
        
        
        
        
        self.maxProductQuantity = Int(prodcutDeatis.stock?.stockLevel ?? 0)
        
        let productImage =  prodcutDeatis.images.map({ (productImage) -> [ProducttImageElement] in
            return productImage.filter ({ (image) -> Bool in
                image.format == "zoom"
                
            })
        })
        
        
        self.itemImage.downloadImageInCellWithoutCornerFullResulution(urlString: productImage?.first?.url ?? "")
        
        
        self.itemNameLbl.text = (prodcutDeatis.images?.first?.altText?.html2AttributedString ?? prodcutDeatis.name?.html2AttributedString ) ?? ""
        
        prodcutDeatis.classifications?.first?.features?.forEach{ (prod) in
            if prod.name == "Country of origin isocode"{
                
                if let country = CountryManager.shared.country(withName: prod.featureValues?.first?.value ?? "")  {
                    self.flagImage.image = country.flag
                    
                    
                }else {
                    if let countryFromCode = CountryManager.shared.country(withCode: String((prod.featureValues?.first?.value ?? "").prefix(2)))  {
                        
                        self.flagImage.image = countryFromCode.flag
                        
                    }
                }
                
            }
        }
        if let country = CountryManager.shared.country(withName: prodcutDeatis.countryOfOrigin ?? "")  {
            self.flagImage.image = country.flag
            
            
        }else {
            
            if let countryFromCode = CountryManager.shared.country(withCode: String((prodcutDeatis.countryOfOriginIsocode ?? "").prefix(2)))  {
                
                self.flagImage.image = countryFromCode.flag
            }
        }
        /*   if  prodcutDeatis.classifications?.first?.features == nil{
         self.itemCurrencyLbl.text  =  "Origin /"
         self.flagImage.image = UIImage.init(named: "AEFlag")
         }*/
        self.itemCurrencyLbl.text = prodcutDeatis.countryOfOrigin
        if prodcutDeatis.baseOptions?.first?.selected?.variantOptionQualifiers?.first?.value ?? "" != ""{
            
            self.itemDetailsLbl.text =  prodcutDeatis.baseOptions?.first?.selected?.variantOptionQualifiers?.first?.value ?? ""
        }else{
            self.itemDetailsLbl.text = "Unit".localized()
        }
        self.itemDetailsLbl.text = prodcutDeatis.unitOfMeasure
        
        
        
        
        if prodcutDeatis.stock?.stockLevel ?? 0 <= 0{
            outOfStockLbl.isHidden = false
            cartBttn.isHidden = true
            itemQuantityView.isHidden = true
        }
        else {
            outOfStockLbl.isHidden = true
            cartBttn.isHidden = false
            itemQuantityView.isHidden = false
            
        }
        
        if prodcutDeatis.inWishlist == true {
            
            self.wishListButton.tintColor = UIColor(named: "AppMainColor")
            
        } else {
            
            self.wishListButton.tintColor =  #colorLiteral(red: 0.3686274886, green: 0.3686274886, blue: 0.3686274886, alpha: 1)
            
        }
        
    }
    
    
}


extension String {
    func strikeThrough() -> NSAttributedString {
        let string = "-" + (self) + "-" 
        let attributeString =  NSMutableAttributedString(string: string)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 3, range: NSMakeRange(0,attributeString.length))
        let attributes0: [NSAttributedString.Key : Any] = [
            .strikethroughColor: UIColor.orange ,
            .foregroundColor: UIColor.gray
            
        ]
        attributeString.addAttributes(attributes0, range: NSRange(location: 0, length: string.count  ))
        
        return attributeString
    }
    
}
