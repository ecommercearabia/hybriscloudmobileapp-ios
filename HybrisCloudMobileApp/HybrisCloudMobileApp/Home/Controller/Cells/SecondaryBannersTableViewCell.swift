//
//  SecondaryBannersTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/27/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import UIKit

class SecondaryBannersTableViewCell: UITableViewCell {

    @IBOutlet weak var firstBannerImage: UIImageView!
    @IBOutlet weak var SecondBannerImage: UIImageView!
    
    var bag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.bag = DisposeBag()
    }

    
}
