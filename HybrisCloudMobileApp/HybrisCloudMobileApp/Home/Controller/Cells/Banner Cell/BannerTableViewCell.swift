//
//  BannerTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/27/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import UIKit
import FSPagerView
import Domain
import NetworkPlatform
import Kingfisher

class BannerTableViewCell: UITableViewCell {
    var bag = DisposeBag()
    var viewModel:BannerViewModel!
    let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
    let component = PublishSubject<HybrisComponent>()
    var banners:[Component] = []
    var selectBannerSubject = PublishSubject<String>()
    let manualDidLoad = BehaviorSubject<Void>(value: ())
    @IBOutlet weak var pagerView: FSPagerView!{
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.itemSize = FSPagerView.automaticSize
            self.pagerView.delegate = self
            self.pagerView.dataSource = self
            self.pagerView.transformer = FSPagerViewTransformer(type: .linear)
            self.pagerView.interitemSpacing = 40
            pagerView.itemSize = CGSize(width: UIScreen.main.bounds.size.width , height: 350)
            pagerView.isInfinite = false
            pagerView.transformer = NoVerticalScalePagerViewTransformer(type: .linear)
            pagerView.automaticSlidingInterval = 3.0

        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()        
        
        
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.bag = DisposeBag()
    }
    
    
    @IBOutlet weak var pageControl: FSPageControl!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUp() {
        self.viewModel = BannerViewModel.init(createComponentUseCase: networkUseCaseProvider.makeComponentUseCase())
 
        
        let input = BannerViewModel.Input(trigger: self.manualDidLoad.asDriverOnErrorJustComplete(),components: component.asDriverOnErrorJustComplete())
        
        let output = viewModel.transform(input: input)
        output.banners.asObservable().subscribe(onNext: { (image) in
            print(image)
            self.banners = image
       self.pageControl.numberOfPages =  self.banners.count
            self.pageControl.setFillColor(#colorLiteral(red: 0.5215686275, green: 0.5098039216, blue: 0.2, alpha: 1) ,for: .selected)
            self.pageControl.setFillColor(#colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1), for: .normal)
            
                         self.pageControl.contentHorizontalAlignment = .center
                         self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.pagerView.reloadData()
        }).disposed(by: bag)
    }
}

extension BannerTableViewCell : FSPagerViewDataSource,FSPagerViewDelegate{
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return banners.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.contentMode = .scaleAspectFill
        
         cell.imageView?.kf.setImage(with: URL(string: "\(Environment.shared.apiEndpoint?.rawValue ?? "")\(self.banners[index].media?.mobile?.downloadURL ?? "")"))

        
        return cell
    }
    
    func jsonToString(json: AnyObject){
            do {
                let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
                let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
                print(convertedString) // <-- here is ur string

            } catch let myJSONError {
                print(myJSONError)
            }

        }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
 
        if let urlLink = banners[index].urlLink?.value as? String {
            selectBannerSubject.onNext(urlLink )
        }
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControl.currentPage = pagerView.currentIndex
    }
    
}
