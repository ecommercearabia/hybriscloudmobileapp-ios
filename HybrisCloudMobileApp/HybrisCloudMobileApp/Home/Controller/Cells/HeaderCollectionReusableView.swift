//
//  HeaderCollectionReusableView.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/27/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class HeaderCollectionReusableView: UICollectionReusableView {
    
    
    
    @IBOutlet weak var bestDealHederLbl: UILabel!
    
    @IBOutlet weak var featuredHederLbl: UILabel!

    @IBOutlet weak var BestSellerProductsLbl: UILabel!
    
}
