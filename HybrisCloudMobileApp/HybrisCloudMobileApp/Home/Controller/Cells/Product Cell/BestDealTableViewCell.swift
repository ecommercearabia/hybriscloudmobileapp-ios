//
//  BestDealTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import UIKit

class BestDealTableViewCell: UITableViewCell {
    var bag = DisposeBag()
  
    let component = PublishSubject<HybrisComponent>()
    var viewModel:BestDealProductViewModel = BestDealProductViewModel.init(createBestDealProductUseCase: NetworkPlatform.UseCaseProvider().makeBestDealProductUseCase())

    
    @IBOutlet weak var bestDealCollectionView: UICollectionView!
    @IBOutlet weak var sectionHeaderLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bestDealCollectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionViewCell")
        if L102Language.isRTL {
            bestDealCollectionView.semanticContentAttribute = .forceRightToLeft
        }
    }
    
    func cellSetUp() {

        
    
        let cellAwakeFromNib = rx.sentMessage(#selector(self.prepareForReuse))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = BestDealProductViewModel.Input(trigger: cellAwakeFromNib, components: component.asDriverOnErrorJustComplete())
        
        let output = viewModel.transform(input: input)
     
        output.products.asObservable().bind(to: self.bestDealCollectionView.rx.items(dataSource: self.viewModel.dataSource())).disposed(by: bag)
 
        output.products.asObservable().subscribe(onNext: { [unowned self](void) in
            print("error")
        }).disposed(by: bag)
 
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
 
    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }
 
     
}

