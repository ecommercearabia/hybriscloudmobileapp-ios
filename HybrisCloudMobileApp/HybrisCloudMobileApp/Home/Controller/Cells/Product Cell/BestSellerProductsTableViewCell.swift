//
//  BestSellerProductsTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/27/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import UIKit
import FSPagerView
import Domain
import Kingfisher

class BestSellerProductsTableViewCell: UITableViewCell {
    
    var bag = DisposeBag()

    let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
    let categorie = PublishSubject<HybrisComponent>()
    let selctCategorySubject = PublishSubject<BestSellerProducts>()
    var categorieComponent:[BestSellerProducts] = []
    var viewModel : BestSellerProductViewModel = BestSellerProductViewModel.init(createBestSellerProductUseCase: NetworkPlatform.UseCaseProvider().makebestSellerUserCase(), createBestDealProductUseCase: NetworkPlatform.UseCaseProvider().makeBestDealProductUseCase())

    
    @IBOutlet weak var productCollectionView: UICollectionView!
    @IBOutlet weak var sectionHeaderLabel: UILabel!
    @IBOutlet weak var pagerView: FSPagerView!{
        didSet {
            self.pagerView.register(UINib(nibName: "BestSellerProductsCell", bundle: nil), forCellWithReuseIdentifier: "BestSellerProductsCell")
            
            self.pagerView.itemSize = FSPagerView.automaticSize
            self.pagerView.delegate = self
            self.pagerView.dataSource = self
            self.pagerView.transformer = FSPagerViewTransformer(type: .linear)
//            self.pagerView.interitemSpacing = 2
//            self.pagerView.decelerationDistance = 2
            pagerView.itemSize = CGSize(width: 90 , height: 17)
            pagerView.isInfinite = true
            pagerView.transformer = NoVerticalScalePagerViewTransformer(type: .linear)
   
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
        self.productCollectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionViewCell")
                 
                 
                 let cellAwakeFromNib = rx.sentMessage(#selector(UITableViewCell.prepareForReuse))
                     .mapToVoid()
                     .asDriverOnErrorJustComplete()
                 
                 
                 let input = BestSellerProductViewModel.Input(trigger: cellAwakeFromNib,components: categorie.asDriverOnErrorJustComplete(),selectedCategory: selctCategorySubject.asDriverOnErrorJustComplete())
                 
                 
                 let output = viewModel.transform(input: input)
                 output.categories.asObservable().subscribe(onNext: { (title) in

                     self.categorieComponent = title
                     self.selctCategorySubject.onNext(self.categorieComponent[0])
                     self.pagerView.reloadData()
                 }) {
                     
                 }.disposed(by: bag)
                 output.bestSellerProducts.asObservable().bind(to: self.productCollectionView.rx.items(dataSource: self.viewModel.dataSource())).disposed(by: bag)
    }
   
      override func prepareForReuse() {
          super.prepareForReuse()
          self.bag = DisposeBag()
      }


}
extension BestSellerProductsTableViewCell : FSPagerViewDataSource,FSPagerViewDelegate{
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return categorieComponent.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        guard let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "BestSellerProductsCell", at: index) as? BestSellerProductsCell else{ return FSPagerViewCell()}
        cell.contentView.layer.shadowColor = UIColor.clear.cgColor
        cell.name.text = categorieComponent[index].title ?? ""
        
        return cell
    }
    
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        self.pagerView.scrollToItem(at: index, animated: true)
        self.selctCategorySubject.onNext(self.categorieComponent[index])

        
    }
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pagerView.scrollToItem(at: targetIndex, animated: true)

        self.selctCategorySubject.onNext(self.categorieComponent[targetIndex])
        
    }
    
    
}

