//
//  BestSellerProductsCell.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/6/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import FSPagerView
class BestSellerProductsCell: FSPagerViewCell {
 
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var borderView: UIView!

    
    func makeSelected() {
        borderView.layer.cornerRadius = 5
        borderView.layer.masksToBounds = true
        borderView.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.5843137255, blue: 0.2745098039, alpha: 1)
        borderView.layer.borderWidth = 1

    }
    
    func makeUnSelected() {
        borderView.layer.cornerRadius = 0
        borderView.layer.masksToBounds = false
        borderView.layer.borderColor = UIColor.clear.cgColor
        borderView.layer.borderWidth = 0

    }

}
