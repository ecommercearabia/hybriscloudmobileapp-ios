//
//  MainBannerTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/27/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class MainBannerTableViewCell: UITableViewCell {

    var bag = DisposeBag()
    var selectBannerSubject = PublishSubject<String>()
    var model : HybrisComponent? = nil
    var refreshTableHeight = PublishSubject<Void>()


    @IBOutlet weak var heightBannerImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerimage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
//    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
//    {
//        let tappedImage = tapGestureRecognizer.view as! UIImageView
//        selectBannerSubject.onNext(model?.urlLink ?? "")
//    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setup(_ model:HybrisComponent){
        self.model = model
        self.bannerimage.downloadImageInCellWithoutCorner(urlString: model.media?.mobile?.url ?? "")
        
        if( model.media?.mobile?.url != nil) {
            
            let url = URL(string: ("\(Environment.shared.apiEndpoint?.rawValue ?? "")\(model.media?.mobile?.url ?? "" )"))
            
            if( url != nil) {

                DispatchQueue.global().async { [weak self] in
                                                  
                         guard let imageData = try? Data(contentsOf: url!) else {
                             return
                         }
                    DispatchQueue.main.async {
                        let image = UIImage(data: imageData)
                        let ratio = ( image?.size.width ?? 3) / (image?.size.height ?? 1)
                        let newHeight = (self?.bannerimage.frame.width ?? self?.window?.frame.width ?? CGFloat(400)) / ratio
                        self?.heightBannerImageConstraint.constant = newHeight
                        self?.refreshTableHeight.onNext(())
                        
                    }
                    

                     }
            }else {
                heightBannerImageConstraint.constant = 100
            }
            
            
        }else {
            heightBannerImageConstraint.constant = 100
        }
        
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
//        bannerimage.isUserInteractionEnabled = true
//        bannerimage.addGestureRecognizer(tapGestureRecognizer)

        
    }

    override func prepareForReuse() {
        super.prepareForReuse()
//        bannerimage.gestureRecognizers?.removeAll()
        self.bag = DisposeBag()
    }
    

}

