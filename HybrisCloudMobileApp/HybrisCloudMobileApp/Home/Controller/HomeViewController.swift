//
//  HomeViewController.swift
//  HybrisCloudMobileApp
//
//  Created by Ahmad Bader on 7/14/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import Domain
import RxSwift
import RxCocoa
import NetworkPlatform
import FSPagerView
import KeychainSwift
import Firebase
class HomeViewController: UIViewController {
    
    
    @IBOutlet weak var notoficationBut: UIBarButtonItem!
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var homeNavigationBar: UINavigationItem!
    let bag = DisposeBag()
    var viewModel:HomeViewModel!
    var productDetailsSubject = PublishSubject<String>()
    let firebaseTokenSubject = PublishSubject<String>()
    
    override func viewWillAppear(_ animated: Bool) {
        if (KeychainSwift().get("user") == "current") {
            
            Messaging.messaging().token { token, error in
              if let error = error {
                print("Error fetching FCM registration token: \(error)")
              } else if let token = token {
                print("FCM registration token: \(token)")

                self.firebaseTokenSubject.onNext( token)
              }
            }
            
            
          
            
            
        

        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backButtonTitle = "Back".localized()
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 0.3686274886, green: 0.3686274886, blue: 0.3686274886, alpha: 1)
        addNavBarImage()
         let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        guard let nvc = self.navigationController , let sb =  self.storyboard else{
            return
        }
        
//        notoficationBut.rx.tap.subscribe(onNext: { (void) in
//            KeychainSwift().delete("guid")
//        }).disposed(by: bag)
        
        let networkNavigator = DefaultHomeNavigator(services: networkUseCaseProvider,    navigationController: nvc,storyBoard: sb)
        
        viewModel = HomeViewModel(useCase: networkUseCaseProvider.makePagesUseCase(), navigator: networkNavigator, userUseCase: networkUseCaseProvider.makeUserUseCase())
        configureTableView()
        
        viewModel.refreshTableHeight.subscribe(onNext: { () in
            self.view.layoutIfNeeded()
        }).disposed(by: bag)
        
        self.homeTableView.rx.modelSelected(HybrisComponent.self).subscribe(onNext: { (model) in
            if model.typeCode == "SimpleResponsiveBannerComponent" {
            self.viewModel.selectBannerSubject.onNext(model.urlLink ?? "")
            }
        }).disposed(by: bag)
        
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let pull = homeTableView.refreshControl!.rx
            .controlEvent(.valueChanged)
            .asDriver()
        let input = HomeViewModel.Input(trigger: Driver.merge(viewWillAppear, pull), selection: homeTableView.rx.itemSelected.asDriver(), setFirebaseToken: firebaseTokenSubject.asDriverOnErrorJustComplete())
        
        let output = viewModel.transform(input: input)
        //Bind Posts to UITableView
        
       
        output.page.mapToVoid().asObservable().subscribe(onNext: { () in
            self.homeTableView.refreshControl?.endRefreshing()
        }, onError: { (error) in
            
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: bag)
        output.page.map({ (page) -> [HybrisHomeSectionModel] in
            
          
            let contentSlot = page.contentSlots?.contentSlot?.sorted(by: {($0.slotID ?? "") < ($1.slotID ?? "")})
            return (contentSlot?.filter({ (slot) -> Bool in
             
             return   !(slot.name?.lowercased().contains("header") ?? false || slot.name?.lowercased().contains("footer") ?? false || slot.name?.lowercased().contains("logo") ?? false || slot.components?.component?.filter({ (prod) -> Bool in
                   return prod.typeCode == "ResponsiveRotatingImagesComponent" || prod.typeCode == "ProductCarouselComponent" || (prod.typeCode == "SimpleResponsiveBannerComponent"  && prod.media?.mobile != nil)
             }).count ?? 0 == 0)

                
            }).map({ (slot) -> HybrisHomeSectionModel in
                return HybrisHomeSectionModel.init(model: slot, items: slot.components?.component ?? [])
            }) ?? [])
            
            
        }).asObservable().bind(to: homeTableView.rx.items(dataSource: self.viewModel.dataSource())).disposed(by: bag)
            
//            .drive(homeTableView.rx.items(dataSource: viewModel.dataSource())).disposed(by: bag)
        
        self.viewModel.toLoginSubject.subscribe(onNext: { [unowned self](bool) in
            
            self.performSegue(withIdentifier: "toLogin", sender: nil)
            
            
        }).disposed(by: bag)
        
        
        
        self.viewModel.productDetailsSubject.subscribe(onNext: { [unowned self](item) in
            
            Analytics.logEvent(AnalyticsEventSelectItem, parameters: [
                AnalyticsParameterItemListID: item.code ?? "" ,
                AnalyticsParameterItemListName: item.name ?? ""
              ])
            
            self.performSegue(withIdentifier: "goToProductDetails", sender: item)
        }).disposed(by: bag)
        
        output.fetching
            .drive(homeTableView.refreshControl!.rx.isRefreshing)
            .disposed(by: bag)
        
        viewModel.selectBannerSubject.subscribe(onNext: { (id) in
            self.navigateToScreen(urlLink: id)
        }).disposed(by: bag)
        
        viewModel.selectBannersSubject.subscribe(onNext: { (id) in
            self.navigateToScreen(urlLink: id)
        }).disposed(by: bag)
        
        
        output.succefullySetFirebaseToken.asObservable().subscribe(onNext: {
            (void) in
            print("")
        }).disposed(by: bag)
        
    }
    
    func addNavBarImage () {
        let navController = navigationController!
        let logoImage = #imageLiteral(resourceName: "logo")
        let logoImageView = UIImageView()
        
        let bannerWidth = navController.navigationBar.frame.size.width / 2
        let bannerHeight = navController.navigationBar.frame.size.height / 2
        let bannerX = bannerWidth / 2 - logoImage.size.width / 2
        let bannerY = bannerHeight / 2 - logoImage.size.height / 2
        
        logoImageView.frame = CGRect(x:bannerX , y: bannerY, width: bannerWidth, height: bannerHeight)
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.image = logoImage
        navigationItem.titleView = logoImageView
        
    }
    
    func navigateToScreen(urlLink: String){
        if (urlLink != "" && urlLink != "{}" && urlLink != "null"){
            switch String(urlLink.characterAtIndex(index:(( urlLink.lastIndex(of: "/")?.encodedOffset ?? 1) - 1)) ?? "a") {
            case "p" :
                //Details
                let code = urlLink.subStringAfterLast("p/")
                let product = ProductDetails.init(availableForPickup: nil, baseOptions: nil, baseProduct: nil, categories: nil, classifications: nil, code: code, configurable: nil, countryOfOrigin: nil, countryOfOriginIsocode: nil, ProductDetailsDescription: nil, images: nil, name: nil, numberOfReviews: nil, potentialPromotions: nil, price: nil, priceRange: nil, productReferences: nil, purchasable: nil, reviews: nil, stock: nil, summary: nil, unitOfMeasure: nil, unitOfMeasureDescription: nil, url: nil, averageRating: nil, discount: nil, vegan: nil, organic: nil, glutenFree: nil, inWishlist: nil , dry: nil , express: nil, frozen: nil , chilled: nil, nutritionFacts: nil )
                self.performSegue(withIdentifier: "goToProductDetails", sender: product)
                break
            case "c" :
                //list
                self.performSegue(withIdentifier: "goToProductListing", sender: urlLink.subStringAfterLast("c/"))
                break
            default: break

            }
        }
          
    }
    private func configureTableView() {
        homeTableView.refreshControl = UIRefreshControl()
        homeTableView.estimatedRowHeight = 64
        homeTableView.rowHeight = UITableView.automaticDimension
    }
    
 
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToProductDetails"{
            if let vc = segue.destination as? ProductDetailsViewController{
                guard let pd = sender as? ProductDetails else {
                    return
                }
                
                vc.productDetailsBehaviorSubject = BehaviorSubject<String>(value: pd.code ?? "")
            }} else if segue.identifier == "goToProductListing" {
                if let vc = segue.destination as? ProductListingViewController {
                    vc.categoryIdBehaviorSubject = BehaviorSubject<String>(value: sender as? String ?? "" )
                    vc.fromBunners = true
                }
                
        }
    }
    
    
}

extension String {
    func characterAtIndex(index: Int) -> Character? {
        var cur = 0
        for char in self {
            if cur == index {
                return char
            }
            cur += 1
        }
        return nil
    }
    

    func subStringAfterLast (_ text: String) -> String {
           guard let subrange = self.range(of: text, options: [.regularExpression, .backwards]) else { return self }
           return String(self[subrange.upperBound...])
       }
}


