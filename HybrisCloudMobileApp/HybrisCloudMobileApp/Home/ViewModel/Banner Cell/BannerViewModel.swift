//
//  BannerViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/29/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import NetworkPlatform

class BannerViewModel: ViewModelType{
    
    
    struct Input {
        let trigger: Driver<Void>
        let components: Driver<HybrisComponent>
        
    }
    struct Output {
        let fetching: Driver<Bool>
        let banners: Driver<[Component]>
        let error: Driver<Error>
        
    }
    
     let networkUseCaseProvider : Domain.UseCaseProvider = NetworkPlatform.UseCaseProvider()
    
    private let createComponentUseCase: ComponentUseCase
    
    init(createComponentUseCase: ComponentUseCase ) {
        self.createComponentUseCase = createComponentUseCase
     }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let componentResult = input.components.flatMap { (component) -> Driver<[Component]> in
            
            let banners = component.banners?.components(separatedBy: ["\n", " ", "\r"]) ?? []
            
            let bannersMap =  banners.map { (element) ->  Driver<Component> in
                self.createComponentUseCase.component(componentId: element)
                   // .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
                
            }
            let combine =  Driver<Component>.combineLatest(bannersMap)
            return combine.asDriver()
        }
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching,
                      banners: componentResult,
                      error: errors)
    }
    
}
