//
//  HomaLayout.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

enum HomeLayoutCell:IdentifiableType,Equatable{
    static func == (lhs: HomeLayoutCell, rhs: HomeLayoutCell) -> Bool {
        return false
    }
    
    var identity: String {
        switch(self){
        
        case .firstSection:
            return "bannersCell"
        case .secondSection:
               return "bannerCell"
        case .carouselTabs:
            return "carouselTabsCell"
        case .categories:
            return "categories"
        case .carouselProducts(let component):
            return  component.typeCode ?? ""
        }
        
    }
    case firstSection(_:HybrisComponent)
    case secondSection(_:HybrisComponent)
    case carouselTabs(_:HybrisComponent)
    case carouselProducts(_:HybrisComponent)

    case categories
}
