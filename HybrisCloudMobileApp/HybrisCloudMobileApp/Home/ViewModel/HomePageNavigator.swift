//
//  HomeNavigator.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import Domain


protocol HomePageNavigator {
    func toCreatePost()
    func toPost(_ post: Page)
    func toPosts()
}

class DefaultHomeNavigator: HomePageNavigator {
    private let storyBoard: UIStoryboard
      private let navigationController: UINavigationController
    private let services : Domain.UseCaseProvider


    init(services: Domain.UseCaseProvider,
         navigationController: UINavigationController,
         storyBoard: UIStoryboard) {
        self.services = services
        self.navigationController = navigationController
        self.storyBoard = storyBoard
    }
    
    func toPosts() {
           
        
        
    }

    func toCreatePost() {
//        let navigator = DefaultCreateHomeNavigator(navigationController: navigationController)
//        let viewModel = CreateHomeViewModel(createPostUseCase: services.makeHomeUseCase(),
//                navigator: navigator)
//        let vc = storyBoard.instantiateViewController(ofType: CreatePostViewController.self)
//        vc.viewModel = viewModel
//        let nc = UINavigationController(rootViewController: vc)
//        navigationController.present(nc, animated: true, completion: nil)
    }

    func toPost(_ post: Page) {
//        let navigator = DefaultEditPostNavigator(navigationController: navigationController)
//        let viewModel = EditPostViewModel(post: post, useCase: services.makeHomeUseCase(), navigator: navigator)
//        let vc = storyBoard.instantiateViewController(ofType: EditPostViewController.self)
//        vc.viewModel = viewModel
//        navigationController.pushViewController(vc, animated: true)
    }
}
