//
//  ProductCollectionViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import NetworkPlatform
import KeychainSwift
class ProductCollectionViewModel: ViewModelType{
   let toLoginSubject = PublishSubject<Void>()
    private let addtoCartUseCase: AddtoCartUseCase
     let useCase: WishListUseCase
    let cartUseCase: CartUseCase

    init(useCase: WishListUseCase,addtoCartUseCase: AddtoCartUseCase , cartUseCase : CartUseCase) {
        self.useCase = useCase
        self.addtoCartUseCase = addtoCartUseCase
        self.cartUseCase = cartUseCase
    }
    
    
    struct Input {
        let addtoCartTrigger: Driver<Void>
        let products: Driver<ProductDetails>
        let quantity : Driver<String>
        let increaseQuantityTrigger: Driver<Void>
        let decreaseQuantityTrigger: Driver<Void>
        let createCartTrigger: Driver<Void>
        let addToWishListTrigger: Driver<Void>



    }
    
    struct Output {
        let fetching: Driver<Bool>
        let error: Driver<Error>
        let addToCartResult: Driver<AddedToCartModel>
        let increaseQuantityOutput : Driver<String>
        let decreseQuantityOutput : Driver<String>
        let createdCart : Driver<CartModel>
        let addToWishListResult : Driver<Void>
        let errorAddToWishList: Driver<Error>


    }

    
    func transform(input: Input) -> Output {
        
        let activityIndicator = ActivityIndicator()
        
        let errorTracker = ErrorTracker()
        let errorAddToWishListTracker = ErrorTracker()

        let inputAddtoCart = Driver.combineLatest(
            input.products,
            input.quantity
        )
        
        let addToWishList = input.addToWishListTrigger.withLatestFrom(input.products).map { (wishListIteam)  -> AddDeleteWishListItemRequest in
           
                 let addToWishListModel = AddDeleteWishListItemRequest(wishlistPK: KeychainSwift().get("WishListPK"), productCode: wishListIteam.code ?? "")
                 return addToWishListModel


             }.flatMapLatest{ [unowned self] in
                 return self.useCase.addWishListIteam(iteamData: $0)
                                              .trackActivity(activityIndicator)
                                              .trackError(errorAddToWishListTracker)
                                              .asDriverOnErrorJustComplete()
             }
        
        let addToCartResult = input.addtoCartTrigger.withLatestFrom(inputAddtoCart).map { (arg0)  in
            
            let (product, quantityNumber) = arg0
            

            let addToCartProductModel = AddToCartProductModel(basePrice: nil, cancellableQuantity: nil, cancelledItemsPrice: nil, configurationInfos: nil, deliveryMode: nil, deliveryPointOfService: nil, entryNumber: nil, product: product , quantity: Int(quantityNumber), quantityAllocated: nil, quantityCancelled: nil, quantityPending: nil, quantityReturned: nil, quantityShipped: nil, quantityUnallocated: nil, returnableQuantity: nil, returnedItemsPrice: nil, totalPrice: nil, updateable: nil, url: nil)
            
            return addToCartProductModel
            
        } .flatMapLatest { [unowned self] in

            return self.addtoCartUseCase.addToCart(userId: KeychainSwift().get("user") ?? "anonymous", cartId:  (KeychainSwift().get("user") ?? "anonymous" == "current") ? "current" : KeychainSwift().get("guid") ?? "", entry: $0)
                          .trackActivity(activityIndicator)
                          .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
                

        let  decreaseQuantityOutput :Driver<String> = input.decreaseQuantityTrigger.withLatestFrom(input.quantity).map { (quantity)  in
                            
                            return quantity
                        }.flatMapLatest { [] in
                            if (Int($0) ?? 0 > 1)
                            {
                           return Driver.just(String((Int($0) ?? 0) - 1))
                            }else {
                             return  Driver.just($0)
                            }
                        }
                 
      let  increaseQuantityOutput :Driver<String> =
            input.increaseQuantityTrigger.withLatestFrom(inputAddtoCart)
                .map { (arg0) -> ModelForIncreaseCondition  in
                    
                let (product, String) = arg0
                    print(product.stock?.stockLevel ?? 0.0)
                return ModelForIncreaseCondition(maxValue: product.stock?.stockLevel, quantityValue: String)
                
            }.flatMapLatest {  [] in

                if ( (Int($0.maxValue ?? 1)) > (Int($0.quantityValue ?? "1") ?? 1 ))
                {
                    return    Driver.just(String((Int($0.quantityValue ?? "0") ?? 0) + 1))
                    }else {
                     return  Driver.just($0.quantityValue ?? "1")
                }
        }
        
        
        let createCart = input.createCartTrigger.flatMapLatest { [unowned self] in
            return self.cartUseCase.creatCart(userId: KeychainSwift().get("user") ?? "anonymous")
                .trackActivity(activityIndicator)
                           .trackError(errorTracker)
                           .asDriverOnErrorJustComplete()

        }
           
        let fetching = activityIndicator.asDriver()
        
        let errors = errorTracker.asDriver()
        let errorAddToWish = errorAddToWishListTracker.asDriver()

        return Output(fetching : fetching ,
                      error : errors ,
                      addToCartResult : addToCartResult ,
                      increaseQuantityOutput : decreaseQuantityOutput,
                      decreseQuantityOutput : increaseQuantityOutput, createdCart: createCart, addToWishListResult: addToWishList, errorAddToWishList: errorAddToWish
        )
         


}}


class ModelForIncreaseCondition {
    
  public var maxValue : Int?
  public var quantityValue : String?
    
    init(maxValue : Int? , quantityValue : String?) {
        self.maxValue = maxValue
        self.quantityValue = quantityValue

    }
}

class ModelForOtputQuantity {
    
    public var increseValue : String?
    public var quantityValue : String?
    public var decreseValue : String?

    init(increseValue : String? , quantityValue : String? , decreseValue : String?) {
        self.increseValue = increseValue
        self.quantityValue = quantityValue
        self.decreseValue = decreseValue

    }
}
