//
//  CreateHomeNavigator.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import UIKit
import Domain

protocol CreateHomeNavigator {

    func toPosts()
}

final class DefaultCreateHomeNavigator: CreateHomeNavigator {
    private let navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func toPosts() {
        navigationController.dismiss(animated: true)
    }
}
