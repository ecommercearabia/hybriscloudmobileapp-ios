//
//  BestDealProductViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import Domain
import NetworkPlatform
import KeychainSwift

class BestDealProductViewModel: ViewModelType{
    
       let toLoginSubject = PublishSubject<Void>()

    struct Input {
        let trigger: Driver<Void>
        let components: Driver<HybrisComponent>
        
    }
    struct Output {
        let fetching: Driver<Bool>
        let products: Driver<[BestDealProductSection]>
        let error: Driver<Error>
        
    }
    
    //let bag:DisposeBag
    let networkUseCaseProvider : Domain.UseCaseProvider = NetworkPlatform.UseCaseProvider()
    
    private let createBestDealProductUseCase: BestDealProductUseCase
    
    init(createBestDealProductUseCase: BestDealProductUseCase ) {
        self.createBestDealProductUseCase = createBestDealProductUseCase
        
       
    }
    
    func transform(input: Input) -> Output {
        
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let productResult = input.components.flatMap { (product) -> Driver<[BestDealProductSection]> in
              let products = product.productCodes?.components(separatedBy: ["\n", " ", "\r"]) ?? []
          
            
            let productMap =  products.uniques.map { (element) ->  Driver<ProductDetails> in
                self.createBestDealProductUseCase.bestDealProduct(productCode: element)
                   // .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .asDriver(onErrorRecover: { (error) -> Driver<ProductDetails> in
                        Driver<ProductDetails>.just(ProductDetails(availableForPickup: nil, baseOptions: nil, baseProduct: nil, categories: nil, classifications: nil, code: nil, configurable: nil, countryOfOrigin: nil, countryOfOriginIsocode: nil, ProductDetailsDescription: nil, images: nil, name: nil, numberOfReviews: nil, potentialPromotions: nil, price: nil, priceRange: nil, productReferences: nil, purchasable: nil, reviews: nil, stock: nil, summary: nil, unitOfMeasure: nil, unitOfMeasureDescription: nil, url: nil, averageRating: nil, discount: nil , vegan : nil , organic : nil, glutenFree : nil, inWishlist: nil , dry: nil , express: nil, frozen: nil , chilled: nil, nutritionFacts: nil))
                    })
                    
                
            }
            let combine =  Driver<ProductDetails>.combineLatest(productMap)
            return combine.map { (products) -> [BestDealProductSection] in
                return [BestDealProductSection(header: product.title ?? "", items: products.filter({ (pd) -> Bool in
                    pd.code != nil
                }))]
            }
        }
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching,
                      products: productResult,
                      error: errors)
    }
    
    func dataSource() ->
        RxCollectionViewSectionedAnimatedDataSource<BestDealProductSection> {
            return RxCollectionViewSectionedAnimatedDataSource<BestDealProductSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .bottom),configureCell: { ds, cv, indexPath, item in
                
                
                guard let cell = cv.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as? ProductCollectionViewCell else {
                    return UICollectionViewCell()
                }
                    cell.setUp(prodcutDeatis: item)
       
                cell.wishListButton.rx.tap.subscribe(onNext: { [unowned self](void) in
                        if KeychainSwift().get("user") == "anonymous" {
                             
                            self.toLoginSubject.onNext(())
                    }
                }).disposed(by: cell.disposeBag)
                
                return cell
                
                })
    }
}

extension Array where Element: Hashable {
    var uniques: Array {
        var buffer = Array()
        var added = Set<Element>()
        for elem in self {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
}
