//
//  BestSellerProductViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/6/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import Domain
import NetworkPlatform
import KeychainSwift

class BestSellerProductViewModel: ViewModelType{
    let toLoginSubject = PublishSubject<Void>()

    struct Input {
        let trigger: Driver<Void>
        let components: Driver<HybrisComponent>
        let selectedCategory:Driver<BestSellerProducts>
        
    }
    struct Output {
        let fetching: Driver<Bool>
        let categories: Driver<[BestSellerProducts]>
        let bestSellerProducts: Driver<[BestDealProductSection]>
        let error: Driver<Error>
        
    }
    
    let networkUseCaseProvider : Domain.UseCaseProvider = NetworkPlatform.UseCaseProvider()
    
    private let createBestSellerProductUseCase: BestSellerProductsUseCase
    private let createBestDealProductUseCase: BestDealProductUseCase

    init(createBestSellerProductUseCase: BestSellerProductsUseCase, createBestDealProductUseCase: BestDealProductUseCase) {
        self.createBestSellerProductUseCase = createBestSellerProductUseCase
        self.createBestDealProductUseCase = createBestDealProductUseCase

    }
    
    func transform(input: Input) -> Output {
        
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let componentResult = input.components.flatMap { (component) -> Driver<[BestSellerProducts]> in
            
            let banners = component.productCarouselComponents?.components(separatedBy: ["\n", " ", "\r"]) ?? []
            
            let bannersMap =  banners.map { (element) ->  Driver<BestSellerProducts> in
                self.createBestSellerProductUseCase.bestSellerProduct(productCode: element)
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
                
            }
            let combine =  Driver<BestSellerProducts>.combineLatest(bannersMap)
            return combine.asDriver()
        }
        
        
     
        let productResult = input.selectedCategory.flatMap { (product) -> Driver<[BestDealProductSection]> in
                   let products = product.productCodes?.components(separatedBy: ["\n", " ", "\r"]) ?? []
               
                 
                 let productMap =  products.map { (element) ->  Driver<ProductDetails> in
                     self.createBestDealProductUseCase.bestDealProduct(productCode: element)
                         .trackActivity(activityIndicator)
                         .trackError(errorTracker)
                         .asDriverOnErrorJustComplete()
                     
                 }
                 let combine =  Driver<ProductDetails>.combineLatest(productMap)
                 return combine.asDriver().map { (products) -> [BestDealProductSection] in
                     return [BestDealProductSection(header: product.title ?? "", items: products)]
                 }
             }
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching,
                      categories: componentResult,
                      bestSellerProducts: productResult,
                      error: errors)
    }
    func dataSource() ->
        RxCollectionViewSectionedAnimatedDataSource<BestDealProductSection> {
            return RxCollectionViewSectionedAnimatedDataSource<BestDealProductSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .bottom),configureCell: { ds, cv, indexPath, item in
                
                
                guard let cell = cv.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as? ProductCollectionViewCell else {
                    return UICollectionViewCell()
                }
                    cell.setUp(prodcutDeatis: item)
                
                         cell.wishListButton.rx.tap.subscribe(onNext: { (void) in
                                 if KeychainSwift().get("user") == "anonymous" {
                                      
                                     self.toLoginSubject.onNext(())
                             }
                         }).disposed(by: cell.disposeBag)
              
                return cell
                
            })
    }
}


