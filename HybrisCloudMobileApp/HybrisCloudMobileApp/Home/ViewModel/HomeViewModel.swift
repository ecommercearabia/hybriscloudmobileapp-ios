//
//  HomeViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Ahmad Bader on 7/14/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Domain
import NetworkPlatform

typealias HybrisHomeSectionModel = AnimatableSectionModel<HybrisContentSlot,HybrisComponent>



class HomeViewModel: ViewModelType{
    
    let toLoginSubject = PublishSubject<Void>()
    var productDetailsSubject = PublishSubject<ProductDetails>()
    var selectBannerSubject = PublishSubject<String>()
    var selectBannersSubject = PublishSubject<String>()
    var refreshTableHeight = PublishSubject<Void>()
    
    struct Input {
        let trigger: Driver<Void>
        let selection: Driver<IndexPath>
        let setFirebaseToken : Driver<String>
    }
    struct Output {
        let fetching: Driver<Bool>
        let page: Driver<Page>
        let error: Driver<Error>
        let succefullySetFirebaseToken : Driver<Void>
    }
    
    
    let bag = DisposeBag()
    let networkUseCaseProvider : Domain.UseCaseProvider = NetworkPlatform.UseCaseProvider()
    var bannersArray: [String]?
    private let useCase: PagesUseCase
    
    private let navigator: HomePageNavigator
    private let userUseCase: UserUseCase
    init(useCase: PagesUseCase,navigator: HomePageNavigator , userUseCase: UserUseCase) {
        self.useCase = useCase
        self.navigator = navigator
        self.userUseCase = userUseCase
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        let page = input.trigger.flatMapLatest {
            return self.useCase.page(pageId: "homepage")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        
        let setFirebaseToken = input.setFirebaseToken.flatMapLatest{ [unowned self] in
            return self.userUseCase.setFirebaseToken(mobileToken: $0) .trackError(errorTracker)
                .trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
        }
        
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching,
                      page: page,
                      error: errors, succefullySetFirebaseToken: setFirebaseToken)
    }
    
    
    func dataSource() -> RxTableViewSectionedAnimatedDataSource<HybrisHomeSectionModel>{
        return RxTableViewSectionedAnimatedDataSource<HybrisHomeSectionModel>(configureCell: { ds, tv, ip, model in
            
            
            if model.typeCode == "ResponsiveRotatingImagesComponent" {
                guard let cell = tv.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: ip) as? BannerTableViewCell else {return UITableViewCell()}
                cell.setUp()

                cell.selectBannerSubject.subscribe(onNext: { (urlLink) in
                    self.selectBannerSubject.onNext(urlLink)
                }).disposed(by: cell.bag)
                cell.component.onNext(model)

//                cell.selectBannerSubject.bind(to: self.selectBannersSubject).disposed(by: cell.bag)
                return cell
                
            }else if model.typeCode == "ProductCarouselComponent" {
                
                guard let cell = tv.dequeueReusableCell(withIdentifier: "BestDealTableViewCell", for: ip) as? BestDealTableViewCell else {return UITableViewCell()}
                
//                cell.bestDealCollectionView.delegate = nil
//                cell.bestDealCollectionView.dataSource = nil
                
                cell.cellSetUp()
                
                cell.viewModel.toLoginSubject.bind(to: self.toLoginSubject).disposed(by: cell.bag)
                cell.sectionHeaderLbl.text = model.title ?? ""
                cell.component.onNext(model)
                
                cell.bestDealCollectionView.rx.modelSelected(BestDealProductSection.Item.self).subscribe(onNext: {[unowned self](item) in print("model Selected")
                    self.productDetailsSubject.onNext(item)
                    
                    
                }).disposed(by: cell.bag)
                
                
                
                return cell
                
            }else if model.typeCode == "TabbedProductCarouselComponent"{
                guard let cell = tv.dequeueReusableCell(withIdentifier: "BestSellerProductsTableViewCell", for: ip) as? BestSellerProductsTableViewCell else {return UITableViewCell()}
                
//                cell.productCollectionView.delegate = nil
//                cell.productCollectionView.dataSource = nil
                
                cell.categorie.onNext(model)
                cell.viewModel.toLoginSubject.bind(to: self.toLoginSubject).disposed(by: cell.bag)
                
                cell.sectionHeaderLabel.text = model.title ?? ""
                cell.productCollectionView.rx.modelSelected(BestDealProductSection.Item.self).subscribe(onNext: {(item) in print("model Selected")
                    self.productDetailsSubject.onNext(item)
                    
                }).disposed(by: self.bag)
                
                return cell
                
            }else{
                guard let cell = tv.dequeueReusableCell(withIdentifier: "MainBannerTableViewCell", for: ip) as? MainBannerTableViewCell else {return UITableViewCell()}
                cell.backgroundColor = .red
                cell.setup(model)
                cell.refreshTableHeight.bind(to : self.refreshTableHeight).disposed(by: cell.bag)
//                cell.selectBannerSubject.bind(to: self.selectBannerSubject).disposed(by: cell.bag)
                return cell
                
            }
            
            //        }
            //            , titleForHeaderInSection: { (ds, sectionIndex) -> String? in
            //            return "header\(sectionIndex)"
            //        }
            //, titleForFooterInSection: { (ds, sectionIndex) -> String? in
            //            return "footer\(sectionIndex)"
            //        }, canEditRowAtIndexPath: { (ds, ip) -> Bool in
            //            return true
            //        }, canMoveRowAtIndexPath: { (ds, ip) -> Bool in
            //            return true
            //        }, sectionIndexTitles: { (ds) -> [String]? in
            //            return []
        }, sectionForSectionIndexTitle:  { (ds, title, sectionIndex) -> Int in
            return sectionIndex
        })
    }
    
    
}



