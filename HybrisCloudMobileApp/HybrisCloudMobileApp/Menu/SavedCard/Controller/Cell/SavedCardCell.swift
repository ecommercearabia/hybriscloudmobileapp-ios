//
//  SavedCardCell.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 26/12/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import SwipeCellKit

class SavedCardCell: SwipeTableViewCell {

    @IBOutlet weak var CardName: UILabel!
    @IBOutlet weak var CardType: UILabel!
    @IBOutlet weak var CardId: UILabel!
    @IBOutlet weak var Email: UILabel!
    @IBOutlet weak var PayoutType: UILabel!
    @IBOutlet weak var CardLabel: UILabel!
    @IBOutlet weak var CardNo: UILabel!
    
    func setUp(savedCard : CustomerPaymentOptions) {
        
        self.CardName.text = savedCard.customerCardName ?? ""
        self.CardType.text = savedCard.customerCardType ?? ""
        self.CardId.text = savedCard.customerCardId ?? ""
        self.Email.text = savedCard.customerEmail ?? ""
        self.CardLabel.text = savedCard.customerCardLabel ?? ""
        self.CardNo.text = savedCard.customerCardNo ?? ""
        
        switch savedCard.customerPayoptType {
        case "OPTCRDC":
            self.PayoutType.text = "Credit Card".localized()
        case "OPTDBCRD":
            self.PayoutType.text = "Debit Card".localized()
        default:
            self.PayoutType.text = "N/A".localized()
        }


    }

    
}
