//
//  SavedCardsViewController.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 26/12/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import UIKit
import Domain

class SavedCardsViewController: UIViewController {
    
    let bag = DisposeBag()
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView.init(frame: CGRect.zero)
            tableView.rx.setDelegate(self).disposed(by: self.bag)
        }
    }
    

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "Mange Saved Cards".localized()

        let viewModel:SavedCardsViewModel = SavedCardsViewModel(useCase: NetworkPlatform.UseCaseProvider().makeSavedCards())
         
        tableView.refreshControl = UIRefreshControl()
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
              .mapToVoid()
              .asDriverOnErrorJustComplete()
        
        
        let pull = tableView.refreshControl!.rx
              .controlEvent(.valueChanged)
              .asDriver()

        let input = SavedCardsViewModel.Input(trigger: Driver.merge(viewWillAppear, pull))

        let output = viewModel.transform(input: input)
       
        output.refreshSavedCards.asObservable().subscribe(onNext: { (_) in
                    self.viewWillAppear(true)
        }).disposed(by: bag)
        
        output.fetching
                  .drive(tableView.refreshControl!.rx.isRefreshing)
                  .disposed(by: bag)
        
        output.error.asObservable().subscribe(onNext: { (error) in
       
            let errorInModel = error as? ErrorResponse
            
            let  msgError =  errorInModel?.errors?.map({ (errorResponseElement) -> String in
                          return errorResponseElement.message ?? ""
                      }).joined(separator: " , ")
                      
            self.errorLoaf(message: msgError ?? "" )
            
            }).disposed(by: bag)
        
        
        output.savedCards.map { (savedCards) -> [SavedCardsSection] in
            
            return [SavedCardsSection(header: "", items: savedCards.customerPaymentOptions  ?? [])]
            }.asObservable().bind(to: tableView.rx.items(dataSource: viewModel.dataSource())).disposed(by: bag)
        

    }
    
}

extension SavedCardsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
           cell.alpha = 0
           cell.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
           UIView.animate(
               withDuration: 0.5,
               delay: 0.01 * Double(indexPath.row),
               animations: {
                   cell.alpha = 1
                   cell.transform = CGAffineTransform.identity

           })

    }
 
}
