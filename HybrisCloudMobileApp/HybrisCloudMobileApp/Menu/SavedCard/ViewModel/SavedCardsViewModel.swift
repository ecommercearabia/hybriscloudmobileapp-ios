//
//  SavedCardsViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 26/12/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import SwipeCellKit
import Domain


class SavedCardsViewModel : ViewModelType{
    
    var deletePublishSubject = PublishSubject<CustomerPaymentOptions>()
    
    struct Input {
        var trigger: Driver<Void>
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let error: Driver<Error>
        let savedCards: Driver<SavedCardsModel>
        let refreshSavedCards:Driver<Void>
        
    }
   
    private let savedCardsUseCase: SavedCardsUseCase

    init(useCase: SavedCardsUseCase) {
       self.savedCardsUseCase = useCase
        
    }
      
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let savedCards = input.trigger.flatMapLatest { [unowned self] in
            return self.savedCardsUseCase.fetchSavedCards(userId: "current")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        

        
       let deleteSavedCards = deletePublishSubject.asDriverOnErrorJustComplete().map { (deleteSavedCard)  -> String in
            
           return deleteSavedCard.identity ?? ""

        }.flatMapLatest{ [unowned self] in
            return self.savedCardsUseCase.deleteSavedCards(userId: "current", savedCardId: $0)
                                         .trackActivity(activityIndicator)
                                         .trackError(errorTracker)
                                         .asDriverOnErrorJustComplete()

        }
        
        
        let refreshSavedCards = Driver<Void>.merge(deleteSavedCards)
        
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        
        return Output(
            fetching: fetching,
            error: errors,
            savedCards: savedCards,
            refreshSavedCards: refreshSavedCards
        )
    }

    func dataSource() -> RxTableViewSectionedAnimatedDataSource<SavedCardsSection>
          {
              return RxTableViewSectionedAnimatedDataSource<SavedCardsSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .bottom),configureCell: { ds, cv, indexPath, item in
                        
                  guard let cell = cv.dequeueReusableCell(withIdentifier: "SavedCardCell", for: indexPath) as? SavedCardCell else { return UITableViewCell()}
                  
                  cell.setUp(savedCard: item)
                  cell.delegate = self

                  return cell

                    })
                }

   
}

extension SavedCardsViewModel: SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]?
    {
      
        guard let cardSelected = try? tableView.rx.model(at: indexPath) as CustomerPaymentOptions else {
                return []
            }
            
        
        let deleteBtn = SwipeAction(style: .default, title: "Delete".localized()) { action, index in
            self.deletePublishSubject.onNext(cardSelected)
        }
        deleteBtn.backgroundColor = UIColor(red:(235.0 / 255.0) , green:(28.0 / 255.0) ,
        blue:(28.0 / 255.0) , alpha: 1)
        deleteBtn.image = UIImage(named: "delete")


        
        return [deleteBtn]
        
    }
    
}
