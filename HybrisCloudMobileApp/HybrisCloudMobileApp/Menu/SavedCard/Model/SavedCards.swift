//
//  SavedCards.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 26/12/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxDataSources


struct SavedCardsSection {
    var header: String
    var items: [Item]
    
}

extension SavedCardsSection : AnimatableSectionModelType, Equatable {
    typealias Item = CustomerPaymentOptions

    var identity: String {
        return header
    }

    init(original: SavedCardsSection, items: [CustomerPaymentOptions]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: SavedCardsSection, rhs: SavedCardsSection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension CustomerPaymentOptions: IdentifiableType, Equatable {
   

    
    public static func == (lhs: CustomerPaymentOptions, rhs: CustomerPaymentOptions) -> Bool {
        
        return (
                (lhs.identity  == rhs.identity)
            &&
                ((lhs.customerCardId ?? "") == (rhs.customerCardId ?? ""))
            &&
                ((lhs.customerCardLabel ?? "") == (rhs.customerCardLabel ?? ""))
            &&
                ((lhs.customerCardName ?? "") == (rhs.customerCardName ?? ""))
            &&
                ((lhs.customerCardNo ?? "") == (rhs.customerCardNo ?? ""))

            &&
                ((lhs.customerCardType ?? "") == (rhs.customerCardType ?? ""))
            &&
                ((lhs.customerEmail ?? "") == (rhs.customerEmail ?? ""))
            &&
                ((lhs.customerPayoptType ?? "") == (rhs.customerPayoptType ?? ""))
        )
        
        
    }
}


