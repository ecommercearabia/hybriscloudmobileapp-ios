//
//  TermsAndConditionsViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 27/01/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation

class TermsAndConditionsViewModel: ViewModelType{
    
    struct Input {
        let trigger: Driver<Void>
    }
    struct Output {
        let fetching: Driver<Bool>
        let componentOutput: Driver<Component>
        let error: Driver<Error>
    }
    
        
    private let componentUseCase: ComponentUseCase

    
    init(
        componentUseCase: ComponentUseCase) {
        self.componentUseCase = componentUseCase
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let component = input.trigger.flatMapLatest {
            return self.componentUseCase.component(componentId: "termsAndConditionsTextParagraph")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
              
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching,
                      componentOutput: component,
                      error: errors)
    }

}
