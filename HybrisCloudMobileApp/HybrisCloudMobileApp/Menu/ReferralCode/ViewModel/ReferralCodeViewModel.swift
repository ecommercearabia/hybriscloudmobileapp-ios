//
//  ReferralCodeViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by admin on 20/06/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation

class ReferralCodeViewModel: ViewModelType {
    
    let referralCodeUseCase: ReferralCodeUseCase
     let configUseCase: ConfigUseCase
    //MARK: Init UseCase
    init(referralCodeUseCase: ReferralCodeUseCase , configUseCase: ConfigUseCase) {
        self.referralCodeUseCase = referralCodeUseCase
        self.configUseCase = configUseCase
    }
    
    //MARK: Input
    struct Input {
        let trigger: Driver<Void>
        let generateCodeTrigger: Driver<Void>
        let sendSubscriptionEmailTrigger: Driver<Void>
        let subscriptionEmail: Driver<String>
    }
    
    //MARK: Output
    struct Output {
        let referralCodeData: Driver<ReferralCode>
        let sendSubscribeEmail: Driver<Void>
        let config : Driver<ConfigModel>
        let fetching: Driver<Bool>
        let error: Driver<Error>
    }
    
    //MARK: Transform Function
    func transform(input: Input) -> Output {
        
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let config = input.trigger.flatMapLatest{
            return self.configUseCase.config()
                .trackError(errorTracker)
                    .trackActivity(activityIndicator)
                    .asDriverOnErrorJustComplete()

        }
        
        //MARK: Get Referral Code
        let referralCodeData = input.trigger.flatMapLatest {
            return self.referralCodeUseCase.getReferralCode()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        //MARK: Generate Referral Code
        let generateReferralCode = input.generateCodeTrigger.flatMapLatest {
            return self.referralCodeUseCase.generateReferralCode()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        //MARK: Send Subscribe Email
        let sendSubscribeEmail = input.sendSubscriptionEmailTrigger.withLatestFrom(input.subscriptionEmail)
            .flatMapLatest { emails in
                return self.referralCodeUseCase.sendSubscribeEmail(email: emails, inSeperateEmails: true)
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
            }

        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(referralCodeData: Driver.merge(referralCodeData,generateReferralCode),
                      sendSubscribeEmail: sendSubscribeEmail, config: config,
                      fetching: fetching,
                      error: errors)
    }
    
    //MARK: TableView Data Source Funciton
    func dataSourceReferralCode() -> RxTableViewSectionedReloadDataSource<ReferralCodeSectionModel> {
        return RxTableViewSectionedReloadDataSource<ReferralCodeSectionModel> { dataSource, tableView, indexPath, item in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReferralCodeTableViewCell", for: indexPath) as! ReferralCodeTableViewCell
            cell.setupCell(item: item)
            return cell
        }
    }
}

//MARK: Referral Code Subscribe Email Model
struct ReferralCodeSubscribeEmailModel {
    var emails: String?
    var inSeperateEmails: Bool?
}
