//
//  ReferralCodeTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by admin on 20/06/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import UIKit

class ReferralCodeTableViewCell: UITableViewCell {

    @IBOutlet weak var customerDataStackView: UIStackView!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
        
    //MARK: Setup Referral Cell
    func setupCell(item: History){
        self.customerNameLabel.text = item.customer?.name ?? ""
        self.amountLabel.text = "\(item.amount ?? 0.0)"
        let formatedDate = self.convertDateFormat(inputDate: item.creationDate ?? "")
        self.dateLabel.text = formatedDate
        
    }
    
    func convertDateFormat(inputDate: String) -> String {

         let olDateFormatter = DateFormatter()
         olDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

         let oldDate = olDateFormatter.date(from: inputDate)

         let convertDateFormatter = DateFormatter()
         convertDateFormatter.dateFormat = "MM-dd-yyyy"

         return convertDateFormatter.string(from: oldDate!)
    }
}
