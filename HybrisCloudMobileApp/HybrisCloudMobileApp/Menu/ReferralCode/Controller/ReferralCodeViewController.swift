//
//  ReferralCodeViewController.swift
//  HybrisCloudMobileApp
//
//  Created by admin on 17/06/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import UIKit

class ReferralCodeViewController: UIViewController {
    
    let cellHeight = 50
    let customerHistorySubject = BehaviorSubject<[History]>(value: [])
    let referralCodeSubject = BehaviorSubject<String>(value: "")
    let subscribeEmailSubject = BehaviorSubject<String>(value: "")
    let referralViewModel = ReferralCodeViewModel(referralCodeUseCase: NetworkPlatform.UseCaseProvider().makeReferralCodeUseCase(), configUseCase: NetworkPlatform.UseCaseProvider().makeConfig())
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var customerDataTableView: UITableView!
    
    @IBOutlet weak var availableAmountTitleLabel: UILabel!
    @IBOutlet weak var availableAmountValueLabel: UILabel!
    @IBOutlet weak var myCodeTitleLabel: UILabel!
    @IBOutlet weak var generateCodeButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var innerDataView: UIView!
    
    
    @IBOutlet weak var subscribeEmailView: UIView!
    @IBOutlet weak var subscribeEmailTextField: UITextField!
    @IBOutlet weak var sendEmailButton: UIButton!
    @IBOutlet weak var cancelEmailButton: UIButton!
    var referralCodeConfigurations : ReferralCodeConfigurations?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.preparation()
    }
    

    
    //MARK: Preparation
    private func preparation(){
        
        self.customerDataTableView.delegate = self
        self.customerDataTableView.tableFooterView = UIView()
        
        self.subscribeEmailView.isHidden = true
        self.innerDataView.isHidden = true
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = ReferralCodeViewModel.Input(
            trigger: viewWillAppear,
            generateCodeTrigger: self.generateCodeButton.rx.tap.asDriver(),
            sendSubscriptionEmailTrigger: self.sendEmailButton.rx.tap.asDriver(),
            subscriptionEmail: self.subscribeEmailTextField.rx.text.orEmpty.asDriver()
        )
        
        let output = self.referralViewModel.transform(input: input)
        self.referralCodeConfigurations(output: output)
        self.getReferralCodeData(output: output)
        self.handleSendSubscribeEmailResponse(output: output)
        self.shareButtonTapped()
        self.handleSubscribeEmailViewCancelButton()
        
        self.subscribeEmailTextField.rx.text.subscribe(onNext: { text in
            
            self.sendEmailButton.isEnabled = !(text?.isEmpty ?? false)
        
        }).disposed(by: disposeBag)
    
    }
    //MARK: Get ReferralCode Configurations
    func referralCodeConfigurations(output: ReferralCodeViewModel.Output){
        output.config.asObservable().subscribe(onNext: {(config) in
            self.referralCodeConfigurations = config.registrationConfiguration?.referralCodeConfigurations
        }).disposed(by: disposeBag)
    }
    
    //MARK: Get ReferralCode Data
    private func getReferralCodeData(output: ReferralCodeViewModel.Output) {
        
        output.referralCodeData.asObservable().subscribe(onNext: { referralCodeData in
            
            self.innerDataView.isHidden = false
            self.availableAmountValueLabel.text = "\(referralCodeData.totalAmount ?? 0.0)"
            
            guard let refferalCode = referralCodeData.code else {
                self.generateCodeButton.isUserInteractionEnabled = true
                self.generateCodeButton.setTitle("Generate Code".localized(), for: .normal)
                self.generateCodeButton.sendActions(for: .valueChanged)
                return
            }
            self.referralCodeSubject.onNext(refferalCode)
            self.generateCodeButton.isUserInteractionEnabled = false
            self.generateCodeButton.setTitle(refferalCode, for: .normal)
            self.generateCodeButton.sendActions(for: .valueChanged)
            
            guard let histories = referralCodeData.histories else {
                self.innerDataView.isHidden = true
                return
            }
            self.innerDataView.isHidden = false
            self.customerHistorySubject.onNext(histories)
        }).disposed(by: disposeBag)
        
        self.customerHistorySubject.asDriverOnErrorJustComplete().map { history -> [ReferralCodeSectionModel] in
            return [ReferralCodeSectionModel(header: "", items: history)]
        }.asObservable()
        .bind(to: self.customerDataTableView.rx.items(dataSource: self.referralViewModel.dataSourceReferralCode()))
        .disposed(by: disposeBag)
    }
    
    //MARK: Handle Send Subscribe Email Response
    private func handleSendSubscribeEmailResponse(output:ReferralCodeViewModel.Output) {
        output.sendSubscribeEmail.asObservable().subscribe(onNext: {
            print($0)
            self.showHideSubscribeEmailView(isHidden: true)
            self.successLoaf(message: "Thank you for sharing your referral code! Once your friend registers and uses the code to place an order successfully, you will receive your free ".localized() +  " \(self.referralCodeConfigurations?.senderRewardAmount ?? 0)" + " AED".localized()  + " store credit in your account.".localized())
            
        }).disposed(by: disposeBag)
    }
    
    //MARK: Share Button Action
    private func shareButtonTapped() {
        self.shareButton.rx.tap.subscribe(onNext: {
            let code = try? self.referralCodeSubject.value()
            if code == "" {
                self.errorLoaf(message: "please generate code before share".localized())
            } else {
                self.showActionSheet()
            }
           
        }).disposed(by: disposeBag)
    }
    
    //MARK: Present Action Sheet
    private func showActionSheet(){
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "share".localized(), style: .default , handler:{ (UIAlertAction)in
            self.shareCode()
        }))
        
        alert.addAction(UIAlertAction(title: "mail".localized(), style: .default , handler:{ (UIAlertAction)in
            self.showHideSubscribeEmailView(isHidden: false)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:{ (UIAlertAction)in
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    //MARK: Share Code Function
    private func shareCode() {
        let code = try? self.referralCodeSubject.value()
        let text = "Congratulations! Your friend has referred Food Crowd to you!".localized() +
        "\n" +
        "Avail free ".localized() +
        "  \(self.referralCodeConfigurations?.receiverRewardAmount ?? 0) " + " AED".localized() + " " +
        "store credit on your first order with Food Crowd - the online grocery store that offers same day delivery across the UAE.".localized() +
        "\n" +
        "Click".localized() +
        "\nhttps://stg.foodcrowd.com/en/register?referralCode=".localized() +
        "\(code ?? "")\n" +
        " to register and start your shopping journey.".localized()
        + "\n" +
        "Join the Crowd!".localized()

        let activityViewController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
        activityViewController.completionWithItemsHandler =  {
            (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
           if  completed {
               self.successLoaf(message: "Thank you for sharing your referral code! Once your friend registers and uses the code to place an order successfully, you will receive your free ".localized()  + " \(self.referralCodeConfigurations?.senderRewardAmount ?? 0)" + " AED".localized() + " store credit in your account.".localized())
            }
        }
    }
    
    //MARK: Handle Subscribe Email View Cancel Button
    private func handleSubscribeEmailViewCancelButton() {
        self.cancelEmailButton.rx.tap.subscribe(onNext: {
            self.showHideSubscribeEmailView(isHidden: true)
        }).disposed(by: disposeBag)
    }
    
    //MARK: Show / Hide Subscribe Email View
    private func showHideSubscribeEmailView(isHidden:Bool) {
        UIView.transition(with: self.subscribeEmailView, duration: 0.7, options: .transitionCrossDissolve, animations: {
            self.subscribeEmailView.isHidden = isHidden
            self.subscribeEmailTextField.text = ""
            self.subscribeEmailTextField.sendActions(for: .valueChanged)
           })
    }
}

//MARK: TableView Delegate Extension
extension ReferralCodeViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(self.cellHeight)
    }
}
