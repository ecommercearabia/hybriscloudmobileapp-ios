//
//  ReferralCodeModel.swift
//  HybrisCloudMobileApp
//
//  Created by admin on 20/06/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

struct ReferralCodeSectionModel {
    var header: String
    var items: [History]
}

extension ReferralCodeSectionModel: SectionModelType {
    
    typealias Item = History
    
    init(original: ReferralCodeSectionModel, items: [History]) {
        self = original
        self.items = items
    }
}
