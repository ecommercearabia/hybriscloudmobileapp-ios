//
//  AboutUsViewController.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 26/01/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import Foundation
import UIKit

class AboutUsViewController: UIViewController {
    
    @IBOutlet weak var describtionAboutUs :UITextView!
    
    var viewModel:AboutUsViewModel!
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
                   .mapToVoid()
                   .asDriverOnErrorJustComplete()
        
        viewModel = AboutUsViewModel(componentUseCase: networkUseCaseProvider.makeComponentUseCase())
      
        
        let input = AboutUsViewModel.Input(trigger: viewWillAppear)
        
         let output = viewModel.transform(input: input)
        
        output.error.asObservable().subscribe(onNext: { (error) in
                print(error)
            }).disposed(by: disposeBag)
        
        output.componentOutput.asObservable().subscribe(onNext: { (aboutUs) in
            let attrText = aboutUs.content?.htmlAttributedString()
                        let paragraph = NSMutableParagraphStyle()
            paragraph.baseWritingDirection = L102Language.isRTL ?  .rightToLeft : .leftToRight
                        paragraph.alignment = .natural
            attrText?.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraph, range: NSMakeRange(0, attrText?.length ?? 0))

            self.describtionAboutUs.attributedText = attrText
        
            }).disposed(by: disposeBag)
        
    }
}
