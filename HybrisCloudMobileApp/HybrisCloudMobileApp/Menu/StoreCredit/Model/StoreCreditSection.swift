//
//  StoreCreditSection.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/19/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
//import UIKit
//import RxSwift
//import RxCocoa
//import RxDataSources


struct StoreCreditSection {
    var header: Header
    var items: [Item]
    
}

extension StoreCreditSection : AnimatableSectionModelType, Equatable {
    typealias Item = StoreCreditHistroyElement
    typealias Header = StoreCreditAmount

    var identity: String {
        return header.identity
    }

    init(original: StoreCreditSection, items: [StoreCreditHistroyElement]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: StoreCreditSection, rhs: StoreCreditSection) -> Bool {
        return lhs.header.identity == rhs.header.identity
    }
}


extension StoreCreditHistroyElement: IdentifiableType, Equatable {
   
    
    public static func == (lhs: StoreCreditHistroyElement, rhs: StoreCreditHistroyElement) -> Bool { return lhs.orderCode  == rhs.orderCode}
}

