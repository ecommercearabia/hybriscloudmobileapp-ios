//
//  StoreCreditViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/19/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Domain
import NetworkPlatform

class StoreCreditViewModel: ViewModelType{
    
    struct Input {
        let trigger: Driver<Void>
    }
    struct Output {
        let fetching: Driver<Bool>
        let history: Driver<StoreCreditHistroy>
        let amount: Driver<StoreCreditAmount>
        let output: Driver<(StoreCreditAmount,StoreCreditHistroy)>
        let error: Driver<Error>
    }
    
    
    let networkUseCaseProvider : Domain.UseCaseProvider = NetworkPlatform.UseCaseProvider()
    
    private let historyUseCase: StoreCreditHistroyUseCase
    private let amountUseCase: StoreCreditAmountUseCase

    
    init(
        historyUseCase: StoreCreditHistroyUseCase
        , amountUseCase: StoreCreditAmountUseCase) {
        self.historyUseCase = historyUseCase
        self.amountUseCase = amountUseCase
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let amount = input.trigger.flatMapLatest {
            return self.amountUseCase.getAmount(userId: "current")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let history = input.trigger.flatMapLatest{
            return self.historyUseCase.getHistory(userId: "current")
            .trackActivity(activityIndicator)
            .trackError(errorTracker)
            .asDriverOnErrorJustComplete()
        }        
        
        let output = Driver.combineLatest( amount, history )
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching,
                      history: history,
                      amount: amount,
                      output : output,
                      error: errors)
    }
    
    func dataSourceStoreCredit() -> RxTableViewSectionedAnimatedDataSource<StoreCreditSection>
           {
               return RxTableViewSectionedAnimatedDataSource<StoreCreditSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .bottom),configureCell: { ds, cv, indexPath, item in
                         
                   guard let cell = cv.dequeueReusableCell(withIdentifier: "StoreCreditHistoryCell", for: indexPath) as? StoreCreditHistoryCell else { return UITableViewCell()}
                   
                   cell.setUp(storeCreditHistroyElement: item)

                   return cell

                     })
                 }
}
