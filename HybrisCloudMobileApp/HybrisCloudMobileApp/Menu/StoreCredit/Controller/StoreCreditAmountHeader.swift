//
//  StoreCreditAmountHeader.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/19/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
class StoreCreditAmountHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var amount: UILabel!
    var storeCreditAmountBehaviorSubject :BehaviorSubject<StoreCreditAmount>?
    let disposeBag = DisposeBag()
   
    override func awakeFromNib() {
           super.awakeFromNib()
           
        storeCreditAmountBehaviorSubject?.subscribe(onNext: { (storeCreditAmount) in
                self.amount.text = storeCreditAmount.formattedValue ?? ""
            }).disposed(by: disposeBag)
        
       }
}
