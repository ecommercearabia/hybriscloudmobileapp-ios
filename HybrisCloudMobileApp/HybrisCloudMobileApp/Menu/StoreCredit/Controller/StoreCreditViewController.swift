//
//  StoreCreditViewController.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/19/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

class  StoreCreditViewController : UIViewController {
    
    @IBOutlet weak var storeCreditTableView: UITableView!{
        didSet{
            storeCreditTableView.tableFooterView = UIView.init(frame: CGRect.zero)

        }
    }
    
    var viewModel:StoreCreditViewModel!
    
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var amountLbl: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
       

        storeCreditTableView.refreshControl = UIRefreshControl()

        
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()

        viewModel = StoreCreditViewModel(
            historyUseCase: networkUseCaseProvider.makeStoreCreditHistroyUseCase(),
            amountUseCase: networkUseCaseProvider.makeStoreCreditAmountUseCase())
        
        let viewWillAppear =
            rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
                   .mapToVoid()
                   .asDriverOnErrorJustComplete()
        
        let pull = storeCreditTableView.refreshControl!.rx
                   .controlEvent(.valueChanged)
                   .asDriver()
        
        let input = StoreCreditViewModel.Input(trigger: Driver.merge(viewWillAppear, pull))
                       
        let output = viewModel.transform(input: input)
        
        output.fetching
                   .drive(storeCreditTableView.refreshControl!.rx.isRefreshing)
                   .disposed(by: disposeBag)
        
        output.error.asObservable().subscribe(onNext: { (error) in
            print(error)
        }).disposed(by: disposeBag)
        
        
        
        output.output.map { (arg0) -> [StoreCreditSection] in
            
            let (StoreCreditAmount, StoreCreditHistroy) = arg0
            
            self.amountLbl.text = StoreCreditAmount.formattedValue ?? ""
            
            return [StoreCreditSection(header: StoreCreditAmount, items: StoreCreditHistroy.storeCreditHistroy ?? [])]

        }.asObservable().bind(to: storeCreditTableView.rx.items(dataSource: viewModel.dataSourceStoreCredit())).disposed(by: disposeBag)

      
        
    }
    
 
}
