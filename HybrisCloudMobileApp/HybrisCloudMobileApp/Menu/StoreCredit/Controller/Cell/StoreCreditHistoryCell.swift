//
//  StoreCreditHistoryCell.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/19/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import SKCountryPicker


class StoreCreditHistoryCell: UITableViewCell {
        
    @IBOutlet weak var DateStoreCreditHistoryLabel: UILabel!
    @IBOutlet weak var TimeStoreCreditHistoryLabel: UILabel!
    @IBOutlet weak var AmountStoreCreditHistoryLabel: UILabel!
    @IBOutlet weak var BalanceStoreCreditHistoryLabel: UILabel!
    @IBOutlet weak var OrderNumberStoreCreditHistoryLabel: UILabel!
    
   
    func setUp(storeCreditHistroyElement : StoreCreditHistroyElement) {
        
        
        self.DateStoreCreditHistoryLabel.text = getDateFormat(inputDate: storeCreditHistroyElement.dateOfPurchase ?? "2020-08-19T10:33:37.477Z")
      self.TimeStoreCreditHistoryLabel.text = getTimeFormat(inputDate: storeCreditHistroyElement.dateOfPurchase ?? "2020-08-19T10:33:37.477Z")
        self.AmountStoreCreditHistoryLabel.text = storeCreditHistroyElement.amount?.formattedValue ?? ""
        self.BalanceStoreCreditHistoryLabel.text =  storeCreditHistroyElement.balance?.formattedValue ?? ""
        self.OrderNumberStoreCreditHistoryLabel.text = storeCreditHistroyElement.orderCode ?? ""

    }
    
    func getDateFormat(inputDate: String) -> String {

         let olDateFormatter = DateFormatter()
         olDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let oldDate = olDateFormatter.date(from: inputDate) ?? Date()

         let convertDateFormatter = DateFormatter()
         convertDateFormatter.dateFormat = "MMM d, yyyy"

        return convertDateFormatter.string(from: oldDate)
    }

    
    func getTimeFormat(inputDate: String) -> String {

         let olDateFormatter = DateFormatter()
         olDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

         let oldDate = olDateFormatter.date(from: inputDate) ?? Date()

         let convertDateFormatter = DateFormatter()
         convertDateFormatter.dateFormat = "h:mm a"
        return convertDateFormatter.string(from: (oldDate))
    }
    
  
}
