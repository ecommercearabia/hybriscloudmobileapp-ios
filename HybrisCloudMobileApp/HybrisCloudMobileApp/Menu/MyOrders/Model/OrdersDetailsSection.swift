//
//  OrdersDetailsSection.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/12/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

struct OrdersDetailsSection {
    var header : String
    var items  : [Item]
}
extension OrdersDetailsSection : AnimatableSectionModelType , Equatable{
    
    typealias Item = ordersDetailsEntry
    var identity: String {
        return header
    }
    
    init(original: OrdersDetailsSection, items: [ordersDetailsEntry]) {
        self = original
        self.items = items
    }
    static func ==(lhs : OrdersDetailsSection , rhs : OrdersDetailsSection) -> Bool{
        return lhs.header == rhs.header
    }
}

extension ordersDetailsEntry : IdentifiableType , Equatable{
    public var identity: String{
        return  product?.code ?? ""
    }
     public static func == (lhs: ordersDetailsEntry, rhs: ordersDetailsEntry) -> Bool { return lhs.product?.code  == rhs.product?.code }
}
