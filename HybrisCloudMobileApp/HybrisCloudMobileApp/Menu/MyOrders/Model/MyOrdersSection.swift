//
//  MyOrdersSection.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

struct MyOrdersSection {
    var header: String
    var items: [Item]
    
}

extension MyOrdersSection : AnimatableSectionModelType, Equatable {
    typealias Item = Order

    var identity: String {
        return header
    }

    init(original: MyOrdersSection, items: [Order]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: MyOrdersSection, rhs: MyOrdersSection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension Order: IdentifiableType, Equatable {
    public var identity: String {
        return  code ?? ""
    }
    
    public static func == (lhs: Order, rhs: Order) -> Bool { return lhs.code  == rhs.code }
}

