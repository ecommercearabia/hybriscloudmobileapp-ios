//
//  OrdersDetailsViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/12/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import SwipeCellKit
import KeychainSwift
class OrdersDetailsViewModel {
     var ordersDeatilsBehaviorsSubject = BehaviorSubject<[OrdersDetailsSection]>(value : [OrdersDetailsSection(header : "",items : [])])
    struct Input {
        let orderDetailsTrigger : Driver<Void>
        let ordersDetails : Driver<Order>
        
    }
    
    struct Ouput {
           let fetching: Driver<Bool>
            var ordersDetails: Driver<ordersDetailsMyOrdersDetails>
            let error: Driver<Error>
    }
    
    let ordersDetailsUseCase : OrdersDetailsUseCase
    init(getordersDetailsUseCase : OrdersDetailsUseCase) {
        self.ordersDetailsUseCase = getordersDetailsUseCase
    }
    
    func transform(input : Input) -> Ouput {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let ordersDetails = input.orderDetailsTrigger.withLatestFrom(input.ordersDetails).map{ (orderDetails) in
            return orderDetails.code ?? "" }.flatMapLatest{ [self] in
                return self.ordersDetailsUseCase.getOrdersDetails(userId: KeychainSwift().get("user") ?? "", orderCode: $0).trackActivity(activityIndicator).trackError(errorTracker).asDriverOnErrorJustComplete()
        }
        
        let fetching = activityIndicator.asDriver()
               let errors = errorTracker.asDriver()
        return Ouput(fetching: fetching, ordersDetails: ordersDetails, error: errors)
    }
    
    func dataSource() -> RxTableViewSectionedAnimatedDataSource<OrdersDetailsSection> {
        return
        RxTableViewSectionedAnimatedDataSource<OrdersDetailsSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .right),configureCell: { ds, cv, indexPath, item in
        
                    guard let cell = cv.dequeueReusableCell(withIdentifier: "OrdersItemTableViewCell", for: indexPath) as? OrdersItemTableViewCell else {
                              return UITableViewCell()
                          }
                    
                    cell.setUp(item: item)
           
                           return cell
            
           
             }  )
            
        
       
    }
    
}
