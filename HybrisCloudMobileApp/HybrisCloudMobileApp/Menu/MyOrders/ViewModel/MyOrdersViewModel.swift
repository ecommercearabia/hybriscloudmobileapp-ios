//
//  MyOrdersViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import NetworkPlatform
class MyOrdersViewModel : ViewModelType{
    
    private let myOrdersUseCase : MyOrdersUseCase
    var userId : String?
    init(myOrdersUseCase : MyOrdersUseCase) {
        self.myOrdersUseCase = myOrdersUseCase
    }
    
    var MyOrdersSubject = BehaviorSubject<[MyOrdersSection]>(value: [MyOrdersSection(header: "", items: [])])
    
    struct Input {
         let trigger: Driver<Void>
        let OrdersTrigger: Driver<Void>
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let myOrders: Driver<MyOrders>
        let error: Driver<Error>
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        
        let errorTracker = ErrorTracker()
        let myOredersResult = input.OrdersTrigger.flatMapLatest {
            [unowned self] in
            return self.myOrdersUseCase.getMyOrders(currentPage: 0, pageSize: 100, statuses: "", sort: "", userId: self.userId!).trackActivity(activityIndicator).asDriverOnErrorJustComplete()
        }
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
                     
        return Output(fetching: fetching,
                      myOrders: myOredersResult,
                                   error: errors)
   
    }
    
    func dataSource() -> RxTableViewSectionedAnimatedDataSource<MyOrdersSection> {
        return RxTableViewSectionedAnimatedDataSource<MyOrdersSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .bottom),configureCell: { ds, cv, indexPath, item in
      
           print(item)
            guard let orderCell = cv.dequeueReusableCell(withIdentifier: "MyOrdersTableViewCell", for: indexPath)as? MyOrdersTableViewCell else{
                return UITableViewCell()
            }
            orderCell.selectionStyle = .none
            orderCell.setUp(order: item)
            return orderCell
        })
    }
    
}
