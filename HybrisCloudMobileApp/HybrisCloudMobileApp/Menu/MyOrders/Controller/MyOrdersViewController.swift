//
//  MyOrdersViewController.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import Domain
import SwipeCellKit
import KeychainSwift
class MyOrdersViewController: UIViewController {

    @IBOutlet weak var ordersTableView: UITableView!
    
    @IBOutlet weak var emptyOrdersView: UIView!
    
    let disposeBag = DisposeBag()
    var MyordersVM : MyOrdersViewModel!
    
    
       var titleLabel = UILabel()
      var subtitleLabel = UILabel()
    
    lazy var titleStackView: UIStackView = {

         titleLabel.textAlignment = .center
    self.titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        self.titleLabel.text = "My Orders".localized()
             subtitleLabel.textAlignment = .center
    self.subtitleLabel.font = UIFont.systemFont(ofSize: 15)
         let stackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
         stackView.axis = .vertical
      
    stackView.spacing = 3
         return stackView
    
     }()
    
    override func viewWillAppear(_ animated: Bool) {
        handelUserIsLogin()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.titleView = titleStackView
    preparation()
    myOrdersSetUP()
    }
    fileprivate func handelUserIsLogin(){
        let userId = KeychainSwift().get("user") ?? ""
        if userId == "anonymous" {
             ordersTableView.isHidden = true
            emptyOrdersView.isHidden = false
        }
        else{
            ordersTableView.isHidden = false
                 emptyOrdersView.isHidden = true
        }
        MyordersVM.userId = userId
    }
    
    func myOrdersSetUP(){
        
        self.MyordersVM.MyOrdersSubject.bind(to: ordersTableView.rx.items(dataSource: self.MyordersVM.dataSource())).disposed(by: disposeBag)
        self.ordersTableView.rx.setDelegate(self).disposed(by: disposeBag)
        self.ordersTableView.rx.modelSelected(MyOrdersSection.Item.self).subscribe(onNext:{(item) in print("order selected")
            self.performSegue(withIdentifier: "toOrdersDetails", sender: item)
            
           
        }).disposed(by: disposeBag)
        
    }
    
    func preparation(){
        let  networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        MyordersVM = MyOrdersViewModel.init(myOrdersUseCase: networkUseCaseProvider.makeMyOrdersUseCase())
        handelUserIsLogin()
        let viewWillApper =
            rx.sentMessage(#selector(UIViewController.viewDidAppear(_:)))
        .mapToVoid()
        .asDriverOnErrorJustComplete()
        
        let input = MyOrdersViewModel.Input(trigger: Driver.merge(viewWillApper), OrdersTrigger: Driver.merge(viewWillApper))
        
        let output = MyordersVM.transform(input: input)
        output.myOrders.asObservable().subscribe(onNext: { (order) in
            self.subtitleLabel.text = (order.orders?.count.description ?? "") + " Orders".localized()
            self.MyordersVM.MyOrdersSubject.onNext([MyOrdersSection(header: "", items: order.orders ?? [])])}).disposed(by: disposeBag)
        
        output.error.asObservable().subscribe(onNext:{
            (Error) in
           
            print("error")
            }).disposed(by: disposeBag)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toOrdersDetails" {
            if let vc = segue.destination as? MasterOrdersDetailsViewController{
                guard let orderDetails = sender as? Order  else {
                    return
                }
                vc.ordersDeatilsBehaviorsSubject = BehaviorSubject<Order>(value: orderDetails)
              
            }
        }
    }

}

extension MyOrdersViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}
