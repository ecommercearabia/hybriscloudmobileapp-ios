//
//  OrdersDetailsViewController.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/12/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import Domain
import SwipeCellKit
import KeychainSwift
class MasterOrdersDetailsViewController: UIViewController {
    @IBOutlet weak var ordersSegmentControl: UISegmentedControl!
       var titleLabel = UILabel()
      var subtitleLabel = UILabel()

    let disposeBag = DisposeBag()
    var orderCode : String?
    var ordersDetailsVM : OrdersDetailsViewModel!
    var ordersDeatilsBehaviorsSubject : BehaviorSubject<Order>?
    
    lazy var titleStackView: UIStackView = {

         titleLabel.textAlignment = .center
    self.titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
             subtitleLabel.textAlignment = .center
    self.subtitleLabel.font = UIFont.systemFont(ofSize: 15)
         let stackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
         stackView.axis = .vertical
      
    stackView.spacing = 3
         return stackView
    
     }()
    
    lazy var orderitemsViewController : OrdersItemsViewController = {
        let storyboard = UIStoryboard(name : "MyOrders" , bundle : Bundle.main)
        var viewController =  storyboard.instantiateViewController(withIdentifier : "OrdersItemsViewController")as! OrdersItemsViewController
        self.addViewControllerAsChildViewController(chaildViewController : viewController)
        
        return viewController
    }()
    
    lazy var ordersDetailsViewController : OrdersDetailsViewController = {
        let storyboard = UIStoryboard(name : "MyOrders" , bundle : Bundle.main)
        var viewController =  storyboard.instantiateViewController(withIdentifier : "OrdersDetailsViewController")as! OrdersDetailsViewController
        self.addViewControllerAsChildViewController(chaildViewController : viewController)
        return viewController
    }()
    

 
    override func viewDidLoad() {
        super.viewDidLoad()
         navigationItem.titleView = titleStackView
        orderitemsViewController.view.isHidden = false
        ordersDetailsViewController.view.isHidden = true
        setupView()
        preparation()
        ordersDetailsSetUp()
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white as Any], for: .selected)
    
    }
    

    func ordersDetailsSetUp(){
        self.ordersDetailsVM.ordersDeatilsBehaviorsSubject.bind(to: orderitemsViewController.itemsTableView.rx.items(dataSource: self.ordersDetailsVM.dataSource())).disposed(by: disposeBag)

     

    }
    
     func preparation(){
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        ordersDetailsVM = OrdersDetailsViewModel.init(getordersDetailsUseCase: networkUseCaseProvider.makeOrdersDetailsUseCase())
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
        .mapToVoid()
        .asDriverOnErrorJustComplete()

        guard  let subject = ordersDeatilsBehaviorsSubject else {
            return
        }
        let input = OrdersDetailsViewModel.Input(orderDetailsTrigger: Driver.merge(viewWillAppear), ordersDetails: subject
            .asObservable()
            .asDriverOnErrorJustComplete())

        let output = ordersDetailsVM.transform(input: input)
        output.ordersDetails.asObservable().subscribe(onNext: { (item) in
            self.ordersDetailsVM.ordersDeatilsBehaviorsSubject.onNext([OrdersDetailsSection(header: "", items: item.entries ?? [] )])
            self.orderitemsViewController.ordersItemSetup(item: item)
            self.ordersDetailsViewController.ordersDetailsSetUp(item: item)
            self.titleLabel.text = item.code
            self.subtitleLabel.text = (item.totalItems?.description ?? "") + " items".localized()
            
        } ).disposed(by: self.disposeBag)
    }

    
    private func setupView(){
       setupSegmentControl()
        if #available(iOS 13, *){
             
        }
        
        else{
            
            ordersSegmentControl.tintColor = #colorLiteral(red: 0.9411764706, green: 0.5529411765, blue: 0.2235294118, alpha: 1)
            ordersSegmentControl.borderWidth = 0
            ordersSegmentControl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            ordersSegmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black as Any], for: .normal)
            
            
        }
        
    }
    
    private func updateView(){
      
             orderitemsViewController.view.isHidden = (ordersSegmentControl.selectedSegmentIndex != 0)
                    ordersDetailsViewController.view.isHidden = (ordersSegmentControl.selectedSegmentIndex == 0)
      
       

    }
    private func setupSegmentControl() {
   
        ordersSegmentControl.removeAllSegments()
        ordersSegmentControl.insertSegment(withTitle : "Items".localized(), at : 0 , animated : false)
        ordersSegmentControl.insertSegment(withTitle : "Details".localized(), at : 1 , animated : false)
        ordersSegmentControl.addTarget(self , action : #selector(selectionDidChange(sender:)) , for : .valueChanged)
        ordersSegmentControl.selectedSegmentIndex = 0
        
    }
    @objc func selectionDidChange(sender : UISegmentedControl)   {
 
        updateView()
    }
    
    private func addViewControllerAsChildViewController(chaildViewController : UIViewController){
        addChild(chaildViewController)
        view.addSubview(chaildViewController.view)
        let width = view.frame.size.width
        let height = view.frame.size.height

        let frame = CGRect(x: width * 0.0, y: height * 0.2, width: width * 1, height: height * 0.55)

        
        chaildViewController.view.frame = frame
       
        chaildViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        chaildViewController.didMove(toParent : self)
    }
    private func removeViewControllerAsChildViewController(chaildViewController : UIViewController){
        chaildViewController.willMove(toParent : nil)
        chaildViewController.view.removeFromSuperview()
        chaildViewController.removeFromParent()
    }
}

    
    
    

