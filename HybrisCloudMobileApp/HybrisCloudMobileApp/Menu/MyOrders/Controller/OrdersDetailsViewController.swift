//
//  OrdersDetailsViewController.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/13/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class OrdersDetailsViewController: UIViewController {
    @IBOutlet weak var orderNumberLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var orderDatePlacedLbl: UILabel!
    
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var orderLocationLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    

    func ordersDetailsSetUp(item : ordersDetailsMyOrdersDetails)  {

        orderNumberLbl.text = item.code
        orderStatusLbl.text = item.status
        orderDatePlacedLbl.text = item.timeSlotInfoData?.date
        userNameLbl.text = item.user?.name
        orderLocationLbl.text = item.deliveryAddress?.formattedAddress


    }

}
