//
//  OrdersItemsViewController.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/13/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import Domain
import SwipeCellKit
import KeychainSwift
class OrdersItemsViewController: UIViewController {

    @IBOutlet weak var orderSubtotalLbl: UILabel!
    
    @IBOutlet weak var orderNetAmountLbl: UILabel!
    
    @IBOutlet weak var orderTotalLbl: UILabel!
    
    @IBOutlet weak var itemsTableView: UITableView!{
        didSet{
            itemsTableView.tableFooterView = UIView.init(frame: CGRect.zero)

        }
    }
    let disposeBag = DisposeBag()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        itemsTableView.rx.setDelegate(self).disposed(by: disposeBag)

    }
    func ordersItemSetup(item :  ordersDetailsMyOrdersDetails){
        orderSubtotalLbl.text = item.subTotal?.formattedValue
        orderNetAmountLbl.text = item.totalTax?.formattedValue
        orderTotalLbl.text = item.totalPriceWithTax?.formattedValue
    }

}


extension OrdersItemsViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 142.5
    }
}
