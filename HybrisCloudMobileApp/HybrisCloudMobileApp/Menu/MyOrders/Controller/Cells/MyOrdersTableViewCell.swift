//
//  MyOrdersTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class MyOrdersTableViewCell: UITableViewCell {

    @IBOutlet weak var orderNumberLbl: UILabel!
    
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var orderDatePlacedLbl: UILabel!
    
    @IBOutlet weak var orderTotalLbl: UILabel!
    
    @IBOutlet weak var orderDetailsBttn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUp(order : Order){
        orderNumberLbl.text = order.code ?? ""
        orderStatusLbl.text = order.status ?? ""
        orderDatePlacedLbl.text = order.placed ?? ""
        orderTotalLbl.text = order.total?.value?.description ?? ""
        
    }
}
