//
//  OrdersDetailsTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/12/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class OrdersItemTableViewCell: UITableViewCell {

    @IBOutlet weak var itemImage: UIImageView!
    
    @IBOutlet weak var itemNameLbl: UILabel!
    
    @IBOutlet weak var itemPriceLbl: UILabel!
    
    @IBOutlet weak var itemQTYLbl: UILabel!
    
    @IBOutlet weak var itemSKULbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUp(item : ordersDetailsEntry)   {
        self.itemImage.downloadImageInCellWithoutCorner(urlString: (item.product?.images?.first(where: { (image) -> Bool in
                       return image.format == "zoom"
                   }))!.url!)
   
        itemNameLbl.text = item.product?.name
        itemPriceLbl.text = item.basePrice?.formattedValue?.description
        itemQTYLbl.text = item.quantity?.description
        itemSKULbl.text = item.product?.code
    }

}
