//
//  WishListItemViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/26/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

class WishListItemViewModel: ViewModelType{
    
    struct Input {
        let quantity : Driver<String>
        let products: Driver<ProductDetails>
        let increaseQuantityTrigger: Driver<Void>
        let decreaseQuantityTrigger: Driver<Void>
    }
     
     struct Output {
        let increaseQuantityOutput : Driver<String>
        let decreseQuantityOutput : Driver<String>
     }
   
    func transform(input: Input) -> Output {
        
        let inputAddtoCart = Driver.combineLatest(
                  input.products,
                  input.quantity
              )
              
        let  decreaseQuantityOutput :Driver<String> = input.decreaseQuantityTrigger.withLatestFrom(input.quantity).map { (quantity)  in
                                   
                                   return quantity
                               }.flatMapLatest { [] in
                                   if (Int($0) ?? 0 > 1)
                                   {
                                  return Driver.just(String((Int($0) ?? 0) - 1))
                                   }else {
                                    return  Driver.just($0)
                                   }
                               }
                        
        let  increaseQuantityOutput :Driver<String> =
                   input.increaseQuantityTrigger.withLatestFrom(inputAddtoCart)
                       .map { (arg0) -> ModelForIncreaseCondition  in
                           
                       let (product, String) = arg0
                           print(product.stock?.stockLevel ?? 0.0)
                       return ModelForIncreaseCondition(maxValue: product.stock?.stockLevel, quantityValue: String)
                       
                   }.flatMapLatest {  [] in

                       if ( (Int($0.maxValue ?? 1)) > (Int($0.quantityValue ?? "1") ?? 1 ))
                       {
                           return    Driver.just(String((Int($0.quantityValue ?? "0") ?? 0) + 1))
                           }else {
                            return  Driver.just($0.quantityValue ?? "1")
                       }
               }


        return Output(increaseQuantityOutput: increaseQuantityOutput, decreseQuantityOutput: decreaseQuantityOutput)
    }

}
