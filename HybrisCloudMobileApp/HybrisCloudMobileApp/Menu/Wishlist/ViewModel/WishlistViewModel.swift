//
//  WishlistViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/26/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import SwipeCellKit
import KeychainSwift
import RxSwift
import RxCocoa


class WishlistViewModel : ViewModelType{
    var deletePublishSubject = PublishSubject<WishListWishlistEntry>()
    var addPublishSubject =  PublishSubject<WishListWishlistEntry>()
    var quantityPublishSubject =  PublishSubject<String>()
    var outOfStockPublishSubject =  PublishSubject<Void>()

    var selectedProductCode : String?
    var wishListPk : String?
    
    let bag = DisposeBag()
    let useCase: WishListUseCase
    private let addtoCartUseCase: AddtoCartUseCase
       let cartUseCase: CartUseCase
    
    init(useCase: WishListUseCase,addtoCartUseCase: AddtoCartUseCase , cartUseCase : CartUseCase ) {
        self.useCase = useCase
        self.addtoCartUseCase = addtoCartUseCase
       self.cartUseCase = cartUseCase
        
    }
    
    
    
    struct Input {
        let trigger: Driver<Void>
        let createCartTrigger: Driver<Void>
        let manualAddToCart: Driver<Void>
        let AddAllToCartSubject : Driver<String>
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let wishList: Driver<WishList>
        let delete: Driver<Void>
        let error: Driver<Error>
        let addToCartResult: Driver<AddedToCartModel>
        let createdCart : Driver<CartModel>
        let outOfStock : Driver<Void>
        let addAllToCart : Driver<AddedToCartModel>
    }
    

    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let wishList = input.trigger.flatMapLatest { [unowned self] in
            return self.useCase.getWishList()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        let addAllToCart = input.AddAllToCartSubject.flatMapLatest { pk in
            return self.addtoCartUseCase.addAllToCart(userId: KeychainSwift().get("user") ?? "anonymous", cartId: (KeychainSwift().get("user") ?? "anonymous" == "current") ? "current" : KeychainSwift().get("guid") ?? "", wishlistPK: pk)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        

        let deleteProduct = deletePublishSubject.asDriverOnErrorJustComplete().map { (deletedIteam)  -> AddDeleteWishListItemRequest in
             
           return AddDeleteWishListItemRequest(wishlistPK: deletedIteam.wishlistPK ?? "", productCode: deletedIteam.product?.code ?? "")
             

        }.flatMapLatest{ [unowned self] in
            return self.useCase.deleteWishListIteam(deleteIteamData: $0)
                                          .trackActivity(activityIndicator)
                                          .trackError(errorTracker)
                                          .asDriverOnErrorJustComplete()
         }
         
        let inputAddtoCart = Driver.combineLatest(addPublishSubject.asDriverOnErrorJustComplete(),quantityPublishSubject.asDriverOnErrorJustComplete())
 
        let addToCartResult  = addPublishSubject.asDriverOnErrorJustComplete().withLatestFrom(inputAddtoCart).map { (arg0)  in
                  
                  let (wishList, quantityNumber) = arg0
                  

            let addToCartProductModel = AddToCartProductModel(basePrice: nil, cancellableQuantity: nil, cancelledItemsPrice: nil, configurationInfos: nil, deliveryMode: nil, deliveryPointOfService: nil, entryNumber: nil, product: wishList.product , quantity: Int(quantityNumber), quantityAllocated: nil, quantityCancelled: nil, quantityPending: nil, quantityReturned: nil, quantityShipped: nil, quantityUnallocated: nil, returnableQuantity: nil, returnedItemsPrice: nil, totalPrice: nil, updateable: nil, url: nil)
                  
                  return addToCartProductModel
                  
              } .flatMapLatest { [unowned self] in

                  return self.addtoCartUseCase.addToCart(userId: KeychainSwift().get("user") ?? "anonymous", cartId:  (KeychainSwift().get("user") ?? "anonymous" == "current") ? "current" : KeychainSwift().get("guid") ?? "", entry: $0)
                                .trackActivity(activityIndicator)
                                .trackError(errorTracker)
                      .asDriverOnErrorJustComplete()
              }
              
        
        let addToCartResultManual  = input.manualAddToCart.withLatestFrom(inputAddtoCart).map { (arg0)  in
                         
                         let (wishList, quantityNumber) = arg0
                         

                   let addToCartProductModel = AddToCartProductModel(basePrice: nil, cancellableQuantity: nil, cancelledItemsPrice: nil, configurationInfos: nil, deliveryMode: nil, deliveryPointOfService: nil, entryNumber: nil, product: wishList.product , quantity: Int(quantityNumber), quantityAllocated: nil, quantityCancelled: nil, quantityPending: nil, quantityReturned: nil, quantityShipped: nil, quantityUnallocated: nil, returnableQuantity: nil, returnedItemsPrice: nil, totalPrice: nil, updateable: nil, url: nil)
                         
                         return addToCartProductModel
                         
                     } .flatMapLatest { [unowned self] in

                         return self.addtoCartUseCase.addToCart(userId: KeychainSwift().get("user") ?? "anonymous", cartId:  (KeychainSwift().get("user") ?? "anonymous" == "current") ? "current" : KeychainSwift().get("guid") ?? "", entry: $0)
                                       .trackActivity(activityIndicator)
                                       .trackError(errorTracker)
                             .asDriverOnErrorJustComplete()
                     }
                     
        let addToCart = Driver.merge(addToCartResult,addToCartResultManual)
        
        let createCart = input.createCartTrigger.flatMapLatest { [unowned self] in
                   return self.cartUseCase.creatCart(userId: KeychainSwift().get("user") ?? "anonymous")
                       .trackActivity(activityIndicator)
                                  .trackError(errorTracker)
                                  .asDriverOnErrorJustComplete()

               }
                      
        let outOfStockResult = outOfStockPublishSubject.asDriverOnErrorJustComplete()
        

        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching, wishList: wishList, delete: deleteProduct, error: errors, addToCartResult: addToCart, createdCart: createCart,outOfStock:outOfStockResult, addAllToCart: addAllToCart)
        
    }

    func dataSource() ->
         RxTableViewSectionedAnimatedDataSource<WishListSection> {
         
                 return RxTableViewSectionedAnimatedDataSource<WishListSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .bottom),configureCell: { ds, cv, indexPath, item in
                
            
                      guard let cell = cv.dequeueReusableCell(withIdentifier: "WishListIteamCell", for: indexPath) as? WishListIteamCell else { return UITableViewCell()}
               
                    cell.delegate = self
                
                    cell.setUp(wishListProduct: item)

                    cell.quantitySubject.subscribe(onNext: { (quantity) in
                        self.quantityPublishSubject.onNext(quantity)
                        }).disposed(by: DisposeBag())

                    
                 
                 return cell
                 
             })
     }
     
    
}

extension WishlistViewModel: SwipeTableViewCellDelegate {
    
     func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]?
    {
        guard let cell :WishListIteamCell? = tableView.cellForRow(at: indexPath) as! WishListIteamCell else { return nil}
    
        guard let  productSelected = try? tableView.rx.model(at: indexPath) as WishListSection.Item else {
            return []
        }
        let addToCartBtn = SwipeAction(style: .default, title: "Add To Cart".localized()) {
               action, index in
               
            let outOfStock : Bool = try! cell?.isOutOfStockSubject.value() ?? false
            
            if(outOfStock)
            {
                self.outOfStockPublishSubject.onNext(())
            }else {
            let quantityValue : String = try! cell?.quantitySubject.value() ?? "0"
            self.quantityPublishSubject.onNext(quantityValue)
            self.addPublishSubject.onNext(productSelected)

           }
        }
           addToCartBtn.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.7333333333, blue: 0.2, alpha: 1)
           
           addToCartBtn.image = UIImage(named: "addTocart")
        
        let deleteBtn = SwipeAction(style: .default, title: "Delete".localized()) { action, index in
         
            self.selectedProductCode = productSelected.product?.code ?? ""
            self.wishListPk =  productSelected.wishlistPK ?? ""
            
            self.deletePublishSubject.onNext(productSelected)
        }
        
        deleteBtn.backgroundColor = .red
        deleteBtn.image = UIImage(named: "delete")
        
        return [deleteBtn,addToCartBtn]
    }
}
