//
//  WishListSection.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/26/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import RxSwift
import RxCocoa
import RxDataSources

struct WishListSection {
    var header: String
    var items: [Item]
    
}

extension WishListSection : AnimatableSectionModelType, Equatable {
    typealias Item = WishListWishlistEntry

    var identity: String {
        return header
    }

    init(original: WishListSection, items: [WishListWishlistEntry]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: WishListSection, rhs: WishListSection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension WishListWishlistEntry: IdentifiableType, Equatable {
    public var identity: String {
        return  product?.code ?? ""
    }
    
    public static func == (lhs: WishListWishlistEntry, rhs: WishListWishlistEntry) -> Bool {
        return (lhs.product?.code  == rhs.product?.code)
    }
}

