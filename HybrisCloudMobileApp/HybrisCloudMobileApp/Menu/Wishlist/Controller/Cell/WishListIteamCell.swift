//
//  WishListIteamCell.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/26/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import SwipeCellKit
import RxKingfisher
import Kingfisher

class WishListIteamCell: SwipeTableViewCell  {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var qtyLbl: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var pluseButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var outOfStockLbl: UILabel!
    @IBOutlet weak var itemQuantityView: UIView!

//   var quantitySubject = PublishSubject<String>()
    var quantitySubject = BehaviorSubject<String>(value: "1")
    var isOutOfStockSubject = BehaviorSubject<Bool>(value: false)

   let product = PublishSubject<ProductDetails>()
    var disposeBag = DisposeBag()
    
    var viewModel : WishListItemViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewModel = WishListItemViewModel()
        let input = WishListItemViewModel.Input(quantity: quantitySubject.asObserver().asDriverOnErrorJustComplete(), products: product.asObserver().asDriverOnErrorJustComplete(), increaseQuantityTrigger: pluseButton.rx.tap.asDriver(), decreaseQuantityTrigger: minusButton.rx.tap.asDriver())
        
         let output = viewModel.transform(input: input)
        
        output.increaseQuantityOutput.asObservable().subscribe( onNext:
                  {
                      (quantity) in
                      self.quantitySubject.onNext(quantity)
                      
              }, onError:
                  {
                      (error) in
                      print(error)
                      
              }, onCompleted:
              {}) {}.disposed(by: disposeBag)
              
              output.decreseQuantityOutput.asObservable().subscribe( onNext:
                  {
                      (quantity) in
                      
                      self.quantitySubject.onNext(quantity)
                      
              }, onError:
                  {
                      (error) in
                      print(error)
                      
              }, onCompleted:
              {}) {}.disposed(by: disposeBag)
              
         quantitySubject.bind(to: qtyLbl.rx.text).disposed(by: disposeBag)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
     }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.disposeBag = DisposeBag()
    }
    
 
    func setUp(wishListProduct:  WishListWishlistEntry){
 
        product.onNext(wishListProduct.product!)
 
        name.text = wishListProduct.product?.name?.html2AttributedString ?? ""
        code.text = wishListProduct.product?.code ?? ""
        price.text = wishListProduct.product?.price?.formattedValue ?? ""
        
        
        let productImage =  wishListProduct.product?.images.map({ (productImage) -> [ProducttImageElement] in
            return productImage.filter ({ (image) -> Bool in
                image.format == "zoom"
                
            })
        })


        self.img?.downloadImageInCellWithoutCorner(urlString: productImage?.first?.url ?? "")


        if wishListProduct.product?.stock?.stockLevel ?? 0 <= 0{
          outOfStockLbl.isHidden = false
          itemQuantityView.isHidden = true
            isOutOfStockSubject.onNext(true)

        }
        else {
            outOfStockLbl.isHidden = true
            itemQuantityView.isHidden = false
            isOutOfStockSubject.onNext(false)

        }
    }
}

