//
//  WishlistViewController.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import KeychainSwift
import FirebaseAnalytics

class WishlistViewController: UIViewController {
    @IBOutlet weak var addAllToCartButton: DesignableButton!
    @IBOutlet weak var tableView: UITableView!{didSet{tableView.tableFooterView = UIView.init(frame: CGRect.zero)}}
    
    @IBOutlet weak var emptyView: UIView!
    
    let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
    let createCartSubject = PublishSubject<Void>()
      let manualAddToCartSubject = PublishSubject<Void>()
    let AddAllToCartSubject = PublishSubject<String>()
    let disposeBag = DisposeBag()
    var wishlistViewModel : WishlistViewModel!
    var pk = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        preparation()
     
    }

    func preparation(){
        wishlistViewModel = .init(useCase: networkUseCaseProvider.makeWishListUseCase(), addtoCartUseCase:networkUseCaseProvider.makeAddToCartUseCase(), cartUseCase: networkUseCaseProvider.makeCartUseCase())
        
 
        
          let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
                    .mapToVoid()
                    .asDriverOnErrorJustComplete()
              
        

        
        let input = WishlistViewModel.Input(trigger: viewWillAppear,createCartTrigger: createCartSubject.asDriverOnErrorJustComplete() , manualAddToCart : manualAddToCartSubject.asDriverOnErrorJustComplete(), AddAllToCartSubject: AddAllToCartSubject.asDriverOnErrorJustComplete())
        
        let output = wishlistViewModel.transform(input: input)
        
      
        output.addToCartResult.asObservable().subscribe(onNext: { (AddedToCartModel) in

            self.successLoaf(message: "product added to cart successfully".localized())
            
            let quantity = (Double(AddedToCartModel.quantity ?? 0) )
            let price = AddedToCartModel.entry?.basePrice?.value ?? 0.0
            let total = quantity * price
            let itemDetails: [String: Any] = [
                AnalyticsParameterCurrency: AddedToCartModel.entry?.basePrice?.currencyISO ?? "",
                AnalyticsParameterValue:  total,
                AnalyticsParameterItemID: AddedToCartModel.entry?.product?.code ?? "" ,
                AnalyticsParameterItemName: AddedToCartModel.entry?.product?.name ?? "",
                AnalyticsParameterPrice: price
            ]
            
            Analytics.logEvent(AnalyticsEventAddToCart, parameters: itemDetails)
                
           
        }).disposed(by: self.disposeBag)
        
        output.addAllToCart.asObservable().subscribe(onNext: {(data) in
            self.successLoaf(message: "products added to cart successfully".localized())
        }).disposed(by: disposeBag)
        
        output.createdCart.asObservable().subscribe(onNext: { (CartModel) in
                   KeychainSwift().set(CartModel.guid ?? "", forKey: "guid")
                   self.manualAddToCartSubject.onNext(())
                  }).disposed(by: self.disposeBag)
        
        output.error.asObservable().subscribe(onNext: { (Error) in
                 let error = Error as? ErrorResponse
                 if error?.errors?.first?.type == "CartError" {
                     self.createCartSubject.onNext(())
                     }

             }).disposed(by: self.disposeBag)
        
        output.wishList.map { (wishList) -> [WishListSection] in
            self.pk = wishList.pk ?? ""
            return [WishListSection.init(header: "", items: wishList.wishlistEntries ?? [])]
            }.asObservable().bind(to: tableView.rx.items(dataSource: self.wishlistViewModel.dataSource())).disposed(by: disposeBag)

        output.wishList.map { (wishList) -> Bool in
          return wishList.wishlistEntries?.count ?? 0 != 0
        }.asObservable().bind(to:self.emptyView.rx.isHidden).disposed(by: disposeBag)

        output.wishList.map { (wishList) -> Bool in
                        wishList.wishlistEntries?.count ?? 0 == 0
             }.asObservable().bind(to:self.tableView.rx.isHidden).disposed(by: disposeBag)
        
        output.wishList.map { (wishList) -> Bool in
                       return wishList.wishlistEntries?.count ?? 0 == 0
             }.asObservable().bind(to:self.addAllToCartButton.rx.isHidden).disposed(by: disposeBag)
        
        addAllToCartButton.rx.tap.subscribe(onNext: { (Void) in
            self.AddAllToCartSubject.onNext(self.pk)
        }).disposed(by: disposeBag)
        
        output.delete.asObservable().subscribe(onNext: { _ in
              self.viewWillAppear(true)
            }).disposed(by: disposeBag)
        output.outOfStock.asObservable().subscribe(onNext: { (arg0) in
            
            let () = arg0
            self.errorLoaf(message: "Out Of Stock".localized())
            }).disposed(by: disposeBag)

    
    }
    
}
