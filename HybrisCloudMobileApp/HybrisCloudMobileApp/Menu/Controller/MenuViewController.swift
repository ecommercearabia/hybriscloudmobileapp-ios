//
//  UserViewController.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/21/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import KeychainSwift
import RappleProgressHUD


class MenuViewController: UIViewController, UIScrollViewDelegate {
    
    enum sectionsMenu{
        case myAccount
        case ourStore
        case appSettings
        case logout
    }
    enum myAccountItemsMenue{
        case emailAddress
        case changePassword
        case deliveryAddress
        case loyaltyPoints
        case storeCredit
        case personalDetails
        case referralCode
        case mangeSavedCards
        case consents
    }
    enum ourStoreItemsMenue{
        case aboutUs
        case call
        case contactUs
        
        
    }
    enum settingsItemsMenue{
        case settings
    }
    
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var signUpLabel: UILabel!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var privacyPolicyButton: UIButton!
    @IBOutlet weak var termsAndConditionsButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var instgramButton: UIButton!
    @IBOutlet weak var whatsappBuuton: UIButton!
    
    let bag = DisposeBag()
    var menuUserFlag : Bool = false
    var viewModel = MenuViewModel(userUseCase: NetworkPlatform.UseCaseProvider().makeUserUseCase(), consentsListUseCase: NetworkPlatform.UseCaseProvider().makeConsentsList(), configUseCase: NetworkPlatform.UseCaseProvider().makeConfig())
    var sectionMenu: [sectionsMenu]?
    var myAccountMenu: [myAccountItemsMenue] = [.emailAddress , .changePassword , .deliveryAddress , .loyaltyPoints , .storeCredit , .personalDetails, .referralCode , .mangeSavedCards , .consents]
    var ourStoreMenu: [ourStoreItemsMenue] = [.aboutUs , .call , .contactUs]
    var settingsMenu: [settingsItemsMenue] = [ .settings]
    var needToShowFaceId = false
    let logoutManual = PublishSubject<Void>()
    let setFaceIdSubject = PublishSubject<Void>()
    var isNationalityRequired =   false
    override func viewDidLoad() {
        super.viewDidLoad()
    
       
        
       
     
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
              .mapToVoid()
              .asDriverOnErrorJustComplete()

        let input = MenuViewModel.Input( setFaceIDTrigger : setFaceIdSubject.asDriverOnErrorJustComplete() , trigger: viewModel.getManual.asDriverOnErrorJustComplete(), triggerForConfig: viewWillAppear , addTrigger: viewModel.addManual.asDriverOnErrorJustComplete() , deleteTrigger: viewModel.deleteManual.asDriverOnErrorJustComplete() , id: viewModel.id.asDriverOnErrorJustComplete() , version: viewModel.version.asDriverOnErrorJustComplete() )
        let output = viewModel.transform(input: input)
        output.confarmFaceId.asObservable().subscribe(onNext: { (void) in
            KeychainSwift().set(true, forKey: "SetFaceID")
            
        }).disposed(by: bag)
        output.getConsents.asObservable().subscribe(onNext: {(data) in
           
            var list = self.viewModel.UserFirstList.filter { (menu) -> Bool in
                return menu.labelText != "Consents"
            }
            
            
            if data.consentTemplates?.count != 0 {
             
                
                list.insert(MenuItem(iconImageName:#imageLiteral(resourceName: "Consent"), fromLabelText: "Consents", disclosureType: UITableViewCell.AccessoryType.disclosureIndicator, consentsList: data.consentTemplates ?? []), at: list.endIndex)
                self.viewModel.UserFirstList = list
               self.viewModel.menuDataList.onNext(self.viewModel.getUserMenu())
             
            } else {
                self.setup()
            }
           
        }).disposed(by: bag)
        output.addConsents.asObservable().subscribe(onNext: {(data  ) in
            self.successLoaf(message: "Added".localized())
           
           self.viewModel.getManual.onNext(())
           
        }).disposed(by: bag)
        
        output.error.asObservable().subscribe(onNext: { (error) in
            if (KeychainSwift().get("user") == "current") {
            let errorInModel = error as? ErrorResponse
            let  msgError =  errorInModel?.errors?.map({ (errorResponseElement) -> String in
                          return errorResponseElement.message ?? ""
                      }).joined(separator: " , ")
            self.errorLoaf(message: msgError ?? "" )
            
            self.viewModel.menuDataList.onNext(self.viewModel.getUserMenu())
            }
            }).disposed(by: bag)

        output.deleteConsents.asObservable().subscribe(onNext: {(data  ) in
            self.successLoaf(message: "Deleted".localized())
            
          self.viewModel.getManual.onNext(())
           
        }).disposed(by: bag)
        
        output.config.asObservable().subscribe(onNext: { (configration) in

            if configration.registrationConfiguration?.nationalityConfigurations?.enabled  == true {
              
                if configration.registrationConfiguration?.nationalityConfigurations?.configurationsRequired == true {
                    self.isNationalityRequired = true
                } else {
                    self.isNationalityRequired = false
                }
            }
        
        }).disposed(by: bag)
        
        self.viewModel.menuDataList.bind(to: menuTableView.rx.items(dataSource: self.viewModel.dataSource())).disposed(by: bag)
      
        
        menuTableView.rx.itemSelected
            .subscribe(onNext: { [unowned self] indexPath in
                let selectedSection = self.sectionMenu?[indexPath.section]
                if (KeychainSwift().get("user") == "current") {
                  
                    switch selectedSection{
                    
                    case .myAccount :
                        switch self.myAccountMenu[indexPath.row] {
                        case .emailAddress:
                            self.performSegue(withIdentifier: "goToEmailAddress", sender: nil)
                        case .changePassword:
                            self.performSegue(withIdentifier: "goToChangePassword", sender: nil)
                        case .deliveryAddress:
                            self.performSegue(withIdentifier: "goToAddress", sender: nil)
                        case .loyaltyPoints :
                            self.performSegue(withIdentifier: "goLoyaltyPoints", sender: nil)
                        case .storeCredit:
                            self.performSegue(withIdentifier: "goToStoreCredit", sender: nil)
                        case .personalDetails:
                            self.performSegue(withIdentifier: "goToPersonalDetails", sender: nil)
                        case .referralCode:
                            self.performSegue(withIdentifier: "goToReferralCode", sender: nil)
                            
                        case .mangeSavedCards:
                            self.performSegue(withIdentifier: "goToMangeSavedCards", sender: nil)
                        case .consents:
                            
                            break
                        }
                        
                    case .ourStore :
                        
                        switch  self.ourStoreMenu[indexPath.row]{
                        case .aboutUs:
//                            self?.openUrl(url: "https://www.foodcrowd.com/en/about-us")
                            self.performSegue(withIdentifier: "goToAboutUs", sender: nil)

                            
                        case .call:
                            if let url: NSURL = URL(string: "TEL://80036632") as NSURL? {
                                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                            }
                        case .contactUs:
//                            self?.openUrl(url: "https://foodcrowd.com/foodcrowd-ae/en/contact-us")
                            self.performSegue(withIdentifier: "goToContactUs", sender: nil)
                         }
                        
                        
                    case .appSettings:
                        switch self.settingsMenu[indexPath.row]{
                        case .settings:
                            self.performSegue(withIdentifier: "goToSettings", sender: nil)
                         }
                    case .logout:
                        logout()
                    default:
                        break
                    }
                    
                }else{
                    switch selectedSection{
                        
                    case .ourStore :
                        
                        switch self.ourStoreMenu[indexPath.row] {
                        case .aboutUs :
//                            self?.openUrl(url: "https://www.foodcrowd.com/en/about-us")
                            self.performSegue(withIdentifier: "goToAboutUs", sender: nil)

                        case .call:
                            if let url: NSURL = URL(string: "TEL://80036632") as NSURL? {
                                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                            }
                        case .contactUs:
                            self.performSegue(withIdentifier: "goToContactUs", sender: nil)

//                            self?.openUrl(url: "https://foodcrowd.com/foodcrowd-ae/en/contact-us")
                         }
                        
                    case .appSettings:
                        switch self.settingsMenu[indexPath.row]{
                        case .settings:
                            self.performSegue(withIdentifier: "goToSettings", sender: nil)
                            
                            
                         }
                        
                    default:
                        break
                    }
                }
            }).disposed(by: bag)
        
        
        

        
        self.loginButton.rx.tap.subscribe(onNext: { (Void) in
            if self.menuUserFlag {
                self.performSegue(withIdentifier: "goToWishlist", sender: nil)
            }
            else {
                self.performSegue(withIdentifier: "goToLogin", sender: nil)
            }
        }).disposed(by: bag)
        
        self.signupButton.rx.tap.subscribe(onNext: { (Void) in
            if self.menuUserFlag {
                self.performSegue(withIdentifier: "goToMyOrders", sender: nil)
                
            }
            else {
                self.performSegue(withIdentifier: "goToSignUp", sender: nil)
            }
        }).disposed(by: bag)
        
        facebookButton.rx.tap.subscribe(onNext: { (Void) in
            self.openUrl(url: "https://www.facebook.com/foodcrowduae/")
        }).disposed(by: bag)
        
        instgramButton.rx.tap.subscribe(onNext: { (Void) in
            self.openUrl(url: "https://www.instagram.com/foodcrowduae/")
        }).disposed(by: bag)
        
        whatsappBuuton.rx.tap.subscribe(onNext:{ (Void) in
            self.openWhatsapp()
            }).disposed(by: bag)
        privacyPolicyButton.rx.tap.subscribe(onNext: { (Void) in
//            self.openUrl(url: "https://foodcrowd.com/foodcrowd-ae/en/privacy-policy")
            
            self.performSegue(withIdentifier: "goToPrivacyPolicy", sender: nil)

            
        }).disposed(by: bag)
        
        termsAndConditionsButton.rx.tap.subscribe(onNext: { (Void) in
//            self.openUrl(url: "https://prod.foodcrowd.com/foodcrowd-ae/en/termsAndConditions")
            
            self.performSegue(withIdentifier: "goToTermsAndConditions", sender: nil)

            
        }).disposed(by: bag)
        
      
    }
    
    func logout(){
        let setFaceID = KeychainSwift().getBool("SetFaceID") ?? false
        
            let secItemClasses = [kSecClassGenericPassword, kSecClassInternetPassword, kSecClassCertificate, kSecClassKey, kSecClassIdentity]
            for itemClass in secItemClasses {
                let spec: NSDictionary = [kSecClass: itemClass]
                 SecItemDelete(spec)
            }
            
 
        NetworkPlatform.UseCaseProvider().makeUserUseCase().logOut().subscribe { (void) in
            print("log out")
        } onError: { (error) in
 
            print(error)
        } onCompleted: {
            print("log out")
            NetworkPlatform.UseCaseProvider().makeCatalogUseCase().categories(id: "foodcrowdProductCatalog", version: "Online").subscribe(onNext: { (test) in
                print("")
            }).disposed(by: self.bag)
        } onDisposed: {
            print("onDisposed")
        }.disposed(by: bag)

        
            
            KeychainSwift().clear()
            KeychainSwift().set("anonymous", forKey: "user")
        KeychainSwift().set(setFaceID, forKey: "SetFaceID")
            self.sectionMenu?.removeAll()
            self.viewWillAppear(true)

 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RappleActivityIndicatorView.stopAnimation()

        if self.needToShowFaceId {
            if   !(KeychainSwift().getBool("SetFaceID") ?? false)  {
                 let iDType = KeychainSwift().get("IdType")
                let alert = UIAlertController(title: KeychainSwift().get("IdType")?.localized(), message: "are you need to use".localized() + " \(iDType?.localized() ?? "") " + "to login?".localized(), preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "Yes".localized(), style: .default, handler: { _ in
                            self.setFaceIdSubject.onNext(())

                            }))
                alert.addAction(UIAlertAction(title: "Close".localized(), style: .default ))


                           self.present(alert, animated: true)
                       }
            
            
            self.needToShowFaceId = false
        }
        if (KeychainSwift().get("user") == "current") {
        viewModel.getManual.onNext(())
        }
        setup()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToLogin" {
            if let nav = segue.destination as? UINavigationController {
                if let vc = nav.viewControllers.first as? LoginViewController {
                    vc.menuVC = self
                }
            }
        }
        if segue.identifier == "goToSignUp" {
            if let nvc = segue.destination as? UINavigationController {
                if let vc = nvc.viewControllers.first as? SignUpViewController {
                    vc.isNationalityRequired = self.isNationalityRequired
                }
            }
        }
        
        
    }
    
    func setup(){
        if (KeychainSwift().get("user") == "current") {
            menuUserFlag = true
            signUpLabel.text = "My Orders".localized()
            loginLabel.text = "Wishlist".localized()
            
            loginButton.setImage(UIImage(named: "wishlist-up"), for: .normal)
            signupButton.setImage(UIImage(named: "orders-up"), for: .normal)
            sectionMenu = [.myAccount ,.ourStore, .appSettings , .logout]
              viewModel.menuDataList.onNext(viewModel.getUserMenu())
        }else{
            signUpLabel.text = "Sign Up".localized()
            loginLabel.text = "Login".localized()
            menuUserFlag = false
            loginButton.setImage(UIImage(named: "Login"), for: .normal)
            signupButton.setImage(UIImage(named: "Siginup"), for: .normal)
            viewModel.menuDataList.onNext(viewModel.getGuestMenu())
            sectionMenu = [.ourStore, .appSettings ]


        }
        print("dff")
    }
    func openUrl(url : String){
        guard let url = URL(string: url) else { return }
        UIApplication.shared.open(url)
    }
    func openWhatsapp(){
        
//        https://api.whatsapp.com/send?phone=97180036632&text=Hi!
        let url = "whatsapp://send?phone=97180036632&text=Hi!"
        if let urlString = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    UIApplication.shared.open(whatsappURL)
                } else {
                    print("Install Whatsapp")
                }
            }
        }
    }
}

