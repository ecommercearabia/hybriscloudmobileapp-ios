//
//  ConsentsListCell.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 9/9/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import UIKit

class ConsentsListCell: UITableViewCell, UIScrollViewDelegate {

    @IBOutlet weak var ConsentsListTable: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imageConsents: UIImageView!
    var viewModel : MenuViewModel!
    var bag = DisposeBag()
    var ConsentsSectionBehaviorSubject = BehaviorSubject<[ConsentsListModel]>(value: [])
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.bag = DisposeBag()
    }
    
    func setup( consentsList : [ConsentTemplate] , viewModel : MenuViewModel){
        self.viewModel = viewModel
        nameLbl.text = "Consents Management".localized()
        imageConsents.image = #imageLiteral(resourceName: "Consent")
        self.ConsentsSectionBehaviorSubject.bind(to: ConsentsListTable.rx.items(dataSource: self.viewModel.dataSourceForConsents())).disposed(by: bag)
        self.ConsentsListTable.rx.setDelegate(self).disposed(by: bag)
        
        if consentsList.count != 0 {
            tableHeight.constant = CGFloat(consentsList.count * 100)
            self.ConsentsSectionBehaviorSubject.onNext([ConsentsListModel(header: "", items: consentsList ?? [] )])
            
        } else {
            tableHeight.constant = 0
            self.ConsentsSectionBehaviorSubject.onNext([ConsentsListModel(header: "", items:   [] )])
               }
        }
    }
