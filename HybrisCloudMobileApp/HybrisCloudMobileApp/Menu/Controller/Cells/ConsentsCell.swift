//
//  ConsentsCell.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 9/9/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import UIKit

class ConsentsCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var decLbl: UILabel!
    
    @IBOutlet weak var switchConsent: UISwitch!
    var bag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.bag = DisposeBag()
    }
    
}
