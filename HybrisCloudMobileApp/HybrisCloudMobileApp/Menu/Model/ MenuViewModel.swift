//
//   MenuViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import RxSwift
import RxDataSources
import KeychainSwift
import Foundation
import FCUUID

class MenuViewModel  : ViewModelType {
    let deleteManual = PublishSubject<String>()
    let addManual = PublishSubject<Void>()
    let getManual = PublishSubject<Void>()
    let id = PublishSubject<String>()
    
    let version = PublishSubject<Int>()
    struct Input {
        let  setFaceIDTrigger : Driver<Void>
        let  trigger : Driver<Void>
        let  triggerForConfig : Driver<Void>
        let  addTrigger : Driver<Void>
        let  deleteTrigger : Driver<String>
        let  id : Driver<String>
        let  version : Driver<Int>
        
    }
    
    struct Output {
        
        let confarmFaceId : Driver<Void>
        let error: Driver<Error>
        let fetching: Driver<Bool>
        let  getConsents: Driver<Consents>
        let  addConsents: Driver<AddConsent>
        let  deleteConsents: Driver<Void>
        let config : Driver<ConfigModel>
    }
    let bag = DisposeBag()
    private let userUseCase: UserUseCase
    private let consentsListUseCase: ConsentsListUseCase
    private let configUseCase: ConfigUseCase
    init(userUseCase: UserUseCase , consentsListUseCase: ConsentsListUseCase , configUseCase: ConfigUseCase) {
        self.userUseCase = userUseCase
        self.consentsListUseCase = consentsListUseCase
        self.configUseCase = configUseCase
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let config = input.triggerForConfig.flatMapLatest{
            return self.configUseCase.config()
                .trackError(errorTracker)
                    .trackActivity(activityIndicator)
                    .asDriverOnErrorJustComplete()

        }
        
        let setFaceID = input.setFaceIDTrigger.flatMapLatest{
            return self.userUseCase.setOrUpdateFaceId(signatureId: FCUUID.uuidForDevice() ?? "", userId: "current")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriver(onErrorDriveWith: Driver<()>.just(()))
        }
        
        let getConsentsList = input.trigger.flatMapLatest{ return self.consentsListUseCase.getConsentsList()
            .trackActivity(activityIndicator)
            .trackError(errorTracker)
            .asDriverOnErrorJustComplete()
            
        }
        
        
   
            
        
        let consentValue = Driver.combineLatest(input.id, input.version)
        let addConsent = input.addTrigger.withLatestFrom(consentValue).flatMapLatest{ (arg0) -> Driver<AddConsent> in
            let (id , version) = arg0
            return self.consentsListUseCase.addConsent(consentTemplateId: id, Version: version)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let deleteConsent = input.deleteTrigger.flatMapLatest{ (id) in
            return self.consentsListUseCase.deleteConsent(code: id)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(confarmFaceId: setFaceID , error: errors , fetching : fetching  , getConsents: getConsentsList , addConsents: addConsent , deleteConsents: deleteConsent, config: config  )
        
    }
    
    var menuDataList = BehaviorSubject<[MenuModel]>(value: [MenuModel(section: MenuGroupSection.init(fromName: ""), items: [])])
    
    func dataSourceForConsents() -> RxTableViewSectionedAnimatedDataSource<ConsentsListModel> {
        return
            RxTableViewSectionedAnimatedDataSource<ConsentsListModel>(animationConfiguration: AnimationConfiguration(insertAnimation: .fade, reloadAnimation: .fade, deleteAnimation: .fade),configureCell: { ds, tv, indexPath, item in
                
                guard let cell = tv.dequeueReusableCell(withIdentifier: "ConsentsCell", for: indexPath) as? ConsentsCell else {
                    return UITableViewCell()
                }
                 if item.currentConsent?.consentWithdrawnDate == nil {
                    cell.switchConsent.isOn = true
                } else {
                    cell.switchConsent.isOn = false
                }
                
                cell.switchConsent.rx.controlEvent(.valueChanged).withLatestFrom(cell.switchConsent.rx.value).subscribe(onNext: {(value) in
                    if value == true {
                        self.id.onNext(item.id ?? "")
                        self.version.onNext(item.version ?? 0)
                        self.addManual.onNext(())
                        
                    } else {
                        self.deleteManual.onNext(item.currentConsent?.code ?? "")
                    }
                    
                }).disposed(by: cell.bag)
                
                
                 cell.titleLbl.text = item.name ?? ""
                cell.decLbl.text = item.consentTemplateDescription ?? ""
                return cell
            })
        
    }
    func dataSource() ->  RxTableViewSectionedAnimatedDataSource<MenuModel> {
        return RxTableViewSectionedAnimatedDataSource<MenuModel>(animationConfiguration: AnimationConfiguration(insertAnimation: .none, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, tv, indexPath, item in
           
            if (KeychainSwift().get("user") == "current") {
               
               
               
                if item.labelText == "Consents" {
                    guard let cell = tv.dequeueReusableCell(withIdentifier: "ConsentsListCell", for: indexPath) as? ConsentsListCell else { return UITableViewCell()}
                   
                    cell.setup(consentsList: item.consentsList ?? [] , viewModel : self)
                    cell.layoutIfNeeded()
                    return cell
                }
                
                else {
                    
                
                guard let cell = tv.dequeueReusableCell(withIdentifier: "GuestTableViewCell", for: indexPath) as? GuestTableViewCell else { return UITableViewCell()}
                
                if indexPath == IndexPath(row: 0, section: 3){
                    cell.TextLabel.text = item.labelText
                    cell.TextLabel.textColor = .red
                    cell.GoImageView.isHidden = true
                    cell.lfetImageView.isHidden = true
                    
                }else{
                    cell.GoImageView.isHidden = false
                    cell.lfetImageView.isHidden = false
                    cell.TextLabel.text = item.labelText
                    cell.lfetImageView.image = item.iconImage
                    cell.TextLabel.textColor = .darkText
                    
                    
                }
                
                
                return cell
                }
            }else{
                guard let cell = tv.dequeueReusableCell(withIdentifier: "GuestTableViewCell", for: indexPath) as? GuestTableViewCell else { return UITableViewCell()}
                cell.TextLabel.text = item.labelText
                cell.lfetImageView.image = item.iconImage
                
                
                return cell
            }
            
            
            
        }
        , titleForHeaderInSection: { (ds, sectionIndex) -> String? in
            return ds.sectionModels[sectionIndex].section.name
        }
        
        
        ) { (ds, title, sectionIndex) -> Int in
            return 0
        }
    }
    
    
    
    var menuFisrtSharedList: [MenuItem] = [
        MenuItem(iconImageName: #imageLiteral(resourceName: "AboutIcon"),fromLabelText: "About Us".localized(), disclosureType: UITableViewCell.AccessoryType.disclosureIndicator, consentsList: []),
        MenuItem(iconImageName: #imageLiteral(resourceName: "callIcon"),fromLabelText: "800 FoodC (36632)".localized(), disclosureType: UITableViewCell.AccessoryType.disclosureIndicator, consentsList: []),
        MenuItem(iconImageName: #imageLiteral(resourceName: "contactIcon"),fromLabelText: "Contact Us".localized(), disclosureType: UITableViewCell.AccessoryType.disclosureIndicator , consentsList: [])
    ]
    var menuSecondSharedList: [MenuItem] = [
        MenuItem(iconImageName: #imageLiteral(resourceName: "settingIcon"),fromLabelText: "Settings".localized(), disclosureType: UITableViewCell.AccessoryType.disclosureIndicator , consentsList:[])
    ]
    
    var UserFirstList: [MenuItem] = [
        MenuItem(iconImageName: #imageLiteral(resourceName: "contactIcon"),fromLabelText: "Email Address".localized(), disclosureType: UITableViewCell.AccessoryType.disclosureIndicator, consentsList: []),
        MenuItem(iconImageName: #imageLiteral(resourceName: "Password"),fromLabelText: "Change Password".localized(), disclosureType: UITableViewCell.AccessoryType.disclosureIndicator, consentsList: []),
        
        MenuItem(iconImageName: #imageLiteral(resourceName: "Delivery"),fromLabelText: "Delivery Address".localized(), disclosureType: UITableViewCell.AccessoryType.disclosureIndicator, consentsList: []),
        MenuItem(iconImageName: #imageLiteral(resourceName: "Store Credit"),fromLabelText: "Loyalty Points".localized(), disclosureType: UITableViewCell.AccessoryType.disclosureIndicator, consentsList: []),
        MenuItem(iconImageName: #imageLiteral(resourceName: "Store Credit"),fromLabelText: "Store Credit".localized(), disclosureType: UITableViewCell.AccessoryType.disclosureIndicator, consentsList:[]),
        MenuItem(iconImageName: #imageLiteral(resourceName: "Store Credit"),fromLabelText: "Personal Details".localized(), disclosureType: UITableViewCell.AccessoryType.disclosureIndicator, consentsList: []),
        MenuItem(iconImageName: #imageLiteral(resourceName: "referal_code_icon"), fromLabelText: "Referral Code".localized(), disclosureType: UITableViewCell.AccessoryType.disclosureIndicator, consentsList:[]),
        MenuItem(iconImageName: #imageLiteral(resourceName: "Store Credit"),fromLabelText: "Mange Saved Cards".localized(), disclosureType: UITableViewCell.AccessoryType.disclosureIndicator, consentsList:[]),
    
        
    ]
    
    
    var LogOutList: [MenuItem] = [
        MenuItem(iconImageName: nil,fromLabelText: "Logout".localized(), disclosureType: UITableViewCell.AccessoryType.none, consentsList: [])
    ]
    
    func getGuestMenu() -> [MenuModel] {
    
        let loggedInMenu: [MenuModel] = [
            MenuModel(section: MenuGroupSection(fromName: "Our Store".localized()), items: menuFisrtSharedList),
            MenuModel(section: MenuGroupSection(fromName: "App Setting".localized()), items: menuSecondSharedList)
            
        ]
        return loggedInMenu
    }
    func getUserMenu() -> [MenuModel] {
        
        let loggedOutMenu: [MenuModel] = [
            MenuModel(section: MenuGroupSection(fromName: "My Account".localized()), items: UserFirstList
            ),
            MenuModel(section: MenuGroupSection(fromName: "Our Store".localized()), items: menuFisrtSharedList),
            MenuModel(section: MenuGroupSection(fromName: "App Setting".localized()), items: menuSecondSharedList),
            MenuModel(section: MenuGroupSection(fromName: ""), items: LogOutList)
            
        ]
        return loggedOutMenu
    }
    
    
}

