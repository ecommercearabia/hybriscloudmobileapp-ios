//
//  ConsentsListModel.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 9/9/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import UIKit
import RxDataSources


struct ConsentsListModel {
    var header: String
    var items: [Item]
    
}
extension ConsentsListModel: AnimatableSectionModelType ,Equatable{

    typealias Item = ConsentTemplate
    var identity: String{
        return header
    }
    
    init(original: ConsentsListModel, items: [ConsentTemplate]) {
        self = original
        self.items = items
    }
}


extension ConsentTemplate: IdentifiableType , Equatable {
    public var identity: String {
        return  id  ?? ""
    }
    public static func == (lhs: ConsentTemplate, rhs: ConsentTemplate) -> Bool {
        return  lhs.id == rhs.id && lhs.currentConsent?.consentWithdrawnDate == rhs.currentConsent?.consentWithdrawnDate
           
       
    }
}
