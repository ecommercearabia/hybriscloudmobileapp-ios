//
//   MenuModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import UIKit
import RxDataSources

struct MenuModel {
    var section: MenuGroupSection
    var items: [Item]
    
}

extension MenuModel: AnimatableSectionModelType {
    
    typealias Item = MenuItem
    
    var identity: String { return self.section.name }
    
    init(original: MenuModel, items: [Item]) {
        self = original
        self.items = items
    }
}
extension MenuItem: IdentifiableType, Equatable {
    var identity: String {
        return labelText ?? ""
    }
    
    static func == (lhs: MenuItem, rhs: MenuItem) -> Bool {
        
        if lhs.labelText == "Consents" {
            return false
        }
        else {
            return lhs.labelText == rhs.labelText
        }
        
        
    }
}

struct MenuItem {
    var iconImage: UIImage?
    var labelText: String?
    var disclosureType: UITableViewCell.AccessoryType?
    var consentsList : [ConsentTemplate]?
    init(iconImageName: UIImage?,fromLabelText labelTextValue: String,
         disclosureType disclousure: UITableViewCell.AccessoryType , consentsList : [ConsentTemplate]?) {
        
        self.iconImage = iconImageName
        self.labelText = labelTextValue
        self.disclosureType = disclousure
        self.consentsList = consentsList
    }
}

struct MenuGroupSection {
    var name:String
    
    init(fromName sectionName:String) {
        name = sectionName
    }
}
