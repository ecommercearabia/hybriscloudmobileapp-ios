//
//  LoyaltyPointsVM.swift
//  HybrisCloudMobileApp
//
//  Created by khalil anqawi on 26/05/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation
import Domain
import NetworkPlatform

class LoyaltyPointsVM : ViewModelType{
    private let myLoyaltyPointsUseCase : LoyaltyPointsUseCase
    
    init(myLoyaltyPointsUseCase : LoyaltyPointsUseCase) {
        self.myLoyaltyPointsUseCase = myLoyaltyPointsUseCase
    }

    var myLoyaltyPointsSubject = BehaviorSubject<[LoyaltyPointsSection]>(value: [LoyaltyPointsSection(header: "", items: [])])
    
    struct Input {
         let trigger: Driver<Void>
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let myLoyaltyPoints: Driver<LoyaltyPointsModel>
        let myAvailableLoyaltyPoints: Driver<Double>
        let error: Driver<Error>
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let myAvailableLoyaltyPoints = input.trigger.flatMapLatest {
            [unowned self] in
            return self.myLoyaltyPointsUseCase.getAvailableLoyaltyPoints().trackActivity(activityIndicator).asDriverOnErrorJustComplete()
        }
        let myLoyaltyPoints = input.trigger.flatMapLatest {
            [unowned self] in
            return self.myLoyaltyPointsUseCase.getLoyaltyPoints().trackActivity(activityIndicator).asDriverOnErrorJustComplete()
        }
        
     
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
                     
        return Output(fetching: fetching,
                      myLoyaltyPoints: myLoyaltyPoints, myAvailableLoyaltyPoints: myAvailableLoyaltyPoints,
                                   error: errors)
   
    }
    
    func dataSource() -> RxTableViewSectionedAnimatedDataSource<LoyaltyPointsSection> {
        return RxTableViewSectionedAnimatedDataSource<LoyaltyPointsSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .bottom),configureCell: { ds, cv, indexPath, item in

            guard let cell = cv.dequeueReusableCell(withIdentifier: "LoyaltyPointsCell", for: indexPath)as? LoyaltyPointsCell else{
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.setUp(order: item)
            cell.contentView.backgroundColor = (indexPath.row % 2 == 1) ? UIColor.white : #colorLiteral(red: 0.9490196109, green: 0.9490197301, blue: 0.9490196109, alpha: 1)
            return cell
        })
    }
    
}
