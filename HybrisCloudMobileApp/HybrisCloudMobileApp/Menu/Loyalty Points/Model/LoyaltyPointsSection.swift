//
//  LoyaltyPointsSection.swift
//  HybrisCloudMobileApp
//
//  Created by khalil anqawi on 26/05/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

struct LoyaltyPointsSection {
    var header: String
    var items: [Item]
    
}

extension LoyaltyPointsSection : AnimatableSectionModelType, Equatable {
    typealias Item = LoyaltyPointHistroy

    var identity: String {
        return header
    }

    init(original: LoyaltyPointsSection, items: [LoyaltyPointHistroy]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: LoyaltyPointsSection, rhs: LoyaltyPointsSection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension LoyaltyPointHistroy: IdentifiableType, Equatable {
    public var identity: String {
        return  "\(orderCode ?? "") \(dateOfPurchase ?? "")"
    }
    
    public static func == (lhs: LoyaltyPointHistroy, rhs: LoyaltyPointHistroy) -> Bool { return lhs.orderCode  == rhs.orderCode }
}

