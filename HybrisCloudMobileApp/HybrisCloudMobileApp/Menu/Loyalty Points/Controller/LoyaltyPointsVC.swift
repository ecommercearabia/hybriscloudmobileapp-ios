//
//  LoyaltyPointsVC.swift
//  HybrisCloudMobileApp
//
//  Created by khalil anqawi on 26/05/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import UIKit
import Domain
class LoyaltyPointsVC: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var loyaltyTableView: UITableView!
    @IBOutlet weak var balanceLbl: UILabel!
    let disposeBag = DisposeBag()
    var loyaltyPointsVM : LoyaltyPointsVM!
    override func viewDidLoad() {
        super.viewDidLoad()

        preparation()
        myLoyaltyPointsSetUP()
    }

    func myLoyaltyPointsSetUP(){
        self.loyaltyPointsVM.myLoyaltyPointsSubject.bind(to: loyaltyTableView.rx.items(dataSource: self.loyaltyPointsVM.dataSource())).disposed(by: disposeBag)
        self.loyaltyTableView.rx.setDelegate(self).disposed(by: disposeBag)
        self.loyaltyTableView.rx.modelSelected(LoyaltyPointsSection.Item.self).subscribe(onNext:{(item) in print("order selected")
            self.performSegue(withIdentifier: "toOrdersDetails", sender: item)
            
           
        }).disposed(by: disposeBag)
    }
    func preparation(){
        let  networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        loyaltyPointsVM = LoyaltyPointsVM.init(myLoyaltyPointsUseCase: networkUseCaseProvider.makeLoyaltyPoints())
        let viewWillApper =
            rx.sentMessage(#selector(UIViewController.viewDidAppear(_:)))
        .mapToVoid()
        .asDriverOnErrorJustComplete()
        
        let input = LoyaltyPointsVM.Input(trigger: viewWillApper)
        
        let output = loyaltyPointsVM.transform(input: input)
        
        output.myAvailableLoyaltyPoints.asObservable().subscribe(onNext: {(availableLoyaltyPoints) in
            self.balanceLbl.text =  "Available Balance ".localized()  + ("\(availableLoyaltyPoints )")
        }).disposed(by: disposeBag)

        output.myLoyaltyPoints.asObservable().subscribe(onNext: { (order) in
           
            self.loyaltyPointsVM.myLoyaltyPointsSubject.onNext([LoyaltyPointsSection(header: "", items: order.loyaltyPointHistroy ?? [])])}).disposed(by: disposeBag)
        
        output.error.asObservable().subscribe(onNext:{
            (Error) in
           
            print("error")
            }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toOrdersDetails" {
            if let vc = segue.destination as? MasterOrdersDetailsViewController{
                guard let orderDetails = sender as? LoyaltyPointHistroy  else {
                    return
                }
                let order = Order(code: orderDetails.orderCode, guid: "", placed: "", status: "", statusDisplay: "", total: nil)
                vc.ordersDeatilsBehaviorsSubject = BehaviorSubject<Order>(value: order)
              
            }
        }
    }
    
}
