//
//  LoyaltyPointsCell.swift
//  HybrisCloudMobileApp
//
//  Created by khalil anqawi on 26/05/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import UIKit

class LoyaltyPointsCell: UITableViewCell {

    @IBOutlet weak var dateValueLbl: UILabel!
    @IBOutlet weak var timeValueLbl: UILabel!
    @IBOutlet weak var pointsValueLbl: UILabel!
   
    @IBOutlet weak var balancePointsValueLbl: UILabel!
    @IBOutlet weak var typeValueLbl: UILabel!
    @IBOutlet weak var orderNumberBttn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setUp(order : LoyaltyPointHistroy){
        dateValueLbl.text = order.dateOfPurchase?.toDateString(format: "MMM dd yyyy ") ?? ""
        timeValueLbl.text = order.dateOfPurchase?.toDateString(format: "h:mm a ") ?? ""
        pointsValueLbl.text = "\(order.points ?? 0.0)"
        balancePointsValueLbl.text = "\(order.balancePoints ?? 0.0)"
        typeValueLbl.text = order.actionType?.name ?? ""
        orderNumberBttn.setTitle(order.orderCode ?? "", for: .normal)
  
        
    }

}
