//
//  ContactUsViewController.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/27/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import UIKit

class ContactUsViewController: UIViewController {
    

    @IBOutlet weak var describtionContactUs :UITextView!
    
    var viewModel:ContactUsViewModel!
     
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()

        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
                   .mapToVoid()
                   .asDriverOnErrorJustComplete()
        
        viewModel = ContactUsViewModel(componentUseCase: networkUseCaseProvider.makeComponentUseCase())
      
        
        let input = ContactUsViewModel.Input(trigger: viewWillAppear)
        
         let output = viewModel.transform(input: input)
        
        output.error.asObservable().subscribe(onNext: { (error) in
                print(error)
            }).disposed(by: disposeBag)
        
        output.componentOutput.asObservable().subscribe(onNext: { (contactUs) in
            let stringForFont  = contactUs.content ?? ""
            
            let containsEmail = stringForFont.contains("<br>E-mail:")
            let containsPhone = stringForFont.contains("<br>Phone:")
            
            var emails : [String] = []
            var phoneNumbers : [String] = []

            if(containsEmail) {
                if let lower = stringForFont.range(of: "<br>E-mail:")?.upperBound,
                    let upper = stringForFont.range(of: "<br>", range: lower..<stringForFont.endIndex)?.lowerBound {
                    let email = String(stringForFont[lower..<upper])
                    emails.append( email)
                    
                }
            }
            
            if(containsPhone) {
                        
                if let lower = stringForFont.range(of: "<br>Phone:")?.upperBound,
                    let upper = stringForFont.range(of: "<br>", range: lower..<stringForFont.endIndex)?.lowerBound {
                    let phoneNumber = String(stringForFont[lower..<upper])
                    phoneNumbers.append(phoneNumber)
                    
                }
            }

            let attributedString = contactUs.content?.htmlAttributedString()


            for item in emails {
                let emailAttributes = [NSAttributedString.Key.foregroundColor: UIColor.orange]
                
                let firstIndex = (attributedString?.string ?? "").index(of: item)? .utf16Offset(in:attributedString?.string ?? "") ?? 0

                let range = NSRange(location: firstIndex, length: item.count)
                attributedString?.addAttributes(emailAttributes, range: range)

            }
            
            for item in phoneNumbers {
                let phoneNumberAttributes = [NSAttributedString.Key.foregroundColor: UIColor.green]
                
                let firstIndex = (attributedString?.string ?? "").index(of: item)? .utf16Offset(in:attributedString?.string ?? "") ?? 0
           

                let range = NSRange(location: firstIndex, length: item.count)
                
                attributedString?.addAttributes(phoneNumberAttributes, range: range)

            }
            let paragraph = NSMutableParagraphStyle()
            paragraph.baseWritingDirection = L102Language.isRTL ?  .rightToLeft : .leftToRight
            paragraph.alignment = .natural
            
            attributedString?.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraph, range: NSMakeRange(0, attributedString?.length ?? 0))
            
            self.describtionContactUs.attributedText = attributedString
            self.describtionContactUs.linkTextAttributes = [:]
            
            }).disposed(by: disposeBag)
        
    }


}

