//
//  UIView.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/17/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import UIKit
// this code could be used in BUTTONS --> btn.applyGradiant(.....

extension UIView {
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }
    
    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
}

//the bellow 2 classes is to set n avigation bar gradiant color .
class UINavigationBarGradientView: UIView {
    
    enum Point {
        case topRight, topLeft
        case bottomRight, bottomLeft
        case custion(point: CGPoint)
        
        var point: CGPoint {
            switch self {
            case .topRight: return CGPoint(x: 1, y: 0)
            case .topLeft: return CGPoint(x: 0, y: 0)
            case .bottomRight: return CGPoint(x: 1, y: 1)
            case .bottomLeft: return CGPoint(x: 0, y: 1)
            case .custion(let point): return point
            }
        }
    }
    
    private weak var gradientLayer: CAGradientLayer!
    convenience init(colors: [UIColor], startPoint: Point = .topLeft,
                     endPoint: Point = .bottomLeft, locations: [NSNumber] = [0, 1]) {
        self.init(frame: .zero)
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = frame
        layer.addSublayer(gradientLayer)
        self.gradientLayer = gradientLayer
        set(colors: colors, startPoint: startPoint, endPoint: endPoint, locations: locations)
        backgroundColor = .clear
    }
    
    func set(colors: [UIColor], startPoint: Point = .topLeft,
             endPoint: Point = .bottomLeft, locations: [NSNumber] = [0, 1]) {
        gradientLayer.colors = colors.map { $0.cgColor }
        gradientLayer.startPoint = startPoint.point
        gradientLayer.endPoint = endPoint.point
        gradientLayer.locations = locations
    }
    
    func setupConstraints() {
        guard let parentView = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: parentView.topAnchor).isActive = true
        leftAnchor.constraint(equalTo: parentView.leftAnchor).isActive = true
        parentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        parentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard let gradientLayer = gradientLayer else { return }
        gradientLayer.frame = frame
        superview?.addSubview(self)
    }
}
@IBDesignable
class cornerImage: UIImageView {
    
    @IBInspectable var imageCornerRadius: CGFloat = 0{
        didSet{
            self.updateView()
        }
    }
    @IBInspectable var imageBorderColor: UIColor = UIColor.white {
        didSet{
            self.updateView()
        }
    }
    
    @IBInspectable var imageBorderWidth: CGFloat = 0.0{
        didSet{
            self.updateView()
        }
    }
    
    //Apply params
    func updateView() {
        self.layer.cornerRadius = self.imageCornerRadius
        self.layer.borderColor = self.imageBorderColor.cgColor
        self.layer.borderWidth = self.imageBorderWidth
        
    }
}

@IBDesignable
class RoundedImageView: UIImageView {

    @IBInspectable var roundedImageRadius: CGFloat = 0.0 { didSet { setUpView() } }

    func setUpView() {
        self.clipsToBounds = true

        setTopCornerRadius(rect: self.bounds)
    }

    func setTopCornerRadius(rect: CGRect) {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners:[.topLeft, .topRight, .bottomLeft , . bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = rect
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
        self.layer.masksToBounds = true
    }
}

@IBDesignable
class ShadowView: UIView {
    let gradientLayer = CAGradientLayer()

    //Shadow
    @IBInspectable var viewShadowColor: UIColor = UIColor.black {
        didSet {
            self.updateView()
        }
    }
    @IBInspectable var viewShadowOpacity: Float = 0.5 {
        didSet {
            self.updateView()
        }
    }
    @IBInspectable var viewShadowOffset: CGSize = CGSize(width: 3, height: 3) {
        didSet {
            self.updateView()
        }
    }
    @IBInspectable var viewShadowRadius: CGFloat = 15.0 {
        didSet {
            self.updateView()
        }
    }

    @IBInspectable var viewCornerRadius: CGFloat = 0{
        didSet{
            self.updateView()
        }
    }
    
    @IBInspectable var viewBorderColor: UIColor = UIColor.black {
        didSet{
            self.updateView()
        }
    }
    
    @IBInspectable var viewBorderWidth: CGFloat = 0.0{
        didSet{
            self.updateView()
        }
    }


     //Apply params
    func updateView() {
        self.layer.shadowColor = self.viewShadowColor.cgColor
        self.layer.shadowOpacity = self.viewShadowOpacity
        self.layer.shadowOffset = self.viewShadowOffset
        self.layer.shadowRadius = self.viewShadowRadius
        self.layer.borderWidth = self.viewBorderWidth
        self.layer.cornerRadius = self.viewCornerRadius
    }
    
}
@IBDesignable class GradientView: UIView {
    @IBInspectable var topColor: UIColor = UIColor.white
    @IBInspectable var bottomColor: UIColor = UIColor.black

    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }

    override func layoutSubviews() {
        (layer as! CAGradientLayer).colors = [topColor.cgColor, bottomColor.cgColor]
    }
}
extension UINavigationBar {
    func setGradientBackground(colors: [UIColor],
                               startPoint: UINavigationBarGradientView.Point = .topLeft,
                               endPoint: UINavigationBarGradientView.Point = .topRight,
                               locations: [NSNumber] = [0, 1]) {
        guard let backgroundView = value(forKey: "backgroundView") as? UIView else { return }
        guard let gradientView = backgroundView.subviews.first(where: { $0 is UINavigationBarGradientView }) as? UINavigationBarGradientView else {
            let gradientView = UINavigationBarGradientView(colors: colors, startPoint: startPoint,
                                                           endPoint: endPoint, locations: locations)
            backgroundView.addSubview(gradientView)
            gradientView.setupConstraints()
            return
        }
        gradientView.set(colors: colors, startPoint: startPoint, endPoint: endPoint, locations: locations)
    }
}




@IBDesignable
class CircleBackgroundView: UIView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.size.width / 2
        layer.masksToBounds = true
    }
    
}
