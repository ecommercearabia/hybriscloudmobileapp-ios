//
//  UINavigationController.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/13/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
extension UINavigationController {
   func backToViewController(vc: Any) {
      // iterate to find the type of vc
      for element in viewControllers as Array {
        if "\(type(of: element)).Type" == "\(type(of: vc))" {
            self.popToViewController(element, animated: true)
            break
         }
      }
   }
}
