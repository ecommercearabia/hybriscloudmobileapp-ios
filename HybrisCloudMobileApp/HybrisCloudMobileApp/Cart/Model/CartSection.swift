//
//  CartModel.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

struct CartSection {
    var header: String
    var items: [Item]
    
}

extension CartSection : AnimatableSectionModelType, Equatable {
    typealias Item = CartEntry

    var identity: String {
        return header
    }

    init(original: CartSection, items: [CartEntry]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: CartSection, rhs: CartSection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension CartEntry: IdentifiableType, Equatable {
    public var identity: String {
        return  "\(product?.code ?? "") \(entryNumber ?? 0)"
    }
    
    public static func == (lhs: CartEntry, rhs: CartEntry) -> Bool {
        return (lhs.product?.code  == rhs.product?.code) && (lhs.quantity == rhs.quantity) && (lhs.entryNumber == rhs.entryNumber) && (lhs.product?.stock?.stockLevel == rhs.product?.stock?.stockLevel)
    }
}

