//
//  VoucherSection.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 9/28/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

//CartAppliedVoucher
import Foundation
import RxSwift
import RxCocoa
import RxDataSources

struct VoucherSection {
    var header: String
    var items: [Item]
    
}

extension VoucherSection : AnimatableSectionModelType, Equatable {
    typealias Item = CartAppliedVoucher

    var identity: String {
        return header
    }

    init(original: VoucherSection, items: [CartAppliedVoucher]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: VoucherSection, rhs: VoucherSection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension CartAppliedVoucher: IdentifiableType, Equatable {
    public var identity: String {
        return  code ?? ""
    }
    
    public static func == (lhs: CartAppliedVoucher, rhs: CartAppliedVoucher) -> Bool {
        return (lhs.code == rhs.code)
    }
}

