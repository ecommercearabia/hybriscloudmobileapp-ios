//
//  BankOffersSection.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 11/2/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources


typealias PromoCodeMsgsSection = AnimatableSectionModel<String,PromoCodeMsgsCellIndexes>

enum PromoCodeMsgsCellIndexes :IdentifiableType, Equatable{
    
    typealias Identity = String
    
    public var identity: Identity {
        
        switch self {
            
        case .bankOffer(bankOffer : let bankOffer):
            return bankOffer.code ?? ""
        case .potentialOrderPromotion(potentialOrderPromotion : let potentialOrderPromotion):
            return potentialOrderPromotion.promotion?.code ?? ""
        case .appliedOrderPromotion(appliedOrderPromotion : let appliedOrderPromotion):
            return appliedOrderPromotion.promotion?.code ?? ""
        }
    }
    
    static func == (lhs: PromoCodeMsgsCellIndexes, rhs: PromoCodeMsgsCellIndexes) -> Bool {
        return false//lhs.identity == rhs.identity
    }
    
    case bankOffer (bankOffer : BankOffer)
    case potentialOrderPromotion (potentialOrderPromotion : PotentialOrderPromotion)
    case appliedOrderPromotion (appliedOrderPromotion : AppliedOrderPromotion)
}

//offersCellIdentifier
