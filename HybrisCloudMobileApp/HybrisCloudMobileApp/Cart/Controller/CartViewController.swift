//
//  CartViewController.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import Domain
import SwipeCellKit
import KeychainSwift
import Firebase
class CartViewController: UIViewController , UITextFieldDelegate{
    @IBOutlet weak var deliveryBttn: UIButton!
    @IBOutlet weak var pickupFromStoreBttn: UIButton!
    @IBOutlet weak var delveryImage: UIImageView!
    @IBOutlet weak var pickupFromStoreImage: UIImageView!
    @IBOutlet weak var continueShoppingButton: DesignableButton!
    @IBOutlet weak var shipmentTypeView: UIView!
    @IBOutlet weak var shipmentAddressLbl: UILabel!
    @IBOutlet weak var shipmentAddressView: UIView!
    @IBOutlet weak var getDirectionBttn: UIButton!
    
    @IBOutlet weak var ViewHeight: NSLayoutConstraint!
    @IBOutlet weak var titleHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!{didSet{tableView.isHidden = true}}
    @IBOutlet weak var shipmentTypeStackHeight: NSLayoutConstraint!
    
    let disposeBag = DisposeBag()
    var cartViewModel : CartViewModel!
    var cartId = String()
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var netAmountLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var checkOutBut: DesignableButton!
    @IBOutlet weak var discountLblVal: UILabel!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profileBttn: UIButton!
    @IBOutlet weak var profileHeight: NSLayoutConstraint!
    @IBOutlet weak var profileTopConstraint: NSLayoutConstraint!
    
    let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
    let createCartSubject = PublishSubject<Void>()
    let selectedshipmentTypeSubject = PublishSubject<String>()
    let getCurrentShipmentTypeSubject  = PublishSubject<Void>()
    let triggerShipmentTypeSubject  = PublishSubject<Void>()
    
    var createShipmentType = ""
    var carts : CartsModel?
    var cart : CartModel?
//    required init?(coder aDecoder: NSCoder) {
//
//        super.init(coder: aDecoder)
//
////        self.cartViewModel.cartDataSubject.subscribe(onNext: { (CartData) in
////            if let tabItems = self.tabBarController?.tabBar.items {
////                let tabItem = tabItems[2]
////                tabItem.badgeValue = String(CartData.deliveryItemsQuantity ?? 0)
////            }
////
////            }).disposed(by: bag)
//
// }

    override func viewWillAppear(_ animated: Bool) {
        Analytics.logEvent(AnalyticsEventViewCart, parameters: [
          AnalyticsParameterCurrency: "AED",
          AnalyticsParameterValue: self.totalLbl.text ?? ""
          ])
        self.triggerShipmentTypeSubject.onNext(())
        self.getCurrentShipmentTypeSubject.onNext(())
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.tableView.rx.setDelegate(self).disposed(by: disposeBag)
        preparation()
        self.tableView.isHidden = true
        print("test10")
        self.ViewHeight.constant = 0
        self.titleHeight.constant = 0
        self.getDirectionBttn.isHidden = true
        self.navigationItem.backButtonTitle = "Back".localized()
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 0.3686274886, green: 0.3686274886, blue: 0.3686274886, alpha: 1)
        if L102Language.isRTL {
            profileBttn.semanticContentAttribute = .forceRightToLeft
        } else {
            profileBttn.semanticContentAttribute = .forceLeftToRight
        }
        
        styleForByClicking()
    }
    
    func styleForByClicking(){
        let textbyClicking = "in order for you to proceed to checkout, Please update your profile".localized()
        profileBttn.setTitle(textbyClicking, for: .normal)
        let orangeAttriString = NSMutableAttributedString(string: textbyClicking)
        let range1 = (textbyClicking as NSString).range(of: "profile".localized())
        
        let orange = UIColor(red: 240/255.0, green: 141/255.0, blue: 57/255.0, alpha: 1.00 )
        
        orangeAttriString.addAttribute(NSAttributedString.Key.foregroundColor , value: orange, range: range1 )
        orangeAttriString.underLine(onRange: range1)
        profileBttn.titleLabel?.numberOfLines = 0
        profileBttn.setAttributedTitle(orangeAttriString, for: .normal)
        profileBttn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        if L102Language.isRTL{
            profileBttn.contentHorizontalAlignment = .right
        } else {
            profileBttn.contentHorizontalAlignment = .left
        }
    
       
        
    }
   

    func preparation(){
        
        tableView.refreshControl = UIRefreshControl()

        let pull = tableView.refreshControl!.rx
              .controlEvent(.valueChanged)
              .asDriver()
        
        cartViewModel = .init(getCartUseCase: networkUseCaseProvider.makeCartUseCase(), getCartsUseCase: networkUseCaseProvider.makeCartsUseCase(), validationsCartErrors: networkUseCaseProvider.makeValidationCartUseCase(), bankOffersUseCase: networkUseCaseProvider.makeBankOffersCartUseCase(), shipmentTypesUseCase: networkUseCaseProvider.makeShipmentTypes(), configUseCase: networkUseCaseProvider.makeConfig(), userUseCase: networkUseCaseProvider.makeUserUseCase())
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewDidAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
            
            
        
        let input = CartViewModel.Input(
            trigger: Driver.merge(viewWillAppear,pull,cartViewModel.refreshCartSubject.asDriverOnErrorJustComplete()), triggerShipmentType: triggerShipmentTypeSubject.asDriverOnErrorJustComplete(),
            deleteEntry: cartViewModel.deleteEntrySubject.asDriverOnErrorJustComplete(),
            creatCart: createCartSubject.asDriverOnErrorJustComplete(),
            cartCode: cartViewModel.cartCode.asDriverOnErrorJustComplete() ,
            checkOut:  checkOutBut.rx.tap.asDriverOnErrorJustComplete() ,
            navigate: cartViewModel.navigate.asDriverOnErrorJustComplete() ,
            selectedshipmentType: selectedshipmentTypeSubject.asDriverOnErrorJustComplete() ,
            getCurrentShipmentType: getCurrentShipmentTypeSubject.asDriverOnErrorJustComplete())
        
        let output = cartViewModel.transform(input: input)
        self.hanelShipmentType(output : output)
        output.cart.map { (cart) -> [CartSection] in
            return [CartSection.init(header: "", items: cart.entries ?? [])]
        }.asObservable().bind(to: tableView.rx.items(dataSource: self.cartViewModel.dataSourceCart())).disposed(by: disposeBag)
        
        self.tableView.rx.modelSelected(CartSection.Item.self).subscribe(onNext: { (entrie) in
            
            Analytics.logEvent(AnalyticsEventSelectItem, parameters: [
                AnalyticsParameterItemListID: entrie.product?.code ?? "" ,
                AnalyticsParameterItemListName: entrie.product?.name ?? ""
              ])
            
            self.performSegue(withIdentifier: "toProductDetails", sender: entrie)
        }).disposed(by: disposeBag)
        
        output.cart.map { (cart) -> Bool in
            cart.entries?.isEmpty ?? true
        }.asObservable().bind(to: self.tableView.rx.isHidden,self.checkOutBut.rx.isHidden).disposed(by: disposeBag)
        
        output.cart.map { (cart) -> Bool in
            cart.entries?.count ?? 0 > 0
        }.asObservable().bind(to: self.emptyView.rx.isHidden).disposed(by: disposeBag)
        
   
        output.cart.asObservable().subscribe(onNext: { (CartModel) in
            if let tabItems = self.tabBarController?.tabBar.items {
                // In this case we want to modify the badge number of the third tab:
                let tabItem = tabItems[2]
                
                tabItem.badgeValue = String(CartModel.entries?.count ?? 0)
            }
            
          

        }).disposed(by: disposeBag)
        
        output.cart.asObservable().map { (cart) -> String in
            self.subTotalLbl.text = cart.subTotalBeforeSavingPrice?.formattedValue ?? ""
            self.netAmountLbl.text = cart.totalTax?.formattedValue ?? ""
            self.totalLbl.text = cart.totalPrice?.formattedValue ?? ""
            self.discountLblVal.text = cart.totalDiscounts?.formattedValue ?? ""
            self.shipmentAddressLbl.text =  cart.entries?.first?.deliveryPointOfService?.address?.formattedAddress ?? ""
            self.cart = cart
            
            if cart.entries?.count == 0  {
                            self.shipmentTypeView.isHidden = true
                            self.ViewHeight.constant = 0
                            self.titleHeight.constant = 0
                            self.shipmentTypeStackHeight.constant = 0
                            self.view.layoutIfNeeded()
                        }
            
//            else {
//                            self.shipmentTypeView.isHidden = false
//                            self.ViewHeight.constant = 100
//                            self.titleHeight.constant = 21
//                            self.shipmentTypeStackHeight.constant = 30
//                            self.view.layoutIfNeeded()
//
//                        }

            return (KeychainSwift().get("user") ?? ("anonymous") == "anonymous") ? (cart.guid ?? "0000") : "current"
            
        }.subscribe(onNext: { (guid) in
            if KeychainSwift().get("user") != "current" {
                KeychainSwift().set(guid, forKey: "guid")
            }
            
        }).disposed(by: disposeBag)
        
        output.navigateTo.asObservable().subscribe(onNext: { (identifier) in
            
            if self.createShipmentType ==  "pickup" && identifier != "login" {
                self.performSegue(withIdentifier: "ToChickOut", sender: self)
            } else {
                self.performSegue(withIdentifier: identifier, sender: self)
            }
          
        }).disposed(by: disposeBag)
        
        
        output.validationProfileErrors.asObservable().subscribe(onNext: { (errors) in
            if errors.valid == true {
                self.profileView.isHidden = true
                self.profileHeight.constant = 0
                self.profileTopConstraint.constant = 0
            }
            else {
                let  error = errors.errors?.joined(separator: ", ") ?? ""
                if error.contains("profile")  || error.contains("ملف") {
                    self.profileView.isHidden = false
                    self.profileHeight.constant = 45
                    self.profileTopConstraint.constant = 23
                } else {
                    self.profileView.isHidden = true
                    self.profileHeight.constant = 0
                    self.profileTopConstraint.constant = 0
                }
            }

        }).disposed(by: disposeBag)
        
        
        
        output.validationCartErrors.asObservable().subscribe(onNext: { (errors) in
            if errors.valid == true {
                self.profileView.isHidden = true
                self.profileHeight.constant = 0
                self.profileTopConstraint.constant = 0
                self.cartViewModel.navigate.onNext(())
            }
            else {
                let  error = errors.errors?.joined(separator: ", ") ?? ""
                if error.contains("profile")  {
                    self.profileView.isHidden = false
                    self.profileHeight.constant = 45
                    self.profileTopConstraint.constant = 23
                } else {
                    self.profileView.isHidden = true
                    self.profileHeight.constant = 0
                    self.profileTopConstraint.constant = 0
                    Loaf(errors.errors?.joined(separator: ", ") ?? "", state: .error(L102Language.isRTL ? .right : .left), sender: self).show()
                }
                self.cartViewModel.refreshCartSubject.onNext(())
                
            }

        }).disposed(by: disposeBag)
        

        
        output.error.asObservable().subscribe(onNext: { (Error) in
            let error = Error as? ErrorResponse
            if error?.errors?.first?.type == "AccessDeniedError" {
                if KeychainSwift().get("guid") == nil
                    || KeychainSwift().get("guid") == "current"{
                    self.createCartSubject.onNext(())
                }
                else {
                    self.cartViewModel.navigate.onNext(())
                }
                self.removeBadge()
            }
           else if error?.errors?.first?.message == "Cart not found." {
            self.removeBadge()
                self.createCartSubject.onNext(())
            }
           else if error?.errors?.first?.type == "CartError"{
                self.tableView.isHidden = true
                self.checkOutBut.isHidden = true
                self.continueShoppingButton.isHidden = false
                self.emptyView.isHidden = false
            self.shipmentTypeView.isHidden = true
            self.ViewHeight.constant = 0
            self.titleHeight.constant = 0
            self.shipmentTypeStackHeight.constant = 0
            
            self.removeBadge()
            }
           else if error?.errors?.first?.type == "InsufficientStockError"  {
            
            Loaf(error?.errors?.first?.message ?? "", state: .error(L102Language.isRTL ? .right : .left), sender: self).show()
            self.cartViewModel.refreshCartSubject.onNext(())

           }
           else {
            return
           }
            
        }).disposed(by: self.disposeBag)
        
        output.fetching
                  .drive(tableView.refreshControl!.rx.isRefreshing)
                  .disposed(by: disposeBag)
        
        continueShoppingButton.rx.tap.subscribe(onNext: { (Void) in
            self.tabBarController?.selectedIndex = 0
        }).disposed(by: disposeBag)
        
//        output.getUserInformation.asObservable().subscribe(onNext: {(data) in
//            if data.title == nil || data.nationality?.code == nil {
//
//                self.profileView.isHidden = false
//                self.profileHeight.constant = 45
//                self.profileTopConstraint.constant = 23
//                self.checkOutBut.isEnabled = false
//                self.checkOutBut.alpha = 0.5
//            } else {
//                self.profileView.isHidden = true
//                self.profileHeight.constant = 0
//                self.profileTopConstraint.constant = 0
//                self.checkOutBut.isEnabled = true
//                self.checkOutBut.alpha = 1
//            }
//        }).disposed(by: disposeBag)
        
        self.profileBttn.rx.tap.subscribe(onNext: {(void) in
            self.performSegue(withIdentifier: "toPersonalDetails", sender: nil)
        }).disposed(by: disposeBag)
    }
    
    //#Mark: handel shipmentType
    func hanelShipmentType(output : CartViewModel.Output) {
//        output.carts.asObservable().subscribe(onNext: {(carts) in
//
//            self.carts = carts
//            if carts.carts?.first?.shipmentType != nil {
//                self.selectedshipmentTypeSubject.onNext(carts.carts?.first?.shipmentType?.code ?? "")
//                if carts.carts?.first?.shipmentType?.code !=  "DELIVERY" {
//                    self.shipmentAddressLbl.text =  carts.carts?.first?.entries?.first?.deliveryPointOfService?.address?.formattedAddress ?? ""
//                    self.shipmentAddressView.isHidden = false
//                } else {
//                    self.shipmentAddressView.isHidden = true
//                }
//            } else {
//
//                self.shipmentTypeView.isHidden = true
//                self.selectedshipmentTypeSubject.onNext("DELIVERY")
//            }
//
//        }).disposed(by: disposeBag)
        
        self.getDirectionBttn.rx.tap.subscribe(onNext: { (void) in
            self.showNavigationOptions(lat: self.cart?.entries?.first?.deliveryPointOfService?.geoPoint?.latitude  ?? 0.0 , lon: self.cart?.entries?.first?.deliveryPointOfService?.geoPoint?.longitude ?? 0.0 )
        }).disposed(by: disposeBag)
        
        
//        output.shipmentTypes.asObservable().withLatestFrom(Driver.combineLatest(output.shipmentTypes,output.carts)).subscribe(onNext: {( shipmentTypes, carts ) in
//            if (shipmentTypes.supportedShipmentTypes?.count ?? 0) < 2 &&  carts.carts?.first?.shipmentType == nil {
//                self.selectedshipmentTypeSubject.onNext("DELIVERY")
//                self.shipmentTypeView.isHidden = true
//            } else {
//                self.shipmentTypeView.isHidden = false
//            }
//
//        }).disposed(by: disposeBag)
        
        output.selectedShipmentType.asObservable().subscribe(onNext: { (selectedShipment) in
          //  print(selectedShipment)
           
            self.getCurrentShipmentTypeSubject.onNext(())
        }).disposed(by: disposeBag)
        output.currentShipmentType.asObservable().subscribe(onNext: { (currentShipment) in
            if currentShipment.code != "DELIVERY" {
                self.shipmentAddressView.isHidden = false
                self.delveryImage.image = UIImage(named: "radioButtonUnSelected")
                self.pickupFromStoreImage.image = UIImage(named: "radioButtonSelected")
                self.createShipmentType = "pickup"
                self.ViewHeight.constant = 100
                self.titleHeight.constant = 21
                self.getDirectionBttn.isHidden = false
                self.view.layoutIfNeeded()
                self.cartViewModel.refreshCartSubject.onNext(())
            } else {
                self.shipmentAddressView.isHidden = true
                self.delveryImage.image = UIImage(named: "radioButtonSelected")
                self.pickupFromStoreImage.image = UIImage(named: "radioButtonUnSelected")
                self.createShipmentType = "fc-ae"
                self.ViewHeight.constant = 0
                self.titleHeight.constant = 0
                self.getDirectionBttn.isHidden = true
                self.view.layoutIfNeeded()
              
                
            }
          
       
         
            
            
            
        }).disposed(by: disposeBag)
        
        self.deliveryBttn.rx.tap.subscribe(onNext: {(void ) in
            self.selectedshipmentTypeSubject.onNext("DELIVERY")
            self.delveryImage.image = UIImage(named: "radioButtonSelected")
            self.pickupFromStoreImage.image = UIImage(named: "radioButtonUnSelected")
            
            self.ViewHeight.constant = 0
            self.titleHeight.constant = 0
            self.getDirectionBttn.isHidden = true
            self.view.layoutIfNeeded()
            
        }).disposed(by: disposeBag)
        
        self.pickupFromStoreBttn.rx.tap.subscribe(onNext: {(void ) in
            self.selectedshipmentTypeSubject.onNext("PICKUP_IN_STORE")
            self.delveryImage.image = UIImage(named: "radioButtonUnSelected")
            self.pickupFromStoreImage.image =  UIImage(named: "radioButtonSelected")
            self.ViewHeight.constant = 100
            self.titleHeight.constant = 21
            self.getDirectionBttn.isHidden = false
            self.view.layoutIfNeeded()
        }).disposed(by: disposeBag)
        
       
        
        output.config.withLatestFrom(Driver.combineLatest(output.config , output.currentShipmentType)).asObservable() .subscribe(onNext: { config , currentShipmentType in
            if config.pickupInStoreEnabled == true {
                
                if currentShipmentType.code ==  "PICKUP_IN_STORE"  {
                    self.shipmentTypeView.isHidden = false
                    self.ViewHeight.constant = 100
                    self.titleHeight.constant = 21
                    self.shipmentTypeStackHeight.constant = 30
                    self.shipmentAddressView.isHidden = false
                    self.view.layoutIfNeeded()
                } else {
                    self.shipmentTypeView.isHidden = false
                    self.selectedshipmentTypeSubject.onNext("DELIVERY")
                    self.ViewHeight.constant = 0
                    self.titleHeight.constant = 0
                    self.shipmentTypeStackHeight.constant = 30
                    self.shipmentAddressView.isHidden = true
                    self.view.layoutIfNeeded()
                }
            
            } else {
               
                self.shipmentTypeView.isHidden = true
                self.ViewHeight.constant = 0
                self.titleHeight.constant = 0
                self.shipmentTypeStackHeight.constant = 0
                self.view.layoutIfNeeded()
            }
            
            
        }).disposed(by: disposeBag)
        
        self.cartViewModel.navToExpressDeliveryInfoSubject.subscribe(onNext: {(void)
            in
            self.performSegue(withIdentifier: "toExpressDeliveryInfoVC", sender: nil)
            
        }).disposed(by: disposeBag)
        
    }
    
    func removeBadge() {
        if let tabItems = self.tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[2]
            
            tabItem.badgeValue = "0"
        }

    }
    
    
    func TitelTowLine(titleOne : String , titleTow : String) {
        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 20))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 1
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.text = titleOne
        
        let label2 = UILabel(frame: CGRect(x: 0.0, y: 20, width: UIScreen.main.bounds.width, height: 20))
        label2.backgroundColor = UIColor.clear
        label2.numberOfLines = 1
        label2.textAlignment = NSTextAlignment.center
        label2.font = label.font.withSize(15)
        
        label2.text = titleTow
        
        view.addSubview(label)
        view.addSubview(label2)
        self.navigationItem.titleView = view
        
    }
    func showNavigationOptions(lat:Double,lon:Double) {

        let navigationAlert = UIAlertController(title: "Selection", message: "Select Navigation App", preferredStyle: .actionSheet)
        
        let appleURL = "http://maps.apple.com/?daddr=\(lat),\(lon)"
        let appleMapsAction = UIAlertAction(title: "Apple Maps", style: UIAlertAction.Style.default) {
            UIAlertAction in
                let url = URL(string:
                                appleURL)
                self.openURLWithString(url: url!)
        }
        
        let googleMapsAction = UIAlertAction(title: "Google Maps", style: UIAlertAction.Style.default) {
            UIAlertAction in

            let googleMapsInstalled = UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)
            if googleMapsInstalled {
                
                let url = URL(string:
                    "comgooglemaps://?saddr=&daddr=\(lat),\(lon)&directionsmode=driving")
                self.openURLWithString(url: url!)
                
            }else{
                
                let url = URL(string: "https://itunes.apple.com/us/app/google-maps-gps-navigation/id585027354?mt=8")
                self.openURLWithString(url: url!)
                
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        navigationAlert.addAction(appleMapsAction)
        navigationAlert.addAction(googleMapsAction)
        navigationAlert.addAction(cancelAction)
        self.present(navigationAlert, animated: true, completion: nil)
    }
  
  func openURLWithString(url: URL){
      if #available(iOS 10.0, *) {
          UIApplication.shared.open(url)
      }else{
          UIApplication.shared.open(url, options: [:], completionHandler: nil)
      }
  }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        

        if segue.identifier == "toPersonalDetails"{
           
                if let personalDetailsViewController = segue.destination as? PersonalDetailsViewController {
                    personalDetailsViewController.isFromCart = true
            }
        }
        
        
        if segue.identifier == "address"{
           
                if let deliveryAddressViewController = segue.destination as? DeliveryAddressViewController {
                    deliveryAddressViewController.isFromCart = true
                    deliveryAddressViewController.cartId = self.cartId
            }
        }
        
        if segue.identifier == "ToChickOut" {
            if let chickOut = segue.destination as? ChickOutStep2ViewController {
                chickOut.createShipmentType.onNext(createShipmentType)
                chickOut.carts = carts
            }
        }
        if segue.identifier == "toProductDetails"{
            if let vc = segue.destination as? ProductDetailsViewController{
                guard let cartEntry = sender as? CartEntry else {
                    return
                }
                
                vc.productDetailsBehaviorSubject =  BehaviorSubject<String>(value: cartEntry.product?.code ?? "")
                    
//                    BehaviorSubject<ProductDetails>(value: ProductDetails(availableForPickup: nil, baseOptions: nil, baseProduct: nil, categories: nil, classifications: nil, code: cartEntry.product?.code ?? "", configurable: nil, countryOfOrigin: nil, countryOfOriginIsocode: nil, ProductDetailsDescription: nil, images: nil, name: nil, numberOfReviews: nil, potentialPromotions: nil, price: nil, priceRange: nil, productReferences: nil, purchasable: nil, reviews: nil, stock: nil, summary: nil, unitOfMeasure: nil, unitOfMeasureDescription: nil, url: nil, averageRating: nil, discount: nil , vegan: nil , organic: nil , glutenFree: nil ))
            }
        }
 
    }
    
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        if textField == voucherTF {
//            if string == "" {
//                // User presses backspace
//                textField.deleteBackward()
//            } else {
//                // User presses a key or pastes
//                textField.insertText(string.uppercased())
//            }
//            // Do not let specified text range to be changed promo code uppercased
//            return false
//        }
//
//        return true
//    }
//
}

 
extension DispatchQueue {
    
    static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    
}
