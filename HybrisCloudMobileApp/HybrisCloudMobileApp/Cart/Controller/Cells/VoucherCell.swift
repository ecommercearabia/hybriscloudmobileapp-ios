//
//  VoucherCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 9/28/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

class VoucherCell: UICollectionViewCell {
    
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var delete: UIButton!
    let refreshCartSubject = PublishSubject<Void>()
    var viewModel = VoucherCellVM(CartUseCase: NetworkPlatform.UseCaseProvider().makeCartUseCase())
    var bag = DisposeBag()
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }
    
    
    func setUp(voucher : CartAppliedVoucher){
        self.code.text = voucher.code ?? ""
        
        let output = viewModel.transform(input: VoucherCellVM.Input(voucher: Driver<CartAppliedVoucher>.just(voucher), deleteVoucher: delete.rx.tap.asDriverOnErrorJustComplete()))
        
        output.refreshCart.asObservable().bind(to: self.refreshCartSubject).disposed(by: bag)
        
        
    }
    
}
