//
//  CartItem.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import SwipeCellKit
import RxKingfisher
import Kingfisher


class CartItem: SwipeTableViewCell  {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var qtyLbl: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var pluseButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var oldPrice: UILabel!
    @IBOutlet weak var outOfStockView: UIView!
    @IBOutlet weak var quantityStackView: UIStackView!
    
    @IBOutlet weak var expressStack: UIStackView!
    @IBOutlet weak var expressButton: UIButton!
    let navToExpressDeliveryInfoSubject = PublishSubject<Void>()
    let refreshCartSubject = PublishSubject<Void>()
    var viewModel = CartItemViewModel(changeQuantityUserCase: NetworkPlatform.UseCaseProvider().makeCartQuantityUserCase())
    let cartEntry = PublishSubject<CartEntry>()
    let entryNumber = PublishSubject<String>()
    var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
     }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.disposeBag = DisposeBag()
    }
    
 
    func setUp(){
 
 
        let output = viewModel.transform(input: CartItemViewModel.Input(cartEntry: cartEntry.asDriverOnErrorJustComplete(), increaseQuantityTrigger: self.pluseButton.rx.tap.asDriver(), decreaseQuantityTrigger: self.minusButton.rx.tap.asDriver(), entryNumber: self.entryNumber.asDriverOnErrorJustComplete()))
        
        output.cartEntry.asObservable().map { (cartEntry) in
            return cartEntry.product?.name?.html2AttributedString ?? ""
        }.asObservable().bind(to: self.name.rx.text).disposed(by: self.disposeBag)

        output.cartEntry.asObservable().map{(cartEntry) in
            return (!((cartEntry.product?.stock?.stockLevel ?? 0.0) <= 0.0))
        }.asObservable().bind(to: self.outOfStockView.rx.isHidden).disposed(by: self.disposeBag)
        
        
        output.cartEntry.asObservable().map{(cartEntry) in
            return ((cartEntry.product?.stock?.stockLevel ?? 0.0) <= 0.0)
        }.asObservable().bind(to: self.quantityStackView.rx.isHidden).disposed(by: self.disposeBag)
        
        output.cartEntry.map { (cartEntry) in
            return cartEntry.product?.code ?? ""
        }.asObservable().bind(to: self.code.rx.text).disposed(by: self.disposeBag)

        output.cartEntry.map { (cartEntry) in
            return cartEntry.basePrice?.formattedValue ?? ""
        }.asObservable().bind(to: self.price.rx.text).disposed(by: self.disposeBag)

        output.cartEntry.map { (cartEntry) in
            return String(cartEntry.quantity ?? 1)
        }.asObservable().bind(to: self.qtyLbl.rx.text).disposed(by: self.disposeBag)

        output.cartEntry.map { (cartEntry) in
            return cartEntry.totalPrice?.formattedValue ?? ""
        }.asObservable().bind(to: self.totalPrice.rx.text).disposed(by: self.disposeBag)

        output.cartEntry.asObservable().subscribe(onNext: { (CartEntry) in
            if CartEntry.product?.discount != nil {
 
                self.oldPrice.isHidden = false
                
                self.price.text = CartEntry.product?.discount?.discountPrice?.formattedValue ?? ""
                self.oldPrice.attributedText = CartEntry.product?.discount?.price?.formattedValue?.strikeThrough()
                
                

            }
            else {

                self.oldPrice.isHidden = true
                self.price.text = CartEntry.basePrice?.formattedValue ?? ""

            }

            
            }).disposed(by: disposeBag)
 
        output.cartEntry.asObservable().subscribe(onNext: { (cartEntry) in
            !(cartEntry.product?.express ?? false) ? (self.expressStack.isHidden = true) : (self.expressStack.isHidden = false)
            let productImage =  cartEntry.product?.images.map({ (productImage) -> [CartProductBanner] in
                return productImage.filter ({ (image) -> Bool in
                    image.format == "zoom"
                    
                })
            })

            
            self.img.downloadImageInCellWithoutCorner(urlString: productImage?.first?.url ?? "")
            }).disposed(by: disposeBag)
        
        output.cartEntry.asObservable()
            .map(){
                ($0.product?.images?.first?.url ?? "")
                    .concatWithMainUrl()
        }.flatMapLatest { (url) in
            return KingfisherManager.shared
                .rx
                .retrieveImage(with: URL(string: url)!)
        }.asObservable()
            .bind(to: self.img.rx.image)
            .disposed(by: disposeBag)
        
        output.refreshCart.asObservable().subscribe(onNext: { (cartQuantityUpdate) in
            if cartQuantityUpdate.statusCode == "success" {
                self.refreshCartSubject.onNext(())
            }
            else if cartQuantityUpdate.statusCode == "lowStock" {
                Loaf( "There is no more left" , state: .error(L102Language.isRTL ? .right : .left), sender: UIApplication.topViewController() ?? UIViewController()).show()
               
            }
            
            }).disposed(by: disposeBag)

        self.expressButton.rx.tap.subscribe(onNext: {(void) in
            let alert = UIAlertController(title:"", message: "Always in stock, ready to ship, 60min delivery.".localized(), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Learn More".localized(), style: .default, handler: { _ in
                
                self.navToExpressDeliveryInfoSubject.onNext(())
                        }))
            
            alert.addAction(UIAlertAction(title: "Close".localized(), style: .default ))
            UIApplication.topViewController()?.present(alert, animated: true)
                 
            
        }).disposed(by: disposeBag)
                                            
    
}

}

extension String {
    func concatWithMainUrl()-> String {
        return "\(Environment.shared.apiEndpoint?.rawValue ?? "")\(self)"
    }
}


