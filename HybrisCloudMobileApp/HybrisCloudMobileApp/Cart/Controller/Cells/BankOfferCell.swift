//
//  BankOfferCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 11/2/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

class BankOfferCell: UITableViewCell {
    
    @IBOutlet weak var msg: UILabel!
    @IBOutlet weak var icon: UIImageView!

    @IBOutlet weak var imgWidth: NSLayoutConstraint!
    var bag = DisposeBag()
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }
    
    
    func setUp(msg : String? , imgUrl : String?){
        self.msg.text = msg ?? ""
        
        if imgUrl == nil || imgUrl == ""{
            imgWidth.constant = 0
        }
        else {
           imgWidth.constant = 70
            self.icon.downloadImageInCellWithoutCorner(urlString: imgUrl ?? "")
        }
        
        
 
        
    }
    
}
