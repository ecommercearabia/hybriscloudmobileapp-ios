//
//  cartViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import SwipeCellKit
import KeychainSwift
import RxSwift
import Firebase
class CartViewModel : ViewModelType{
    
    let deleteEntrySubject = PublishSubject<CartEntry>()
    let refreshCartSubject = PublishSubject<Void>()
    let navigate = PublishSubject<Void>()
    let cartCode = PublishSubject<String>()
    var navToExpressDeliveryInfoSubject = PublishSubject<Void>()
    let promoCodeMsgsSubject = PublishSubject<[PromoCodeMsgsSection]>()
    
    struct Input {
        let trigger: Driver<Void>
        let triggerShipmentType: Driver<Void>
        let deleteEntry: Driver<CartEntry>
        let creatCart : Driver<Void>
        let cartCode : Driver<String>
        let checkOut : Driver<Void>
        let navigate : Driver<Void>
        let selectedshipmentType : Driver<String>
        let getCurrentShipmentType : Driver<Void>
      
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let cart: Driver<CartModel>
        let carts: Driver<CartsModel>
        let navigateTo:Driver<String>
        let validationCartErrors:Driver<ValidationCartErrors>
        let error: Driver<Error>
        let bankOffers: Driver<BankOffers>
        let shipmentTypes : Driver<ShipmentTypesModel>
        let selectedShipmentType : Driver<ShipmentSelctedShipmentTypesModel>
        let currentShipmentType : Driver<CurrentShipmentTypeModel>
        let config : Driver<ConfigModel>
        let getUserInformation: Driver<UserData>
        let validationProfileErrors:Driver<ValidationCartErrors>
    }
    
    let bag = DisposeBag()
    
    
    let getCartUseCase: CartUseCase
    let getCartsUseCase: CartsUseCase
    let validationsCartErrors: ValidationCartUseCase
    let bankOffersUseCase: BankOffersCartUseCase
    let shipmentTypesUseCase : ShipmentTypesUseCase
    private let configUseCase: ConfigUseCase
    private let userUseCase: UserUseCase
    init(getCartUseCase: CartUseCase ,getCartsUseCase: CartsUseCase , validationsCartErrors: ValidationCartUseCase , bankOffersUseCase : BankOffersCartUseCase , shipmentTypesUseCase : ShipmentTypesUseCase , configUseCase: ConfigUseCase ,  userUseCase: UserUseCase) {
        self.getCartUseCase = getCartUseCase
        self.getCartsUseCase = getCartsUseCase
        self.validationsCartErrors = validationsCartErrors
        self.bankOffersUseCase = bankOffersUseCase
        self.shipmentTypesUseCase = shipmentTypesUseCase
        self.configUseCase = configUseCase
        self.userUseCase = userUseCase
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        let applyVoucherErrorTracker = ErrorTracker()
        
        let getUserInformation = input.trigger.flatMapLatest{
            
            return self.userUseCase.getCustomerProfile(userId: "current")
            .trackActivity(activityIndicator)
            .trackError(errorTracker)
            .asDriverOnErrorJustComplete()
        }
        
        let config = input.trigger.flatMapLatest{
            return self.configUseCase.config()
                .trackError(errorTracker)
                    .trackActivity(activityIndicator)
                    .asDriverOnErrorJustComplete()

        }
        
        let shipmentTypes = input.trigger.flatMapLatest{ [unowned self] in
            return self.shipmentTypesUseCase.getShipmentTypes()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        let selectedShipmentType = input.selectedshipmentType.flatMapLatest{ [unowned self] in
            return self.shipmentTypesUseCase.selectedShipmentType(code: $0)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        let currentShipmentType = input.getCurrentShipmentType.flatMapLatest{ [unowned self] in
            return self.shipmentTypesUseCase.getType()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let carts = input.trigger.flatMapLatest { [unowned self] in
            return self.getCartsUseCase.getCarts(userId: KeychainSwift().get("user") ?? "anonymous")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        let cart = input.trigger.flatMapLatest {[unowned self] (code)  in
            return self.getCartUseCase.getCart(userId: KeychainSwift().get("user") == "current" ? "current" : "anonymous", cartId: KeychainSwift().get("user") ?? "anonymous" == "current" ? "current" : KeychainSwift().get("guid") ?? "0000"  )
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        let createCart = input.creatCart.flatMapLatest { [unowned self] in
            return self.getCartUseCase.creatCart(userId: KeychainSwift().get("user") ?? "anonymous")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        
        
        let deleteEntry = input.deleteEntry.flatMapLatest { (entry) in
            
            return self.getCartUseCase.deleteCartEntry(entryNumber: "\(entry.entryNumber ?? -1)" , userId: KeychainSwift().get("user") ?? "anonymous", cartId: KeychainSwift().get("user") ?? "anonymous" == "current" ? "current" : KeychainSwift().get("guid") ?? "0000").trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete().flatMapLatest { (_) in
                    return self.getCartUseCase.getCart(userId: KeychainSwift().get("user") ?? "anonymous", cartId:  KeychainSwift().get("user") ?? "anonymous" == "current" ? "current" : KeychainSwift().get("guid") ?? "0000")
                        .trackActivity(activityIndicator)
                        .trackError(errorTracker)
                        .asDriverOnErrorJustComplete()
            }
        }
        let navigateTo = input.navigate.map { () -> String in
            return KeychainSwift().get("user") ?? "anonymous"
        }.flatMap { (user)  in
            return Observable.just( user == "anonymous" ? "login" : "address").trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        
        let validationCartErrors = input.checkOut.flatMapLatest { [unowned self] in
            return self.validationsCartErrors.validationCart(userId: KeychainSwift().get("user") ?? "anonymous" == "current" ? "current" : "anonymous", cartId: KeychainSwift().get("user") ?? "anonymous" == "current" ? "current" : KeychainSwift().get("guid") ?? "")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let validationProfileErrors = input.trigger.flatMapLatest { [unowned self] in
            return self.validationsCartErrors.validationCart(userId: KeychainSwift().get("user") ?? "anonymous" == "current" ? "current" : "anonymous", cartId: KeychainSwift().get("user") ?? "anonymous" == "current" ? "current" : KeychainSwift().get("guid") ?? "")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        
        
        let bankOffers =  input.trigger.flatMapLatest { (void) -> Driver<BankOffers> in
            return self.bankOffersUseCase.bankOffersCart()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        let applyVoucherError = applyVoucherErrorTracker.asDriver()
        
        return Output(fetching: fetching, cart: Driver.merge(cart,deleteEntry,createCart), carts: carts, navigateTo: navigateTo, validationCartErrors: validationCartErrors,
                      error: errors , bankOffers: bankOffers , shipmentTypes: shipmentTypes ,selectedShipmentType: selectedShipmentType , currentShipmentType: currentShipmentType , config: config, getUserInformation: getUserInformation, validationProfileErrors: validationProfileErrors)
    }
    
    func dataSourceCart() ->
        RxTableViewSectionedAnimatedDataSource<CartSection> {
            return RxTableViewSectionedAnimatedDataSource<CartSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .none, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, cv, indexPath, item in
                
                guard let cell = cv.dequeueReusableCell(withIdentifier: "CartItem", for: indexPath) as? CartItem else {
                    return UITableViewCell()
                }
                cell.delegate = self
                cell.setUp()
                cell.cartEntry.onNext(item)
                cell.entryNumber.onNext(String(item.entryNumber ?? 0))
                cell.refreshCartSubject.bind(to: self.refreshCartSubject).disposed(by: cell.disposeBag)
                cell.navToExpressDeliveryInfoSubject.subscribe(onNext: {(void) in
                    self.navToExpressDeliveryInfoSubject.onNext(())
                }).disposed(by: cell.disposeBag)
                return cell
                
            })
    }
    
//    func dataSourceBankOffers() ->
//        RxTableViewSectionedAnimatedDataSource<PromoCodeMsgsSection> {
//            return RxTableViewSectionedAnimatedDataSource<PromoCodeMsgsSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .none, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, cv, indexPath, item in
//
//                switch item {
//
//                case .bankOffer(bankOffer: let bankOffer):
//                    guard let cell = cv.dequeueReusableCell(withIdentifier: "BankOfferCell", for: indexPath) as? BankOfferCell else {
//                        return UITableViewCell()
//                    }
//                    cell.setUp(msg: bankOffer.message ?? "", imgUrl: bankOffer.icon?.url ?? "")
//                    cell.msg.textColor = .black
//                    cell.msg.font = cell.msg.font.withSize(12.0)
//                    cell.layoutIfNeeded()
//                    return cell
//
//                case .potentialOrderPromotion(potentialOrderPromotion: let potentialOrderPromotion):
//                    guard let cell = cv.dequeueReusableCell(withIdentifier: "BankOfferCell", for: indexPath) as? BankOfferCell else {
//                        return UITableViewCell()
//                    }
//                    cell.setUp(msg: potentialOrderPromotion.potentialOrderPromotionDescription ?? "", imgUrl: nil)
//                    cell.msg.textColor = UIColor(named: "AppMainColor")
//                    cell.msg.font = cell.msg.font.withSize(17.0)
//                    cell.layoutIfNeeded()
//                    return cell
//
//                case .appliedOrderPromotion(appliedOrderPromotion: let appliedOrderPromotion):
//                    guard let cell = cv.dequeueReusableCell(withIdentifier: "BankOfferCell", for: indexPath) as? BankOfferCell else {
//                        return UITableViewCell()
//                    }
//                    cell.setUp(msg: appliedOrderPromotion.appliedOrderPromotionDescription ?? "", imgUrl: nil)
//                    cell.msg.textColor = UIColor(named: "AppMainColor")
//                    cell.msg.font = cell.msg.font.withSize(17.0)
//
//                    cell.layoutIfNeeded()
//                    return cell
//
//                }
//             },titleForHeaderInSection: { dataSource, index in
//                let sectionModel = dataSource.sectionModels[index]
//                return "\(sectionModel.model)".localized()
//                 })
//    }
}

extension CartViewModel: SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        guard let  cartEntry = try? tableView.rx.model(at: indexPath) as CartSection.Item else {
            return []
        }
        let deleteBtn = SwipeAction(style: .default, title: "Delete".localized()) { action, index in
            self.deleteEntrySubject.onNext(cartEntry)
            Analytics.logEvent(AnalyticsEventRemoveFromCart, parameters: [
                AnalyticsParameterCurrency: cartEntry.totalPrice?.currencyISO ?? "AED",
                AnalyticsParameterValue: cartEntry.totalPrice?.value ?? 0.0
              ])
        }
        
        deleteBtn.backgroundColor = .red
        deleteBtn.image = UIImage(named: "delete")
        
        return [deleteBtn]
    }
}
