//
//  CartItemViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/19/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import NetworkPlatform
import Domain
import RxSwift
import RxDataSources
import SwipeCellKit
import KeychainSwift


class CartItemViewModel: ViewModelType{
    
    struct Input {
//         let trigger: Driver<Void>
         let cartEntry: Driver<CartEntry>
         let increaseQuantityTrigger: Driver<Void>
         let decreaseQuantityTrigger: Driver<Void>
        let entryNumber : Driver<String>

    }
     
     struct Output {
         let cartEntry: Driver<CartEntry>
         let refreshCart: Driver<CartQuantityUpdate>
        let error: Driver<Error>
        let fetching: Driver<Bool>

     }

    
    let changeQuantityUserCase: CartQuantityUserCase

      init(changeQuantityUserCase: CartQuantityUserCase) {
           self.changeQuantityUserCase = changeQuantityUserCase
       }

    
    func transform(input: Input) -> Output {
        
    let activityIndicator = ActivityIndicator()
    let errorTracker = ErrorTracker()

 
 
        let cartEntryAfterIncreaseQty = Driver.combineLatest(input.cartEntry,input.increaseQuantityTrigger,input.entryNumber).map({ (entry, _, entrynumber) -> (CartEntry, String) in
            var updatedentry = entry
            updatedentry.quantity = (updatedentry.quantity ?? 0) + 1
            return (updatedentry , entrynumber)
        }).flatMapLatest { (cartEntry  , entryNumber) in
         
            return self.changeQuantityUserCase.updateCartQuantity(entryNumber: entryNumber, entry: cartEntry, userId: KeychainSwift().get("user") ?? "anonymous", cartId: (KeychainSwift().get("user") == "current") ? "current" : KeychainSwift().get("guid") ?? "")
            .trackActivity(activityIndicator)
            .trackError(errorTracker)
            .asDriverOnErrorJustComplete()
        }
        let cartEntryAfterDecreaseQty = Driver.combineLatest(input.cartEntry,input.decreaseQuantityTrigger,input.entryNumber).map({ (entry, _, entrynumber) -> (CartEntry, String) in
            var updatedentry = entry
            updatedentry.quantity = (updatedentry.quantity ?? 0) - 1
            return (updatedentry , entrynumber)
        }).flatMapLatest { (cartEntry , entryNumber) in
          
            return self.changeQuantityUserCase.updateCartQuantity(entryNumber: entryNumber, entry: cartEntry, userId: KeychainSwift().get("user") ?? "anonymous", cartId: (KeychainSwift().get("user") == "current") ? "current" : KeychainSwift().get("guid") ?? "")
            .trackActivity(activityIndicator)
                      .trackError(errorTracker)
                      .asDriverOnErrorJustComplete()
            }
          

        
        

 let fetching = activityIndicator.asDriver()
 let errors = errorTracker.asDriver()

        return Output(cartEntry: input.cartEntry , refreshCart: Driver.merge(cartEntryAfterIncreaseQty,cartEntryAfterDecreaseQty) ,error: errors , fetching: fetching)

    }

}
