//
//  VoucherCellVM.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 9/28/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import NetworkPlatform
import Domain
import RxSwift
import RxDataSources
import SwipeCellKit
import KeychainSwift


class VoucherCellVM: ViewModelType{
    
    var activityIndicator : ActivityIndicator?

    struct Input {
          let voucher: Driver<CartAppliedVoucher>
         let deleteVoucher: Driver<Void>
    }
     
     struct Output {
         let deletedVoucher: Driver<Void>
         let refreshCart: Driver<Void>
        let error: Driver<Error>
        let fetching: Driver<Bool>

     }

    
    let CartUseCase: CartUseCase

      init(CartUseCase: CartUseCase) {
           self.CartUseCase = CartUseCase
       }

    
    func transform(input: Input) -> Output {
        
    let errorTracker = ErrorTracker()

 
 
        let deletesVoucher = Driver.combineLatest(input.voucher,input.deleteVoucher ).flatMapLatest { (arg0) -> Driver<Void> in
            
            let (voucher, _) = arg0
            return self.CartUseCase.deletesVoucher(cartId: KeychainSwift().get("user") ?? "anonymous" == "current" ? "current" : KeychainSwift().get("guid") ?? "", userId: KeychainSwift().get("user") ?? "anonymous" == "current" ? "current" : "anonymous", voucherId: voucher.voucherCode ?? "")
                .trackActivity(self.activityIndicator!)
            .trackError(errorTracker)
            .asDriverOnErrorJustComplete()
        }

        
        

        let fetching = activityIndicator!.asDriver()
 let errors = errorTracker.asDriver()

        return Output(deletedVoucher: deletesVoucher, refreshCart: deletesVoucher, error: errors, fetching: fetching)
    }

}
