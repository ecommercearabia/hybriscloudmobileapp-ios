//
//  AddAddressMapViewController.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/6/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import GoogleMaps


class AddAddressMapViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var confirmBarButton: UIBarButtonItem!
    @IBOutlet weak var closeBarButton: UIBarButtonItem!
   
   var isFirstTime = true

    var line1 :String = ""
    var locality :String? = ""
    var sublocality :String? = ""

    private let locationManager = CLLocationManager()

    var line1BehaviorSubject  = BehaviorSubject<String>(value: "")
    var localityBehaviorSubject  = BehaviorSubject<String>(value: "")
    var subLocalityBehaviorSubject  = BehaviorSubject<String>(value: "")

    var successBehaviorSubject  = BehaviorSubject<Bool>(value: false)

    var goToAddAddress: Bool = true
    
    override func viewDidLoad() {
            super.viewDidLoad()
        
              self.title = "My Location"
        self.mapView.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: 23.559365, longitude: 54.051919), zoom: 6.5, bearing: 0, viewingAngle: 0)

        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()

        }
        
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = true
            mapView.delegate = self
        }
    
    private func handleMyLocation(_ coordinate: CLLocationCoordinate2D) {
        print(coordinate.latitude)
        print(coordinate.longitude)
        
 
        let geocoder = GMSGeocoder()
          geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else { return }
                          
            if(((address.country ?? "") == "United Arab Emirates" ) || ((address.country ?? "") == "الإمارات العربية المتحدة")) {
                self.successBehaviorSubject.onNext(true)
                self.locality = address.locality ?? ""
                self.sublocality = address.subLocality ?? ""

                    self.line1 = lines.joined(separator: "\n")
                
                if self.isFirstTime {
                    self.isFirstTime = false
                    let marker =  GMSMarker(position: coordinate)
                    self.mapView.clear()
                    marker.map = self.mapView
                }
                UIView.animate(withDuration: 0.25) {
                    self.view.layoutIfNeeded()
                }


             }else {

            self.mapView.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: 23.559365, longitude: 54.051919), zoom: 6.5, bearing: 0, viewingAngle: 0)
        self.isFirstTime = false
                let marker =  GMSMarker(position:  CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: 23.559365)!, longitude: CLLocationDegrees(exactly: 54.051919)!))
                
                geocoder.reverseGeocodeCoordinate(CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: 23.559365)!, longitude: CLLocationDegrees(exactly: 54.051919)!)) { response, error in
                guard let address = response?.firstResult(), let lines = address.lines else { return }
                              
                     self.successBehaviorSubject.onNext(true)
                    self.locality = address.locality ?? ""
                    self.sublocality = address.subLocality ?? ""
                    self.line1 = lines.joined(separator: "\n")
                    
                     UIView.animate(withDuration: 0.25) {
                        self.view.layoutIfNeeded()
                    }
                }

 
        self.mapView.clear()
        marker.map = self.mapView

        UIView.animate(withDuration: 0.25) {
        self.view.layoutIfNeeded()
    }
                self.successBehaviorSubject.onNext(true)

 
            }
          }

        }
    

  

    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {

      let geocoder = GMSGeocoder()
      geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
        guard let address = response?.firstResult(), let lines = address.lines else {
            self.errorLoaf(message: "Delivary not avalible here")
            return }
                  
        if(((address.country ?? "") == "United Arab Emirates" ) || ((address.country ?? "") == "الإمارات العربية المتحدة"))
        {
            self.successBehaviorSubject.onNext(true)
            self.locality = address.locality ?? ""
            self.sublocality = address.subLocality ?? ""

            self.line1 = lines.joined(separator: "\n")

            let marker =  GMSMarker(position: coordinate)
            self.mapView.clear()
            marker.map = self.mapView
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }else {
            
            self.successBehaviorSubject.onNext(false)
            self.errorLoaf(message: "Delivary not avalible here")
            }
        
      }

    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func confirmButtonTapped(_ sender: Any) {
        
        
        let isSuccess :Bool? = try? successBehaviorSubject.value()
        
        if(isSuccess ?? false){
            line1BehaviorSubject.onNext(line1)
            localityBehaviorSubject.onNext(locality ?? "")
//            subLocalityBehaviorSubject.onNext(sublocality ?? "")
            if(goToAddAddress)
            {
                performSegue(withIdentifier: "addAddressSegue", sender: nil)
            }else {
                self.navigationController?.popViewController(animated: true)
            }
        }else {
            errorLoaf(message: "Please select valid delivary address ")
        }
        
    
    }

   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let addAddressUIStoryboardSegue = segue.destination as? AddAddressViewController {
            addAddressUIStoryboardSegue.line1FromMap = line1
            addAddressUIStoryboardSegue.locality = locality ?? ""
            addAddressUIStoryboardSegue.sublocality = sublocality ?? ""

        }
        
       
    }
    
}



extension AddAddressMapViewController: CLLocationManagerDelegate {
 
     func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
         locationManager.startUpdatingLocation()
     }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        handleMyLocation(CLLocationCoordinate2D(latitude: manager.location?.coordinate.latitude ?? 0.0, longitude: manager.location?.coordinate.longitude ?? 0.0))
        locationManager.stopUpdatingLocation()

    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.mapView.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: 23.559365, longitude: 54.051919), zoom: 6.5, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
        print(error.localizedDescription)
    }
    

}

extension AddAddressMapViewController: GMSMapViewDelegate {
    

    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.reverseGeocodeCoordinate(coordinate)
       
        
    }

    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
         }
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        return false
    }
}

