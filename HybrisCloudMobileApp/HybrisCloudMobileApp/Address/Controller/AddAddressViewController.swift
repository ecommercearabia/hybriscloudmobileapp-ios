//
//  AddAddressViewController.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/6/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import UIKit
import Domain
import RxSwift
import RxCocoa
import NetworkPlatform
import GooglePlaces

class AddAddressViewController: UIViewController , UITextFieldDelegate {
    
    let countryPickerView = UIPickerView()
    let cityPickerView = UIPickerView()
//    let areaPickerView = UIPickerView()
    let codePickerView = UIPickerView()
    let titlePickerView = UIPickerView()
    
    let disposeBag = DisposeBag()
    var viewModel : AddAddressViewModel!
    
    var shippingCountrySubject = PublishSubject<Country>()
    var citySubject = PublishSubject<City>()
    var mobileCountrySubject = PublishSubject<Country>()
    var titleSubject = PublishSubject<Title>()
    var personalDetailsSubject = PublishSubject<UserData>()
    var areaSubject = BehaviorSubject<Area?>(value : nil)
    var isDeafultAddressSubject = BehaviorSubject<Bool>(value: false)
    let addAddressManual = PublishSubject<Void>()
    
    var areas = [Area]()
    let token = GMSAutocompleteSessionToken.init()

    @IBOutlet weak var goToMap: UIButton!
    @IBOutlet weak var saveBarButton: UIButton!
    @IBOutlet weak var checkBoxDefaultAddress: UIButton!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var addressLine1TextField: UITextField!
    @IBOutlet weak var AddressLine2TextField: UITextField!
    @IBOutlet weak var PostCodeTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var areaTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var AddressNameTextField: UITextField!
    @IBOutlet weak var NearstLandmarkTextField: UITextField!
    @IBOutlet weak var BuildingNameTextField: UITextField!
    @IBOutlet weak var streetNameTextField: CustomSearchTextField!
    @IBOutlet weak var apartmentNumberTextField: UITextField!
    @IBOutlet weak var scrollVIew: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!
    
    
    var line1FromMap :String = ""
    var locality :String = ""
    var sublocality :String = ""
 
     override func viewDidLoad() {
        super.viewDidLoad()
        
//        if(!(line1FromMap.isEmpty))
//        {
//            self.streetNameTextField.text = line1FromMap
//        }
        
         
        
        AddressNameTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        titleTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        firstNameTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        lastNameTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        countryTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        cityTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
//        areaTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        addressLine1TextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        AddressLine2TextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        PostCodeTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        NearstLandmarkTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        BuildingNameTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        streetNameTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        apartmentNumberTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        codeTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        phoneNumberTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        numberTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        
        
 
        areaTextField.addTarget(self, action: #selector(onClickAreaNameTextField), for: .editingDidBegin)

        
         streetNameTextField.addTarget(self, action: #selector(onClickStreetNameTextField), for: .editingChanged)


        self.streetNameTextField.didSelectAction = {
            self.scrollVIew.isScrollEnabled = true
            self.streetNameTextField.sendActions(for: .valueChanged)
        }
 
        self.codeTextField.isUserInteractionEnabled = false
        self.titleTextField.isUserInteractionEnabled = false
        self.countryTextField.isUserInteractionEnabled = false
        self.cityTextField.isUserInteractionEnabled = false
        self.areaTextField.isUserInteractionEnabled = false
        

        goToMap.rx.tap.subscribe(onNext: { (void) in
           
              self.performSegue(withIdentifier: "goToMapFromAdd", sender: nil)
            
        }).disposed(by: disposeBag)
        
        
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        
        viewModel = AddAddressViewModel(
            countryUseCase: networkUseCaseProvider.makeCountryUseCase(),
            addressesUseCase:networkUseCaseProvider.makeAddAddressesUseCase(),
            citiesUseCase:networkUseCaseProvider.makeCitiesUseCase(),
            titleCase: networkUseCaseProvider.makeTitleUseCase(),
            userUseCase: networkUseCaseProvider.makeUserUseCase())
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        
        let input = AddAddressViewModel.Input(
            trigger: Driver.merge(viewWillAppear),
            saveTrigger: addAddressManual.asDriverOnErrorJustComplete(),
            validation: saveBarButton!.rx.tap.asDriver(),
            country: shippingCountrySubject.asDriverOnErrorJustComplete(),
            addressName : AddressNameTextField.rx.text.orEmpty.asDriver(),
            nearstLandmark : NearstLandmarkTextField.rx.text.orEmpty.asDriver() ,
            isDefaultAddress : isDeafultAddressSubject.asDriverOnErrorJustComplete() ,
            title :titleSubject.asDriverOnErrorJustComplete() ,
            firstName: firstNameTextField.rx.text.orEmpty.asDriver(),
            lastName: lastNameTextField.rx.text.orEmpty.asDriver(),
            mobileCountry: mobileCountrySubject.asDriverOnErrorJustComplete(),
            addressLineOne: addressLine1TextField.rx.text.orEmpty.asDriver(),
            addressLineTow: AddressLine2TextField.rx.text.orEmpty.asDriver(),
            city: citySubject.asDriverOnErrorJustComplete(),
            area: areaSubject.asDriverOnErrorJustComplete(),
            postCode: PostCodeTextField.rx.text.orEmpty.asDriver(),
            mobileNum: numberTextField.rx.text.orEmpty.asDriver(),
            buildingName: BuildingNameTextField.rx.text.orEmpty.asDriver(),
            streetName: streetNameTextField.rx.text.orEmpty.asDriver(),
            apartmentNumber: apartmentNumberTextField.rx.text.orEmpty.asDriver(),
            personalDetails: personalDetailsSubject.asDriverOnErrorJustComplete()
            
        )
        
        let output = viewModel.transform(input: input)
        
        
        
        
        output.validation.asObservable().subscribe(onNext: { (msg) in
            if msg != "" {
                self.errorLoaf(message: msg)
            }
            else {
                self.addAddressManual.onNext(())
            }
            }).disposed(by: disposeBag)
        
        
        output.error.asObservable().subscribe(onNext: { (error) in
            
            let errorInModel = error as? ErrorResponse
            
            let  msgError =  errorInModel?.errors?.map({ (errorResponseElement) -> String in
                return errorResponseElement.message ?? ""
            }).joined(separator: " , ")
            
            self.errorLoaf(message: msgError ?? "" )
            
        }).disposed(by: disposeBag)
        
        output.country.asObservable().bind(to: countryPickerView.rx.itemTitles){ _, item in
            return item.name
        } .disposed(by: disposeBag)
        
        output.country.asObservable().subscribe(onNext: {[unowned self] (countries) in
            self.countryPickerView.selectRow(0, inComponent: 0, animated: true)
            if(countries.count != 0)
            { if let country = countries.first{
                self.shippingCountrySubject.onNext(country)
                self.countryTextField.text =  country.name
                if(countries.count != 1)
                {
                    self.countryTextField.inputView = self.countryPickerView
                    self.countryTextField.isUserInteractionEnabled = true
                }
                
                }
            }
        }).disposed(by: disposeBag)
        
        
        output.title.asObservable().map({ (arg0) -> [Title] in
            
            let (list, _) = arg0
            return list
        }).bind(to: titlePickerView.rx.itemTitles){ _, item in
            
            return item.name
        } .disposed(by: disposeBag)
        
        output.title.asObservable().subscribe(onNext: {[unowned self] (titles , titleSelected) in
            if(titles.count != 0 )
            {
                self.titlePickerView.selectRow(self.getSelectedTitle(titles: titles, selectedTitle: titleSelected), inComponent: 0 , animated: true)
                
                self.titlePickerView.delegate?.pickerView?(self.titlePickerView, didSelectRow: self.getSelectedTitle(titles: titles, selectedTitle: titleSelected), inComponent: 0)
                if(titles.count != 1 )
                {
                    self.titleTextField.inputView = self.titlePickerView
                    self.titleTextField.isUserInteractionEnabled = true
                }
            }
        }).disposed(by: disposeBag)
        
        
        
        
        
        output.city.asObservable().bind(to: cityPickerView.rx.itemTitles){ _, item in
            return item.name
        } .disposed(by: disposeBag)
        
        output.city.asObservable().subscribe(onNext: {[unowned self] (city) in
            var selectedIndex = 0
        
            for (index, element) in city.enumerated() {
                if element.name == self.locality {
                    selectedIndex = index
                }
                
            }
            
            self.cityPickerView.selectRow(selectedIndex, inComponent:0 , animated: true)
            self.cityPickerView.delegate?.pickerView?(self.cityPickerView, didSelectRow: selectedIndex, inComponent: 0)
            self.cityTextField.inputView = self.cityPickerView
            self.cityTextField.isUserInteractionEnabled = true
            
        }).disposed(by: disposeBag)
        
        
//        output.area.asObservable().bind(to: areaPickerView.rx.itemTitles){ _, item in
//            return item.name
//        } .disposed(by: disposeBag)
        
        output.area.asObservable().subscribe(onNext: {[unowned self] (area) in
            var selectedIndex = 0
            self.areas = area
            for (index, element) in area.enumerated() {
                if element.name == self.sublocality {
                    selectedIndex = index
                }
                
            }
            
//            self.areaPickerView.selectRow(selectedIndex, inComponent:0 , animated: true)
//            
//            self.areaPickerView.delegate?.pickerView?(self.areaPickerView, didSelectRow: selectedIndex, inComponent: 0)
            if(area.count != 1){
//            self.areaTextField.inputView = self.areaPickerView
            self.areaTextField.isUserInteractionEnabled = true
            }else {
                
                self.areaTextField.isUserInteractionEnabled = false
                
                self.areaTextField.text = ""
                
                self.areaSubject.onNext(nil)
                
            }
        }).disposed(by: disposeBag)
        
        
        
        output.getUserInformation.asObservable().map { (User) -> String in
            return User.firstName ?? ""
            
        }.bind(to: firstNameTextField.rx.text).disposed(by: disposeBag)
        
        output.getUserInformation.asObservable().map { (User) -> String in
            return User.lastName ?? ""
            
        }.bind(to: lastNameTextField.rx.text).disposed(by: disposeBag)
        
        output.getUserInformation.asObservable().subscribe(onNext: { (user) in
            self.personalDetailsSubject.onNext(user)
        }).disposed(by: disposeBag)
        
        output.codePhone.asObservable().bind(to: codePickerView.rx.itemTitles){ _, item in
            return item.isdcode
        } .disposed(by: disposeBag)
        
        output.codePhone.asObservable().subscribe(onNext: {[unowned self] (countries) in
            self.codePickerView.selectRow(0, inComponent: 0, animated: true)
            if(countries.count != 0){
                if let country = countries.first{
                    
                    self.mobileCountrySubject.onNext(country)
                    self.codeTextField.text = country.isdcode
                    if(countries.count != 1){
                        self.codeTextField.inputView = self.codePickerView
                        self.codeTextField.isUserInteractionEnabled = true
                    }
                }
            }
        }).disposed(by: disposeBag)
        
        
        countryPickerView.rx.modelSelected(Country.self).map { (countries) -> Country in
            return countries.first ?? Country(isdcode: nil, isocode: nil, name: nil)
        }.bind(to: shippingCountrySubject).disposed(by: disposeBag)
        
        countryPickerView.rx.modelSelected(Country.self).map { (countries) -> String in
            return countries.first?.name ?? ""
        }.bind(to: countryTextField.rx.text).disposed(by: disposeBag)
        
        
        
        titlePickerView.rx.modelSelected(Title.self).map { (titles) -> Title in
            return titles.first ?? Title(code: nil, name: nil)
        }.bind(to: titleSubject).disposed(by: disposeBag)
        
        titlePickerView.rx.modelSelected(Title.self).map { (titles) -> String in
            return titles.first?.name ?? ""
        }.bind(to: titleTextField.rx.text).disposed(by: disposeBag)
        
        
        
        
//        areaPickerView.rx.modelSelected(Area.self).map { (area) -> Area in
//            return area.first ?? Area(code: nil, name: nil)
//        }.bind(to: areaSubject).disposed(by: disposeBag)
//
//        areaPickerView.rx.modelSelected(Area.self).map { (area) -> String in
//            return area.first?.name ?? ""
//        }.bind(to: areaTextField.rx.text).disposed(by: disposeBag)
        
        
        
        cityPickerView.rx.modelSelected(City.self).map { (cities) -> City in
            return cities.first ?? City(areas: nil, code: nil, name: nil)
        }.bind(to: citySubject).disposed(by: disposeBag)
        
        cityPickerView.rx.modelSelected(City.self).map { (cities) -> String in
            return cities.first?.name ?? ""
        }.bind(to: cityTextField.rx.text).disposed(by: disposeBag)
        
        
        
        codePickerView.rx.modelSelected(Country.self).map { (countries) -> Country in
            return countries.first ?? Country(isdcode: nil, isocode: nil, name: nil)
        }.bind(to: mobileCountrySubject).disposed(by: disposeBag)
        
        codePickerView.rx.modelSelected(Country.self).map { (countries) -> String in
            return countries.first?.isdcode ?? ""
        }.bind(to: codeTextField.rx.text).disposed(by: disposeBag)
        
        output.addAddress.asObservable().subscribe(onNext: { (address) in
            self.navigationController?.backToViewController(vc: DeliveryAddressViewController.self)
        }).disposed(by: disposeBag)
        
        countryPickerView.rx.modelSelected(Country.self)
            .subscribe(onNext: { models in
                self.countryTextField.text =  models.first?.name
            }).disposed(by: disposeBag)
        
        titlePickerView.rx.modelSelected(Title.self)
            .subscribe(onNext: { models in
                self.titleTextField.text =  models.first?.name
            }).disposed(by: disposeBag)
        
        cityPickerView.rx.modelSelected(City.self)
            .subscribe(onNext: { models in
                self.areaTextField.text = ""
                self.areaTextField.sendActions(for: .valueChanged)
                self.areaSubject.onNext(nil)

                self.cityTextField.text =  models.first?.name
            }).disposed(by: disposeBag)
        
        
//        areaPickerView.rx.modelSelected(Area.self)
//            .subscribe(onNext: { models in
//                if(models.count != 0)
//                {
//                    self.areaTextField.text =  models.first?.name
//                    if(models.count != 1)
//                    {
//                        self.areaTextField.inputView = self.areaPickerView
//                        self.areaTextField.isUserInteractionEnabled = true
//                    }
//                }else {
//                    self.areaTextField.text =  ""
//                    self.areaTextField.isUserInteractionEnabled = false
//                    self.areaSubject.onNext(nil)
//                    
//                    
//                }
//            }).disposed(by: disposeBag)
        
        codePickerView.rx.modelSelected(Country.self)
            .subscribe(onNext: { models in
                self.codeTextField.text =  models.first?.isdcode
            }).disposed(by: disposeBag)
  

    }
    
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
 
    @objc func onClickStreetNameTextField(textField: UITextField) {
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
          //  .establishment
        filter.country = "AE"
        

        let placesClient = GMSPlacesClient()
        
        let bottomOffset = CGPoint(x: 0, y: (self.navigationController?.navigationBar.frame.maxX ?? 0.0) - 50)
        scrollVIew.setContentOffset(bottomOffset, animated: false)
        self.scrollVIew.isScrollEnabled = false

        
        placesClient.findAutocompletePredictions(fromQuery: self.streetNameTextField.text ?? "", filter: filter, sessionToken: self.token , callback: { (results, error) in
                        if let error = error {
                          print("Autocomplete error: \(error)")
                          return
                        }

            
            var list = [AddressFromGoogle]()

                        if let results = results {
                          for result in results {
                            print("Result \(result.attributedFullText) with placeID \(result.placeID)")
                            list.append(AddressFromGoogle(address: result.attributedPrimaryText.string , street: result.attributedSecondaryText?.string ?? ""))
                           }
                            self.streetNameTextField.updateList(list: list)

                        }
                    })

        
 

    }

    
    @objc func onClickAreaNameTextField(textField: UITextField) {
performSegue(withIdentifier: "toList", sender: nil)

    }
    


    
    func getSelectedTitle(titles : [Title], selectedTitle : Title?) -> Int{
        
        var indexSelected = 0
        
        if ((selectedTitle) != nil)
        {
            for (index, element) in titles.enumerated() {
                if (element ==  selectedTitle)
                {
                    indexSelected = index
                    
                }
            }
        }else {
            indexSelected = 0
        }
        titleSubject.onNext(titles[indexSelected])
        
        return indexSelected
    }
    @IBAction func makeAsDefult(_ sender: Any) {
        
        let button = sender as? UIButton
        button?.isSelected.toggle()
        isDeafultAddressSubject.onNext(button?.isSelected ?? false)
    }
    

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToMapFromAdd"{
            
            if let vc = segue.destination as? AddAddressMapViewController{
                vc.goToAddAddress = false
                 vc.line1BehaviorSubject.subscribe(onNext: { (text) in
                    self.streetNameTextField.text = text
                    self.streetNameTextField.sendActions(for: .valueChanged)

                    }).disposed(by: disposeBag)
            }
        }
        
        if segue.identifier == "toList"{
            if let vc = segue.destination as? ListVC{
                vc.fullList = self.areas
                vc.selectedAreaSubject.subscribe { (area) in
                    self.areaSubject.onNext(area)
                    self.areaTextField.text = area.name ?? ""
                    self.areaTextField.sendActions(for: .valueChanged)
                    vc.dismiss(animated: true, completion: nil)
                } onError: { (Error) in
                    print("error on get area selected")
                } onCompleted: {
                    print("completed")
                } onDisposed: {
                    print("Disposed")
                }.disposed(by: vc.bag)


             }
        }

 
        
    }
    
    
    @objc func doneButtonClicked(_ textField: UITextField) {
    
        switch textField{
            
        case AddressNameTextField:
            
            if(self.titleTextField.isUserInteractionEnabled)
            {self.titleTextField.becomeFirstResponder()}
            else{self.firstNameTextField.becomeFirstResponder()}
        
        case titleTextField: self.firstNameTextField.becomeFirstResponder()
        
        case firstNameTextField:lastNameTextField.becomeFirstResponder()
        
        case lastNameTextField:
            if(countryTextField.isUserInteractionEnabled)
            {countryTextField.becomeFirstResponder()}
            else {
            if(cityTextField.isUserInteractionEnabled)
            {cityTextField.becomeFirstResponder()}
            else {
                if(areaTextField.isUserInteractionEnabled)
                {areaTextField.becomeFirstResponder()}
                else {addressLine1TextField.becomeFirstResponder()}
            }
            }
            
        case countryTextField:
            if(cityTextField.isUserInteractionEnabled)
            {
                cityTextField.becomeFirstResponder()
            }
            else {
            if(areaTextField.isUserInteractionEnabled)
            {areaTextField.becomeFirstResponder()}
            else {addressLine1TextField.becomeFirstResponder()}
            }
            
        case cityTextField:
            if(areaTextField.isUserInteractionEnabled)
            {
//            areaTextField.becomeFirstResponder()
            }
            else {addressLine1TextField.becomeFirstResponder()}
       
        case areaTextField:addressLine1TextField.becomeFirstResponder()
            view.endEditing(true)
      
        case addressLine1TextField:AddressLine2TextField.becomeFirstResponder()
      
        case AddressLine2TextField:NearstLandmarkTextField.becomeFirstResponder()
        
        case NearstLandmarkTextField:apartmentNumberTextField.becomeFirstResponder()

//        case BuildingNameTextField:
//            apartmentNumberTextField.becomeFirstResponder()
  break
        case streetNameTextField:
            NearstLandmarkTextField.becomeFirstResponder()
            self.scrollVIew.isScrollEnabled = true
        case apartmentNumberTextField:
            if(codeTextField.isUserInteractionEnabled)
            {codeTextField.becomeFirstResponder()}
            else {phoneNumberTextField.becomeFirstResponder()}
       
        case codeTextField:numberTextField.becomeFirstResponder()
            
        default:
            break
            
        }
    }
    
    
    
    
}

 
