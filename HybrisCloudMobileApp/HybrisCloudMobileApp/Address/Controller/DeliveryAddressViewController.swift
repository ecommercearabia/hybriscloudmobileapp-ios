//
//  DeliveryAddressViewController.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/4/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import RxSwift
import RxCocoa
import NetworkPlatform
import UIKit
//import ProgressHUD

class DeliveryAddressViewController: UIViewController {
   
    let bag = DisposeBag()
    var isFromCart  = Bool()
    var cartId = String()
     
    @IBOutlet weak var addressTableView: UITableView!{
        didSet{
            addressTableView.tableFooterView = UIView.init(frame: CGRect.zero)
            addressTableView.rx.setDelegate(self).disposed(by: self.bag)
        }
    }
    @IBOutlet weak var noAddressView: UIView!

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
       
        self.title = "Delivery Address".localized()

        let viewModel:DeliveryAddressViewModel = DeliveryAddressViewModel(useCase: NetworkPlatform.UseCaseProvider().makeAddressesUseCase(), deliveryAddressUseCase: NetworkPlatform.UseCaseProvider().makeChickOutUseCase(), isFromCart : isFromCart)
         
        addressTableView.refreshControl = UIRefreshControl()

        
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
              .mapToVoid()
              .asDriverOnErrorJustComplete()
        
        
        let pull = addressTableView.refreshControl!.rx
              .controlEvent(.valueChanged)
              .asDriver()

        let input = DeliveryAddressViewModel.Input(trigger: Driver.merge(viewWillAppear, pull), selectedAddress: self.addressTableView.rx.modelSelected(AddressSection.Item.self).asDriver())

        let output = viewModel.transform(input: input)
       
        output.addresses.asObservable().subscribe(onNext: { (address) in
            if((address.addresses ?? []).isEmpty)
            {
                self.noAddressView.isHidden = false
                self.addressTableView.isHidden = true
            }else {
                self.noAddressView.isHidden = true
                self.addressTableView.isHidden = false

            }
           }, onError: { (Error) in
                print("test")
           }).disposed(by: bag)

    
        
        output.refreshAddress.asObservable().subscribe(onNext: { (address) in
                    self.viewWillAppear(true)
        }).disposed(by: bag)
        
        output.fetching
                  .drive(addressTableView.refreshControl!.rx.isRefreshing)
                  .disposed(by: bag)
        
        output.error.asObservable().subscribe(onNext: { (error) in
       
            let errorInModel = error as? ErrorResponse
            
            let  msgError =  errorInModel?.errors?.map({ (errorResponseElement) -> String in
                          return errorResponseElement.message ?? ""
                      }).joined(separator: " , ")
                      
            self.errorLoaf(message: msgError ?? "" )
            
            }).disposed(by: bag)
        
        output.addresses.map { (address) -> [AddressSection] in
            
            return [AddressSection(header: "", items: address.addresses  ?? [])]
            }.asObservable().bind(to: addressTableView.rx.items(dataSource: viewModel.dataSourceDeliveryAddress())).disposed(by: bag)

    
        Observable.zip(output.setDeliveryAddress.asObservable() , self.addressTableView.rx.modelSelected(Address.self)).subscribe(onNext: { (arg0) in
            let (_, address) = arg0
            self.performSegue(withIdentifier: "toChickOutStep2", sender: address)

        }).disposed(by: self.bag)
        
       output.editAddress.asObservable().subscribe(onNext: { (address) in
          self.performSegue(withIdentifier: "goToEditAddress", sender: address)
                  }, onError: { (Error) in
                      print(Error)
                      }).disposed(by: bag)

    }
    
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "goToEditAddress"{
               if let editAddressViewController = segue.destination as? EditAddressViewController {
                guard let address =  sender as? Address else {
                    return
                }
                editAddressViewController.addressBehaviorSubject = BehaviorSubject<Address>(value: address)
               }
           }
            else {
            if let ChickOut = segue.destination as? ChickOutStep2ViewController {
             guard let address =  sender as? Address else {
                 return
             }
             ChickOut.Address = address
//                ChickOut.cartId = self.cartId

            }

            
        }
    
       }
   
    
}

extension DeliveryAddressViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
           cell.alpha = 0
           cell.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
           UIView.animate(
               withDuration: 0.5,
               delay: 0.01 * Double(indexPath.row),
               animations: {
                   cell.alpha = 1
                   cell.transform = CGAffineTransform.identity

           })

    }
 
}

