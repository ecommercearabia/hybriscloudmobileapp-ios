//
//  CustomSearchTextField.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 1/26/21.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import UIKit
import CoreData

struct AddressFromGoogle {
    let address : String?
    let street : String?
}

class CustomSearchTextField: UITextField{
    
    var dataList : [AddressFromGoogle] = [AddressFromGoogle]()
    var tableView: UITableView?
    
    var didSelectAction : (() -> Void)? = nil

    
    // Connecting the new element to the parent view
    open override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        tableView?.removeFromSuperview()
     }
    
    override open func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        
        self.addTarget(self, action: #selector(CustomSearchTextField.textFieldDidChange), for: .editingChanged)
        self.addTarget(self, action: #selector(CustomSearchTextField.textFieldDidBeginEditing), for: .editingDidBegin)
        self.addTarget(self, action: #selector(CustomSearchTextField.textFieldDidEndEditing), for: .editingDidEnd)
        self.addTarget(self, action: #selector(CustomSearchTextField.textFieldDidEndEditingOnExit), for: .editingDidEndOnExit)
    }
    
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        buildSearchTableView()

        
    }
    
    
    //////////////////////////////////////////////////////////////////////////////
    // Text Field related methods
    //////////////////////////////////////////////////////////////////////////////
    
    @objc open func textFieldDidChange(){
        print("Text changed ...")
        
        if (self.text?.count ?? 0) > 0 {
            tableView?.isHidden = false
        }
        else {
            tableView?.isHidden = true
        }
        
    }
    
    func updateList (list : [AddressFromGoogle]){
        self.dataList = list
        self.tableView?.reloadData()
        updateSearchTableView()

    }
    
    @objc open func textFieldDidBeginEditing() {
        print("Begin Editing")
        
    }
    
    @objc open func textFieldDidEndEditing() {
        print("End editing")
        tableView?.isHidden = true


    }
    
    @objc open func textFieldDidEndEditingOnExit() {
        print("End on Exit")
        tableView?.isHidden = true

    }
    
 
 

}

extension CustomSearchTextField: UITableViewDelegate, UITableViewDataSource {
    

 
    // Create SearchTableview
    func buildSearchTableView() {

        if let tableView = tableView {
             tableView.register(UINib(nibName: "CustomSearchTextFieldCell", bundle: nil), forCellReuseIdentifier: "CustomSearchTextFieldCell")

            tableView.delegate = self
            tableView.dataSource = self
            self.window?.addSubview(tableView)

        } else {
            //addData()
            print("tableView created")
            tableView = UITableView(frame: CGRect.zero)
        }
        
        updateSearchTableView()
    }
    
    // Updating SearchtableView
    func updateSearchTableView() {
 
        if let tableView = tableView {
            superview?.bringSubviewToFront(tableView)
            var tableHeight: CGFloat = 0
            tableHeight = tableView.contentSize.height
            
            // Set a bottom margin of 10p
            if tableHeight < tableView.contentSize.height {
                tableHeight -= 0
            }
            
            var addedHight = CGFloat()
            if self.dataList.count > 0 {
                addedHight = 50.0
            }
            else {
                addedHight = 0.0
            }
            
            
            // Set tableView frame
            var tableViewFrame = CGRect(x: 0, y: 0, width: frame.size.width - 4, height: tableHeight + addedHight)
            tableViewFrame.origin = self.convert(tableViewFrame.origin, to: nil)
            tableViewFrame.origin.x += 2
            tableViewFrame.origin.y += frame.size.height + 10
            UIView.animate(withDuration: 0.2, animations: { [weak self] in
                self?.tableView?.frame = tableViewFrame
            })
            
            //Setting tableView style
            tableView.layer.masksToBounds = true
            tableView.separatorInset = UIEdgeInsets.zero
            tableView.layer.cornerRadius = 5.0
            tableView.separatorColor = UIColor.lightGray
            tableView.backgroundColor = UIColor.white.withAlphaComponent(0.4)
            
            if self.isFirstResponder {
                superview?.bringSubviewToFront(self)
            }
            
            tableView.reloadData()
        }
    }
    
    
    
    // MARK: TableViewDataSource methods
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return dataList.count
    }
    
    // MARK: TableViewDelegate methods
    
    //Adding rows in the tableview with the data from dataList

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomSearchTextFieldCell", for: indexPath) as! CustomSearchTextFieldCell
//        cell.backgroundColor = UIColor.clear
        cell.address.text = (dataList[indexPath.row].address ?? "") + " " + (dataList[indexPath.row].street ?? "")
        cell.street.text = dataList[indexPath.row].address ?? ""
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row")
        self.text = "\(dataList[indexPath.row].address ?? "") \(dataList[indexPath.row].street ?? "")"
        tableView.isHidden = true
        self.endEditing(true)
        if let didSelectAction = self.didSelectAction
        {
            didSelectAction()
        }
    }
    

 
}
