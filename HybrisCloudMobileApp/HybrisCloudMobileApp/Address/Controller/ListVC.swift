//
//  ListVC.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 1/20/21.
//  Copyright © 2021 Erabia. All rights reserved.
//

import UIKit
class ListVC: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var listSubject = BehaviorSubject<[Area]>(value: [])
    var fullList = [Area]()
    let selectedAreaSubject = PublishSubject<Area>()
    let bag = DisposeBag()
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        view.endEditing(true)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.becomeFirstResponder()
      
              NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.listSubject.onNext(fullList)
       
   
 
        self.listSubject.bind(to: tableView.rx.items(cellIdentifier: "listCell", cellType: listCell.self)){
            row ,element , cell in
            cell.setUp(item: element)
        }.disposed(by: self.bag)
        
        self.tableView.rx.modelSelected(Area.self).subscribe { (area) in
            print("selected area : \(area.name ?? "")")
            self.selectedAreaSubject.onNext(area)
        } onError: { (Error) in
            print("error on get area selected")
        } onCompleted: {
            print("completed")
        } onDisposed: {
            print("Disposed")
        }.disposed(by: self.bag)
        
        
        searchBar.rx.searchButtonClicked.subscribe(onNext: {(void) in
            self.view.endEditing(true)
        }).disposed(by: bag)
        
        searchBar.rx.text.orEmpty.subscribe(onNext: { (text) in
                if text.isEmpty {
                    self.listSubject.onNext(self.fullList)
                }
                else {
                    let fulterd = self.fullList.filter { (($0.name?.lowercased())?.contains(text.lowercased()) ?? false) }
                    self.listSubject.onNext(fulterd)
                }
            }).disposed(by: self.bag)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            UIView.animate(withDuration: 0.2, animations: {
                self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
            })
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.2, animations: {
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        })
    }
    
}
