//
//  listCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 1/20/21.
//  Copyright © 2021 Erabia. All rights reserved.
//

import UIKit

class listCell: UITableViewCell {

    @IBOutlet weak var lbl: UILabel!


    var bag = DisposeBag()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setUp(item : Area){
        self.lbl.text = item.name ?? ""
    }
 
    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }
    
}
