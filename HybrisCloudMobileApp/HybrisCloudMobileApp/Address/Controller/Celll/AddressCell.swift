//
//  AddressCell.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/4/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import SwipeCellKit

class AddressCell: SwipeTableViewCell {

    @IBOutlet weak var AddressName: UILabel!
     @IBOutlet weak var AddressPersonName: UILabel!
    @IBOutlet weak var AddressPhoneNumber: UILabel!
    @IBOutlet weak var AddressPostalCode: UILabel!

    @IBOutlet weak var AddressRegion: UILabel!
    
    @IBOutlet weak var AddressAddress1: UILabel!
    
      func setUp(address : Address , cart : CartModel) {
        if cart.shipmentType?.code == "PICKUP_IN_STORE" {
            self.AddressPersonName.isHidden = true
            self.AddressPhoneNumber.text = cart.entries?.first?.deliveryPointOfService?.address?.phone ?? ""
            self.AddressName.text = cart.entries?.first?.deliveryPointOfService?.name ?? ""
            self.AddressRegion.text = cart.entries?.first?.deliveryPointOfService?.address?.country?.name ?? ""
            self.AddressAddress1.text =  cart.entries?.first?.deliveryPointOfService?.address?.formattedAddress ?? ""
            self.AddressPostalCode.isHidden = true
        } else {
            self.AddressPostalCode.isHidden = false
            self.AddressPersonName.isHidden = false
            var addressPersonName = "\(address.title ?? "") \(address.firstName ?? "") \(address.lastName ?? "")"
            if(address.defaultAddress ?? true)
            {
                addressPersonName = addressPersonName + " (DEFAULT)".localized()
                
            }
            self.AddressPersonName.text = addressPersonName
            self.AddressName.text = address.addressName ?? ""

            self.AddressPostalCode.isHidden = address.postalCode == nil

            self.AddressPhoneNumber.text = address.mobileNumber ?? ""
            self.AddressPostalCode.text =  address.postalCode ?? ""
            self.AddressAddress1.text =  address.line1 ?? ""
            self.AddressRegion.text = "\(address.town ?? "") , \(address.country?.name ?? "")"
        }
        
      

      }
    
    
    func setUp2(address : Address) {
      
          var addressPersonName = "\(address.title ?? "") \(address.firstName ?? "") \(address.lastName ?? "")"
          if(address.defaultAddress ?? true)
          {
              addressPersonName = addressPersonName + " (DEFAULT)".localized()
              
          }
          self.AddressPersonName.text = addressPersonName
          self.AddressName.text = address.addressName ?? ""

          self.AddressPostalCode.isHidden = address.postalCode == nil

          self.AddressPhoneNumber.text = address.mobileNumber ?? ""
          self.AddressPostalCode.text =  address.postalCode ?? ""
          self.AddressAddress1.text =  address.line1 ?? ""
          self.AddressRegion.text = "\(address.town ?? "") , \(address.country?.name ?? "")"
      }
    

}
