//
//  EditAddressViewController.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/12/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import UIKit
import Domain
import RxSwift
import RxCocoa
import NetworkPlatform

class EditAddressViewController: UIViewController , UITextFieldDelegate {
    
    let countryPickerView = UIPickerView()
    let codePickerView = UIPickerView()
    let cityPickerView = UIPickerView()
//    let areaPickerView = UIPickerView()
    let titlePickerView = UIPickerView()

    let disposeBag = DisposeBag()
    var viewModel : EditAddressViewModel!
    
    var shippingCountrySubject = PublishSubject<Country>()
    var citySubject = PublishSubject<City>()
    var areaSubject = BehaviorSubject<Area?>(value : nil)
    var mobileCountrySubject = PublishSubject<Country>()
    var titleSubject = PublishSubject<Title>()
    var isDeafultAddressPublishSubject = PublishSubject<Bool>()
    var addressBehaviorSubject:BehaviorSubject<Address>?
    var localityBehaviorSubject = BehaviorSubject<String>(value : "")
    var sublocalityBehaviorSubject = BehaviorSubject<String>(value : "")
    let editAddressManual = PublishSubject<Void>()

    var cityListForSave : [City] = []
    var indexSelectedCityForSave : Int = 0

    var areas = [Area]()

    @IBOutlet weak var goToMap: UIButton!
    @IBOutlet weak var saveBarButton: UIButton!
    @IBOutlet weak var checkBoxDefaultAddress: UIButton!

    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var addressLine1TextField: UITextField!
    @IBOutlet weak var AddressLine2TextField: UITextField!
    @IBOutlet weak var PostCodeTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var BuildingNameTextField: UITextField!
    @IBOutlet weak var streetNameTextField: CustomSearchTextField!
    @IBOutlet weak var apartmentNumberTextField: UITextField!
    @IBOutlet weak var AddressNameTextField: UITextField!
    @IBOutlet weak var NearstLandmarkTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var areaTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var scrollVIew: UIScrollView!


    let token = GMSAutocompleteSessionToken.init()

    
    @IBAction func makeAsDefult(_ sender: Any) {
          
          let button = sender as? UIButton
          button?.isSelected.toggle()
          isDeafultAddressPublishSubject.onNext(button?.isSelected ?? false)

          
      }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        AddressNameTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        titleTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        firstNameTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        lastNameTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        countryTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        cityTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
//        areaTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        addressLine1TextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        AddressLine2TextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        PostCodeTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        NearstLandmarkTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        BuildingNameTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        streetNameTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        apartmentNumberTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        codeTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        phoneNumberTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
        numberTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.doneButtonClicked(_:)))
            
        areaTextField.addTarget(self, action: #selector(onClickAreaNameTextField), for: .editingDidBegin)

        streetNameTextField.addTarget(self, action: #selector(onClickStreetNameTextField), for: .editingChanged)

        addressLine1TextField.delegate = self
        self.streetNameTextField.didSelectAction = {
            self.scrollVIew.isScrollEnabled = true
            self.streetNameTextField.sendActions(for: .valueChanged)
        }

         
         self.codeTextField.isUserInteractionEnabled = false
         self.titleTextField.isUserInteractionEnabled = false
         self.countryTextField.isUserInteractionEnabled = false
         self.cityTextField.isUserInteractionEnabled = false
         self.areaTextField.isUserInteractionEnabled = false
 
         goToMap.rx.tap.subscribe(onNext: { (void) in
        
          self.performSegue(withIdentifier: "goToMapFromEdit", sender: nil)

         }).disposed(by: disposeBag)

        
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
     
        viewModel = EditAddressViewModel(
            countryUseCase: networkUseCaseProvider.makeCountryUseCase(),
            addAddressesUseCase : networkUseCaseProvider.makeAddAddressesUseCase(),
            citiesUseCase: networkUseCaseProvider.makeCitiesUseCase(),
            titleUseCase: networkUseCaseProvider.makeTitleUseCase())
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
              .mapToVoid()
              .asDriverOnErrorJustComplete()

        guard let getAddress = addressBehaviorSubject else{
            return
                     }
               
        
        let input = EditAddressViewModel.Input(
                  trigger: Driver.merge(viewWillAppear),
                  saveTrigger: editAddressManual.asDriverOnErrorJustComplete(),
                  validation: saveBarButton!.rx.tap.asDriver(),
                  country: shippingCountrySubject.asDriverOnErrorJustComplete(),
            addressName : AddressNameTextField.rx.text.observe(on: MainScheduler.instance)
                  .distinctUntilChanged().asDriverOnErrorJustComplete(),
                  nearstLandmark : NearstLandmarkTextField.rx.text.asDriver() ,
                  isDefaultAddress : isDeafultAddressPublishSubject.asDriverOnErrorJustComplete() ,
                  title :titleSubject.asDriverOnErrorJustComplete() ,
                  firstName: firstNameTextField.rx.text.asDriver(),
                  lastName: lastNameTextField.rx.text.asDriver(),
                  mobileCountry: mobileCountrySubject.asDriverOnErrorJustComplete(),
                  addressLineOne: addressLine1TextField.rx.text.asDriver(),
                  addressLineTow: AddressLine2TextField.rx.text.asDriver(),
                  city: citySubject.asDriverOnErrorJustComplete(),
                  area: areaSubject.asDriverOnErrorJustComplete(),
                  postCode: PostCodeTextField.rx.text.asDriver(),
                  mobileNum: numberTextField.rx.text.asDriver(),
                  buildingName: BuildingNameTextField.rx.text.asDriver(),
                  streetName: streetNameTextField.rx.text.asDriver(),
                  apartmentNumber: apartmentNumberTextField.rx.text.asDriver(),
                  addressForEdit: getAddress.asDriverOnErrorJustComplete())
        
        let output = viewModel.transform(input: input)
        
  output.validation.asObservable().subscribe(onNext: { (msg) in
      if msg != "" {
          self.errorLoaf(message: msg)
      }
      else {
          self.editAddressManual.onNext(())
      }
      }).disposed(by: disposeBag)


        
        output.country.asObservable().bind(to: countryPickerView.rx.itemTitles){ _, item in
             
            return item.name
          } .disposed(by: disposeBag)
        
        output.country.asObservable().subscribe(onNext: {[unowned self] (countries) in
            if(countries.count != 0 )
            {
                self.countryPickerView.selectRow(getSelectedCountry(countries: countries), inComponent:0 , animated: true)
                
                self.countryPickerView.delegate?.pickerView?(self.countryPickerView, didSelectRow: getSelectedCountry(countries: countries), inComponent: 0)
                if(countries.count != 1 )
                                      {
                self.countryTextField.inputView = self.countryPickerView
                self.countryTextField.isUserInteractionEnabled = true
                }
            }
            
            }).disposed(by: disposeBag)
        
        output.title.asObservable().bind(to: titlePickerView.rx.itemTitles){ _, item in
                    
                   return item.name
                 } .disposed(by: disposeBag)
               
               output.title.asObservable().subscribe(onNext: {[unowned self] (titles) in
                   if(titles.count != 0 )
                   {
                       self.titlePickerView.selectRow(getSelectedTitle(titles: titles), inComponent:0 , animated: true)
                       
                       self.titlePickerView.delegate?.pickerView?(self.titlePickerView, didSelectRow: getSelectedTitle(titles: titles), inComponent: 0)
                       if(titles.count != 1 )
                       {
                       self.titleTextField.inputView = self.titlePickerView
                       self.titleTextField.isUserInteractionEnabled = true
                    }
                   }
                   
                   }).disposed(by: disposeBag)
        
        output.city.asObservable().bind(to: cityPickerView.rx.itemTitles){ _, item in
                    
                   return item.name
                 } .disposed(by: disposeBag)
               
        output.city.asObservable().subscribe(onNext: {[unowned self] (city) in
            self.cityListForSave = city
            if((city.count != 0) && (try! self.localityBehaviorSubject.value() == ""))
            {
               
                self.cityPickerView.selectRow(getSelectedCity(City: city), inComponent:0 , animated: true)
                             
                self.cityPickerView.delegate?.pickerView?(self.cityPickerView, didSelectRow: getSelectedCity(City: city), inComponent: 0)
                if (city.count != 1)
                {
                    self.cityTextField.inputView = self.cityPickerView
                    self.cityTextField.isUserInteractionEnabled = true
                }
            }
                   }).disposed(by: disposeBag)
        
        
//        output.area.asObservable().bind(to: areaPickerView.rx.itemTitles){ _, item in
//
//                          return item.name
//                        } .disposed(by: disposeBag)
                      
        output.area.asObservable().subscribe(onNext: {[unowned self] (areas) in
            if(areas.count != 0)
            {
                
                self.areas = areas
                
                self.areaTextField.text = areas[getSelectedArea(Area: areas)].name ?? ""
                self.areaTextField.sendActions(for: .valueChanged)

//                self.areaPickerView.selectRow(getSelectedArea(Area: areas), inComponent:0 , animated: true)
//
//                self.areaPickerView.delegate?.pickerView?(self.areaPickerView, didSelectRow: getSelectedArea(Area: areas), inComponent: 0)
                                        
                if(areas.count != 1)
                {
//                self.areaTextField.inputView = self.areaPickerView
                self.areaTextField.isUserInteractionEnabled = true
                }

        
            }else {
                self.areaSubject.onNext(nil)
                self.areaTextField.isUserInteractionEnabled = false
                self.areaTextField.text = ""
            }
                         
    }).disposed(by: disposeBag)
        
        
        
        
        output.codePhone.asObservable().bind(to: codePickerView.rx.itemTitles){ _, item in
    
            return item.isdcode
        } .disposed(by: disposeBag)
        output.codePhone.asObservable().subscribe(onNext: {[unowned self] (countries) in
            if(countries.count != 0)
            {
                self.codePickerView.selectRow(getSelectedCodePhone(countries: countries), inComponent: 0 , animated: true)
                
                self.codePickerView.delegate?.pickerView?(self.codePickerView, didSelectRow: getSelectedCodePhone(countries: countries), inComponent: 0)
                if(countries.count != 1)
                {
                    self.codeTextField.inputView = self.codePickerView
                    self.codeTextField.isUserInteractionEnabled = true
                }
            }
            
        }).disposed(by: disposeBag)
        
        countryPickerView.rx.modelSelected(Country.self).map { (countries) -> Country in
            return countries.first ?? Country(isdcode: nil, isocode: nil, name: nil)
            }.bind(to: shippingCountrySubject).disposed(by: disposeBag)
        
        countryPickerView.rx.modelSelected(Country.self).map { (countries) -> String in
            return countries.first?.name ?? ""
        }.bind(to: countryTextField.rx.text).disposed(by: disposeBag)
          
        
        titlePickerView.rx.modelSelected(Title.self).map { (titles) -> Title in
               return titles.first ?? Title(code: nil, name: nil)
               }.bind(to: titleSubject).disposed(by: disposeBag)
           
           titlePickerView.rx.modelSelected(Title.self).map { (titles) -> String in
               return titles.first?.name ?? ""
           }.bind(to: titleTextField.rx.text).disposed(by: disposeBag)
             
        
        cityPickerView.rx.modelSelected(City.self).map { (city) -> City in
                   return city.first ?? City(areas: nil, code: nil, name: nil)
                   }.bind(to: citySubject).disposed(by: disposeBag)
               
        cityPickerView.rx.modelSelected(City.self).map { (city) -> String in
                   return city.first?.name ?? ""
               }.bind(to: cityTextField.rx.text).disposed(by: disposeBag)
                 
 
        
//        areaPickerView.rx.modelSelected(Area.self).map { (area) -> Area in
//                          return area.first ?? Area(code: nil, name: nil)
//                          }.bind(to: areaSubject).disposed(by: disposeBag)
//
//        areaPickerView.rx.modelSelected(Area.self).map { (area) -> String in
//                          return area.first?.name ?? ""
//                      }.bind(to: areaTextField.rx.text).disposed(by: disposeBag)
             
        
       codePickerView.rx.modelSelected(Country.self).map { (countries) -> Country in
                   return countries.first ?? Country(isdcode: nil, isocode: nil, name: nil)
                   }.bind(to: mobileCountrySubject).disposed(by: disposeBag)
        
        codePickerView.rx.modelSelected(Country.self).map { (countries) -> String in
                   return countries.first?.isdcode ?? ""
               }.bind(to: codeTextField.rx.text).disposed(by: disposeBag)
               
        output.editAddressOutput.asObservable().subscribe(onNext: { (address) in
            self.navigationController?.backToViewController(vc: DeliveryAddressViewController.self)
        }).disposed(by: disposeBag)
        
        output.error.asObservable().subscribe(onNext: { (error) in
           let error = error as? ErrorResponse
            
           let errorsString =  error?.errors?.map({ (errorResponseElement) -> String in
            let msg1 = (errorResponseElement.type ?? "" ) + " "  + ( errorResponseElement.reason ?? "") + " "
            let msg2 = (errorResponseElement.subject ?? "") + " " + (errorResponseElement.message ?? "")
            return msg1+msg2
                }).joined(separator: " , ")
            
            self.errorLoaf(message: errorsString ?? "")
        }).disposed(by: disposeBag)
        
        output.editAddressInput.asObservable().subscribe(onNext: { (Address) in
            
            self.AddressNameTextField.setText(Address.addressName)
            self.firstNameTextField.setText(Address.firstName )
            self.lastNameTextField.setText(Address.lastName )
            self.addressLine1TextField.setText(  Address.line1 )
            self.AddressLine2TextField.setText(  Address.line2  )
            self.NearstLandmarkTextField.setText( Address.nearestLandmark )
            
            var mobileNumber : String =  ""
            let number = Address.mobileNumber ?? ""
            let code = Address.mobileCountry?.isdcode ?? " "

            if let space = number.firstIndex(of: (code.last ?? " ")) {
                mobileNumber =  String(number[number.index(after: space)..<number.endIndex])
            }else{
                mobileNumber =  number
            }
            self.numberTextField.setText( mobileNumber  )
            self.PostCodeTextField.setText(  Address.postalCode )
            self.checkBoxDefaultAddress.isSelected = Address.defaultAddress ?? false
            self.streetNameTextField.setText(Address.streetName)
            self.apartmentNumberTextField.setText(Address.apartmentNumber)
            self.BuildingNameTextField.setText(Address.buildingName)



        }, onError: { (Error) in
            
            }).disposed(by: disposeBag)

        output.editAddressInput.asObservable().map({ (Address) -> Bool in
            return Address.defaultAddress ?? false
            }).bind(to: isDeafultAddressPublishSubject).disposed(by: disposeBag)
        
              
        countryPickerView.rx.modelSelected(Country.self)
              .subscribe(onNext: { models in
                  self.countryTextField.text =  models.first?.name
                  }).disposed(by: disposeBag)
        
        titlePickerView.rx.modelSelected(Title.self)
                     .subscribe(onNext: { models in
                         self.titleTextField.text =  models.first?.name
                         }).disposed(by: disposeBag)
        
        cityPickerView.rx.modelSelected(City.self)
        .subscribe(onNext: { models in
            self.cityTextField.text =  models.first?.name
            }).disposed(by: disposeBag)
        
//        areaPickerView.rx.modelSelected(Area.self)
//               .subscribe(onNext: { models in
//                   self.areaTextField.text =  models.first?.name
//                   }).disposed(by: disposeBag)
        
        codePickerView.rx.modelSelected(Country.self)
        .subscribe(onNext: { models in
            self.codeTextField.text =  models.first?.isdcode
            }).disposed(by: disposeBag)

        localityBehaviorSubject.subscribe(onNext: { (locality) in
              
           var selectedIndex = 0
            if(self.cityListForSave.count != 0 )
            {
           for (index, element) in self.cityListForSave.enumerated() {
                                                  if element.name == locality {
                                                      selectedIndex = index
                                                  }
                                              }
                                            
            self.cityPickerView.selectRow(selectedIndex, inComponent:0 , animated: true)
            self.cityPickerView.delegate?.pickerView?(self.cityPickerView, didSelectRow: selectedIndex, inComponent: 0)
            self.citySubject.onNext(self.cityListForSave[selectedIndex])
            self.indexSelectedCityForSave = selectedIndex
            }
           }).disposed(by: disposeBag)
        
        
        
        sublocalityBehaviorSubject.subscribe(onNext: { (sublocality) in
              
           var selectedIndex = 0
            if(self.cityListForSave.count != 0  )
            {
                if(self.cityListForSave[self.indexSelectedCityForSave].areas != nil )
                {
                for (index, element) in self.cityListForSave[self.indexSelectedCityForSave].areas!.enumerated() {
                                                  if element.name == sublocality {
                                                      selectedIndex = index
                                                  }
                                              }
                                            
//            self.areaPickerView.selectRow(selectedIndex, inComponent:0 , animated: true)
//            self.areaPickerView.delegate?.pickerView?(self.areaPickerView, didSelectRow: selectedIndex, inComponent: 0)
                self.areaSubject.onNext(self.cityListForSave[self.indexSelectedCityForSave].areas?[selectedIndex])
                }}
           }).disposed(by: disposeBag)
        
        
        
        
        func getSelectedTitle(titles: [Title]) -> Int{
            let titleCode = try? getAddress.value().titleCode
                   var indexSelected = 0
                   if ((titleCode) != nil)
                   {
                       for (index, element) in titles.enumerated() {
                        if (element.code ==  titleCode)
                           {
                               indexSelected = index
                       }
                       }
                   }else {
                       indexSelected =  0
                   }
                   titleSubject.onNext(titles[indexSelected])
                   return indexSelected
               }
        
        func getSelectedCountry(countries : [Country]) -> Int{
            let country = try? getAddress.value().country
            var indexSelected = 0
            if ((country) != nil)
            {
                for (index, element) in countries.enumerated() {
                    if (element ==  country)
                    {
                        indexSelected = index
                }
                }
            }else {
                indexSelected =  0
            }
            shippingCountrySubject.onNext(countries[indexSelected])
            return indexSelected
        }
        
        func getSelectedCity(City : [City]) -> Int{
            let city = try? getAddress.value().city
            var indexSelected = 0
            if ((city) != nil)
            {
                for (index, element) in City.enumerated() {
                    if (element ==  city)
                    {
                        indexSelected = index
                }
                }
            }else {
                indexSelected =  0
            }
            citySubject.onNext(City[indexSelected])
            return indexSelected
        }
          
        func getSelectedArea(Area : [Area]) -> Int{
                   let area = try? getAddress.value().area
                   var indexSelected = 0
                   if ((area) != nil)
                   {
                       for (index, element) in Area.enumerated() {
                           if (element ==  area)
                           {
                               indexSelected = index
                       }
                       }
                   }else {
                       indexSelected =  0
                   }
                   areaSubject.onNext(Area[indexSelected])
                   return indexSelected
               }
        
        func getSelectedCodePhone(countries : [Country]) -> Int{
            let country = try? getAddress.value().mobileCountry
            var indexSelected = 0

                   if ((country) != nil)
                   {
                       for (index, element) in countries.enumerated() {
                           if (element ==  country)
                           {
                               indexSelected = index

                           }
                       }
                   }else {
                       indexSelected = 0
                   }
            mobileCountrySubject.onNext(countries[indexSelected])

               return indexSelected
               }
        
        
      }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 
           if segue.identifier == "goToMapFromEdit"{
             
               if let vc = segue.destination as? AddAddressMapViewController{
                vc.goToAddAddress = false
                
                vc.line1BehaviorSubject.subscribe(onNext: { (text) in
                   self.streetNameTextField.text = text
                   self.streetNameTextField.sendActions(for: .valueChanged)

                   }).disposed(by: disposeBag)
                
                 vc.localityBehaviorSubject.bind(to: self.localityBehaviorSubject).disposed(by: disposeBag)
                vc.subLocalityBehaviorSubject.bind(to: self.sublocalityBehaviorSubject).disposed(by: disposeBag)
               }
           }
           
        if segue.identifier == "toList"{
            self.view.endEditing(true)

            if let vc = segue.destination as? ListVC{
                vc.fullList = self.areas
                vc.selectedAreaSubject.subscribe { (area) in
                    self.areaSubject.onNext(area)
                    self.areaTextField.text = area.name ?? ""
                    self.areaTextField.sendActions(for: .valueChanged)
                    vc.dismiss(animated: true, completion: nil)
                } onError: { (Error) in
                    print("error on get area selected")
                } onCompleted: {
                    print("completed")
                } onDisposed: {
                    print("Disposed")
                }.disposed(by: vc.bag)
             }
        }

           
       }
    
    @objc func doneButtonClicked(_ textField: UITextField) {
    
        switch textField{
            
        case AddressNameTextField:
            
            if(self.titleTextField.isUserInteractionEnabled)
            {self.titleTextField.becomeFirstResponder()}
            else{self.firstNameTextField.becomeFirstResponder()}
        
        case titleTextField: self.firstNameTextField.becomeFirstResponder()
        
        case firstNameTextField:lastNameTextField.becomeFirstResponder()
        
        case lastNameTextField:
            if(countryTextField.isUserInteractionEnabled)
            {countryTextField.becomeFirstResponder()}
            else {
            if(cityTextField.isUserInteractionEnabled)
            {cityTextField.becomeFirstResponder()}
            else {
                if(areaTextField.isUserInteractionEnabled)
                {areaTextField.becomeFirstResponder()}
                else {addressLine1TextField.becomeFirstResponder()}
            }
            }
            
        case countryTextField:
            if(cityTextField.isUserInteractionEnabled)
            {cityTextField.becomeFirstResponder()}
            else {
            if(areaTextField.isUserInteractionEnabled)
            {areaTextField.becomeFirstResponder()}
            else {addressLine1TextField.becomeFirstResponder()}
            }
            
        case cityTextField:
            if(!areaTextField.isUserInteractionEnabled)
            {addressLine1TextField.becomeFirstResponder()}
       
        case areaTextField:addressLine1TextField.becomeFirstResponder()
      
        case addressLine1TextField:AddressLine2TextField.becomeFirstResponder()
      
        case AddressLine2TextField:NearstLandmarkTextField.becomeFirstResponder()
        
        case NearstLandmarkTextField:apartmentNumberTextField.becomeFirstResponder()
       
       
        case streetNameTextField:
            NearstLandmarkTextField.becomeFirstResponder()
            self.scrollVIew.isScrollEnabled = true

        case apartmentNumberTextField:
            if(codeTextField.isUserInteractionEnabled)
            {codeTextField.becomeFirstResponder()}
            else {phoneNumberTextField.becomeFirstResponder()}
       
        case codeTextField:numberTextField.becomeFirstResponder()
            
        default:
            break
            
        }
        
    }
    @objc func onClickStreetNameTextField(textField: UITextField) {

        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
            //.establishment
        filter.country = "AE"
        

        let placesClient = GMSPlacesClient()
        
        let bottomOffset = CGPoint(x: 0, y: (self.navigationController?.navigationBar.frame.maxX ?? 0.0) - 50)
        scrollVIew.setContentOffset(bottomOffset, animated: false)
        self.scrollVIew.isScrollEnabled = false

        
        placesClient.findAutocompletePredictions(fromQuery: self.streetNameTextField.text ?? "", filter: filter, sessionToken: self.token , callback: { (results, error) in
                        if let error = error {
                          print("Autocomplete error: \(error)")
                          return
                        }
            
            var list = [AddressFromGoogle]()

                        if let results = results {
                          for result in results {
                            print("Result \(result.attributedFullText) with placeID \(result.placeID)")
                            list.append(AddressFromGoogle(address: result.attributedPrimaryText.string , street: result.attributedSecondaryText?.string ?? ""))
                           }
                            self.streetNameTextField.updateList(list: list)

                        }
                    })


    }

    @objc func onClickAreaNameTextField(textField: UITextField) {
        view.endEditing(true)
performSegue(withIdentifier: "toList", sender: nil)

    }

  
        
}

extension UITextField {
    
    func setText(_ value : String?) {
        self.text = value ?? ""
        self.sendActions(for: .valueChanged)
    }
}

extension EditAddressViewController: GMSAutocompleteViewControllerDelegate {

  // Handle the user's selection.
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    print("Place name: \(place.name)")
    print("Place ID: \(place.placeID)")
    print("Place attributions: \(place.attributions)")

    dismiss(animated: true, completion: nil)
  }

  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // TODO: handle the error.
    print("Error: ", error.localizedDescription)
  }
    
    func viewController(_ viewController: GMSAutocompleteViewController,
                        didSelect prediction: GMSAutocompletePrediction) -> Bool {
        print("Primary: \(prediction.attributedPrimaryText)")
        print("Secondary: \(prediction.attributedSecondaryText)")
        self.streetNameTextField.text = "\(prediction.attributedPrimaryText.string ), \(prediction.attributedSecondaryText?.string ?? "")"
        self.streetNameTextField.sendActions(for: .valueChanged)

        return true
    }


  // User canceled the operation.
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    dismiss(animated: true, completion: nil)
  }

  // Turn the network activity indicator on and off again.
  func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  }

  func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }

}

extension EditAddressViewController  {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
