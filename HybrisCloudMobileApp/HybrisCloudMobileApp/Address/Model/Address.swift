//
//  Address.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxDataSources


struct AddressSection {
    var header: String
    var items: [Item]
    
}

extension AddressSection : AnimatableSectionModelType, Equatable {
    typealias Item = Address

    var identity: String {
        return header
    }

    init(original: AddressSection, items: [Address]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: AddressSection, rhs: AddressSection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension Address: IdentifiableType, Equatable {
   

    
    public static func == (lhs: Address, rhs: Address) -> Bool {
        
        return (
                (lhs.id  == rhs.id)
            &&
                ((lhs.addressName ?? "") == (rhs.addressName ?? ""))
            &&
                ((lhs.firstName ?? "") == (rhs.firstName ?? ""))
            &&
                ((lhs.lastName ?? "") == (rhs.lastName ?? ""))
            &&
                ((lhs.title ?? "") == (rhs.title ?? ""))
            &&
                ((lhs.country?.isocode ?? "") == (rhs.country?.isocode  ?? ""))
            &&
                ((lhs.city?.code ?? "") == (rhs.city?.code  ?? ""))
            &&
                ((lhs.area?.code ?? "") == (rhs.area?.code  ?? ""))
            &&
                ((lhs.line1 ?? "") == (rhs.line1 ?? ""))
            &&
                ((lhs.line2 ?? "") == (rhs.line2 ?? ""))
            &&
                ((lhs.streetName ?? "") == (rhs.streetName ?? ""))
            &&
                ((lhs.apartmentNumber ?? "") == (rhs.apartmentNumber ?? ""))
            &&
                ((lhs.buildingName ?? "") == (rhs.buildingName ?? ""))
            &&
                ((lhs.postalCode ?? "") == (rhs.postalCode ?? ""))
            &&
                ((lhs.nearestLandmark ?? "") == (rhs.nearestLandmark ?? ""))
            &&
                ((lhs.mobileCountry?.isdcode ?? "") == (rhs.mobileCountry?.isdcode ?? ""))
            &&
                ((lhs.defaultAddress ?? false) == (rhs.defaultAddress ?? false))
            &&
                ((lhs.mobileNumber ?? "") == (rhs.mobileNumber ?? ""))
        )
        
        
    }
}


 
 
