//
//  EditAddressViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/12/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

class EditAddressViewModel: ViewModelType{
    
    
    private let countryUseCase: CountryUseCase
    private let addAddressesUseCase:AddAddressesUseCase
    private let citiesUseCase:CitiesUseCase
    private let titleUseCase: MiscsUseCase
    
    init(countryUseCase: CountryUseCase,
         addAddressesUseCase:AddAddressesUseCase,
         citiesUseCase:CitiesUseCase ,
         titleUseCase: MiscsUseCase
        
    ) {
        self.countryUseCase = countryUseCase
        self.addAddressesUseCase = addAddressesUseCase
        self.citiesUseCase = citiesUseCase
        self.titleUseCase = titleUseCase
    }
    
    struct Input {
        let trigger: Driver<Void>
        let saveTrigger: Driver<Void>
        let validation: Driver<Void>
        let country: Driver<Country>
        let addressName:  Driver<ControlProperty<String?>.Element>
        let nearstLandmark:  Driver<ControlProperty<String?>.Element>
        let isDefaultAddress:  Driver<Bool>
        let title: Driver<Title>
        let firstName:  Driver<ControlProperty<String?>.Element>
        let lastName:  Driver<ControlProperty<String?>.Element>
        let mobileCountry: Driver<Country>
        let addressLineOne:  Driver<ControlProperty<String?>.Element>
        let addressLineTow:  Driver<ControlProperty<String?>.Element>
        let city: Driver<City>
        let area: Driver<Area?>
        let postCode:  Driver<ControlProperty<String?>.Element>
        let mobileNum:  Driver<ControlProperty<String?>.Element>
        let buildingName:  Driver<ControlProperty<String?>.Element>
        let streetName:  Driver<ControlProperty<String?>.Element>
        let apartmentNumber:  Driver<ControlProperty<String?>.Element>
        let addressForEdit :Driver<Address>

        
    }
    struct Output {
        let fetching: Driver<Bool>
        let country: Driver<[Country]>
        let city: Driver<[City]>
        let area: Driver<[Area]>
        let codePhone: Driver<[Country]>
        let title: Driver<[Title]>
        let error: Driver<Error>
        let editAddressOutput: Driver<Void>
        let editAddressInput: Driver<Address>
        let validation: Driver<String>

    }
    
    func transform(input: Input) -> Output {
        
        let activityIndicator = ActivityIndicator()
        
        let errorTracker = ErrorTracker()
        
        let country = input.trigger.flatMapLatest{
            return self.countryUseCase.fetchCountriesShipping()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        
        let codePhone = input.trigger.flatMapLatest{
            return self.countryUseCase.fetchCountriesMobile()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        let city =  input.country.map { (Country) -> String in
            return Country.isocode ?? ""
        } .flatMapLatest { [unowned self] in
            self.citiesUseCase.fetchCities(countryIsoCode: $0)
                .trackError(errorTracker)
                .trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
        }
        
        let areaResult = input.city.map({ (area) -> [Area] in
            return (area.areas ?? [])})
        
        
        let cityResult = city.map({ (city) -> [City] in
            return (city.cities ?? [])})
        
        
        let countryResult = country.map({ (country) -> [Country] in
            return (country.countries ?? [])})
        
        let codePhoneResult = codePhone.map({ (country) -> [Country] in
            return (country.countries ?? [])})
        
        
        let addressDetails1 = Driver.combineLatest(
            input.country,
            input.addressName,
            input.nearstLandmark,
            input.isDefaultAddress ,
            input.title ,
            input.firstName ,
            input.lastName ,
            input.mobileCountry
        )
        
        
        let addressDetails2 = Driver.combineLatest(
            input.addressLineOne,
            input.addressLineTow,
            input.city,
            input.area,
            //                   input.postCode,
            input.mobileNum
        )
        
        let addressDetails3 = Driver.combineLatest(
                   input.streetName,
                   input.apartmentNumber,
                   input.buildingName
                   
               )
        
        let addressDetails = Driver.combineLatest(addressDetails1,addressDetails2,addressDetails3)
        
        let editAddressDetails = Driver.combineLatest(addressDetails,input.addressForEdit)
        
        
        
        let editAddress = input.saveTrigger.withLatestFrom(editAddressDetails).map { (arg0) -> Address in
            
            var (((
            country,
                addressName, nearstLandmark, isDefaultAddress, title, firstName, lastName,mobileCountry), (addressLineOne,_,city,
                    area,
                        //                postCode,
                        mobileNum),(streetName,apartmentNumber,buildingName)), Address) = arg0
            
            if addressLineOne == "" {
                addressLineOne = streetName
            }
            var editaddress : Address = Address
            
            editaddress.addressName =  addressName ?? ""
            editaddress.titleCode = title.code ?? ""
            editaddress.title = title.name ?? ""
            editaddress.firstName =  firstName ?? ""
            editaddress.lastName =  lastName ?? ""
            editaddress.streetName =  streetName ?? ""
            editaddress.apartmentNumber =  apartmentNumber  ?? ""
            editaddress.buildingName =  buildingName  ?? ""

            
            //    editaddress.postalCode =  postCode ?? ""
            
            editaddress.country =  country
            
            editaddress.city = city
            
            editaddress.area = area
            
            editaddress.line1 =  addressLineOne 
            editaddress.line2 =  streetName ?? ""
            editaddress.nearestLandmark =  nearstLandmark  ?? ""
            
            
            editaddress.mobileCountry =  mobileCountry
            
            editaddress.mobileNumber =  mobileNum  ?? ""
            
            
            editaddress.defaultAddress = isDefaultAddress
            
            
            
            return editaddress
        }
        .flatMapLatest {
            self.addAddressesUseCase.updateAddress(userId: "current", address: $0)
                .trackActivity(activityIndicator)                       .trackError(errorTracker).asDriverOnErrorJustComplete()
        }
        
        let validation = input.validation.withLatestFrom(Driver.combineLatest(input.streetName , input.buildingName , input.apartmentNumber)).flatMapLatest { (arg0) -> Driver<String> in

            let (streetName, buildingName, apartmentNumber) = arg0

            if streetName == "" || streetName == nil {
                return Driver<String>.just("street field is required")
            }
//            else if buildingName == "" || buildingName == nil{
//                return Driver<String>.just("building Name field is required")
//            }
            else if apartmentNumber == "" || apartmentNumber == nil{
                return Driver<String>.just("apartment number field is required")
            }
            else {
                return Driver<String>.just("")
            }
        }

        
        let title = input.trigger.flatMapLatest{
            
            return self.titleUseCase.titles()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        
        
        let titleResult = title.map({ (title) -> [Title] in
            return (title.titles ?? [])
        })
        
        let fetching = activityIndicator.asDriver()
        
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching, country: countryResult,city: cityResult,area: areaResult, codePhone: codePhoneResult,title : titleResult , error: errors, editAddressOutput: editAddress, editAddressInput:  input.addressForEdit , validation: validation )
        
    }
    
}
