//
//  AddAddressViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/6/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

class AddAddressViewModel: ViewModelType{
 


    private let countryUseCase: CountryUseCase
    private let addressesUseCase:AddAddressesUseCase
    private let citiesUseCase:CitiesUseCase
    private let titleCase: MiscsUseCase
    private let userUseCase: UserUseCase
    
    init(countryUseCase: CountryUseCase,addressesUseCase:AddAddressesUseCase,citiesUseCase:CitiesUseCase,titleCase: MiscsUseCase ,
         userUseCase: UserUseCase) {
        self.countryUseCase = countryUseCase
        self.addressesUseCase = addressesUseCase
        self.citiesUseCase = citiesUseCase
        self.titleCase = titleCase
        self.userUseCase = userUseCase
    }
    
 

    
    struct Input {
        let trigger: Driver<Void>
        let saveTrigger: Driver<Void>
        let validation: Driver<Void>
        let country: Driver<Country>
        let addressName: Driver<String>
        let nearstLandmark: Driver<String>
        let isDefaultAddress: Driver<Bool>
        let title: Driver<Title>
        let firstName: Driver<String>
        let lastName: Driver<String>
        let mobileCountry: Driver<Country>
        let addressLineOne: Driver<String>
        let addressLineTow: Driver<String>
        let city: Driver<City>
        let area: Driver<Area?>
        let postCode: Driver<String>
        let mobileNum: Driver<String>
        let buildingName: Driver<String>
        let streetName: Driver<String>
        let apartmentNumber: Driver<String>
        let personalDetails: Driver<UserData>
        
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let country: Driver<[Country]>
        let city: Driver<[City]>
        let area: Driver<[Area]>
        let codePhone: Driver<[Country]>
        let title: Driver<([Title],Title)>
        let error: Driver<Error>
        let addAddress: Driver<Address>
        let getUserInformation: Driver<UserData>
        let validation: Driver<String>
    }
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let getUserInformation = input.trigger.flatMapLatest{
            
            return self.userUseCase.getCustomerProfile(userId: "current")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        
        let addressDetails1 = Driver.combineLatest(
            input.country,
            input.addressName,
            input.nearstLandmark,
            input.isDefaultAddress ,
            input.title ,
            input.firstName ,
            input.lastName ,
            input.mobileCountry
            
            )
        
        let addressDetails2 = Driver.combineLatest(
            input.addressLineOne,
            input.addressLineTow,
            input.city,
            input.area,
            //                   input.postCode,
            input.mobileNum ,
            input.personalDetails
            
            )
        
        let addressDetails3 = Driver.combineLatest(
            input.streetName,
            input.apartmentNumber,
            input.buildingName
            
        )
        
        let country = input.trigger.flatMapLatest{
            return self.countryUseCase.fetchCountriesShipping()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        let codePhone = input.trigger.flatMapLatest{
            return self.countryUseCase.fetchCountriesMobile()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        let countryResult = country.map({ (country) -> [Country] in
            return (country.countries ?? [])})
        let codePhoneResult = codePhone.map({ (country) -> [Country] in
            return (country.countries ?? [])})
        
        let title = input.trigger.flatMapLatest{
            
            return self.titleCase.titles()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        
        
        let titleResult = title.map({ (title) -> [Title] in
            return (title.titles ?? [])
        })
        
        let titleSelected = getUserInformation.map({ (user) -> Title in
            let title = Title(code: user.titleCode, name: user.title)
            return title  })
        
        
        let addressDetails = Driver.combineLatest(addressDetails1,addressDetails2,addressDetails3)
        
        
        let addAddress = input.saveTrigger.withLatestFrom(addressDetails).map { (arg0)  in
            var ((
            country,
                addressName, nearstLandmark, isDefaultAddress, title, firstName, lastName,mobileCountry), (addressLineOne,addressLineTow,city,
                    area,
                        //                postCode,
                        mobileNum,personalDetails),( streetName,apartmentNumber,buildingName)) = arg0
            
            if(firstName == "" ) { firstName = personalDetails.firstName ?? ""}
            if(lastName == "" ) { lastName = personalDetails.lastName ?? ""}
            if addressLineOne == "" {
                addressLineOne = streetName
            }
            let address = Address(
                addressName: addressName,
                apartmentNumber: apartmentNumber,
                area: area,
                cellphone: nil,
                buildingName: buildingName,
                city: City(areas: nil, code: city.code, name: city.name ),
                companyName: nil,
                //                country: nil,
                country: country,
                defaultAddress: isDefaultAddress,
                district: nil,
                email: nil,
                firstName: firstName,
                formattedAddress: nil,
                id: nil,
                lastName:lastName,
                line1:
                    addressLineOne  ,
                line2: streetName,
                mobileCountry: mobileCountry ,
                mobileNumber: mobileNum,
                nearestLandmark: nearstLandmark,
                phone: nil,
                postalCode: nil,
                //                postalCode: postCode,
                region: nil,
                shippingAddress: nil,
                title: title.name,
                titleCode: title.code,
                town: nil,
                visibleInAddressBook: nil,
                streetName: streetName
                )
            
            return address
        }
        .flatMapLatest { [unowned self] in
            self.addressesUseCase.addAddress(userId: "current", address: $0)
                .trackError(errorTracker)
                .trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
        }
        
 
        let validation = input.validation.withLatestFrom(Driver.combineLatest(input.streetName , input.buildingName , input.apartmentNumber)).flatMapLatest { (arg0) -> Driver<String> in

            let (streetName, _, apartmentNumber) = arg0
            if streetName == "" {
                return Driver<String>.just("street field is required")
            }
//            else if buildingName == "" {
//                return Driver<String>.just("building Name field is required")
//            }
            else if apartmentNumber == "" {
                return Driver<String>.just("apartment number field is required")
            }
            else {
                return Driver<String>.just("")
            }
        }
 
        
        let city =  input.country.map { (Country) -> String in
            return Country.isocode ?? ""
        } .flatMapLatest { [unowned self] in
            self.citiesUseCase.fetchCities(countryIsoCode: $0)
                .trackError(errorTracker)
                .trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
        }
        
        let areaResult = input.city.map({ (area) -> [Area] in
            return (area.areas ?? [])})
        
        let cityResult = city.map({ (city) -> [City] in
            return (city.cities ?? [])})
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching, country:countryResult, city: cityResult,area : areaResult , codePhone : codePhoneResult, title: Driver.combineLatest(titleResult,titleSelected) , error: errors, addAddress: addAddress, getUserInformation: getUserInformation, validation: validation)
    }
}

