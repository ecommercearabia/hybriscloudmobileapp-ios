//
//  DeliveryAddressViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/4/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import NetworkPlatform
import Domain
import RxSwift
import RxDataSources
import SwipeCellKit


class DeliveryAddressViewModel: ViewModelType{
    
    var deletePublishSubject = PublishSubject<Address>()
    var defaultPublishSubject = PublishSubject<Address>()
    var editPublishSubject = PublishSubject<Address>()
    var isFromCart :Bool

  struct Input {
    var trigger: Driver<Void>
    let selectedAddress : Driver<Address>

   }
   struct Output {
       let fetching: Driver<Bool>
       let error: Driver<Error>
       let addresses: Driver<AddressesModel>
       let setDeliveryAddress: Driver<Void>
       let refreshAddress:Driver<Void>
       let editAddress:Driver<Address>

   }
   
   private let addressesUseCase: AddressesUseCase
   private let deliveryAddressUseCase: chickOutUseCase

    init(useCase: AddressesUseCase , deliveryAddressUseCase : chickOutUseCase , isFromCart : Bool) {
       self.addressesUseCase = useCase
       self.deliveryAddressUseCase = deliveryAddressUseCase
       self.isFromCart = isFromCart
   }
      
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let addresses = input.trigger.flatMapLatest { [unowned self] in
            return self.addressesUseCase.fetchAddresses(userId: "current")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
            
        }
        
        
        var   DeliveryAddress : Driver<Void>
         if(isFromCart)
            {  DeliveryAddress =  input.selectedAddress.withLatestFrom(input.selectedAddress).map { (address) in
            return address.id ?? ""
        }.flatMapLatest { [unowned self] in
            return self.deliveryAddressUseCase.SetDeliveryAddress(addressId: $0)
                              .trackActivity(activityIndicator)
//            .trackError(errorTracker)
            .asDriver(onErrorJustReturn: ())

            }
            }else {
              DeliveryAddress =  Driver<Void>.empty()
        }
        
        
        
       let deleteAddress = deletePublishSubject.asDriverOnErrorJustComplete().map { (deletedAddress)  -> String in
            
            return deletedAddress.id ?? ""

        }.flatMapLatest{ [unowned self] in
            return self.addressesUseCase.deleteAddress(userId: "current", addressId: $0)
                                         .trackActivity(activityIndicator)
                                         .trackError(errorTracker)
                                         .asDriverOnErrorJustComplete()

        }
        
       let defaultAddress = defaultPublishSubject.asDriverOnErrorJustComplete().map{(address) -> Address  in
            var addressForReturn = address
        addressForReturn.defaultAddress = !(address.defaultAddress ?? false)
            return addressForReturn
        }.flatMapLatest{ [unowned self] in
            return self.addressesUseCase.setDefaultAddress(userId: "current", address: $0)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()

        }
        
        let refreshAddress = Driver.merge(deleteAddress,defaultAddress)
        
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        
        return Output(
            fetching: fetching,
            error: errors, addresses: addresses,
            setDeliveryAddress: DeliveryAddress,
            refreshAddress: refreshAddress ,
            editAddress : editPublishSubject.asDriverOnErrorJustComplete()
        )
    }

    func dataSourceDeliveryAddress() -> RxTableViewSectionedAnimatedDataSource<AddressSection>
          {
              return RxTableViewSectionedAnimatedDataSource<AddressSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .bottom),configureCell: { ds, cv, indexPath, item in
                        
                  guard let cell = cv.dequeueReusableCell(withIdentifier: "addressesCell", for: indexPath) as? AddressCell else { return UITableViewCell()}
                  
                cell.setUp2(address: item)
                  cell.delegate = self

                  return cell

                    })
                }

   
}

extension DeliveryAddressViewModel: SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]?
    {
      
        guard let addressSelected = try? tableView.rx.model(at: indexPath) as Address else {
                return []
            }
            
        let defaultBtn = SwipeAction(style: .default, title: "Default".localized()) { action, index in
            
            self.defaultPublishSubject.onNext(addressSelected)

            
        }
        defaultBtn.backgroundColor =  UIColor(red:(255.0 / 255.0) , green:(149.0 / 255.0) ,
               blue:(28.0 / 255.0) , alpha: 1)
        defaultBtn.image = UIImage(named: "defaultAddress")
        
        let editBtn = SwipeAction(style: .default, title: "Edit".localized()) {
            action, index in
            
            self.editPublishSubject.onNext(addressSelected)

        }
        editBtn.backgroundColor = UIColor(red:(36.0 / 255.0) , green:(164.0 / 255.0) ,
        blue:(204.0 / 255.0) , alpha: 1)
        
        editBtn.image = UIImage(named: "edit")
        
        let deleteBtn = SwipeAction(style: .default, title: "Delete".localized()) { action, index in
            self.deletePublishSubject.onNext(addressSelected)
        }
        deleteBtn.backgroundColor = UIColor(red:(235.0 / 255.0) , green:(28.0 / 255.0) ,
        blue:(28.0 / 255.0) , alpha: 1)
        deleteBtn.image = UIImage(named: "delete")


        
        return [deleteBtn ,editBtn , defaultBtn]
        
    }
    
}
