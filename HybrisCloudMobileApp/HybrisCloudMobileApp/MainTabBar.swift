//
//  MainTabBar.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 1/6/21.
//  Copyright © 2021 Erabia. All rights reserved.
//

import UIKit

class MainTabBar: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

//        viewControllers?.forEach {
//            if let navController = $0 as? UINavigationController {
//                navController.topViewController?.view
//            } else {
//                $0.view.description
//            }
//        }
        
       let nav = viewControllers?[2] as? UINavigationController
        let topViewController = nav?.topViewController as? CartViewController
        topViewController?.view
        topViewController?.cartViewModel.refreshCartSubject.onNext(())
        
        let navMenu = viewControllers?[4] as? UINavigationController
         let topViewControllerMenu = navMenu?.topViewController as? MenuViewController
        topViewControllerMenu?.view
        topViewControllerMenu?.viewModel.getManual.onNext(())
        
        let navWebView = viewControllers?[3] as? UINavigationController
         let topViewControllerWebView = navWebView?.topViewController as? CommunityViewController
        topViewControllerWebView?.view

        
     }
    

 
}
