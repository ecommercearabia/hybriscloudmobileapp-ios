//
//  AppDelegate.swift
//  HybrisCloudMobileApp
//
//  Created by Ahmad Bader on 7/14/20.
//  Copyright © 2020 Erabia. All rights reserved.
//
import CoreData
import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import CCAvenueSDK
import Firebase
import FirebaseCore
import Siren
import UserNotifications
import FirebaseMessaging
import KeychainSwift


let googleApiKey = "AIzaSyApn_DRwYE631OHapamoYmIM0MTsZDfKxk"
let googlePlcaeApiKey = "AIzaSyCD2tZCjaNQeGfqhS2pJTmtywU7-AEM5hc"

let gcmMessageIDKey = "gcm.message_id"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , TAGContainerOpenerNotifier {
    func containerAvailable(_ container: TAGContainer!) {
        container.refresh()
    }
    
 
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        GMSServices.provideAPIKey(googleApiKey)
        GMSPlacesClient.provideAPIKey(googlePlcaeApiKey)
       
        //MARK: keyboard manager
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        hyperCriticalRulesExample()
        L102Localizer.DoTheMagic()
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()

        if application.applicationState == .inactive {
            if let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String : Any] {
                goToSpecificVC(userInfo: userInfo)
            }
        }
        
        let GTM = TAGManager.instance()

        GTM?.logger.setLogLevel(kTAGLoggerLogLevelVerbose) // enable verbose level log
        
        // back to integration 
        TAGContainerOpener.openContainer(withId: "GTM-N9T53ZS", tagManager: GTM, openType: kTAGOpenTypePreferFresh, timeout: nil, notifier: self)
        
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
    func hyperCriticalRulesExample() {
        let siren = Siren.shared
        siren.rulesManager = RulesManager(globalRules: .critical,
                                          showAlertAfterCurrentVersionHasBeenReleasedForDays: 1)

        siren.wail { results in
            switch results {
            case .success(let updateResults):
                print("AlertAction ", updateResults.alertAction)
                print("Localization ", updateResults.localization)
                print("Model ", updateResults.model)
                print("UpdateType ", updateResults.updateType)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    func reset() {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let stry = UIStoryboard(name: "Main", bundle: nil)
        rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
    }
   
}

extension AppDelegate {
    func application(_ application: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        if Auth.auth().canHandle(url) {
            return true
        }
        return false
        
    }
}
    
    

    

extension AppDelegate :  UNUserNotificationCenterDelegate, MessagingDelegate  {
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        let firebaseAuth = Auth.auth()
        if (firebaseAuth.canHandleNotification(userInfo)){
            return
        }
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    
        let firebaseAuth = Auth.auth()
        
        if (firebaseAuth.canHandleNotification(userInfo)){
            completionHandler(UIBackgroundFetchResult.newData)
            return
        }
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        print(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
 
        let deviceTokenString = deviceToken.map{ data in String(format: "%02.2hhx", data) }.joined()

        print("APNs token retrieved: \(deviceTokenString)")

        let firebaseAuth = Auth.auth()
        firebaseAuth.setAPNSToken(deviceToken, type: AuthAPNSTokenType.sandbox)
      
        Messaging.messaging().apnsToken = deviceToken
         
        
       
    }
    
    
    
    // on come notifcation
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        print(userInfo)

        completionHandler([.alert, .badge, .sound ])
        
    }
    
    
    
    // on click at notification
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo as Dictionary
        goToSpecificVC(userInfo: (userInfo as? [String:Any]) ?? [:])
        print(userInfo)
        
        completionHandler()
        
    }
    
    func goToSpecificVC(userInfo : [String:Any]){
        if let type = userInfo["type"] as? String ,let id = userInfo["id"] as? String{
            print(type)
            switch type {
            case "product":
                let mainStoryBoard = UIStoryboard(name: "ProductDetails", bundle: nil)
                let navController = mainStoryBoard.instantiateViewController(withIdentifier: "ProductDetailsViewControllerNav") as! UINavigationController
                let viewController = navController.viewControllers.first as? ProductDetailsViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                viewController?.productDetailsBehaviorSubject = BehaviorSubject<String>(value: id)
                viewController?.fromPustNotification = true
                appDelegate.window?.rootViewController = navController
            case  "category":
                let mainStoryBoard = UIStoryboard(name: "ProductListing", bundle: nil)
                let navController = mainStoryBoard.instantiateViewController(withIdentifier: "ListingNav") as! UINavigationController
                let viewController = navController.viewControllers.first as? ProductListingViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                viewController?.selectedCategorySubjec = BehaviorSubject<String>(value: id)
                viewController?.catId = id
                viewController?.fromPustNotification = true
                viewController?.fromBunners = true

                appDelegate.window?.rootViewController = navController
            default:
                break
            }
        }
        }
     
    
 

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken ?? "")")
       
      //  Foundation.UserDefaults.standard.set(fcmToken ?? "", forKey: "FirebaseToken")
        let dataDict:[String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
      
 
     }
    
    
   
    
}

 


 
