//
//  ProductReviewSection.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 9/1/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
struct ProductReviewSection {
    var header: String
    var items: [Item]
    
}

extension ProductReviewSection : AnimatableSectionModelType, Equatable {
    typealias Item = Review

    var identity: String {
        return header
    }

    init(original: ProductReviewSection, items: [Review]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: ProductReviewSection, rhs: ProductReviewSection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension Review: IdentifiableType, Equatable {    
   
    
    public static func == (lhs: Review, rhs: Review) -> Bool { return lhs.id  == rhs.id}
}

