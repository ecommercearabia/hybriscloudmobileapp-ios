//
//  ProductDetailsSection.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxDataSources


typealias MultibleSectionModel = AnimatableSectionModel<String,DetailsCellIndexes>

enum DetailsCellIndexes :IdentifiableType, Equatable{

    typealias Identity = String

    public var identity: Identity {

        switch self {

        case .product:

            return "product"

        case .ProductInformation:

            return "ProductInformation"

        case .Reviews:

            return "Reviews"
 
        case .ProductFacets:
            return "ProductFacets"
        
        case .YouMayAlsoLike:
        return "youMayAlsoLike"
            
        case.AddReview:
            return "addReview"

        }
    }

    

    static func == (lhs: DetailsCellIndexes, rhs: DetailsCellIndexes) -> Bool {

        return true

    }

    case product (product : ProductDetails)

    case ProductInformation (product : ProductDetails)
    
    case ProductFacets (product : ProductDetails)

    case Reviews (product : ProductDetails)
    
    case YouMayAlsoLike (product : ProductDetails)
    case AddReview(product : ProductDetails)
     
}


 
  
