//
//  ProductDetailsViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import NetworkPlatform
import KeychainSwift

class ProductDetailsViewModel {
    let toLoginSubject = PublishSubject<Void>()
    let toReviewsSubject = PublishSubject<ProductDetails>()
    let toAddReviewSubject = PublishSubject<String>()
    let toImageZoomSubject = PublishSubject<ProductDetailsImageZoomModel>()
    var informationCellState = BehaviorSubject<Int>(value:1)
    var toProductDetails = PublishSubject<ProductDetails>()
    var navToExpressDeliveryInfoSubject = PublishSubject<Void>()
  
   var  cellState = BehaviorSubject<Int>(value:1)
    var productId  = String()
    var productDetailsSubject = BehaviorSubject<[MultibleSectionModel]>(value: [MultibleSectionModel(model: "", items: [])])
   
    struct Input {
        let trigger : Driver<Void>
        let productDetails  : Driver<String>
    }
    
    struct Output {
        let fetching : Driver<Bool>
        let productDetailsCom : Driver<ProductDetails>
        let error : Driver<Error>
    }
    
    let bag = DisposeBag()
    let networkUseCaseProvider : Domain.UseCaseProvider = NetworkPlatform.UseCaseProvider()
    private let productDetailsUseCase : ProductDetailsUseCase
    
    init(productDetailsUseCase : ProductDetailsUseCase) {
        self.productDetailsUseCase = productDetailsUseCase
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let componentResult = input.trigger.withLatestFrom(input.productDetails).flatMapLatest { [ self] in
            return self.productDetailsUseCase.productDetails(productCode: $0 )  .trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
        }
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching, productDetailsCom: componentResult, error: errors)
    }
    
    
    func dataSourceProductDetails() -> RxTableViewSectionedAnimatedDataSource<MultibleSectionModel> {
        return
            RxTableViewSectionedAnimatedDataSource<MultibleSectionModel>(animationConfiguration: AnimationConfiguration(insertAnimation: .automatic, reloadAnimation: .none, deleteAnimation: .none),configureCell: { ds, cv, indexPath, item in
               
                switch item {
                case .product(let product):
                    guard let cell = cv.dequeueReusableCell(withIdentifier: "ProductDetailsTableViewCell", for: indexPath)as? ProductDetailsTableViewCell else { return UITableViewCell() }
                    print("reuse product cell")
                    
                    cell.setUp(productDetails: product )
                    cell.wishListButton.rx.tap.subscribe(onNext: { [unowned self](void) in
                        if KeychainSwift().get("user") == "anonymous" {
                            
                            self.toLoginSubject.onNext(())
                        }
                    }).disposed(by: cell.disposeBag)
                    
                    cell.toImageZoomSubject.bind(to: self.toImageZoomSubject).disposed(by: cell.disposeBag)
                    
                    cell.seeAllReviews.rx.tap.subscribe(onNext: {[unowned self] (void) in
                        self.toReviewsSubject.onNext(product)
                    }).disposed(by: cell.disposeBag)
                    
                    cell.navToExpressDeliveryInfoSubject.subscribe(onNext: {(void) in
                        self.navToExpressDeliveryInfoSubject.onNext(())
                    }).disposed(by: cell.disposeBag)

                    return cell
            
                    
                case .ProductInformation(let productClassification):
                    guard  let cell = cv.dequeueReusableCell(withIdentifier: "ProductDetailsDescriptionTableViewCell", for: indexPath)as? ProductDetailsDescriptionTableViewCell else { return UITableViewCell() }
                    cell.descriptionTitleLbl.text = "Product Information".localized()
                    cell.descriptionTextView.attributedText = NSAttributedString(string: "")
                
                    cell.showBttn.rx.tap.scan(ButtonState.Show){ lastState, _ in
                    
                                                switch lastState {
                                                case .Show :
                                                    cell.descriptionTextView.attributedText =
                                                        productClassification.description?.htmlAttributedString()
                                                    cell.showBttn.setImage(#imageLiteral(resourceName: "downArroww"), for: .normal)
                                                    cell.layoutIfNeeded()
                                                    self.cellState.onNext(0)
                                                    return .Hidden
                                                case .Hidden :
                                                    cell.descriptionTextView.attributedText = NSAttributedString(string: "")
                                                    cell.showBttn.setImage(#imageLiteral(resourceName: "rightArroww"), for: .normal)
                                                    cell.layoutIfNeeded()
                                                    self.cellState.onNext(0)
                                                    return .Show
                                                }
                                               
                        
                    
                                            }.subscribe().disposed(by: cell.disposeBag)
                    
                   
                    print(productClassification.description ?? "")
                    
                    return cell
                    
                case .AddReview( let productClassification):
                    guard  let cell = cv.dequeueReusableCell(withIdentifier: "ReviewTableViewCell", for: indexPath)as? ReviewTableViewCell else { return UITableViewCell() }
                   
                    cell.showBttn.rx.tap.scan(ButtonState.Show){ lastState, _ in

                        switch lastState {
                        case .Show :
                            cell.heightView.constant = CGFloat(30)
                            
                            cell.layoutIfNeeded()
                            cell.showBttn.setImage(#imageLiteral(resourceName: "downArroww"), for: .normal)
                            self.cellState.onNext(0)
                            return .Hidden
                        case .Hidden :

                            cell.heightView.constant = CGFloat(0)
                            cell.showBttn.setImage(#imageLiteral(resourceName: "rightArroww"), for: .normal)
                            self.cellState.onNext(0)
                            cell.layoutIfNeeded()
                            return .Show
                        }
                     

                    }.subscribe().disposed(by: cell.disposeBag)
                    
                    cell.addReviewButton.rx.tap.subscribe(onNext: { [unowned self](Void) in
                        print("click")
                        self.toAddReviewSubject.onNext((productClassification.code ?? ""))
                    }).disposed(by: cell.disposeBag)
                    cell.layoutIfNeeded()
                    return cell
                    
                case .Reviews(let product):
                    guard let cell = cv.dequeueReusableCell(withIdentifier: "ProductDetailsTableViewCell", for: indexPath)as? ProductDetailsTableViewCell else { return UITableViewCell() }
                    cell.setUp(productDetails: product )
                    cell.wishListButton.rx.tap.subscribe(onNext: { [unowned self](void) in
                        if KeychainSwift().get("user") == "anonymous" {
                            
                            self.toLoginSubject.onNext(())
                        }
                    }).disposed(by: cell.disposeBag)
                    
                    return cell
                case.ProductFacets(let productClassification):
                    
                    guard  let cell = cv.dequeueReusableCell(withIdentifier: "ProductDetailsDescriptionTableViewCell", for: indexPath)as? ProductDetailsDescriptionTableViewCell else { return UITableViewCell() }
                    cell.descriptionTitleLbl.text = "Nutritional Facts".localized()
                    cell.descriptionTextView.attributedText = NSAttributedString(string: "")
                
                    cell.showBttn.rx.tap.scan(ButtonState.Show){ lastState, _ in

                                                switch lastState {
                                                case .Show :
                                                    cell.descriptionTextView.attributedText =
                                                        productClassification.nutritionFacts?.htmlAttributedString()
                                                    cell.showBttn.setImage(#imageLiteral(resourceName: "downArroww"), for: .normal)
                                                    cell.layoutIfNeeded()
                                                    self.cellState.onNext(0)
                                                    return .Hidden
                                                case .Hidden :
                                                    cell.descriptionTextView.attributedText = NSAttributedString(string: "")
                                                    cell.showBttn.setImage(#imageLiteral(resourceName: "rightArroww"), for: .normal)
                                                    cell.layoutIfNeeded()
                                                    self.cellState.onNext(0)
                                                    return .Show
                                                }



                                            }.subscribe().disposed(by: cell.disposeBag)
                    
                   
                    
                    return cell
                    
                    
                    
//                    guard  let cell = cv.dequeueReusableCell(withIdentifier: "ProductDetailsFacetsTableViewCell", for: indexPath)as? FacetsDetailsTableViewCell else { return UITableViewCell() }
//                    cell.naturalFactsSubject.onNext(productClassification.features ?? [])
//
//                    cell.showBttn.rx.tap.scan(ButtonState.Show){ lastState, _ in
//
//                        switch lastState {
//                        case .Show :
//                            cell.tableViewHeight.constant = CGFloat((productClassification.features?.count ?? 0) * 50)
//
//                            cell.layoutIfNeeded()
//                            cell.showBttn.setImage(#imageLiteral(resourceName: "downArroww"), for: .normal)
//                            self.cellState.onNext(0)
//                            return .Hidden
//                        case .Hidden :
//
//                            cell.tableViewHeight.constant = CGFloat(0)
//                            cell.showBttn.setImage(#imageLiteral(resourceName: "rightArroww"), for: .normal)
//                            self.cellState.onNext(0)
//                            cell.layoutIfNeeded()
//                            return .Show
//                        }
//
//
//                    }.subscribe().disposed(by: cell.bag)
//
//
//                    return cell
                    
                case .YouMayAlsoLike(let product):
                    
                    guard  let cell = cv.dequeueReusableCell(withIdentifier: "youMayAlsoLike", for: indexPath)as? YouMayAlsoLikeTableViewCell else { return UITableViewCell() }
                    cell.setUp()
                    cell.productDetails.onNext(product)
                    cell.viewModel.toLoginSubject.bind(to: self.toLoginSubject).disposed(by: cell.bag)
                    cell.toProductDetails.bind(to: self.toProductDetails).disposed(by: cell.bag)
                    return cell
                    
                }
            })
    }
}

public enum ClassificationsTypes: String {
    case FoodCrowdClassifications
    case KafDescriptionandcalssification
    case KafRecipes
    case KafUnitsCalssification
    case KafDairyandLifestyle
    case KafListingCalssification
}

public enum ButtonState {
    case Show, Hidden
  
}
