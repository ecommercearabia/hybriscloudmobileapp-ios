//
//  YouMayAlsoLikeTableViewCellViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 8/18/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import NetworkPlatform
import KeychainSwift

class YouMayAlsoLikeTableViewCellViewModel: ViewModelType{
    
    private let youMayAlsoLikeUseCase: BestDealProductUseCase
    let toLoginSubject = PublishSubject<Void>()
    let bag = DisposeBag()
    
    init( youMayAlsoLikeUseCase: BestDealProductUseCase ) {
        self.youMayAlsoLikeUseCase = youMayAlsoLikeUseCase
    }
    
    
    struct Input {
        let trigger: Driver<Void>
        let productDetails:Driver<ProductDetails>
        
    }
    struct Output {
        let fetching: Driver<Bool>
        let youMayAlsoLikeProducts: Driver<[BestDealProductSection]>
        let error: Driver<Error>
        
    }
    
    let networkUseCaseProvider : Domain.UseCaseProvider = NetworkPlatform.UseCaseProvider()
    
    
    func transform(input: Input) -> Output {
        
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let productResult = input.productDetails.flatMap { (product) -> Driver<[BestDealProductSection]> in
            
            var products : [String] = []
            
            product.productReferences?.forEach({ (element) in
                products.append(element.target?.code ?? "")
            })
            
            let productMap =  products.map { (element) ->  Driver<ProductDetails> in
                self.youMayAlsoLikeUseCase.bestDealProduct(productCode: element)
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
                
            }
            let combine =  Driver<ProductDetails>.combineLatest(productMap)
            return combine.asDriver().map { (products) -> [BestDealProductSection] in
                return [BestDealProductSection(header: "You May Also Like".localized(), items: products)]
            }
        }
        
        
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching,
                      youMayAlsoLikeProducts: productResult,
                      error: errors)
    }
    func dataSource() ->
        RxCollectionViewSectionedAnimatedDataSource<BestDealProductSection> {
            return RxCollectionViewSectionedAnimatedDataSource<BestDealProductSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .bottom),configureCell: { ds, cv, indexPath, item in
                
                
                guard let cell = cv.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as? ProductCollectionViewCell else {
                    return UICollectionViewCell()
                }
                cell.setUp(prodcutDeatis: item)
                cell.wishListButton.rx.tap.subscribe(onNext: { (void) in
                    if KeychainSwift().get("user") == "anonymous" {
                        
                        self.toLoginSubject.onNext(())
                    }
                }).disposed(by: self.bag)
                
                
                return cell
                
            })
    }
}


