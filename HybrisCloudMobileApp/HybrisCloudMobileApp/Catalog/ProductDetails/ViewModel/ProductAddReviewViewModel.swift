//
//  ProductAddReviewViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 9/2/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
class ProductAddReviewViewModel: ViewModelType{
    


  struct Input {
    var trigger: Driver<Void>
    var productCode: Driver<String>
    var rateReview: Driver<Double?>
    var titleReview: Driver<String>
    var descrptionReview: Driver<String>
    var nameReview: Driver<String>
    var addReviewManual:Driver<Void>
   }
   struct Output {
       let fetching: Driver<Bool>
       let error: Driver<Error>
       let addToReviewOutput: Driver<Review>
       let canAddToReviewOutput: Driver<Bool>

   }
   
   private let productReviewUseCase:ProductReviewUseCase

    init(productReviewUseCase: ProductReviewUseCase) {
       self.productReviewUseCase = productReviewUseCase
   }
      
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let inputForAddReview = Driver.combineLatest(input.rateReview,input.titleReview,input.descrptionReview,input.nameReview,input.productCode)
        
        let canAddToReviewInput = Driver.combineLatest(input.rateReview,input.titleReview,input.descrptionReview)
        
        
        let canAddToReview = input.trigger.withLatestFrom(canAddToReviewInput).map { (arg0) -> Bool in
                   
                   let (double, title,descrption) = arg0
                              if(title == "" || descrption == "" ||  double == nil)
                              {
                                  return false
                              }else {
                                  return true
                              }
               }
        
        let addToReview = input.addReviewManual.withLatestFrom(inputForAddReview).map { (arg0) -> AddToReview in
            
            let (rate, title, descrption, name, productCode) = arg0
            var review = Review()
            review.headline = title
            review.rating = rate
            review.comment = descrption
            if(name == "" ) {review.alias = nil}else {review.alias = name}
            return AddToReview(review: review, productCode: productCode)
        }.flatMapLatest { [ self] in
            return self.productReviewUseCase.addReviews(
                productCode: $0.productCode, review: $0.review)
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
        }
             
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        
        return Output(
            fetching: fetching,
            error: errors, addToReviewOutput: addToReview ,
            canAddToReviewOutput : canAddToReview       )
    }

   
}
public struct AddToReview
{
    public let review: Review
    public let productCode: String
}
