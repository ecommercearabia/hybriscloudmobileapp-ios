//
//  ProductReviewsViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 9/1/20.
//  Copyright © 2020 Erabia. All rights reserved.
//
//
//import Foundation
//import NetworkPlatform
//import Domain
//import RxSwift
//import RxDataSources



class ProductReviewsViewModel: ViewModelType{
    


  struct Input {
    var trigger: Driver<Void>
    var product: Driver<ProductDetails>

   }
   struct Output {
       let fetching: Driver<Bool>
       let error: Driver<Error>
       let reviews: Driver<Reviews>
   }
   
   private let productReviewsUseCase: ProductReviewsUseCase

    init(productReviewsUseCase: ProductReviewsUseCase) {
       self.productReviewsUseCase = productReviewsUseCase
   }
      
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let reviews = input.trigger.withLatestFrom(input.product).map { (product)   in
            return product.code ?? ""
        }.flatMapLatest { [ self] in
            return self.productReviewsUseCase.productReviews(productCode: $0)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
 
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        
        return Output(
            fetching: fetching,
            error: errors,
            reviews: reviews
        )
    }

    func dataSourceProductReviews() -> RxTableViewSectionedAnimatedDataSource<ProductReviewSection>
          {
              return RxTableViewSectionedAnimatedDataSource<ProductReviewSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .bottom),configureCell: { ds, cv, indexPath, item in
                        
                  guard let cell = cv.dequeueReusableCell(withIdentifier: "productReviewCell", for: indexPath) as? ProductReviewCell else { return UITableViewCell()}
                  
                  cell.setUp(review: item)

                  return cell

                    })
                }

   
}
