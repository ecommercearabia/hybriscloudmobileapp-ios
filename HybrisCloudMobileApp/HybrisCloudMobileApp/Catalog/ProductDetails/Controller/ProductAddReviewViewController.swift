//
//  ProductAddReviewViewController.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 9/2/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Cosmos

class ProductAddReviewViewController: UIViewController {
   
    let bag = DisposeBag()
    var codeBehaviorSubject:BehaviorSubject<String>?
    var rateBehaviorSubject = BehaviorSubject<Double?>(value: nil)
    var addReviewSubject = PublishSubject<Void>()

    @IBOutlet weak var rateReviewsCosmosView: CosmosView!
    
     @IBOutlet weak var titleReviewTextField: UITextField!
    
    @IBOutlet weak var nameReviewTextField: UITextField!
    
    
    @IBOutlet weak var descriprionReviewTextView: UITextView!

    @IBOutlet weak var addReviewUIButton: UIButton!

    override func viewDidLoad() {
        
        super.viewDidLoad()
                
        guard let getCode = codeBehaviorSubject else{
                   return}
        
        rateReviewsCosmosView.didFinishTouchingCosmos = { rating in self.rateBehaviorSubject.onNext(rating) }

       
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()

        let viewModel = ProductAddReviewViewModel(productReviewUseCase: networkUseCaseProvider.makeProductReviewUseCase())
    

        let input = ProductAddReviewViewModel.Input(
            trigger:addReviewUIButton.rx.tap.asDriver(),
            productCode: getCode.asDriverOnErrorJustComplete(),
            rateReview: rateBehaviorSubject.asDriverOnErrorJustComplete(),
            titleReview:  titleReviewTextField.rx.text.orEmpty.asDriver(),
            descrptionReview: descriprionReviewTextView.rx.text.orEmpty.asDriver(),
            nameReview: nameReviewTextField.rx.text.orEmpty.asDriver(),
            addReviewManual : addReviewSubject.asDriverOnErrorJustComplete()
        )

        let output = viewModel.transform(input: input)
        
        output.canAddToReviewOutput.asObservable().subscribe(onNext: { (can) in
            if can {
                self.addReviewSubject.onNext(())
            }else {
                self.errorLoaf(message: "please fill the data".localized())
            }
        }).disposed(by: bag)
        
        output.addToReviewOutput.asObservable().subscribe(onNext: { (Review) in
            self.navigationController?.popViewController(animated: true)
            }).disposed(by: bag)

        output.error.asObservable().subscribe(onNext: { (error) in
            let msgError = error as? ErrorResponse
            let msg = msgError?.errors?.map({ (ErrorResponseElement) -> String in
                return ErrorResponseElement.message ?? ""
                }).joined(separator: " , ")
            self.errorLoaf(message: msg ?? "")
            }).disposed(by: bag)
        

    }
    
    
}
