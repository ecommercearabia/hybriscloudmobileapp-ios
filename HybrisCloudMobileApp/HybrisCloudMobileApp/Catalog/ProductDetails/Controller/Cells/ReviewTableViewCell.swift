//
//  ReviewTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 27/12/2020.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {
    @IBOutlet weak var addReviewButton: UIButton!
    
    @IBOutlet weak var showBttn: UIButton!
    
    
    @IBOutlet weak var heightView: NSLayoutConstraint!
    
    
    var disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.disposeBag = DisposeBag()
    }

}
