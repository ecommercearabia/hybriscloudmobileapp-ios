//
//  FacetsDetailsTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by Admin on 2020-08-13.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import RxSwift
import Domain

class FacetsDetailsTableViewCell: UITableViewCell, UITableViewDelegate {
    
    @IBOutlet weak var facetsTableView: UITableView!
    @IBOutlet weak var showBttn: UIButton!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    var naturalFactsSubject = BehaviorSubject<[ProducttFeature]>(value: [])
    var bag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
   //     facetsTableView.isHidden = true
     naturalFactsSubject.bind(to: facetsTableView.rx.items(cellIdentifier: "featureCell", cellType: FeatureTableViewCell.self)) {
            row, element, cell in
            cell.layer.cornerRadius = 6.0
            cell.contentView.backgroundColor = (row % 2 == 1) ? UIColor.white : UIColor.lightGray.withAlphaComponent(0.7)
            cell.title.text = element.name ?? ""
            cell.value.text = element.featureValues?.first?.value ?? ""
        }.disposed(by: bag)
        facetsTableView.rx.setDelegate(self).disposed(by: bag)
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
