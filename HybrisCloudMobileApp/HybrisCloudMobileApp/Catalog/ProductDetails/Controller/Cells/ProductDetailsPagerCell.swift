//
//  ProductDetailsPagerCell.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import FSPagerView
class ProductDetailsPagerCell: FSPagerViewCell {
    
    @IBOutlet weak var productDetailsImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.productDetailsImage.layer.cornerRadius = self.productDetailsImage.bounds.width / 2
        productDetailsImage.layer.masksToBounds = true
    }
    
    func makeSelected() {
        productDetailsImage.layer.cornerRadius = contentView.frame.width / 2
        productDetailsImage.layer.masksToBounds = true
    }
    
    func makeUnSelected() {
        productDetailsImage.layer.cornerRadius = contentView.frame.width / 2
        productDetailsImage.layer.masksToBounds = false
    }
}
