//
//  ProductDetailsDescriptionTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class ProductDetailsDescriptionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var descriptionTextView: UILabel!
    @IBOutlet weak var descriptionTitleLbl: UILabel!
    @IBOutlet weak var showBttn: UIButton!
       
    var disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.disposeBag = DisposeBag()
    }


}

extension String {
    func htmlAttributedString() -> NSMutableAttributedString? {
        guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil) else { return nil }
        return html
    }
    
    var html2AttributedString: String? {
       guard let data = data(using: .utf8) else { return nil }
       do {
           return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil).string

       } catch let error as NSError {
           print(error.localizedDescription)
           return  nil
       }
     }
    
}

