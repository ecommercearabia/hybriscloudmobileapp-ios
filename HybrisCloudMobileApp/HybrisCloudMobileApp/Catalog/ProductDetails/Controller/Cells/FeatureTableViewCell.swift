//
//  FeatureTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by Admin on 2020-08-15.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class FeatureTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var value: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
