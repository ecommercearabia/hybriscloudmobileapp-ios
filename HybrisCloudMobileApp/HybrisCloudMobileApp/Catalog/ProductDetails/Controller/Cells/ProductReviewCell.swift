//
//  ProductReviewCell.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 9/1/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Cosmos

class ProductReviewCell: UITableViewCell {

    @IBOutlet weak var StarRateReview: CosmosView!
    @IBOutlet weak var TitleReview: UILabel!
    @IBOutlet weak var DescriptionReview: UILabel!
    @IBOutlet weak var NameReview: UILabel!
    @IBOutlet weak var DateReview: UILabel!
        
      func setUp(review : Review) {
        
        self.TitleReview.text = review.headline ?? ""
        
        self.StarRateReview.rating = review.rating ?? 0.0
        
        if(review.comment == nil || review.comment == ""){
            DescriptionReview.isHidden = true
        }else {
            DescriptionReview.isHidden = false
            DescriptionReview.text =  review.comment ?? ""
        }

        if(review.alias == nil || review.alias == ""){
            NameReview.isHidden = true
        }else {
            NameReview.isHidden = false
            NameReview.text =  review.alias ?? ""
        }

//        if(review.date == nil || review.date == ""){
            DateReview.isHidden = true
//        }else {
//            DateReview.isHidden = false
//            DateReview.text =  review.date ?? ""
//        }
      }

}
