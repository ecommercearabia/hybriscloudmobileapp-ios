//
//  ProductDetailsTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import FSPagerView
import Domain
import NetworkPlatform
import Kingfisher
import SKCountryPicker
import KeychainSwift
import Cosmos
import FirebaseAnalytics

class ProductDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var frozenImage: UIImageView!
    @IBOutlet weak var dryImage: UIImageView!
    @IBOutlet weak var chilledImage: UIImageView!
    @IBOutlet weak var expressStack: UIStackView!
    
    
    @IBOutlet weak var wishListButton: UIButton!
    @IBOutlet weak var steperVire: UIView!
    @IBOutlet weak var addToCartStack: UIStackView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var skuId: UILabel!
    @IBOutlet weak var summary: UILabel!
    @IBOutlet weak var storage: UILabel!
    @IBOutlet weak var countryFlagIcon: UIImageView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var quantityDesc: UILabel!
    @IBOutlet weak var priceCurrency: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var oldPriceCurrency: UILabel!
    @IBOutlet weak var productOldPrice: UILabel!
    @IBOutlet weak var inStockIcon: UIImageView!
    @IBOutlet weak var inStockLbl: UILabel!
    @IBOutlet weak var quantityUITextField: UITextField!
    @IBOutlet weak var seeAllReviews: UIButton!
    @IBOutlet weak var addToCartUiButton: UIButton!
    @IBOutlet weak var rateCosmosView: CosmosView!
    @IBOutlet weak var decreaseQuantityUiButton: UIButton!
    @IBOutlet weak var increaseQuantityUiButton: UIButton!
    
    @IBOutlet weak var discView: UIView!
    @IBOutlet weak var discLbl: UILabel!
    
    @IBOutlet weak var glutenFree: UIImageView!
    @IBOutlet weak var organic: UIImageView!
    @IBOutlet weak var vegan: UIImageView!

    @IBOutlet weak var typeStack: UIStackView!
    
    @IBOutlet weak var unitOfMeasureLbl: UILabel!
    
    @IBOutlet weak var infoExpressButton: UIButton!
    @IBOutlet weak var discountHeight: NSLayoutConstraint!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var productPagerView: FSPagerView!{
        didSet{
            self.productPagerView.register(UINib(nibName: "ProductDetailsPagerCell", bundle: nil), forCellWithReuseIdentifier: "ProductDetailsPagerCell")
            self.productPagerView.itemSize = FSPagerView.automaticSize
            self.productPagerView.delegate = self
            self.productPagerView.dataSource = self
            self.productPagerView.transformer = FSPagerViewTransformer(type: .linear)
            self.productPagerView.interitemSpacing = 0
            self.productPagerView.itemSize = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)
            self.productPagerView.isInfinite = true
            self.productPagerView.transformer = NoVerticalScalePagerViewTransformer(type: .linear)
            self.productPagerView.layer.cornerRadius = self.productPagerView.bounds.width / 2
        }
    }
    
    @IBOutlet weak var productPagerControl: FSPageControl!{
        didSet{
            self.productPagerControl.numberOfPages = banners.count
            self.productPagerControl.contentHorizontalAlignment = .center
            self.productPagerControl.contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    var product : ProductDetails!
    var productDetailsPublishSubject = PublishSubject<ProductDetails>()
    var disposeBag = DisposeBag()
    var viewModel: ProductDetailsCellViewModel!
    let component = PublishSubject<HybrisComponent>()
    var banners: [ProducttImageElement] = []
    var quantitySubject = PublishSubject<String>()
    let createCartSubject = PublishSubject<Void>()
    let manualAddTopPoductSubject = PublishSubject<Void>()
    let toLoginSubject = PublishSubject<Void>()
    let addToWishListSubject = PublishSubject<Void>()
     let toImageZoomSubject = PublishSubject<ProductDetailsImageZoomModel>()
    let navToExpressDeliveryInfoSubject = PublishSubject<Void>()
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setUp(productDetails: ProductDetails) {
        print("setUp product cell")
        preparation()
        
        self.infoExpressButton.rx.tap.subscribe(onNext: {(void) in
            let alert = UIAlertController(title:"", message: "Always in stock, ready to ship, 60min delivery.".localized(), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Learn More".localized(), style: .default, handler: { _ in
                
                self.navToExpressDeliveryInfoSubject.onNext(())
                        }))
            
            alert.addAction(UIAlertAction(title: "Close".localized(), style: .default ))
            UIApplication.topViewController()?.present(alert, animated: true)
                 
            
        }).disposed(by: disposeBag)
        productPagerView.register(UINib(nibName: "ProductDetailsPagerCell", bundle: nil), forCellWithReuseIdentifier: "ProductDetailsPagerCell")
        
        if productDetails.inWishlist == true {

                    self.wishListButton.tintColor = UIColor(named: "AppMainColor")

                } else {

                    self.wishListButton.tintColor =  #colorLiteral(red: 0.3686274886, green: 0.3686274886, blue: 0.3686274886, alpha: 1)

                }
        
        if productDetails.discount != nil {
            self.discView.isHidden = false
            self.discLbl.text = "-\(productDetails.discount?.percentage  ?? 0)%"
            self.productOldPrice.isHidden = false
            
            self.productPrice.text = productDetails.discount?.discountPrice?.formattedValue ?? ""
            self.productOldPrice.attributedText = productDetails.price?.formattedValue?.strikeThrough()
            
            
            discountHeight.constant = 20.5
            discountLbl.text = "Save".localized() + " \(productDetails.discount?.percentage ?? 0)% " + "Discount".localized()
            discountLbl.isHidden = false
        }
        else {
            self.discView.isHidden = true
            self.productOldPrice.isHidden = true
            self.productPrice.text = productDetails.price?.formattedValue ?? ""
            discountHeight.constant = 0
            discountLbl.isHidden = true
        }
        
        !(productDetails.glutenFree ?? false) ? (self.glutenFree.isHidden = true) : (self.glutenFree.isHidden = false)
        !(productDetails.organic ?? false) ? (self.organic.isHidden = true) : (self.organic.isHidden = false)
        !(productDetails.vegan ?? false) ? (self.vegan.isHidden = true) : (self.vegan.isHidden = false)
        
        !(productDetails.frozen ?? false) ? (self.frozenImage.isHidden = true) : (self.frozenImage.isHidden = false)
        !(productDetails.dry ?? false) ? (self.dryImage.isHidden = true) : (self.dryImage.isHidden = false)
        !(productDetails.chilled ?? false) ? (self.chilledImage.isHidden = true) : (self.chilledImage.isHidden = false)
        !(productDetails.express ?? false) ? (self.expressStack.isHidden = true) : (self.expressStack.isHidden = false)
        
        wishListButton.rx.tap.subscribe(onNext: { (void) in
            if KeychainSwift().get("user") == "anonymous" {
                
                self.toLoginSubject.onNext(())
            } else {
                self.addToWishListSubject.onNext(())
            }
        }).disposed(by: self.disposeBag)
 
        
        quantitySubject.bind(to: quantityUITextField.rx.text).disposed(by: disposeBag)

        productDetails.classifications?.first?.features?.forEach{ (prod) in
            if prod.name == "Country of origin isocode"{
                
                if let country = CountryManager.shared.country(withName: prod.featureValues?.first?.value ?? "")  {
                    self.countryFlagIcon.image = country.flag
                    
                    
                }else {
                    if let countryFromCode = CountryManager.shared.country(withCode: String((prod.featureValues?.first?.value ?? "").prefix(2)))  {
                      
                    self.countryFlagIcon.image = countryFromCode.flag
                    }
                }
                
            }
        }
        
        productDetails.classifications?.first?.features?.forEach{ (prod) in
                  if prod.name == "Storage Instructions"{
                    let storage = (prod.featureValues?.compactMap({ (productFeatureValue) -> String in
                        return productFeatureValue.value ?? ""
                    }) ?? []).joined(separator:" , ")
                    if(storage != "")
                    {
                        self.storage.isHidden = false
                        self.storage.text = ((prod.name ?? "" ) + " : " + (storage))
                    } else {
                        self.storage.isHidden = true
                    }
                        }
              }
        
        productName.text = productDetails.name?.html2AttributedString ?? ""
        skuId.text = "\(productDetails.identity)"
        if(productDetails.summary == nil || productDetails.summary == "")
        {
            summary.isHidden = true
        }else {
            summary.isHidden = false
            summary.text = "\(productDetails.summary ?? "")"
        }
        
        if(productDetails.unitOfMeasureDescription == nil || productDetails.unitOfMeasureDescription == "")
        {
            unitOfMeasureLbl.isHidden = true
        }else {
            unitOfMeasureLbl.isHidden = false
            unitOfMeasureLbl.text = "\(productDetails.unitOfMeasureDescription ?? "")"
        }
        
        
        countryName.text = productDetails.countryOfOrigin ?? ""
 
        
        if productDetails.stock?.stockLevel ?? 0 <= 0{
            inStockIcon.isHidden = true
            
            addToCartStack.isHidden = true
            inStockLbl.text =  "Out Of Stock".localized()
        }
        
        
        quantityDesc.text = "- " +  (productDetails.baseOptions?.first?.selected?.variantOptionQualifiers?.first?.value ?? "")
        productDetailsPublishSubject.onNext(productDetails)
        self.product = productDetails
        quantitySubject.onNext("1")
        banners = productDetails.images?.filter({ (ProducttImage) -> Bool in
            ProducttImage.format == "zoom" && ProducttImage.imageType == "GALLERY"
        }) ?? []
        productPagerControl.numberOfPages = banners.count
        
        productPagerControl.setFillColor(#colorLiteral(red: 0.5215686275, green: 0.5098039216, blue: 0.2, alpha: 1) ,for: .selected)
        productPagerControl.setFillColor(#colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1), for: .normal)
        
        productPagerControl.contentHorizontalAlignment = .center
        productPagerControl.contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        productPagerView.reloadData()
    }
    
    func preparation(){
        
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        
        viewModel = ProductDetailsCellViewModel(useCase: networkUseCaseProvider.makeWishListUseCase(),addtoCartUseCase: networkUseCaseProvider.makeAddToCartUseCase(), cartUseCase: networkUseCaseProvider.makeCartUseCase())
        
        let input = ProductDetailsCellViewModel.Input(addtoCartTrigger: Driver.merge(addToCartUiButton.rx.tap.asDriver() , self.manualAddTopPoductSubject.asDriverOnErrorJustComplete())
            , products: productDetailsPublishSubject.asObserver().asDriverOnErrorJustComplete(),
              quantity: quantitySubject.asObserver().asDriverOnErrorJustComplete(),
              increaseQuantityTrigger: increaseQuantityUiButton.rx.tap.asDriver(),
              decreaseQuantityTrigger: decreaseQuantityUiButton.rx.tap.asDriver(), createCartTrigger: createCartSubject.asDriverOnErrorJustComplete(), addToWishListTrigger: addToWishListSubject.asDriverOnErrorJustComplete())
        
        let output = viewModel.transform(input: input)
        handelAddToWishList(output : output)
        handelAddToCartResult(output : output)
        handelError(output : output)
    }
    
    func handelError(output : ProductDetailsCellViewModel.Output) {

        output.error.asObservable().subscribe(onNext: { (Error) in
            let error = Error as? ErrorResponse
            if error?.errors?.first?.type == "CartError" {
                self.createCartSubject.onNext(())
            }
            self.createCartSubject.onNext(())

        }).disposed(by: self.disposeBag)
        
        output.errorAddToWishList.asObservable().subscribe(onNext: { (Error) in
            let error = Error as? ErrorResponse
            UIApplication.topViewController()?.errorLoaf(message: error?.errors?.first?.message ?? "")
        }).disposed(by: self.disposeBag)
        
    }
    func handelAddToCartResult(output : ProductDetailsCellViewModel.Output) {
        
        output.createdCart.asObservable().subscribe(onNext: { (CartModel) in
            print("created Cart ")
            KeychainSwift().set(CartModel.guid ?? "", forKey: "guid")
            self.manualAddTopPoductSubject.onNext(())
        }).disposed(by: self.disposeBag)
        
        output.addToCartResult.asObservable().subscribe(onNext: { (AddedToCartModel) in
            print("add To Cart Result")
            
            let quantity = (Double(AddedToCartModel.quantity ?? 0) )
            let price = AddedToCartModel.entry?.basePrice?.value ?? 0.0
            let total = quantity * price
            let itemDetails: [String: Any] = [
                AnalyticsParameterCurrency: AddedToCartModel.entry?.basePrice?.currencyISO ?? "",
                AnalyticsParameterValue:  total,
                AnalyticsParameterItemID: AddedToCartModel.entry?.product?.code ?? "" ,
                AnalyticsParameterItemName: AddedToCartModel.entry?.product?.name ?? "",
                AnalyticsParameterPrice: price
            ]
            
            Analytics.logEvent(AnalyticsEventAddToCart, parameters: itemDetails)
            
            let tabBarController = UIApplication.topViewController()?.tabBarController
            let cartNav = tabBarController?.viewControllers?[2] as? UINavigationController
           let cartVC =  cartNav?.topViewController as? CartViewController
             cartVC?.cartViewModel.refreshCartSubject.onNext(())

            Loaf("product added to cart successfully".localized(), state: .success(L102Language.isRTL ? .right : .left),location: .top, sender: UIApplication.topViewController() ?? UIViewController()).show()
            
        }).disposed(by: self.disposeBag)
        
        output.increaseQuantityOutput.asObservable().subscribe( onNext:
            {
                (quantity) in
                self.quantitySubject.onNext(quantity)
                
        }, onError:
            {
                (error) in
                print(error)
                
        }, onCompleted:
        {}) {}.disposed(by: disposeBag)
        
        output.decreseQuantityOutput.asObservable().subscribe( onNext:
            {
                (quantity) in
                
                self.quantitySubject.onNext(quantity)
                
        }, onError:
            {
                (error) in
                print(error)
                
        }, onCompleted:
        {}) {}.disposed(by: disposeBag)
        
    }
    func handelAddToWishList(output : ProductDetailsCellViewModel.Output) {
        output.addToWishListResult.asObservable().subscribe(onNext: { (AddedToCartModel) in
            
            if KeychainSwift().get("user") == "current" {
                Loaf("product added to Wish List successfully".localized(), state: .success(L102Language.isRTL ? .right : .left), location: .top, sender: UIApplication.topViewController() ?? UIViewController()).show()
            
                
               
                let price = self.product.price?.value ?? 0.0
               
                let itemDetails: [String: Any] = [
                    AnalyticsParameterCurrency: self.product.price?.currencyISO ?? "AED",
                    AnalyticsParameterValue:  price,
                    AnalyticsParameterItemID: self.product.code ?? "" ,
                    AnalyticsParameterItemName: self.product.name ?? "",

                ]
                Analytics.logEvent(AnalyticsEventAddToWishlist, parameters: itemDetails)
                self.wishListButton.tintColor = UIColor(named: "AppMainColor")
            }
            
        }).disposed(by: self.disposeBag)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.disposeBag = DisposeBag()
    }
}


extension ProductDetailsTableViewCell : FSPagerViewDelegate , FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return banners.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        guard  let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "ProductDetailsPagerCell", at: index) as? ProductDetailsPagerCell else{ return FSPagerViewCell()}
        
        cell.imageView?.contentMode = .scaleAspectFill
        
        cell.imageView?.kf.setImage(with: URL(string: "\(Environment.shared.apiEndpoint?.rawValue ?? "")\(self.banners[index].url ?? "")"))

        
 
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
         self.toImageZoomSubject.onNext(ProductDetailsImageZoomModel(banners: self.banners, selectedIndex: index))
        productPagerView.deselectItem(at: index, animated: true)
        productPagerView.scrollToItem(at: index, animated: true)
          
        
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.productPagerControl.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.productPagerControl.currentPage = productPagerView.currentIndex
    }
}
