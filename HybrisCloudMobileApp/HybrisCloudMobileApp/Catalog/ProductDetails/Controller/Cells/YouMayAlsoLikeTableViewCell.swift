//
//  YouMayAlsoLikeTableViewCell.swift
//  YErabiaApp
//
//  Created by Ahmad Khwaja on 4/14/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class YouMayAlsoLikeTableViewCell: UITableViewCell {

    @IBOutlet weak var youMayAlsoLikeLbl: UILabel!
    @IBOutlet weak var referencedProductsCollectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    
    var productDetails = PublishSubject<ProductDetails>()
    var toProductDetails = PublishSubject<ProductDetails>()
    let toLoginSubject = PublishSubject<Void>()
    var bag = DisposeBag()
    var viewModel:YouMayAlsoLikeTableViewCellViewModel!
    let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUp() {
        
        self.youMayAlsoLikeLbl.isHidden = true
        referencedProductsCollectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionViewCell")
        
        self.viewModel = YouMayAlsoLikeTableViewCellViewModel(youMayAlsoLikeUseCase: networkUseCaseProvider.makeBestDealProductUseCase())
        
        let cellAwakeFromNib = rx.sentMessage(#selector(UITableViewCell.prepareForReuse))
                            .mapToVoid()
                            .asDriverOnErrorJustComplete()
                        
        
        let input = YouMayAlsoLikeTableViewCellViewModel.Input(trigger: cellAwakeFromNib, productDetails: productDetails.asObserver().asDriverOnErrorJustComplete())
        
         let output = viewModel.transform(input: input)
        viewModel.toLoginSubject.onNext(())

        output.youMayAlsoLikeProducts.asObservable().bind(to: self.referencedProductsCollectionView.rx.items(dataSource: self.viewModel.dataSource())).disposed(by: bag)
        
        output.youMayAlsoLikeProducts.asObservable().subscribe (onNext: { (array) in
            
            (array.first?.items.count ?? 0) > 0 ? (self.youMayAlsoLikeLbl.isHidden = false) : (self.youMayAlsoLikeLbl.isHidden = true)
            self.collectionViewHeightConstraint.constant =   (array.first?.items.count ?? 0) > 0 ? 430 : 0
        }).disposed(by: bag)

        self.referencedProductsCollectionView.rx.modelSelected(BestDealProductSection.Item.self).subscribe(onNext: {(item) in print("model Selected")
            self.toProductDetails.onNext(item)
        }).disposed(by: bag)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.bag = DisposeBag()
    }
    
}
