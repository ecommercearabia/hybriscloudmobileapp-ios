//
//  ProductReviewsViewController.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 9/1/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Foundation
import Domain
import RxSwift
import RxCocoa
import NetworkPlatform
import UIKit
//import ProgressHUD
import FSPagerView

class ProductReviewsViewController: UIViewController {
   
    let bag = DisposeBag()
    var codeBehaviorSubject:BehaviorSubject<String>?

    var productDetailsBehaviorSubject:BehaviorSubject<ProductDetails>?

    @IBOutlet weak var ReviewsTableView: UITableView!

    @IBOutlet weak var noReviewsLabel: UILabel!
    
    var banners: [ProducttImageElement] = []

    @IBOutlet weak var productPagerView: FSPagerView!{
           didSet{
               self.productPagerView.register(UINib(nibName: "ProductDetailsPagerCell", bundle: nil), forCellWithReuseIdentifier: "ProductDetailsPagerCell")
               self.productPagerView.itemSize = FSPagerView.automaticSize
               self.productPagerView.delegate = self
               self.productPagerView.dataSource = self
               self.productPagerView.transformer = FSPagerViewTransformer(type: .linear)
               self.productPagerView.interitemSpacing = 0
//               self.productPagerView.itemSize = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)
               self.productPagerView.isInfinite = true
               self.productPagerView.transformer = NoVerticalScalePagerViewTransformer(type: .linear)
              // self.productPagerView.layer.cornerRadius = self.productPagerView.bounds.width / 2
           }
       }
       
    
    @IBOutlet weak var productPagerControl: FSPageControl!{
        didSet{
            self.productPagerControl.numberOfPages = banners.count
            self.productPagerControl.contentHorizontalAlignment = .center
            self.productPagerControl.contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
                
        self.title = "Reviews".localized()
        
         productPagerView.register(UINib(nibName: "ProductDetailsPagerCell", bundle: nil), forCellWithReuseIdentifier: "ProductDetailsPagerCell")
        
        ReviewsTableView.refreshControl = UIRefreshControl()
        
//        guard let getCode = codeBehaviorSubject else{
//                   return
//                            }
        
        self.noReviewsLabel.isHidden = false
        self.ReviewsTableView.isHidden = true
        
        guard let getProduct = productDetailsBehaviorSubject else{
                         return
                                  }

        getProduct.subscribe(onNext: { (productDetails) in
            self.banners = productDetails.images?.filter({ (ProducttImage) -> Bool in
                ProducttImage.format == "zoom" && ProducttImage.imageType == "GALLERY"
            }) ?? []
            }).disposed(by: bag)
       
            productPagerControl.numberOfPages = banners.count

            productPagerControl.setFillColor(#colorLiteral(red: 0.5215686275, green: 0.5098039216, blue: 0.2, alpha: 1) ,for: .selected)
            productPagerControl.setFillColor(#colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1), for: .normal)

            productPagerControl.contentHorizontalAlignment = .center
            productPagerControl.contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            productPagerView.reloadData()
        
    
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
                  .mapToVoid()
                  .asDriverOnErrorJustComplete()
            
        let pull = ReviewsTableView.refreshControl!.rx
                  .controlEvent(.valueChanged)
                  .asDriver()
        
       
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()

        let viewModel = ProductReviewsViewModel(productReviewsUseCase: networkUseCaseProvider.makeProductReviewsUseCase())
    

        let input = ProductReviewsViewModel.Input(trigger: Driver.merge(viewWillAppear, pull), product: getProduct.asDriverOnErrorJustComplete())

        let output = viewModel.transform(input: input)
            
        output.fetching
                  .drive(ReviewsTableView.refreshControl!.rx.isRefreshing)
                  .disposed(by: bag)
        
        output.error.asObservable().subscribe(onNext: { (error) in
            let msgError = error as? ErrorResponse
                  let msg = msgError?.errors?.map({ (ErrorResponseElement) -> String in
                      return ErrorResponseElement.message ?? ""
                      }).joined(separator: " , ")
                  self.errorLoaf(message: msg ?? "")
            
        }).disposed(by: bag)
        
        output.reviews.map { (reviews) -> [ProductReviewSection] in
            
            return [ProductReviewSection(header: "", items: reviews.reviews ?? [])]
            
        }.asObservable().bind(to: ReviewsTableView.rx.items(dataSource: viewModel.dataSourceProductReviews())).disposed(by: bag)

        output.reviews.asObservable().subscribe(onNext: { (reviews) in
            if((reviews.reviews ?? []).isEmpty)
                 {
                     self.noReviewsLabel.isHidden = false
                     self.ReviewsTableView.isHidden = true
                 }else {
                     self.noReviewsLabel.isHidden = true
                     self.ReviewsTableView.isHidden = false

                 }
                }).disposed(by: bag)

    }
    
    
}
extension ProductReviewsViewController : FSPagerViewDelegate , FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return banners.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        guard  let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "ProductDetailsPagerCell", at: index) as? ProductDetailsPagerCell else{ return FSPagerViewCell()}
        cell.contentView.layer.shadowColor = UIColor.clear.cgColor
        cell.imageView?.contentMode = .scaleAspectFit
        
        cell.imageView?.kf.setImage(with: URL(string: "\(Environment.shared.apiEndpoint?.rawValue ?? "")\(self.banners[index].url ?? "")"))

        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        productPagerView.deselectItem(at: index, animated: true)
        productPagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.productPagerControl.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.productPagerControl.currentPage = productPagerView.currentIndex
    }
}
