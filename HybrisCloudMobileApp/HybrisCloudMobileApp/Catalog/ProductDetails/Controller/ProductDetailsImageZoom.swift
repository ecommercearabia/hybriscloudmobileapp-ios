//
//  ProductDetailsImageZoom.swift
//  HybrisCloudMobileApp
//
//  Created by walaa omar on 9/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

class ProductDetailsImageZoom: UIViewController {

    var getBannersBehaviorSubject : BehaviorSubject<ProductDetailsImageZoomModel>?
    let bag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let banners = getBannersBehaviorSubject else{
             return
                      }
        
        banners.subscribe(onNext: { (producttImage) in
            self.setImage(banners: producttImage.banners,selectedIndex : producttImage.selectedIndex)
            }).disposed(by: bag)
        
        
       
    }
    


    func setImage(banners: [ProducttImageElement],selectedIndex :Int){
         let viewHeight: CGFloat = self.view.bounds.size.height
                let viewWidth: CGFloat = self.view.bounds.size.width
                
                let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight))
                scrollView.isPagingEnabled = true
                
                var xPostion: CGFloat = 0
                
                for banner in banners {
                    let view = UIView(frame: CGRect(x: xPostion, y: 0, width: viewWidth, height: viewHeight))
                    xPostion += viewWidth
                    let imageView = ImageViewZoom(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
                    
                    imageView.setup()
                    imageView.imageContentMode = .aspectFit
                    imageView.initialOffset = .center

                    let url = URL(string: "\(Environment.shared.apiEndpoint?.rawValue ?? "")\(banner.url ?? "" )")
                    
                    DispatchQueue.global().async {
                    if let data = try? Data( contentsOf:url!)
                    {
                      DispatchQueue.main.async
                        {
                       let imageViewForClass = UIImage( data:data)
                        imageView.display(image: imageViewForClass ?? UIImage())
                       
                        }
                        
                    }
                        
                    }
                    
                    view.addSubview(imageView)
                    scrollView.addSubview(view)
                }

         
                let width = ((UIScreen.main.bounds.size.width) * (CGFloat(selectedIndex)))
        

                scrollView.contentSize = CGSize(width: xPostion, height: viewHeight)
        
        scrollView.contentOffset = CGPoint(x:width, y: CGFloat(0))
        
       
        if #available(iOS 11, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
                self.view.addSubview(scrollView)
    }
}

struct ProductDetailsImageZoomModel {
    var banners: [ProducttImageElement]
    var selectedIndex :Int
    init(banners : [ProducttImageElement] , selectedIndex : Int) {
        self.banners = banners
        self.selectedIndex = selectedIndex
    }
}
