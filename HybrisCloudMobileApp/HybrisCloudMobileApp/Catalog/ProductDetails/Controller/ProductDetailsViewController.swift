//
//  ProductDetailsViewController.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Firebase
class ProductDetailsViewController: UIViewController {
    
    @IBOutlet weak var productDetailsTableView: UITableView!
    
    
    
    let toLoginSubject = PublishSubject<Void>()

    let disposeBag = DisposeBag()
    
    var detailsviewModel: ProductDetailsViewModel!
    var viewModel : YouMayAlsoLikeTableViewCellViewModel!

    var productDetailsBehaviorSubject: BehaviorSubject<String>?
    var productDetails: ProductDetails!
    var fromPustNotification = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavBarImage()
        preparation()
        prodctDetailsSetUp()
        
        detailsviewModel.cellState.subscribe(onNext: { [self] (state) in
           
            self.productDetailsTableView.beginUpdates()
            self.productDetailsTableView.endUpdates()
        }).disposed(by: disposeBag)

        if self.fromPustNotification {
            let backButton = UIBarButtonItem(title: "back".localized(), style: .plain, target: self, action: #selector(self.backAction))
            backButton.setTitleTextAttributes([.foregroundColor: UIColor.black], for: .normal)

                self.navigationItem.leftBarButtonItem = backButton
        }
        
    }
    
    @objc func backAction() {

        self.goToHome()

    }
    
    func prodctDetailsSetUp(){
        detailsviewModel.productDetailsSubject.bind(to: productDetailsTableView.rx.items(dataSource: detailsviewModel.dataSourceProductDetails())).disposed(by: disposeBag)
        productDetailsTableView.rx.modelSelected(MultibleSectionModel.Item.self).subscribe(onNext: { (item) in
            print("item selected")
        }).disposed(by: disposeBag)
        productDetailsTableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        self.detailsviewModel.navToExpressDeliveryInfoSubject.subscribe(onNext: {(void)
            in
            self.performSegue(withIdentifier: "toExpressDeliveryInfoVC", sender: nil)
            
        }).disposed(by: disposeBag)
    }
    
    
    
    func preparation(){
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        detailsviewModel = ProductDetailsViewModel.init(productDetailsUseCase: networkUseCaseProvider.makeProductDetailsUseCase())
        _ = productDetailsTableView.refreshControl?.rx
            .controlEvent(.valueChanged)
            .asDriver()
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        guard let subject = productDetailsBehaviorSubject else { return}
        
        let input = ProductDetailsViewModel.Input(trigger: Driver.merge(viewWillAppear), productDetails: subject
            .asObserver()
            .asDriverOnErrorJustComplete())
        
        let output = detailsviewModel.transform(input: input)
        output.productDetailsCom.asObservable().subscribe(onNext: { [weak self] (item) in
            
            var items: [DetailsCellIndexes] = [.product(product: item), .ProductInformation(product: item), .AddReview(product: item), .YouMayAlsoLike(product: item)]
            
            //MARK: classifications is hidden from storyboard
            
//            if let classifications = item.classifications {
//                classifications.forEach { (classificationItem) in
//                    switch classificationItem.code {
//                    case ClassificationsTypes.FoodCrowdClassifications.rawValue:
            if item.nutritionFacts != nil && item.nutritionFacts != "" {
                items.insert(.ProductFacets(product: item), at: 3 )
            }
                      //  items.append(.ProductFacets(product: classificationItem))
//                    default:
//                      break
//                    }
//                }
//            }
//
 
 
            

            
           
            
            self?.detailsviewModel.productDetailsSubject.onNext([MultibleSectionModel(model: "product", items: items)])
           // self?.productDetailsTableView.reloadData()
       
            let productDetails: [String: Any] = [
                AnalyticsParameterCurrency: item.price?.currencyISO ??  "AED",
                AnalyticsParameterValue: item.price?.value ?? 0.0,
                AnalyticsParameterItemID: item.code,
                  AnalyticsParameterItemName:item.name ?? ""
                  
               ]


               // Log view item event
               Analytics.logEvent(AnalyticsEventViewItem, parameters: productDetails)
            
        }).disposed(by: disposeBag)
        
 

        
        
//        self.detailsviewModel.toLoginSubject.subscribe(onNext: { (bool) in
//
//                self.performSegue(withIdentifier: "toLogin", sender: nil)
//
//
//            }).disposed(by: disposeBag)
//
      
        self.detailsviewModel.toLoginSubject.subscribe(onNext: { (bool) in
        
             self.performSegue(withIdentifier: "toLogin", sender: nil)
          
         
         }).disposed(by: disposeBag)
        
        
        
        self.detailsviewModel.toProductDetails.subscribe(onNext: { (product) in
            self.performSegue(withIdentifier: "toProductDetails", sender: product)
        }).disposed(by: disposeBag)
        
        self.detailsviewModel.toReviewsSubject.subscribe(onNext: { (productDetails) in
            self.performSegue(withIdentifier: "goToProductReviews", sender: productDetails)

            }).disposed(by: disposeBag)
        
        self.detailsviewModel.toAddReviewSubject.subscribe(onNext: { (code) in
                   self.performSegue(withIdentifier: "goToProductAddReview", sender: code)

                   }).disposed(by: disposeBag)
 
        self.detailsviewModel.toImageZoomSubject.subscribe(onNext: { (image) in
                          self.performSegue(withIdentifier: "goToProductImageZoom", sender: image)

                          }).disposed(by: disposeBag)
        
    }
    
    
    func addNavBarImage () {
         let navController = navigationController!
         let logoImage = #imageLiteral(resourceName: "logo")
         let logoImageView = UIImageView()
         
         let bannerWidth = navController.navigationBar.frame.size.width / 2
         let bannerHeight = navController.navigationBar.frame.size.height / 2
         let bannerX = bannerWidth / 2 - logoImage.size.width / 2
         let bannerY = bannerHeight / 2 - logoImage.size.height / 2
         
         logoImageView.frame = CGRect(x:bannerX , y: bannerY, width: bannerWidth, height: bannerHeight)
         logoImageView.contentMode = .scaleAspectFit
         logoImageView.image = logoImage
         navigationItem.titleView = logoImageView
         
     }
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
               if segue.identifier == "goToProductReviews"{
                   if let productReviewsViewController = segue.destination as? ProductReviewsViewController {
                    guard let productDetails =  sender as? ProductDetails else {
                        return
                    }
                    productReviewsViewController.productDetailsBehaviorSubject = BehaviorSubject<ProductDetails>(value: productDetails)
                
                   }
               } else if segue.identifier == "goToProductAddReview" {
                if let productAddReviewViewController = segue.destination as? ProductAddReviewViewController {
                 guard let code =  sender as? String else {
                     return
                 }
                 productAddReviewViewController.codeBehaviorSubject = BehaviorSubject<String>(value: code)
                }
                
               } else if segue.identifier == "goToProductImageZoom" {
                if let productDetailsImageZoom = segue.destination as? ProductDetailsImageZoom {
                    guard let productImage =  sender as? ProductDetailsImageZoomModel else {
                                        return
                                    }
                 
                    productDetailsImageZoom.getBannersBehaviorSubject = BehaviorSubject<ProductDetailsImageZoomModel>(value: productImage)
                                   }
                    
               } else if segue.identifier == "toProductDetails"{
                if let vc = segue.destination as? ProductDetailsViewController{
                    guard let product = sender as? ProductDetails else {
                        return
                    }
                    vc.productDetailsBehaviorSubject = BehaviorSubject<String>(value: product.code ?? "")
                }
            }
                
                
            }
}
        

extension ProductDetailsViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        guard let model = try? tableView.rx.model(at: indexPath) as DetailsCellIndexes else { return 0.0 }
//
//
//        switch model {
//        case .product:
//            return 770
//        case .ProductFacets:
//            return 0
//        case .ProductInformation :
//            return 260
//        case .YouMayAlsoLike:
//            return 472
//        default:
//            return UITableView.automaticDimension
//      }
//
//    }
  

}
