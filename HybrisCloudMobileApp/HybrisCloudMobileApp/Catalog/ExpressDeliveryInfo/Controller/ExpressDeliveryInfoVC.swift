//
//  ExpressDeliveryInfoVC.swift
//  HybrisCloudMobileApp
//
//  Created by khalil anqawi on 23/07/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import UIKit

class ExpressDeliveryInfoVC: UIViewController {

    @IBOutlet weak var expressDeliveryScrollView: UIScrollView!
    @IBOutlet weak var expressDeliveryIamge: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavBarImage()
       expressDeliveryScrollView.delegate = self
        updateZoomFor(size: expressDeliveryScrollView.bounds.size)
        
        if L102Language.isRTL {
            expressDeliveryIamge.image = UIImage(named: "EXPRESSInfo-AR" )
        }  else {
            expressDeliveryIamge.image = UIImage(named: "EXPRESSInfo")
        }
    
    }
    func addNavBarImage () {
         let navController = navigationController!
         let logoImage = #imageLiteral(resourceName: "logo")
         let logoImageView = UIImageView()
         
         let bannerWidth = navController.navigationBar.frame.size.width / 2
         let bannerHeight = navController.navigationBar.frame.size.height / 2
         let bannerX = bannerWidth / 2 - logoImage.size.width / 2
         let bannerY = bannerHeight / 2 - logoImage.size.height / 2
         
         logoImageView.frame = CGRect(x:bannerX , y: bannerY, width: bannerWidth, height: bannerHeight)
         logoImageView.contentMode = .scaleAspectFit
         logoImageView.image = logoImage
         navigationItem.titleView = logoImageView
         
     }
    
    func updateZoomFor(size: CGSize){
        let widthScale = size.width / expressDeliveryIamge.bounds.width
        let heightScale = size.height / expressDeliveryIamge.bounds.height
        let scale = min(widthScale,heightScale)
        expressDeliveryScrollView.minimumZoomScale = scale
    }
}


extension ExpressDeliveryInfoVC : UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.expressDeliveryIamge
    }
}
