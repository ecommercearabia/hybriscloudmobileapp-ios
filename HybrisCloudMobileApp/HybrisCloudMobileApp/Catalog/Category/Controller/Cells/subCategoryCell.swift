//
//  subCategoryCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/3/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class subCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var borderView: UILabel!
    var disposBag = DisposeBag()

    
    override  func awakeFromNib() {
        super.awakeFromNib()
        borderView.backgroundColor = .clear
        borderView.layer.cornerRadius = 10
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = #colorLiteral(red: 0.8588235294, green: 0.8588235294, blue: 0.8588235294, alpha: 1)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposBag = DisposeBag()
    }
    
    func setUp(category : CategoryElement) {
        
        self.name.text = category.name ?? ""
        self.img.downloadImageInCellWithoutCorner(urlString: category.category?.image?.url ?? "")
    }

}



 
