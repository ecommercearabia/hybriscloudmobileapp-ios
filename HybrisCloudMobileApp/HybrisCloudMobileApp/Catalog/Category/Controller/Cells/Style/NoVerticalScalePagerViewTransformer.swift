//
//  NoVerticalScalePagerViewTransformer.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/2/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import FSPagerView
import UIKit

class NoVerticalScalePagerViewTransformer: FSPagerViewTransformer {
    
    override func applyTransform(to attributes: FSPagerViewLayoutAttributes) {
        guard let pagerView = self.pagerView else {
            return
        }
        let position = attributes.position
        let scrollDirection = pagerView.scrollDirection
        let itemSpacing = (scrollDirection == .horizontal ? attributes.bounds.width : attributes.bounds.height) + self.proposedInteritemSpacing()
        if self.type == .linear {
            guard scrollDirection == .horizontal else {
                // This type doesn't support vertical mode
                return
            }
            let scale = max(1 - (1-self.minimumScale) * abs(position), self.minimumScale)
//             let transform = CGAffineTransform(scaleX: scale, y: scale)
            // **Modify here**
            
            
            let transform = CGAffineTransform(scaleX: scale, y: scale)
            attributes.transform = transform
            let alpha = (0.8 + (1-abs(position))*(1 - 0.8))
            attributes.alpha = alpha
            let zIndex = (1-abs(position)) * 10
            attributes.zIndex = Int(zIndex)
        } else {
            super.applyTransform(to: attributes)
        }
    }
    
}
