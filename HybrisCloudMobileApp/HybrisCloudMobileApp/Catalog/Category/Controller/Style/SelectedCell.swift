//
//  SelectedCell.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/3/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

class SelectedCell: UIView {
    override func awakeFromNib() {
        layer.cornerRadius = 7.5
        layer.masksToBounds = true
        layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.5843137255, blue: 0.2745098039, alpha: 1)
        layer.borderWidth = 2.5
        
    }
}
