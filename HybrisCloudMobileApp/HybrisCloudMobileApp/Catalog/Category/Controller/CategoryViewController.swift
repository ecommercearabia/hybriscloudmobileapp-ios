//
//  CategoryViewController.swift
//  HybrisCloudMobileApp
//
//  Created by Ahmad Bader on 8/2/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import FSPagerView
import Domain
import Kingfisher

class CategoryViewController: UIViewController {
    
    @IBOutlet weak var subCategory: UICollectionView!
    @IBOutlet weak var selectedMainCatLbl: UILabel!
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            
            self.pagerView.register(UINib(nibName: "CategoriesTypeCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesTypeCell")
            
            self.pagerView.itemSize = FSPagerView.automaticSize
            self.pagerView.delegate = self
            self.pagerView.dataSource = self
            self.pagerView.transformer = FSPagerViewTransformer(type: .linear)
            self.pagerView.interitemSpacing = 40
            
            pagerView.itemSize = CGSize(width: 65 , height: 65)
            pagerView.isInfinite = true
            pagerView.transformer = NoVerticalScalePagerViewTransformer(type: .linear)
        }
    }
    
    
    let disposeBag = DisposeBag()
    var categoryviewModel : CategoryViewModel!
    
    var categorys : [Subcategory]?
    var categoryId : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavBarImage ()
        preparation()
        subCategorySetUp()
        
//        let backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
//        backBarButtonItem.tintColor = #colorLiteral(red: 0.3843137255, green: 0.431372549, blue: 0.4156862745, alpha: 1)
//
//
     
    }
    
    func subCategorySetUp(){
        
        self.categoryviewModel.catalogSubject.bind(to: subCategory.rx.items(dataSource: self.categoryviewModel.dataSourceCategory())).disposed(by: disposeBag)
        self.subCategory.rx.setDelegate(self).disposed(by: disposeBag)
        
        self.subCategory.rx.modelSelected(CategorySection.Item.self).subscribe(onNext: { (item) in
            print("model Selected")
            
            self.performSegue(withIdentifier: "toProduct", sender: item.id ?? "")
            
            
        }).disposed(by: disposeBag)
    }
    
    func preparation(){
        
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        categoryviewModel = CategoryViewModel.init(getCategoryUseCase: networkUseCaseProvider.makeCatalogUseCase())
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = CategoryViewModel.Input(trigger: Driver.merge(viewWillAppear), categoryTrigger: Driver.merge(viewWillAppear))
        let output = categoryviewModel.transform(input: input)
        
        output.category.asObservable().subscribe(onNext: { (Catalog) in
            self.categorys = Catalog.categories?.filter({ (Category) -> Bool in
                return Category.id == "1"
            }).first?.subcategories
            self.selectedMainCatLbl.text = self.categorys?.first?.name ?? ""
            self.categoryviewModel.catalogSubject.onNext([CategorySection(header: "", items: self.categorys?.first?.subcategories ?? [])])
            self.pagerView.reloadData()
        }).disposed(by: disposeBag)
         
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toProduct"{
            if let vc = segue.destination as? ProductListingViewController {
                vc.categoryId = (sender as? String) ?? ""
            }
        }
    }
    
    func addNavBarImage () {
         let navController = navigationController!
         let logoImage = #imageLiteral(resourceName: "logo")
         let logoImageView = UIImageView()
         
         let bannerWidth = navController.navigationBar.frame.size.width / 2
         let bannerHeight = navController.navigationBar.frame.size.height / 2
         let bannerX = bannerWidth / 2 - logoImage.size.width / 2
         let bannerY = bannerHeight / 2 - logoImage.size.height / 2
         
         logoImageView.frame = CGRect(x:bannerX , y: bannerY, width: bannerWidth, height: bannerHeight)
         logoImageView.contentMode = .scaleAspectFit
         logoImageView.image = logoImage
         navigationItem.titleView = logoImageView
         
     }
}


extension CategoryViewController : FSPagerViewDelegate , FSPagerViewDataSource , UICollectionViewDelegateFlowLayout{
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.categorys?.count ?? 0
    }
    
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        guard let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "CategoriesTypeCell", at: index) as? CategoriesTypeCell else{ return FSPagerViewCell()}
        cell.contentView.layer.shadowColor = UIColor.clear.cgColor
        
        cell.img?.kf.setImage(with: URL(string: "\(Environment.shared.apiEndpoint?.rawValue ?? "")\(self.categorys?[index].category?.image?.url ?? "")"))

        
        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        self.pagerView.scrollToItem(at: index, animated: true)
       
        UIView.transition(with: self.selectedMainCatLbl,
                          duration: 0.25,
                          options: .transitionCrossDissolve,
                          animations: { [weak self] in
                            self?.selectedMainCatLbl.text = self?.categorys?[index].name ?? ""
            }, completion: nil)
        //
        self.categoryviewModel.catalogSubject.onNext([CategorySection(header: "", items: self.categorys?[index].subcategories ?? [])])
        
        
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        
        self.categoryviewModel.catalogSubject.onNext([CategorySection(header: "", items: self.categorys?[targetIndex].subcategories ?? [])])
        
        UIView.transition(with: self.selectedMainCatLbl,
                          duration: 0.25,
                          options: .transitionCrossDissolve,
                          animations: { [weak self] in
                            self?.selectedMainCatLbl.text = self?.categorys?[targetIndex].name ?? ""
            }, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.width) / 3.2 , height: (collectionView.frame.width) / 3.2)
    }
    
    
    
}



