//
//  CategoryViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/3/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Domain
import NetworkPlatform

class CategoryViewModel{
    
    var catalogSubject = BehaviorSubject<[CategorySection]>(value: [CategorySection(header: "", items: [])])

    
    struct Input {
        let trigger: Driver<Void>
        let categoryTrigger: Driver<Void>

    }
    
    struct Output {
        let fetching: Driver<Bool>
        let category: Driver<Catalog>
        let error: Driver<Error>
    }

    let bag = DisposeBag()
    let networkUseCaseProvider : Domain.UseCaseProvider = NetworkPlatform.UseCaseProvider()
    private let getCategoryUseCase: CatalogUseCase

    
    init(getCategoryUseCase: CatalogUseCase) {
        self.getCategoryUseCase = getCategoryUseCase
      
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        let category = input.categoryTrigger.flatMapLatest { [unowned self] in
            return self.getCategoryUseCase.categories(id: "foodcrowdProductCatalog", version: "Online").trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
                
            }

        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching, category: category,
                      error: errors)
        
    }
    
    
       func dataSourceCategory() ->
        RxCollectionViewSectionedAnimatedDataSource<CategorySection> {
           return RxCollectionViewSectionedAnimatedDataSource<CategorySection>(animationConfiguration: AnimationConfiguration(insertAnimation: .left, reloadAnimation: .left, deleteAnimation: .bottom),configureCell: { ds, cv, indexPath, item in
 
                    guard let cell = cv.dequeueReusableCell(withReuseIdentifier: "subCategoryCell", for: indexPath) as? subCategoryCell else {
                       return UICollectionViewCell()
                   }
                   cell.setUp(category: item)
    
                    return cell
 
           })
       }
}
