//
//  CategorySiction.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/3/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxDataSources

struct CategorySection {
    var header: String
    var items: [Item]
    
}

extension CategorySection : AnimatableSectionModelType, Equatable {
    typealias Item = CategoryElement

    var identity: String {
        return header
    }

    init(original: CategorySection, items: [CategoryElement]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: CategorySection, rhs: CategorySection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension CategoryElement: IdentifiableType, Equatable {
    public var identity: String {
        return  id ?? ""
    }
    
    public static func == (lhs: CategoryElement, rhs: CategoryElement) -> Bool { return lhs.id  == rhs.id }
}

