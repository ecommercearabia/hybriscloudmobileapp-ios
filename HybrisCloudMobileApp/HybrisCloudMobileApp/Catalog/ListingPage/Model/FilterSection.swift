//
//  FilterSection.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/24/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

 import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxDataSources

struct FilterSection {
    var header: String
    var items: [Item]
    
}

extension FilterSection : AnimatableSectionModelType, Equatable {
    typealias Item = searchValue
    
    var identity: String {
        return header
    }
    
    init(original: FilterSection, items: [searchValue]) {
        self = original
        self.items = items
    }
    
    static func ==(lhs: FilterSection, rhs: FilterSection) -> Bool {
        return lhs.header == rhs.header
    }
}


extension searchValue: IdentifiableType , Equatable {
     public var identity : String {
        return "\(query?.query?.value ?? "") \(name ?? "")"
     }

    
    public static func == (lhs: searchValue, rhs: searchValue) -> Bool {
        return lhs.name == rhs.name && lhs.selected == rhs.selected
        
    }
}

 
 
