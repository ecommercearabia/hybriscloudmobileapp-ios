//
//  ListingFitersViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 8/24/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import Domain
import NetworkPlatform
import RxSwift
import RxDataSources

class ListingFiltersViewModel {
    
    private let filterUseCase: ProductUseCase
    
    init(filterUseCase: ProductUseCase) {
        self.filterUseCase = filterUseCase
    }
    
    //MARK: Input Struct
    struct Input {
        let categoryIdTrigger: Driver<String>
        let relevanceTrigger: Driver<String>
    }
    
    //MARK: Output Struct
    struct Output {
        let fetching: Driver<Bool>
        let filterResult: Driver<ProductSearch>
        let error: Driver<Error>
    }
        
    
    //MARK: Transform Function
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        

        
        let filterResult = Driver.combineLatest(input.categoryIdTrigger, input.relevanceTrigger).flatMapLatest { (categoryId, relevance) -> Driver<ProductSearch> in

            var relevanceAfterUpdate = String()
            if relevance != "" {
                  relevanceAfterUpdate = ":relevance" + relevance
            }
            else {
                relevanceAfterUpdate = ":relevance"
            }
            return self.filterUseCase.search(categoryID: categoryId, currentPage: 5, pageSize: 5, query: "\(relevanceAfterUpdate)" , sort: "")
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching,
                      filterResult: filterResult,
                      error: errors)
    }
    
    //MARK: Filter Data Source Function
    func filterdataSource() -> RxTableViewSectionedAnimatedDataSource<FilterSection> {
        return
            RxTableViewSectionedAnimatedDataSource<FilterSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .fade, reloadAnimation: .fade, deleteAnimation: .fade),configureCell: { ds, cv, indexPath, item in
                
                guard let cell = cv.dequeueReusableCell(withIdentifier: "ListingTableViewCell", for: indexPath) as? ListingTableViewCell else {
                    return UITableViewCell()
                }
                
                cell.setUp(item: item)
                
                return cell
            }, titleForHeaderInSection: { dataSource, index in
                let sectionModel = dataSource.sectionModels[index]
                return "\(sectionModel.header)"
            })}
}
