//
//  VCpopUp.swift
//  HybrisCloudMobileApp
//
//  Created by ahmadSaleh on 10/22/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import Lottie
class VCpopUp : UIViewController {
    
    @IBOutlet weak var backGroundBut: UIButton!
 
    @IBOutlet weak var lotteView: AnimationView!
    let closePopUp = PublishSubject<Void>()
    
    let bag = DisposeBag()

 
    override func viewDidLoad() {
        backGroundBut.rx.tap.subscribe(onNext: { (void) in
            self.dismiss(animated: true) {
                self.closePopUp.onNext(())
            }
        }).disposed(by: bag)
 
        lotteView.contentMode = .scaleAspectFit
        lotteView.loopMode = .loop
        lotteView.animationSpeed = 2
        lotteView.play()

    }
}
