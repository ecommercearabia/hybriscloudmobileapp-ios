//
//  ListingFitersTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/4/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class ListingFitersTableViewCell: UITableViewCell {
    
 
    @IBOutlet weak var fiterTitelLbl: UILabel!
    
    @IBOutlet weak var fiterNameLbl: UILabel!
    
    @IBOutlet weak var checkBttn: UIButton!
    
    @IBOutlet weak var dropDwonBttn: UIButton!
    
    @IBAction func checkBttnAction(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveLinear, animations: {
                   sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                   
               }) { (success) in
                   UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveLinear, animations: {
                       sender.isSelected = !sender.isSelected
                       sender.transform = .identity
                   }, completion: nil)
               }
    }
    
    @IBAction func dropDwonBttnAction(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveLinear, animations: {
                          sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                          
                      }) { (success) in
                          UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveLinear, animations: {
                              sender.isSelected = !sender.isSelected
                              sender.transform = .identity
                          }, completion: nil)
                      }
        
    }
    
  
    override func awakeFromNib() {
        super.awakeFromNib()
        
        checkBttn.setImage(UIImage(named: "checkmark.rectangle.fill"), for: .normal)
       checkBttn.setImage(UIImage(named:"checkmarkrectanglefill"), for: .selected)

//        dropDwonBttn.setImage(UIImage(named:"chevronDown"), for: .normal)
//        dropDwonBttn.setImage(UIImage(named:"chevronUp"), for: .selected)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
