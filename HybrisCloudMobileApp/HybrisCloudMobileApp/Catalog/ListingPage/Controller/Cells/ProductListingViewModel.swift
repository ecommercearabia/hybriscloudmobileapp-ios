//
//  ProductListingViewModel.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//
import Foundation
import Domain
import NetworkPlatform
import KeychainSwift


class ProductListingViewModel {
    
    var productSubject = BehaviorSubject<[CategorySection]>(value: [CategorySection(header: "", items: [])])
    let toLoginSubject = PublishSubject<Void>()

     var relevancesFacets = BehaviorSubject<[searchFacet]>(value: [])
    struct Input {
        
        let selectCategoryTrigger : Driver<String>
        let trigger : Driver<Void>
        let gerCatigoryTrigger : Driver<Void>
        let switchTrigger : Driver<Bool>
        let veganTrigger : Driver<Bool>
        let glutenTrigger : Driver<Bool>
        let filterTrigger : Driver<String>
        let pageNumber : Driver<Int>
        let sort : Driver<searchSort>

 
    }
    struct Output {
        let fetching: Driver<Bool>
        let category: Driver<Catalog>
        let product: Driver<ProductSearch>
        let error: Driver<Error>
    }
    
    let bag = DisposeBag()
    let networkUseCaseProvider : Domain.UseCaseProvider = NetworkPlatform.UseCaseProvider()
    private let getCategoryUseCase: CatalogUseCase
    private let useCase: ProductUseCase
    init(useCase : ProductUseCase, getCategoryUseCase: CatalogUseCase) {
        self.useCase = useCase
        self.getCategoryUseCase = getCategoryUseCase
        
    }
    
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let category = input.gerCatigoryTrigger.flatMapLatest { [unowned self] in
            return self.getCategoryUseCase.categories(id: "foodcrowdProductCatalog", version: "Online").trackActivity(activityIndicator)
                .asDriverOnErrorJustComplete()
        }
 
 
        
        let product = Driver.combineLatest(input.selectCategoryTrigger,input.switchTrigger ,input.filterTrigger,input.sort , input.pageNumber , input.veganTrigger ,input.glutenTrigger).flatMapLatest {(categoryId ,isOrganic,relevance, sort, pageNumber , isVegan , isGluten) -> Driver<ProductSearch> in
            
            let organic = isOrganic ? ":Organic:true" : ""
            let vegan = isVegan ? ":Vegan:true" : ""
            let gluten = isGluten ? ":GlutenFree:true" : ""

            return self.useCase.search(categoryID: categoryId ?? "", currentPage: pageNumber, pageSize: 20, query: ":relevance\(relevance)\(organic)\(vegan)\(gluten)" , sort: sort.code ?? "" ).trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
              }

 
 
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching,
                      category: category,
                      product: product,
                      error: errors)
    }
    
    
    
    
    func dataSource() -> RxCollectionViewSectionedAnimatedDataSource<BestDealProductSection> {
        return
            RxCollectionViewSectionedAnimatedDataSource<BestDealProductSection>(animationConfiguration: AnimationConfiguration(insertAnimation: .automatic, reloadAnimation: .automatic, deleteAnimation: .automatic),configureCell: { ds, cv, indexPath, item in
                
                
                
                guard let cell = cv.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as? ProductCollectionViewCell else {
                    return UICollectionViewCell()
                }
                cell.setUp(prodcutDeatis: item)

                cell.wishListButton.rx.tap.subscribe(onNext: { (void) in
                        if KeychainSwift().get("user") == "anonymous" {
                             
                            self.toLoginSubject.onNext(())
                    }
                }).disposed(by: self.bag)
           

                return cell
                
                
                
                
            })
    }
    
 
    
}
