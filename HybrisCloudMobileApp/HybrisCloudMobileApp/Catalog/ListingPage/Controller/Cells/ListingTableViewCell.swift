//
//  ListingTableViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit

class ListingTableViewCell: UITableViewCell {
      @IBOutlet weak var expandLbl: UILabel!
   
    @IBOutlet weak var checkBoxImg: UIImageView!
 var setSelectedSubject = PublishSubject<Bool>()
    var bag = DisposeBag()
 var itemB : searchValue?
     override func awakeFromNib() {
 super.awakeFromNib()
 //self.checkBoxImg.image = UIImage(named: "unchecked_checkbox")
        }
    
    func setUp(item : searchValue) {
        self.expandLbl.text = item.name
//        isSelected = item.selected ?? false

        if item.selected ?? false {
            self.checkBoxImg.setImage(UIImage(named: "hecked_checkbox") ?? UIImage())
             self.checkBoxImg.tintColor = #colorLiteral(red: 0.9529411765, green: 0.5137254902, blue: 0.2392156863, alpha: 1)
        }
        else {
        self.checkBoxImg.image = UIImage(named: "unchecked_checkbox")
            self.checkBoxImg.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }

        
//        self.setSelectedSubject.subscribe(onNext: {[unowned self] (selected) in
//           isSelected = selected
//           // self.setSelected(selected, animated: true)
//        }).disposed(by: bag)
        
    }
    
    
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        if selected {
//             self.checkBoxImg.image = UIImage(named: "hecked_checkbox")
//            self.checkBoxImg.tintColor = #colorLiteral(red: 0.9529411765, green: 0.5137254902, blue: 0.2392156863, alpha: 1)
//        }
//        else {
//        self.checkBoxImg.image = UIImage(named: "unchecked_checkbox")
//            self.checkBoxImg.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        }
//     //   self.setSelectedSubject.onNext(selected)
//
//
//     }
    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }
}
            
   


     
   


