//
//  ProdcutListingCollectionViewCell.swift
//  HybrisCloudMobileApp
//
//  Created by khalil on 8/7/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import  Foundation
import KeychainSwift
import FirebaseAnalytics

class ProdcutListingCollectionViewCell: UICollectionViewCell {
    var productQuantity : Int = 1
    var maxProductQuantity : Int?
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var itemDetailsLbl: UILabel!
    @IBOutlet weak var itemCurrencyLbl: UILabel!
    @IBOutlet weak var itemPriceLbl: UILabel!
    @IBOutlet weak var itemOldPriceLbl: UILabel!
    @IBOutlet weak var itemQuantityUITextField: UITextField!
    @IBOutlet weak var flagImage: UIImageView!
    @IBOutlet weak var cartBttn: UIButton!
    @IBOutlet weak var increaseQuantityBttn: UIButton!
    @IBOutlet weak var decreaseQuantityBttn: UIButton!
    @IBOutlet weak var outOfStockLbl: UILabel!
    @IBOutlet weak var wishListButton: UIButton!

    
    var quantitySubject = PublishSubject<String>()
    
    let product = PublishSubject<ProductDetails>()
    var productForLog : ProductDetails!
    let disposeBag = DisposeBag()
    
    
    var viewModel : ProductCollectionViewModel!
    
    
    
    @IBAction func favoriteBttn(_ sender: Any) {
    }
    
    @IBOutlet weak var stepperView: UIView!
    
    
    
    static let identifier = "ProdcutListingCollectionViewCell"
    static func nib()-> UINib {
        return UINib(nibName: "ProdcutListingCollectionViewCell", bundle: nil)
    }
    
    
    override func awakeFromNib() {
        stepperView.layer.borderWidth = 1
        stepperView.layer.borderColor = #colorLiteral(red: 0.8588235294, green: 0.8588235294, blue: 0.8588235294, alpha: 1)
        super.awakeFromNib()
        
        itemOldPriceLbl.attributedText = "PreviousPrice".strikeThrough()
        
        let networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
        
        viewModel = ProductCollectionViewModel(useCase: networkUseCaseProvider.makeWishListUseCase(), addtoCartUseCase: networkUseCaseProvider.makeAddToCartUseCase(), cartUseCase : networkUseCaseProvider.makeCartUseCase())
        
        let input = ProductCollectionViewModel.Input(addtoCartTrigger: cartBttn.rx.tap.asDriver()
            , products: product.asObserver().asDriverOnErrorJustComplete(),
              quantity: quantitySubject.asObserver().asDriverOnErrorJustComplete(),
              increaseQuantityTrigger: increaseQuantityBttn.rx.tap.asDriver(),
              decreaseQuantityTrigger: decreaseQuantityBttn.rx.tap.asDriver(), createCartTrigger: Driver.empty(), addToWishListTrigger: wishListButton.rx.tap.asDriver())
        
        
        
        
        let output = viewModel.transform(input: input)
        output.addToWishListResult.asObservable().subscribe(onNext: { (AddedToCartModel) in
        
                   if KeychainSwift().get("user") == "current" {
                          Loaf("product added to Wish List successfully".localized(), state: .success(L102Language.isRTL ? .right : .left), sender: UIApplication.topViewController() ?? UIViewController()).show()
                   }
            
            let price = self.productForLog.price?.value ?? 0.0
           
            let itemDetails: [String: Any] = [
                AnalyticsParameterCurrency: self.productForLog.price?.currencyISO ?? "AED",
                AnalyticsParameterValue:  price,
                AnalyticsParameterItemID: self.productForLog.code ?? "" ,
                AnalyticsParameterItemName: self.productForLog.name ?? "",

            ]
            Analytics.logEvent(AnalyticsEventAddToWishlist, parameters: itemDetails)


               }).disposed(by: self.disposeBag)
        output.addToCartResult.asObservable().subscribe(
            onNext:
            {
                (addtoCartModel) in
                print(addtoCartModel)
                
                let quantity = (Double(addtoCartModel.quantity ?? 0) )
                let price = addtoCartModel.entry?.basePrice?.value ?? 0.0
                let total = quantity * price
                let itemDetails: [String: Any] = [
                    AnalyticsParameterCurrency: addtoCartModel.entry?.basePrice?.currencyISO ?? "AED",
                    AnalyticsParameterValue:  total,
                    AnalyticsParameterItemID: addtoCartModel.entry?.product?.code ?? "" ,
                    AnalyticsParameterItemName: addtoCartModel.entry?.product?.name ?? "",
                    AnalyticsParameterPrice: price
                ]
                
                Analytics.logEvent(AnalyticsEventAddToCart, parameters: itemDetails)
               
                
        }, onError:
            {
                (error) in
                print(error)
                
        }, onCompleted:
        {}) {}.disposed(by: disposeBag)
        
        output.increaseQuantityOutput.asObservable().subscribe( onNext:
            {
                (quantity) in
                self.quantitySubject.onNext(quantity)
                
        }, onError:
            {
                (error) in
                print(error)
                
        }, onCompleted:
        {}) {}.disposed(by: disposeBag)
        
        output.decreseQuantityOutput.asObservable().subscribe( onNext:
            {
                (quantity) in
                
                self.quantitySubject.onNext(quantity)
                
        }, onError:
            {
                (error) in
                print(error)
                
        }, onCompleted:
        {}) {}.disposed(by: disposeBag)
        
        
        quantitySubject.bind(to: itemQuantityUITextField.rx.text).disposed(by: disposeBag)
        
        
    }
    func setUp(prodcutDeatis : ProductDetails) {
        
        self.itemImage.cornerRadius = self.itemImage.frame.height / 2
        
        product.onNext(prodcutDeatis)
        productForLog = prodcutDeatis
        quantitySubject.onNext("1")
        
        self.itemOldPriceLbl.isHidden = true
        self.maxProductQuantity = prodcutDeatis.stock?.stockLevel ?? 0
        
        
        
        let productImagee =  prodcutDeatis.images.map({ (page) -> [ProducttImageElement] in
            return page.filter ({ (image) -> Bool in
                image.format == "product"
                
            })
        })
        
        self.itemImage?.kf.setImage(with: URL(string: "\(Environment.shared.apiEndpoint?.rawValue ?? "")" + (productImagee?.first?.url ?? "")))
        
        self.itemNameLbl.text =
            prodcutDeatis.images?.first?.altText ?? ""
        
        
        prodcutDeatis.classifications?.first?.features?.forEach{ (prod) in
            if prod.name == "Country of origin"{
                self.itemCurrencyLbl.text  =  (prod.featureValues?.first?.value ?? "") + " /"
            }
            
        }
        self.itemDetailsLbl.text =  prodcutDeatis.baseOptions?.first?.selected?.variantOptionQualifiers?.first?.value ?? ""
        
        self.itemPriceLbl.text = prodcutDeatis.price?.formattedValue ?? ""
        if prodcutDeatis.stock?.stockLevel ?? 0 <= 0{
            outOfStockLbl.isHidden = false
            cartBttn.isUserInteractionEnabled = false
        }
        else {
            outOfStockLbl.isHidden = true
            cartBttn.isUserInteractionEnabled = true

        }
    }
}
