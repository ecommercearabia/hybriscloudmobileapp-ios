//
//  CommunityViewController.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import UIKit
import WebKit


class CommunityViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        var urlString = String()
        self.isArabic() ? (urlString = "https://blog.foodcrowd.com/?lang=ar") : (urlString = "https://blog.foodcrowd.com/?lang=en")
        
        webView.load(NSURLRequest(url: NSURL(string: urlString)! as URL) as URLRequest)

    }
    
}
