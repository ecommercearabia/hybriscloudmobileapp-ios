//
//  PostsUseCase.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 7/15/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol PostsUseCase {
    func posts() -> Observable<[Post]>
    func save(post: Post) -> Observable<Void>
    func delete(post: Post) -> Observable<Void>
}
