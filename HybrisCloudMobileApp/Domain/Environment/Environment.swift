//
//  Environment.swift
//  Domain
//
//  Created by ahmadSaleh on 9/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import KeychainSwift

public class Environment : NSObject {
    
     public static let shared = Environment()
     public var apiEndpoint : EnvironmentType?
 
    
    override init() {
        super.init()
        self.chickIfEnvironmentChanged()
        #if PROD
        apiEndpoint = .prod
        KeychainSwift().set("https://api.foodcrowd.com", forKey: "lastEnvironment")
        #else
        apiEndpoint = .stg
        KeychainSwift().set("https://api-stg.foodcrowd.com", forKey: "lastEnvironment")
        #endif
      }
    
    private func chickIfEnvironmentChanged() {
        var lastEnvironment : EnvironmentType?
        var newEnvironment : EnvironmentType?

        #if PROD
        lastEnvironment = ((KeychainSwift().get("lastEnvironment")).map { EnvironmentType(rawValue: $0) } as? EnvironmentType) ?? EnvironmentType.prod
        newEnvironment = .prod
        #else
        lastEnvironment = ((KeychainSwift().get("lastEnvironment")).map { EnvironmentType(rawValue: $0) } as? EnvironmentType) ?? EnvironmentType.stg
        newEnvironment = .stg

        #endif
        
        if lastEnvironment != newEnvironment {
                   removeCredential()
            }
        
    }
    
    private func removeCredential() {
        print("removeCredential")
        KeychainSwift().delete("usercredentials")
        KeychainSwift().delete("appcredentials")
        KeychainSwift().set("anonymous", forKey: "user")

    }
    
}

public enum EnvironmentType : String{
    case stg = "https://api-stg.foodcrowd.com"
    case prod = "https://api.foodcrowd.com"
}
