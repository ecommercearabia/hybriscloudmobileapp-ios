//
//  SavedCardsModel.swift
//  Domain
//
//  Created by walaa omar on 26/12/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation

// MARK: - SavedCardsModel
public struct SavedCardsModel: Codable , Identifiable
{
    public var identity: String{
        return "1"
    }
    public let customerPaymentOptions: [CustomerPaymentOptions]?

    public init(customerPaymentOptions: [CustomerPaymentOptions]?) {
        self.customerPaymentOptions = customerPaymentOptions
    }
    
}

// MARK: - CustomerPaymentOptions
public struct CustomerPaymentOptions: Codable , Identifiable
{
    public var identity: String{
        return customerCardId ?? ""
    }
    public var customerCardId, customerCardLabel, customerCardName, customerCardNo , customerCardType , customerEmail , customerPayoptType: String?

    public init(customerCardId: String?, customerCardLabel: String?, customerCardName: String?, customerCardNo: String?, customerCardType: String?, customerEmail: String?, customerPayoptType: String?) {
        self.customerCardId = customerCardId
        self.customerCardLabel = customerCardLabel
        self.customerCardName = customerCardName
        self.customerCardNo = customerCardNo
        self.customerCardType = customerCardType
        self.customerEmail = customerEmail
        self.customerPayoptType = customerPayoptType
    }
}
