//
//  BestSellerProducts.swift
//  Domain
//
//  Created by Saja Hammad on 8/6/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation

// MARK: - BestSellerProducts
public struct BestSellerProducts: Codable, Identifiable {
    
    public var identity: String{
       return uid ?? ""
       }
    public let uid, uuid, typeCode, modifiedTime: String?
    public let name, container, popup, scroll: String?
    public let tabbedProductCarouselComponents, productCodes, title: String?

    public init(uid: String?, uuid: String?, typeCode: String?, modifiedTime: String?, name: String?, container: String?, popup: String?, scroll: String?, tabbedProductCarouselComponents: String?, productCodes: String?, title: String?) {
        self.uid = uid
        self.uuid = uuid
        self.typeCode = typeCode
        self.modifiedTime = modifiedTime
        self.name = name
        self.container = container
        self.popup = popup
        self.scroll = scroll
        self.tabbedProductCarouselComponents = tabbedProductCarouselComponents
        self.productCodes = productCodes
        self.title = title
    }
}

// MARK: BestSellerProducts convenience initializers and mutators

public extension BestSellerProducts {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(BestSellerProducts.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        uid: String?? = nil,
        uuid: String?? = nil,
        typeCode: String?? = nil,
        modifiedTime: String?? = nil,
        name: String?? = nil,
        container: String?? = nil,
        popup: String?? = nil,
        scroll: String?? = nil,
        tabbedProductCarouselComponents: String?? = nil,
        productCodes: String?? = nil,
        title: String?? = nil
    ) -> BestSellerProducts {
        return BestSellerProducts(
            uid: uid ?? self.uid,
            uuid: uuid ?? self.uuid,
            typeCode: typeCode ?? self.typeCode,
            modifiedTime: modifiedTime ?? self.modifiedTime,
            name: name ?? self.name,
            container: container ?? self.container,
            popup: popup ?? self.popup,
            scroll: scroll ?? self.scroll,
            tabbedProductCarouselComponents: tabbedProductCarouselComponents ?? self.tabbedProductCarouselComponents,
            productCodes: productCodes ?? self.productCodes,
            title: title ?? self.title
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

