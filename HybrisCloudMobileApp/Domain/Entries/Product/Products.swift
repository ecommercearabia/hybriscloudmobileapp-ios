//
//  BestDealProduct.swift
//  Domain
//
//  Created by Saja Hammad on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

//import Foundation
//
//// MARK: - BestDealProduct
//public struct Products: Codable ,Identifiable {
//    
//    public var identity: String{
//       return code ?? ""
//       }
//    public let availableForPickup: Bool?
//    public let baseOptions: [BestDealBaseOption]?
//    public let baseProduct: String?
//    public let categories: [BestDealCategory]?
//    public let classifications: [BestDealClassification]?
//    public let code: String?
//    public let configurable: Bool?
//    public let bestDealProductDescription: String?
//    public let images: [BestDealImage]?
//    public let name: String?
//    public let numberOfReviews: Int?
//    public let potentialPromotions: [BestDealPotentialPromotion]?
//    public let price: BestDealPrice?
//    public let priceRange: BestDealPriceRange?
//    public let productReferences: [BestDealProductReference]?
//    public let purchasable: Bool?
//    public let reviews: [JSONAny]?
//    public let stock: BestDealStock?
//    public let summary, url: String?
//
//    enum CodingKeys: String, CodingKey {
//        case availableForPickup, baseOptions, baseProduct, categories, classifications, code, configurable
//        case bestDealProductDescription = "description"
//        case images, name, numberOfReviews, potentialPromotions, price, priceRange, productReferences, purchasable, reviews, stock, summary, url
//    }
//
//    public init(availableForPickup: Bool?, baseOptions: [BestDealBaseOption]?, baseProduct: String?, categories: [BestDealCategory]?, classifications: [BestDealClassification]?, code: String?, configurable: Bool?, bestDealProductDescription: String?, images: [BestDealImage]?, name: String?, numberOfReviews: Int?, potentialPromotions: [BestDealPotentialPromotion]?, price: BestDealPrice?, priceRange: BestDealPriceRange?, productReferences: [BestDealProductReference]?, purchasable: Bool?, reviews: [JSONAny]?, stock: BestDealStock?, summary: String?, url: String?) {
//
//        self.availableForPickup = availableForPickup
//        self.baseOptions = baseOptions
//        self.baseProduct = baseProduct
//        self.categories = categories
//        self.classifications = classifications
//        self.code = code
//        self.configurable = configurable
//        self.bestDealProductDescription = bestDealProductDescription
//        self.images = images
//        self.name = name
//        self.numberOfReviews = numberOfReviews
//        self.potentialPromotions = potentialPromotions
//        self.price = price
//        self.priceRange = priceRange
//        self.productReferences = productReferences
//        self.purchasable = purchasable
//        self.reviews = reviews
//        self.stock = stock
//        self.summary = summary
//        self.url = url
//    }
//}
//
//// MARK: BestDealProduct convenience initializers and mutators
//
//public extension Products {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(Products.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        availableForPickup: Bool?? = nil,
//        baseOptions: [BestDealBaseOption]?? = nil,
//        baseProduct: String?? = nil,
//        categories: [BestDealCategory]?? = nil,
//        classifications: [BestDealClassification]?? = nil,
//        code: String?? = nil,
//        configurable: Bool?? = nil,
//        bestDealProductDescription: String?? = nil,
//        images: [BestDealImage]?? = nil,
//        name: String?? = nil,
//        numberOfReviews: Int?? = nil,
//        potentialPromotions: [BestDealPotentialPromotion]?? = nil,
//        price: BestDealPrice?? = nil,
//        priceRange: BestDealPriceRange?? = nil,
//        productReferences: [BestDealProductReference]?? = nil,
//        purchasable: Bool?? = nil,
//        reviews: [JSONAny]?? = nil,
//        stock: BestDealStock?? = nil,
//        summary: String?? = nil,
//        url: String?? = nil
//    ) -> Products {
//        return Products(
//            availableForPickup: availableForPickup ?? self.availableForPickup,
//            baseOptions: baseOptions ?? self.baseOptions,
//            baseProduct: baseProduct ?? self.baseProduct,
//            categories: categories ?? self.categories,
//            classifications: classifications ?? self.classifications,
//            code: code ?? self.code,
//            configurable: configurable ?? self.configurable,
//            bestDealProductDescription: bestDealProductDescription ?? self.bestDealProductDescription,
//            images: images ?? self.images,
//            name: name ?? self.name,
//            numberOfReviews: numberOfReviews ?? self.numberOfReviews,
//            potentialPromotions: potentialPromotions ?? self.potentialPromotions,
//            price: price ?? self.price,
//            priceRange: priceRange ?? self.priceRange,
//            productReferences: productReferences ?? self.productReferences,
//            purchasable: purchasable ?? self.purchasable,
//            reviews: reviews ?? self.reviews,
//            stock: stock ?? self.stock,
//            summary: summary ?? self.summary,
//            url: url ?? self.url
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - BestDealBaseOption
//public struct BestDealBaseOption: Codable {
//    public let options: [BestDealSelected]?
//    public let selected: BestDealSelected?
//    public let variantType: String?
//
//    public init(options: [BestDealSelected]?, selected: BestDealSelected?, variantType: String?) {
//        self.options = options
//        self.selected = selected
//        self.variantType = variantType
//    }
//}
//
//// MARK: BestDealBaseOption convenience initializers and mutators
//
//public extension BestDealBaseOption {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(BestDealBaseOption.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        options: [BestDealSelected]?? = nil,
//        selected: BestDealSelected?? = nil,
//        variantType: String?? = nil
//    ) -> BestDealBaseOption {
//        return BestDealBaseOption(
//            options: options ?? self.options,
//            selected: selected ?? self.selected,
//            variantType: variantType ?? self.variantType
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - BestDealSelected
//public struct BestDealSelected: Codable {
//    public let code: String?
//    public let priceData: BestDealPrice?
//    public let stock: BestDealStock?
//    public let url: String?
//    public let variantOptionQualifiers: [BestDealVariantOptionQualifier]?
//
//    public init(code: String?, priceData: BestDealPrice?, stock: BestDealStock?, url: String?, variantOptionQualifiers: [BestDealVariantOptionQualifier]?) {
//        self.code = code
//        self.priceData = priceData
//        self.stock = stock
//        self.url = url
//        self.variantOptionQualifiers = variantOptionQualifiers
//    }
//}
//
//// MARK: BestDealSelected convenience initializers and mutators
//
//public extension BestDealSelected {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(BestDealSelected.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        code: String?? = nil,
//        priceData: BestDealPrice?? = nil,
//        stock: BestDealStock?? = nil,
//        url: String?? = nil,
//        variantOptionQualifiers: [BestDealVariantOptionQualifier]?? = nil
//    ) -> BestDealSelected {
//        return BestDealSelected(
//            code: code ?? self.code,
//            priceData: priceData ?? self.priceData,
//            stock: stock ?? self.stock,
//            url: url ?? self.url,
//            variantOptionQualifiers: variantOptionQualifiers ?? self.variantOptionQualifiers
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - BestDealPrice
//public struct BestDealPrice: Codable {
//    public let currencyISO, formattedValue, priceType: String?
//    public let value: Double?
//
//    enum CodingKeys: String, CodingKey {
//        case currencyISO = "currencyIso"
//        case formattedValue, priceType, value
//    }
//
//    public init(currencyISO: String?, formattedValue: String?, priceType: String?, value: Double?) {
//        self.currencyISO = currencyISO
//        self.formattedValue = formattedValue
//        self.priceType = priceType
//        self.value = value
//    }
//}
//
//// MARK: BestDealPrice convenience initializers and mutators
//
//public extension BestDealPrice {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(BestDealPrice.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        currencyISO: String?? = nil,
//        formattedValue: String?? = nil,
//        priceType: String?? = nil,
//        value: Double?? = nil
//    ) -> BestDealPrice {
//        return BestDealPrice(
//            currencyISO: currencyISO ?? self.currencyISO,
//            formattedValue: formattedValue ?? self.formattedValue,
//            priceType: priceType ?? self.priceType,
//            value: value ?? self.value
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - BestDealStock
//public struct BestDealStock: Codable {
//    public let stockLevel: Int?
//    public let stockLevelStatus: String?
//
//    public init(stockLevel: Int?, stockLevelStatus: String?) {
//        self.stockLevel = stockLevel
//        self.stockLevelStatus = stockLevelStatus
//    }
//}
//
//// MARK: BestDealStock convenience initializers and mutators
//
//public extension BestDealStock {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(BestDealStock.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        stockLevel: Int?? = nil,
//        stockLevelStatus: String?? = nil
//    ) -> BestDealStock {
//        return BestDealStock(
//            stockLevel: stockLevel ?? self.stockLevel,
//            stockLevelStatus: stockLevelStatus ?? self.stockLevelStatus
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - BestDealVariantOptionQualifier
//public struct BestDealVariantOptionQualifier: Codable {
//    public let name, qualifier, value: String?
//
//    public init(name: String?, qualifier: String?, value: String?) {
//        self.name = name
//        self.qualifier = qualifier
//        self.value = value
//    }
//}
//
//// MARK: BestDealVariantOptionQualifier convenience initializers and mutators
//
//public extension BestDealVariantOptionQualifier {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(BestDealVariantOptionQualifier.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        name: String?? = nil,
//        qualifier: String?? = nil,
//        value: String?? = nil
//    ) -> BestDealVariantOptionQualifier {
//        return BestDealVariantOptionQualifier(
//            name: name ?? self.name,
//            qualifier: qualifier ?? self.qualifier,
//            value: value ?? self.value
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - BestDealCategory
//public struct BestDealCategory: Codable {
//    public let code, name, url: String?
//
//    public init(code: String?, name: String?, url: String?) {
//        self.code = code
//        self.name = name
//        self.url = url
//    }
//}
//
//// MARK: BestDealCategory convenience initializers and mutators
//
//public extension BestDealCategory {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(BestDealCategory.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        code: String?? = nil,
//        name: String?? = nil,
//        url: String?? = nil
//    ) -> BestDealCategory {
//        return BestDealCategory(
//            code: code ?? self.code,
//            name: name ?? self.name,
//            url: url ?? self.url
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - BestDealClassification
//public struct BestDealClassification: Codable {
//    public let code: String?
//    public let features: [BestDealFeature]?
//    public let name: String?
//
//    public init(code: String?, features: [BestDealFeature]?, name: String?) {
//        self.code = code
//        self.features = features
//        self.name = name
//    }
//}
//
//// MARK: BestDealClassification convenience initializers and mutators
//
//public extension BestDealClassification {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(BestDealClassification.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        code: String?? = nil,
//        features: [BestDealFeature]?? = nil,
//        name: String?? = nil
//    ) -> BestDealClassification {
//        return BestDealClassification(
//            code: code ?? self.code,
//            features: features ?? self.features,
//            name: name ?? self.name
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - BestDealFeature
//public struct BestDealFeature: Codable {
//    public let code: String?
//    public let comparable: Bool?
//    public let featureValues: [BestDealFeatureValue]?
//    public let name: String?
//    public let range: Bool?
//
//    public init(code: String?, comparable: Bool?, featureValues: [BestDealFeatureValue]?, name: String?, range: Bool?) {
//        self.code = code
//        self.comparable = comparable
//        self.featureValues = featureValues
//        self.name = name
//        self.range = range
//    }
//}
//
//// MARK: BestDealFeature convenience initializers and mutators
//
//public extension BestDealFeature {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(BestDealFeature.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        code: String?? = nil,
//        comparable: Bool?? = nil,
//        featureValues: [BestDealFeatureValue]?? = nil,
//        name: String?? = nil,
//        range: Bool?? = nil
//    ) -> BestDealFeature {
//        return BestDealFeature(
//            code: code ?? self.code,
//            comparable: comparable ?? self.comparable,
//            featureValues: featureValues ?? self.featureValues,
//            name: name ?? self.name,
//            range: range ?? self.range
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - BestDealFeatureValue
//public struct BestDealFeatureValue: Codable {
//    public let value: String?
//
//    public init(value: String?) {
//        self.value = value
//    }
//}
//
//// MARK: BestDealFeatureValue convenience initializers and mutators
//
//public extension BestDealFeatureValue {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(BestDealFeatureValue.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        value: String?? = nil
//    ) -> BestDealFeatureValue {
//        return BestDealFeatureValue(
//            value: value ?? self.value
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - BestDealImage
//public struct BestDealImage: Codable {
//    public let altText, format, imageType, url: String?
//    public let galleryIndex: Int?
//
//    public init(altText: String?, format: String?, imageType: String?, url: String?, galleryIndex: Int?) {
//        self.altText = altText
//        self.format = format
//        self.imageType = imageType
//        self.url = url
//        self.galleryIndex = galleryIndex
//    }
//}
//
//// MARK: BestDealImage convenience initializers and mutators
//
//public extension BestDealImage {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(BestDealImage.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        altText: String?? = nil,
//        format: String?? = nil,
//        imageType: String?? = nil,
//        url: String?? = nil,
//        galleryIndex: Int?? = nil
//    ) -> BestDealImage {
//        return BestDealImage(
//            altText: altText ?? self.altText,
//            format: format ?? self.format,
//            imageType: imageType ?? self.imageType,
//            url: url ?? self.url,
//            galleryIndex: galleryIndex ?? self.galleryIndex
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - BestDealPotentialPromotion
//public struct BestDealPotentialPromotion: Codable {
//    public let code, potentialPromotionDescription, promotionType: String?
//
//    enum CodingKeys: String, CodingKey {
//        case code
//        case potentialPromotionDescription = "description"
//        case promotionType
//    }
//
//    public init(code: String?, potentialPromotionDescription: String?, promotionType: String?) {
//        self.code = code
//        self.potentialPromotionDescription = potentialPromotionDescription
//        self.promotionType = promotionType
//    }
//}
//
//// MARK: BestDealPotentialPromotion convenience initializers and mutators
//
//public extension BestDealPotentialPromotion {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(BestDealPotentialPromotion.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        code: String?? = nil,
//        potentialPromotionDescription: String?? = nil,
//        promotionType: String?? = nil
//    ) -> BestDealPotentialPromotion {
//        return BestDealPotentialPromotion(
//            code: code ?? self.code,
//            potentialPromotionDescription: potentialPromotionDescription ?? self.potentialPromotionDescription,
//            promotionType: promotionType ?? self.promotionType
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - BestDealPriceRange
//public struct BestDealPriceRange: Codable {
//    public let maxPrice, minPrice: BestDealMaxPriceClass?
//
//    public init(maxPrice: BestDealMaxPriceClass?, minPrice: BestDealMaxPriceClass?) {
//        self.maxPrice = maxPrice
//        self.minPrice = minPrice
//    }
//}
//
//// MARK: BestDealPriceRange convenience initializers and mutators
//
//public extension BestDealPriceRange {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(BestDealPriceRange.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        maxPrice: BestDealMaxPriceClass?? = nil,
//        minPrice: BestDealMaxPriceClass?? = nil
//    ) -> BestDealPriceRange {
//        return BestDealPriceRange(
//            maxPrice: maxPrice ?? self.maxPrice,
//            minPrice: minPrice ?? self.minPrice
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - BestDealMaxPriceClass
//public struct BestDealMaxPriceClass: Codable {
//    public let currencyISO: String?
//    public let value: Double?
//
//    enum CodingKeys: String, CodingKey {
//        case currencyISO = "currencyIso"
//        case value
//    }
//
//    public init(currencyISO: String?, value: Double?) {
//        self.currencyISO = currencyISO
//        self.value = value
//    }
//}
//
//// MARK: BestDealMaxPriceClass convenience initializers and mutators
//
//public extension BestDealMaxPriceClass {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(BestDealMaxPriceClass.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        currencyISO: String?? = nil,
//        value: Double?? = nil
//    ) -> BestDealMaxPriceClass {
//        return BestDealMaxPriceClass(
//            currencyISO: currencyISO ?? self.currencyISO,
//            value: value ?? self.value
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - BestDealProductReference
//public struct BestDealProductReference: Codable {
//    public let referenceType: String?
//    public let target: BestDealCategory?
//    public let preselected: Bool?
//
//    public init(referenceType: String?, target: BestDealCategory?, preselected: Bool?) {
//        self.referenceType = referenceType
//        self.target = target
//        self.preselected = preselected
//    }
//}
//
//// MARK: BestDealProductReference convenience initializers and mutators
//
//public extension BestDealProductReference {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(BestDealProductReference.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        referenceType: String?? = nil,
//        target: BestDealCategory?? = nil,
//        preselected: Bool?? = nil
//    ) -> BestDealProductReference {
//        return BestDealProductReference(
//            referenceType: referenceType ?? self.referenceType,
//            target: target ?? self.target,
//            preselected: preselected ?? self.preselected
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
