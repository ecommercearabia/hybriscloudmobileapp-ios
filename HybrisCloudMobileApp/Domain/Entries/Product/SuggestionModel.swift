//
//  SuggestionModel.swift
//  Domain
//
//  Created by ahmadSaleh on 12/31/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// MARK: - SuggestionModel
public struct SuggestionModel: Codable , Identifiable {
    
    public var identity: String{
       return "SuggestionModel"
       }
    public let suggestions: [Suggestion]?

    public init(suggestions: [Suggestion]?) {
        self.suggestions = suggestions
    }
}

// MARK: - Suggestion
public struct Suggestion: Codable {
    public let value: String?

    public init(value: String?) {
        self.value = value
    }
}
