//
//  ProductSearch.swift
//  Domain
//
//  Created by khalil on 8/2/20.
//  Copyright © 2020 Erabia. All rights reserved.
//
import Foundation

// MARK: - ProductSearch


public struct ProductSearch: Codable , Identifiable{
    public var identity: String{
        return categoryCode ?? ""
    }
    public let breadcrumbs: [searchBreadcrumb]?
    public let categoryCode: String?
    public let currentQuery: searchCurrentQueryClass?
    public var facets: [searchFacet]?
    public let freeTextSearch, keywordRedirectURL: String?
    public let pagination: searchPagination?
    public let products: [ProductDetails]?
    public let sorts: [searchSort]
    public let spellingSuggestion: searchSpellingSuggestion?

    enum CodingKeys: String, CodingKey {
        case breadcrumbs, categoryCode, currentQuery, facets, freeTextSearch
        case keywordRedirectURL = "keywordRedirectUrl"
        case pagination, products, sorts, spellingSuggestion
    }

    public init(breadcrumbs: [searchBreadcrumb]?, categoryCode: String?, currentQuery: searchCurrentQueryClass?, facets: [searchFacet]?, freeTextSearch: String?, keywordRedirectURL: String?, pagination: searchPagination?, products: [ProductDetails]?, sorts: [searchSort], spellingSuggestion: searchSpellingSuggestion?) {
        self.breadcrumbs = breadcrumbs
        self.categoryCode = categoryCode
        self.currentQuery = currentQuery
        self.facets = facets
        self.freeTextSearch = freeTextSearch
        self.keywordRedirectURL = keywordRedirectURL
        self.pagination = pagination
        self.products = products
        self.sorts = sorts
        self.spellingSuggestion = spellingSuggestion
    }
}

// MARK: - searchBreadcrumb
public struct searchBreadcrumb: Codable {
    public let facetCode, facetName, facetValueCode, facetValueName: String?
    public let removeQuery, truncateQuery: searchCurrentQueryClass?

    public init(facetCode: String?, facetName: String?, facetValueCode: String?, facetValueName: String?, removeQuery: searchCurrentQueryClass?, truncateQuery: searchCurrentQueryClass?) {
        self.facetCode = facetCode
        self.facetName = facetName
        self.facetValueCode = facetValueCode
        self.facetValueName = facetValueName
        self.removeQuery = removeQuery
        self.truncateQuery = truncateQuery
    }
}

// MARK: - searchCurrentQueryClass
public struct searchCurrentQueryClass: Codable {
    public let query: searchQueryElement?
    public let url: String?

    public init(query: searchQueryElement?, url: String?) {
        self.query = query
        self.url = url
    }
}

// MARK: - searchQueryElement
public struct searchQueryElement: Codable {
    public let value: String?

    public init(value: String?) {
        self.value = value
    }
}

// MARK: - searchFacet
public struct searchFacet: Codable {
    public let category, multiSelect: Bool?
    public let name: String?
    public let priority: Double?
    public var topValues, values: [searchValue]?
    public let visible: Bool?

    public init(category: Bool?, multiSelect: Bool?, name: String?, priority: Double?, topValues: [searchValue]?, values: [searchValue]?, visible: Bool?) {
        self.category = category
        self.multiSelect = multiSelect
        self.name = name
        self.priority = priority
        self.topValues = topValues
        self.values = values
        self.visible = visible
    }
}

// MARK: - searchValue
public struct searchValue: Codable {
    public let count: Double?
    public let name: String?
    public let query: searchCurrentQueryClass?
    public var selected: Bool?

    public init(count: Double?, name: String?, query: searchCurrentQueryClass?, selected: Bool?) {
        self.count = count
        self.name = name
        self.query = query
        self.selected = selected
    }
    
}

// MARK: - searchPagination
public struct searchPagination: Codable {
    public let currentPage, pageSize: Double?
    public let sort: String?
    public let totalPages, totalResults: Int?

    public init(currentPage: Double?, pageSize: Double?, sort: String?, totalPages: Int?, totalResults: Int?) {
        self.currentPage = currentPage
        self.pageSize = pageSize
        self.sort = sort
        self.totalPages = totalPages
        self.totalResults = totalResults
    }
}



// MARK: - searchBaseOption
public struct searchBaseOption: Codable {
    public let options: [searchVariantOption]?
    public let selected: searchVariantOption?
    public let variantType: String?

    public init(options: [searchVariantOption]?, selected: searchVariantOption?, variantType: String?) {
        self.options = options
        self.selected = selected
        self.variantType = variantType
    }
}

// MARK: - searchVariantOption
public struct searchVariantOption: Codable {
    public let code: String?
    public let priceData: searchPrice?
    public let stock: searchStock?
    public let url: String?
    public let variantOptionQualifiers: [searchVariantOptionQualifier]?

    public init(code: String?, priceData: searchPrice?, stock: searchStock?, url: String?, variantOptionQualifiers: [searchVariantOptionQualifier]?) {
        self.code = code
        self.priceData = priceData
        self.stock = stock
        self.url = url
        self.variantOptionQualifiers = variantOptionQualifiers
    }
}

// MARK: - searchPrice
public struct searchPrice: Codable {
    public let currencyISO, formattedValue: String?
    public let maxQuantity, minQuantity: Double?
    public let priceType: String?
    public let value: Double?

    enum CodingKeys: String, CodingKey {
        case currencyISO = "currencyIso"
        case formattedValue, maxQuantity, minQuantity, priceType, value
    }

    public init(currencyISO: String?, formattedValue: String?, maxQuantity: Double?, minQuantity: Double?, priceType: String?, value: Double?) {
        self.currencyISO = currencyISO
        self.formattedValue = formattedValue
        self.maxQuantity = maxQuantity
        self.minQuantity = minQuantity
        self.priceType = priceType
        self.value = value
    }
}

// MARK: - searchStock
public struct searchStock: Codable {
    public let stockLevel: Double?
    public let stockLevelStatus: String?

    public init(stockLevel: Double?, stockLevelStatus: String?) {
        self.stockLevel = stockLevel
        self.stockLevelStatus = stockLevelStatus
    }
}

// MARK: - searchVariantOptionQualifier
public struct searchVariantOptionQualifier: Codable {
    public let image: searchImage?
    public let name, qualifier, value: String?

    public init(image: searchImage?, name: String?, qualifier: String?, value: String?) {
        self.image = image
        self.name = name
        self.qualifier = qualifier
        self.value = value
    }
}

// MARK: - searchImage
public struct searchImage: Codable {
    public let altText, format: String?
    public let galleryIndex: Double?
    public let imageType, url: String?

    public init(altText: String?, format: String?, galleryIndex: Double?, imageType: String?, url: String?) {
        self.altText = altText
        self.format = format
        self.galleryIndex = galleryIndex
        self.imageType = imageType
        self.url = url
    }
}

// MARK: - searchCategory
public struct searchCategory: Codable {
    public let code: String?
    public let image: searchImage?
    public let name: String?
    public let plpPicture, plpPictureResponsive: searchImage?
    public let url: String?

    public init(code: String?, image: searchImage?, name: String?, plpPicture: searchImage?, plpPictureResponsive: searchImage?, url: String?) {
        self.code = code
        self.image = image
        self.name = name
        self.plpPicture = plpPicture
        self.plpPictureResponsive = plpPictureResponsive
        self.url = url
    }
}

// MARK: - searchClassification
public struct searchClassification: Codable {
    public let code: String?
    public let features: [searchFeature]?
    public let name: String?

    public init(code: String?, features: [searchFeature]?, name: String?) {
        self.code = code
        self.features = features
        self.name = name
    }
}

// MARK: - searchFeature
public struct searchFeature: Codable {
    public let code: String?
    public let comparable: Bool?
    public let featureDescription: String?
    public let featureUnit: searchFeatureUnit?
    public let featureValues: [searchQueryElement]?
    public let name: String?
    public let range: Bool?
    public let type: String?

    enum CodingKeys: String, CodingKey {
        case code, comparable
        case featureDescription = "description"
        case featureUnit, featureValues, name, range, type
    }

    public init(code: String?, comparable: Bool?, featureDescription: String?, featureUnit: searchFeatureUnit?, featureValues: [searchQueryElement]?, name: String?, range: Bool?, type: String?) {
        self.code = code
        self.comparable = comparable
        self.featureDescription = featureDescription
        self.featureUnit = featureUnit
        self.featureValues = featureValues
        self.name = name
        self.range = range
        self.type = type
    }
}

// MARK: - searchFeatureUnit
public struct searchFeatureUnit: Codable {
    public let name, symbol, unitType: String?

    public init(name: String?, symbol: String?, unitType: String?) {
        self.name = name
        self.symbol = symbol
        self.unitType = unitType
    }
}

// MARK: - searchFutureStock
public struct searchFutureStock: Codable {
    public let date, formattedDate: String?
    public let stock: searchStock?

    public init(date: String?, formattedDate: String?, stock: searchStock?) {
        self.date = date
        self.formattedDate = formattedDate
        self.stock = stock
    }
}

// MARK: - searchPotentialPromotion
public struct searchPotentialPromotion: Codable {
    public let code: String?
    public let couldFireMessages: [String]?
    public let potentialPromotionDescription: String?
    public let enabled: Bool?
    public let endDate: String?
    public let firedMessages: [String]?
    public let priority: Double?
    public let productBanner: searchImage?
    public let promotionGroup, promotionType: String?
    public let restrictions: [searchRestriction]?
    public let startDate, title: String?

    enum CodingKeys: String, CodingKey {
        case code, couldFireMessages
        case potentialPromotionDescription = "description"
        case enabled, endDate, firedMessages, priority, productBanner, promotionGroup, promotionType, restrictions, startDate, title
    }

    public init(code: String?, couldFireMessages: [String]?, potentialPromotionDescription: String?, enabled: Bool?, endDate: String?, firedMessages: [String]?, priority: Double?, productBanner: searchImage?, promotionGroup: String?, promotionType: String?, restrictions: [searchRestriction]?, startDate: String?, title: String?) {
        self.code = code
        self.couldFireMessages = couldFireMessages
        self.potentialPromotionDescription = potentialPromotionDescription
        self.enabled = enabled
        self.endDate = endDate
        self.firedMessages = firedMessages
        self.priority = priority
        self.productBanner = productBanner
        self.promotionGroup = promotionGroup
        self.promotionType = promotionType
        self.restrictions = restrictions
        self.startDate = startDate
        self.title = title
    }
}

// MARK: - searchRestriction
public struct searchRestriction: Codable {
    public let restrictionDescription, restrictionType: String?

    enum CodingKeys: String, CodingKey {
        case restrictionDescription = "description"
        case restrictionType
    }

    public init(restrictionDescription: String?, restrictionType: String?) {
        self.restrictionDescription = restrictionDescription
        self.restrictionType = restrictionType
    }
}

// MARK: - searchPriceRange
public struct searchPriceRange: Codable {
    public let maxPrice, minPrice: searchPrice?

    public init(maxPrice: searchPrice?, minPrice: searchPrice?) {
        self.maxPrice = maxPrice
        self.minPrice = minPrice
    }
}

// MARK: - searchProductReference
public struct searchProductReference: Codable {
    public let productReferenceDescription: String?
    public let preselected: Bool?
    public let quantity: Double?
    public let referenceType: String?

    enum CodingKeys: String, CodingKey {
        case productReferenceDescription = "description"
        case preselected, quantity, referenceType
    }

    public init(productReferenceDescription: String?, preselected: Bool?, quantity: Double?, referenceType: String?) {
        self.productReferenceDescription = productReferenceDescription
        self.preselected = preselected
        self.quantity = quantity
        self.referenceType = referenceType
    }
}

// MARK: - searchReview
public struct searchReview: Codable {
    public let alias, comment, date, headline: String?
    public let id: String?
    public let principal: searchPrincipal?
    public let rating: Double?

    public init(alias: String?, comment: String?, date: String?, headline: String?, id: String?, principal: searchPrincipal?, rating: Double?) {
        self.alias = alias
        self.comment = comment
        self.date = date
        self.headline = headline
        self.id = id
        self.principal = principal
        self.rating = rating
    }
}

// MARK: - searchPrincipal
public struct searchPrincipal: Codable {
    public let currency: searchCurrency?
    public let customerID, deactivationDate: String?
    public let defaultAddress: searchDefaultAddress?
    public let displayUid, firstName: String?
    public let language: searchCurrency?
    public let lastName: String?
    public let mobileCountry: searchCountry?
    public let mobileNumber, name, title, titleCode: String?
    public let uid: String?

    enum CodingKeys: String, CodingKey {
        case currency
        case customerID = "customerId"
        case deactivationDate, defaultAddress, displayUid, firstName, language, lastName, mobileCountry, mobileNumber, name, title, titleCode, uid
    }

    public init(currency: searchCurrency?, customerID: String?, deactivationDate: String?, defaultAddress: searchDefaultAddress?, displayUid: String?, firstName: String?, language: searchCurrency?, lastName: String?, mobileCountry: searchCountry?, mobileNumber: String?, name: String?, title: String?, titleCode: String?, uid: String?) {
        self.currency = currency
        self.customerID = customerID
        self.deactivationDate = deactivationDate
        self.defaultAddress = defaultAddress
        self.displayUid = displayUid
        self.firstName = firstName
        self.language = language
        self.lastName = lastName
        self.mobileCountry = mobileCountry
        self.mobileNumber = mobileNumber
        self.name = name
        self.title = title
        self.titleCode = titleCode
        self.uid = uid
    }
}

// MARK: - searchCurrency
public struct searchCurrency: Codable {
    public let active: Bool?
    public let isocode, name, symbol, nativeName: String?

    public init(active: Bool?, isocode: String?, name: String?, symbol: String?, nativeName: String?) {
        self.active = active
        self.isocode = isocode
        self.name = name
        self.symbol = symbol
        self.nativeName = nativeName
    }
}

// MARK: - searchDefaultAddress
public struct searchDefaultAddress: Codable {
    public let addressName: String?
    public let area: searchArea?
    public let cellphone: String?
    public let city: searchCity?
    public let companyName: String?
    public let country: searchCountry?
    public let defaultAddress: Bool?
    public let district, email, firstName, formattedAddress: String?
    public let id, lastName, line1, line2: String?
    public let mobileCountry: searchCountry?
    public let mobileNumber, nearestLandmark, phone, postalCode: String?
    public let region: searchRegion?
    public let shippingAddress: Bool?
    public let title, titleCode, town: String?
    public let visibleInAddressBook: Bool?

    public init(addressName: String?, area: searchArea?, cellphone: String?, city: searchCity?, companyName: String?, country: searchCountry?, defaultAddress: Bool?, district: String?, email: String?, firstName: String?, formattedAddress: String?, id: String?, lastName: String?, line1: String?, line2: String?, mobileCountry: searchCountry?, mobileNumber: String?, nearestLandmark: String?, phone: String?, postalCode: String?, region: searchRegion?, shippingAddress: Bool?, title: String?, titleCode: String?, town: String?, visibleInAddressBook: Bool?) {
        self.addressName = addressName
        self.area = area
        self.cellphone = cellphone
        self.city = city
        self.companyName = companyName
        self.country = country
        self.defaultAddress = defaultAddress
        self.district = district
        self.email = email
        self.firstName = firstName
        self.formattedAddress = formattedAddress
        self.id = id
        self.lastName = lastName
        self.line1 = line1
        self.line2 = line2
        self.mobileCountry = mobileCountry
        self.mobileNumber = mobileNumber
        self.nearestLandmark = nearestLandmark
        self.phone = phone
        self.postalCode = postalCode
        self.region = region
        self.shippingAddress = shippingAddress
        self.title = title
        self.titleCode = titleCode
        self.town = town
        self.visibleInAddressBook = visibleInAddressBook
    }
}

// MARK: - searchArea
public struct searchArea: Codable {
    public let code, name: String?

    public init(code: String?, name: String?) {
        self.code = code
        self.name = name
    }
}

// MARK: - searchCity
public struct searchCity: Codable {
    public let areas: [searchArea]?
    public let code, name: String?

    public init(areas: [searchArea]?, code: String?, name: String?) {
        self.areas = areas
        self.code = code
        self.name = name
    }
}

// MARK: - searchCountry
public struct searchCountry: Codable {
    public let isdcode, isocode, name: String?

    public init(isdcode: String?, isocode: String?, name: String?) {
        self.isdcode = isdcode
        self.isocode = isocode
        self.name = name
    }
}

// MARK: - searchRegion
public struct searchRegion: Codable {
    public let countryISO, isocode, isocodeShort, name: String?

    enum CodingKeys: String, CodingKey {
        case countryISO = "countryIso"
        case isocode, isocodeShort, name
    }

    public init(countryISO: String?, isocode: String?, isocodeShort: String?, name: String?) {
        self.countryISO = countryISO
        self.isocode = isocode
        self.isocodeShort = isocodeShort
        self.name = name
    }
}

// MARK: - searchVariantMatrix
public struct searchVariantMatrix: Codable {
    public let elements: [JSONNull?]?
    public let isLeaf: Bool?
    public let parentVariantCategory: searchParentVariantCategoryElement?
    public let variantOption: searchVariantOption?
    public let variantValueCategory: searchVariantValueCategory?

    public init(elements: [JSONNull?]?, isLeaf: Bool?, parentVariantCategory: searchParentVariantCategoryElement?, variantOption: searchVariantOption?, variantValueCategory: searchVariantValueCategory?) {
        self.elements = elements
        self.isLeaf = isLeaf
        self.parentVariantCategory = parentVariantCategory
        self.variantOption = variantOption
        self.variantValueCategory = variantValueCategory
    }
}

// MARK: - searchParentVariantCategoryElement
public struct searchParentVariantCategoryElement: Codable {
    public let hasImage: Bool?
    public let name: String?
    public let priority: Double?

    public init(hasImage: Bool?, name: String?, priority: Double?) {
        self.hasImage = hasImage
        self.name = name
        self.priority = priority
    }
}

// MARK: - searchVariantValueCategory
public struct searchVariantValueCategory: Codable {
    public let name: String?
    public let sequence: Double?
    public let superCategories: [searchParentVariantCategoryElement]?

    public init(name: String?, sequence: Double?, superCategories: [searchParentVariantCategoryElement]?) {
        self.name = name
        self.sequence = sequence
        self.superCategories = superCategories
    }
}

// MARK: - searchSort
public struct searchSort: Codable {
    public let code, name: String?
    public let selected: Bool?

    public init(code: String?, name: String?, selected: Bool?) {
        self.code = code
        self.name = name
        self.selected = selected
    }
}

// MARK: - searchSpellingSuggestion
public struct searchSpellingSuggestion: Codable {
    public let query, suggestion: String?

    public init(query: String?, suggestion: String?) {
        self.query = query
        self.suggestion = suggestion
    }
}


 
