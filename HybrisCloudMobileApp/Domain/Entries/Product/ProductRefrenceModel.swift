//
//  ProductRefrenceModel.swift
//  Domain
//
//  Created by Admin on 2020-08-15.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation

// MARK: - ProductRefrenceModel
public struct ProductRefrenceModel: Codable {
    public let references: [Reference]?
}

// MARK: ProductRefrenceModel convenience initializers and mutators

public extension ProductRefrenceModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(ProductRefrenceModel.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        references: [Reference]?? = nil
    ) -> ProductRefrenceModel {
        return ProductRefrenceModel(
            references: references ?? self.references
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Reference
public struct Reference: Codable {
    public let referenceDescription: String?
    public let preselected: Bool?
    public let quantity: Int?
    public let referenceType: String?
    public let target: Target?

    enum CodingKeys: String, CodingKey {
        case referenceDescription = "description"
        case preselected, quantity, referenceType, target
    }
}

// MARK: Reference convenience initializers and mutators

public extension Reference {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Reference.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        referenceDescription: String?? = nil,
        preselected: Bool?? = nil,
        quantity: Int?? = nil,
        referenceType: String?? = nil,
        target: Target?? = nil
    ) -> Reference {
        return Reference(
            referenceDescription: referenceDescription ?? self.referenceDescription,
            preselected: preselected ?? self.preselected,
            quantity: quantity ?? self.quantity,
            referenceType: referenceType ?? self.referenceType,
            target: target ?? self.target
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Target
public struct Target: Codable {
    public let availableForPickup: Bool?
    public let baseOptions: [ProducttBaseOption]?
    public let baseProduct: String?
    public let categories: [ProducttCategory]?
    public let classifications: [ProducttClassification]?
    public let code: String?
    public let futureStocks: [ProducttStock]?
    public let images: [Image]?
    public let name: String?
    public let numberOfReviews: Int?
    public let potentialPromotions: [productPotentialPromotion]?
    public let price: ProducttPrice?
    public let priceRange: ProducttPriceRange?
    public let stock: ProducttStock?
    public let summary: String?
    public let tags: [String]?
    public let url: String?
    public let variantType: String?
    public let volumePrices: [ProducttPrice]?
    public let productReferences: [JSONNull?]?

    enum CodingKeys: String, CodingKey {
        case availableForPickup, baseOptions, baseProduct, categories, classifications, code
        case futureStocks, images, name, numberOfReviews, potentialPromotions, price, priceRange, productReferences, stock, summary, tags, url, variantType, volumePrices
    }
}

// MARK: Target convenience initializers and mutators

public extension Target {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Target.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        availableForPickup: Bool?? = nil,
        baseOptions: [ProducttBaseOption]?? = nil,
        baseProduct: String?? = nil,
        categories: [ProducttCategory]?? = nil,
        classifications: [ProducttClassification]?? = nil,
        code: String?? = nil,
        futureStocks: [ProducttStock]?? = nil,
        images: [Image]?? = nil,
        name: String?? = nil,
        numberOfReviews: Int?? = nil,
        potentialPromotions: [productPotentialPromotion]?? = nil,
        price: ProducttPrice?? = nil,
        priceRange: ProducttPriceRange?? = nil,
        productReferences: [JSONNull?]?? = nil,
        purchasable: Bool?? = nil,
        stock: ProducttStock?? = nil,
        summary: String?? = nil,
        url: String?? = nil,
        variantType: String?? = nil,
        volumePrices: [ProducttPrice]?? = nil
    ) -> Target {
        return Target(availableForPickup: availableForPickup ?? self.availableForPickup,
                      baseOptions: baseOptions ?? self.baseOptions,
                      baseProduct: baseProduct ?? self.baseProduct,
                      categories: categories ?? self.categories,
                      classifications: classifications ?? self.classifications,
                      code: code ?? self.code,
                      futureStocks: futureStocks ?? self.futureStocks,
                      images: images ?? self.images,
                      name: name ?? self.name,
                      numberOfReviews: numberOfReviews ?? self.numberOfReviews,
                      potentialPromotions: potentialPromotions ?? self.potentialPromotions,
                      price: price ?? self.price,
                      priceRange: priceRange ?? self.priceRange,
                      stock: stock ?? self.stock,
                      summary: summary ?? self.summary,
                      tags: tags ?? self.tags,
                      url: url ?? self.url, variantType: variantType ?? self.variantType,
                      volumePrices: volumePrices ?? self.volumePrices,
                      productReferences: productReferences ?? self.productReferences
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
