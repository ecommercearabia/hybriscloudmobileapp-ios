//
//  AddDeleteWishListItemRequest.swift
//  Domain
//
//  Created by Saja Hammad on 8/27/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

public struct AddDeleteWishListItemRequest: Codable {
    public let wishlistPK: String?
    public let productCode: String?

    
    public init( wishlistPK: String?, productCode: String?) {
        self.wishlistPK = wishlistPK
        self.productCode = productCode
    }
    
}
