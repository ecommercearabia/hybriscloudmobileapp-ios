//
//  WishList.swift
//  Domain
//
//  Created by Saja Hammad on 8/26/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation

// MARK: - WishList
public class WishList: Codable  ,Identifiable{
   public var identity: String{
       get{
           return pk ?? ""
       }
   }
    public let wishListDescription, name, pk: String?
    public let wishlistEntries: [WishListWishlistEntry]?

    enum CodingKeys: String, CodingKey {
        case wishListDescription
        case name, pk, wishlistEntries
    }

    public init(wishListDescription: String?, name: String?, pk: String?, wishlistEntries: [WishListWishlistEntry]?) {
        self.wishListDescription = wishListDescription
        self.name = name
        self.pk = pk
        self.wishlistEntries = wishlistEntries
    }
}

// MARK: - WishListWishlistEntry
public class WishListWishlistEntry: Codable {
    public let addedDate: String?
    public let product: ProductDetails?
    public let wishlistPK: String?

    public init(addedDate: String?, product: ProductDetails?, wishlistPK: String?) {
        self.addedDate = addedDate
        self.product = product
        self.wishlistPK = wishlistPK
    }
}


// MARK: - WishListBaseOption
public class WishListBaseOption: Codable {
    public let options: [WishListSelected]?
    public let selected: WishListSelected?
    public let variantType: String?

    public init(options: [WishListSelected]?, selected: WishListSelected?, variantType: String?) {
        self.options = options
        self.selected = selected
        self.variantType = variantType
    }
}

// MARK: - WishListSelected
public class WishListSelected: Codable {
    public let code: String?
    public let priceData: WishListPrice?
    public let stock: WishListStock?
    public let url: String?
    public let variantOptionQualifiers: [WishListVariantOptionQualifier]?

    public init(code: String?, priceData: WishListPrice?, stock: WishListStock?, url: String?, variantOptionQualifiers: [WishListVariantOptionQualifier]?) {
        self.code = code
        self.priceData = priceData
        self.stock = stock
        self.url = url
        self.variantOptionQualifiers = variantOptionQualifiers
    }
}

// MARK: - WishListPrice
public class WishListPrice: Codable {
    public let currencyISO, formattedValue, priceType: String?
    public let value: Double?

    enum CodingKeys: String, CodingKey {
        case currencyISO
        case formattedValue, priceType, value
    }

    public init(currencyISO: String?, formattedValue: String?, priceType: String?, value: Double?) {
        self.currencyISO = currencyISO
        self.formattedValue = formattedValue
        self.priceType = priceType
        self.value = value
    }
}

// MARK: - WishListStock
public class WishListStock: Codable {
    public let stockLevel: Int?
    public let stockLevelStatus: String?

    public init(stockLevel: Int?, stockLevelStatus: String?) {
        self.stockLevel = stockLevel
        self.stockLevelStatus = stockLevelStatus
    }
}

// MARK: - WishListVariantOptionQualifier
public class WishListVariantOptionQualifier: Codable {
    public let name, qualifier, value: String?

    public init(name: String?, qualifier: String?, value: String?) {
        self.name = name
        self.qualifier = qualifier
        self.value = value
    }
}

// MARK: - WishListCategory
public class WishListCategory: Codable {
    public let code: String?
    public let image: WishListCategoryImage?
    public let name, url: String?

    public init(code: String?, image: WishListCategoryImage?, name: String?, url: String?) {
        self.code = code
        self.image = image
        self.name = name
        self.url = url
    }
}

// MARK: - WishListCategoryImage
public class WishListCategoryImage: Codable {
    public let format, url: String?

    public init(format: String?, url: String?) {
        self.format = format
        self.url = url
    }
}

// MARK: - WishListClassification
public class WishListClassification: Codable {
    public let code: String?
    public let features: [WishListFeature]?
    public let name: String?

    public init(code: String?, features: [WishListFeature]?, name: String?) {
        self.code = code
        self.features = features
        self.name = name
    }
}

// MARK: - WishListFeature
public class WishListFeature: Codable {
    public let code: String?
    public let comparable: Bool?
    public let featureValues: [WishListFeatureValue]?
    public let name: String?
    public let range: Bool?

    public init(code: String?, comparable: Bool?, featureValues: [WishListFeatureValue]?, name: String?, range: Bool?) {
        self.code = code
        self.comparable = comparable
        self.featureValues = featureValues
        self.name = name
        self.range = range
    }
}

// MARK: - WishListFeatureValue
public class WishListFeatureValue: Codable {
    public let value: String?

    public init(value: String?) {
        self.value = value
    }
}

// MARK: - WishListImageElement
public class WishListImageElement: Codable {
    public let altText, format, imageType, url: String?
    public let galleryIndex: Int?

    public init(altText: String?, format: String?, imageType: String?, url: String?, galleryIndex: Int?) {
        self.altText = altText
        self.format = format
        self.imageType = imageType
        self.url = url
        self.galleryIndex = galleryIndex
    }
}

// MARK: - WishListPriceRange
public class WishListPriceRange: Codable {
    public let maxPrice, minPrice: WishListMaxPriceClass?

    public init(maxPrice: WishListMaxPriceClass?, minPrice: WishListMaxPriceClass?) {
        self.maxPrice = maxPrice
        self.minPrice = minPrice
    }
}

// MARK: - WishListMaxPriceClass
public class WishListMaxPriceClass: Codable {
    public let currencyISO: String?
    public let value: Double?

    enum CodingKeys: String, CodingKey {
        case currencyISO
        case value
    }

    public init(currencyISO: String?, value: Double?) {
        self.currencyISO = currencyISO
        self.value = value
    }
}


