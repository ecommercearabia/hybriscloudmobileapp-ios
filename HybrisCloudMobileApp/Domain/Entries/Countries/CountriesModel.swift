//
//  CountriesModel.swift
//  Domain
//
//  Created by Admin on 2020-08-03.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation


// MARK: - AddressesModel
public struct CountriesModel: Codable , Identifiable
{
    public var identity: String{
        return countries?.first?.isocode ?? ""
    }
   public let countries: [Country]?
}

// MARK: AddressesModel convenience initializers and mutators

extension CountriesModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(CountriesModel.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        addresses: [Country]?? = nil
    ) -> CountriesModel {
        return CountriesModel(
            countries: addresses ?? self.countries
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - AddressesModel
public struct RegionsModel: Codable {
    let regions: [Region]?
}

// MARK: AddressesModel convenience initializers and mutators

extension RegionsModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(RegionsModel.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        addresses: [Region]?? = nil
    ) -> RegionsModel {
        return RegionsModel(
            regions: addresses ?? self.regions
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
