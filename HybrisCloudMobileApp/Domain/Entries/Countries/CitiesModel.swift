//
//  CitiesModel.swift
//  Domain
//
//  Created by walaa omar on 8/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// MARK: - CitiesModel

public struct CitiesModel: Codable , Identifiable {
    
    public let cities: [City]?
    
    public var identity: String{
        return cities?.first?.code ?? ""
    }
  
    
}

// MARK: CitiesModel convenience initializers and mutators

public extension CitiesModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(CitiesModel.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        cities: [City]?? = nil
    ) -> CitiesModel {
        return CitiesModel(
            cities: cities ?? self.cities
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
