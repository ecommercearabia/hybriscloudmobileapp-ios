// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let catalogVersion = try? newJSONDecoder().decode(CatalogVersion.self, from: jsonData)

import Foundation

public protocol Identifiable {
    var identity: String { get }
}



// MARK: - CatalogVersion
public struct Catalog: Codable , Identifiable {
    public var identity: String{
        return  id ?? ""
    }

    public let type, id, lastModified, url: String?
    public let categories: [CategoryElement]?

    public init(type: String?, id: String?, lastModified: String?, url: String?, categories: [CategoryElement]?) {
        self.type = type
        self.id = id
        self.lastModified = lastModified
        self.url = url
        self.categories = categories
    }
}

// MARK: - Subcategory
public struct Subcategory: Codable {
    public let category: PurpleCategory?
    public let id, lastModified, name, url: String?
    public let subcategories: [CategoryElement]?

    public init(category: PurpleCategory?, id: String?, lastModified: String?, name: String?, url: String?, subcategories: [CategoryElement]?) {
        self.category = category
        self.id = id
        self.lastModified = lastModified
        self.name = name
        self.url = url
        self.subcategories = subcategories
    }
}

// MARK: - CategoryElement
public struct CategoryElement: Codable {
    public let category: CategoryCategory?
    public let id, lastModified, name, url: String?
    public let subcategories: [Subcategory]?

    public init(category: CategoryCategory?, id: String?, lastModified: String?, name: String?, url: String?, subcategories: [Subcategory]?) {
        self.category = category
        self.id = id
        self.lastModified = lastModified
        self.name = name
        self.url = url
        self.subcategories = subcategories
    }
}

// MARK: - PurpleCategory
public struct PurpleCategory: Codable {
    public let code: String?
    public let image: Image?
    public let name: String?
    public let plpPicture, plpPictureResponsive: Image?
    public let url: String?

    public init(code: String?, image: Image?, name: String?, plpPicture: Image?, plpPictureResponsive: Image?, url: String?) {
        self.code = code
        self.image = image
        self.name = name
        self.plpPicture = plpPicture
        self.plpPictureResponsive = plpPictureResponsive
        self.url = url
    }
}

// MARK: - Image
public struct Image: Codable {
    public let format, url: String?

    public init(format: String?, url: String?) {
        self.format = format
        self.url = url
    }
}

// MARK: - CategoryCategory
public struct CategoryCategory: Codable {
    public let code, name, url: String?
    public let image: Image?

    public init(code: String?, name: String?, url: String?, image: Image?) {
        self.code = code
        self.name = name
        self.url = url
        self.image = image
    }
}
