//
//  timeSloteModel.swift
//  Domain
//
//  Created by ahmadSaleh on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// MARK: - TimeSloteModel
public struct TimeSloteModel: Codable {
    public let date, day: String?
    public let end: End?
    public let periodCode: String?
    public let start: End?

    public init(date: String?, day: String?, end: End?, periodCode: String?, start: End?) {
        self.date = date
        self.day = day
        self.end = end
        self.periodCode = periodCode
        self.start = start
    }
}

// MARK: - End
public struct End: Codable {
    public let hour, minute, nano, second: Int?

    public init(hour: Int?, minute: Int?, nano: Int?, second: Int?) {
        self.hour = hour
        self.minute = minute
        self.nano = nano
        self.second = second
    }
}
