//
//  setPaymentModeResponse.swift
//  Domain
//
//  Created by ahmadSaleh on 8/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

 import Foundation

// MARK: - SetPaymentModeResponse
public struct SetPaymentModeResponse: Codable {
    public let code, setPaymentModeResponseDescription, name: String?

    enum CodingKeys: String, CodingKey {
        case code
        case setPaymentModeResponseDescription = "description"
        case name
    }

    public init(code: String?, setPaymentModeResponseDescription: String?, name: String?) {
        self.code = code
        self.setPaymentModeResponseDescription = setPaymentModeResponseDescription
        self.name = name
    }
}
