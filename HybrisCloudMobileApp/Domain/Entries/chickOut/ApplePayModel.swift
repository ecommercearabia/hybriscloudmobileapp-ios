//
//  ApplePayModel.swift
//  Domain
//
//  Created by khalil on 5/8/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation

// MARK: - ApplePayModel
public struct ApplePayModel: Codable , Identifiable  {
    public var identity: String{
        return id ?? ""
    }
    
    public var accountHolderName: String?
    public var webSource: String?
    public var success: Bool?
    public var billingAddress: ApplePayBillingAddress?
    public var cardNumber: String?
    public var cardType: ApplePayCardType?
    public var defaultPayment: Bool?
    public var expiryMonth: String?
    public var expiryYear: String?
    public var id: String?
    public var issueNumber: String?
    public var saved: Bool?
    public var startMonth: String?
    public var startYear: String?
    public var subscriptionID: String?

    enum CodingKeys: String, CodingKey {
        case accountHolderName = "accountHolderName"
        case webSource = "webSource"
        case success = "success"
        case billingAddress = "billingAddress"
        case cardNumber = "cardNumber"
        case cardType = "cardType"
        case defaultPayment = "defaultPayment"
        case expiryMonth = "expiryMonth"
        case expiryYear = "expiryYear"
        case id = "id"
        case issueNumber = "issueNumber"
        case saved = "saved"
        case startMonth = "startMonth"
        case startYear = "startYear"
        case subscriptionID = "subscriptionId"
    }

    public init(accountHolderName: String?, billingAddress: ApplePayBillingAddress?, cardNumber: String?, cardType: ApplePayCardType?, defaultPayment: Bool?, expiryMonth: String?, expiryYear: String?, id: String?, issueNumber: String?, saved: Bool?, startMonth: String?, startYear: String?, subscriptionID: String?) {
        self.accountHolderName = accountHolderName
        self.billingAddress = billingAddress
        self.cardNumber = cardNumber
        self.cardType = cardType
        self.defaultPayment = defaultPayment
        self.expiryMonth = expiryMonth
        self.expiryYear = expiryYear
        self.id = id
        self.issueNumber = issueNumber
        self.saved = saved
        self.startMonth = startMonth
        self.startYear = startYear
        self.subscriptionID = subscriptionID
    }
}

// MARK: - ApplePayBillingAddress
public struct ApplePayBillingAddress: Codable {
    public var addressName: String?
    public var apartmentNumber: String?
    public var area: ApplePayCardType?
    public var buildingName: String?
    public var cellphone: String?
    public var city: ApplePayCity?
    public var companyName: String?
    public var country: ApplePayCountry?
    public var defaultAddress: Bool?
    public var district: String?
    public var email: String?
    public var firstName: String?
    public var formattedAddress: String?
    public var id: String?
    public var lastName: String?
    public var line1: String?
    public var line2: String?
    public var mobileCountry: ApplePayCountry?
    public var mobileNumber: String?
    public var nearestLandmark: String?
    public var phone: String?
    public var postalCode: String?
    public var region: ApplePayRegion?
    public var shippingAddress: Bool?
    public var streetName: String?
    public var title: String?
    public var titleCode: String?
    public var town: String?
    public var visibleInAddressBook: Bool?

    enum CodingKeys: String, CodingKey {
        case addressName = "addressName"
        case apartmentNumber = "apartmentNumber"
        case area = "area"
        case buildingName = "buildingName"
        case cellphone = "cellphone"
        case city = "city"
        case companyName = "companyName"
        case country = "country"
        case defaultAddress = "defaultAddress"
        case district = "district"
        case email = "email"
        case firstName = "firstName"
        case formattedAddress = "formattedAddress"
        case id = "id"
        case lastName = "lastName"
        case line1 = "line1"
        case line2 = "line2"
        case mobileCountry = "mobileCountry"
        case mobileNumber = "mobileNumber"
        case nearestLandmark = "nearestLandmark"
        case phone = "phone"
        case postalCode = "postalCode"
        case region = "region"
        case shippingAddress = "shippingAddress"
        case streetName = "streetName"
        case title = "title"
        case titleCode = "titleCode"
        case town = "town"
        case visibleInAddressBook = "visibleInAddressBook"
    }

    public init(addressName: String?, apartmentNumber: String?, area: ApplePayCardType?, buildingName: String?, cellphone: String?, city: ApplePayCity?, companyName: String?, country: ApplePayCountry?, defaultAddress: Bool?, district: String?, email: String?, firstName: String?, formattedAddress: String?, id: String?, lastName: String?, line1: String?, line2: String?, mobileCountry: ApplePayCountry?, mobileNumber: String?, nearestLandmark: String?, phone: String?, postalCode: String?, region: ApplePayRegion?, shippingAddress: Bool?, streetName: String?, title: String?, titleCode: String?, town: String?, visibleInAddressBook: Bool?) {
        self.addressName = addressName
        self.apartmentNumber = apartmentNumber
        self.area = area
        self.buildingName = buildingName
        self.cellphone = cellphone
        self.city = city
        self.companyName = companyName
        self.country = country
        self.defaultAddress = defaultAddress
        self.district = district
        self.email = email
        self.firstName = firstName
        self.formattedAddress = formattedAddress
        self.id = id
        self.lastName = lastName
        self.line1 = line1
        self.line2 = line2
        self.mobileCountry = mobileCountry
        self.mobileNumber = mobileNumber
        self.nearestLandmark = nearestLandmark
        self.phone = phone
        self.postalCode = postalCode
        self.region = region
        self.shippingAddress = shippingAddress
        self.streetName = streetName
        self.title = title
        self.titleCode = titleCode
        self.town = town
        self.visibleInAddressBook = visibleInAddressBook
    }
}

// MARK: - ApplePayCardType
public struct ApplePayCardType: Codable {
    public var code: String?
    public var name: String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case name = "name"
    }

    public init(code: String?, name: String?) {
        self.code = code
        self.name = name
    }
}

// MARK: - ApplePayCity
public struct ApplePayCity: Codable {
    public var areas: [ApplePayCardType]?
    public var code: String?
    public var name: String?

    enum CodingKeys: String, CodingKey {
        case areas = "areas"
        case code = "code"
        case name = "name"
    }

    public init(areas: [ApplePayCardType]?, code: String?, name: String?) {
        self.areas = areas
        self.code = code
        self.name = name
    }
}

// MARK: - ApplePayCountry
public struct ApplePayCountry: Codable {
    public var isdcode: String?
    public var isocode: String?
    public var name: String?

    enum CodingKeys: String, CodingKey {
        case isdcode = "isdcode"
        case isocode = "isocode"
        case name = "name"
    }

    public init(isdcode: String?, isocode: String?, name: String?) {
        self.isdcode = isdcode
        self.isocode = isocode
        self.name = name
    }
}

// MARK: - ApplePayRegion
public struct ApplePayRegion: Codable {
    public var countryISO: String?
    public var isocode: String?
    public var isocodeShort: String?
    public var name: String?

    enum CodingKeys: String, CodingKey {
        case countryISO = "countryIso"
        case isocode = "isocode"
        case isocodeShort = "isocodeShort"
        case name = "name"
    }

    public init(countryISO: String?, isocode: String?, isocodeShort: String?, name: String?) {
        self.countryISO = countryISO
        self.isocode = isocode
        self.isocodeShort = isocodeShort
        self.name = name
    }
}
