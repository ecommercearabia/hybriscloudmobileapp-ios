//
//  TimeSlotsModel.swift
//  Domain
//
//  Created by ahmadSaleh on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation

// MARK: - TimeSlotsModel
public class TimeSlotsModel: Codable , Identifiable
{
    public var identity: String{
        return timeSlotDays?.first?.periods?.first?.code ?? ""
    }
    public let timeSlotDays: [TimeSlotDay]?

    public init(timeSlotDays: [TimeSlotDay]?) {
        self.timeSlotDays = timeSlotDays
    }
}

// MARK: - TimeSlotDay
public class TimeSlotDay: Codable {
    public let date, day: String?
    public let periods: [Period]?
    public var selected: Bool? = false
 

    public init(date: String?, day: String?, periods: [Period]?) {
        self.date = date
        self.day = day
        self.periods = periods
    }
}

// MARK: - Period
public class Period: Codable {
    public let code: String?
    public let enabled: Bool?
    public let end, intervalFormattedValue, start: String?
    public var selected : Bool? = false
    public init(code: String?, enabled: Bool?, end: String?, intervalFormattedValue: String?, start: String?) {
        self.code = code
        self.enabled = enabled
        self.end = end
        self.intervalFormattedValue = intervalFormattedValue
        self.start = start
    }
}
