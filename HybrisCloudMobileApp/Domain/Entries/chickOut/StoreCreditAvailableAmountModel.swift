//
//  StoreCreditAvailableAmountModel.swift
//  Domain
//
//  Created by ahmadSaleh on 9/7/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let storeCreditAvailableAmountModel = try? newJSONDecoder().decode(StoreCreditAvailableAmountModel.self, from: jsonData)

import Foundation

// MARK: - StoreCreditAvailableAmountModel
public struct StoreCreditAvailableAmountModel: Codable , Identifiable
{
    public var identity: String{
        return  String(value ?? 0.0)
    }
    public let currencyISO, formattedValue, priceType: String?
    public let value: Double?

    enum CodingKeys: String, CodingKey {
        case currencyISO = "currencyIso"
        case formattedValue, priceType, value
    }

    public init(currencyISO: String?, formattedValue: String?, priceType: String?, value: Double?) {
        self.currencyISO = currencyISO
        self.formattedValue = formattedValue
        self.priceType = priceType
        self.value = value
    }
}
