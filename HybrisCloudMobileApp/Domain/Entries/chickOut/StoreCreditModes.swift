//
//  StoreCreditModes.swift
//  Domain
//
//  Created by ahmadSaleh on 9/7/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let storeCreditModes = try? newJSONDecoder().decode(StoreCreditModes.self, from: jsonData)

import Foundation

// MARK: - StoreCreditModes
public struct StoreCreditModes: Codable , Identifiable
{
    public var identity: String{
        return  storeCreditModes?.first?.storeCreditModeType?.code ?? ""
    }
    public let storeCreditModes: [StoreCreditMode]?

    public init(storeCreditModes: [StoreCreditMode]?) {
        self.storeCreditModes = storeCreditModes
    }
}

// MARK: - StoreCreditMode
public struct StoreCreditMode: Codable   {
 
    public let name: String?
    public let storeCreditModeType: StoreCreditModeType?

    public init(name: String?, storeCreditModeType: StoreCreditModeType?) {
        self.name = name
        self.storeCreditModeType = storeCreditModeType
    }
}

// MARK: - StoreCreditModeType
public struct StoreCreditModeType: Codable {
    public let code: String?

    public init(code: String?) {
        self.code = code
    }
}
