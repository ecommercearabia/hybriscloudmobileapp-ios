//
//  setTimeSlotModel.swift
//  Domain
//
//  Created by ahmadSaleh on 8/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// MARK: - SetTimeSlotModel
public struct SetTimeSlotModel: Codable {
    public let date, day, end, periodCode: String?
    public let start: String?

    public init(date: String?, day: String?, end: String?, periodCode: String?, start: String?) {
        self.date = date
        self.day = day
        self.end = end
        self.periodCode = periodCode
        self.start = start
    }
}


