//
//  DeliveryModeModel.swift
//  Domain
//
//  Created by ahmadSaleh on 8/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// MARK: - DeliveryModeModel
public struct DeliveryModeModel: Codable , Identifiable
{
    public var identity: String{
        return code ?? ""
    }
    public let code: String?
    public let deliveryCost: DeliveryCost?
    public let deliveryModeModelDescription, name: String?

    enum CodingKeys: String, CodingKey {
        case code, deliveryCost
        case deliveryModeModelDescription = "description"
        case name
    }

    public init(code: String?, deliveryCost: DeliveryCost?, deliveryModeModelDescription: String?, name: String?) {
        self.code = code
        self.deliveryCost = deliveryCost
        self.deliveryModeModelDescription = deliveryModeModelDescription
        self.name = name
    }
}

// MARK: - DeliveryCost
public struct DeliveryCost: Codable {
    public let currencyISO, formattedValue, priceType: String?
    public let value: Double?

    enum CodingKeys: String, CodingKey {
        case currencyISO = "currencyIso"
        case formattedValue, priceType, value
    }

    public init(currencyISO: String?, formattedValue: String?, priceType: String?, value: Double?) {
        self.currencyISO = currencyISO
        self.formattedValue = formattedValue
        self.priceType = priceType
        self.value = value
    }
}
