//
//  ConsentsListModel.swift
//  Domain
//
//  Created by khalil on 9/9/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//




import Foundation

// MARK: - Consents
public struct Consents: Codable , Identifiable {
    public var identity: String{
        return consentTemplates?.first?.id ?? ""
    }

    public var consentTemplates: [ConsentTemplate]?

    enum CodingKeys: String, CodingKey {
        case consentTemplates = "consentTemplates"
    }

    public init(consentTemplates: [ConsentTemplate]?) {
        self.consentTemplates = consentTemplates
    }
}

// MARK: - ConsentTemplate
public struct ConsentTemplate: Codable {
    public var currentConsent: CurrentConsent?
    public var consentTemplateDescription: String?
    public var id: String?
    public var name: String?
    public var version: Int?

    enum CodingKeys: String, CodingKey {
        case currentConsent = "currentConsent"
        case consentTemplateDescription = "description"
        case id = "id"
        case name = "name"
        case version = "version"
    }

    public init(currentConsent: CurrentConsent?, consentTemplateDescription: String?, id: String?, name: String?, version: Int?) {
        self.currentConsent = currentConsent
        self.consentTemplateDescription = consentTemplateDescription
        self.id = id
        self.name = name
        self.version = version
    }
}

// MARK: - CurrentConsent
public struct CurrentConsent: Codable {
    public var code: String?
    public var consentGivenDate: String?
    public var consentWithdrawnDate: String?
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case consentGivenDate = "consentGivenDate"
        case consentWithdrawnDate = "consentWithdrawnDate"
    }

    public init(code: String?, consentGivenDate: String?, consentWithdrawnDate: String?) {
            self.code = code
            self.consentGivenDate = consentGivenDate
            self.consentWithdrawnDate = consentWithdrawnDate
        
    }

}

