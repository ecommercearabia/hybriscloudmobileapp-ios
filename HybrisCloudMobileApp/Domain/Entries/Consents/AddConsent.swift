//
//  AddConsent.swift
//  Domain
//
//  Created by khalil on 9/9/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation

// MARK: - AddConsent
public struct AddConsent: Codable {
    public var currentConsent: AddCurrentConsent?
    public var addConsentDescription: String?
    public var id: String?
    public var name: String?
    public var version: Int?

    enum CodingKeys: String, CodingKey {
        case currentConsent = "currentConsent"
        case addConsentDescription = "description"
        case id = "id"
        case name = "name"
        case version = "version"
    }

    public init(currentConsent: AddCurrentConsent?, addConsentDescription: String?, id: String?, name: String?, version: Int?) {
        self.currentConsent = currentConsent
        self.addConsentDescription = addConsentDescription
        self.id = id
        self.name = name
        self.version = version
    }
}

// MARK: - AddCurrentConsent
public struct AddCurrentConsent: Codable {
    public var code: String?
    public var consentGivenDate: String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case consentGivenDate = "consentGivenDate"
    }

    public init(code: String?, consentGivenDate: String?) {
        self.code = code
        self.consentGivenDate = consentGivenDate
    }
}
