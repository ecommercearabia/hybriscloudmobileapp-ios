//
//  AddressesModels.swift
//  Domain
//
//  Created by Admin on 2020-08-03.
//  Eadited by walaa on 2020-08-08
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// MARK: - AddressesModel
public struct AddressesModel: Codable , Identifiable
{
    public var identity: String{
        return "1"
    }
    public let addresses: [Address]?

    public init(addresses: [Address]?) {
        self.addresses = addresses
    }
}

// MARK: - Address
public struct Address: Codable , Identifiable
{
    public var identity: String{
        return id ?? ""
    }
    public var addressName,apartmentNumber: String?
    public var area: Area?
    public var cellphone,buildingName: String?
    public var city: City?
    public var companyName: String?
    public var country: Country?
    public var defaultAddress: Bool?
    public var district, email, firstName, formattedAddress: String?
    public var id, lastName, line1, line2: String?
    public var mobileCountry: Country?
    public var mobileNumber, nearestLandmark, phone, postalCode: String?
    public var region: Region?
    public var shippingAddress: Bool?
    public var title, titleCode, town , streetName: String?
    public var visibleInAddressBook: Bool?

    public init(addressName: String?, apartmentNumber: String?, area: Area?, cellphone: String?, buildingName: String?, city: City?, companyName: String?, country: Country?, defaultAddress: Bool?, district: String?, email: String?, firstName: String?, formattedAddress: String?, id: String?, lastName: String?, line1: String?, line2: String?, mobileCountry: Country?, mobileNumber: String?, nearestLandmark: String?, phone: String?, postalCode: String?, region: Region?, shippingAddress: Bool?, title: String?, titleCode: String?, town: String?, visibleInAddressBook: Bool?, streetName: String?) {
        self.addressName = addressName
        self.apartmentNumber = apartmentNumber
        self.area = area
        self.buildingName = buildingName
        self.cellphone = cellphone
        self.city = city
        self.companyName = companyName
        self.country = country
        self.defaultAddress = defaultAddress
        self.district = district
        self.email = email
        self.firstName = firstName
        self.formattedAddress = formattedAddress
        self.id = id
        self.lastName = lastName
        self.line1 = line1
        self.line2 = line2
        self.mobileCountry = mobileCountry
        self.mobileNumber = mobileNumber
        self.nearestLandmark = nearestLandmark
        self.phone = phone
        self.postalCode = postalCode
        self.region = region
        self.shippingAddress = shippingAddress
        self.title = title
        self.titleCode = titleCode
        self.town = town
        self.visibleInAddressBook = visibleInAddressBook
        self.streetName = streetName

    }
}

// MARK: - Area
public struct Area: Codable, Equatable {
    public let code, name: String?

    public init(code: String?, name: String?) {
        self.code = code
        self.name = name
    }
}

// MARK: - City
public struct City: Codable, Equatable {
    public let areas: [Area]?
    public let code, name: String?

    public init(areas: [Area]?, code: String?, name: String?) {
        self.areas = areas
        self.code = code
        self.name = name
    }
}
