//
//  PaymentDetailsResponse.swift
//  Domain
//
//  Created by Ahmad Bader on 8/18/20.
//  Copyright © 2020 Erabia. All rights reserved.



import Foundation

// MARK: - PaymentDetailsResponse
public struct PaymentDetailsResponse: Codable ,Identifiable{
    public var identity: String{
        get{
            return id ?? ""
        }
    }
    
    
    public let accountHolderName: String?
    public let billingAddress: PaymentDetailsResponseBillingAddress?
    public let cardNumber: String?
    public let cardType: PaymentDetailsResponseCardType?
    public let defaultPayment: Bool?
    public let expiryMonth, expiryYear, id, issueNumber: String?
    public let saved: Bool?
    public let startMonth, startYear, subscriptionID: String?

    enum CodingKeys: String, CodingKey {
        case accountHolderName, billingAddress, cardNumber, cardType, defaultPayment, expiryMonth, expiryYear, id, issueNumber, saved, startMonth, startYear
        case subscriptionID = "subscriptionId"
    }

    public init(accountHolderName: String?, billingAddress: PaymentDetailsResponseBillingAddress?, cardNumber: String?, cardType: PaymentDetailsResponseCardType?, defaultPayment: Bool?, expiryMonth: String?, expiryYear: String?, id: String?, issueNumber: String?, saved: Bool?, startMonth: String?, startYear: String?, subscriptionID: String?) {
        self.accountHolderName = accountHolderName
        self.billingAddress = billingAddress
        self.cardNumber = cardNumber
        self.cardType = cardType
        self.defaultPayment = defaultPayment
        self.expiryMonth = expiryMonth
        self.expiryYear = expiryYear
        self.id = id
        self.issueNumber = issueNumber
        self.saved = saved
        self.startMonth = startMonth
        self.startYear = startYear
        self.subscriptionID = subscriptionID
    }
}

// MARK: PaymentDetailsResponse convenience initializers and mutators

public extension PaymentDetailsResponse {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(PaymentDetailsResponse.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        accountHolderName: String?? = nil,
        billingAddress: PaymentDetailsResponseBillingAddress?? = nil,
        cardNumber: String?? = nil,
        cardType: PaymentDetailsResponseCardType?? = nil,
        defaultPayment: Bool?? = nil,
        expiryMonth: String?? = nil,
        expiryYear: String?? = nil,
        id: String?? = nil,
        issueNumber: String?? = nil,
        saved: Bool?? = nil,
        startMonth: String?? = nil,
        startYear: String?? = nil,
        subscriptionID: String?? = nil
    ) -> PaymentDetailsResponse {
        return PaymentDetailsResponse(
            accountHolderName: accountHolderName ?? self.accountHolderName,
            billingAddress: billingAddress ?? self.billingAddress,
            cardNumber: cardNumber ?? self.cardNumber,
            cardType: cardType ?? self.cardType,
            defaultPayment: defaultPayment ?? self.defaultPayment,
            expiryMonth: expiryMonth ?? self.expiryMonth,
            expiryYear: expiryYear ?? self.expiryYear,
            id: id ?? self.id,
            issueNumber: issueNumber ?? self.issueNumber,
            saved: saved ?? self.saved,
            startMonth: startMonth ?? self.startMonth,
            startYear: startYear ?? self.startYear,
            subscriptionID: subscriptionID ?? self.subscriptionID
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - PaymentDetailsResponseBillingAddress
public struct PaymentDetailsResponseBillingAddress: Codable {
    public let addressName: String?
    public let area: PaymentDetailsResponseCardType?
    public let cellphone: String?
    public let city: PaymentDetailsResponseCity?
    public let companyName: String?
    public let country: PaymentDetailsResponseCountry?
    public let defaultAddress: Bool?
    public let district, email, firstName, formattedAddress: String?
    public let id, lastName, line1, line2: String?
    public let mobileCountry: PaymentDetailsResponseCountry?
    public let mobileNumber, nearestLandmark, phone, postalCode: String?
    public let region: PaymentDetailsResponseRegion?
    public let shippingAddress: Bool?
    public let title, titleCode, town: String?
    public let visibleInAddressBook: Bool?

    public init(addressName: String?, area: PaymentDetailsResponseCardType?, cellphone: String?, city: PaymentDetailsResponseCity?, companyName: String?, country: PaymentDetailsResponseCountry?, defaultAddress: Bool?, district: String?, email: String?, firstName: String?, formattedAddress: String?, id: String?, lastName: String?, line1: String?, line2: String?, mobileCountry: PaymentDetailsResponseCountry?, mobileNumber: String?, nearestLandmark: String?, phone: String?, postalCode: String?, region: PaymentDetailsResponseRegion?, shippingAddress: Bool?, title: String?, titleCode: String?, town: String?, visibleInAddressBook: Bool?) {
        self.addressName = addressName
        self.area = area
        self.cellphone = cellphone
        self.city = city
        self.companyName = companyName
        self.country = country
        self.defaultAddress = defaultAddress
        self.district = district
        self.email = email
        self.firstName = firstName
        self.formattedAddress = formattedAddress
        self.id = id
        self.lastName = lastName
        self.line1 = line1
        self.line2 = line2
        self.mobileCountry = mobileCountry
        self.mobileNumber = mobileNumber
        self.nearestLandmark = nearestLandmark
        self.phone = phone
        self.postalCode = postalCode
        self.region = region
        self.shippingAddress = shippingAddress
        self.title = title
        self.titleCode = titleCode
        self.town = town
        self.visibleInAddressBook = visibleInAddressBook
    }
}

// MARK: PaymentDetailsResponseBillingAddress convenience initializers and mutators

public extension PaymentDetailsResponseBillingAddress {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(PaymentDetailsResponseBillingAddress.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        addressName: String?? = nil,
        area: PaymentDetailsResponseCardType?? = nil,
        cellphone: String?? = nil,
        city: PaymentDetailsResponseCity?? = nil,
        companyName: String?? = nil,
        country: PaymentDetailsResponseCountry?? = nil,
        defaultAddress: Bool?? = nil,
        district: String?? = nil,
        email: String?? = nil,
        firstName: String?? = nil,
        formattedAddress: String?? = nil,
        id: String?? = nil,
        lastName: String?? = nil,
        line1: String?? = nil,
        line2: String?? = nil,
        mobileCountry: PaymentDetailsResponseCountry?? = nil,
        mobileNumber: String?? = nil,
        nearestLandmark: String?? = nil,
        phone: String?? = nil,
        postalCode: String?? = nil,
        region: PaymentDetailsResponseRegion?? = nil,
        shippingAddress: Bool?? = nil,
        title: String?? = nil,
        titleCode: String?? = nil,
        town: String?? = nil,
        visibleInAddressBook: Bool?? = nil
    ) -> PaymentDetailsResponseBillingAddress {
        return PaymentDetailsResponseBillingAddress(
            addressName: addressName ?? self.addressName,
            area: area ?? self.area,
            cellphone: cellphone ?? self.cellphone,
            city: city ?? self.city,
            companyName: companyName ?? self.companyName,
            country: country ?? self.country,
            defaultAddress: defaultAddress ?? self.defaultAddress,
            district: district ?? self.district,
            email: email ?? self.email,
            firstName: firstName ?? self.firstName,
            formattedAddress: formattedAddress ?? self.formattedAddress,
            id: id ?? self.id,
            lastName: lastName ?? self.lastName,
            line1: line1 ?? self.line1,
            line2: line2 ?? self.line2,
            mobileCountry: mobileCountry ?? self.mobileCountry,
            mobileNumber: mobileNumber ?? self.mobileNumber,
            nearestLandmark: nearestLandmark ?? self.nearestLandmark,
            phone: phone ?? self.phone,
            postalCode: postalCode ?? self.postalCode,
            region: region ?? self.region,
            shippingAddress: shippingAddress ?? self.shippingAddress,
            title: title ?? self.title,
            titleCode: titleCode ?? self.titleCode,
            town: town ?? self.town,
            visibleInAddressBook: visibleInAddressBook ?? self.visibleInAddressBook
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - PaymentDetailsResponseCardType
public struct PaymentDetailsResponseCardType: Codable {
    public let code, name: String?

    public init(code: String?, name: String?) {
        self.code = code
        self.name = name
    }
}

// MARK: PaymentDetailsResponseCardType convenience initializers and mutators

public extension PaymentDetailsResponseCardType {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(PaymentDetailsResponseCardType.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        code: String?? = nil,
        name: String?? = nil
    ) -> PaymentDetailsResponseCardType {
        return PaymentDetailsResponseCardType(
            code: code ?? self.code,
            name: name ?? self.name
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - PaymentDetailsResponseCity
public struct PaymentDetailsResponseCity: Codable {
    public let areas: [PaymentDetailsResponseCardType]?
    public let code, name: String?

    public init(areas: [PaymentDetailsResponseCardType]?, code: String?, name: String?) {
        self.areas = areas
        self.code = code
        self.name = name
    }
}

// MARK: PaymentDetailsResponseCity convenience initializers and mutators

public extension PaymentDetailsResponseCity {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(PaymentDetailsResponseCity.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        areas: [PaymentDetailsResponseCardType]?? = nil,
        code: String?? = nil,
        name: String?? = nil
    ) -> PaymentDetailsResponseCity {
        return PaymentDetailsResponseCity(
            areas: areas ?? self.areas,
            code: code ?? self.code,
            name: name ?? self.name
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - PaymentDetailsResponseCountry
public struct PaymentDetailsResponseCountry: Codable {
    public let isdcode, isocode, name: String?

    public init(isdcode: String?, isocode: String?, name: String?) {
        self.isdcode = isdcode
        self.isocode = isocode
        self.name = name
    }
}

// MARK: PaymentDetailsResponseCountry convenience initializers and mutators

public extension PaymentDetailsResponseCountry {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(PaymentDetailsResponseCountry.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        isdcode: String?? = nil,
        isocode: String?? = nil,
        name: String?? = nil
    ) -> PaymentDetailsResponseCountry {
        return PaymentDetailsResponseCountry(
            isdcode: isdcode ?? self.isdcode,
            isocode: isocode ?? self.isocode,
            name: name ?? self.name
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - PaymentDetailsResponseRegion
public struct PaymentDetailsResponseRegion: Codable {
    public let countryISO, isocode, isocodeShort, name: String?

    enum CodingKeys: String, CodingKey {
        case countryISO = "countryIso"
        case isocode, isocodeShort, name
    }

    public init(countryISO: String?, isocode: String?, isocodeShort: String?, name: String?) {
        self.countryISO = countryISO
        self.isocode = isocode
        self.isocodeShort = isocodeShort
        self.name = name
    }
}

// MARK: PaymentDetailsResponseRegion convenience initializers and mutators

public extension PaymentDetailsResponseRegion {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(PaymentDetailsResponseRegion.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        countryISO: String?? = nil,
        isocode: String?? = nil,
        isocodeShort: String?? = nil,
        name: String?? = nil
    ) -> PaymentDetailsResponseRegion {
        return PaymentDetailsResponseRegion(
            countryISO: countryISO ?? self.countryISO,
            isocode: isocode ?? self.isocode,
            isocodeShort: isocodeShort ?? self.isocodeShort,
            name: name ?? self.name
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
