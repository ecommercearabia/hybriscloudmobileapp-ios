// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let paymentDetailsRequest = try? newJSONDecoder().decode(PaymentDetailsRequest.self, from: jsonData)

import Foundation

// MARK: - PaymentDetailsRequest
public class PaymentDetailsRequest:Codable {
    public let accountHolderName: String?
    public let billingAddress: Address?
    public let cardNumber: String?
    public let cardType: CardType?
    public let defaultPayment: Bool?
    public let expiryMonth: String?
    public let expiryYear: String?
    public let id: String?
    public let issueNumber: String?
    public let saved: Bool?
    public let startMonth: String?
    public let startYear: String?
    public let subscriptionID: String?

    public init(accountHolderName: String?, billingAddress: Address?, cardNumber: String?, cardType: CardType?, defaultPayment: Bool?, expiryMonth: String?, expiryYear: String?, id: String?, issueNumber: String?, saved: Bool?, startMonth: String?, startYear: String?, subscriptionID: String?) {
        self.accountHolderName = accountHolderName
        self.billingAddress = billingAddress
        self.cardNumber = cardNumber
        self.cardType = cardType
        self.defaultPayment = defaultPayment
        self.expiryMonth = expiryMonth
        self.expiryYear = expiryYear
        self.id = id
        self.issueNumber = issueNumber
        self.saved = saved
        self.startMonth = startMonth
        self.startYear = startYear
        self.subscriptionID = subscriptionID
    }
}


// MARK: - CardType
public class CardType : Codable{
    public let code: String?
    public let name: String?

    public init(code: String?, name: String?) {
        self.code = code
        self.name = name
    }

}
