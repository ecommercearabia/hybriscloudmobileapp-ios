//
//  PaymentDetailsModel.swift
//  Domain
//
//  Created by Saja Hammad on 8/18/20.
//  Copyright © 2020 Erabia. All rights reserved.
//



import Foundation

public struct PaymentDetailsModel: Codable {
    public let cartId: String?
    public let mobileNumber: String?
    public let otpCode: String?
    
    public init( cartId: String?, mobileNumber: String?, otpCode: String?) {
        self.cartId = cartId
        self.mobileNumber = mobileNumber
        self.otpCode = otpCode
    }
    public enum CodingKeys: String, CodingKey {
        case cartId = "cartId"
        case mobileNumber = "mobileNumber"
        case otpCode = "code"
        
    }
    
}
