//
//  PaymentInfo.swift
//  Domain
//
//  Created by Ahmad Bader on 8/17/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let paymentInfo = try? newJSONDecoder().decode(PaymentInfo.self, from: jsonData)

import Foundation

// MARK: - PaymentInfo
public class PaymentInfo : Codable ,Identifiable{
    public var identity: String{
        get{
            return orderId ?? ""
        }
    }
    
    public let accessCode: String?
    public let amount: String?
    public let cancelURL: String?
    public let currency: String?
    public let customerIdentifier: String?
    public let deliveryAddress: String?
    public let deliveryCity: String?
    public let deliveryCountry: String?
    public let deliveryName: String?
    public let deliveryState: String?
    public let deliveryTel: String?
    public let deliveryZip: String?
    public let integrationType: String?
    public let language: String?
    public let merchantId: String?
    public let merchantParam1: String?
    public let merchantParam2: String?
    public let merchantParam3: String?
    public let merchantParam4: String?
    public let orderId: String?
    public let promoCode: String?
    public let redirectURL: String?
    public let workingKey: String?

    public init(accessCode: String?, amount: String?, cancelURL: String?, currency: String?, customerIdentifier: String?, deliveryAddress: String?, deliveryCity: String?, deliveryCountry: String?, deliveryName: String?, deliveryState: String?, deliveryTel: String?, deliveryZip: String?, integrationType: String?, language: String?, merchantId: String?, merchantParam1: String?, merchantParam2: String?, merchantParam3: String?, merchantParam4: String?, orderId: String?, promoCode: String?, redirectURL: String?, workingKey: String?) {
        self.accessCode = accessCode
        self.amount = amount
        self.cancelURL = cancelURL
        self.currency = currency
        self.customerIdentifier = customerIdentifier
        self.deliveryAddress = deliveryAddress
        self.deliveryCity = deliveryCity
        self.deliveryCountry = deliveryCountry
        self.deliveryName = deliveryName
        self.deliveryState = deliveryState
        self.deliveryTel = deliveryTel
        self.deliveryZip = deliveryZip
        self.integrationType = integrationType
        self.language = language
        self.merchantId = merchantId
        self.merchantParam1 = merchantParam1
        self.merchantParam2 = merchantParam2
        self.merchantParam3 = merchantParam3
        self.merchantParam4 = merchantParam4
        self.orderId = orderId
        self.promoCode = promoCode
        self.redirectURL = redirectURL
        self.workingKey = workingKey
    }
}
