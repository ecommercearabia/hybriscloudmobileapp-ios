
//
//  OrderDetails.swift
//  Domain
//
//  Created by khalil on 8/12/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let myOrdersDetails = try? newJSONDecoder().decode(MyOrdersDetails.self, from: jsonData)

import Foundation


// MARK: - MyOrdersDetails
public struct ordersDetailsMyOrdersDetails: Codable , Identifiable {
    public var identity: String{
        return code ?? ""
    }

        public let type: String?
        public let appliedOrderPromotions: [JSONAny]?
        public let appliedProductPromotions: [ordersDetailsAppliedProductPromotion]?
        public let appliedVouchers: [JSONAny]?
        public let calculated: Bool?
        public let code: String?
        public let deliveryAddress: ordersDetailsDeliveryAddress?
        public let deliveryCost: ordersDetailsDeliveryCost?
        public let deliveryItemsQuantity: Int?
        public let deliveryMode: ordersDetailsMode?
        public let deliveryOrderGroups: [ordersDetailsDeliveryOrderGroup]?
        public let entries: [ordersDetailsEntry]?
        public let guid: String?
        public let net: Bool?
        public let orderDiscounts: ordersDetailsDeliveryCost?
        public let paymentMode: ordersDetailsMode?
        public let pickupItemsQuantity: Int?
        public let pickupOrderGroups: [JSONAny]?
        public let productDiscounts: ordersDetailsDeliveryCost?
        public let site, store: String?
        public let storeCreditAmount: ordersDetailsDeliveryCost?
        public let storeCreditAmountSelected: Double?
        public let subTotal: ordersDetailsDeliveryCost?
        public let timeSlotInfoData: ordersDetailsTimeSlotInfoData?
        public let totalDiscounts: ordersDetailsDeliveryCost?
        public let totalItems: Int?
        public let totalPrice, totalPriceWithTax, totalTax: ordersDetailsDeliveryCost?
        public let user: ordersDetailsUser?
        public let consignments: [JSONAny]?
        public let created: String?
        public let guestCustomer: Bool?
        public let status, statusDisplay: String?
        public let unconsignedEntries: [ordersDetailsEntry]?

        public init(type: String?, appliedOrderPromotions: [JSONAny]?, appliedProductPromotions: [ordersDetailsAppliedProductPromotion]?, appliedVouchers: [JSONAny]?, calculated: Bool?, code: String?, deliveryAddress: ordersDetailsDeliveryAddress?, deliveryCost: ordersDetailsDeliveryCost?, deliveryItemsQuantity: Int?, deliveryMode: ordersDetailsMode?, deliveryOrderGroups: [ordersDetailsDeliveryOrderGroup]?, entries: [ordersDetailsEntry]?, guid: String?, net: Bool?, orderDiscounts: ordersDetailsDeliveryCost?, paymentMode: ordersDetailsMode?, pickupItemsQuantity: Int?, pickupOrderGroups: [JSONAny]?, productDiscounts: ordersDetailsDeliveryCost?, site: String?, store: String?, storeCreditAmount: ordersDetailsDeliveryCost?, storeCreditAmountSelected: Double?, subTotal: ordersDetailsDeliveryCost?, timeSlotInfoData: ordersDetailsTimeSlotInfoData?, totalDiscounts: ordersDetailsDeliveryCost?, totalItems: Int?, totalPrice: ordersDetailsDeliveryCost?, totalPriceWithTax: ordersDetailsDeliveryCost?, totalTax: ordersDetailsDeliveryCost?, user: ordersDetailsUser?, consignments: [JSONAny]?, created: String?, guestCustomer: Bool?, status: String?, statusDisplay: String?, unconsignedEntries: [ordersDetailsEntry]?) {
            self.type = type
            self.appliedOrderPromotions = appliedOrderPromotions
            self.appliedProductPromotions = appliedProductPromotions
            self.appliedVouchers = appliedVouchers
            self.calculated = calculated
            self.code = code
            self.deliveryAddress = deliveryAddress
            self.deliveryCost = deliveryCost
            self.deliveryItemsQuantity = deliveryItemsQuantity
            self.deliveryMode = deliveryMode
            self.deliveryOrderGroups = deliveryOrderGroups
            self.entries = entries
            self.guid = guid
            self.net = net
            self.orderDiscounts = orderDiscounts
            self.paymentMode = paymentMode
            self.pickupItemsQuantity = pickupItemsQuantity
            self.pickupOrderGroups = pickupOrderGroups
            self.productDiscounts = productDiscounts
            self.site = site
            self.store = store
            self.storeCreditAmount = storeCreditAmount
            self.storeCreditAmountSelected = storeCreditAmountSelected
            self.subTotal = subTotal
            self.timeSlotInfoData = timeSlotInfoData
            self.totalDiscounts = totalDiscounts
            self.totalItems = totalItems
            self.totalPrice = totalPrice
            self.totalPriceWithTax = totalPriceWithTax
            self.totalTax = totalTax
            self.user = user
            self.consignments = consignments
            self.created = created
            self.guestCustomer = guestCustomer
            self.status = status
            self.statusDisplay = statusDisplay
            self.unconsignedEntries = unconsignedEntries
        }
    }

    // MARK: - ordersDetailsAppliedProductPromotion
    public struct ordersDetailsAppliedProductPromotion: Codable {
        public let consumedEntries: [ordersDetailsConsumedEntry]?
        public let appliedProductPromotionDescription: String?
        public let promotion: ordersDetailsPromotion?

        enum CodingKeys: String, CodingKey {
            case consumedEntries
            case appliedProductPromotionDescription = "description"
            case promotion
        }

        public init(consumedEntries: [ordersDetailsConsumedEntry]?, appliedProductPromotionDescription: String?, promotion: ordersDetailsPromotion?) {
            self.consumedEntries = consumedEntries
            self.appliedProductPromotionDescription = appliedProductPromotionDescription
            self.promotion = promotion
        }
    }

    // MARK: - ordersDetailsConsumedEntry
    public struct ordersDetailsConsumedEntry: Codable {
        public let adjustedUnitPrice: Double?
        public let orderEntryNumber, quantity: Int?

        public init(adjustedUnitPrice: Double?, orderEntryNumber: Int?, quantity: Int?) {
            self.adjustedUnitPrice = adjustedUnitPrice
            self.orderEntryNumber = orderEntryNumber
            self.quantity = quantity
        }
    }

    // MARK: - ordersDetailsPromotion
    public struct ordersDetailsPromotion: Codable {
        public let code, promotionDescription, promotionType: String?

        enum CodingKeys: String, CodingKey {
            case code
            case promotionDescription = "description"
            case promotionType
        }

        public init(code: String?, promotionDescription: String?, promotionType: String?) {
            self.code = code
            self.promotionDescription = promotionDescription
            self.promotionType = promotionType
        }
    }

    // MARK: - ordersDetailsDeliveryAddress
    public struct ordersDetailsDeliveryAddress: Codable {
        public let addressName: String?
        public let city: ordersDetailsCity?
        public let country: ordersDetailsCountry?
        public let defaultAddress: Bool?
        public let firstName, formattedAddress, id, lastName: String?
        public let line1, line2: String?
        public let mobileCountry: ordersDetailsMobileCountry?
        public let mobileNumber, nearestLandmark: String?
        public let shippingAddress: Bool?
        public let title, titleCode, town: String?
        public let visibleInAddressBook: Bool?

        public init(addressName: String?, city: ordersDetailsCity?, country: ordersDetailsCountry?, defaultAddress: Bool?, firstName: String?, formattedAddress: String?, id: String?, lastName: String?, line1: String?, line2: String?, mobileCountry: ordersDetailsMobileCountry?, mobileNumber: String?, nearestLandmark: String?, shippingAddress: Bool?, title: String?, titleCode: String?, town: String?, visibleInAddressBook: Bool?) {
            self.addressName = addressName
            self.city = city
            self.country = country
            self.defaultAddress = defaultAddress
            self.firstName = firstName
            self.formattedAddress = formattedAddress
            self.id = id
            self.lastName = lastName
            self.line1 = line1
            self.line2 = line2
            self.mobileCountry = mobileCountry
            self.mobileNumber = mobileNumber
            self.nearestLandmark = nearestLandmark
            self.shippingAddress = shippingAddress
            self.title = title
            self.titleCode = titleCode
            self.town = town
            self.visibleInAddressBook = visibleInAddressBook
        }
    }

    // MARK: - ordersDetailsCity
    public struct ordersDetailsCity: Codable {
        public let areas: [ordersDetailsArea]?
        public let code, name: String?

        public init(areas: [ordersDetailsArea]?, code: String?, name: String?) {
            self.areas = areas
            self.code = code
            self.name = name
        }
    }

    // MARK: - ordersDetailsArea
    public struct ordersDetailsArea: Codable {
        public let code, name: String?

        public init(code: String?, name: String?) {
            self.code = code
            self.name = name
        }
    }

    // MARK: - ordersDetailsCountry
    public struct ordersDetailsCountry: Codable {
        public let isocode, name: String?

        public init(isocode: String?, name: String?) {
            self.isocode = isocode
            self.name = name
        }
    }

    // MARK: - ordersDetailsMobileCountry
    public struct ordersDetailsMobileCountry: Codable {
        public let isdcode, isocode, name: String?

        public init(isdcode: String?, isocode: String?, name: String?) {
            self.isdcode = isdcode
            self.isocode = isocode
            self.name = name
        }
    }

    // MARK: - ordersDetailsDeliveryCost
    public struct ordersDetailsDeliveryCost: Codable {
        public let currencyISO, formattedValue, priceType: String?
        public let value: Double?

        enum CodingKeys: String, CodingKey {
            case currencyISO = "currencyIso"
            case formattedValue, priceType, value
        }

        public init(currencyISO: String?, formattedValue: String?, priceType: String?, value: Double?) {
            self.currencyISO = currencyISO
            self.formattedValue = formattedValue
            self.priceType = priceType
            self.value = value
        }
    }

    // MARK: - ordersDetailsMode
    public struct ordersDetailsMode: Codable {
        public let code: String?
        public let deliveryCost: ordersDetailsDeliveryCost?
        public let modeDescription, name: String?

        enum CodingKeys: String, CodingKey {
            case code, deliveryCost
            case modeDescription = "description"
            case name
        }

        public init(code: String?, deliveryCost: ordersDetailsDeliveryCost?, modeDescription: String?, name: String?) {
            self.code = code
            self.deliveryCost = deliveryCost
            self.modeDescription = modeDescription
            self.name = name
        }
    }

    // MARK: - ordersDetailsDeliveryOrderGroup
    public struct ordersDetailsDeliveryOrderGroup: Codable {
        public let entries: [ordersDetailsEntry]?
        public let totalPriceWithTax: ordersDetailsDeliveryCost?

        public init(entries: [ordersDetailsEntry]?, totalPriceWithTax: ordersDetailsDeliveryCost?) {
            self.entries = entries
            self.totalPriceWithTax = totalPriceWithTax
        }
    }

    // MARK: - ordersDetailsEntry
    public struct ordersDetailsEntry: Codable {
        public let basePrice: ordersDetailsDeliveryCost?
        public let configurationInfos: [JSONAny]?
        public let entryNumber: Int?
        public let product: ProductDetails?
        public let quantity: Int?
        public let totalPrice: ordersDetailsDeliveryCost?
        public let updateable: Bool?

        public init(basePrice: ordersDetailsDeliveryCost?, configurationInfos: [JSONAny]?, entryNumber: Int?, product: ProductDetails?, quantity: Int?, totalPrice: ordersDetailsDeliveryCost?, updateable: Bool?) {
            self.basePrice = basePrice
            self.configurationInfos = configurationInfos
            self.entryNumber = entryNumber
            self.product = product
            self.quantity = quantity
            self.totalPrice = totalPrice
            self.updateable = updateable
        }
    }

    // MARK: - ordersDetailsProduct
    public struct ordersDetailsProduct: Codable {
        public let availableForPickup: Bool?
        public let baseOptions: [ordersDetailsBaseOption]?
        public let baseProduct: String?
        public let categories: [ordersDetailsCategory]?
        public let code: String?
        public let configurable: Bool?
        public let images: [ordersDetailsImageElement]?
        public let name: String?
        public let purchasable: Bool?
        public let stock: ordersDetailsStock?
        public let url: String?

        public init(availableForPickup: Bool?, baseOptions: [ordersDetailsBaseOption]?, baseProduct: String?, categories: [ordersDetailsCategory]?, code: String?, configurable: Bool?, images: [ordersDetailsImageElement]?, name: String?, purchasable: Bool?, stock: ordersDetailsStock?, url: String?) {
            self.availableForPickup = availableForPickup
            self.baseOptions = baseOptions
            self.baseProduct = baseProduct
            self.categories = categories
            self.code = code
            self.configurable = configurable
            self.images = images
            self.name = name
            self.purchasable = purchasable
            self.stock = stock
            self.url = url
        }
    }

    // MARK: - ordersDetailsBaseOption
    public struct ordersDetailsBaseOption: Codable {
        public let selected: ordersDetailsSelected?
        public let variantType: String?

        public init(selected: ordersDetailsSelected?, variantType: String?) {
            self.selected = selected
            self.variantType = variantType
        }
    }

    // MARK: - ordersDetailsSelected
    public struct ordersDetailsSelected: Codable {
        public let code: String?
        public let priceData: ordersDetailsDeliveryCost?
        public let stock: ordersDetailsStock?
        public let url: String?
        public let variantOptionQualifiers: [ordersDetailsVariantOptionQualifier]?

        public init(code: String?, priceData: ordersDetailsDeliveryCost?, stock: ordersDetailsStock?, url: String?, variantOptionQualifiers: [ordersDetailsVariantOptionQualifier]?) {
            self.code = code
            self.priceData = priceData
            self.stock = stock
            self.url = url
            self.variantOptionQualifiers = variantOptionQualifiers
        }
    }

    // MARK: - ordersDetailsStock
    public struct ordersDetailsStock: Codable {
        public let stockLevel: Int?
        public let stockLevelStatus: String?

        public init(stockLevel: Int?, stockLevelStatus: String?) {
            self.stockLevel = stockLevel
            self.stockLevelStatus = stockLevelStatus
        }
    }

    // MARK: - ordersDetailsVariantOptionQualifier
    public struct ordersDetailsVariantOptionQualifier: Codable {
        public let name, qualifier, value: String?

        public init(name: String?, qualifier: String?, value: String?) {
            self.name = name
            self.qualifier = qualifier
            self.value = value
        }
    }

    // MARK: - ordersDetailsCategory
    public struct ordersDetailsCategory: Codable {
        public let code: String?
        public let image: ordersDetailsCategoryImage?
        public let name, url: String?

        public init(code: String?, image: ordersDetailsCategoryImage?, name: String?, url: String?) {
            self.code = code
            self.image = image
            self.name = name
            self.url = url
        }
    }

    // MARK: - ordersDetailsCategoryImage
    public struct ordersDetailsCategoryImage: Codable {
        public let format, url: String?

        public init(format: String?, url: String?) {
            self.format = format
            self.url = url
        }
    }

    // MARK: - ordersDetailsImageElement
    public struct ordersDetailsImageElement: Codable {
        public let altText, format, imageType, url: String?

        public init(altText: String?, format: String?, imageType: String?, url: String?) {
            self.altText = altText
            self.format = format
            self.imageType = imageType
            self.url = url
        }
    }

    // MARK: - ordersDetailsTimeSlotInfoData
    public struct ordersDetailsTimeSlotInfoData: Codable {
        public let date, day, end, periodCode: String?
        public let start: String?

        public init(date: String?, day: String?, end: String?, periodCode: String?, start: String?) {
            self.date = date
            self.day = day
            self.end = end
            self.periodCode = periodCode
            self.start = start
        }
    }

    // MARK: - ordersDetailsUser
    public struct ordersDetailsUser: Codable {
        public let name, uid: String?

        public init(name: String?, uid: String?) {
            self.name = name
            self.uid = uid
        }
    }

    // MARK: - Encode/decode helpers


   
