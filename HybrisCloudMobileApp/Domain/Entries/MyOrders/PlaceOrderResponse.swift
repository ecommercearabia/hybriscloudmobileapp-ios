// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let placeOrderResponseEmpty = try? newJSONDecoder().decode(PlaceOrderResponseEmpty.self, from: jsonData)

import Foundation

// MARK: - PlaceOrderResponseEmpty
public class PalceOrderResponse: Codable,Identifiable {
    public var identity: String{
        get{
            return code ?? ""
        }
    }
    
    
    public let type: String?
    public let appliedOrderPromotions: [JSONAny]?
    public let appliedProductPromotions: [JSONAny]?
    public let appliedVouchers: [JSONAny]?
    public let calculated: Bool?
    public let code: String?
    public let deliveryAddress: Address?
    public let deliveryCost: PlaceOrderResponseDeliveryCost?
    public let deliveryItemsQuantity: Int?
    public let deliveryMode: PlaceOrderResponseMode?
    public let deliveryOrderGroups: [PlaceOrderResponseDeliveryOrderGroup]?
    public let entries: [CartEntry]?
    public let guid: String?
    public let net: Bool?
    public let orderDiscounts: PlaceOrderResponseDeliveryCost?
    public let paymentMode: PlaceOrderResponseMode?
    public let pickupItemsQuantity: Int?
    public let pickupOrderGroups: [JSONAny]?
    public let productDiscounts: PlaceOrderResponseDeliveryCost?
    public let site: String?
    public let store: String?
    public let storeCreditAmount: PlaceOrderResponseDeliveryCost?
    public let storeCreditAmountSelected: Double?
    public let subTotal: PlaceOrderResponseDeliveryCost?
    public let timeSlotInfoData: PlaceOrderResponseTimeSlotInfoData?
    public let totalDiscounts: PlaceOrderResponseDeliveryCost?
    public let totalItems: Int?
    public let totalPrice: PlaceOrderResponseDeliveryCost?
    public let totalPriceWithTax: PlaceOrderResponseDeliveryCost?
    public let totalTax: PlaceOrderResponseDeliveryCost?
    public let user: PlaceOrderResponseUser?
    public let consignments: [JSONAny]?
    public let created: String?
    public let guestCustomer: Bool?
    public let statusDisplay: String?
    public let unconsignedEntries: [CartEntry]?
    public var shipmentType: CartShipmentType?

    enum CodingKeys: String, CodingKey {
        case type
        case appliedOrderPromotions
        case appliedProductPromotions
        case appliedVouchers
        case calculated
        case code
        case deliveryAddress
        case deliveryCost
        case deliveryItemsQuantity
        case deliveryMode
        case deliveryOrderGroups
        case entries
        case guid
        case net
        case orderDiscounts
        case paymentMode
        case pickupItemsQuantity
        case pickupOrderGroups
        case productDiscounts
        case site
        case store
        case storeCreditAmount
        case storeCreditAmountSelected
        case subTotal
        case timeSlotInfoData
        case totalDiscounts
        case totalItems
        case totalPrice
        case totalPriceWithTax
        case totalTax
        case user
        case consignments
        case created
        case guestCustomer
        case statusDisplay
        case unconsignedEntries
        case shipmentType
    }

    public init(type: String?, appliedOrderPromotions: [JSONAny]?, appliedProductPromotions: [JSONAny]?, appliedVouchers: [JSONAny]?, calculated: Bool?, code: String?, deliveryAddress: Address?, deliveryCost: PlaceOrderResponseDeliveryCost?, deliveryItemsQuantity: Int?, deliveryMode: PlaceOrderResponseMode?, deliveryOrderGroups: [PlaceOrderResponseDeliveryOrderGroup]?, entries: [CartEntry]?, guid: String?, net: Bool?, orderDiscounts: PlaceOrderResponseDeliveryCost?, paymentMode: PlaceOrderResponseMode?, pickupItemsQuantity: Int?, pickupOrderGroups: [JSONAny]?, productDiscounts: PlaceOrderResponseDeliveryCost?, site: String?, store: String?, storeCreditAmount: PlaceOrderResponseDeliveryCost?, storeCreditAmountSelected: Double?, subTotal: PlaceOrderResponseDeliveryCost?, timeSlotInfoData: PlaceOrderResponseTimeSlotInfoData?, totalDiscounts: PlaceOrderResponseDeliveryCost?, totalItems: Int?, totalPrice: PlaceOrderResponseDeliveryCost?, totalPriceWithTax: PlaceOrderResponseDeliveryCost?, totalTax: PlaceOrderResponseDeliveryCost?, user: PlaceOrderResponseUser?, consignments: [JSONAny]?, created: String?, guestCustomer: Bool?, statusDisplay: String?, unconsignedEntries: [CartEntry]? , shipmentType: CartShipmentType?) {
        self.type = type
        self.appliedOrderPromotions = appliedOrderPromotions
        self.appliedProductPromotions = appliedProductPromotions
        self.appliedVouchers = appliedVouchers
        self.calculated = calculated
        self.code = code
        self.deliveryAddress = deliveryAddress
        self.deliveryCost = deliveryCost
        self.deliveryItemsQuantity = deliveryItemsQuantity
        self.deliveryMode = deliveryMode
        self.deliveryOrderGroups = deliveryOrderGroups
        self.entries = entries
        self.guid = guid
        self.net = net
        self.orderDiscounts = orderDiscounts
        self.paymentMode = paymentMode
        self.pickupItemsQuantity = pickupItemsQuantity
        self.pickupOrderGroups = pickupOrderGroups
        self.productDiscounts = productDiscounts
        self.site = site
        self.store = store
        self.storeCreditAmount = storeCreditAmount
        self.storeCreditAmountSelected = storeCreditAmountSelected
        self.subTotal = subTotal
        self.timeSlotInfoData = timeSlotInfoData
        self.totalDiscounts = totalDiscounts
        self.totalItems = totalItems
        self.totalPrice = totalPrice
        self.totalPriceWithTax = totalPriceWithTax
        self.totalTax = totalTax
        self.user = user
        self.consignments = consignments
        self.created = created
        self.guestCustomer = guestCustomer
        self.statusDisplay = statusDisplay
        self.unconsignedEntries = unconsignedEntries
    }
}

// MARK: - PlaceOrderResponseArea
public class PlaceOrderResponseArea: Codable {
    public let code: String?
    public let name: String?

    enum CodingKeys: String, CodingKey {
        case code
        case name
    }

    public init(code: String?, name: String?) {
        self.code = code
        self.name = name
    }
}

// MARK: - PlaceOrderResponseCity
public class PlaceOrderResponseCity: Codable {
    public let areas: [PlaceOrderResponseArea]?
    public let code: String?
    public let name: String?

    enum CodingKeys: String, CodingKey {
        case areas
        case code
        case name
    }

    public init(areas: [PlaceOrderResponseArea]?, code: String?, name: String?) {
        self.areas = areas
        self.code = code
        self.name = name
    }
}

// MARK: - PlaceOrderResponseCountry
public class PlaceOrderResponseCountry: Codable {
    public let isocode: String?
    public let name: String?

    enum CodingKeys: String, CodingKey {
        case isocode
        case name
    }

    public init(isocode: String?, name: String?) {
        self.isocode = isocode
        self.name = name
    }
}

// MARK: - PlaceOrderResponseMobileCountry
public class PlaceOrderResponseMobileCountry: Codable {
    public let isdcode: String?
    public let isocode: String?
    public let name: String?

    enum CodingKeys: String, CodingKey {
        case isdcode
        case isocode
        case name
    }

    public init(isdcode: String?, isocode: String?, name: String?) {
        self.isdcode = isdcode
        self.isocode = isocode
        self.name = name
    }
}

// MARK: - PlaceOrderResponseDeliveryCost
public class PlaceOrderResponseDeliveryCost: Codable {
    public let currencyISO: String?
    public let formattedValue: String?
    public let priceType: String?
    public let value: Double?

    enum CodingKeys: String, CodingKey {
        case currencyISO
        case formattedValue
        case priceType
        case value
    }

    public init(currencyISO: String?, formattedValue: String?, priceType: String?, value: Double?) {
        self.currencyISO = currencyISO
        self.formattedValue = formattedValue
        self.priceType = priceType
        self.value = value
    }
}

// MARK: - PlaceOrderResponseMode
public class PlaceOrderResponseMode: Codable {
    public let code: String?
    public let deliveryCost: PlaceOrderResponseDeliveryCost?
    public let modeDescription: String?
    public let name: String?

    enum CodingKeys: String, CodingKey {
        case code
        case deliveryCost
        case modeDescription
        case name
    }

    public init(code: String?, deliveryCost: PlaceOrderResponseDeliveryCost?, modeDescription: String?, name: String?) {
        self.code = code
        self.deliveryCost = deliveryCost
        self.modeDescription = modeDescription
        self.name = name
    }
}

// MARK: - PlaceOrderResponseDeliveryOrderGroup
public class PlaceOrderResponseDeliveryOrderGroup: Codable {
    public let entries: [CartEntry]?
    public let totalPriceWithTax: PlaceOrderResponseDeliveryCost?

    enum CodingKeys: String, CodingKey {
        case entries
        case totalPriceWithTax
    }

    public init(entries: [CartEntry]?, totalPriceWithTax: PlaceOrderResponseDeliveryCost?) {
        self.entries = entries
        self.totalPriceWithTax = totalPriceWithTax
    }
}

//// MARK: - PlaceOrderResponseEntry
//public class PlaceOrderResponseEntry: Codable {
//    public let basePrice: PlaceOrderResponseDeliveryCost?
//    public let configurationInfos: [JSONAny]?
//    public let entryNumber: Int?
//    public let product: PlaceOrderResponseProduct?
//    public let quantity: Int?
//    public let totalPrice: PlaceOrderResponseDeliveryCost?
//    public let updateable: Bool?
//
//    enum CodingKeys: String, CodingKey {
//        case basePrice
//        case configurationInfos
//        case entryNumber
//        case product
//        case quantity
//        case totalPrice
//        case updateable
//    }
//
//    public init(basePrice: PlaceOrderResponseDeliveryCost?, configurationInfos: [JSONAny]?, entryNumber: Int?, product: PlaceOrderResponseProduct?, quantity: Int?, totalPrice: PlaceOrderResponseDeliveryCost?, updateable: Bool?) {
//        self.basePrice = basePrice
//        self.configurationInfos = configurationInfos
//        self.entryNumber = entryNumber
//        self.product = product
//        self.quantity = quantity
//        self.totalPrice = totalPrice
//        self.updateable = updateable
//    }
//}

// MARK: - PlaceOrderResponseProduct
public class PlaceOrderResponseProduct: Codable {
    public let availableForPickup: Bool?
    public let baseOptions: [PlaceOrderResponseBaseOption]?
    public let baseProduct: String?
    public let categories: [JSONAny]?
    public let code: String?
    public let configurable: Bool?
    public let countryOfOrigin: String?
    public let countryOfOriginIsocode: String?
    public let images: [PlaceOrderResponseImage]?
    public let name: String?
    public let purchasable: Bool?
    public let stock: PlaceOrderResponseStock?
    public let unitOfMeasure: String?
    public let url: String?

    enum CodingKeys: String, CodingKey {
        case availableForPickup
        case baseOptions
        case baseProduct
        case categories
        case code
        case configurable
        case countryOfOrigin
        case countryOfOriginIsocode
        case images
        case name
        case purchasable
        case stock
        case unitOfMeasure
        case url
    }

    public init(availableForPickup: Bool?, baseOptions: [PlaceOrderResponseBaseOption]?, baseProduct: String?, categories: [JSONAny]?, code: String?, configurable: Bool?, countryOfOrigin: String?, countryOfOriginIsocode: String?, images: [PlaceOrderResponseImage]?, name: String?, purchasable: Bool?, stock: PlaceOrderResponseStock?, unitOfMeasure: String?, url: String?) {
        self.availableForPickup = availableForPickup
        self.baseOptions = baseOptions
        self.baseProduct = baseProduct
        self.categories = categories
        self.code = code
        self.configurable = configurable
        self.countryOfOrigin = countryOfOrigin
        self.countryOfOriginIsocode = countryOfOriginIsocode
        self.images = images
        self.name = name
        self.purchasable = purchasable
        self.stock = stock
        self.unitOfMeasure = unitOfMeasure
        self.url = url
    }
}

// MARK: - PlaceOrderResponseBaseOption
public class PlaceOrderResponseBaseOption: Codable {
    public let selected: PlaceOrderResponseSelected?
    public let variantType: String?

    enum CodingKeys: String, CodingKey {
        case selected
        case variantType
    }

    public init(selected: PlaceOrderResponseSelected?, variantType: String?) {
        self.selected = selected
        self.variantType = variantType
    }
}

// MARK: - PlaceOrderResponseSelected
public class PlaceOrderResponseSelected: Codable {
    public let code: String?
    public let priceData: PlaceOrderResponseDeliveryCost?
    public let stock: PlaceOrderResponseStock?
    public let url: String?
    public let variantOptionQualifiers: [PlaceOrderResponseVariantOptionQualifier]?

    enum CodingKeys: String, CodingKey {
        case code
        case priceData
        case stock
        case url
        case variantOptionQualifiers
    }

    public init(code: String?, priceData: PlaceOrderResponseDeliveryCost?, stock: PlaceOrderResponseStock?, url: String?, variantOptionQualifiers: [PlaceOrderResponseVariantOptionQualifier]?) {
        self.code = code
        self.priceData = priceData
        self.stock = stock
        self.url = url
        self.variantOptionQualifiers = variantOptionQualifiers
    }
}

// MARK: - PlaceOrderResponseStock
public class PlaceOrderResponseStock: Codable {
    public let stockLevel: Int?
    public let stockLevelStatus: String?

    enum CodingKeys: String, CodingKey {
        case stockLevel
        case stockLevelStatus
    }

    public init(stockLevel: Int?, stockLevelStatus: String?) {
        self.stockLevel = stockLevel
        self.stockLevelStatus = stockLevelStatus
    }
}

// MARK: - PlaceOrderResponseVariantOptionQualifier
public class PlaceOrderResponseVariantOptionQualifier: Codable {
    public let name: String?
    public let qualifier: String?
    public let value: String?

    enum CodingKeys: String, CodingKey {
        case name
        case qualifier
        case value
    }

    public init(name: String?, qualifier: String?, value: String?) {
        self.name = name
        self.qualifier = qualifier
        self.value = value
    }
}

// MARK: - PlaceOrderResponseImage
public class PlaceOrderResponseImage: Codable {
    public let altText: String?
    public let format: String?
    public let imageType: String?
    public let url: String?

    enum CodingKeys: String, CodingKey {
        case altText
        case format
        case imageType
        case url
    }

    public init(altText: String?, format: String?, imageType: String?, url: String?) {
        self.altText = altText
        self.format = format
        self.imageType = imageType
        self.url = url
    }
}

// MARK: - PlaceOrderResponseTimeSlotInfoData
public class PlaceOrderResponseTimeSlotInfoData: Codable {
    public let date: String?
    public let day: String?
    public let end: String?
    public let periodCode: String?
    public let start: String?

    enum CodingKeys: String, CodingKey {
        case date
        case day
        case end
        case periodCode
        case start
    }

    public init(date: String?, day: String?, end: String?, periodCode: String?, start: String?) {
        self.date = date
        self.day = day
        self.end = end
        self.periodCode = periodCode
        self.start = start
    }
}

// MARK: - PlaceOrderResponseUser
public class PlaceOrderResponseUser: Codable {
    public let name: String?
    public let uid: String?

    enum CodingKeys: String, CodingKey {
        case name
        case uid
    }

    public init(name: String?, uid: String?) {
        self.name = name
        self.uid = uid
    }
}


