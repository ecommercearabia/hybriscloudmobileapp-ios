//
//  MyOrders.swift
//  Domain
//
//  Created by khalil on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// MARK: - MyOrders
public struct MyOrders: Codable ,Identifiable {
   public var identity: String{
    return orders?.first?.code ?? ""
      }
  

    // MARK: - MyOrders
    
        public let orders: [Order]?
        public let pagination: Pagination?
        public let sorts: [Sort]?

        public init(orders: [Order]?, pagination: Pagination?, sorts: [Sort]?) {
            self.orders = orders
            self.pagination = pagination
            self.sorts = sorts
        }
    }

    // MARK: - Order
    public struct Order: Codable {
        public let code, guid, placed, status: String?
        public let statusDisplay: String?
        public let total: Total?

        public init(code: String?, guid: String?, placed: String?, status: String?, statusDisplay: String?, total: Total?) {
            self.code = code
            self.guid = guid
            self.placed = placed
            self.status = status
            self.statusDisplay = statusDisplay
            self.total = total
        }
    }

    // MARK: - Total
    public struct Total: Codable {
        public let currencyISO, formattedValue, priceType: String?
        public let value: Double?

        enum CodingKeys: String, CodingKey {
            case currencyISO = "currencyIso"
            case formattedValue, priceType, value
        }

        public init(currencyISO: String?, formattedValue: String?, priceType: String?, value: Double?) {
            self.currencyISO = currencyISO
            self.formattedValue = formattedValue
            self.priceType = priceType
            self.value = value
        }
    }

    // MARK: - Pagination
    public struct Pagination: Codable {
        public let currentPage, pageSize: Int?
        public let sort: String?
        public let totalPages, totalResults: Int?

        public init(currentPage: Int?, pageSize: Int?, sort: String?, totalPages: Int?, totalResults: Int?) {
            self.currentPage = currentPage
            self.pageSize = pageSize
            self.sort = sort
            self.totalPages = totalPages
            self.totalResults = totalResults
        }
    }

    // MARK: - Sort
    public struct Sort: Codable {
        public let code: String?
        public let selected: Bool?

        public init(code: String?, selected: Bool?) {
            self.code = code
            self.selected = selected
        }
    }
