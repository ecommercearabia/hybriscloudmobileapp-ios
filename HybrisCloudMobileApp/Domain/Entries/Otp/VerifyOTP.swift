//
//  VerifyOTP.swift
//  Domain
//
//  Created by Saja Hammad on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation

// MARK: - VerifyOTP
public struct VerifyOTP: Codable ,Identifiable {
      
      public var identity: String{
         return verifyOTPDescription ?? ""
         }
    public let verifyOTPDescription: String?
    public let status: Bool?

    enum CodingKeys: String, CodingKey {
        case verifyOTPDescription = "description"
        case status
    }

    public init(verifyOTPDescription: String?, status: Bool?) {
        self.verifyOTPDescription = verifyOTPDescription
        self.status = status
    }
}

// MARK: VerifyOTP convenience initializers and mutators

public extension VerifyOTP {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(VerifyOTP.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        verifyOTPDescription: String?? = nil,
        status: Bool?? = nil
    ) -> VerifyOTP {
        return VerifyOTP(
            verifyOTPDescription: verifyOTPDescription ?? self.verifyOTPDescription,
            status: status ?? self.status
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

