//
//  SendOtpModel.swift
//  HybrisCloudMobileApp
//
//  Created by Saja Hammad on 8/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation

public struct SendOtpModel: Codable {
    public let isoCode: String?
    public let mobileNumber: String?
    public let otpCode: String?
    
    public init( isoCode: String?, mobileNumber: String?, otpCode: String?) {
        self.isoCode = isoCode
        self.mobileNumber = mobileNumber
        self.otpCode = otpCode
    }
    public enum CodingKeys: String, CodingKey {
        case isoCode = "countryisoCode"
        case mobileNumber = "mobileNumber"
        case otpCode = "code"
        
    }
    
}

