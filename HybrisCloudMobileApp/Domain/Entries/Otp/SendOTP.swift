//
//  SendOTP.swift
//  Domain
//
//  Created by Saja Hammad on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// MARK: - SendOTP
public struct SendOTP: Codable ,Identifiable {
       
       public var identity: String{
          return sendOTPDescription ?? ""
          }
    public let sendOTPDescription: String?
    public let status: Bool?

    enum CodingKeys: String, CodingKey {
        case sendOTPDescription = "description"
        case status
    }

    public init(sendOTPDescription: String?, status: Bool?) {
        self.sendOTPDescription = sendOTPDescription
        self.status = status
    }
}

// MARK: SendOTP convenience initializers and mutators

public extension SendOTP {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SendOTP.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        sendOTPDescription: String?? = nil,
        status: Bool?? = nil
    ) -> SendOTP {
        return SendOTP(
            sendOTPDescription: sendOTPDescription ?? self.sendOTPDescription,
            status: status ?? self.status
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
