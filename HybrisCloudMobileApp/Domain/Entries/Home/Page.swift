//
//  Post.swift
//  Domain
//
//  Created by Saja Hammad on 7/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let hybrisHomeResponse = try HybrisHomeResponse(json)

import Foundation

// MARK: - HybrisHomeResponse
public struct Page: Codable  ,Identifiable{

   public var identity: String{
       return uid ?? ""
   }
    public let uid, uuid, title, template: String?
    public let typeCode, name: String?
    public let defaultPage: Bool?
    public let contentSlots: HybrisContentSlots?
    public let label: String?

    public init(uid: String?, uuid: String?, title: String?, template: String?, typeCode: String?, name: String?, defaultPage: Bool?, contentSlots: HybrisContentSlots?, label: String?) {
        self.uid = uid
        self.uuid = uuid
        self.title = title
        self.template = template
        self.typeCode = typeCode
        self.name = name
        self.defaultPage = defaultPage
        self.contentSlots = contentSlots
        self.label = label
    }
}

// MARK: HybrisHomeResponse convenience initializers and mutators

public extension Page {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Page.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        uid: String?? = nil,
        uuid: String?? = nil,
        title: String?? = nil,
        template: String?? = nil,
        typeCode: String?? = nil,
        name: String?? = nil,
        defaultPage: Bool?? = nil,
        contentSlots: HybrisContentSlots?? = nil,
        label: String?? = nil
    ) -> Page {
        return Page(
            uid: uid ?? self.uid,
            uuid: uuid ?? self.uuid,
            title: title ?? self.title,
            template: template ?? self.template,
            typeCode: typeCode ?? self.typeCode,
            name: name ?? self.name,
            defaultPage: defaultPage ?? self.defaultPage,
            contentSlots: contentSlots ?? self.contentSlots,
            label: label ?? self.label
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - HybrisContentSlots
public struct HybrisContentSlots: Codable {
    public let contentSlot: [HybrisContentSlot]?

    public init(contentSlot: [HybrisContentSlot]?) {
        self.contentSlot = contentSlot
    }
}

// MARK: HybrisContentSlots convenience initializers and mutators

public extension HybrisContentSlots {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(HybrisContentSlots.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        contentSlot: [HybrisContentSlot]?? = nil
    ) -> HybrisContentSlots {
        return HybrisContentSlots(
            contentSlot: contentSlot ?? self.contentSlot
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - HybrisContentSlot
public struct HybrisContentSlot: Codable {
    public let slotID, slotUUID, position, name: String?
    public let slotShared: Bool?
    public let components: HybrisComponents?

    enum CodingKeys: String, CodingKey {
        case slotID = "slotId"
        case slotUUID = "slotUuid"
        case position, name, slotShared, components
    }

    public init(slotID: String?, slotUUID: String?, position: String?, name: String?, slotShared: Bool?, components: HybrisComponents?) {
        self.slotID = slotID
        self.slotUUID = slotUUID
        self.position = position
        self.name = name
        self.slotShared = slotShared
        self.components = components
    }
}

// MARK: HybrisContentSlot convenience initializers and mutators

public extension HybrisContentSlot {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(HybrisContentSlot.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        slotID: String?? = nil,
        slotUUID: String?? = nil,
        position: String?? = nil,
        name: String?? = nil,
        slotShared: Bool?? = nil,
        components: HybrisComponents?? = nil
    ) -> HybrisContentSlot {
        return HybrisContentSlot(
            slotID: slotID ?? self.slotID,
            slotUUID: slotUUID ?? self.slotUUID,
            position: position ?? self.position,
            name: name ?? self.name,
            slotShared: slotShared ?? self.slotShared,
            components: components ?? self.components
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - HybrisComponents
public struct HybrisComponents: Codable {
    public let component: [HybrisComponent]?

    public init(component: [HybrisComponent]?) {
        self.component = component
    }
}

// MARK: HybrisComponents convenience initializers and mutators

public extension HybrisComponents {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(HybrisComponents.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        component: [HybrisComponent]?? = nil
    ) -> HybrisComponents {
        return HybrisComponents(
            component: component ?? self.component
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - HybrisComponent
public struct HybrisComponent: Codable {
    public let uid, uuid, typeCode, modifiedTime: String?
    public let name, container: String?
    public let media: HybrisMedia?
    public let urlLink: String?
    public let productCarouselComponents, content, popup, scroll: String?
    public let productCodes, title, effect, displayNavigation: String?
    public let rotationSpeed, position, autoRotate, pauseInHover: String?
    public let banners, displayPagination, timeout, external: String?
    public let wrapAfter: String?
    public let navigationNode: HybrisNavigationNode?
    public let shownProductCount, totalDisplay, contentPage, contentPageLabelOrID: String?
    public let url, target, maxSuggestions, maxProducts: String?
    public let displaySuggestions, displayProducts, displayProductImages, waitTimeBeforeRequest: String?
    public let minCharactersBeforeRequest, page, showLanguageCurrency: String?

    enum CodingKeys: String, CodingKey {
        case uid, uuid, typeCode, modifiedTime, name, container, media, urlLink, productCarouselComponents, content, popup, scroll, productCodes, title, effect, displayNavigation, rotationSpeed, position, autoRotate, pauseInHover, banners, displayPagination, timeout, external, wrapAfter, navigationNode, shownProductCount, totalDisplay, contentPage
        case contentPageLabelOrID = "contentPageLabelOrId"
        case url, target, maxSuggestions, maxProducts, displaySuggestions, displayProducts, displayProductImages, waitTimeBeforeRequest, minCharactersBeforeRequest, page, showLanguageCurrency
    }

    public init(uid: String?, uuid: String?, typeCode: String?, modifiedTime: String?, name: String?, container: String?, media: HybrisMedia?, urlLink: String?, productCarouselComponents: String?, content: String?, popup: String?, scroll: String?, productCodes: String?, title: String?, effect: String?, displayNavigation: String?, rotationSpeed: String?, position: String?, autoRotate: String?, pauseInHover: String?, banners: String?, displayPagination: String?, timeout: String?, external: String?, wrapAfter: String?, navigationNode: HybrisNavigationNode?, shownProductCount: String?, totalDisplay: String?, contentPage: String?, contentPageLabelOrID: String?, url: String?, target: String?, maxSuggestions: String?, maxProducts: String?, displaySuggestions: String?, displayProducts: String?, displayProductImages: String?, waitTimeBeforeRequest: String?, minCharactersBeforeRequest: String?, page: String?, showLanguageCurrency: String?) {
        self.uid = uid
        self.uuid = uuid
        self.typeCode = typeCode
        self.modifiedTime = modifiedTime
        self.name = name
        self.container = container
        self.media = media
        self.urlLink = urlLink
        self.productCarouselComponents = productCarouselComponents
        self.content = content
        self.popup = popup
        self.scroll = scroll
        self.productCodes = productCodes
        self.title = title
        self.effect = effect
        self.displayNavigation = displayNavigation
        self.rotationSpeed = rotationSpeed
        self.position = position
        self.autoRotate = autoRotate
        self.pauseInHover = pauseInHover
        self.banners = banners
        self.displayPagination = displayPagination
        self.timeout = timeout
        self.external = external
        self.wrapAfter = wrapAfter
        self.navigationNode = navigationNode
        self.shownProductCount = shownProductCount
        self.totalDisplay = totalDisplay
        self.contentPage = contentPage
        self.contentPageLabelOrID = contentPageLabelOrID
        self.url = url
        self.target = target
        self.maxSuggestions = maxSuggestions
        self.maxProducts = maxProducts
        self.displaySuggestions = displaySuggestions
        self.displayProducts = displayProducts
        self.displayProductImages = displayProductImages
        self.waitTimeBeforeRequest = waitTimeBeforeRequest
        self.minCharactersBeforeRequest = minCharactersBeforeRequest
        self.page = page
        self.showLanguageCurrency = showLanguageCurrency
    }
}

// MARK: HybrisComponent convenience initializers and mutators

public extension HybrisComponent {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(HybrisComponent.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        uid: String?? = nil,
        uuid: String?? = nil,
        typeCode: String?? = nil,
        modifiedTime: String?? = nil,
        name: String?? = nil,
        container: String?? = nil,
        media: HybrisMedia?? = nil,
        urlLink: String?? = nil,
        productCarouselComponents: String?? = nil,
        content: String?? = nil,
        popup: String?? = nil,
        scroll: String?? = nil,
        productCodes: String?? = nil,
        title: String?? = nil,
        effect: String?? = nil,
        displayNavigation: String?? = nil,
        rotationSpeed: String?? = nil,
        position: String?? = nil,
        autoRotate: String?? = nil,
        pauseInHover: String?? = nil,
        banners: String?? = nil,
        displayPagination: String?? = nil,
        timeout: String?? = nil,
        external: String?? = nil,
        wrapAfter: String?? = nil,
        navigationNode: HybrisNavigationNode?? = nil,
        shownProductCount: String?? = nil,
        totalDisplay: String?? = nil,
        contentPage: String?? = nil,
        contentPageLabelOrID: String?? = nil,
        url: String?? = nil,
        target: String?? = nil,
        maxSuggestions: String?? = nil,
        maxProducts: String?? = nil,
        displaySuggestions: String?? = nil,
        displayProducts: String?? = nil,
        displayProductImages: String?? = nil,
        waitTimeBeforeRequest: String?? = nil,
        minCharactersBeforeRequest: String?? = nil,
        page: String?? = nil,
        showLanguageCurrency: String?? = nil
    ) -> HybrisComponent {
        return HybrisComponent(
            uid: uid ?? self.uid,
            uuid: uuid ?? self.uuid,
            typeCode: typeCode ?? self.typeCode,
            modifiedTime: modifiedTime ?? self.modifiedTime,
            name: name ?? self.name,
            container: container ?? self.container,
            media: media ?? self.media,
            urlLink: urlLink ?? self.urlLink,
            productCarouselComponents: productCarouselComponents ?? self.productCarouselComponents,
            content: content ?? self.content,
            popup: popup ?? self.popup,
            scroll: scroll ?? self.scroll,
            productCodes: productCodes ?? self.productCodes,
            title: title ?? self.title,
            effect: effect ?? self.effect,
            displayNavigation: displayNavigation ?? self.displayNavigation,
            rotationSpeed: rotationSpeed ?? self.rotationSpeed,
            position: position ?? self.position,
            autoRotate: autoRotate ?? self.autoRotate,
            pauseInHover: pauseInHover ?? self.pauseInHover,
            banners: banners ?? self.banners,
            displayPagination: displayPagination ?? self.displayPagination,
            timeout: timeout ?? self.timeout,
            external: external ?? self.external,
            wrapAfter: wrapAfter ?? self.wrapAfter,
            navigationNode: navigationNode ?? self.navigationNode,
            shownProductCount: shownProductCount ?? self.shownProductCount,
            totalDisplay: totalDisplay ?? self.totalDisplay,
            contentPage: contentPage ?? self.contentPage,
            contentPageLabelOrID: contentPageLabelOrID ?? self.contentPageLabelOrID,
            url: url ?? self.url,
            target: target ?? self.target,
            maxSuggestions: maxSuggestions ?? self.maxSuggestions,
            maxProducts: maxProducts ?? self.maxProducts,
            displaySuggestions: displaySuggestions ?? self.displaySuggestions,
            displayProducts: displayProducts ?? self.displayProducts,
            displayProductImages: displayProductImages ?? self.displayProductImages,
            waitTimeBeforeRequest: waitTimeBeforeRequest ?? self.waitTimeBeforeRequest,
            minCharactersBeforeRequest: minCharactersBeforeRequest ?? self.minCharactersBeforeRequest,
            page: page ?? self.page,
            showLanguageCurrency: showLanguageCurrency ?? self.showLanguageCurrency
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - HybrisMedia
public struct HybrisMedia: Codable {
    public let tablet, desktop, mobile, widescreen: HybrisDesktop?
    public let code, mime, altText, url: String?
    public let downloadURL: String?

    enum CodingKeys: String, CodingKey {
        case tablet, desktop, mobile, widescreen, code, mime, altText, url
        case downloadURL = "downloadUrl"
    }

    public init(tablet: HybrisDesktop?, desktop: HybrisDesktop?, mobile: HybrisDesktop?, widescreen: HybrisDesktop?, code: String?, mime: String?, altText: String?, url: String?, downloadURL: String?) {
        self.tablet = tablet
        self.desktop = desktop
        self.mobile = mobile
        self.widescreen = widescreen
        self.code = code
        self.mime = mime
        self.altText = altText
        self.url = url
        self.downloadURL = downloadURL
    }
}

// MARK: HybrisMedia convenience initializers and mutators

public extension HybrisMedia {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(HybrisMedia.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        tablet: HybrisDesktop?? = nil,
        desktop: HybrisDesktop?? = nil,
        mobile: HybrisDesktop?? = nil,
        widescreen: HybrisDesktop?? = nil,
        code: String?? = nil,
        mime: String?? = nil,
        altText: String?? = nil,
        url: String?? = nil,
        downloadURL: String?? = nil
    ) -> HybrisMedia {
        return HybrisMedia(
            tablet: tablet ?? self.tablet,
            desktop: desktop ?? self.desktop,
            mobile: mobile ?? self.mobile,
            widescreen: widescreen ?? self.widescreen,
            code: code ?? self.code,
            mime: mime ?? self.mime,
            altText: altText ?? self.altText,
            url: url ?? self.url,
            downloadURL: downloadURL ?? self.downloadURL
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - HybrisDesktop
public struct HybrisDesktop: Codable {
    public let code, catalogID, mime, altText: String?
    public let url, downloadURL: String?

    enum CodingKeys: String, CodingKey {
        case code
        case catalogID = "catalogId"
        case mime, altText, url
        case downloadURL = "downloadUrl"
    }

    public init(code: String?, catalogID: String?, mime: String?, altText: String?, url: String?, downloadURL: String?) {
        self.code = code
        self.catalogID = catalogID
        self.mime = mime
        self.altText = altText
        self.url = url
        self.downloadURL = downloadURL
    }
}

// MARK: HybrisDesktop convenience initializers and mutators

public extension HybrisDesktop {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(HybrisDesktop.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        code: String?? = nil,
        catalogID: String?? = nil,
        mime: String?? = nil,
        altText: String?? = nil,
        url: String?? = nil,
        downloadURL: String?? = nil
    ) -> HybrisDesktop {
        return HybrisDesktop(
            code: code ?? self.code,
            catalogID: catalogID ?? self.catalogID,
            mime: mime ?? self.mime,
            altText: altText ?? self.altText,
            url: url ?? self.url,
            downloadURL: downloadURL ?? self.downloadURL
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - HybrisNavigationNode
public struct HybrisNavigationNode: Codable {
    public let uid, uuid, name: String?
    public let entries: [JSONAny]?
    public let children: [HybrisChild]?

    public init(uid: String?, uuid: String?, name: String?, entries: [JSONAny]?, children: [HybrisChild]?) {
        self.uid = uid
        self.uuid = uuid
        self.name = name
        self.entries = entries
        self.children = children
    }
}

// MARK: HybrisNavigationNode convenience initializers and mutators

public extension HybrisNavigationNode {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(HybrisNavigationNode.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        uid: String?? = nil,
        uuid: String?? = nil,
        name: String?? = nil,
        entries: [JSONAny]?? = nil,
        children: [HybrisChild]?? = nil
    ) -> HybrisNavigationNode {
        return HybrisNavigationNode(
            uid: uid ?? self.uid,
            uuid: uuid ?? self.uuid,
            name: name ?? self.name,
            entries: entries ?? self.entries,
            children: children ?? self.children
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - HybrisChild
public struct HybrisChild: Codable {
    public let uid, uuid: String?
    public let entries: [HybrisEntry]?
    public let children: [HybrisChild]?
    public let title: String?

    public init(uid: String?, uuid: String?, entries: [HybrisEntry]?, children: [HybrisChild]?, title: String?) {
        self.uid = uid
        self.uuid = uuid
        self.entries = entries
        self.children = children
        self.title = title
    }
}

// MARK: HybrisChild convenience initializers and mutators

public extension HybrisChild {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(HybrisChild.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        uid: String?? = nil,
        uuid: String?? = nil,
        entries: [HybrisEntry]?? = nil,
        children: [HybrisChild]?? = nil,
        title: String?? = nil
    ) -> HybrisChild {
        return HybrisChild(
            uid: uid ?? self.uid,
            uuid: uuid ?? self.uuid,
            entries: entries ?? self.entries,
            children: children ?? self.children,
            title: title ?? self.title
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - HybrisEntry
public struct HybrisEntry: Codable {
    public let itemID, itemSuperType, itemType: String?

    enum CodingKeys: String, CodingKey {
        case itemID = "itemId"
        case itemSuperType, itemType
    }

    public init(itemID: String?, itemSuperType: String?, itemType: String?) {
        self.itemID = itemID
        self.itemSuperType = itemSuperType
        self.itemType = itemType
    }
}

// MARK: HybrisEntry convenience initializers and mutators

public extension HybrisEntry {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(HybrisEntry.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        itemID: String?? = nil,
        itemSuperType: String?? = nil,
        itemType: String?? = nil
    ) -> HybrisEntry {
        return HybrisEntry(
            itemID: itemID ?? self.itemID,
            itemSuperType: itemSuperType ?? self.itemSuperType,
            itemType: itemType ?? self.itemType
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Encode/decode helpers

public class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

public class JSONAny: Codable {

    public let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
