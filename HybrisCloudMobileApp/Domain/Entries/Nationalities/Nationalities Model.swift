//
//  NationalityModel.swift
//  Domain
//
//  Created by khalil on 4/2/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//


import Foundation

// MARK: - NationalitiesModel
public struct NationalitiesModel: Codable  , Identifiable{
    public var identity: String{
        return nationalities?.first?.code ?? ""
    }
    public var nationalities: [Nationality]?

    enum CodingKeys: String, CodingKey {
        case nationalities
    }

    public init(nationalities: [Nationality]?) {
        self.nationalities = nationalities
    }
}

// MARK: - Nationality
public struct Nationality: Codable {
    public var code: String?
    public var name: String?

    enum CodingKeys: String, CodingKey {
        case code
        case name
    }

    public init(code: String?, name: String?) {
        self.code = code
        self.name = name
    }
}

