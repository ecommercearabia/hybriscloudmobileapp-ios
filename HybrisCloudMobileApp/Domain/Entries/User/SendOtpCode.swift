//
//  SendOtpCode.swift
//  Domain
//
//  Created by ahmadSaleh on 12/3/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// MARK: - SendOtpCode
public struct SendOtpCode: Codable  ,Identifiable{
//
   public var identity: String{
       return sendOtpCodeDescription ?? ""
   }
    public let sendOtpCodeDescription: String?
    public let status: Bool?

    enum CodingKeys: String, CodingKey {
        case sendOtpCodeDescription = "description"
        case status
    }

    public init(sendOtpCodeDescription: String?, status: Bool?) {
        self.sendOtpCodeDescription = sendOtpCodeDescription
        self.status = status
    }
}
