//
//  UserLogin.swift
//  Domain
//
//  Created by Ahmad Bader on 8/2/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
public struct UserLogin: Codable {
    public let password: String?
    public let uid: String?


    public init( password: String?, uid: String?) {
         self.password = password
         self.uid = uid
     }
   public enum CodingKeys: String, CodingKey {
        case password = "password"
        case uid = "uid"
    }
 
}

