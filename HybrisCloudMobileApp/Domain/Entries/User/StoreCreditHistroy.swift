//
//  StoreCreditHistroy.swift
//  Domain
//
//  Created by walaa omar on 8/19/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let storeCreditHistroy = try StoreCreditHistroy(json)

import Foundation

// MARK: - StoreCreditHistroy
public struct StoreCreditHistroy: Codable , Identifiable{
    
    public var identity: String{
        return   ""
    }
    public var storeCreditHistroy: [StoreCreditHistroyElement]?

    public init(storeCreditHistroy: [StoreCreditHistroyElement]?) {
        self.storeCreditHistroy = storeCreditHistroy
    }
}

// MARK: StoreCreditHistroy convenience initializers and mutators

public extension StoreCreditHistroy {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(StoreCreditHistroy.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        storeCreditHistroy: [StoreCreditHistroyElement]?? = nil
    ) -> StoreCreditHistroy {
        return StoreCreditHistroy(
            storeCreditHistroy: storeCreditHistroy ?? self.storeCreditHistroy
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - StoreCreditHistroyElement
public struct StoreCreditHistroyElement: Codable , Identifiable{
  
    public var identity: String{
        return  orderCode ?? ""
    }
    public var amount, balance: Amount?
    public var dateOfPurchase: String?
    public var orderCode: String?

    public init(amount: Amount?, balance: Amount?, dateOfPurchase: String?, orderCode: String?) {
        self.amount = amount
        self.balance = balance
        self.dateOfPurchase = dateOfPurchase
        self.orderCode = orderCode
    }
}

// MARK: StoreCreditHistroyElement convenience initializers and mutators

public extension StoreCreditHistroyElement {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(StoreCreditHistroyElement.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        amount: Amount?? = nil,
        balance: Amount?? = nil,
        dateOfPurchase: String?? = nil,
        orderCode: String?? = nil
    ) -> StoreCreditHistroyElement {
        return StoreCreditHistroyElement(
            amount: amount ?? self.amount,
            balance: balance ?? self.balance,
            dateOfPurchase: dateOfPurchase ?? self.dateOfPurchase,
            orderCode: orderCode ?? self.orderCode
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Amount
public struct Amount: Codable {
    public var currencyISO, formattedValue, priceType, symbolLOC: String?
    public var value: Double?

    enum CodingKeys: String, CodingKey {
        case currencyISO = "currencyIso"
        case formattedValue, priceType
        case symbolLOC = "symbolLoc"
        case value
    }

    public init(currencyISO: String?, formattedValue: String?, priceType: String?, symbolLOC: String?, value: Double?) {
        self.currencyISO = currencyISO
        self.formattedValue = formattedValue
        self.priceType = priceType
        self.symbolLOC = symbolLOC
        self.value = value
    }
}

// MARK: Amount convenience initializers and mutators

public extension Amount {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Amount.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        currencyISO: String?? = nil,
        formattedValue: String?? = nil,
        priceType: String?? = nil,
        symbolLOC: String?? = nil,
        value: Double?? = nil
    ) -> Amount {
        return Amount(
            currencyISO: currencyISO ?? self.currencyISO,
            formattedValue: formattedValue ?? self.formattedValue,
            priceType: priceType ?? self.priceType,
            symbolLOC: symbolLOC ?? self.symbolLOC,
            value: value ?? self.value
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}





//// MARK: - StoreCreditHistroy
//public struct StoreCreditHistroy: Codable , Identifiable
//{
//    public var identity: String{
//        return   ""
//    }
//    public let storeCreditHistroy: [StoreCreditHistroyElement]?
//
//    public init(storeCreditHistroy: [StoreCreditHistroyElement]?) {
//        self.storeCreditHistroy = storeCreditHistroy
//    }
//}
//
//// MARK: StoreCreditHistroy convenience initializers and mutators
//
//public extension StoreCreditHistroy {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(StoreCreditHistroy.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        storeCreditHistroy: [StoreCreditHistroyElement]?? = nil
//    ) -> StoreCreditHistroy {
//        return StoreCreditHistroy(
//            storeCreditHistroy: storeCreditHistroy ?? self.storeCreditHistroy
//        )
//    }
//
//}
//
//// MARK: - StoreCreditHistroyElement
//public struct StoreCreditHistroyElement: Codable , Identifiable
//{
//    public var identity: String{
//        return  orderCode ?? ""
//    }
//    public let amount, balance: Amount?
//    public let dateOfPurchase, orderCode: String?
//
//    public init(amount: Amount?, balance: Amount?, dateOfPurchase: String?, orderCode: String?) {
//        self.amount = amount
//        self.balance = balance
//        self.dateOfPurchase = dateOfPurchase
//        self.orderCode = orderCode
//    }
//}
//
//// MARK: StoreCreditHistroyElement convenience initializers and mutators
//
//public extension StoreCreditHistroyElement {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(StoreCreditHistroyElement.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        amount: Amount?? = nil,
//        balance: Amount?? = nil,
//        dateOfPurchase: String?? = nil,
//        orderCode: String?? = nil
//    ) -> StoreCreditHistroyElement {
//        return StoreCreditHistroyElement(
//            amount: amount ?? self.amount,
//            balance: balance ?? self.balance,
//            dateOfPurchase: dateOfPurchase ?? self.dateOfPurchase,
//            orderCode: orderCode ?? self.orderCode
//        )
//    }
//
//}
//
//// MARK: - Amount
//public struct Amount: Codable {
//    public let currencyISO, formattedValue: String?
//    public let maxQuantity, minQuantity: Int?
//    public let priceType, symbolLOC: String?
//    public let value: Int?
//
//    enum CodingKeys: String, CodingKey {
//        case currencyISO = "currencyIso"
//        case formattedValue, maxQuantity, minQuantity, priceType
//        case symbolLOC = "symbolLoc"
//        case value
//    }
//
//    public init(currencyISO: String?, formattedValue: String?, maxQuantity: Int?, minQuantity: Int?, priceType: String?, symbolLOC: String?, value: Int?) {
//        self.currencyISO = currencyISO
//        self.formattedValue = formattedValue
//        self.maxQuantity = maxQuantity
//        self.minQuantity = minQuantity
//        self.priceType = priceType
//        self.symbolLOC = symbolLOC
//        self.value = value
//    }
//}
//
//// MARK: Amount convenience initializers and mutators
//
//public extension Amount {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(Amount.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        currencyISO: String?? = nil,
//        formattedValue: String?? = nil,
//        maxQuantity: Int?? = nil,
//        minQuantity: Int?? = nil,
//        priceType: String?? = nil,
//        symbolLOC: String?? = nil,
//        value: Int?? = nil
//    ) -> Amount {
//        return Amount(
//            currencyISO: currencyISO ?? self.currencyISO,
//            formattedValue: formattedValue ?? self.formattedValue,
//            maxQuantity: maxQuantity ?? self.maxQuantity,
//            minQuantity: minQuantity ?? self.minQuantity,
//            priceType: priceType ?? self.priceType,
//            symbolLOC: symbolLOC ?? self.symbolLOC,
//            value: value ?? self.value
//        )
//    }
//
//}
//
