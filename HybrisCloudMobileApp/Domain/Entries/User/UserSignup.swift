//
//  UserSignup.swift
//  Domain
//
//  Created by Saja Hammad on 7/21/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

// MARK: - UserSignup
public struct UserSignup: Codable {
    
    public let firstName: String?
    public let lastName: String?
    public let mobileCountryCode: String?
    public let mobileNumber: String?
    public let password: String?
    public let referralCode: String?
    public let titleCode: String?
    public let uid: String?
    public let nationality : String?
    public let nationalityId : String?
    
    public init(firstName: String?, lastName: String?,mobileCountryCode: String?, mobileNumber : String , password: String?, referralCode: String?, titleCode: String?, uid: String? , nationality : String? , nationalityId : String? ) {
        self.firstName = firstName
        self.lastName = lastName
        self.mobileCountryCode = mobileCountryCode
        self.mobileNumber = mobileNumber
        self.password = password
        self.referralCode = referralCode
        self.titleCode = titleCode
        self.uid = uid
        self.nationality = nationality
        self.nationalityId = nationalityId
    }
    
    public enum CodingKeys: String, CodingKey {
        case firstName = "firstName"
        case lastName = "lastName"
        case mobileCountryCode = "mobileCountryCode"
        case mobileNumber = "mobileNumber"
        case password = "password"
        case referralCode = "referralCode"
        case titleCode = "titleCode"
        case uid = "uid"
        case  nationality = "nationality"
        case nationalityId = "nationalityId"
        
    }
    
    
    
}




