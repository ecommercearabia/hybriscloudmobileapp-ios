//
//  StoreCreditAmount.swift
//  Domain
//
//  Created by walaa omar on 8/19/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// MARK: - StoreCreditAmount
public struct StoreCreditAmount: Codable , Identifiable
{
    
    public var identity: String{
        return  String(value ?? 0.0)
    }
    public let currencyISO, formattedValue: String?
    public let maxQuantity, minQuantity: Int?
    public let priceType: String?
    public let value: Double?

    enum CodingKeys: String, CodingKey {
        case currencyISO = "currencyIso"
        case formattedValue, maxQuantity, minQuantity, priceType, value
    }

    public init(currencyISO: String?, formattedValue: String?, maxQuantity: Int?, minQuantity: Int?, priceType: String?, value: Double?) {
        self.currencyISO = currencyISO
        self.formattedValue = formattedValue
        self.maxQuantity = maxQuantity
        self.minQuantity = minQuantity
        self.priceType = priceType
        self.value = value
    }
}

// MARK: StoreCreditAmount convenience initializers and mutators

public extension StoreCreditAmount {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(StoreCreditAmount.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        currencyISO: String?? = nil,
        formattedValue: String?? = nil,
        maxQuantity: Int?? = nil,
        minQuantity: Int?? = nil,
        priceType: String?? = nil,
        value: Double?? = nil
    ) -> StoreCreditAmount {
        return StoreCreditAmount(
            currencyISO: currencyISO ?? self.currencyISO,
            formattedValue: formattedValue ?? self.formattedValue,
            maxQuantity: maxQuantity ?? self.maxQuantity,
            minQuantity: minQuantity ?? self.minQuantity,
            priceType: priceType ?? self.priceType,
            value: value ?? self.value
        )
    }

}
