//
//  NewLoginModel.swift
//  Domain
//
//  Created by khalil on 21/2/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation

// MARK: - NewLoginModel
public struct NewLoginModel: Codable ,Identifiable{
       public var identity: String{
           return accessToken ?? ""
       }
    
    public var accessToken: String?
    public var refreshToken: String?
    public var scope: String?
    public var tokenType: String?
    public var expiresIn: Int?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
        case scope =  "scope"
        case tokenType = "token_type"
        case expiresIn = "expires_in"
    }

    public init(accessToken: String?, refreshToken: String?, scope: String?, tokenType: String?, expiresIn: Int?) {
        self.accessToken = accessToken
        self.refreshToken = refreshToken
        self.scope = scope
        self.tokenType = tokenType
        self.expiresIn = expiresIn
    }
}
