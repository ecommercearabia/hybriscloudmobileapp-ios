//
//  Register.swift
//  Domain
//
//  Created by Saja Hammad on 7/21/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation

//// MARK: - RegisterResponseModel
//public struct User: Codable  ,Identifiable{
//
//   public var identity: String{
//       return uid ?? ""
//   }
//    public let currency: Currency?
//    public let customerID: String?
//    public let deactivationDate: String?
//    public let defaultAddress: DefaultAddress?
//    public let displayUid: String?
//    public let firstName: String?
//    public let language: Currency?
//    public let lastName: String?
//    public let name: String?
//    public let title: String?
//    public let titleCode: String?
//    public let uid: String?
//    public let type: String?
//    public let mobileCountry: Country?
//    public let mobileNumber: String?
//
//
//    enum CodingKeys: String, CodingKey {
//        case currency = "currency"
//        case customerID = "customerId"
//        case deactivationDate = "deactivationDate"
//        case defaultAddress = "defaultAddress"
//        case displayUid = "displayUid"
//        case firstName = "firstName"
//        case language = "language"
//        case lastName = "lastName"
//        case name = "name"
//        case title = "title"
//        case titleCode = "titleCode"
//        case uid = "uid"
//        case type  = "type"
//        case mobileNumber = "mobileNumber"
//        case mobileCountry = "mobileCountry"
//    }
//
//    public init(type: String?, name: String?, uid: String?, currency: Currency?, customerID: String?, defaultAddress: DefaultAddress?, displayUid: String?, firstName: String?, language: Currency?, lastName: String?, mobileCountry: Country?, mobileNumber: String?, title: String?, titleCode: String?) {
//        self.type = type
//        self.name = name
//        self.uid = uid
//        self.currency = currency
//        self.customerID = customerID
//        self.defaultAddress = defaultAddress
//        self.displayUid = displayUid
//        self.firstName = firstName
//        self.language = language
//        self.lastName = lastName
//        self.mobileCountry = mobileCountry
//        self.mobileNumber = mobileNumber
//        self.title = title
//        self.titleCode = titleCode
//    }
//}
//
//// MARK: - Currency
//public struct Currency: Codable {
//    let active: Bool?
//    let isocode: String?
//    let name: String?
//    let symbol: String?
//    let nativeName: String?
//
//    enum CodingKeys: String, CodingKey {
//        case active = "active"
//        case isocode = "isocode"
//        case name = "name"
//        case symbol = "symbol"
//        case nativeName = "nativeName"
//    }
//}
//
//// MARK: - DefaultAddress
//public struct DefaultAddress: Codable {
//    let cellphone: String?
//    let companyName: String?
//  public  let country: Country?
//    let defaultAddress: Bool?
//    let district: String?
//    let email: String?
//    let firstName: String?
//    let formattedAddress: String?
//    let id: String?
//    let lastName: String?
//    let line1: String?
//    let line2: String?
//    let phone: String?
//    let postalCode: String?
//    let region: Region?
//    let shippingAddress: Bool?
//    let title: String?
//    let titleCode: String?
//    let town: String?
//    let visibleInAddressBook: Bool?
//
//    enum CodingKeys: String, CodingKey {
//        case cellphone = "cellphone"
//        case companyName = "companyName"
//        case country = "country"
//        case defaultAddress = "defaultAddress"
//        case district = "district"
//        case email = "email"
//        case firstName = "firstName"
//        case formattedAddress = "formattedAddress"
//        case id = "id"
//        case lastName = "lastName"
//        case line1 = "line1"
//        case line2 = "line2"
//        case phone = "phone"
//        case postalCode = "postalCode"
//        case region = "region"
//        case shippingAddress = "shippingAddress"
//        case title = "title"
//        case titleCode = "titleCode"
//        case town = "town"
//        case visibleInAddressBook = "visibleInAddressBook"
//    }
//}
//
//// MARK: - Country
//public struct Country: Codable, Equatable {
//   public let isocode: String?
//   public let isdcode: String?
//   public let name: String?
//
//    enum CodingKeys: String, CodingKey {
//        case isocode = "isocode"
//        case isdcode = "isdcode"
//        case name = "name"
//    }
//       public init(isdcode: String?, isocode: String?, name: String?) {
//            self.isdcode = isdcode
//            self.isocode = isocode
//            self.name = name
//        }
//}
//

//



//import Foundation
//
//// MARK: - User
//public struct User: Codable  ,Identifiable{
//
//   public var identity: String{
//       return uid ?? ""
//   }
//    public let type, name, uid: String?
//    public let currency: Currency?
//    public let customerID: String?
//    public let defaultAddress: DefaultAddress?
//    public let displayUid, firstName: String?
//    public let language: Currency?
//    public let lastName: String?
//    public let mobileCountry: Country?
//    public let mobileNumber, title, titleCode: String?
//
//    enum CodingKeys: String, CodingKey {
//        case type, name, uid, currency
//        case customerID = "customerId"
//        case defaultAddress, displayUid, firstName, language, lastName, mobileCountry, mobileNumber, title, titleCode
//    }
//
//    public init(type: String?, name: String?, uid: String?, currency: Currency?, customerID: String?, defaultAddress: DefaultAddress?, displayUid: String?, firstName: String?, language: Currency?, lastName: String?, mobileCountry: Country?, mobileNumber: String?, title: String?, titleCode: String?) {
//        self.type = type
//        self.name = name
//        self.uid = uid
//        self.currency = currency
//        self.customerID = customerID
//        self.defaultAddress = defaultAddress
//        self.displayUid = displayUid
//        self.firstName = firstName
//        self.language = language
//        self.lastName = lastName
//        self.mobileCountry = mobileCountry
//        self.mobileNumber = mobileNumber
//        self.title = title
//        self.titleCode = titleCode
//    }
//}
//
//// MARK: User convenience initializers and mutators
//
//public extension User {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(User.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        type: String?? = nil,
//        name: String?? = nil,
//        uid: String?? = nil,
//        currency: Currency?? = nil,
//        customerID: String?? = nil,
//        defaultAddress: DefaultAddress?? = nil,
//        displayUid: String?? = nil,
//        firstName: String?? = nil,
//        language: Currency?? = nil,
//        lastName: String?? = nil,
//        mobileCountry: Country?? = nil,
//        mobileNumber: String?? = nil,
//        title: String?? = nil,
//        titleCode: String?? = nil
//    ) -> User {
//        return User(
//            type: type ?? self.type,
//            name: name ?? self.name,
//            uid: uid ?? self.uid,
//            currency: currency ?? self.currency,
//            customerID: customerID ?? self.customerID,
//            defaultAddress: defaultAddress ?? self.defaultAddress,
//            displayUid: displayUid ?? self.displayUid,
//            firstName: firstName ?? self.firstName,
//            language: language ?? self.language,
//            lastName: lastName ?? self.lastName,
//            mobileCountry: mobileCountry ?? self.mobileCountry,
//            mobileNumber: mobileNumber ?? self.mobileNumber,
//            title: title ?? self.title,
//            titleCode: titleCode ?? self.titleCode
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - Currency
//public struct Currency: Codable {
//    public let active: Bool?
//    public let isocode, name, symbol, nativeName: String?
//
//    public init(active: Bool?, isocode: String?, name: String?, symbol: String?, nativeName: String?) {
//        self.active = active
//        self.isocode = isocode
//        self.name = name
//        self.symbol = symbol
//        self.nativeName = nativeName
//    }
//}
//
//// MARK: Currency convenience initializers and mutators
//
//public extension Currency {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(Currency.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        active: Bool?? = nil,
//        isocode: String?? = nil,
//        name: String?? = nil,
//        symbol: String?? = nil,
//        nativeName: String?? = nil
//    ) -> Currency {
//        return Currency(
//            active: active ?? self.active,
//            isocode: isocode ?? self.isocode,
//            name: name ?? self.name,
//            symbol: symbol ?? self.symbol,
//            nativeName: nativeName ?? self.nativeName
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//// MARK: - DefaultAddress
//public struct DefaultAddress: Codable {
//    public let addressName: String?
//    public let country: Country?
//    public let defaultAddress: Bool?
//    public let district, firstName, formattedAddress, id: String?
//    public let lastName, line1, line2: String?
//    public let mobileCountry: Country?
//    public let mobileNumber, postalCode: String?
//    public let shippingAddress: Bool?
//    public let town: String?
//    public let visibleInAddressBook: Bool?
//
//    public init(addressName: String?, country: Country?, defaultAddress: Bool?, district: String?, firstName: String?, formattedAddress: String?, id: String?, lastName: String?, line1: String?, line2: String?, mobileCountry: Country?, mobileNumber: String?, postalCode: String?, shippingAddress: Bool?, town: String?, visibleInAddressBook: Bool?) {
//        self.addressName = addressName
//        self.country = country
//        self.defaultAddress = defaultAddress
//        self.district = district
//        self.firstName = firstName
//        self.formattedAddress = formattedAddress
//        self.id = id
//        self.lastName = lastName
//        self.line1 = line1
//        self.line2 = line2
//        self.mobileCountry = mobileCountry
//        self.mobileNumber = mobileNumber
//        self.postalCode = postalCode
//        self.shippingAddress = shippingAddress
//        self.town = town
//        self.visibleInAddressBook = visibleInAddressBook
//    }
//}
//
//// MARK: DefaultAddress convenience initializers and mutators
//
//public extension DefaultAddress {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(DefaultAddress.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        addressName: String?? = nil,
//        country: Country?? = nil,
//        defaultAddress: Bool?? = nil,
//        district: String?? = nil,
//        firstName: String?? = nil,
//        formattedAddress: String?? = nil,
//        id: String?? = nil,
//        lastName: String?? = nil,
//        line1: String?? = nil,
//        line2: String?? = nil,
//        mobileCountry: Country?? = nil,
//        mobileNumber: String?? = nil,
//        postalCode: String?? = nil,
//        shippingAddress: Bool?? = nil,
//        town: String?? = nil,
//        visibleInAddressBook: Bool?? = nil
//    ) -> DefaultAddress {
//        return DefaultAddress(
//            addressName: addressName ?? self.addressName,
//            country: country ?? self.country,
//            defaultAddress: defaultAddress ?? self.defaultAddress,
//            district: district ?? self.district,
//            firstName: firstName ?? self.firstName,
//            formattedAddress: formattedAddress ?? self.formattedAddress,
//            id: id ?? self.id,
//            lastName: lastName ?? self.lastName,
//            line1: line1 ?? self.line1,
//            line2: line2 ?? self.line2,
//            mobileCountry: mobileCountry ?? self.mobileCountry,
//            mobileNumber: mobileNumber ?? self.mobileNumber,
//            postalCode: postalCode ?? self.postalCode,
//            shippingAddress: shippingAddress ?? self.shippingAddress,
//            town: town ?? self.town,
//            visibleInAddressBook: visibleInAddressBook ?? self.visibleInAddressBook
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
//
//
//
// MARK: - Country
public struct Country: Codable , Equatable {
    public let isdcode, isocode, name: String?

    public init(isdcode: String?, isocode: String?, name: String?) {
        self.isdcode = isdcode
        self.isocode = isocode
        self.name = name
    }
}

// MARK: Country convenience initializers and mutators

public extension Country {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Country.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        isdcode: String?? = nil,
        isocode: String?? = nil,
        name: String?? = nil
    ) -> Country {
        return Country(
            isdcode: isdcode ?? self.isdcode,
            isocode: isocode ?? self.isocode,
            name: name ?? self.name
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
//// MARK: - Region
public struct Region: Codable {
   public let countryISO: String?
   public  let isocode: String?
   public let isocodeShort: String?
   public let name: String?

    enum CodingKeys: String, CodingKey {
        case countryISO = "countryIso"
        case isocode = "isocode"
        case isocodeShort = "isocodeShort"
        case name = "name"
    }
    public init(countryISO : String , isocode : String , isocodeShort : String , name : String  ){
        self.countryISO = countryISO
        self.isocode = isocode
        self.isocodeShort = isocodeShort
        self.name = name

    }
}




import Foundation

// MARK: - User
public struct UserData: Codable  ,Identifiable{
//
   public var identity: String{
       return uid ?? ""
   }
    public var type, name, uid: String?
    public var currency: Currency?
    public var customerID: String?
    public var defaultAddress: DefaultAddress?
    public var displayUid, firstName: String?
    public var language: Currency?
    public var lastName: String?
    public var mobileCountry: Country?
    public var mobileNumber, title, titleCode,phone  ,  nationalityID : String?
    public var nationality: Nationality?
   
    enum CodingKeys: String, CodingKey {
        case type, name, uid, currency
        case customerID = "customerId"
        case defaultAddress, displayUid, firstName, language, lastName, mobileCountry, mobileNumber, title, titleCode,phone ,  nationality ,  nationalityID
    }

    public init(type: String?, name: String?, uid: String?, currency: Currency?, customerID: String?, defaultAddress: DefaultAddress?, displayUid: String?, firstName: String?, language: Currency?, lastName: String?, mobileCountry: Country?, mobileNumber: String?, title: String?, titleCode: String?,phone : String? ,  nationality: Nationality?,  nationalityID: String?) {
        self.type = type
        self.name = name
        self.uid = uid
        self.currency = currency
        self.customerID = customerID
        self.defaultAddress = defaultAddress
        self.displayUid = displayUid
        self.firstName = firstName
        self.language = language
        self.lastName = lastName
        self.mobileCountry = mobileCountry
        self.mobileNumber = mobileNumber
        self.title = title
        self.titleCode = titleCode
        self.phone = phone
        self.nationality = nationality
        self.nationalityID = nationalityID
        
    }
}

// MARK: User convenience initializers and mutators

public extension UserData {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(UserData.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        type: String?? = nil,
        name: String?? = nil,
        uid: String?? = nil,
        currency: Currency?? = nil,
        customerID: String?? = nil,
        defaultAddress: DefaultAddress?? = nil,
        displayUid: String?? = nil,
        firstName: String?? = nil,
        language: Currency?? = nil,
        lastName: String?? = nil,
        mobileCountry: Country?? = nil,
        mobileNumber: String?? = nil,
        title: String?? = nil,
        titleCode: String?? = nil,
        phone : String?? = nil,
        nationality : Nationality?? = nil,
        nationalityID : String?? = nil
       
       
    ) -> UserData {
        return UserData(
            type: type ?? self.type,
            name: name ?? self.name,
            uid: uid ?? self.uid,
            currency: currency ?? self.currency,
            customerID: customerID ?? self.customerID,
            defaultAddress: defaultAddress ?? self.defaultAddress,
            displayUid: displayUid ?? self.displayUid,
            firstName: firstName ?? self.firstName,
            language: language ?? self.language,
            lastName: lastName ?? self.lastName,
            mobileCountry: mobileCountry ?? self.mobileCountry,
            mobileNumber: mobileNumber ?? self.mobileNumber,
            title: title ?? self.title,
            titleCode: titleCode ?? self.titleCode,
            phone: phone ?? self.phone,
            nationality : nationality ?? self.nationality,
            nationalityID : nationalityID ?? self.nationalityID
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Currency
public struct Currency: Codable {
    public let active: Bool?
    public let isocode, name, symbol, nativeName: String?

    public init(active: Bool?, isocode: String?, name: String?, symbol: String?, nativeName: String?) {
        self.active = active
        self.isocode = isocode
        self.name = name
        self.symbol = symbol
        self.nativeName = nativeName
    }
}

// MARK: Currency convenience initializers and mutators

public extension Currency {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Currency.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        active: Bool?? = nil,
        isocode: String?? = nil,
        name: String?? = nil,
        symbol: String?? = nil,
        nativeName: String?? = nil
    ) -> Currency {
        return Currency(
            active: active ?? self.active,
            isocode: isocode ?? self.isocode,
            name: name ?? self.name,
            symbol: symbol ?? self.symbol,
            nativeName: nativeName ?? self.nativeName
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - DefaultAddress
public struct DefaultAddress: Codable {
    public var addressName: String?
    public var country: Country?
    public var defaultAddress: Bool?
    public var district, firstName, formattedAddress, id: String?
    public var lastName, line1, line2: String?
    public var mobileCountry: Country?
    public var mobileNumber, postalCode: String?
    public var shippingAddress: Bool?
    public var town: String?
    public var visibleInAddressBook: Bool?

    public init(addressName: String?, country: Country?, defaultAddress: Bool?, district: String?, firstName: String?, formattedAddress: String?, id: String?, lastName: String?, line1: String?, line2: String?, mobileCountry: Country?, mobileNumber: String?, postalCode: String?, shippingAddress: Bool?, town: String?, visibleInAddressBook: Bool?) {
        self.addressName = addressName
        self.country = country
        self.defaultAddress = defaultAddress
        self.district = district
        self.firstName = firstName
        self.formattedAddress = formattedAddress
        self.id = id
        self.lastName = lastName
        self.line1 = line1
        self.line2 = line2
        self.mobileCountry = mobileCountry
        self.mobileNumber = mobileNumber
        self.postalCode = postalCode
        self.shippingAddress = shippingAddress
        self.town = town
        self.visibleInAddressBook = visibleInAddressBook
    }
}

// MARK: DefaultAddress convenience initializers and mutators

public extension DefaultAddress {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(DefaultAddress.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        addressName: String?? = nil,
        country: Country?? = nil,
        defaultAddress: Bool?? = nil,
        district: String?? = nil,
        firstName: String?? = nil,
        formattedAddress: String?? = nil,
        id: String?? = nil,
        lastName: String?? = nil,
        line1: String?? = nil,
        line2: String?? = nil,
        mobileCountry: Country?? = nil,
        mobileNumber: String?? = nil,
        postalCode: String?? = nil,
        shippingAddress: Bool?? = nil,
        town: String?? = nil,
        visibleInAddressBook: Bool?? = nil
    ) -> DefaultAddress {
        return DefaultAddress(
            addressName: addressName ?? self.addressName,
            country: country ?? self.country,
            defaultAddress: defaultAddress ?? self.defaultAddress,
            district: district ?? self.district,
            firstName: firstName ?? self.firstName,
            formattedAddress: formattedAddress ?? self.formattedAddress,
            id: id ?? self.id,
            lastName: lastName ?? self.lastName,
            line1: line1 ?? self.line1,
            line2: line2 ?? self.line2,
            mobileCountry: mobileCountry ?? self.mobileCountry,
            mobileNumber: mobileNumber ?? self.mobileNumber,
            postalCode: postalCode ?? self.postalCode,
            shippingAddress: shippingAddress ?? self.shippingAddress,
            town: town ?? self.town,
            visibleInAddressBook: visibleInAddressBook ?? self.visibleInAddressBook
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

//// MARK: - Country
//public struct Country: Codable {
//    public let isocode, name: String?
//
//    public init(isocode: String?, name: String?) {
//        self.isocode = isocode
//        self.name = name
//    }
//}

// MARK: Country convenience initializers and mutators

//public extension Country {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(Country.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        isocode: String?? = nil,
//        name: String?? = nil
//    ) -> Country {
//        return Country(
//            isocode: isocode ?? self.isocode,
//            name: name ?? self.name
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
