//
//  ChangePassword.swift
//  Domain
//
//  Created by Saja Hammad on 8/17/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
public struct ChangePassword: Codable {
    public let newPassword: String?
    public let oldPassword: String?
    public let userId: String?
    
    
    public init(newPassword: String?, oldPassword: String?, userId: String?) {
        self.newPassword = newPassword
        self.oldPassword = oldPassword
        self.userId = userId
    }
    public enum CodingKeys: String, CodingKey {
        case newPassword = "new"
        case oldPassword = "old"
        case userId = "userId"
    }
    
}

