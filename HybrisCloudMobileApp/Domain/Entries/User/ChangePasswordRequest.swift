//
//  ChnagePasswordRequest.swift
//  Domain
//
//  Created by Saja Hammad on 8/18/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

public struct ChangePasswordRequest: Codable {
    public let newPassword: String?
    public let oldPassword: String?

    
    public init( newPassword: String?, oldPassword: String?) {
        self.newPassword = newPassword
        self.oldPassword = oldPassword
    }
    public enum CodingKeys: String, CodingKey {
        case newPassword = "new"
        case oldPassword = "old"
        
    }
    
}

