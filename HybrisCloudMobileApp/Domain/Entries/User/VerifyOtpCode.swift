//
//  VerifyCode.swift
//  Domain
//
//  Created by ahmadSaleh on 12/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// MARK: - VerifyCode
public struct VerifyOtpCode: Codable ,Identifiable{
//
   public var identity: String{
       return accessToken ?? ""
   }
    public let accessToken, refreshToken: String?
    public let scope: [String]?
    public let tokenType: String?
    public let expiresIn: Int?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
        case scope
        case tokenType = "token_type"
        case expiresIn = "expires_in"
    }

    public init(accessToken: String?, refreshToken: String?, scope: [String]?, tokenType: String?, expiresIn: Int?) {
        self.accessToken = accessToken
        self.refreshToken = refreshToken
        self.scope = scope
        self.tokenType = tokenType
        self.expiresIn = expiresIn
    }
}
