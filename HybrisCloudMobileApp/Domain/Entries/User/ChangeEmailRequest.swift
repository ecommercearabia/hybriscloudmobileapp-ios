//
//  ChangeEmailRequest.swift
//  Domain
//
//  Created by walaa omar on 8/24/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

public struct ChangeEmailRequest: Codable {
    public let newLogin: String?
    public let password: String?

    
    public init( newLogin: String?, password: String?) {
        self.newLogin = newLogin
        self.password = password
    }
    
}
