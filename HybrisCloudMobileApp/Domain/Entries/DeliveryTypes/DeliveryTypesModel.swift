//
//  DeliveryTypesModel.swift
//  Domain
//
//  Created by khalil anqawi on 12/11/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation

// MARK: - DeliveryTypesModel
public struct DeliveryTypesModel: Codable  ,Identifiable {
    public var identity: String{
        return "DeliveryTypesModel"
    }
    public var deliveryModeTypes: [DeliveryModeType]?

    enum CodingKeys: String, CodingKey {
        case deliveryModeTypes = "deliveryModeTypes"
    }

    public init(deliveryModeTypes: [DeliveryModeType]?) {
        self.deliveryModeTypes = deliveryModeTypes
    }
}

// MARK: - DeliveryModeType
public struct DeliveryModeType: Codable {
    public var code: String?
    public var name: String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case name = "name"
    }

    public init(code: String?, name: String?) {
        self.code = code
        self.name = name
    }
}

