//
//  Reviews.swift
//  Domain
//
//  Created by walaa omar on 9/1/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// MARK: - Reviews
public struct Reviews: Codable ,Identifiable {
    public var identity: String{
        return  reviews?.first?.id ?? ""
    }
    public var reviews: [Review]?

    public init(reviews: [Review]?) {
        self.reviews = reviews
    }
}

// MARK: Reviews convenience initializers and mutators

public extension Reviews {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Reviews.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        reviews: [Review]?? = nil
    ) -> Reviews {
        return Reviews(
            reviews: reviews ?? self.reviews
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Review
public struct Review: Codable ,Identifiable {
    public var identity: String{
        return id ?? ""
    }
    public var alias, comment, date, headline: String?
    public var id: String?
    public var principal: Principal?
    public var rating: Double?

    public init(alias: String?, comment: String?, date: String?, headline: String?, id: String?, principal: Principal?, rating: Double?) {
        self.alias = alias
        self.comment = comment
        self.date = date
        self.headline = headline
        self.id = id
        self.principal = principal
        self.rating = rating
    }
    
    public init() {
       }
}

// MARK: Review convenience initializers and mutators

public extension Review {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Review.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        alias: String?? = nil,
        comment: String?? = nil,
        date: String?? = nil,
        headline: String?? = nil,
        id: String?? = nil,
        principal: Principal?? = nil,
        rating: Double?? = nil
    ) -> Review {
        return Review(
            alias: alias ?? self.alias,
            comment: comment ?? self.comment,
            date: date ?? self.date,
            headline: headline ?? self.headline,
            id: id ?? self.id,
            principal: principal ?? self.principal,
            rating: rating ?? self.rating
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Principal
public struct Principal: Codable ,Identifiable {
    
    public var identity: String{
    return customerID ?? ""
    }
    public var currency: Currency?
    public var customerID, deactivationDate: String?
    public var defaultAddress: DefaultAddress?
    public var displayUid, firstName: String?
    public var language: Currency?
    public var lastName: String?
    public var mobileCountry: Country?
    public var mobileNumber, name, title, titleCode: String?
    public var uid: String?

    enum CodingKeys: String, CodingKey {
        case currency
        case customerID = "customerId"
        case deactivationDate, defaultAddress, displayUid, firstName, language, lastName, mobileCountry, mobileNumber, name, title, titleCode, uid
    }

    public init(currency: Currency?, customerID: String?, deactivationDate: String?, defaultAddress: DefaultAddress?, displayUid: String?, firstName: String?, language: Currency?, lastName: String?, mobileCountry: Country?, mobileNumber: String?, name: String?, title: String?, titleCode: String?, uid: String?) {
        self.currency = currency
        self.customerID = customerID
        self.deactivationDate = deactivationDate
        self.defaultAddress = defaultAddress
        self.displayUid = displayUid
        self.firstName = firstName
        self.language = language
        self.lastName = lastName
        self.mobileCountry = mobileCountry
        self.mobileNumber = mobileNumber
        self.name = name
        self.title = title
        self.titleCode = titleCode
        self.uid = uid
    }
}

// MARK: Principal convenience initializers and mutators

public extension Principal {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Principal.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        currency: Currency?? = nil,
        customerID: String?? = nil,
        deactivationDate: String?? = nil,
        defaultAddress: DefaultAddress?? = nil,
        displayUid: String?? = nil,
        firstName: String?? = nil,
        language: Currency?? = nil,
        lastName: String?? = nil,
        mobileCountry: Country?? = nil,
        mobileNumber: String?? = nil,
        name: String?? = nil,
        title: String?? = nil,
        titleCode: String?? = nil,
        uid: String?? = nil
    ) -> Principal {
        return Principal(
            currency: currency ?? self.currency,
            customerID: customerID ?? self.customerID,
            deactivationDate: deactivationDate ?? self.deactivationDate,
            defaultAddress: defaultAddress ?? self.defaultAddress,
            displayUid: displayUid ?? self.displayUid,
            firstName: firstName ?? self.firstName,
            language: language ?? self.language,
            lastName: lastName ?? self.lastName,
            mobileCountry: mobileCountry ?? self.mobileCountry,
            mobileNumber: mobileNumber ?? self.mobileNumber,
            name: name ?? self.name,
            title: title ?? self.title,
            titleCode: titleCode ?? self.titleCode,
            uid: uid ?? self.uid
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
