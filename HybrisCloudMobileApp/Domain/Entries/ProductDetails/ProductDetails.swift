// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let ProductDetailss = try ProductDetailss(json)

import Foundation

// MARK: - ProductDetails
public class ProductDetails: Codable ,Identifiable{
    public var identity: String{
        get{
           
            return "\(price?.value ?? 0.0)\(code ?? "")\(classifications?.map({ $0.code ?? ""}).joined() ?? "")"
        }
    }
    public let availableForPickup: Bool?
    public let baseOptions: [ProducttBaseOption]?
    public let baseProduct: String?
    public let categories: [ProducttCategory]?
    public let classifications: [ProducttClassification]?
    public let code: String?
    public let configurable: Bool?
    public let countryOfOrigin: String?
    public let countryOfOriginIsocode: String?
    public let description: String?
    public let images: [ProducttImageElement]?
    public let discount: Discount?
    public let name: String?
    public let numberOfReviews: Int?
    public let potentialPromotions: [PotentialPromotion]?
    public let price: ProducttPrice?
    public let priceRange: ProducttPriceRange?
    public let productReferences: [ProducttProductReference]?
    public let purchasable: Bool?
    public let reviews: [ProducttReview]?
    public let stock: ProducttStock?
    public let summary: String?
    public let unitOfMeasure: String?
    public let unitOfMeasureDescription: String?
    public let url: String?
    public let averageRating: Double?
    public let vegan: Bool?
    public let organic: Bool?
    public let glutenFree: Bool?
    public let inWishlist: Bool?
    public var dry: Bool?
    public var express: Bool?
    public var frozen: Bool?
    public var chilled: Bool?
    public let nutritionFacts: String?
    enum CodingKeys: String, CodingKey {
        case availableForPickup
        case baseOptions
        case baseProduct
        case categories
        case classifications
        case code
        case configurable
        case countryOfOrigin
        case countryOfOriginIsocode
        case description
        case images
        case name
        case numberOfReviews
        case potentialPromotions
        case price
        case priceRange
        case productReferences
        case purchasable
        case reviews
        case stock
        case summary
        case unitOfMeasure
        case unitOfMeasureDescription
        case url
        case averageRating
        case discount
        case organic
        case glutenFree
        case vegan
        case inWishlist
        case dry
        case express
        case frozen
        case chilled
        case nutritionFacts
    
    }
    
    public init(availableForPickup: Bool?, baseOptions: [ProducttBaseOption]?, baseProduct: String?, categories: [ProducttCategory]?, classifications: [ProducttClassification]?, code: String?, configurable: Bool?, countryOfOrigin: String?, countryOfOriginIsocode: String?, ProductDetailsDescription: String?, images: [ProducttImageElement]?, name: String?, numberOfReviews: Int?, potentialPromotions: [PotentialPromotion]?, price: ProducttPrice?, priceRange: ProducttPriceRange?, productReferences: [ProducttProductReference]?, purchasable: Bool?, reviews: [ProducttReview]?, stock: ProducttStock?, summary: String?, unitOfMeasure: String?, unitOfMeasureDescription: String?, url: String?, averageRating: Double? , discount : Discount? , vegan:Bool?, organic:Bool?, glutenFree:Bool?, inWishlist:Bool? ,  dry:Bool?,  express:Bool?,  frozen:Bool?,  chilled:Bool? , nutritionFacts: String?) {
        self.availableForPickup = availableForPickup
        self.baseOptions = baseOptions
        self.baseProduct = baseProduct
        self.categories = categories
        self.classifications = classifications
        self.code = code
        self.configurable = configurable
        self.countryOfOrigin = countryOfOrigin
        self.countryOfOriginIsocode = countryOfOriginIsocode
        self.description = ProductDetailsDescription
        self.images = images
        self.name = name
        self.numberOfReviews = numberOfReviews
        self.potentialPromotions = potentialPromotions
        self.price = price
        self.priceRange = priceRange
        self.productReferences = productReferences
        self.purchasable = purchasable
        self.reviews = reviews
        self.stock = stock
        self.summary = summary
        self.unitOfMeasure = unitOfMeasure
        self.unitOfMeasureDescription = unitOfMeasureDescription
        self.url = url
        self.averageRating = averageRating
        self.discount = discount
        self.vegan = vegan
        self.organic = organic
        self.glutenFree = glutenFree
        self.inWishlist = inWishlist
        self.dry = dry
        self.express = express
        self.frozen = frozen
        self.chilled = chilled
        self.nutritionFacts = nutritionFacts
        
    }
}


// MARK: - Discount
public struct Discount: Codable {
    public let discountPrice: DiscountPrice?
    public let percentage: Int?
    public let price, saving: DiscountPrice?
    
    public init(discountPrice: DiscountPrice?, percentage: Int?, price: DiscountPrice?, saving: DiscountPrice?) {
        self.discountPrice = discountPrice
        self.percentage = percentage
        self.price = price
        self.saving = saving
    }
}

// MARK: - DiscountPrice
public struct DiscountPrice: Codable {
    public let currencyISO, formattedValue, priceType, symbolLOC: String?
    public let value: Double?
    
    enum CodingKeys: String, CodingKey {
        case currencyISO = "currencyIso"
        case formattedValue, priceType
        case symbolLOC = "symbolLoc"
        case value
    }
    
    public init(currencyISO: String?, formattedValue: String?, priceType: String?, symbolLOC: String?, value: Double?) {
        self.currencyISO = currencyISO
        self.formattedValue = formattedValue
        self.priceType = priceType
        self.symbolLOC = symbolLOC
        self.value = value
    }
}


// MARK: - PotentialPromotion
public struct PotentialPromotion: Codable {
    public let code, potentialPromotionDescription: String?
    public let endDate: String?
    public let promotionType: String?
    
    enum CodingKeys: String, CodingKey {
        case code
        case potentialPromotionDescription = "description"
        case endDate, promotionType
    }
    
    public init(code: String?, potentialPromotionDescription: String?, endDate: String?, promotionType: String?) {
        self.code = code
        self.potentialPromotionDescription = potentialPromotionDescription
        self.endDate = endDate
        self.promotionType = promotionType
    }
}




// MARK: - ProducttBaseOption
public class ProducttBaseOption: Codable {
    public let options: [ProducttSelected]?
    public let selected: ProducttSelected?
    public let variantType: String?
    
    enum CodingKeys: String, CodingKey {
        case options
        case selected
        case variantType
    }
    
    public init(options: [ProducttSelected]?, selected: ProducttSelected?, variantType: String?) {
        self.options = options
        self.selected = selected
        self.variantType = variantType
    }
}

// MARK: ProducttBaseOption convenience initializers and mutators

public extension ProducttBaseOption {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttBaseOption.self, from: data)
        self.init(options: me.options, selected: me.selected, variantType: me.variantType)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        options: [ProducttSelected]?? = nil,
        selected: ProducttSelected?? = nil,
        variantType: String?? = nil
    ) -> ProducttBaseOption {
        return ProducttBaseOption(
            options: options ?? self.options,
            selected: selected ?? self.selected,
            variantType: variantType ?? self.variantType
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttSelected
public class ProducttSelected: Codable {
    public let code: String?
    public let priceData: ProducttPrice?
    public let stock: ProducttStock?
    public let url: String?
    public let variantOptionQualifiers: [ProducttVariantOptionQualifier]?
    
    enum CodingKeys: String, CodingKey {
        case code
        case priceData
        case stock
        case url
        case variantOptionQualifiers
    }
    
    public init(code: String?, priceData: ProducttPrice?, stock: ProducttStock?, url: String?, variantOptionQualifiers: [ProducttVariantOptionQualifier]?) {
        self.code = code
        self.priceData = priceData
        self.stock = stock
        self.url = url
        self.variantOptionQualifiers = variantOptionQualifiers
    }
}

// MARK: ProducttSelected convenience initializers and mutators

public extension ProducttSelected {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttSelected.self, from: data)
        self.init(code: me.code, priceData: me.priceData, stock: me.stock, url: me.url, variantOptionQualifiers: me.variantOptionQualifiers)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        code: String?? = nil,
        priceData: ProducttPrice?? = nil,
        stock: ProducttStock?? = nil,
        url: String?? = nil,
        variantOptionQualifiers: [ProducttVariantOptionQualifier]?? = nil
    ) -> ProducttSelected {
        return ProducttSelected(
            code: code ?? self.code,
            priceData: priceData ?? self.priceData,
            stock: stock ?? self.stock,
            url: url ?? self.url,
            variantOptionQualifiers: variantOptionQualifiers ?? self.variantOptionQualifiers
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttPrice
public class ProducttPrice: Codable {
    public let currencyISO: String?
    public let formattedValue: String?
    public let priceType: String?
    public let value: Double?
    
    enum CodingKeys: String, CodingKey {
        case currencyISO
        case formattedValue
        case priceType
        case value
    }
    
    public init(currencyISO: String?, formattedValue: String?, priceType: String?, value: Double?) {
        self.currencyISO = currencyISO
        self.formattedValue = formattedValue
        self.priceType = priceType
        self.value = value
    }
}

// MARK: ProducttPrice convenience initializers and mutators

public extension ProducttPrice {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttPrice.self, from: data)
        self.init(currencyISO: me.currencyISO, formattedValue: me.formattedValue, priceType: me.priceType, value: me.value)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        currencyISO: String?? = nil,
        formattedValue: String?? = nil,
        priceType: String?? = nil,
        value: Double?? = nil
    ) -> ProducttPrice {
        return ProducttPrice(
            currencyISO: currencyISO ?? self.currencyISO,
            formattedValue: formattedValue ?? self.formattedValue,
            priceType: priceType ?? self.priceType,
            value: value ?? self.value
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttStock
public class ProducttStock: Codable {
    public let stockLevel: Int?
    public let stockLevelStatus: String?
    
    enum CodingKeys: String, CodingKey {
        case stockLevel
        case stockLevelStatus
    }
    
    public init(stockLevel: Int?, stockLevelStatus: String?) {
        self.stockLevel = stockLevel
        self.stockLevelStatus = stockLevelStatus
    }
}

// MARK: ProducttStock convenience initializers and mutators

public extension ProducttStock {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttStock.self, from: data)
        self.init(stockLevel: me.stockLevel, stockLevelStatus: me.stockLevelStatus)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        stockLevel: Int?? = nil,
        stockLevelStatus: String?? = nil
    ) -> ProducttStock {
        return ProducttStock(
            stockLevel: stockLevel ?? self.stockLevel,
            stockLevelStatus: stockLevelStatus ?? self.stockLevelStatus
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttVariantOptionQualifier
public class ProducttVariantOptionQualifier: Codable {
    public let name: String?
    public let qualifier: String?
    public let value: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case qualifier
        case value
    }
    
    public init(name: String?, qualifier: String?, value: String?) {
        self.name = name
        self.qualifier = qualifier
        self.value = value
    }
}

// MARK: ProducttVariantOptionQualifier convenience initializers and mutators

public extension ProducttVariantOptionQualifier {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttVariantOptionQualifier.self, from: data)
        self.init(name: me.name, qualifier: me.qualifier, value: me.value)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        name: String?? = nil,
        qualifier: String?? = nil,
        value: String?? = nil
    ) -> ProducttVariantOptionQualifier {
        return ProducttVariantOptionQualifier(
            name: name ?? self.name,
            qualifier: qualifier ?? self.qualifier,
            value: value ?? self.value
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttCategory
public class ProducttCategory: Codable {
    public let code: String?
    public let name: String?
    public let url: String?
    public let image: ProducttCategoryImage?
    
    enum CodingKeys: String, CodingKey {
        case code
        case name
        case url
        case image
    }
    
    public init(code: String?, name: String?, url: String?, image: ProducttCategoryImage?) {
        self.code = code
        self.name = name
        self.url = url
        self.image = image
    }
}

// MARK: ProducttCategory convenience initializers and mutators

public extension ProducttCategory {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttCategory.self, from: data)
        self.init(code: me.code, name: me.name, url: me.url, image: me.image)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        code: String?? = nil,
        name: String?? = nil,
        url: String?? = nil,
        image: ProducttCategoryImage?? = nil
    ) -> ProducttCategory {
        return ProducttCategory(
            code: code ?? self.code,
            name: name ?? self.name,
            url: url ?? self.url,
            image: image ?? self.image
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttCategoryImage
public class ProducttCategoryImage: Codable {
    public let format: String?
    public let url: String?
    
    enum CodingKeys: String, CodingKey {
        case format
        case url
    }
    
    public init(format: String?, url: String?) {
        self.format = format
        self.url = url
    }
}

// MARK: ProducttCategoryImage convenience initializers and mutators

public extension ProducttCategoryImage {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttCategoryImage.self, from: data)
        self.init(format: me.format, url: me.url)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        format: String?? = nil,
        url: String?? = nil
    ) -> ProducttCategoryImage {
        return ProducttCategoryImage(
            format: format ?? self.format,
            url: url ?? self.url
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttClassification
public class ProducttClassification: Codable {
    public let code: String?
    public let features: [ProducttFeature]?
    public let name: String?
    
    enum CodingKeys: String, CodingKey {
        case code
        case features
        case name
    }
    
    public init(code: String?, features: [ProducttFeature]?, name: String?) {
        self.code = code
        self.features = features
        self.name = name
    }
}

// MARK: ProducttClassification convenience initializers and mutators

public extension ProducttClassification {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttClassification.self, from: data)
        self.init(code: me.code, features: me.features, name: me.name)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        code: String?? = nil,
        features: [ProducttFeature]?? = nil,
        name: String?? = nil
    ) -> ProducttClassification {
        return ProducttClassification(
            code: code ?? self.code,
            features: features ?? self.features,
            name: name ?? self.name
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttFeature
public class ProducttFeature: Codable {
    public let code: String?
    public let comparable: Bool?
    public let featureValues: [ProducttFeatureValue]?
    public let name: String?
    public let range: Bool?
    
    enum CodingKeys: String, CodingKey {
        case code
        case comparable
        case featureValues
        case name
        case range
    }
    
    public init(code: String?, comparable: Bool?, featureValues: [ProducttFeatureValue]?, name: String?, range: Bool?) {
        self.code = code
        self.comparable = comparable
        self.featureValues = featureValues
        self.name = name
        self.range = range
    }
}

// MARK: ProducttFeature convenience initializers and mutators

public extension ProducttFeature {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttFeature.self, from: data)
        self.init(code: me.code, comparable: me.comparable, featureValues: me.featureValues, name: me.name, range: me.range)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        code: String?? = nil,
        comparable: Bool?? = nil,
        featureValues: [ProducttFeatureValue]?? = nil,
        name: String?? = nil,
        range: Bool?? = nil
    ) -> ProducttFeature {
        return ProducttFeature(
            code: code ?? self.code,
            comparable: comparable ?? self.comparable,
            featureValues: featureValues ?? self.featureValues,
            name: name ?? self.name,
            range: range ?? self.range
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttFeatureValue
public class ProducttFeatureValue: Codable {
    public let value: String?
    
    enum CodingKeys: String, CodingKey {
        case value
    }
    
    public init(value: String?) {
        self.value = value
    }
}

// MARK: ProducttFeatureValue convenience initializers and mutators

public extension ProducttFeatureValue {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttFeatureValue.self, from: data)
        self.init(value: me.value)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        value: String?? = nil
    ) -> ProducttFeatureValue {
        return ProducttFeatureValue(
            value: value ?? self.value
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttImageElement
public class ProducttImageElement: Codable {
    public let altText: String?
    public let format: String?
    public let imageType: String?
    public let url: String?
    public let galleryIndex: Int?
    
    enum CodingKeys: String, CodingKey {
        case altText
        case format
        case imageType
        case url
        case galleryIndex
    }
    
    public init(altText: String?, format: String?, imageType: String?, url: String?, galleryIndex: Int?) {
        self.altText = altText
        self.format = format
        self.imageType = imageType
        self.url = url
        self.galleryIndex = galleryIndex
    }
}

// MARK: ProducttImageElement convenience initializers and mutators

public extension ProducttImageElement {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttImageElement.self, from: data)
        self.init(altText: me.altText, format: me.format, imageType: me.imageType, url: me.url, galleryIndex: me.galleryIndex)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        altText: String?? = nil,
        format: String?? = nil,
        imageType: String?? = nil,
        url: String?? = nil,
        galleryIndex: Int?? = nil
    ) -> ProducttImageElement {
        return ProducttImageElement(
            altText: altText ?? self.altText,
            format: format ?? self.format,
            imageType: imageType ?? self.imageType,
            url: url ?? self.url,
            galleryIndex: galleryIndex ?? self.galleryIndex
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttPriceRange
public class ProducttPriceRange: Codable {
    public let maxPrice: ProducttMaxPriceClass?
    public let minPrice: ProducttMaxPriceClass?
    
    enum CodingKeys: String, CodingKey {
        case maxPrice
        case minPrice
    }
    
    public init(maxPrice: ProducttMaxPriceClass?, minPrice: ProducttMaxPriceClass?) {
        self.maxPrice = maxPrice
        self.minPrice = minPrice
    }
}

// MARK: ProducttPriceRange convenience initializers and mutators

public extension ProducttPriceRange {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttPriceRange.self, from: data)
        self.init(maxPrice: me.maxPrice, minPrice: me.minPrice)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        maxPrice: ProducttMaxPriceClass?? = nil,
        minPrice: ProducttMaxPriceClass?? = nil
    ) -> ProducttPriceRange {
        return ProducttPriceRange(
            maxPrice: maxPrice ?? self.maxPrice,
            minPrice: minPrice ?? self.minPrice
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttMaxPriceClass
public class ProducttMaxPriceClass: Codable {
    public let currencyISO: String?
    public let value: Double?
    
    enum CodingKeys: String, CodingKey {
        case currencyISO
        case value
    }
    
    public init(currencyISO: String?, value: Double?) {
        self.currencyISO = currencyISO
        self.value = value
    }
}

// MARK: ProducttMaxPriceClass convenience initializers and mutators

public extension ProducttMaxPriceClass {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttMaxPriceClass.self, from: data)
        self.init(currencyISO: me.currencyISO, value: me.value)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        currencyISO: String?? = nil,
        value: Double?? = nil
    ) -> ProducttMaxPriceClass {
        return ProducttMaxPriceClass(
            currencyISO: currencyISO ?? self.currencyISO,
            value: value ?? self.value
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttProductReference
public class ProducttProductReference: Codable {
    public let preselected: Bool?
    public let referenceType: String?
    public let target: ProducttCategory?
    
    enum CodingKeys: String, CodingKey {
        case preselected
        case referenceType
        case target
    }
    
    public init(preselected: Bool?, referenceType: String?, target: ProducttCategory?) {
        self.preselected = preselected
        self.referenceType = referenceType
        self.target = target
    }
}

// MARK: ProducttProductReference convenience initializers and mutators

public extension ProducttProductReference {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttProductReference.self, from: data)
        self.init(preselected: me.preselected, referenceType: me.referenceType, target: me.target)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        preselected: Bool?? = nil,
        referenceType: String?? = nil,
        target: ProducttCategory?? = nil
    ) -> ProducttProductReference {
        return ProducttProductReference(
            preselected: preselected ?? self.preselected,
            referenceType: referenceType ?? self.referenceType,
            target: target ?? self.target
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttReview
public class ProducttReview: Codable {
    public let alias: String?
    public let comment: String?
    public let date: String?
    public let headline: String?
    public let id: String?
    public let principal: ProducttPrincipal?
    public let rating: Double?
    
    enum CodingKeys: String, CodingKey {
        case alias
        case comment
        case date
        case headline
        case id
        case principal
        case rating
    }
    
    public init(alias: String?, comment: String?, date: String?, headline: String?, id: String?, principal: ProducttPrincipal?, rating: Double?) {
        self.alias = alias
        self.comment = comment
        self.date = date
        self.headline = headline
        self.id = id
        self.principal = principal
        self.rating = rating
    }
}

// MARK: ProducttReview convenience initializers and mutators

public extension ProducttReview {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttReview.self, from: data)
        self.init(alias: me.alias, comment: me.comment, date: me.date, headline: me.headline, id: me.id, principal: me.principal, rating: me.rating)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        alias: String?? = nil,
        comment: String?? = nil,
        date: String?? = nil,
        headline: String?? = nil,
        id: String?? = nil,
        principal: ProducttPrincipal?? = nil,
        rating: Double?? = nil
    ) -> ProducttReview {
        return ProducttReview(
            alias: alias ?? self.alias,
            comment: comment ?? self.comment,
            date: date ?? self.date,
            headline: headline ?? self.headline,
            id: id ?? self.id,
            principal: principal ?? self.principal,
            rating: rating ?? self.rating
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ProducttPrincipal
public class ProducttPrincipal: Codable {
    public let name: String?
    public let uid: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case uid
    }
    
    public init(name: String?, uid: String?) {
        self.name = name
        self.uid = uid
    }
}

// MARK: ProducttPrincipal convenience initializers and mutators

public extension ProducttPrincipal {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ProducttPrincipal.self, from: data)
        self.init(name: me.name, uid: me.uid)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        name: String?? = nil,
        uid: String?? = nil
    ) -> ProducttPrincipal {
        return ProducttPrincipal(
            name: name ?? self.name,
            uid: uid ?? self.uid
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

public typealias ProductDetailss = [ProductDetails]

public extension Array where Element == ProductDetailss.Element {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(ProductDetailss.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

