//
//  UserMessages.swift
//  Domain
//
//  Created by Saja Hammad on 9/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// MARK: - UserMessages
public struct UserMessages: Codable,Identifiable{
  public var identity: String{
      get{
        return messages?.first?.uid ?? ""
      }
  }
    public let messages: [Message]?
    public let paginationMessages: PaginationMessages?
    public let sortsMessages: [SortsMessage]?

    public init(messages: [Message]?, paginationMessages: PaginationMessages?, sortsMessages: [SortsMessage]?) {
        self.messages = messages
        self.paginationMessages = paginationMessages
        self.sortsMessages = sortsMessages
    }
}

// MARK: - Message
public struct Message: Codable {
    public let body, notificationType, sentDate, subject: String?
    public let uid: String?

    public init(body: String?, notificationType: String?, sentDate: String?, subject: String?, uid: String?) {
        self.body = body
        self.notificationType = notificationType
        self.sentDate = sentDate
        self.subject = subject
        self.uid = uid
    }
}

// MARK: - PaginationMessages
public struct PaginationMessages: Codable {
    public let count: Int?
    public let hasNext, hasPrevious: Bool?
    public let page, totalCount, totalPages: Int?

    public init(count: Int?, hasNext: Bool?, hasPrevious: Bool?, page: Int?, totalCount: Int?, totalPages: Int?) {
        self.count = count
        self.hasNext = hasNext
        self.hasPrevious = hasPrevious
        self.page = page
        self.totalCount = totalCount
        self.totalPages = totalPages
    }
}

// MARK: - SortsMessage
public struct SortsMessage: Codable {
    public let asc: Bool?
    public let code: String?

    public init(asc: Bool?, code: String?) {
        self.asc = asc
        self.code = code
    }
}
