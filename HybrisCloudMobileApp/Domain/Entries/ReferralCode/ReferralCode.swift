//
//  ReferralCode.swift
//  Domain
//
//  Created by admin on 20/06/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation

// MARK: - ReferralCode
public struct ReferralCode: Codable, Identifiable {

    public var code: String?
    public var customers: [Customer]?
    public var histories: [History]?
    public var newAppliedAmount: Int?
    public var percentage: Double?
    public var totalAmount: Float?
    public var valid: Bool?
    public var identity: String{
        return code ?? ""
    }

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case customers = "customers"
        case histories = "histories"
        case newAppliedAmount = "newAppliedAmount"
        case percentage = "percentage"
        case totalAmount = "totalAmount"
        case valid = "valid"
    }

    public init(code: String?, customers: [Customer]?, histories: [History]?, newAppliedAmount: Int?, percentage: Double?, totalAmount: Float?, valid: Bool?) {
        self.code = code
        self.customers = customers
        self.histories = histories
        self.newAppliedAmount = newAppliedAmount
        self.percentage = percentage
        self.totalAmount = totalAmount
        self.valid = valid
    }
}

// MARK: - Customer
public struct Customer: Codable {
    public var appliedReferralCode: String?
    public var currency: Currency?
    public var customerID: String?
    public var deactivationDate: String?
    public var defaultAddress: DefaultAddress?
    public var displayUid: String?
    public var firstName: String?
    public var language: Currency?
    public var lastName: String?
    public var mobileCountry: Country?
    public var mobileNumber: String?
    public var name: String?
    public var title: String?
    public var titleCode: String?
    public var uid: String?

    enum CodingKeys: String, CodingKey {
        case appliedReferralCode = "appliedReferralCode"
        case currency = "currency"
        case customerID = "customerId"
        case deactivationDate = "deactivationDate"
        case defaultAddress = "defaultAddress"
        case displayUid = "displayUid"
        case firstName = "firstName"
        case language = "language"
        case lastName = "lastName"
        case mobileCountry = "mobileCountry"
        case mobileNumber = "mobileNumber"
        case name = "name"
        case title = "title"
        case titleCode = "titleCode"
        case uid = "uid"
    }

    public init(appliedReferralCode: String?, currency: Currency?, customerID: String?, deactivationDate: String?, defaultAddress: DefaultAddress?, displayUid: String?, firstName: String?, language: Currency?, lastName: String?, mobileCountry: Country?, mobileNumber: String?, name: String?, title: String?, titleCode: String?, uid: String?) {
        self.appliedReferralCode = appliedReferralCode
        self.currency = currency
        self.customerID = customerID
        self.deactivationDate = deactivationDate
        self.defaultAddress = defaultAddress
        self.displayUid = displayUid
        self.firstName = firstName
        self.language = language
        self.lastName = lastName
        self.mobileCountry = mobileCountry
        self.mobileNumber = mobileNumber
        self.name = name
        self.title = title
        self.titleCode = titleCode
        self.uid = uid
    }
}

// MARK: - History
public struct History: Codable {
    public var amount: Float?
    public var creationDate: String?
    public var customer: Customer?
    
    enum CodingKeys: String, CodingKey {
        case amount = "amount"
        case creationDate = "creationDate"
        case customer = "customer"
    }

    public init(amount: Float?, creationDate: String?, customer: Customer?) {
        self.amount = amount
        self.creationDate = creationDate
        self.customer = customer
    }
}
