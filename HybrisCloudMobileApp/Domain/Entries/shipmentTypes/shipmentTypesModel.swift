//
//  shipmentTypesModel.swift
//  Domain
//
//  Created by khalil on 12/2/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation

// MARK: - ShipmentTypesModel
public struct ShipmentTypesModel: Codable , Identifiable {
    public var identity: String{
        return "ShipmentTypesModel"
    }
    public var supportedShipmentTypes: [SupportedShipmentType]?

    enum CodingKeys: String, CodingKey {
        case supportedShipmentTypes
    }

    public init(supportedShipmentTypes: [SupportedShipmentType]?) {
        self.supportedShipmentTypes = supportedShipmentTypes
    }
}

// MARK: - SupportedShipmentType
public struct SupportedShipmentType: Codable {
    public var code: String?
    public var name: String?

    enum CodingKeys: String, CodingKey {
        case code
        case name
    }

    public init(code: String?, name: String?) {
        self.code = code
        self.name = name
    }
}
