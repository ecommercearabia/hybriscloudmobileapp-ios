//
//  SelctedShipmentTypesModel.swift
//  Domain
//
//  Created by khalil on 12/2/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation

// MARK: - ShipmentSelctedShipmentTypesModel
public struct ShipmentSelctedShipmentTypesModel: Codable {
    public var cartModifications: [ShipmentCartModification]?

    enum CodingKeys: String, CodingKey {
        case cartModifications
    }

    public init(cartModifications: [ShipmentCartModification]?) {
        self.cartModifications = cartModifications
    }
}

// MARK: - ShipmentCartModification
public struct ShipmentCartModification: Codable {
    public var entry: ShipmentEntry?
    public var quantity: Int?
    public var quantityAdded: Int?
    public var statusCode: String?

    enum CodingKeys: String, CodingKey {
        case entry
        case quantity
        case quantityAdded
        case statusCode
    }

    public init(entry: ShipmentEntry?, quantity: Int?, quantityAdded: Int?, statusCode: String?) {
        self.entry = entry
        self.quantity = quantity
        self.quantityAdded = quantityAdded
        self.statusCode = statusCode
    }
}

// MARK: - ShipmentEntry
public struct ShipmentEntry: Codable {
    public var basePrice: ShipmentBasePrice?
    public var configurationInfos: [JSONAny]?
    public var entryNumber: Int?
    public var product: ShipmentProduct?
    public var quantity: Int?
    public var totalPrice: ShipmentBasePrice?
    public var updateable: Bool?

    enum CodingKeys: String, CodingKey {
        case basePrice
        case configurationInfos
        case entryNumber
        case product
        case quantity
        case totalPrice
        case updateable
    }

    public init(basePrice: ShipmentBasePrice?, configurationInfos: [JSONAny]?, entryNumber: Int?, product: ShipmentProduct?, quantity: Int?, totalPrice: ShipmentBasePrice?, updateable: Bool?) {
        self.basePrice = basePrice
        self.configurationInfos = configurationInfos
        self.entryNumber = entryNumber
        self.product = product
        self.quantity = quantity
        self.totalPrice = totalPrice
        self.updateable = updateable
    }
}

// MARK: - ShipmentBasePrice
public struct ShipmentBasePrice: Codable {
    public var currencyISO: String?
    public var formattedValue: String?
    public var priceType: String?
    public var value: Double?

    enum CodingKeys: String, CodingKey {
        case currencyISO
        case formattedValue
        case priceType
        case value
    }

    public init(currencyISO: String?, formattedValue: String?, priceType: String?, value: Double?) {
        self.currencyISO = currencyISO
        self.formattedValue = formattedValue
        self.priceType = priceType
        self.value = value
    }
}

// MARK: - ShipmentProduct
public struct ShipmentProduct: Codable {
    public var availableForPickup: Bool?
    public var baseOptions: [ShipmentBaseOption]?
    public var baseProduct: String?
    public var categories: [ShipmentCategory]?
    public var code: String?
    public var configurable: Bool?
    public var countryOfOrigin: String?
    public var countryOfOriginIsocode: String?
    public var glutenFree: Bool?
    public var images: [ShipmentImageElement]?
    public var name: String?
    public var organic: Bool?
    public var purchasable: Bool?
    public var stock: ShipmentStock?
    public var unitOfMeasure: String?
    public var url: String?
    public var vegan: Bool?

    enum CodingKeys: String, CodingKey {
        case availableForPickup
        case baseOptions
        case baseProduct
        case categories
        case code
        case configurable
        case countryOfOrigin
        case countryOfOriginIsocode
        case glutenFree
        case images
        case name
        case organic
        case purchasable
        case stock
        case unitOfMeasure
        case url
        case vegan
    }

    public init(availableForPickup: Bool?, baseOptions: [ShipmentBaseOption]?, baseProduct: String?, categories: [ShipmentCategory]?, code: String?, configurable: Bool?, countryOfOrigin: String?, countryOfOriginIsocode: String?, glutenFree: Bool?, images: [ShipmentImageElement]?, name: String?, organic: Bool?, purchasable: Bool?, stock: ShipmentStock?, unitOfMeasure: String?, url: String?, vegan: Bool?) {
        self.availableForPickup = availableForPickup
        self.baseOptions = baseOptions
        self.baseProduct = baseProduct
        self.categories = categories
        self.code = code
        self.configurable = configurable
        self.countryOfOrigin = countryOfOrigin
        self.countryOfOriginIsocode = countryOfOriginIsocode
        self.glutenFree = glutenFree
        self.images = images
        self.name = name
        self.organic = organic
        self.purchasable = purchasable
        self.stock = stock
        self.unitOfMeasure = unitOfMeasure
        self.url = url
        self.vegan = vegan
    }
}

// MARK: - ShipmentBaseOption
public struct ShipmentBaseOption: Codable {
    public var selected: ShipmentSelected?
    public var variantType: String?

    enum CodingKeys: String, CodingKey {
        case selected
        case variantType
    }

    public init(selected: ShipmentSelected?, variantType: String?) {
        self.selected = selected
        self.variantType = variantType
    }
}

// MARK: - ShipmentSelected
public struct ShipmentSelected: Codable {
    public var code: String?
    public var priceData: ShipmentBasePrice?
    public var stock: ShipmentStock?
    public var url: String?
    public var variantOptionQualifiers: [ShipmentVariantOptionQualifier]?

    enum CodingKeys: String, CodingKey {
        case code
        case priceData
        case stock
        case url
        case variantOptionQualifiers
    }

    public init(code: String?, priceData: ShipmentBasePrice?, stock: ShipmentStock?, url: String?, variantOptionQualifiers: [ShipmentVariantOptionQualifier]?) {
        self.code = code
        self.priceData = priceData
        self.stock = stock
        self.url = url
        self.variantOptionQualifiers = variantOptionQualifiers
    }
}

// MARK: - ShipmentStock
public struct ShipmentStock: Codable {
    public var stockLevel: Int?
    public var stockLevelStatus: String?

    enum CodingKeys: String, CodingKey {
        case stockLevel
        case stockLevelStatus
    }

    public init(stockLevel: Int?, stockLevelStatus: String?) {
        self.stockLevel = stockLevel
        self.stockLevelStatus = stockLevelStatus
    }
}

// MARK: - ShipmentVariantOptionQualifier
public struct ShipmentVariantOptionQualifier: Codable {
    public var name: String?
    public var qualifier: String?
    public var value: String?

    enum CodingKeys: String, CodingKey {
        case name
        case qualifier
        case value
    }

    public init(name: String?, qualifier: String?, value: String?) {
        self.name = name
        self.qualifier = qualifier
        self.value = value
    }
}

// MARK: - ShipmentCategory
public struct ShipmentCategory: Codable {
    public var code: String?
    public var image: ShipmentCategoryImage?
    public var name: String?
    public var url: String?

    enum CodingKeys: String, CodingKey {
        case code
        case image
        case name
        case url
    }

    public init(code: String?, image: ShipmentCategoryImage?, name: String?, url: String?) {
        self.code = code
        self.image = image
        self.name = name
        self.url = url
    }
}

// MARK: - ShipmentCategoryImage
public struct ShipmentCategoryImage: Codable {
    public var format: String?
    public var url: String?

    enum CodingKeys: String, CodingKey {
        case format
        case url
    }

    public init(format: String?, url: String?) {
        self.format = format
        self.url = url
    }
}

// MARK: - ShipmentImageElement
public struct ShipmentImageElement: Codable {
    public var altText: String?
    public var format: String?
    public var imageType: String?
    public var url: String?

    enum CodingKeys: String, CodingKey {
        case altText
        case format
        case imageType
        case url
    }

    public init(altText: String?, format: String?, imageType: String?, url: String?) {
        self.altText = altText
        self.format = format
        self.imageType = imageType
        self.url = url
    }
}

