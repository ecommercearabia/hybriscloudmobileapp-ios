//
//  currentShipmentTypeModel.swift
//  Domain
//
//  Created by khalil on 12/2/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation

// MARK: - CurrentShipmentTypeModel
public struct CurrentShipmentTypeModel: Codable {
    public var code: String?
    public var name: String?

    enum CodingKeys: String, CodingKey {
        case code
        case name
    }

    public init(code: String?, name: String?) {
        self.code = code
        self.name = name
    }
}
