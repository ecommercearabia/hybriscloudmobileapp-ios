//
//  CartAddedToCartModel.swift
//  Domain
//
//  Created by ahmadSaleh on 8/7/20.
//  Copyright © 2020 Erabia. All rights reserved.
//
 import Foundation

// MARK: - AddedToCartModel
public struct AddedToCartModel: Codable ,Identifiable{
    public var identity: String{
        return statusCode ?? ""
    }

     public let entry: AddedEntry?
        public let quantity, quantityAdded: Int?
        public let statusCode: String?

        public init(entry: AddedEntry?, quantity: Int?, quantityAdded: Int?, statusCode: String?) {
            self.entry = entry
            self.quantity = quantity
            self.quantityAdded = quantityAdded
            self.statusCode = statusCode
        }
    }

    // MARK: - AddedEntry
    public struct AddedEntry: Codable {
        public let basePrice: AddedBasePrice?
        public let configurationInfos: [JSONAny]?
        public let entryNumber: Int?
        public let product: AddedProduct?
        public let quantity: Int?
        public let totalPrice: AddedBasePrice?
        public let updateable: Bool?

        public init(basePrice: AddedBasePrice?, configurationInfos: [JSONAny]?, entryNumber: Int?, product: AddedProduct?, quantity: Int?, totalPrice: AddedBasePrice?, updateable: Bool?) {
            self.basePrice = basePrice
            self.configurationInfos = configurationInfos
            self.entryNumber = entryNumber
            self.product = product
            self.quantity = quantity
            self.totalPrice = totalPrice
            self.updateable = updateable
        }
    }

    // MARK: - AddedBasePrice
    public struct AddedBasePrice: Codable {
        public let currencyISO, formattedValue, priceType: String?
        public let value: Double?

        enum CodingKeys: String, CodingKey {
            case currencyISO = "currencyIso"
            case formattedValue, priceType, value
        }

        public init(currencyISO: String?, formattedValue: String?, priceType: String?, value: Double?) {
            self.currencyISO = currencyISO
            self.formattedValue = formattedValue
            self.priceType = priceType
            self.value = value
        }
    }

    // MARK: - AddedProduct
    public struct AddedProduct: Codable {
        public let availableForPickup: Bool?
        public let baseOptions: [AddedBaseOption]?
        public let baseProduct: String?
        public let categories: [AddedCategory]?
        public let code: String?
        public let configurable: Bool?
        public let countryOfOrigin, countryOfOriginIsocode: String?
        public let images: [AddedImageElement]?
        public let name: String?
        public let purchasable: Bool?
        public let stock: AddedStock?
        public let unitOfMeasure, url: String?

        public init(availableForPickup: Bool?, baseOptions: [AddedBaseOption]?, baseProduct: String?, categories: [AddedCategory]?, code: String?, configurable: Bool?, countryOfOrigin: String?, countryOfOriginIsocode: String?, images: [AddedImageElement]?, name: String?, purchasable: Bool?, stock: AddedStock?, unitOfMeasure: String?, url: String?) {
            self.availableForPickup = availableForPickup
            self.baseOptions = baseOptions
            self.baseProduct = baseProduct
            self.categories = categories
            self.code = code
            self.configurable = configurable
            self.countryOfOrigin = countryOfOrigin
            self.countryOfOriginIsocode = countryOfOriginIsocode
            self.images = images
            self.name = name
            self.purchasable = purchasable
            self.stock = stock
            self.unitOfMeasure = unitOfMeasure
            self.url = url
        }
    }

    // MARK: - AddedBaseOption
    public struct AddedBaseOption: Codable {
        public let selected: AddedSelected?
        public let variantType: String?

        public init(selected: AddedSelected?, variantType: String?) {
            self.selected = selected
            self.variantType = variantType
        }
    }

    // MARK: - AddedSelected
    public struct AddedSelected: Codable {
        public let code: String?
        public let priceData: AddedBasePrice?
        public let stock: AddedStock?
        public let url: String?
        public let variantOptionQualifiers: [AddedVariantOptionQualifier]?

        public init(code: String?, priceData: AddedBasePrice?, stock: AddedStock?, url: String?, variantOptionQualifiers: [AddedVariantOptionQualifier]?) {
            self.code = code
            self.priceData = priceData
            self.stock = stock
            self.url = url
            self.variantOptionQualifiers = variantOptionQualifiers
        }
    }

    // MARK: - AddedStock
    public struct AddedStock: Codable {
        public let stockLevel: Int?
        public let stockLevelStatus: String?

        public init(stockLevel: Int?, stockLevelStatus: String?) {
            self.stockLevel = stockLevel
            self.stockLevelStatus = stockLevelStatus
        }
    }

    // MARK: - AddedVariantOptionQualifier
    public struct AddedVariantOptionQualifier: Codable {
        public let name, qualifier, value: String?

        public init(name: String?, qualifier: String?, value: String?) {
            self.name = name
            self.qualifier = qualifier
            self.value = value
        }
    }

    // MARK: - AddedCategory
    public struct AddedCategory: Codable {
        public let code: String?
        public let image: AddedCategoryImage?
        public let name, url: String?

        public init(code: String?, image: AddedCategoryImage?, name: String?, url: String?) {
            self.code = code
            self.image = image
            self.name = name
            self.url = url
        }
    }

    // MARK: - AddedCategoryImage
    public struct AddedCategoryImage: Codable {
        public let format, url: String?

        public init(format: String?, url: String?) {
            self.format = format
            self.url = url
        }
    }

    // MARK: - AddedImageElement
    public struct AddedImageElement: Codable {
        public let altText, format, imageType, url: String?

        public init(altText: String?, format: String?, imageType: String?, url: String?) {
            self.altText = altText
            self.format = format
            self.imageType = imageType
            self.url = url
        }
    }
