//
//  addToCartProductModel.swift
//  Domain
//
//  Created by ahmadSaleh on 8/7/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
// MARK: - productAddToCartProductModel
public struct AddToCartProductModel: Codable {
    public let basePrice: productBasePrice?
    public let cancellableQuantity: Int?
    public let cancelledItemsPrice: productBasePrice?
    public let configurationInfos: [productConfigurationInfo]?
    public let deliveryMode: productDeliveryMode?
    public let deliveryPointOfService: productDeliveryPointOfService?
    public let entryNumber: Int?
    public let product: ProductDetails?
    public let quantity, quantityAllocated, quantityCancelled, quantityPending: Int?
    public let quantityReturned, quantityShipped, quantityUnallocated, returnableQuantity: Int?
    public let returnedItemsPrice, totalPrice: productBasePrice?
    public let updateable: Bool?
    public let url: String?

    public init(basePrice: productBasePrice?, cancellableQuantity: Int?, cancelledItemsPrice: productBasePrice?, configurationInfos: [productConfigurationInfo]?, deliveryMode: productDeliveryMode?, deliveryPointOfService: productDeliveryPointOfService?, entryNumber: Int?, product: ProductDetails?, quantity: Int?, quantityAllocated: Int?, quantityCancelled: Int?, quantityPending: Int?, quantityReturned: Int?, quantityShipped: Int?, quantityUnallocated: Int?, returnableQuantity: Int?, returnedItemsPrice: productBasePrice?, totalPrice: productBasePrice?, updateable: Bool?, url: String?) {
        self.basePrice = basePrice
        self.cancellableQuantity = cancellableQuantity
        self.cancelledItemsPrice = cancelledItemsPrice
        self.configurationInfos = configurationInfos
        self.deliveryMode = deliveryMode
        self.deliveryPointOfService = deliveryPointOfService
        self.entryNumber = entryNumber
        self.product = product
        self.quantity = quantity
        self.quantityAllocated = quantityAllocated
        self.quantityCancelled = quantityCancelled
        self.quantityPending = quantityPending
        self.quantityReturned = quantityReturned
        self.quantityShipped = quantityShipped
        self.quantityUnallocated = quantityUnallocated
        self.returnableQuantity = returnableQuantity
        self.returnedItemsPrice = returnedItemsPrice
        self.totalPrice = totalPrice
        self.updateable = updateable
        self.url = url
    }
}

// MARK: - productBasePrice
public struct productBasePrice: Codable {
    public let currencyISO, formattedValue: String?
    public let maxQuantity, minQuantity: Int?
    public let priceType: String?
    public let value: Int?

    enum CodingKeys: String, CodingKey {
        case currencyISO = "currencyIso"
        case formattedValue, maxQuantity, minQuantity, priceType, value
    }

    public init(currencyISO: String?, formattedValue: String?, maxQuantity: Int?, minQuantity: Int?, priceType: String?, value: Int?) {
        self.currencyISO = currencyISO
        self.formattedValue = formattedValue
        self.maxQuantity = maxQuantity
        self.minQuantity = minQuantity
        self.priceType = priceType
        self.value = value
    }
}

// MARK: - productConfigurationInfo
public struct productConfigurationInfo: Codable {
    public let configurationLabel, configurationValue, configuratorType, status: String?

    public init(configurationLabel: String?, configurationValue: String?, configuratorType: String?, status: String?) {
        self.configurationLabel = configurationLabel
        self.configurationValue = configurationValue
        self.configuratorType = configuratorType
        self.status = status
    }
}

// MARK: - productDeliveryMode
public struct productDeliveryMode: Codable {
    public let code: String?
    public let deliveryCost: productBasePrice?
    public let deliveryModeDescription, name: String?

    enum CodingKeys: String, CodingKey {
        case code, deliveryCost
        case deliveryModeDescription = "description"
        case name
    }

    public init(code: String?, deliveryCost: productBasePrice?, deliveryModeDescription: String?, name: String?) {
        self.code = code
        self.deliveryCost = deliveryCost
        self.deliveryModeDescription = deliveryModeDescription
        self.name = name
    }
}

// MARK: - productDeliveryPointOfService
public struct productDeliveryPointOfService: Codable {
    public let address: productAddress?
    public let deliveryPointOfServiceDescription, displayName: String?
    public let distanceKM: Int?
    public let features: productFeatures?
    public let formattedDistance: String?
    public let geoPoint: productGeoPoint?
    public let mapIcon: productMapIcon?
    public let name: String?
    public let openingHours: productOpeningHours?
    public let storeContent: String?
    public let storeImages: [productMapIcon]?
    public let url: String?
    public let warehouseCodes: [String]?

    enum CodingKeys: String, CodingKey {
        case address
        case deliveryPointOfServiceDescription = "description"
        case displayName
        case distanceKM = "distanceKm"
        case features, formattedDistance, geoPoint, mapIcon, name, openingHours, storeContent, storeImages, url, warehouseCodes
    }

    public init(address: productAddress?, deliveryPointOfServiceDescription: String?, displayName: String?, distanceKM: Int?, features: productFeatures?, formattedDistance: String?, geoPoint: productGeoPoint?, mapIcon: productMapIcon?, name: String?, openingHours: productOpeningHours?, storeContent: String?, storeImages: [productMapIcon]?, url: String?, warehouseCodes: [String]?) {
        self.address = address
        self.deliveryPointOfServiceDescription = deliveryPointOfServiceDescription
        self.displayName = displayName
        self.distanceKM = distanceKM
        self.features = features
        self.formattedDistance = formattedDistance
        self.geoPoint = geoPoint
        self.mapIcon = mapIcon
        self.name = name
        self.openingHours = openingHours
        self.storeContent = storeContent
        self.storeImages = storeImages
        self.url = url
        self.warehouseCodes = warehouseCodes
    }
}

// MARK: - productAddress
public struct productAddress: Codable {
    public let addressName: String?
    public let area: productArea?
    public let cellphone: String?
    public let city: productCity?
    public let companyName: String?
    public let country: productCountry?
    public let defaultAddress: Bool?
    public let district, email, firstName, formattedAddress: String?
    public let id, lastName, line1, line2: String?
    public let mobileCountry: productCountry?
    public let mobileNumber, nearestLandmark, phone, postalCode: String?
    public let region: productRegion?
    public let shippingAddress: Bool?
    public let title, titleCode, town: String?
    public let visibleInAddressBook: Bool?

    public init(addressName: String?, area: productArea?, cellphone: String?, city: productCity?, companyName: String?, country: productCountry?, defaultAddress: Bool?, district: String?, email: String?, firstName: String?, formattedAddress: String?, id: String?, lastName: String?, line1: String?, line2: String?, mobileCountry: productCountry?, mobileNumber: String?, nearestLandmark: String?, phone: String?, postalCode: String?, region: productRegion?, shippingAddress: Bool?, title: String?, titleCode: String?, town: String?, visibleInAddressBook: Bool?) {
        self.addressName = addressName
        self.area = area
        self.cellphone = cellphone
        self.city = city
        self.companyName = companyName
        self.country = country
        self.defaultAddress = defaultAddress
        self.district = district
        self.email = email
        self.firstName = firstName
        self.formattedAddress = formattedAddress
        self.id = id
        self.lastName = lastName
        self.line1 = line1
        self.line2 = line2
        self.mobileCountry = mobileCountry
        self.mobileNumber = mobileNumber
        self.nearestLandmark = nearestLandmark
        self.phone = phone
        self.postalCode = postalCode
        self.region = region
        self.shippingAddress = shippingAddress
        self.title = title
        self.titleCode = titleCode
        self.town = town
        self.visibleInAddressBook = visibleInAddressBook
    }
}

// MARK: - productArea
public struct productArea: Codable {
    public let code, name: String?

    public init(code: String?, name: String?) {
        self.code = code
        self.name = name
    }
}

// MARK: - productCity
public struct productCity: Codable {
    public let areas: [productArea]?
    public let code, name: String?

    public init(areas: [productArea]?, code: String?, name: String?) {
        self.areas = areas
        self.code = code
        self.name = name
    }
}

// MARK: - productCountry
public struct productCountry: Codable {
    public let isdcode, isocode, name: String?

    public init(isdcode: String?, isocode: String?, name: String?) {
        self.isdcode = isdcode
        self.isocode = isocode
        self.name = name
    }
}

// MARK: - productRegion
public struct productRegion: Codable {
    public let countryISO, isocode, isocodeShort, name: String?

    enum CodingKeys: String, CodingKey {
        case countryISO = "countryIso"
        case isocode, isocodeShort, name
    }

    public init(countryISO: String?, isocode: String?, isocodeShort: String?, name: String?) {
        self.countryISO = countryISO
        self.isocode = isocode
        self.isocodeShort = isocodeShort
        self.name = name
    }
}

// MARK: - productFeatures
public struct productFeatures: Codable {
    public let additionalProp1, additionalProp2, additionalProp3: String?

    public init(additionalProp1: String?, additionalProp2: String?, additionalProp3: String?) {
        self.additionalProp1 = additionalProp1
        self.additionalProp2 = additionalProp2
        self.additionalProp3 = additionalProp3
    }
}

// MARK: - productGeoPoint
public struct productGeoPoint: Codable {
    public let latitude, longitude: Int?

    public init(latitude: Int?, longitude: Int?) {
        self.latitude = latitude
        self.longitude = longitude
    }
}

// MARK: - productMapIcon
public struct productMapIcon: Codable {
    public let altText, format: String?
    public let galleryIndex: Int?
    public let imageType, url: String?

    public init(altText: String?, format: String?, galleryIndex: Int?, imageType: String?, url: String?) {
        self.altText = altText
        self.format = format
        self.galleryIndex = galleryIndex
        self.imageType = imageType
        self.url = url
    }
}

// MARK: - productOpeningHours
public struct productOpeningHours: Codable {
    public let code, name: String?
    public let specialDayOpeningList: [productSpecialDayOpeningList]?
    public let weekDayOpeningList: [productWeekDayOpeningList]?

    public init(code: String?, name: String?, specialDayOpeningList: [productSpecialDayOpeningList]?, weekDayOpeningList: [productWeekDayOpeningList]?) {
        self.code = code
        self.name = name
        self.specialDayOpeningList = specialDayOpeningList
        self.weekDayOpeningList = weekDayOpeningList
    }
}

// MARK: - productSpecialDayOpeningList
public struct productSpecialDayOpeningList: Codable {
    public let closed: Bool?
    public let closingTime: productIngTime?
    public let comment, date, formattedDate, name: String?
    public let openingTime: productIngTime?

    public init(closed: Bool?, closingTime: productIngTime?, comment: String?, date: String?, formattedDate: String?, name: String?, openingTime: productIngTime?) {
        self.closed = closed
        self.closingTime = closingTime
        self.comment = comment
        self.date = date
        self.formattedDate = formattedDate
        self.name = name
        self.openingTime = openingTime
    }
}

// MARK: - productIngTime
public struct productIngTime: Codable {
    public let formattedHour: String?
    public let hour, minute: Int?

    public init(formattedHour: String?, hour: Int?, minute: Int?) {
        self.formattedHour = formattedHour
        self.hour = hour
        self.minute = minute
    }
}

// MARK: - productWeekDayOpeningList
public struct productWeekDayOpeningList: Codable {
    public let closed: Bool?
    public let closingTime, openingTime: productIngTime?
    public let weekDay: String?

    public init(closed: Bool?, closingTime: productIngTime?, openingTime: productIngTime?, weekDay: String?) {
        self.closed = closed
        self.closingTime = closingTime
        self.openingTime = openingTime
        self.weekDay = weekDay
    }
}

// MARK: - productProduct
//public struct productProduct: Codable {
//    public let availableForPickup: Bool?
//    public let averageRating: Int?
//    public let baseOptions: [productBaseOption]?
//    public let baseProduct: String?
//    public let categories: [productCategory]?
//    public let classifications: [productClassification]?
//    public let code: String?
//    public let configurable: Bool?
//    public let configuratorType, productDescription: String?
//    public let futureStocks: [productFutureStock]?
//    public let images: [productMapIcon]?
//    public let manufacturer: String?
//    public let multidimensional: Bool?
//    public let name: String?
//    public let numberOfReviews: Double?
//    public let potentialPromotions: [productPotentialPromotion]?
//    public let price: productBasePrice?
//    public let priceRange: productPriceRange?
//    public let productReferences: [productProductReference]?
//    public let purchasable: Bool?
//    public let reviews: [productReview]?
//    public let stock: productStock?
//    public let summary: String?
//    public let tags: [String]?
//    public let url: String?
//    public let variantMatrix: [productVariantMatrix]?
//    public let variantOptions: [productVariantOption]?
//    public let variantType: String?
//    public let volumePrices: [productBasePrice]?
//    public let volumePricesFlag: Bool?
//
//    enum CodingKeys: String, CodingKey {
//        case availableForPickup, averageRating, baseOptions, baseProduct, categories, classifications, code, configurable, configuratorType
//        case productDescription = "description"
//        case futureStocks, images, manufacturer, multidimensional, name, numberOfReviews, potentialPromotions, price, priceRange, productReferences, purchasable, reviews, stock, summary, tags, url, variantMatrix, variantOptions, variantType, volumePrices, volumePricesFlag
//    }
//
//    public init(availableForPickup: Bool?, averageRating: Int?, baseOptions: [productBaseOption]?, baseProduct: String?, categories: [productCategory]?, classifications: [productClassification]?, code: String?, configurable: Bool?, configuratorType: String?, productDescription: String?, futureStocks: [productFutureStock]?, images: [productMapIcon]?, manufacturer: String?, multidimensional: Bool?, name: String?, numberOfReviews: Double?, potentialPromotions: [productPotentialPromotion]?, price: productBasePrice?, priceRange: productPriceRange?, productReferences: [productProductReference]?, purchasable: Bool?, reviews: [productReview]?, stock: productStock?, summary: String?, tags: [String]?, url: String?, variantMatrix: [productVariantMatrix]?, variantOptions: [productVariantOption]?, variantType: String?, volumePrices: [productBasePrice]?, volumePricesFlag: Bool?) {
//        self.availableForPickup = availableForPickup
//        self.averageRating = averageRating
//        self.baseOptions = baseOptions
//        self.baseProduct = baseProduct
//        self.categories = categories
//        self.classifications = classifications
//        self.code = code
//        self.configurable = configurable
//        self.configuratorType = configuratorType
//        self.productDescription = productDescription
//        self.futureStocks = futureStocks
//        self.images = images
//        self.manufacturer = manufacturer
//        self.multidimensional = multidimensional
//        self.name = name
//        self.numberOfReviews = numberOfReviews
//        self.potentialPromotions = potentialPromotions
//        self.price = price
//        self.priceRange = priceRange
//        self.productReferences = productReferences
//        self.purchasable = purchasable
//        self.reviews = reviews
//        self.stock = stock
//        self.summary = summary
//        self.tags = tags
//        self.url = url
//        self.variantMatrix = variantMatrix
//        self.variantOptions = variantOptions
//        self.variantType = variantType
//        self.volumePrices = volumePrices
//        self.volumePricesFlag = volumePricesFlag
//    }
//}

// MARK: - productBaseOption
public struct productBaseOption: Codable {
    public let options: [productVariantOption]?
    public let selected: productVariantOption?
    public let variantType: String?

    public init(options: [productVariantOption]?, selected: productVariantOption?, variantType: String?) {
        self.options = options
        self.selected = selected
        self.variantType = variantType
    }
}

// MARK: - productVariantOption
public struct productVariantOption: Codable {
    public let code: String?
    public let priceData: productBasePrice?
    public let stock: productStock?
    public let url: String?
    public let variantOptionQualifiers: [productVariantOptionQualifier]?

    public init(code: String?, priceData: productBasePrice?, stock: productStock?, url: String?, variantOptionQualifiers: [productVariantOptionQualifier]?) {
        self.code = code
        self.priceData = priceData
        self.stock = stock
        self.url = url
        self.variantOptionQualifiers = variantOptionQualifiers
    }
}

// MARK: - productStock
public struct productStock: Codable {
    public let stockLevel: Int?
    public let stockLevelStatus: String?

    public init(stockLevel: Int?, stockLevelStatus: String?) {
        self.stockLevel = stockLevel
        self.stockLevelStatus = stockLevelStatus
    }
}

// MARK: - productVariantOptionQualifier
public struct productVariantOptionQualifier: Codable {
    public let image: productMapIcon?
    public let name, qualifier, value: String?

    public init(image: productMapIcon?, name: String?, qualifier: String?, value: String?) {
        self.image = image
        self.name = name
        self.qualifier = qualifier
        self.value = value
    }
}

// MARK: - productCategory
public struct productCategory: Codable {
    public let code: String?
    public let image: productMapIcon?
    public let name: String?
    public let plpPicture, plpPictureResponsive: productMapIcon?
    public let url: String?

    public init(code: String?, image: productMapIcon?, name: String?, plpPicture: productMapIcon?, plpPictureResponsive: productMapIcon?, url: String?) {
        self.code = code
        self.image = image
        self.name = name
        self.plpPicture = plpPicture
        self.plpPictureResponsive = plpPictureResponsive
        self.url = url
    }
}

// MARK: - productClassification
public struct productClassification: Codable {
    public let code: String?
    public let features: [productFeature]?
    public let name: String?

    public init(code: String?, features: [productFeature]?, name: String?) {
        self.code = code
        self.features = features
        self.name = name
    }
}

// MARK: - productFeature
public struct productFeature: Codable {
    public let code: String?
    public let comparable: Bool?
    public let featureDescription: String?
    public let featureUnit: productFeatureUnit?
    public let featureValues: [productFeatureValue]?
    public let name: String?
    public let range: Bool?
    public let type: String?

    enum CodingKeys: String, CodingKey {
        case code, comparable
        case featureDescription = "description"
        case featureUnit, featureValues, name, range, type
    }

    public init(code: String?, comparable: Bool?, featureDescription: String?, featureUnit: productFeatureUnit?, featureValues: [productFeatureValue]?, name: String?, range: Bool?, type: String?) {
        self.code = code
        self.comparable = comparable
        self.featureDescription = featureDescription
        self.featureUnit = featureUnit
        self.featureValues = featureValues
        self.name = name
        self.range = range
        self.type = type
    }
}

// MARK: - productFeatureUnit
public struct productFeatureUnit: Codable {
    public let name, symbol, unitType: String?

    public init(name: String?, symbol: String?, unitType: String?) {
        self.name = name
        self.symbol = symbol
        self.unitType = unitType
    }
}

// MARK: - productFeatureValue
public struct productFeatureValue: Codable {
    public let value: String?

    public init(value: String?) {
        self.value = value
    }
}

// MARK: - productFutureStock
public struct productFutureStock: Codable {
    public let date, formattedDate: String?
    public let stock: productStock?

    public init(date: String?, formattedDate: String?, stock: productStock?) {
        self.date = date
        self.formattedDate = formattedDate
        self.stock = stock
    }
}

// MARK: - productPotentialPromotion
public struct productPotentialPromotion: Codable {
    public let code: String?
    public let couldFireMessages: [String]?
    public let potentialPromotionDescription: String?
    public let enabled: Bool?
    public let endDate: String?
    public let firedMessages: [String]?
    public let priority: Int?
    public let productBanner: productMapIcon?
    public let promotionGroup, promotionType: String?
    public let restrictions: [productRestriction]?
    public let startDate, title: String?

    enum CodingKeys: String, CodingKey {
        case code, couldFireMessages
        case potentialPromotionDescription = "description"
        case enabled, endDate, firedMessages, priority, productBanner, promotionGroup, promotionType, restrictions, startDate, title
    }

    public init(code: String?, couldFireMessages: [String]?, potentialPromotionDescription: String?, enabled: Bool?, endDate: String?, firedMessages: [String]?, priority: Int?, productBanner: productMapIcon?, promotionGroup: String?, promotionType: String?, restrictions: [productRestriction]?, startDate: String?, title: String?) {
        self.code = code
        self.couldFireMessages = couldFireMessages
        self.potentialPromotionDescription = potentialPromotionDescription
        self.enabled = enabled
        self.endDate = endDate
        self.firedMessages = firedMessages
        self.priority = priority
        self.productBanner = productBanner
        self.promotionGroup = promotionGroup
        self.promotionType = promotionType
        self.restrictions = restrictions
        self.startDate = startDate
        self.title = title
    }
}

// MARK: - productRestriction
public struct productRestriction: Codable {
    public let restrictionDescription, restrictionType: String?

    enum CodingKeys: String, CodingKey {
        case restrictionDescription = "description"
        case restrictionType
    }

    public init(restrictionDescription: String?, restrictionType: String?) {
        self.restrictionDescription = restrictionDescription
        self.restrictionType = restrictionType
    }
}

// MARK: - productPriceRange
public struct productPriceRange: Codable {
    public let maxPrice, minPrice: productBasePrice?

    public init(maxPrice: productBasePrice?, minPrice: productBasePrice?) {
        self.maxPrice = maxPrice
        self.minPrice = minPrice
    }
}

// MARK: - productProductReference
public struct productProductReference: Codable {
    public let productReferenceDescription: String?
    public let preselected: Bool?
    public let quantity: Int?
    public let referenceType: String?

    enum CodingKeys: String, CodingKey {
        case productReferenceDescription = "description"
        case preselected, quantity, referenceType
    }

    public init(productReferenceDescription: String?, preselected: Bool?, quantity: Int?, referenceType: String?) {
        self.productReferenceDescription = productReferenceDescription
        self.preselected = preselected
        self.quantity = quantity
        self.referenceType = referenceType
    }
}

// MARK: - productReview
public struct productReview: Codable {
    public let alias, comment, date, headline: String?
    public let id: String?
    public let principal: productPrincipal?
    public let rating: Int?

    public init(alias: String?, comment: String?, date: String?, headline: String?, id: String?, principal: productPrincipal?, rating: Int?) {
        self.alias = alias
        self.comment = comment
        self.date = date
        self.headline = headline
        self.id = id
        self.principal = principal
        self.rating = rating
    }
}

// MARK: - productPrincipal
public struct productPrincipal: Codable {
    public let currency: productCurrency?
    public let customerID, deactivationDate: String?
    public let defaultAddress: productAddress?
    public let displayUid, firstName: String?
    public let language: productCurrency?
    public let lastName: String?
    public let mobileCountry: productCountry?
    public let mobileNumber, name, title, titleCode: String?
    public let uid: String?

    enum CodingKeys: String, CodingKey {
        case currency
        case customerID = "customerId"
        case deactivationDate, defaultAddress, displayUid, firstName, language, lastName, mobileCountry, mobileNumber, name, title, titleCode, uid
    }

    public init(currency: productCurrency?, customerID: String?, deactivationDate: String?, defaultAddress: productAddress?, displayUid: String?, firstName: String?, language: productCurrency?, lastName: String?, mobileCountry: productCountry?, mobileNumber: String?, name: String?, title: String?, titleCode: String?, uid: String?) {
        self.currency = currency
        self.customerID = customerID
        self.deactivationDate = deactivationDate
        self.defaultAddress = defaultAddress
        self.displayUid = displayUid
        self.firstName = firstName
        self.language = language
        self.lastName = lastName
        self.mobileCountry = mobileCountry
        self.mobileNumber = mobileNumber
        self.name = name
        self.title = title
        self.titleCode = titleCode
        self.uid = uid
    }
}

// MARK: - productCurrency
public struct productCurrency: Codable {
    public let active: Bool?
    public let isocode, name, symbol, nativeName: String?

    public init(active: Bool?, isocode: String?, name: String?, symbol: String?, nativeName: String?) {
        self.active = active
        self.isocode = isocode
        self.name = name
        self.symbol = symbol
        self.nativeName = nativeName
    }
}

// MARK: - productVariantMatrix
public struct productVariantMatrix: Codable {
    public let elements: [JSONNull?]?
    public let isLeaf: Bool?
    public let parentVariantCategory: productParentVariantCategoryElement?
    public let variantOption: productVariantOption?
    public let variantValueCategory: productVariantValueCategory?

    public init(elements: [JSONNull?]?, isLeaf: Bool?, parentVariantCategory: productParentVariantCategoryElement?, variantOption: productVariantOption?, variantValueCategory: productVariantValueCategory?) {
        self.elements = elements
        self.isLeaf = isLeaf
        self.parentVariantCategory = parentVariantCategory
        self.variantOption = variantOption
        self.variantValueCategory = variantValueCategory
    }
}

// MARK: - productParentVariantCategoryElement
public struct productParentVariantCategoryElement: Codable {
    public let hasImage: Bool?
    public let name: String?
    public let priority: Int?

    public init(hasImage: Bool?, name: String?, priority: Int?) {
        self.hasImage = hasImage
        self.name = name
        self.priority = priority
    }
}

// MARK: - productVariantValueCategory
public struct productVariantValueCategory: Codable {
    public let name: String?
    public let sequence: Int?
    public let superCategories: [productParentVariantCategoryElement]?

    public init(name: String?, sequence: Int?, superCategories: [productParentVariantCategoryElement]?) {
        self.name = name
        self.sequence = sequence
        self.superCategories = superCategories
    }
}
