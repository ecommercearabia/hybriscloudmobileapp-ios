//
//  CartMargeModel.swift
//  Domain
//
//  Created by ahmadSaleh on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation

// MARK: - CartMargeModel
public struct CartMargeModel: Codable , Identifiable{
public var identity: String{
    return code ?? ""
}
//    public let type: String?
//    public let appliedOrderPromotions, appliedProductPromotions, appliedVouchers: [JSONAny]?
//    public let calculated: Bool?
    public let code: String?
//    public let deliveryItemsQuantity: Int?
//    public let deliveryOrderGroups, entries: [JSONAny]?
//    public let guid: String?
//    public let net: Bool?
//    public let orderDiscounts: CartOrderDiscounts?
//    public let pickupItemsQuantity: Int?
//    public let pickupOrderGroups: [JSONAny]?
//    public let productDiscounts: CartOrderDiscounts?
//    public let site, store: String?
//    public let storeCreditAmount: CartOrderDiscounts?
//    public let storeCreditAmountSelected: Int?
//    public let subTotal, totalDiscounts: CartOrderDiscounts?
//    public let totalItems: Int?
//    public let totalPrice, totalPriceWithTax, totalTax: CartOrderDiscounts?
//    public let user: CartUser?
//    public let potentialOrderPromotions, potentialProductPromotions: [JSONAny]?
//    public let totalUnitCount: Int?
//
//    public init(type: String?, appliedOrderPromotions: [JSONAny]?, appliedProductPromotions: [JSONAny]?, appliedVouchers: [JSONAny]?, calculated: Bool?, code: String?, deliveryItemsQuantity: Int?, deliveryOrderGroups: [JSONAny]?, entries: [JSONAny]?, guid: String?, net: Bool?, orderDiscounts: CartOrderDiscounts?, pickupItemsQuantity: Int?, pickupOrderGroups: [JSONAny]?, productDiscounts: CartOrderDiscounts?, site: String?, store: String?, storeCreditAmount: CartOrderDiscounts?, storeCreditAmountSelected: Int?, subTotal: CartOrderDiscounts?, totalDiscounts: CartOrderDiscounts?, totalItems: Int?, totalPrice: CartOrderDiscounts?, totalPriceWithTax: CartOrderDiscounts?, totalTax: CartOrderDiscounts?, user: CartUser?, potentialOrderPromotions: [JSONAny]?, potentialProductPromotions: [JSONAny]?, totalUnitCount: Int?) {
//        self.type = type
//        self.appliedOrderPromotions = appliedOrderPromotions
//        self.appliedProductPromotions = appliedProductPromotions
//        self.appliedVouchers = appliedVouchers
//        self.calculated = calculated
//        self.code = code
//        self.deliveryItemsQuantity = deliveryItemsQuantity
//        self.deliveryOrderGroups = deliveryOrderGroups
//        self.entries = entries
//        self.guid = guid
//        self.net = net
//        self.orderDiscounts = orderDiscounts
//        self.pickupItemsQuantity = pickupItemsQuantity
//        self.pickupOrderGroups = pickupOrderGroups
//        self.productDiscounts = productDiscounts
//        self.site = site
//        self.store = store
//        self.storeCreditAmount = storeCreditAmount
//        self.storeCreditAmountSelected = storeCreditAmountSelected
//        self.subTotal = subTotal
//        self.totalDiscounts = totalDiscounts
//        self.totalItems = totalItems
//        self.totalPrice = totalPrice
//        self.totalPriceWithTax = totalPriceWithTax
//        self.totalTax = totalTax
//        self.user = user
//        self.potentialOrderPromotions = potentialOrderPromotions
//        self.potentialProductPromotions = potentialProductPromotions
//        self.totalUnitCount = totalUnitCount
//    }
}
 
