// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let carttCartModel = try? newJSONDecoder().decode(CarttCartModel.self, from: jsonData)

import Foundation

 
// MARK: - CartModel
public struct CartModel: Codable , Identifiable {

public var identity: String{
    return guid ?? ""
}
    public let appliedProductPromotions: [CartPromotion]?
    public let appliedOrderPromotions: [AppliedOrderPromotion]?
    public let appliedVouchers: [CartAppliedVoucher]?
    public let calculated: Bool?
    public let code: String?
    public let deliveryAddress: Address?
    public let deliveryCost: CartDeliveryCost?
    public let deliveryItemsQuantity: Double?	
    public let deliveryMode: CartMode?
    public let deliveryOrderGroups: [CartDeliveryOrderGroup]?
    public let cartModelDescription: String?
    public let entries: [CartEntry]?
    public let expirationTime, guid, name: String?
    public let net: Bool?
    public let orderDiscounts: CartDeliveryCost?
    public let paymentInfo: CartPaymentInfo?
    public let paymentMode: CartMode?
    public let pickupItemsQuantity: Double?
    public let pickupOrderGroups: [CartPickupOrderGroup]?
    public let potentialProductPromotions: [CartPromotion]?
    public let potentialOrderPromotions: [PotentialOrderPromotion]?
    public let productDiscounts: CartDeliveryCost?
    public let saveTime: String?
    public let savedBy: CartSavedBy?
    public let site, store: String?
    public let storeCreditAmount: CartDeliveryCost?
    public let storeCreditAmountSelected: Double?
    public let storeCreditMode: CartStoreCreditMode?
    public let subTotal: CartDeliveryCost?
    public let subTotalBeforeSavingPrice: CartsDeliveryCost?
    public let timeSlotInfoData: CartTimeSlotInfoData?
    public let totalDiscounts: CartDeliveryCost?
    public let totalItems: Double?
    public let totalPrice, totalPriceWithTax, totalTax: CartDeliveryCost?
    public let totalUnitCount: Double?
    public let user: CartSavedBy?
    public var shipmentType: CartShipmentType?
    public var cartTimeSlot: CartTimeSlot?
    public var express: Bool?
    
    enum CodingKeys: String, CodingKey {
        case appliedOrderPromotions, appliedProductPromotions, appliedVouchers, calculated, code, deliveryAddress, deliveryCost, deliveryItemsQuantity, deliveryMode, deliveryOrderGroups
        case cartModelDescription = "description"
        case entries, expirationTime, guid, name, net, orderDiscounts, paymentInfo, paymentMode, pickupItemsQuantity, pickupOrderGroups, potentialOrderPromotions, potentialProductPromotions, productDiscounts, saveTime, savedBy, site, store, storeCreditAmount, storeCreditAmountSelected, storeCreditMode, subTotal, timeSlotInfoData, totalDiscounts, totalItems, totalPrice, totalPriceWithTax, totalTax, totalUnitCount, user , subTotalBeforeSavingPrice , shipmentType
        case cartTimeSlot = "cartTimeSlot"
        case express = "express"
    }
   
    public init(appliedOrderPromotions: [AppliedOrderPromotion]?, appliedProductPromotions: [CartPromotion]?, appliedVouchers: [CartAppliedVoucher]?, calculated: Bool?, code: String?, deliveryAddress: Address?, deliveryCost: CartDeliveryCost?, deliveryItemsQuantity: Double?, deliveryMode: CartMode?, deliveryOrderGroups: [CartDeliveryOrderGroup]?, cartModelDescription: String?, entries: [CartEntry]?, expirationTime: String?, guid: String?, name: String?, net: Bool?, orderDiscounts: CartDeliveryCost?, paymentInfo: CartPaymentInfo?, paymentMode: CartMode?, pickupItemsQuantity: Double?, pickupOrderGroups: [CartPickupOrderGroup]?, potentialOrderPromotions: [PotentialOrderPromotion]?, potentialProductPromotions: [CartPromotion]?, productDiscounts: CartDeliveryCost?, saveTime: String?, savedBy: CartSavedBy?, site: String?, store: String?, storeCreditAmount: CartDeliveryCost?, storeCreditAmountSelected: Double?, storeCreditMode: CartStoreCreditMode?, subTotal: CartDeliveryCost?,subTotalBeforeSavingPrice : CartsDeliveryCost?, timeSlotInfoData: CartTimeSlotInfoData?, totalDiscounts: CartDeliveryCost?, totalItems: Double?, totalPrice: CartDeliveryCost?, totalPriceWithTax: CartDeliveryCost?, totalTax: CartDeliveryCost?, totalUnitCount: Double?, user: CartSavedBy? ,  shipmentType: CartShipmentType? , cartTimeSlot: CartTimeSlot? , express: Bool?) {
        self.cartTimeSlot = cartTimeSlot
        self.appliedOrderPromotions = appliedOrderPromotions
        self.appliedProductPromotions = appliedProductPromotions
        self.appliedVouchers = appliedVouchers
        self.calculated = calculated
        self.code = code
        self.deliveryAddress = deliveryAddress
        self.deliveryCost = deliveryCost
        self.deliveryItemsQuantity = deliveryItemsQuantity
        self.deliveryMode = deliveryMode
        self.deliveryOrderGroups = deliveryOrderGroups
        self.cartModelDescription = cartModelDescription
        self.entries = entries
        self.expirationTime = expirationTime
        self.guid = guid
        self.name = name
        self.net = net
        self.orderDiscounts = orderDiscounts
        self.paymentInfo = paymentInfo
        self.paymentMode = paymentMode
        self.pickupItemsQuantity = pickupItemsQuantity
        self.pickupOrderGroups = pickupOrderGroups
        self.potentialOrderPromotions = potentialOrderPromotions
        self.potentialProductPromotions = potentialProductPromotions
        self.productDiscounts = productDiscounts
        self.saveTime = saveTime
        self.savedBy = savedBy
        self.site = site
        self.store = store
        self.storeCreditAmount = storeCreditAmount
        self.storeCreditAmountSelected = storeCreditAmountSelected
        self.storeCreditMode = storeCreditMode
        self.subTotal = subTotal
        self.timeSlotInfoData = timeSlotInfoData
        self.totalDiscounts = totalDiscounts
        self.totalItems = totalItems
        self.totalPrice = totalPrice
        self.totalPriceWithTax = totalPriceWithTax
        self.totalTax = totalTax
        self.totalUnitCount = totalUnitCount
        self.user = user
        self.subTotalBeforeSavingPrice = subTotalBeforeSavingPrice
        self.shipmentType = shipmentType
        self.express = express
    }
}

// MARK: - CartExpressTimeSlot
public struct CartTimeSlot: Codable {
    public var messages: [String]?
    public var showTimeSlot: Bool?

    enum CodingKeys: String, CodingKey {
        case messages = "messages"
        case showTimeSlot = "showTimeSlot"
    }

    public init(messages: [String]?, showTimeSlot: Bool?) {
        self.messages = messages
        self.showTimeSlot = showTimeSlot
    }
}

// MARK: - CartPromotion
public struct CartPromotion: Codable {
    public let consumedEntries: [CartConsumedEntry]?
    public let promotionDescription: String?
    public let promotion: CartPromotionElement?

    enum CodingKeys: String, CodingKey {
        case consumedEntries
        case promotionDescription = "description"
        case promotion
    }

    public init(consumedEntries: [CartConsumedEntry]?, promotionDescription: String?, promotion: CartPromotionElement?) {
        self.consumedEntries = consumedEntries
        self.promotionDescription = promotionDescription
        self.promotion = promotion
    }
}

// MARK: - CartConsumedEntry
public struct CartConsumedEntry: Codable {
    public let adjustedUnitPrice: Double?
    public let code: String?
    public let orderEntryNumber, quantity: Double?

    public init(adjustedUnitPrice: Double?, code: String?, orderEntryNumber: Double?, quantity: Double?) {
        self.adjustedUnitPrice = adjustedUnitPrice
        self.code = code
        self.orderEntryNumber = orderEntryNumber
        self.quantity = quantity
    }
}

// MARK: - CartPromotionElement
public struct CartPromotionElement: Codable {
    public let code: String?
    public let couldFireMessages: [String]?
    public let promotionDescription: String?
    public let enabled: Bool?
    public let endDate: String?
    public let firedMessages: [String]?
    public let priority: Double?
    public let productBanner: CartProductBanner?
    public let promotionGroup, promotionType: String?
    public let restrictions: [CartRestriction]?
    public let startDate, title: String?

    enum CodingKeys: String, CodingKey {
        case code, couldFireMessages
        case promotionDescription = "description"
        case enabled, endDate, firedMessages, priority, productBanner, promotionGroup, promotionType, restrictions, startDate, title
    }

    public init(code: String?, couldFireMessages: [String]?, promotionDescription: String?, enabled: Bool?, endDate: String?, firedMessages: [String]?, priority: Double?, productBanner: CartProductBanner?, promotionGroup: String?, promotionType: String?, restrictions: [CartRestriction]?, startDate: String?, title: String?) {
        self.code = code
        self.couldFireMessages = couldFireMessages
        self.promotionDescription = promotionDescription
        self.enabled = enabled
        self.endDate = endDate
        self.firedMessages = firedMessages
        self.priority = priority
        self.productBanner = productBanner
        self.promotionGroup = promotionGroup
        self.promotionType = promotionType
        self.restrictions = restrictions
        self.startDate = startDate
        self.title = title
    }
}

// MARK: - CartProductBanner
public struct CartProductBanner: Codable {
    public let altText, format: String?
    public let galleryIndex: Double?
    public let imageType, url: String?

    public init(altText: String?, format: String?, galleryIndex: Double?, imageType: String?, url: String?) {
        self.altText = altText
        self.format = format
        self.galleryIndex = galleryIndex
        self.imageType = imageType
        self.url = url
    }
}

// MARK: - CartRestriction
public struct CartRestriction: Codable {
    public let restrictionDescription, restrictionType: String?

    enum CodingKeys: String, CodingKey {
        case restrictionDescription = "description"
        case restrictionType
    }

    public init(restrictionDescription: String?, restrictionType: String?) {
        self.restrictionDescription = restrictionDescription
        self.restrictionType = restrictionType
    }
}

// MARK: - CartAppliedVoucher
public struct CartAppliedVoucher: Codable {
    public let appliedValue: CartDeliveryCost?
    public let code: String?
    public let currency: CartCurrency?
    public let appliedVoucherDescription: String?
    public let freeShipping: Bool?
    public let name: String?
    public let value: Double?
    public let valueFormatted, valueString, voucherCode: String?

    enum CodingKeys: String, CodingKey {
        case appliedValue, code, currency
        case appliedVoucherDescription = "description"
        case freeShipping, name, value, valueFormatted, valueString, voucherCode
    }

    public init(appliedValue: CartDeliveryCost?, code: String?, currency: CartCurrency?, appliedVoucherDescription: String?, freeShipping: Bool?, name: String?, value: Double?, valueFormatted: String?, valueString: String?, voucherCode: String?) {
        self.appliedValue = appliedValue
        self.code = code
        self.currency = currency
        self.appliedVoucherDescription = appliedVoucherDescription
        self.freeShipping = freeShipping
        self.name = name
        self.value = value
        self.valueFormatted = valueFormatted
        self.valueString = valueString
        self.voucherCode = voucherCode
    }
}

// MARK: - CartDeliveryCost
public struct CartDeliveryCost: Codable {
    public let currencyISO, formattedValue: String?
    public let maxQuantity, minQuantity: Double?
    public let priceType: String?
    public let value: Double?

    enum CodingKeys: String, CodingKey {
        case currencyISO = "currencyIso"
        case formattedValue, maxQuantity, minQuantity, priceType, value
    }

    public init(currencyISO: String?, formattedValue: String?, maxQuantity: Double?, minQuantity: Double?, priceType: String?, value: Double?) {
        self.currencyISO = currencyISO
        self.formattedValue = formattedValue
        self.maxQuantity = maxQuantity
        self.minQuantity = minQuantity
        self.priceType = priceType
        self.value = value
    }
}

// MARK: - CartCurrency
public struct CartCurrency: Codable {
    public let active: Bool?
    public let isocode, name, symbol, nativeName: String?

    public init(active: Bool?, isocode: String?, name: String?, symbol: String?, nativeName: String?) {
        self.active = active
        self.isocode = isocode
        self.name = name
        self.symbol = symbol
        self.nativeName = nativeName
    }
}

 
// MARK: - CartArea
public struct CartArea: Codable {
    public let code, name: String?

    public init(code: String?, name: String?) {
        self.code = code
        self.name = name
    }
}

// MARK: - CartCity
public struct CartCity: Codable {
    public let areas: [CartArea]?
    public let code, name: String?

    public init(areas: [CartArea]?, code: String?, name: String?) {
        self.areas = areas
        self.code = code
        self.name = name
    }
}

// MARK: - CartCountry
public struct CartCountry: Codable {
    public let isdcode, isocode, name: String?

    public init(isdcode: String?, isocode: String?, name: String?) {
        self.isdcode = isdcode
        self.isocode = isocode
        self.name = name
    }
}

// MARK: - CartRegion
public struct CartRegion: Codable {
    public let countryISO, isocode, isocodeShort, name: String?

    enum CodingKeys: String, CodingKey {
        case countryISO = "countryIso"
        case isocode, isocodeShort, name
    }

    public init(countryISO: String?, isocode: String?, isocodeShort: String?, name: String?) {
        self.countryISO = countryISO
        self.isocode = isocode
        self.isocodeShort = isocodeShort
        self.name = name
    }
}

// MARK: - CartMode
public struct CartMode: Codable {
    public let code: String?
    public let deliveryCost: CartDeliveryCost?
    public let modeDescription, name: String?

    enum CodingKeys: String, CodingKey {
        case code, deliveryCost
        case modeDescription = "description"
        case name
    }

    public init(code: String?, deliveryCost: CartDeliveryCost?, modeDescription: String?, name: String?) {
        self.code = code
        self.deliveryCost = deliveryCost
        self.modeDescription = modeDescription
        self.name = name
    }
}

// MARK: - CartDeliveryOrderGroup
public struct CartDeliveryOrderGroup: Codable {
    public let deliveryAddress: Address?
    public let entries: [CartEntry]?
    public let quantity: Double?
    public let totalPriceWithTax: CartDeliveryCost?

    public init(deliveryAddress: Address?, entries: [CartEntry]?, quantity: Double?, totalPriceWithTax: CartDeliveryCost?) {
        self.deliveryAddress = deliveryAddress
        self.entries = entries
        self.quantity = quantity
        self.totalPriceWithTax = totalPriceWithTax
    }
}

// MARK: - CartEntry
public struct CartEntry: Codable {
    public let basePrice: CartDeliveryCost?
    public let cancellableQuantity: Double?
    public let cancelledItemsPrice: CartDeliveryCost?
    public let configurationInfos: [CartConfigurationInfo]?
    public let deliveryMode: CartMode?
    public let deliveryPoDoubleOfService: CartDeliveryPoDoubleOfService?
    public let entryNumber: Int?
    public let product: CartProduct?
    public let quantityAllocated, quantityCancelled, quantityPending: Double?
    public var quantity : Int?
    public let quantityReturned, quantityShipped, quantityUnallocated, returnableQuantity: Double?
    public let returnedItemsPrice, totalPrice: CartDeliveryCost?
    public let updateable: Bool?
    public let url: String?
    public var deliveryPointOfService: DeliveryPointOfService?
   
    public init(basePrice: CartDeliveryCost?, cancellableQuantity: Double?, cancelledItemsPrice: CartDeliveryCost?, configurationInfos: [CartConfigurationInfo]?, deliveryMode: CartMode?, deliveryPoDoubleOfService: CartDeliveryPoDoubleOfService?, entryNumber: Int?, product: CartProduct?, quantity: Int?, quantityAllocated: Double?, quantityCancelled: Double?, quantityPending: Double?, quantityReturned: Double?, quantityShipped: Double?, quantityUnallocated: Double?, returnableQuantity: Double?, returnedItemsPrice: CartDeliveryCost?, totalPrice: CartDeliveryCost?, updateable: Bool?, url: String? , deliveryPointOfService: DeliveryPointOfService? ) {
        self.basePrice = basePrice
        self.cancellableQuantity = cancellableQuantity
        self.cancelledItemsPrice = cancelledItemsPrice
        self.configurationInfos = configurationInfos
        self.deliveryMode = deliveryMode
        self.deliveryPoDoubleOfService = deliveryPoDoubleOfService
        self.entryNumber = entryNumber
        self.product = product
        self.quantity = quantity
        self.quantityAllocated = quantityAllocated
        self.quantityCancelled = quantityCancelled
        self.quantityPending = quantityPending
        self.quantityReturned = quantityReturned
        self.quantityShipped = quantityShipped
        self.quantityUnallocated = quantityUnallocated
        self.returnableQuantity = returnableQuantity
        self.returnedItemsPrice = returnedItemsPrice
        self.totalPrice = totalPrice
        self.updateable = updateable
        self.url = url
        self.deliveryPointOfService = deliveryPointOfService
        
    }
}

// MARK: - CartConfigurationInfo
public struct CartConfigurationInfo: Codable {
    public let configurationLabel, configurationValue, configuratorType, status: String?

    public init(configurationLabel: String?, configurationValue: String?, configuratorType: String?, status: String?) {
        self.configurationLabel = configurationLabel
        self.configurationValue = configurationValue
        self.configuratorType = configuratorType
        self.status = status
    }
}

// MARK: - CartDeliveryPoDoubleOfService
public struct CartDeliveryPoDoubleOfService: Codable {
    public let address: Address?
    public let deliveryPoDoubleOfServiceDescription, displayName: String?
    public let distanceKM: Double?
    public let features: CartFeatures?
    public let formattedDistance: String?
    public let geoPoDouble: CartGeoPoDouble?
    public let mapIcon: CartProductBanner?
    public let name: String?
    public let openingHours: CartOpeningHours?
    public let storeContent: String?
    public let storeImages: [CartProductBanner]?
    public let url: String?
    public let warehouseCodes: [String]?

    enum CodingKeys: String, CodingKey {
        case address
        case deliveryPoDoubleOfServiceDescription = "description"
        case displayName
        case distanceKM = "distanceKm"
        case features, formattedDistance, geoPoDouble, mapIcon, name, openingHours, storeContent, storeImages, url, warehouseCodes
    }

    public init(address: Address?, deliveryPoDoubleOfServiceDescription: String?, displayName: String?, distanceKM: Double?, features: CartFeatures?, formattedDistance: String?, geoPoDouble: CartGeoPoDouble?, mapIcon: CartProductBanner?, name: String?, openingHours: CartOpeningHours?, storeContent: String?, storeImages: [CartProductBanner]?, url: String?, warehouseCodes: [String]?) {
        self.address = address
        self.deliveryPoDoubleOfServiceDescription = deliveryPoDoubleOfServiceDescription
        self.displayName = displayName
        self.distanceKM = distanceKM
        self.features = features
        self.formattedDistance = formattedDistance
        self.geoPoDouble = geoPoDouble
        self.mapIcon = mapIcon
        self.name = name
        self.openingHours = openingHours
        self.storeContent = storeContent
        self.storeImages = storeImages
        self.url = url
        self.warehouseCodes = warehouseCodes
    }
}

// MARK: - CartFeatures
public struct CartFeatures: Codable {
    public let additionalProp1, additionalProp2, additionalProp3: String?

    public init(additionalProp1: String?, additionalProp2: String?, additionalProp3: String?) {
        self.additionalProp1 = additionalProp1
        self.additionalProp2 = additionalProp2
        self.additionalProp3 = additionalProp3
    }
}

// MARK: - CartGeoPoDouble
public struct CartGeoPoDouble: Codable {
    public let latitude, longitude: Double?

    public init(latitude: Double?, longitude: Double?) {
        self.latitude = latitude
        self.longitude = longitude
    }
}

// MARK: - CartOpeningHours
public struct CartOpeningHours: Codable {
    public let code, name: String?
    public let specialDayOpeningList: [CartSpecialDayOpeningList]?
    public let weekDayOpeningList: [CartWeekDayOpeningList]?

    public init(code: String?, name: String?, specialDayOpeningList: [CartSpecialDayOpeningList]?, weekDayOpeningList: [CartWeekDayOpeningList]?) {
        self.code = code
        self.name = name
        self.specialDayOpeningList = specialDayOpeningList
        self.weekDayOpeningList = weekDayOpeningList
    }
}

// MARK: - CartSpecialDayOpeningList
public struct CartSpecialDayOpeningList: Codable {
    public let closed: Bool?
    public let closingTime: CartIngTime?
    public let comment, date, formattedDate, name: String?
    public let openingTime: CartIngTime?

    public init(closed: Bool?, closingTime: CartIngTime?, comment: String?, date: String?, formattedDate: String?, name: String?, openingTime: CartIngTime?) {
        self.closed = closed
        self.closingTime = closingTime
        self.comment = comment
        self.date = date
        self.formattedDate = formattedDate
        self.name = name
        self.openingTime = openingTime
    }
}

// MARK: - CartIngTime
public struct CartIngTime: Codable {
    public let formattedHour: String?
    public let hour, minute: Double?

    public init(formattedHour: String?, hour: Double?, minute: Double?) {
        self.formattedHour = formattedHour
        self.hour = hour
        self.minute = minute
    }
}

// MARK: - CartWeekDayOpeningList
public struct CartWeekDayOpeningList: Codable {
    public let closed: Bool?
    public let closingTime, openingTime: CartIngTime?
    public let weekDay: String?

    public init(closed: Bool?, closingTime: CartIngTime?, openingTime: CartIngTime?, weekDay: String?) {
        self.closed = closed
        self.closingTime = closingTime
        self.openingTime = openingTime
        self.weekDay = weekDay
    }
}

// MARK: - CartProduct
public struct CartProduct: Codable {
    public let availableForPickup: Bool?
    public let averageRating: Double?
    public let baseOptions: [CartBaseOption]?
    public let baseProduct: String?
    public let categories: [CartCategory]?
    public let classifications: [CartClassification]?
    public let code: String?
    public let discount: Discount?
    public let configurable: Bool?
    public let configuratorType, productDescription: String?
    public let futureStocks: [CartFutureStock]?
    public let images: [CartProductBanner]?
    public let manufacturer: String?
    public let multidimensional: Bool?
    public let name: String?
    public let numberOfReviews: Double?
    public let potentialPromotions: [CartPromotionElement]?
    public let price: CartDeliveryCost?
    public let priceRange: CartPriceRange?
    public let productReferences: [CartProductReference]?
    public let purchasable: Bool?
    public let reviews: [CartReview]?
    public let stock: CartStock?
    public let summary: String?
    public let tags: [String]?
    public let url: String?
    public let variantMatrix: [CartVariantMatrix]?
    public let variantOptions: [CartVariantOption]?
    public let variantType: String?
    public let volumePrices: [CartDeliveryCost]?
    public let volumePricesFlag: Bool?
    public var express: Bool?
    enum CodingKeys: String, CodingKey {
        case availableForPickup, averageRating, baseOptions, baseProduct, categories, classifications, code, configurable, configuratorType
        case productDescription = "description"
        case futureStocks, images, manufacturer, multidimensional, name, numberOfReviews, potentialPromotions, price, priceRange, productReferences, purchasable, reviews, stock, summary, tags, url, variantMatrix, variantOptions, variantType, volumePrices, volumePricesFlag , discount
        case express = "express"
    }

    public init(availableForPickup: Bool?, averageRating: Double?, baseOptions: [CartBaseOption]?, baseProduct: String?, categories: [CartCategory]?, classifications: [CartClassification]?, code: String?,discount : Discount? , configurable: Bool?, configuratorType: String?, productDescription: String?, futureStocks: [CartFutureStock]?, images: [CartProductBanner]?, manufacturer: String?, multidimensional: Bool?, name: String?, numberOfReviews: Double?, potentialPromotions: [CartPromotionElement]?, price: CartDeliveryCost?, priceRange: CartPriceRange?, productReferences: [CartProductReference]?, purchasable: Bool?, reviews: [CartReview]?, stock: CartStock?, summary: String?, tags: [String]?, url: String?, variantMatrix: [CartVariantMatrix]?, variantOptions: [CartVariantOption]?, variantType: String?, volumePrices: [CartDeliveryCost]?, volumePricesFlag: Bool? ,  express:Bool?) {
        self.availableForPickup = availableForPickup
        self.averageRating = averageRating
        self.baseOptions = baseOptions
        self.baseProduct = baseProduct
        self.categories = categories
        self.classifications = classifications
        self.code = code
        self.configurable = configurable
        self.configuratorType = configuratorType
        self.productDescription = productDescription
        self.futureStocks = futureStocks
        self.images = images
        self.manufacturer = manufacturer
        self.multidimensional = multidimensional
        self.name = name
        self.numberOfReviews = numberOfReviews
        self.potentialPromotions = potentialPromotions
        self.price = price
        self.priceRange = priceRange
        self.productReferences = productReferences
        self.purchasable = purchasable
        self.reviews = reviews
        self.stock = stock
        self.summary = summary
        self.tags = tags
        self.url = url
        self.variantMatrix = variantMatrix
        self.variantOptions = variantOptions
        self.variantType = variantType
        self.volumePrices = volumePrices
        self.volumePricesFlag = volumePricesFlag
        self.discount = discount
        self.express = express
    }
}

// MARK: - CartBaseOption
public struct CartBaseOption: Codable {
    public let options: [CartVariantOption]?
    public let selected: CartVariantOption?
    public let variantType: String?

    public init(options: [CartVariantOption]?, selected: CartVariantOption?, variantType: String?) {
        self.options = options
        self.selected = selected
        self.variantType = variantType
    }
}

// MARK: - CartVariantOption
public struct CartVariantOption: Codable {
    public let code: String?
    public let priceData: CartDeliveryCost?
    public let stock: CartStock?
    public let url: String?
    public let variantOptionQualifiers: [CartVariantOptionQualifier]?

    public init(code: String?, priceData: CartDeliveryCost?, stock: CartStock?, url: String?, variantOptionQualifiers: [CartVariantOptionQualifier]?) {
        self.code = code
        self.priceData = priceData
        self.stock = stock
        self.url = url
        self.variantOptionQualifiers = variantOptionQualifiers
    }
}

// MARK: - CartStock
public struct CartStock: Codable {
    public let stockLevel: Double?
    public let stockLevelStatus: String?

    public init(stockLevel: Double?, stockLevelStatus: String?) {
        self.stockLevel = stockLevel
        self.stockLevelStatus = stockLevelStatus
    }
}

// MARK: - CartVariantOptionQualifier
public struct CartVariantOptionQualifier: Codable {
    public let image: CartProductBanner?
    public let name, qualifier, value: String?

    public init(image: CartProductBanner?, name: String?, qualifier: String?, value: String?) {
        self.image = image
        self.name = name
        self.qualifier = qualifier
        self.value = value
    }
}

// MARK: - CartCategory
public struct CartCategory: Codable {
    public let code: String?
    public let image: CartProductBanner?
    public let name: String?
    public let plpPicture, plpPictureResponsive: CartProductBanner?
    public let url: String?

    public init(code: String?, image: CartProductBanner?, name: String?, plpPicture: CartProductBanner?, plpPictureResponsive: CartProductBanner?, url: String?) {
        self.code = code
        self.image = image
        self.name = name
        self.plpPicture = plpPicture
        self.plpPictureResponsive = plpPictureResponsive
        self.url = url
    }
}

// MARK: - CartClassification
public struct CartClassification: Codable {
    public let code: String?
    public let features: [CartFeature]?
    public let name: String?

    public init(code: String?, features: [CartFeature]?, name: String?) {
        self.code = code
        self.features = features
        self.name = name
    }
}

// MARK: - CartFeature
public struct CartFeature: Codable {
    public let code: String?
    public let comparable: Bool?
    public let featureDescription: String?
    public let featureUnit: CartFeatureUnit?
    public let featureValues: [CartFeatureValue]?
    public let name: String?
    public let range: Bool?
    public let type: String?

    enum CodingKeys: String, CodingKey {
        case code, comparable
        case featureDescription = "description"
        case featureUnit, featureValues, name, range, type
    }

    public init(code: String?, comparable: Bool?, featureDescription: String?, featureUnit: CartFeatureUnit?, featureValues: [CartFeatureValue]?, name: String?, range: Bool?, type: String?) {
        self.code = code
        self.comparable = comparable
        self.featureDescription = featureDescription
        self.featureUnit = featureUnit
        self.featureValues = featureValues
        self.name = name
        self.range = range
        self.type = type
    }
}

// MARK: - CartFeatureUnit
public struct CartFeatureUnit: Codable {
    public let name, symbol, unitType: String?

    public init(name: String?, symbol: String?, unitType: String?) {
        self.name = name
        self.symbol = symbol
        self.unitType = unitType
    }
}

// MARK: - CartFeatureValue
public struct CartFeatureValue: Codable {
    public let value: String?

    public init(value: String?) {
        self.value = value
    }
}

// MARK: - CartFutureStock
public struct CartFutureStock: Codable {
    public let date, formattedDate: String?
    public let stock: CartStock?

    public init(date: String?, formattedDate: String?, stock: CartStock?) {
        self.date = date
        self.formattedDate = formattedDate
        self.stock = stock
    }
}

// MARK: - CartPriceRange
public struct CartPriceRange: Codable {
    public let maxPrice, minPrice: CartDeliveryCost?

    public init(maxPrice: CartDeliveryCost?, minPrice: CartDeliveryCost?) {
        self.maxPrice = maxPrice
        self.minPrice = minPrice
    }
}

// MARK: - CartProductReference
public struct CartProductReference: Codable {
    public let productReferenceDescription: String?
    public let preselected: Bool?
    public let quantity: Double?
    public let referenceType: String?

    enum CodingKeys: String, CodingKey {
        case productReferenceDescription = "description"
        case preselected, quantity, referenceType
    }

    public init(productReferenceDescription: String?, preselected: Bool?, quantity: Double?, referenceType: String?) {
        self.productReferenceDescription = productReferenceDescription
        self.preselected = preselected
        self.quantity = quantity
        self.referenceType = referenceType
    }
}

// MARK: - CartReview
public struct CartReview: Codable {
    public let alias, comment, date, headline: String?
    public let id: String?
    public let principal: CartPrincipal?
    public let rating: Double?

    public init(alias: String?, comment: String?, date: String?, headline: String?, id: String?, principal: CartPrincipal?, rating: Double?) {
        self.alias = alias
        self.comment = comment
        self.date = date
        self.headline = headline
        self.id = id
        self.principal = principal
        self.rating = rating
    }
}

// MARK: - CartPrincipal
public struct CartPrincipal: Codable {
    public let currency: CartCurrency?
    public let customerID, deactivationDate: String?
    public let defaultAddress: Address?
    public let displayUid, firstName: String?
    public let language: CartCurrency?
    public let lastName: String?
    public let mobileCountry: CartCountry?
    public let mobileNumber, name, title, titleCode: String?
    public let uid: String?

    enum CodingKeys: String, CodingKey {
        case currency
        case customerID = "customerId"
        case deactivationDate, defaultAddress, displayUid, firstName, language, lastName, mobileCountry, mobileNumber, name, title, titleCode, uid
    }

    public init(currency: CartCurrency?, customerID: String?, deactivationDate: String?, defaultAddress: Address?, displayUid: String?, firstName: String?, language: CartCurrency?, lastName: String?, mobileCountry: CartCountry?, mobileNumber: String?, name: String?, title: String?, titleCode: String?, uid: String?) {
        self.currency = currency
        self.customerID = customerID
        self.deactivationDate = deactivationDate
        self.defaultAddress = defaultAddress
        self.displayUid = displayUid
        self.firstName = firstName
        self.language = language
        self.lastName = lastName
        self.mobileCountry = mobileCountry
        self.mobileNumber = mobileNumber
        self.name = name
        self.title = title
        self.titleCode = titleCode
        self.uid = uid
    }
}

// MARK: - CartVariantMatrix
public struct CartVariantMatrix: Codable {
    public let elements: [JSONNull?]?
    public let isLeaf: Bool?
    public let parentVariantCategory: CartParentVariantCategoryElement?
    public let variantOption: CartVariantOption?
    public let variantValueCategory: CartVariantValueCategory?

    public init(elements: [JSONNull?]?, isLeaf: Bool?, parentVariantCategory: CartParentVariantCategoryElement?, variantOption: CartVariantOption?, variantValueCategory: CartVariantValueCategory?) {
        self.elements = elements
        self.isLeaf = isLeaf
        self.parentVariantCategory = parentVariantCategory
        self.variantOption = variantOption
        self.variantValueCategory = variantValueCategory
    }
}

// MARK: - CartParentVariantCategoryElement
public struct CartParentVariantCategoryElement: Codable {
    public let hasImage: Bool?
    public let name: String?
    public let priority: Double?

    public init(hasImage: Bool?, name: String?, priority: Double?) {
        self.hasImage = hasImage
        self.name = name
        self.priority = priority
    }
}

// MARK: - CartVariantValueCategory
public struct CartVariantValueCategory: Codable {
    public let name: String?
    public let sequence: Double?
    public let superCategories: [CartParentVariantCategoryElement]?

    public init(name: String?, sequence: Double?, superCategories: [CartParentVariantCategoryElement]?) {
        self.name = name
        self.sequence = sequence
        self.superCategories = superCategories
    }
}

// MARK: - CartPaymentInfo
public struct CartPaymentInfo: Codable {
    public let accountHolderName: String?
    public let billingAddress: Address?
    public let cardNumber: String?
    public let cardType: CartArea?
    public let defaultPayment: Bool?
    public let expiryMonth, expiryYear, id, issueNumber: String?
    public let saved: Bool?
    public let startMonth, startYear, subscriptionID: String?

    enum CodingKeys: String, CodingKey {
        case accountHolderName, billingAddress, cardNumber, cardType, defaultPayment, expiryMonth, expiryYear, id, issueNumber, saved, startMonth, startYear
        case subscriptionID = "subscriptionId"
    }

    public init(accountHolderName: String?, billingAddress: Address?, cardNumber: String?, cardType: CartArea?, defaultPayment: Bool?, expiryMonth: String?, expiryYear: String?, id: String?, issueNumber: String?, saved: Bool?, startMonth: String?, startYear: String?, subscriptionID: String?) {
        self.accountHolderName = accountHolderName
        self.billingAddress = billingAddress
        self.cardNumber = cardNumber
        self.cardType = cardType
        self.defaultPayment = defaultPayment
        self.expiryMonth = expiryMonth
        self.expiryYear = expiryYear
        self.id = id
        self.issueNumber = issueNumber
        self.saved = saved
        self.startMonth = startMonth
        self.startYear = startYear
        self.subscriptionID = subscriptionID
    }
}

// MARK: - CartPickupOrderGroup
public struct CartPickupOrderGroup: Codable {
    public let deliveryPoDoubleOfService: CartDeliveryPoDoubleOfService?
    public let distance: Double?
    public let entries: [CartEntry]?
    public let quantity: Double?
    public let totalPriceWithTax: CartDeliveryCost?

    public init(deliveryPoDoubleOfService: CartDeliveryPoDoubleOfService?, distance: Double?, entries: [CartEntry]?, quantity: Double?, totalPriceWithTax: CartDeliveryCost?) {
        self.deliveryPoDoubleOfService = deliveryPoDoubleOfService
        self.distance = distance
        self.entries = entries
        self.quantity = quantity
        self.totalPriceWithTax = totalPriceWithTax
    }
}

// MARK: - CartSavedBy
public struct CartSavedBy: Codable {
    public let name, uid: String?

    public init(name: String?, uid: String?) {
        self.name = name
        self.uid = uid
    }
}

// MARK: - CartStoreCreditMode
public struct CartStoreCreditMode: Codable {
    public let storeCreditModeDescription, name: String?
    public let storeCreditModeType: CartArea?

    enum CodingKeys: String, CodingKey {
        case storeCreditModeDescription = "description"
        case name, storeCreditModeType
    }

    public init(storeCreditModeDescription: String?, name: String?, storeCreditModeType: CartArea?) {
        self.storeCreditModeDescription = storeCreditModeDescription
        self.name = name
        self.storeCreditModeType = storeCreditModeType
    }
}

// MARK: - CartTimeSlotInfoData
public struct CartTimeSlotInfoData: Codable {
    public let date, day: String?
    public let end: String?
    public let periodCode: String?
    public let start: String?

    public init(date: String?, day: String?, end: String?, periodCode: String?, start: String?) {
        self.date = date
        self.day = day
        self.end = end
        self.periodCode = periodCode
        self.start = start
    }
}

// MARK: - CartEnd
public struct CartEnd: Codable {
    public let hour, minute, nano, second: Double?

    public init(hour: Double?, minute: Double?, nano: Double?, second: Double?) {
        self.hour = hour
        self.minute = minute
        self.nano = nano
        self.second = second
    }
}

 
