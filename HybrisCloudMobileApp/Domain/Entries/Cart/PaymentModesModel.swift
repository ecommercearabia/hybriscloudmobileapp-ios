//
//  PaymentModesModel.swift
//  Domain
//
//  Created by ahmadSaleh on 8/13/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

 import Foundation

// MARK: - DeliveryModesModel
public struct PaymentModesModel: Codable , Identifiable{
public var identity: String{
    return paymentModes?.first?.code ?? ""
}
    public var paymentModes: [PaymentMode]?

    public init(paymentModes: [PaymentMode]?) {
        self.paymentModes = paymentModes
    }
}

// MARK: - PaymentMode
public struct PaymentMode: Codable {
    public var code, paymentModeDescription, name: String?
    public var selected : Bool? = false
    enum CodingKeys: String, CodingKey {
        case code
        case paymentModeDescription = "description"
        case name
    }

    public init(code: String?, paymentModeDescription: String?, name: String?) {
        self.code = code
        self.paymentModeDescription = paymentModeDescription
        self.name = name
    }
}
