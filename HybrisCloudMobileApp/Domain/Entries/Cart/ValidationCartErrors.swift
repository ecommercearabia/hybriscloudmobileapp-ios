//
//  ValidationCartErrors.swift
//  Domain
//
//  Created by ahmadSaleh on 8/22/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation


// MARK: - ValidationCartErrors
public struct ValidationCartErrors: Codable , Identifiable {

public var identity: String{
    return errors?.first ?? ""
}
    public let errors: [String]?
    public let valid: Bool?

    public init(errors: [String]?, valid: Bool?) {
        self.errors = errors
        self.valid = valid
    }
}
