//
//  CartBankOffers.swift
//  Domain
//
//  Created by ahmadSaleh on 11/2/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

 
import Foundation

// MARK: - BankOffers
public struct BankOffers: Codable , Identifiable{
public var identity: String{
    return ""
}
    public let bankOffers: [BankOffer]?

    public init(bankOffers: [BankOffer]?) {
        self.bankOffers = bankOffers
    }
}

// MARK: - BankOffer
public struct BankOffer: Codable {
    public let code: String?
    public let icon: Icon?
    public let message: String?

    public init(code: String?, icon: Icon?, message: String?) {
        self.code = code
        self.icon = icon
        self.message = message
    }
}

// MARK: - Icon
public struct Icon: Codable {
    public let format, url: String?

    public init(format: String?, url: String?) {
        self.format = format
        self.url = url
    }
}
