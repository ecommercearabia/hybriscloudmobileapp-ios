// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let cartsModel = try? newJSONDecoder().decode(CartsModel.self, from: jsonData)

import Foundation


// MARK: - CartsModel
public struct CartsModel: Codable , Identifiable {

public var identity: String{
    return carts?.first?.code ?? ""
}
    
        public let carts: [CartsCart]?

        public init(carts: [CartsCart]?) {
            self.carts = carts
        }
    }

    // MARK: - CartsCart
    public struct CartsCart: Codable {
        public let appliedProductPromotions, appliedVouchers: [JSONAny]?
        public let appliedOrderPromotions: [AppliedOrderPromotion]?
        public let calculated: Bool?
        public let code: String?
        public let deliveryAddress: CartsDeliveryAddress?
        public let deliveryCost: CartsDeliveryCost?
        public let deliveryItemsQuantity: Int?
        public let deliveryMode: CartsMode?
        public let deliveryOrderGroups: [CartsDeliveryOrderGroup]?
        public let entries: [CartsEntry]?
        public let guid: String?
        public let net: Bool?
        public let orderDiscounts: CartsDeliveryCost?
        public let paymentMode: CartsMode?
        public let pickupItemsQuantity: Int?
        public let pickupOrderGroups: [JSONAny]?
        public let productDiscounts: CartsDeliveryCost?
        public let site, store: String?
        public let storeCreditAmount: CartsDeliveryCost?
        public let storeCreditAmountSelected: Int?
        public let subTotal: CartsDeliveryCost?
        public let timeSlotInfoData: CartsTimeSlotInfoData?
        public let totalDiscounts: CartsDeliveryCost?
        public let totalItems: Int?
        public let totalPrice, totalPriceWithTax, totalTax: CartsDeliveryCost?
        public let user: CartsUser?
        public let potentialProductPromotions: [JSONAny]?
        public let potentialOrderPromotions: [PotentialOrderPromotion]?
        public let totalUnitCount: Int?
        public var shipmentType: CartShipmentType?
        public init(appliedOrderPromotions: [AppliedOrderPromotion]?, appliedProductPromotions: [JSONAny]?, appliedVouchers: [JSONAny]?, calculated: Bool?, code: String?, deliveryAddress: CartsDeliveryAddress?, deliveryCost: CartsDeliveryCost?, deliveryItemsQuantity: Int?, deliveryMode: CartsMode?, deliveryOrderGroups: [CartsDeliveryOrderGroup]?, entries: [CartsEntry]?, guid: String?, net: Bool?, orderDiscounts: CartsDeliveryCost?, paymentMode: CartsMode?, pickupItemsQuantity: Int?, pickupOrderGroups: [JSONAny]?, productDiscounts: CartsDeliveryCost?, site: String?, store: String?, storeCreditAmount: CartsDeliveryCost?, storeCreditAmountSelected: Int?, subTotal: CartsDeliveryCost?, timeSlotInfoData: CartsTimeSlotInfoData?, totalDiscounts: CartsDeliveryCost?, totalItems: Int?, totalPrice: CartsDeliveryCost?, totalPriceWithTax: CartsDeliveryCost?, totalTax: CartsDeliveryCost? ,  user: CartsUser?, potentialOrderPromotions: [PotentialOrderPromotion]?, potentialProductPromotions: [JSONAny]?, totalUnitCount: Int? ,  shipmentType: CartShipmentType?) {
            self.appliedOrderPromotions = appliedOrderPromotions
            self.appliedProductPromotions = appliedProductPromotions
            self.appliedVouchers = appliedVouchers
            self.calculated = calculated
            self.code = code
            self.deliveryAddress = deliveryAddress
            self.deliveryCost = deliveryCost
            self.deliveryItemsQuantity = deliveryItemsQuantity
            self.deliveryMode = deliveryMode
            self.deliveryOrderGroups = deliveryOrderGroups
            self.entries = entries
            self.guid = guid
            self.net = net
            self.orderDiscounts = orderDiscounts
            self.paymentMode = paymentMode
            self.pickupItemsQuantity = pickupItemsQuantity
            self.pickupOrderGroups = pickupOrderGroups
            self.productDiscounts = productDiscounts
            self.site = site
            self.store = store
            self.storeCreditAmount = storeCreditAmount
            self.storeCreditAmountSelected = storeCreditAmountSelected
            self.subTotal = subTotal
            self.timeSlotInfoData = timeSlotInfoData
            self.totalDiscounts = totalDiscounts
            self.totalItems = totalItems
            self.totalPrice = totalPrice
            self.totalPriceWithTax = totalPriceWithTax
            self.totalTax = totalTax
            self.user = user
            self.potentialOrderPromotions = potentialOrderPromotions
            self.potentialProductPromotions = potentialProductPromotions
            self.totalUnitCount = totalUnitCount
            self.shipmentType = shipmentType
        }
    }

// MARK: - CartShipmentType
public struct CartShipmentType: Codable {
    public var code: String?
    public var name: String?

    enum CodingKeys: String, CodingKey {
        case code
        case name
    }

    public init(code: String?, name: String?) {
        self.code = code
        self.name = name
    }
}

// MARK: - PotentialOrderPromotion
public struct PotentialOrderPromotion: Codable {
    public let consumedEntries: [JSONAny]?
    public let potentialOrderPromotionDescription: String?
    public let promotion: Promotion?

    enum CodingKeys: String, CodingKey {
        case consumedEntries
        case potentialOrderPromotionDescription = "description"
        case promotion
    }

    public init(consumedEntries: [JSONAny]?, potentialOrderPromotionDescription: String?, promotion: Promotion?) {
        self.consumedEntries = consumedEntries
        self.potentialOrderPromotionDescription = potentialOrderPromotionDescription
        self.promotion = promotion
    }
}

// MARK: - Promotion
 

// MARK: - AppliedOrderPromotion
public struct AppliedOrderPromotion: Codable {
    public let consumedEntries: [JSONAny]?
    public let appliedOrderPromotionDescription: String?
    public let promotion: Promotion?

    enum CodingKeys: String, CodingKey {
        case consumedEntries
        case appliedOrderPromotionDescription = "description"
        case promotion
    }

    public init(consumedEntries: [JSONAny]?, appliedOrderPromotionDescription: String?, promotion: Promotion?) {
        self.consumedEntries = consumedEntries
        self.appliedOrderPromotionDescription = appliedOrderPromotionDescription
        self.promotion = promotion
    }
}

// MARK: - Promotion
public struct Promotion: Codable {
    public let code: String?
    public let couldFireMessages: [JSONAny]?
    public let promotionDescription: String?
    public let endDate: String?
    public let firedMessages: [JSONAny]?
    public let promotionType: String?

    enum CodingKeys: String, CodingKey {
        case code, couldFireMessages
        case promotionDescription = "description"
        case endDate, firedMessages, promotionType
    }

    public init(code: String?, couldFireMessages: [JSONAny]?, promotionDescription: String?, endDate: String?, firedMessages: [JSONAny]?, promotionType: String?) {
        self.code = code
        self.couldFireMessages = couldFireMessages
        self.promotionDescription = promotionDescription
        self.endDate = endDate
        self.firedMessages = firedMessages
        self.promotionType = promotionType
    }
}


    // MARK: - CartsDeliveryAddress
    public struct CartsDeliveryAddress: Codable {
        public let addressName: String?
        public let area: CartsArea?
        public let city: CartsCity?
        public let country: CartsCountry?
        public let defaultAddress: Bool?
        public let firstName, formattedAddress, id, lastName: String?
        public let line1, line2: String?
        public let mobileCountry: CartsMobileCountry?
        public let mobileNumber, postalCode: String?
        public let shippingAddress, visibleInAddressBook: Bool?

        public init(addressName: String?, area: CartsArea?, city: CartsCity?, country: CartsCountry?, defaultAddress: Bool?, firstName: String?, formattedAddress: String?, id: String?, lastName: String?, line1: String?, line2: String?, mobileCountry: CartsMobileCountry?, mobileNumber: String?, postalCode: String?, shippingAddress: Bool?, visibleInAddressBook: Bool?) {
            self.addressName = addressName
            self.area = area
            self.city = city
            self.country = country
            self.defaultAddress = defaultAddress
            self.firstName = firstName
            self.formattedAddress = formattedAddress
            self.id = id
            self.lastName = lastName
            self.line1 = line1
            self.line2 = line2
            self.mobileCountry = mobileCountry
            self.mobileNumber = mobileNumber
            self.postalCode = postalCode
            self.shippingAddress = shippingAddress
            self.visibleInAddressBook = visibleInAddressBook
        }
    }

    // MARK: - CartsArea
    public struct CartsArea: Codable {
        public let code, name: String?

        public init(code: String?, name: String?) {
            self.code = code
            self.name = name
        }
    }

    // MARK: - CartsCity
    public struct CartsCity: Codable {
        public let areas: [CartsArea]?
        public let code, name: String?

        public init(areas: [CartsArea]?, code: String?, name: String?) {
            self.areas = areas
            self.code = code
            self.name = name
        }
    }

    // MARK: - CartsCountry
    public struct CartsCountry: Codable {
        public let isocode, name: String?

        public init(isocode: String?, name: String?) {
            self.isocode = isocode
            self.name = name
        }
    }

    // MARK: - CartsMobileCountry
    public struct CartsMobileCountry: Codable {
        public let isdcode, isocode, name: String?

        public init(isdcode: String?, isocode: String?, name: String?) {
            self.isdcode = isdcode
            self.isocode = isocode
            self.name = name
        }
    }

    // MARK: - CartsDeliveryCost
    public struct CartsDeliveryCost: Codable {
        public let currencyISO, formattedValue, priceType: String?
        public let value: Double?

        enum CodingKeys: String, CodingKey {
            case currencyISO = "currencyIso"
            case formattedValue, priceType, value
        }

        public init(currencyISO: String?, formattedValue: String?, priceType: String?, value: Double?) {
            self.currencyISO = currencyISO
            self.formattedValue = formattedValue
            self.priceType = priceType
            self.value = value
        }
    }

    // MARK: - CartsMode
    public struct CartsMode: Codable {
        public let code: String?
        public let deliveryCost: CartsDeliveryCost?
        public let modeDescription, name: String?

        enum CodingKeys: String, CodingKey {
            case code, deliveryCost
            case modeDescription = "description"
            case name
        }

        public init(code: String?, deliveryCost: CartsDeliveryCost?, modeDescription: String?, name: String?) {
            self.code = code
            self.deliveryCost = deliveryCost
            self.modeDescription = modeDescription
            self.name = name
        }
    }

    // MARK: - CartsDeliveryOrderGroup
    public struct CartsDeliveryOrderGroup: Codable {
        public let entries: [CartsEntry]?
        public let totalPriceWithTax: CartsDeliveryCost?

        public init(entries: [CartsEntry]?, totalPriceWithTax: CartsDeliveryCost?) {
            self.entries = entries
            self.totalPriceWithTax = totalPriceWithTax
        }
    }

    // MARK: - CartsEntry
    public struct CartsEntry: Codable {
        public let basePrice: CartsDeliveryCost?
        public let configurationInfos: [JSONAny]?
        public let entryNumber: Int?
        public let product: CartsProduct?
        public let quantity: Int?
        public let totalPrice: CartsDeliveryCost?
        public let updateable: Bool?
        public var deliveryPointOfService: DeliveryPointOfService?

        public init(basePrice: CartsDeliveryCost?, configurationInfos: [JSONAny]?, entryNumber: Int?, product: CartsProduct?, quantity: Int?, totalPrice: CartsDeliveryCost?, updateable: Bool? , deliveryPointOfService: DeliveryPointOfService?) {
            self.basePrice = basePrice
            self.configurationInfos = configurationInfos
            self.entryNumber = entryNumber
            self.product = product
            self.quantity = quantity
            self.totalPrice = totalPrice
            self.updateable = updateable
            self.deliveryPointOfService = deliveryPointOfService
        }
    }

// MARK: - DeliveryPointOfService
public struct DeliveryPointOfService: Codable {
    public var address: Address?
    public var deliveryPointOfServiceDescription: String?
    public var displayName: String?
    public var features: Features?
    public var geoPoint: GeoPoint?
    public var name: String?
    public var openingHours: OpeningHours?
    public var storeContent: String?
    public var storeImages: [JSONAny]?

    enum CodingKeys: String, CodingKey {
        case address
        case deliveryPointOfServiceDescription
        case displayName
        case features
        case geoPoint
        case name
        case openingHours
        case storeContent
        case storeImages
    }

    public init(address: Address?, deliveryPointOfServiceDescription: String?, displayName: String?, features: Features?, geoPoint: GeoPoint?, name: String?, openingHours: OpeningHours?, storeContent: String?, storeImages: [JSONAny]?) {
        self.address = address
        self.deliveryPointOfServiceDescription = deliveryPointOfServiceDescription
        self.displayName = displayName
        self.features = features
        self.geoPoint = geoPoint
        self.name = name
        self.openingHours = openingHours
        self.storeContent = storeContent
        self.storeImages = storeImages
    }
}

// MARK: - Features
public struct Features: Codable {
    public var entry: [FeaturesEntry]?

    enum CodingKeys: String, CodingKey {
        case entry
    }

    public init(entry: [FeaturesEntry]?) {
        self.entry = entry
    }
}

// MARK: - FeaturesEntry
public struct FeaturesEntry: Codable {
    public var key: String?

    enum CodingKeys: String, CodingKey {
        case key
    }

    public init(key: String?) {
        self.key = key
    }
}

// MARK: - GeoPoint
public struct GeoPoint: Codable {
    public var latitude: Double?
    public var longitude: Double?

    enum CodingKeys: String, CodingKey {
        case latitude
        case longitude
    }

    public init(latitude: Double?, longitude: Double?) {
        self.latitude = latitude
        self.longitude = longitude
    }
}

// MARK: - OpeningHours
public struct OpeningHours: Codable {
    public var code: String?
    public var specialDayOpeningList: [JSONAny]?
    public var weekDayOpeningList: [WeekDayOpeningList]?

    enum CodingKeys: String, CodingKey {
        case code
        case specialDayOpeningList
        case weekDayOpeningList
    }

    public init(code: String?, specialDayOpeningList: [JSONAny]?, weekDayOpeningList: [WeekDayOpeningList]?) {
        self.code = code
        self.specialDayOpeningList = specialDayOpeningList
        self.weekDayOpeningList = weekDayOpeningList
    }
}


// MARK: - WeekDayOpeningList
public struct WeekDayOpeningList: Codable {
    public var closingTime: IngTime?
    public var openingTime: IngTime?
    public var closed: Bool?
    public var weekDay: String?

    enum CodingKeys: String, CodingKey {
        case closingTime
        case openingTime
        case closed
        case weekDay
    }

    public init(closingTime: IngTime?, openingTime: IngTime?, closed: Bool?, weekDay: String?) {
        self.closingTime = closingTime
        self.openingTime = openingTime
        self.closed = closed
        self.weekDay = weekDay
    }
}

// MARK: - IngTime
public struct IngTime: Codable {
    public var formattedHour: String?
    public var hour: Int?
    public var minute: Int?

    enum CodingKeys: String, CodingKey {
        case formattedHour
        case hour
        case minute
    }

    public init(formattedHour: String?, hour: Int?, minute: Int?) {
        self.formattedHour = formattedHour
        self.hour = hour
        self.minute = minute
    }
}

    // MARK: - CartsProduct
    public struct CartsProduct: Codable {
        public let availableForPickup: Bool?
        public let baseOptions: [CartsBaseOption]?
        public let baseProduct: String?
        public let categories: [CartsCategory]?
        public let code: String?
        public let configurable: Bool?
        public let countryOfOrigin, countryOfOriginIsocode: String?
        public let images: [CartsImage]?
        public let name: String?
        public let purchasable: Bool?
        public let stock: CartsStock?
        public let unitOfMeasure, url: String?

        public init(availableForPickup: Bool?, baseOptions: [CartsBaseOption]?, baseProduct: String?, categories: [CartsCategory]?, code: String?, configurable: Bool?, countryOfOrigin: String?, countryOfOriginIsocode: String?, images: [CartsImage]?, name: String?, purchasable: Bool?, stock: CartsStock?, unitOfMeasure: String?, url: String?) {
            self.availableForPickup = availableForPickup
            self.baseOptions = baseOptions
            self.baseProduct = baseProduct
            self.categories = categories
            self.code = code
            self.configurable = configurable
            self.countryOfOrigin = countryOfOrigin
            self.countryOfOriginIsocode = countryOfOriginIsocode
            self.images = images
            self.name = name
            self.purchasable = purchasable
            self.stock = stock
            self.unitOfMeasure = unitOfMeasure
            self.url = url
        }
    }

    // MARK: - CartsBaseOption
    public struct CartsBaseOption: Codable {
        public let selected: CartsSelected?
        public let variantType: String?

        public init(selected: CartsSelected?, variantType: String?) {
            self.selected = selected
            self.variantType = variantType
        }
    }

    // MARK: - CartsSelected
    public struct CartsSelected: Codable {
        public let code: String?
        public let priceData: CartsDeliveryCost?
        public let stock: CartsStock?
        public let url: String?
        public let variantOptionQualifiers: [CartsVariantOptionQualifier]?

        public init(code: String?, priceData: CartsDeliveryCost?, stock: CartsStock?, url: String?, variantOptionQualifiers: [CartsVariantOptionQualifier]?) {
            self.code = code
            self.priceData = priceData
            self.stock = stock
            self.url = url
            self.variantOptionQualifiers = variantOptionQualifiers
        }
    }

    // MARK: - CartsStock
    public struct CartsStock: Codable {
        public let stockLevel: Int?
        public let stockLevelStatus: String?

        public init(stockLevel: Int?, stockLevelStatus: String?) {
            self.stockLevel = stockLevel
            self.stockLevelStatus = stockLevelStatus
        }
    }

    // MARK: - CartsVariantOptionQualifier
    public struct CartsVariantOptionQualifier: Codable {
        public let name, qualifier, value: String?

        public init(name: String?, qualifier: String?, value: String?) {
            self.name = name
            self.qualifier = qualifier
            self.value = value
        }
    }

    // MARK: - CartsCategory
    public struct CartsCategory: Codable {
        public let code, name, url: String?

        public init(code: String?, name: String?, url: String?) {
            self.code = code
            self.name = name
            self.url = url
        }
    }

    // MARK: - CartsImage
    public struct CartsImage: Codable {
        public let altText, format, imageType, url: String?

        public init(altText: String?, format: String?, imageType: String?, url: String?) {
            self.altText = altText
            self.format = format
            self.imageType = imageType
            self.url = url
        }
    }

    // MARK: - CartsTimeSlotInfoData
    public struct CartsTimeSlotInfoData: Codable {
        public let date, day, end, periodCode: String?
        public let start: String?

        public init(date: String?, day: String?, end: String?, periodCode: String?, start: String?) {
            self.date = date
            self.day = day
            self.end = end
            self.periodCode = periodCode
            self.start = start
        }
    }

    // MARK: - CartsUser
    public struct CartsUser: Codable {
        public let name, uid: String?

        public init(name: String?, uid: String?) {
            self.name = name
            self.uid = uid
        }
    }
