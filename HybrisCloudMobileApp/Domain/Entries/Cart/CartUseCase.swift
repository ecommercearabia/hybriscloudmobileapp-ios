//
//  CartUseCase.swift
//  Domain
//
//  Created by ahmadSaleh on 8/4/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift


public protocol CartUseCase {
    func getCart(userId : String , cartId : String) -> Observable<CartModel>
    func deleteCartEntry(entryNumber : String , userId : String,cartId : String) -> Observable<Void>
    func creatCart(userId : String) -> Observable<CartModel>
    func mergeCart(oldCartGUID:String,currentCartGUID:String) -> Observable<CartModel>
    func appliesVoucher(cartId : String , userId : String , voucherId : String) -> Observable<Void>
    func deletesVoucher(cartId : String , userId : String , voucherId : String) -> Observable<Void>


}

public protocol AddtoCartUseCase {
    func addToCart(userId : String , cartId : String , entry:AddToCartProductModel) -> Observable<AddedToCartModel>
    func addAllToCart(userId : String , cartId : String , wishlistPK:String) -> Observable<AddedToCartModel>
}

 
public protocol CartMeargUseCase {
    func meargeCart(userId : String , oldCartGuid : String , toMergeCartGuid:String) -> Observable<CartMargeModel>
}

public protocol CartQuantityUserCase {
    func updateCartQuantity(entryNumber : String , entry : CartEntry , userId : String , cartId : String) -> Observable<CartQuantityUpdate>
}

public protocol CartsUseCase {
    func getCarts(userId : String) -> Observable<CartsModel>
}

public protocol ValidationCartUseCase {
    func validationCart(userId : String , cartId : String) -> Observable<ValidationCartErrors>
}

public protocol BankOffersCartUseCase {
    func bankOffersCart() -> Observable<BankOffers>
}






 
