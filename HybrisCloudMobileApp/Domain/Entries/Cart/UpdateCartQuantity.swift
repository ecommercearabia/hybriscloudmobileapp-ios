//
//  UpdateCartQuantity.swift
//  Domain
//
//  Created by Saja Hammad on 8/18/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation

// MARK: - CartQuantityUpdate
public class CartQuantityUpdate: Codable , Identifiable{
public var identity: String{
    return statusCode ?? ""
}
        public let entry: CartQuantityEntry?
        public let quantity, quantityAdded: Int?
        public let statusCode: String?

        public init(entry: CartQuantityEntry?, quantity: Int?, quantityAdded: Int?, statusCode: String?) {
            self.entry = entry
            self.quantity = quantity
            self.quantityAdded = quantityAdded
            self.statusCode = statusCode
        }
    }

    // MARK: - CartQuantityEntry
    public struct CartQuantityEntry: Codable {
        public let configurationInfos: [JSONAny]?
        public let entryNumber: Int?
        public let product: CartQuantityProduct?
        public let quantity: Int?
        public let totalPrice: CartQuantityTotalPrice?

        public init(configurationInfos: [JSONAny]?, entryNumber: Int?, product: CartQuantityProduct?, quantity: Int?, totalPrice: CartQuantityTotalPrice?) {
            self.configurationInfos = configurationInfos
            self.entryNumber = entryNumber
            self.product = product
            self.quantity = quantity
            self.totalPrice = totalPrice
        }
    }

    // MARK: - CartQuantityProduct
    public struct CartQuantityProduct: Codable {
        public let availableForPickup: Bool?
        public let baseOptions: [CartQuantityBaseOption]?
        public let baseProduct: String?
        public let categories: [CartQuantityCategory]?
        public let code: String?
        public let configurable: Bool?
        public let name: String?
        public let purchasable: Bool?
        public let stock: CartQuantityProductStock?
        public let url: String?

        public init(availableForPickup: Bool?, baseOptions: [CartQuantityBaseOption]?, baseProduct: String?, categories: [CartQuantityCategory]?, code: String?, configurable: Bool?, name: String?, purchasable: Bool?, stock: CartQuantityProductStock?, url: String?) {
            self.availableForPickup = availableForPickup
            self.baseOptions = baseOptions
            self.baseProduct = baseProduct
            self.categories = categories
            self.code = code
            self.configurable = configurable
            self.name = name
            self.purchasable = purchasable
            self.stock = stock
            self.url = url
        }
    }

    // MARK: - CartQuantityBaseOption
    public struct CartQuantityBaseOption: Codable {
        public let selected: CartQuantitySelected?
        public let variantType: String?

        public init(selected: CartQuantitySelected?, variantType: String?) {
            self.selected = selected
            self.variantType = variantType
        }
    }

    // MARK: - CartQuantitySelected
    public struct CartQuantitySelected: Codable {
        public let code: String?
        public let priceData: CartQuantityTotalPrice?
        public let stock: CartQuantitySelectedStock?
        public let url: String?
        public let variantOptionQualifiers: [CartQuantityVariantOptionQualifier]?

        public init(code: String?, priceData: CartQuantityTotalPrice?, stock: CartQuantitySelectedStock?, url: String?, variantOptionQualifiers: [CartQuantityVariantOptionQualifier]?) {
            self.code = code
            self.priceData = priceData
            self.stock = stock
            self.url = url
            self.variantOptionQualifiers = variantOptionQualifiers
        }
    }

    // MARK: - CartQuantityTotalPrice
    public struct CartQuantityTotalPrice: Codable {
        public let currencyISO: String?
        public let value: Double?

        enum CodingKeys: String, CodingKey {
            case currencyISO = "currencyIso"
            case value
        }

        public init(currencyISO: String?, value: Double?) {
            self.currencyISO = currencyISO
            self.value = value
        }
    }

    // MARK: - CartQuantitySelectedStock
    public struct CartQuantitySelectedStock: Codable {
        public let stockLevel: Int?

        public init(stockLevel: Int?) {
            self.stockLevel = stockLevel
        }
    }

    // MARK: - CartQuantityVariantOptionQualifier
    public struct CartQuantityVariantOptionQualifier: Codable {
        public let name, qualifier, value: String?

        public init(name: String?, qualifier: String?, value: String?) {
            self.name = name
            self.qualifier = qualifier
            self.value = value
        }
    }

    // MARK: - CartQuantityCategory
    public struct CartQuantityCategory: Codable {
        public let code, name: String?

        public init(code: String?, name: String?) {
            self.code = code
            self.name = name
        }
    }

    // MARK: - CartQuantityProductStock
    public struct CartQuantityProductStock: Codable {
        public let stockLevel: Int?
        public let stockLevelStatus: String?

        public init(stockLevel: Int?, stockLevelStatus: String?) {
            self.stockLevel = stockLevel
            self.stockLevelStatus = stockLevelStatus
        }
    }
