//
//  LoyaltyPointsModel.swift
//  Domain
//
//  Created by khalil anqawi on 26/05/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation

// MARK: - LoyaltyPointsModel
public struct LoyaltyPointsModel: Codable ,Identifiable {
    public var identity: String{
        return "LoyaltyPointsModel"
    }
    public var loyaltyPointHistroy: [LoyaltyPointHistroy]?

    enum CodingKeys: String, CodingKey {
        case loyaltyPointHistroy = "loyaltyPointHistroy"
    }

    public init(loyaltyPointHistroy: [LoyaltyPointHistroy]?) {
        self.loyaltyPointHistroy = loyaltyPointHistroy
    }
}

// MARK: - LoyaltyPointHistroy
public struct LoyaltyPointHistroy: Codable {
    public var actionType: ActionType?
    public var balancePoints: Double?
    public var convertLimitLoyaltyPoints: Int?
    public var dateOfPurchase: String?
    public var loyaltyPointToStoreCreditConversion: Int?
    public var orderCode: String?
    public var points: Double?
    public var productTOLoyaltyPointsConversion: Double?

    enum CodingKeys: String, CodingKey {
        case actionType = "actionType"
        case balancePoints = "balancePoints"
        case convertLimitLoyaltyPoints = "convertLimitLoyaltyPoints"
        case dateOfPurchase = "dateOfPurchase"
        case loyaltyPointToStoreCreditConversion = "loyaltyPointToStoreCreditConversion"
        case orderCode = "orderCode"
        case points = "points"
        case productTOLoyaltyPointsConversion = "productTOLoyaltyPointsConversion"
    }

    public init(actionType: ActionType?, balancePoints: Double?, convertLimitLoyaltyPoints: Int?, dateOfPurchase: String?, loyaltyPointToStoreCreditConversion: Int?, orderCode: String?, points: Double?, productTOLoyaltyPointsConversion: Double?) {
        self.actionType = actionType
        self.balancePoints = balancePoints
        self.convertLimitLoyaltyPoints = convertLimitLoyaltyPoints
        self.dateOfPurchase = dateOfPurchase
        self.loyaltyPointToStoreCreditConversion = loyaltyPointToStoreCreditConversion
        self.orderCode = orderCode
        self.points = points
        self.productTOLoyaltyPointsConversion = productTOLoyaltyPointsConversion
    }
}

// MARK: - ActionType
public struct ActionType: Codable {
    public var code: String?
    public var name: String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case name = "name"
    }

    public init(code: String?, name: String?) {
        self.code = code
        self.name = name
    }
}
