//
//  AvailableLoyaltyPointsModel.swift
//  Domain
//
//  Created by khalil anqawi on 01/06/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//


import Foundation

// MARK: - LoyaltyPointsModel
public struct AvailableLoyaltyPointsModel: Codable ,Identifiable {
    public var identity: String{
        return "LoyaltyPointsModel"
    }
    public var availableLoyaltyPoints: Int?

  

    public init(availableLoyaltyPoints: Int?) {
        self.availableLoyaltyPoints = availableLoyaltyPoints
    }
}


