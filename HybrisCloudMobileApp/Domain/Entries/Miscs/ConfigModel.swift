//
//  configModel.swift
//  Domain
//
//  Created by khalil on 4/2/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation

// MARK: - ConfigModel
public struct ConfigModel: Codable, Identifiable {
    public var identity: String{
        return "ConfigModel"
    }
        public var addressConfiguration: AddressConfiguration?
        public var disableCouponForPickUpInStore: Bool?
        public var enableLoyaltyPoint: Bool?
        public var pickupInStoreEnabled: Bool?
        public var registrationConfiguration: RegistrationConfiguration?
        public var storeCreditConfiguration: StoreCreditConfiguration?
        enum CodingKeys: String, CodingKey {
            case addressConfiguration = "addressConfiguration"
            case disableCouponForPickUpInStore = "disableCouponForPickUpInStore"
            case enableLoyaltyPoint = "enableLoyaltyPoint"
            case pickupInStoreEnabled = "pickupInStoreEnabled"
            case registrationConfiguration = "registrationConfiguration"
            case storeCreditConfiguration = "storeCreditConfiguration"
        }

        public init(addressConfiguration: AddressConfiguration?, disableCouponForPickUpInStore: Bool?, enableLoyaltyPoint: Bool?, pickupInStoreEnabled: Bool?, registrationConfiguration: RegistrationConfiguration? , storeCreditConfiguration: StoreCreditConfiguration?) {
            self.addressConfiguration = addressConfiguration
            self.disableCouponForPickUpInStore = disableCouponForPickUpInStore
            self.enableLoyaltyPoint = enableLoyaltyPoint
            self.pickupInStoreEnabled = pickupInStoreEnabled
            self.registrationConfiguration = registrationConfiguration
            self.storeCreditConfiguration = storeCreditConfiguration
        }
    }



    // MARK: - AddressConfiguration
    public struct AddressConfiguration: Codable {
        public var apartmentNumberConfigurations: Configurations?
        public var buildingNameConfigurations: Configurations?
        public var enableMap: Bool?
        public var streetNameConfigurations: Configurations?

        enum CodingKeys: String, CodingKey {
            case apartmentNumberConfigurations = "apartmentNumberConfigurations"
            case buildingNameConfigurations = "buildingNameConfigurations"
            case enableMap = "enableMap"
            case streetNameConfigurations = "streetNameConfigurations"
        }

        public init(apartmentNumberConfigurations: Configurations?, buildingNameConfigurations: Configurations?, enableMap: Bool?, streetNameConfigurations: Configurations?) {
            self.apartmentNumberConfigurations = apartmentNumberConfigurations
            self.buildingNameConfigurations = buildingNameConfigurations
            self.enableMap = enableMap
            self.streetNameConfigurations = streetNameConfigurations
        }
    }

    // MARK: - Configurations
    public struct Configurations: Codable {
        public var enabled: Bool?
        public var hidden: Bool?
        public var configurationsRequired: Bool?

        enum CodingKeys: String, CodingKey {
            case enabled = "enabled"
            case hidden = "hidden"
            case configurationsRequired = "required"
        }

        public init(enabled: Bool?, hidden: Bool?, configurationsRequired: Bool?) {
            self.enabled = enabled
            self.hidden = hidden
            self.configurationsRequired = configurationsRequired
        }
    }

    // MARK: - RegistrationConfiguration
    public struct RegistrationConfiguration: Codable {
        public var nationalityConfigurations: Configurations?
        public var nationalityIDConfigurations: Configurations?
        public var referralCodeConfigurations: ReferralCodeConfigurations?

        enum CodingKeys: String, CodingKey {
            case nationalityConfigurations = "nationalityConfigurations"
            case nationalityIDConfigurations = "nationalityIdConfigurations"
            case referralCodeConfigurations = "referralCodeConfigurations"
        }

        public init(nationalityConfigurations: Configurations?, nationalityIDConfigurations: Configurations?, referralCodeConfigurations: ReferralCodeConfigurations?) {
            self.nationalityConfigurations = nationalityConfigurations
            self.nationalityIDConfigurations = nationalityIDConfigurations
            self.referralCodeConfigurations = referralCodeConfigurations
        }
    }

    // MARK: - ReferralCodeConfigurations
    public struct ReferralCodeConfigurations: Codable {
        public var enabled: Bool?
        public var receiverRewardAmount: Int?
        public var senderRewardAmount: Int?

        enum CodingKeys: String, CodingKey {
            case enabled = "enabled"
            case receiverRewardAmount = "receiverRewardAmount"
            case senderRewardAmount = "senderRewardAmount"
        }

        public init(enabled: Bool?, receiverRewardAmount: Int?, senderRewardAmount: Int?) {
            self.enabled = enabled
            self.receiverRewardAmount = receiverRewardAmount
            self.senderRewardAmount = senderRewardAmount
        }
    }

// MARK: - StoreCreditConfiguration
public struct StoreCreditConfiguration: Codable {
    public var enableCheckTotalPriceWithStoreCreditLimit: Bool?
    public var totalPriceWithStoreCreditLimit: Int?

    enum CodingKeys: String, CodingKey {
        case enableCheckTotalPriceWithStoreCreditLimit = "enableCheckTotalPriceWithStoreCreditLimit"
        case totalPriceWithStoreCreditLimit = "totalPriceWithStoreCreditLimit"
    }

    public init(enableCheckTotalPriceWithStoreCreditLimit: Bool?, totalPriceWithStoreCreditLimit: Int?) {
        self.enableCheckTotalPriceWithStoreCreditLimit = enableCheckTotalPriceWithStoreCreditLimit
        self.totalPriceWithStoreCreditLimit = totalPriceWithStoreCreditLimit
    }
}
