//
//  Titles.swift
//  Domain
//
//  Created by Saja Hammad on 7/21/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation

public struct Titles: Codable ,Identifiable{

    public var identity: String{
        return "titles"
    }
    
  public let titles: [Title]?

    enum CodingKeys: String, CodingKey {
        case titles = "titles"
    }
}

// MARK: - Title
public struct Title: Codable , Equatable {
  public let code: String?
  public let name: String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case name = "name"
    }
    
    public init(code: String?, name: String?) {
           self.code = code
           self.name = name
       }
}
