//
//  Component.swift
//  Domain
//
//  Created by Saja Hammad on 7/27/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation

// MARK: - Component
public struct Component: Codable ,Identifiable{

    public var identity: String{
    return uid ?? ""
    }
    
    public let uid, uuid, typeCode, modifiedTime: String?
    public let name, container: String?
    public let media: Media?
    public let responsiveRotatingComponent: String?
    public let content:String?
    public let urlLink:JSONAny?

    public init(uid: String?, uuid: String?, typeCode: String?, modifiedTime: String?, name: String?, container: String?, media: Media?, responsiveRotatingComponent: String?,content:String? , urlLink :JSONAny?) {
        self.uid = uid
        self.uuid = uuid
        self.typeCode = typeCode
        self.modifiedTime = modifiedTime
        self.name = name
        self.container = container
        self.media = media
        self.responsiveRotatingComponent = responsiveRotatingComponent
        self.content = content
        self.urlLink = urlLink
    }
}

 

// MARK: Component convenience initializers and mutators

public extension Component {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Component.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        uid: String?? = nil,
        uuid: String?? = nil,
        typeCode: String?? = nil,
        modifiedTime: String?? = nil,
        name: String?? = nil,
        container: String?? = nil,
        media: Media?? = nil,
        responsiveRotatingComponent: String?? = nil ,
        content :String? = nil ,
        urlLink :JSONAny?  = nil
    ) -> Component {
        return Component(
            uid: uid ?? self.uid,
            uuid: uuid ?? self.uuid,
            typeCode: typeCode ?? self.typeCode,
            modifiedTime: modifiedTime ?? self.modifiedTime,
            name: name ?? self.name,
            container: container ?? self.container,
            media: media ?? self.media,
            responsiveRotatingComponent: responsiveRotatingComponent ?? self.responsiveRotatingComponent ,
            content: content ?? self.content ,
            urlLink: urlLink ?? self.urlLink
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Media
public struct Media: Codable {
    public let tablet, desktop, mobile, widescreen: Desktop?

    public init(tablet: Desktop?, desktop: Desktop?, mobile: Desktop?, widescreen: Desktop?) {
        self.tablet = tablet
        self.desktop = desktop
        self.mobile = mobile
        self.widescreen = widescreen
    }
}

// MARK: Media convenience initializers and mutators

public extension Media {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Media.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        tablet: Desktop?? = nil,
        desktop: Desktop?? = nil,
        mobile: Desktop?? = nil,
        widescreen: Desktop?? = nil
    ) -> Media {
        return Media(
            tablet: tablet ?? self.tablet,
            desktop: desktop ?? self.desktop,
            mobile: mobile ?? self.mobile,
            widescreen: widescreen ?? self.widescreen
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Desktop
public struct Desktop: Codable {
    public let code, catalogID, mime, altText: String?
    public let url, downloadURL: String?

    enum CodingKeys: String, CodingKey {
        case code
        case catalogID = "catalogId"
        case mime, altText, url
        case downloadURL = "downloadUrl"
    }

    public init(code: String?, catalogID: String?, mime: String?, altText: String?, url: String?, downloadURL: String?) {
        self.code = code
        self.catalogID = catalogID
        self.mime = mime
        self.altText = altText
        self.url = url
        self.downloadURL = downloadURL
    }
}

// MARK: Desktop convenience initializers and mutators

public extension Desktop {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Desktop.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        code: String?? = nil,
        catalogID: String?? = nil,
        mime: String?? = nil,
        altText: String?? = nil,
        url: String?? = nil,
        downloadURL: String?? = nil
    ) -> Desktop {
        return Desktop(
            code: code ?? self.code,
            catalogID: catalogID ?? self.catalogID,
            mime: mime ?? self.mime,
            altText: altText ?? self.altText,
            url: url ?? self.url,
            downloadURL: downloadURL ?? self.downloadURL
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Helper functions for creating encoders and decoders

func componentJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func componentJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}
