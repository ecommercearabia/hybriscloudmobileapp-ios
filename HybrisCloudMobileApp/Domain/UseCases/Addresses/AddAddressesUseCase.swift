//
//  AddAddressesUseCase.swift
//  Domain
//
//  Created by walaa omar on 8/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol AddAddressesUseCase {

    func addAddress(userId: String, address: Address) -> Observable<Address>
    func updateAddress(userId: String, address: Address) -> Observable<Void>

}
