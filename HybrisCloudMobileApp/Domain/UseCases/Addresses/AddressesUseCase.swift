//
//  AddressesUseCase.swift
//  Domain
//
//  Created by Admin on 2020-08-03.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol AddressesUseCase {
    func fetchAddresses(userId: String) -> Observable<AddressesModel>
    func deleteAddress(userId: String, addressId: String) -> Observable<Void>
    func setDefaultAddress(userId: String, address: Address) -> Observable<Void>

//    func getAddress(userId: String, addressId: String) -> Observable<Address>
//    func addAddress(userId: String, address: Address) -> Observable<Address>
//    func updateAddress(userId: String, address: Address) -> Observable<Address>
//    func verifiesAddress(userId: String, address: Address) -> Observable<Address>
}
