//
//  SuggestionUseCase.swift
//  Domain
//
//  Created by ahmadSaleh on 12/31/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift


public protocol SuggestionUseCase {
    func Suggestion(max : String, term: String) -> Observable<SuggestionModel>
  }
