//
//  BestDealProductUseCase.swift
//  Domain
//
//  Created by Saja Hammad on 8/5/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import RxSwift

public protocol BestDealProductUseCase {
    func bestDealProduct(productCode: String) -> Observable<ProductDetails>

}
