//
//  ProductUseCase.swift
//  Domain
//
//  Created by khalil on 8/2/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift


public protocol ProductUseCase {
    func  search(categoryID : String,currentPage: Int,pageSize: Int,query: String,sort:String) -> Observable<ProductSearch>

    
    
}
