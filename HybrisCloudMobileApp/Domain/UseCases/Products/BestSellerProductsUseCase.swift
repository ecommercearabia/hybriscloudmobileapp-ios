//
//  BestSellerProductsUseCase.swift
//  Domain
//
//  Created by Saja Hammad on 8/6/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import RxSwift

public protocol BestSellerProductsUseCase {
    func bestSellerProduct(productCode: String) -> Observable<BestSellerProducts>

}
