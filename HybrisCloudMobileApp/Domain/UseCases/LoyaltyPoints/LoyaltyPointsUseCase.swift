//
//  LoyaltyPointsUseCase.swift
//  Domain
//
//  Created by khalil anqawi on 26/05/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol LoyaltyPointsUseCase {
    func getLoyaltyPoints() -> Observable<LoyaltyPointsModel>
    func getAvailableLoyaltyPoints() -> Observable<Double>
}

