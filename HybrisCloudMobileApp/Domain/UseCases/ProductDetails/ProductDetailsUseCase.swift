//
//  ProductDetailsUseCase.swift
//  Domain
//
//  Created by khalil on 8/8/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol ProductDetailsUseCase{
    func productDetails(productCode : String) -> Observable<ProductDetails>
}

//public protocol ProductRefernceUseCase{
//    func productRefernce(productCode : String) -> Observable<ProductRefrenceModel>
//}

public protocol ProductReviewsUseCase{
    func productReviews(productCode : String) -> Observable<Reviews>

}

public protocol ProductReviewUseCase{
    func addReviews(productCode : String,review:Review) -> Observable<Review>

}
