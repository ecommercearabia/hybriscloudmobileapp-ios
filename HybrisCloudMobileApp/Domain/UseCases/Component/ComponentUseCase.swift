//
//  ComponentUseCase.swift
//  Domain
//
//  Created by Saja Hammad on 7/27/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol ComponentUseCase {
    func component(componentId: String) -> Observable<Component>

}
