//
//  HomeUseCase.swift
//  Domain
//
//  Created by Saja Hammad on 7/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//



import Foundation
import RxSwift

public protocol PagesUseCase {
   // func pages() -> Observable<[Page]>
    func page(pageId: String) -> Observable<Page>
    func save(page: Page) -> Observable<Void>
    func delete(page: Page) -> Observable<Void>
}
