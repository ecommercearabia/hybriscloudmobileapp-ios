//
//  UserMessagesUseCase.swift
//  Domain
//
//  Created by Saja Hammad on 9/10/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import RxSwift


public protocol UserMessagesUseCase {
func getUserMessages() -> Observable<UserMessages>
    
}
