//
//  UseCaseProvider.swift
//  Domain
//
//  Created by Saja Hammad on 7/16/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation

public protocol UseCaseProvider {
    
    func makePagesUseCase() -> Domain.PagesUseCase
    func makeUserUseCase() -> Domain.UserUseCase
    func makeTitleUseCase() -> Domain.MiscsUseCase
    func makeComponentUseCase() -> Domain.ComponentUseCase
    func makebestSellerUserCase() -> Domain.BestSellerProductsUseCase
    func makeAddressesUseCase() ->  Domain.AddressesUseCase
    func makeAddToCartUseCase() ->  Domain.AddtoCartUseCase
    func makeChickOutUseCase() ->  Domain.chickOutUseCase
    func makeProductDetailsUseCase() -> Domain.ProductDetailsUseCase
    func makeCitiesUseCase() ->  Domain.CitiesUseCase
    func makeStoreCreditHistroyUseCase() ->  Domain.StoreCreditHistroyUseCase
    func makeStoreCreditAmountUseCase() ->  Domain.StoreCreditAmountUseCase
    func makeWishListUseCase() ->  Domain.WishListUseCase
    func makeForgetPassword() -> Domain.UserUseCase
    func makeUserMesseges() -> Domain.UserMessagesUseCase
    func makeSuggestionUseCase() -> Domain.SuggestionUseCase
    func makeReferralCodeUseCase() -> Domain.ReferralCodeUseCase
    func makeApplePayUseCase() -> Domain.applePayUseCase
    
    func makeConsentsList() -> Domain.ConsentsListUseCase
    func makeSavedCards() -> Domain.SavedCardsUseCase
    func makeConfig() -> Domain.ConfigUseCase
    func makeNationalities() -> Domain.NationalitiesUseCase
    func makeshipmentTypes() -> Domain.ShipmentTypesUseCase
    func makeLoyaltyPoints() -> Domain.LoyaltyPointsUseCase
    func makeDeliveryTypes() -> Domain.DeliveryTypesUseCase
}
