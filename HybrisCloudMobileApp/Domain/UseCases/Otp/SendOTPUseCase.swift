//
//  SendOTPUseCase.swift
//  Domain
//
//  Created by Saja Hammad on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift


public protocol SendOTPUseCase {
func sendOtp(sendOtpParam: SendOtpModel) -> Observable<SendOTP>
    
}
