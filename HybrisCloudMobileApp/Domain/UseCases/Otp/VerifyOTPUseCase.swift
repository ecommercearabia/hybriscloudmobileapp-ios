//
//  VerifyOTPUseCase.swift
//  Domain
//
//  Created by Saja Hammad on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import RxSwift


public protocol VerifyOTPUseCase {
    func vrifyOTP(sendOtpParam: SendOtpModel) -> Observable<VerifyOTP>
    
}
