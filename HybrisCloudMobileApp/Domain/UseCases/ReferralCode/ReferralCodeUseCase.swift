//
//  ReferralCodeUseCase.swift
//  Domain
//
//  Created by admin on 20/06/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol ReferralCodeUseCase {
    func getReferralCode() -> Observable<ReferralCode>
    func generateReferralCode() -> Observable<ReferralCode>
    func sendSubscribeEmail(email:String, inSeperateEmails:Bool) -> Observable<Void>
}
