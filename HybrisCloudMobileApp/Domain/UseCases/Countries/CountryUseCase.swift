//
//  CountryUseCase.swift
//  Domain
//
//  Created by Admin on 2020-08-03.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol CountryUseCase {
    func fetchCountries() -> Observable<CountriesModel>
    func fetchCountriesMobile() -> Observable<CountriesModel>
    func fetchCountriesShipping() -> Observable<CountriesModel>

}

public protocol RegionsUseCase {
    func fetchRegions(countryIsoCode: String) -> Observable<RegionsModel>
}

public protocol CitiesUseCase {
    func fetchCities(countryIsoCode: String) -> Observable<CitiesModel>
}

