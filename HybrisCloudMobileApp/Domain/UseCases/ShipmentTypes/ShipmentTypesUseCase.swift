//
//  shipmentTypesUseCase.swift
//  Domain
//
//  Created by khalil on 12/2/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol ShipmentTypesUseCase {
    func getShipmentTypes() -> Observable<ShipmentTypesModel>
    func selectedShipmentType(code: String) -> Observable<ShipmentSelctedShipmentTypesModel>
    func getType() -> Observable<CurrentShipmentTypeModel>
    
}

