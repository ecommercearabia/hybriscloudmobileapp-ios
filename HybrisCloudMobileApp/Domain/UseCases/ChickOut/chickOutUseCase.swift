//
//  chickOutUseCase.swift
//  Domain
//
//  Created by ahmadSaleh on 8/9/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift


public protocol chickOutUseCase {
    func SetDeliveryAddress(addressId : String) -> Observable<Void>
    func getTimeSlots() -> Observable<TimeSlotsModel>
    func setTimeSlots(cartId : String , timeSlotInfos : SetTimeSlotModel
    ) -> Observable<Void>
}

public protocol setDeliveryModeUseCase {
    func getDeliveryModes(cartId : String) -> Observable<DeliveryModeModel>
    func setDeliveryMode(deliveryModeId : String , cartId : String) -> Observable<Void>
}

 
public protocol paymentModeUseCase {
    func getPaymentModes() -> Observable<PaymentModesModel>
    func setPaymentMode(paymentModeCode : String) -> Observable<SetPaymentModeResponse>
 }

public protocol StoreCreditCartUseCase {
    func getStoreCreditModes(cartId : String , userId : String) -> Observable<StoreCreditModes>
    func setStoreCreditMode(amount : Double , cartId : String , storeCreditModeTypeCode : String , userId : String) -> Observable<Void>
    func getStoreCreditAvailableAmount(userId : String) -> Observable<StoreCreditAvailableAmountModel>
}


public protocol applePayUseCase {
    func setApplePay( paymentOption : String , cardType : String ,cardName : String ,  paymentData : String , appleCardType : String ,  amount : String , currency : String  , orderId : String  , userId : String  , cartId : String) -> Observable<ApplePayModel>
    
    
    
}



