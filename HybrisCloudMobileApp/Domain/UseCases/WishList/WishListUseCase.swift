//
//  WishListUseCase.swift
//  Domain
//
//  Created by Saja Hammad on 8/26/20.
//  Copyright © 2020 Erabia. All rights reserved.
//


import Foundation
import RxSwift


public protocol WishListUseCase {
    func getWishList() -> Observable<WishList>
    func deleteWishListIteam(deleteIteamData: AddDeleteWishListItemRequest) -> Observable<Void>
    func addWishListIteam(iteamData: AddDeleteWishListItemRequest) -> Observable<Void>
    
}
