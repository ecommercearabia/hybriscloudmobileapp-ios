//
//  SavedCardsUseCase.swift
//  Domain
//
//  Created by walaa omar on 26/12/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol SavedCardsUseCase {
    func fetchSavedCards(userId: String) -> Observable<SavedCardsModel>
    func deleteSavedCards(userId: String, savedCardId: String) -> Observable<Void>

}

