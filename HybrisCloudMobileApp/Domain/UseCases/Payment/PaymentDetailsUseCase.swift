//
//  PaymentUseCase.swift
//  Domain
//
//  Created by Ahmad Bader on 8/18/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol PaymentDetailsUseCase {
    func cartPaymentDetials(paymentDetails: PaymentDetailsRequest) -> Observable<PaymentDetailsResponse>
    func cartPaymentDetials(trackingId: String) -> Observable<PaymentDetailsResponse>
}

public protocol PaymentInfoUseCase {
    func cartPaymentInfo() -> Observable<PaymentInfo>
}
