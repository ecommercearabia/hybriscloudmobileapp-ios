//
//  RegisterUseCase.swift
//  Domain
//
//  Created by Saja Hammad on 7/21/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol UserUseCase {
    func register(user: UserSignup) -> Observable<UserData>
    func login(user: UserLogin) -> Observable<Void>
    func logOut() -> Observable<Void>
    func changePassword(user: ChangePasswordRequest) -> Observable<Void>
    func getCustomerProfile(userId : String) -> Observable<UserData>
    func updateCustomerProfile(userId : String,user:UserData) -> Observable<Void>
    func updateEmailAddress(newLogin : String, password : String , userId : String) -> Observable<Void>
    func forgetPassword(forgetPasswordParam: String) -> Observable<Void>
    func verifyOtpCode(code : String , email: String , name : String) -> Observable<Void>
    
    func setOrUpdateFaceId(signatureId : String , userId : String) -> Observable<Void>
    func loginWithFaceId( signatureId: String , email : String) -> Observable<LoginFaceID>
    func setFirebaseToken( mobileToken : String ) -> Observable<Void>
}

public protocol StoreCreditHistroyUseCase {
    func getHistory(userId: String) -> Observable<StoreCreditHistroy>
}

public protocol StoreCreditAmountUseCase {
    func getAmount(userId: String) -> Observable<StoreCreditAmount>
}

public protocol SendOtpCodeAutoLoginUseCase {
    func sendOtpCode(userId: String) -> Observable<SendOtpCode>
}

