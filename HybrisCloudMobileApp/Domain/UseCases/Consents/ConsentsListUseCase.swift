//
//  ConsentsListUseCase.swift
//  Domain
//
//  Created by khalil on 9/9/2021.
//  Copyright © 2021 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol ConsentsListUseCase {
    func getConsentsList() -> Observable<Consents>
    func addConsent(consentTemplateId :String  , Version : Int) -> Observable<AddConsent>
    func deleteConsent(code:String) -> Observable<Void>
}
