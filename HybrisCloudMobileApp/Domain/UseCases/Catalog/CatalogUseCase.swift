//
//  CatalogUseCase.swift
//  Domain
//
//  Created by Ahmad Bader on 8/1/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation


import Foundation
import RxSwift

public protocol CatalogUseCase {
    func categories(id: String,version: String) -> Observable<Catalog>

}
