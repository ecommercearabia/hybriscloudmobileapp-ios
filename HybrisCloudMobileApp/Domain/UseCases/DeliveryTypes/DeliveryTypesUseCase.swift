//
//  DeliveryTypesUseCase.swift
//  Domain
//
//  Created by khalil anqawi on 12/11/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol DeliveryTypesUseCase {
    func getDeliveryTypes() -> Observable<DeliveryTypesModel>
    func getDeliveryType() -> Observable<String>
    func setDeliveryType(deliveryType : String) -> Observable<CartModel>
    
}

