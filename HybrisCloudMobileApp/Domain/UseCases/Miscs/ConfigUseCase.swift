//
//  ConfigModel.swift
//  Domain
//
//  Created by khalil on 4/2/2022.
//  Copyright © 2022 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol ConfigUseCase {
    func config() -> Observable<ConfigModel>

}
