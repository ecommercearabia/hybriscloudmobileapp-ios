//
//  TitleUseCase.swift
//  Domain
//
//  Created by Saja Hammad on 7/21/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol MiscsUseCase {
    func titles() -> Observable<Titles>

}
