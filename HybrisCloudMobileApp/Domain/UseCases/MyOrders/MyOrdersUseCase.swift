//
//  MyOrdersUseCase.swift
//  Domain
//
//  Created by khalil on 8/11/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift

public protocol MyOrdersUseCase {
    
    func getMyOrders(currentPage : Int , pageSize: Int , statuses: String , sort:String , userId : String) -> Observable<MyOrders>
}

