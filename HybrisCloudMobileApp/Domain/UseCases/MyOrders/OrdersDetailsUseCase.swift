//
//  OrdersDetailsUseCase.swift
//  Domain
//
//  Created by khalil on 8/12/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift
public protocol OrdersDetailsUseCase {
    
    func getOrdersDetails(userId : String , orderCode : String)-> Observable<ordersDetailsMyOrdersDetails>
    
}
