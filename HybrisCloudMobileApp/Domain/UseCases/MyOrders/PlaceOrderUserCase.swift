//
//  PlaceOrderUserCase.swift
//  Domain
//
//  Created by Saja Hammad on 8/18/20.
//  Copyright © 2020 Erabia. All rights reserved.
//

import Foundation
import RxSwift
public protocol PlaceOrderUserCase {
    
    func setPlaceOrderUserCase(cartId : String)-> Observable<PalceOrderResponse>
    
}
